from turtle import width
from dash import dcc
from dash import html

from dash.dependencies import Input, Output
from dash.exceptions import PreventUpdate
import plotly.express as px
import dash

from app import app
import method as me
import json
import os
import pandas as pd

#structure of dashboard
layout1 = html.Div([
    html.Div([
    html.Div([
        html.H4("Steerable MNN Projection"),
        html.H6("by Joep Robben")
    ], style={'padding': '10px'}),
    html.Div([
        html.Div([
            html.Div([
            dcc.Dropdown(id='demo-dropdown1',
                options=[
                    {'label': 'PCA', 'value': 'PCA'},
                    {'label': 'UMAP', 'value': 'UMAP'},
                    {'label': 'LLE', 'value': 'LLE'},
                    {'label': 'TSNE', 'value': 'TSNE'},
                    {'label': 'ISOMAP', 'value': 'ISOMAP'},
                    {'label': 't-SVD', 'value': 'TSVD'}
                ], value='PCA', searchable=False), html.Div(id='dd-output-container1'),
            ], style={'padding': '5px', 'width': '150px', 'display': 'inline-block', 'textAlign': 'left'}),

            html.Div([
                dcc.Slider(
                    id='my-slider1',
                    min=0,
                    max=100,
                    step=5,
                    value=100,
                    marks={0: '0', 
                            10: '10', 
                            20: '20',
                            30: '30',
                            40: '40',
                            50: '50',
                            60: '60',
                            70: '70',
                            80: '80',
                            90: '90',
                            100: '100'},
                    included=False),
                html.Div(id='slider-output-container1')
            ], style={'width': '300px', 'padding-top': '0px', 'display': 'inline-block'}),
        ], style={'textAlign': 'left'}),

        html.Div([
            html.Div([
            dcc.Dropdown(id='demo-dropdown2',
                options=[
                    {'label': 'PCA', 'value': 'PCA'},
                    {'label': 'UMAP', 'value': 'UMAP'},
                    {'label': 'LLE', 'value': 'LLE'},
                    {'label': 'TSNE', 'value': 'TSNE'},
                    {'label': 'ISOMAP', 'value': 'ISOMAP'},
                    {'label': 't-SVD', 'value': 'TSVD'}
                ], value='UMAP', searchable=False), html.Div(id='dd-output-container2'),
            ], style={'padding': '5px', 'width': '150px', 'display': 'inline-block', 'textAlign': 'left'}),

            html.Div([
                dcc.Slider(
                    id='my-slider2',
                    min=0,
                    max=100,
                    step=5,
                    value=0,
                    marks={0: '0', 
                            10: '10', 
                            20: '20',
                            30: '30',
                            40: '40',
                            50: '50',
                            60: '60',
                            70: '70',
                            80: '80',
                            90: '90',
                            100: '100'},
                    included=False),
                html.Div(id='slider-output-container2')
            ], style={'width': '300px', 'padding-top': '10px', 'display': 'inline-block'}),
        ], style={'textAlign': 'left'}),

        html.Div([
            html.Div([
            dcc.Dropdown(id='demo-dropdown3',
                options=[
                    {'label': 'PCA', 'value': 'PCA'},
                    {'label': 'UMAP', 'value': 'UMAP'},
                    {'label': 'LLE', 'value': 'LLE'},
                    {'label': 'TSNE', 'value': 'TSNE'},
                    {'label': 'ISOMAP', 'value': 'ISOMAP'},
                    {'label': 't-SVD', 'value': 'TSVD'}
                ], value='TSVD', searchable=False), html.Div(id='dd-output-container3'),
            ], style={'padding': '5px', 'width': '150px', 'display': 'inline-block', 'textAlign': 'left'}),

            html.Div([
                dcc.Slider(
                    id='my-slider3',
                    min=0,
                    max=100,
                    step=5,
                    value=0,
                    marks={0: '0', 
                            10: '10', 
                            20: '20',
                            30: '30',
                            40: '40',
                            50: '50',
                            60: '60',
                            70: '70',
                            80: '80',
                            90: '90',
                            100: '100'},
                    included=False),
                html.Div(id='slider-output-container3')
            ], style={'width': '300px', 'padding-top': '10px', 'display': 'inline-block'}),
        ]),
    ]),
    

    html.Div(
    [
        html.Div([
            html.Div(children='Data set'),
            dcc.Input(
            id="input_dataset",
            name="Data set",
            type="text",
            value="nmnist",
        )], style={'width': '100px', 'padding-top': '10px'}),
        
        html.Div([
            html.Div(children='Classes'),
            dcc.Input(
            id="input_classes",
            type="text",
            value='2',
        )], style={'width': '100px', 'padding-top': '10px'}),
        
        html.Div([
            html.Div(children='Samples'),
            dcc.Input(
            id="input_training_samples",
            type="text",
            value='5000',
        )], style={'width': '100px', 'padding-top': '10px'})
    ], style={'padding-left':'5px'}),

    html.Div(
    [
        html.Div(id='output-container-loss',
             children='Loss: 0'),

        html.Div(id='output-container-trustworthiness',
             children='Trustworthiness: 0'),

        html.Div(id='output-container-continuity',
             children='Continuity: 0'),

        html.Div(id='output-container-neighborhood',
             children='Neighborhood hit: 0')
    ], style={'width': '300px', 'padding-top': '10px', 'padding-left':'10px'}),


    html.Div([
        html.Button('Go!', id='submit-val', n_clicks=0),
        html.Div(id='container-button-basic')
    ], style={'padding-top': '30px', 'padding-left':'5px'})

    ], style={'width': '40%', 'display': 'inline-block', 'padding-left':'70px'}),
        

    html.Div([
        dcc.Loading(
            id="ls-loading-2",
            children = html.Div([dcc.Graph(id="dbsplom")]),
            type="circle")
    ], style={'width': '50%', 'display': 'inline-block'})
   
    
])
@app.callback(
    [Output('output-container-loss', 'children'), Output('output-container-trustworthiness', 'children'), Output('output-container-continuity', 'children'), Output('output-container-neighborhood', 'children'), Output('input_dataset', 'value'),  Output('input_classes', 'value'), Output('input_training_samples', 'value'), Output('dd-output-container1', 'children'), Output('dd-output-container2', 'children'), Output('dd-output-container3', 'children'), Output('slider-output-container1', 'children'), Output('slider-output-container2', 'children'), Output('slider-output-container3', 'children'), Output('container-button-basic', 'children'), Output("dbsplom", "figure")],
    [Input('input_dataset', 'value'), Input('input_classes', 'value'), Input('input_training_samples', 'value'), Input('demo-dropdown1', 'value'), Input('demo-dropdown2', 'value'), Input('demo-dropdown3', 'value'), Input('my-slider1', 'value'), Input('my-slider2', 'value'), Input('my-slider3', 'value'), Input('submit-val', 'n_clicks')]
)
def update_output(dataset, classes, training_samples, value1, value2, value3, slider_value1, slider_value2, slider_value3, n_clicks):
    list_of_sv = [str(slider_value1 / 100), str(slider_value2 / 100), str(slider_value3 / 100)]

    list_of_v = []
    for v in list_of_sv:
        if v == '1.0' or v == '0.0':
            list_of_v.append(int(float(v)))
        else:
            list_of_v.append(float(v))

    filename = "config.json"
    with open(filename) as jsonFile:
            jsonObject = json.load(jsonFile)
            jsonObject['DATASET'] = dataset
            jsonObject['NUMBER_OF_CLASSES'] = classes
            jsonObject['NUMBER_OF_TSAMPLES'] = training_samples
            jsonObject['DRS'] = [value1, value2, value3]
            jsonObject['PROPORTIONS'] = list_of_v

    #constants
    NUMBER_OF_POINTS = int(jsonObject['NUMBER_OF_POINTS'])
    NUMBER_OF_TSAMPLES = int(jsonObject['NUMBER_OF_TSAMPLES'])
    NUMBER_OF_VSAMPLES = int(jsonObject['NUMBER_OF_VSAMPLES'])
    DRS = list(jsonObject['DRS'])
    REPLACE = int(jsonObject['REPLACE'])
    PROPORTIONS = list(jsonObject['PROPORTIONS'])
    DRS_NAME = ''.join(DRS).lower()
    PROPORTIONS_NAME = ''.join(str(PROPORTIONS)).lower()
    DATASET = str(jsonObject['DATASET'])
    NUMBER_OF_CLASSES = int(jsonObject['NUMBER_OF_CLASSES'])
    
    fig = px.scatter(width=700, height=700)
    fig = fig.update_yaxes(range=[-0.1, 1.1], title='PC2')
    fig = fig.update_xaxes(range=[-0.1, 1.1], title='PC1')
    fig = fig.update_traces(marker=dict(size=2))

    changed_id = [p['prop_id'] for p in dash.callback_context.triggered][0]
    loss = 0
    trustworthiness = 0
    continuity = 0
    neighborhood_hit = 0

    if 'submit-val' in changed_id:
        print('train')
        loss, trustworthiness, continuity, neighborhood_hit = me.method()
        prediction = pd.read_csv(('results/projections/{b}{e}_{w}{v}_{f}_{g}.csv').format(b=DATASET[0:3],e=NUMBER_OF_CLASSES, w=DRS_NAME, v=PROPORTIONS, f=NUMBER_OF_TSAMPLES, g=NUMBER_OF_VSAMPLES))
        prediction["label"] = prediction["label"].astype(str)
        fig = px.scatter(prediction, x="PC1", y="PC2", color="label", width=700, height=700)
        fig = fig.update_yaxes(range=[-0.1, 1.1], title='PC2')
        fig = fig.update_xaxes(range=[-0.1, 1.1], title='PC1')
        fig = fig.update_traces(marker=dict(size=2))

    os.remove(filename)
    with open(filename, 'w') as f:
        json.dump(jsonObject, f, indent=2)

    return "Loss: {}".format(loss), "Trustworthiness: {}".format(trustworthiness), "Continuity: {}".format(continuity), "Neighborhood hit: {}".format(neighborhood_hit), dash.no_update, dash.no_update, dash.no_update, dash.no_update, dash.no_update, dash.no_update, dash.no_update, dash.no_update, dash.no_update, dash.no_update, fig

    
