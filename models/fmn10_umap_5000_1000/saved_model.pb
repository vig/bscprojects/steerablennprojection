��
��
B
AssignVariableOp
resource
value"dtype"
dtypetype�
~
BiasAdd

value"T	
bias"T
output"T" 
Ttype:
2	"-
data_formatstringNHWC:
NHWCNCHW
8
Const
output"dtype"
valuetensor"
dtypetype
.
Identity

input"T
output"T"	
Ttype
q
MatMul
a"T
b"T
product"T"
transpose_abool( "
transpose_bbool( "
Ttype:

2	
e
MergeV2Checkpoints
checkpoint_prefixes
destination_prefix"
delete_old_dirsbool(�

NoOp
M
Pack
values"T*N
output"T"
Nint(0"	
Ttype"
axisint 
C
Placeholder
output"dtype"
dtypetype"
shapeshape:
@
ReadVariableOp
resource
value"dtype"
dtypetype�
E
Relu
features"T
activations"T"
Ttype:
2	
o
	RestoreV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0�
l
SaveV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0�
?
Select
	condition

t"T
e"T
output"T"	
Ttype
H
ShardedFilename
basename	
shard

num_shards
filename
0
Sigmoid
x"T
y"T"
Ttype:

2
�
StatefulPartitionedCall
args2Tin
output2Tout"
Tin
list(type)("
Tout
list(type)("	
ffunc"
configstring "
config_protostring "
executor_typestring �
@
StaticRegexFullMatch	
input

output
"
patternstring
N

StringJoin
inputs*N

output"
Nint(0"
	separatorstring 
�
VarHandleOp
resource"
	containerstring "
shared_namestring "
dtypetype"
shapeshape"#
allowed_deviceslist(string)
 �"serve*2.6.02unknown8��
�
fmn10_umap_5000_1000_1/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*.
shared_namefmn10_umap_5000_1000_1/kernel
�
1fmn10_umap_5000_1000_1/kernel/Read/ReadVariableOpReadVariableOpfmn10_umap_5000_1000_1/kernel* 
_output_shapes
:
��*
dtype0
�
fmn10_umap_5000_1000_1/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*,
shared_namefmn10_umap_5000_1000_1/bias
�
/fmn10_umap_5000_1000_1/bias/Read/ReadVariableOpReadVariableOpfmn10_umap_5000_1000_1/bias*
_output_shapes	
:�*
dtype0
�
fmn10_umap_5000_1000_2/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*.
shared_namefmn10_umap_5000_1000_2/kernel
�
1fmn10_umap_5000_1000_2/kernel/Read/ReadVariableOpReadVariableOpfmn10_umap_5000_1000_2/kernel* 
_output_shapes
:
��*
dtype0
�
fmn10_umap_5000_1000_2/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*,
shared_namefmn10_umap_5000_1000_2/bias
�
/fmn10_umap_5000_1000_2/bias/Read/ReadVariableOpReadVariableOpfmn10_umap_5000_1000_2/bias*
_output_shapes	
:�*
dtype0
�
fmn10_umap_5000_1000_3/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*.
shared_namefmn10_umap_5000_1000_3/kernel
�
1fmn10_umap_5000_1000_3/kernel/Read/ReadVariableOpReadVariableOpfmn10_umap_5000_1000_3/kernel* 
_output_shapes
:
��*
dtype0
�
fmn10_umap_5000_1000_3/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*,
shared_namefmn10_umap_5000_1000_3/bias
�
/fmn10_umap_5000_1000_3/bias/Read/ReadVariableOpReadVariableOpfmn10_umap_5000_1000_3/bias*
_output_shapes	
:�*
dtype0
�
fmn10_umap_5000_1000_out/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:	�*0
shared_name!fmn10_umap_5000_1000_out/kernel
�
3fmn10_umap_5000_1000_out/kernel/Read/ReadVariableOpReadVariableOpfmn10_umap_5000_1000_out/kernel*
_output_shapes
:	�*
dtype0
�
fmn10_umap_5000_1000_out/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:*.
shared_namefmn10_umap_5000_1000_out/bias
�
1fmn10_umap_5000_1000_out/bias/Read/ReadVariableOpReadVariableOpfmn10_umap_5000_1000_out/bias*
_output_shapes
:*
dtype0
f
	Adam/iterVarHandleOp*
_output_shapes
: *
dtype0	*
shape: *
shared_name	Adam/iter
_
Adam/iter/Read/ReadVariableOpReadVariableOp	Adam/iter*
_output_shapes
: *
dtype0	
j
Adam/beta_1VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nameAdam/beta_1
c
Adam/beta_1/Read/ReadVariableOpReadVariableOpAdam/beta_1*
_output_shapes
: *
dtype0
j
Adam/beta_2VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nameAdam/beta_2
c
Adam/beta_2/Read/ReadVariableOpReadVariableOpAdam/beta_2*
_output_shapes
: *
dtype0
h

Adam/decayVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name
Adam/decay
a
Adam/decay/Read/ReadVariableOpReadVariableOp
Adam/decay*
_output_shapes
: *
dtype0
x
Adam/learning_rateVarHandleOp*
_output_shapes
: *
dtype0*
shape: *#
shared_nameAdam/learning_rate
q
&Adam/learning_rate/Read/ReadVariableOpReadVariableOpAdam/learning_rate*
_output_shapes
: *
dtype0
^
totalVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nametotal
W
total/Read/ReadVariableOpReadVariableOptotal*
_output_shapes
: *
dtype0
^
countVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_namecount
W
count/Read/ReadVariableOpReadVariableOpcount*
_output_shapes
: *
dtype0
b
total_1VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name	total_1
[
total_1/Read/ReadVariableOpReadVariableOptotal_1*
_output_shapes
: *
dtype0
b
count_1VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name	count_1
[
count_1/Read/ReadVariableOpReadVariableOpcount_1*
_output_shapes
: *
dtype0
�
$Adam/fmn10_umap_5000_1000_1/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*5
shared_name&$Adam/fmn10_umap_5000_1000_1/kernel/m
�
8Adam/fmn10_umap_5000_1000_1/kernel/m/Read/ReadVariableOpReadVariableOp$Adam/fmn10_umap_5000_1000_1/kernel/m* 
_output_shapes
:
��*
dtype0
�
"Adam/fmn10_umap_5000_1000_1/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*3
shared_name$"Adam/fmn10_umap_5000_1000_1/bias/m
�
6Adam/fmn10_umap_5000_1000_1/bias/m/Read/ReadVariableOpReadVariableOp"Adam/fmn10_umap_5000_1000_1/bias/m*
_output_shapes	
:�*
dtype0
�
$Adam/fmn10_umap_5000_1000_2/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*5
shared_name&$Adam/fmn10_umap_5000_1000_2/kernel/m
�
8Adam/fmn10_umap_5000_1000_2/kernel/m/Read/ReadVariableOpReadVariableOp$Adam/fmn10_umap_5000_1000_2/kernel/m* 
_output_shapes
:
��*
dtype0
�
"Adam/fmn10_umap_5000_1000_2/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*3
shared_name$"Adam/fmn10_umap_5000_1000_2/bias/m
�
6Adam/fmn10_umap_5000_1000_2/bias/m/Read/ReadVariableOpReadVariableOp"Adam/fmn10_umap_5000_1000_2/bias/m*
_output_shapes	
:�*
dtype0
�
$Adam/fmn10_umap_5000_1000_3/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*5
shared_name&$Adam/fmn10_umap_5000_1000_3/kernel/m
�
8Adam/fmn10_umap_5000_1000_3/kernel/m/Read/ReadVariableOpReadVariableOp$Adam/fmn10_umap_5000_1000_3/kernel/m* 
_output_shapes
:
��*
dtype0
�
"Adam/fmn10_umap_5000_1000_3/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*3
shared_name$"Adam/fmn10_umap_5000_1000_3/bias/m
�
6Adam/fmn10_umap_5000_1000_3/bias/m/Read/ReadVariableOpReadVariableOp"Adam/fmn10_umap_5000_1000_3/bias/m*
_output_shapes	
:�*
dtype0
�
&Adam/fmn10_umap_5000_1000_out/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:	�*7
shared_name(&Adam/fmn10_umap_5000_1000_out/kernel/m
�
:Adam/fmn10_umap_5000_1000_out/kernel/m/Read/ReadVariableOpReadVariableOp&Adam/fmn10_umap_5000_1000_out/kernel/m*
_output_shapes
:	�*
dtype0
�
$Adam/fmn10_umap_5000_1000_out/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:*5
shared_name&$Adam/fmn10_umap_5000_1000_out/bias/m
�
8Adam/fmn10_umap_5000_1000_out/bias/m/Read/ReadVariableOpReadVariableOp$Adam/fmn10_umap_5000_1000_out/bias/m*
_output_shapes
:*
dtype0
�
$Adam/fmn10_umap_5000_1000_1/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*5
shared_name&$Adam/fmn10_umap_5000_1000_1/kernel/v
�
8Adam/fmn10_umap_5000_1000_1/kernel/v/Read/ReadVariableOpReadVariableOp$Adam/fmn10_umap_5000_1000_1/kernel/v* 
_output_shapes
:
��*
dtype0
�
"Adam/fmn10_umap_5000_1000_1/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*3
shared_name$"Adam/fmn10_umap_5000_1000_1/bias/v
�
6Adam/fmn10_umap_5000_1000_1/bias/v/Read/ReadVariableOpReadVariableOp"Adam/fmn10_umap_5000_1000_1/bias/v*
_output_shapes	
:�*
dtype0
�
$Adam/fmn10_umap_5000_1000_2/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*5
shared_name&$Adam/fmn10_umap_5000_1000_2/kernel/v
�
8Adam/fmn10_umap_5000_1000_2/kernel/v/Read/ReadVariableOpReadVariableOp$Adam/fmn10_umap_5000_1000_2/kernel/v* 
_output_shapes
:
��*
dtype0
�
"Adam/fmn10_umap_5000_1000_2/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*3
shared_name$"Adam/fmn10_umap_5000_1000_2/bias/v
�
6Adam/fmn10_umap_5000_1000_2/bias/v/Read/ReadVariableOpReadVariableOp"Adam/fmn10_umap_5000_1000_2/bias/v*
_output_shapes	
:�*
dtype0
�
$Adam/fmn10_umap_5000_1000_3/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*5
shared_name&$Adam/fmn10_umap_5000_1000_3/kernel/v
�
8Adam/fmn10_umap_5000_1000_3/kernel/v/Read/ReadVariableOpReadVariableOp$Adam/fmn10_umap_5000_1000_3/kernel/v* 
_output_shapes
:
��*
dtype0
�
"Adam/fmn10_umap_5000_1000_3/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*3
shared_name$"Adam/fmn10_umap_5000_1000_3/bias/v
�
6Adam/fmn10_umap_5000_1000_3/bias/v/Read/ReadVariableOpReadVariableOp"Adam/fmn10_umap_5000_1000_3/bias/v*
_output_shapes	
:�*
dtype0
�
&Adam/fmn10_umap_5000_1000_out/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:	�*7
shared_name(&Adam/fmn10_umap_5000_1000_out/kernel/v
�
:Adam/fmn10_umap_5000_1000_out/kernel/v/Read/ReadVariableOpReadVariableOp&Adam/fmn10_umap_5000_1000_out/kernel/v*
_output_shapes
:	�*
dtype0
�
$Adam/fmn10_umap_5000_1000_out/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:*5
shared_name&$Adam/fmn10_umap_5000_1000_out/bias/v
�
8Adam/fmn10_umap_5000_1000_out/bias/v/Read/ReadVariableOpReadVariableOp$Adam/fmn10_umap_5000_1000_out/bias/v*
_output_shapes
:*
dtype0

NoOpNoOp
�1
ConstConst"/device:CPU:0*
_output_shapes
: *
dtype0*�0
value�0B�0 B�0
�
layer-0
layer_with_weights-0
layer-1
layer_with_weights-1
layer-2
layer_with_weights-2
layer-3
layer_with_weights-3
layer-4
	optimizer
	variables
regularization_losses
	trainable_variables

	keras_api

signatures
 
h

kernel
bias
	variables
regularization_losses
trainable_variables
	keras_api
h

kernel
bias
	variables
regularization_losses
trainable_variables
	keras_api
h

kernel
bias
	variables
regularization_losses
trainable_variables
	keras_api
h

kernel
bias
 	variables
!regularization_losses
"trainable_variables
#	keras_api
�
$iter

%beta_1

&beta_2
	'decay
(learning_ratemMmNmOmPmQmRmSmTvUvVvWvXvYvZv[v\
8
0
1
2
3
4
5
6
7
 
8
0
1
2
3
4
5
6
7
�
)metrics

*layers
	variables
+layer_metrics
,layer_regularization_losses
regularization_losses
	trainable_variables
-non_trainable_variables
 
ig
VARIABLE_VALUEfmn10_umap_5000_1000_1/kernel6layer_with_weights-0/kernel/.ATTRIBUTES/VARIABLE_VALUE
ec
VARIABLE_VALUEfmn10_umap_5000_1000_1/bias4layer_with_weights-0/bias/.ATTRIBUTES/VARIABLE_VALUE

0
1
 

0
1
�
.metrics

/layers
	variables
regularization_losses
0layer_metrics
1layer_regularization_losses
trainable_variables
2non_trainable_variables
ig
VARIABLE_VALUEfmn10_umap_5000_1000_2/kernel6layer_with_weights-1/kernel/.ATTRIBUTES/VARIABLE_VALUE
ec
VARIABLE_VALUEfmn10_umap_5000_1000_2/bias4layer_with_weights-1/bias/.ATTRIBUTES/VARIABLE_VALUE

0
1
 

0
1
�
3metrics

4layers
	variables
regularization_losses
5layer_metrics
6layer_regularization_losses
trainable_variables
7non_trainable_variables
ig
VARIABLE_VALUEfmn10_umap_5000_1000_3/kernel6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUE
ec
VARIABLE_VALUEfmn10_umap_5000_1000_3/bias4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUE

0
1
 

0
1
�
8metrics

9layers
	variables
regularization_losses
:layer_metrics
;layer_regularization_losses
trainable_variables
<non_trainable_variables
ki
VARIABLE_VALUEfmn10_umap_5000_1000_out/kernel6layer_with_weights-3/kernel/.ATTRIBUTES/VARIABLE_VALUE
ge
VARIABLE_VALUEfmn10_umap_5000_1000_out/bias4layer_with_weights-3/bias/.ATTRIBUTES/VARIABLE_VALUE

0
1
 

0
1
�
=metrics

>layers
 	variables
!regularization_losses
?layer_metrics
@layer_regularization_losses
"trainable_variables
Anon_trainable_variables
HF
VARIABLE_VALUE	Adam/iter)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUE
LJ
VARIABLE_VALUEAdam/beta_1+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUE
LJ
VARIABLE_VALUEAdam/beta_2+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUE
JH
VARIABLE_VALUE
Adam/decay*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUE
ZX
VARIABLE_VALUEAdam/learning_rate2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUE

B0
C1
#
0
1
2
3
4
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
4
	Dtotal
	Ecount
F	variables
G	keras_api
D
	Htotal
	Icount
J
_fn_kwargs
K	variables
L	keras_api
OM
VARIABLE_VALUEtotal4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUE
OM
VARIABLE_VALUEcount4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUE

D0
E1

F	variables
QO
VARIABLE_VALUEtotal_14keras_api/metrics/1/total/.ATTRIBUTES/VARIABLE_VALUE
QO
VARIABLE_VALUEcount_14keras_api/metrics/1/count/.ATTRIBUTES/VARIABLE_VALUE
 

H0
I1

K	variables
��
VARIABLE_VALUE$Adam/fmn10_umap_5000_1000_1/kernel/mRlayer_with_weights-0/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
��
VARIABLE_VALUE"Adam/fmn10_umap_5000_1000_1/bias/mPlayer_with_weights-0/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
��
VARIABLE_VALUE$Adam/fmn10_umap_5000_1000_2/kernel/mRlayer_with_weights-1/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
��
VARIABLE_VALUE"Adam/fmn10_umap_5000_1000_2/bias/mPlayer_with_weights-1/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
��
VARIABLE_VALUE$Adam/fmn10_umap_5000_1000_3/kernel/mRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
��
VARIABLE_VALUE"Adam/fmn10_umap_5000_1000_3/bias/mPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
��
VARIABLE_VALUE&Adam/fmn10_umap_5000_1000_out/kernel/mRlayer_with_weights-3/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
��
VARIABLE_VALUE$Adam/fmn10_umap_5000_1000_out/bias/mPlayer_with_weights-3/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
��
VARIABLE_VALUE$Adam/fmn10_umap_5000_1000_1/kernel/vRlayer_with_weights-0/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
��
VARIABLE_VALUE"Adam/fmn10_umap_5000_1000_1/bias/vPlayer_with_weights-0/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
��
VARIABLE_VALUE$Adam/fmn10_umap_5000_1000_2/kernel/vRlayer_with_weights-1/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
��
VARIABLE_VALUE"Adam/fmn10_umap_5000_1000_2/bias/vPlayer_with_weights-1/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
��
VARIABLE_VALUE$Adam/fmn10_umap_5000_1000_3/kernel/vRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
��
VARIABLE_VALUE"Adam/fmn10_umap_5000_1000_3/bias/vPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
��
VARIABLE_VALUE&Adam/fmn10_umap_5000_1000_out/kernel/vRlayer_with_weights-3/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
��
VARIABLE_VALUE$Adam/fmn10_umap_5000_1000_out/bias/vPlayer_with_weights-3/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
�
*serving_default_fmn10_umap_5000_1000_inputPlaceholder*(
_output_shapes
:����������*
dtype0*
shape:����������
�
StatefulPartitionedCallStatefulPartitionedCall*serving_default_fmn10_umap_5000_1000_inputfmn10_umap_5000_1000_1/kernelfmn10_umap_5000_1000_1/biasfmn10_umap_5000_1000_2/kernelfmn10_umap_5000_1000_2/biasfmn10_umap_5000_1000_3/kernelfmn10_umap_5000_1000_3/biasfmn10_umap_5000_1000_out/kernelfmn10_umap_5000_1000_out/bias*
Tin
2	*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������**
_read_only_resource_inputs

*0
config_proto 

CPU

GPU2*0J 8� *.
f)R'
%__inference_signature_wrapper_2365103
O
saver_filenamePlaceholder*
_output_shapes
: *
dtype0*
shape: 
�
StatefulPartitionedCall_1StatefulPartitionedCallsaver_filename1fmn10_umap_5000_1000_1/kernel/Read/ReadVariableOp/fmn10_umap_5000_1000_1/bias/Read/ReadVariableOp1fmn10_umap_5000_1000_2/kernel/Read/ReadVariableOp/fmn10_umap_5000_1000_2/bias/Read/ReadVariableOp1fmn10_umap_5000_1000_3/kernel/Read/ReadVariableOp/fmn10_umap_5000_1000_3/bias/Read/ReadVariableOp3fmn10_umap_5000_1000_out/kernel/Read/ReadVariableOp1fmn10_umap_5000_1000_out/bias/Read/ReadVariableOpAdam/iter/Read/ReadVariableOpAdam/beta_1/Read/ReadVariableOpAdam/beta_2/Read/ReadVariableOpAdam/decay/Read/ReadVariableOp&Adam/learning_rate/Read/ReadVariableOptotal/Read/ReadVariableOpcount/Read/ReadVariableOptotal_1/Read/ReadVariableOpcount_1/Read/ReadVariableOp8Adam/fmn10_umap_5000_1000_1/kernel/m/Read/ReadVariableOp6Adam/fmn10_umap_5000_1000_1/bias/m/Read/ReadVariableOp8Adam/fmn10_umap_5000_1000_2/kernel/m/Read/ReadVariableOp6Adam/fmn10_umap_5000_1000_2/bias/m/Read/ReadVariableOp8Adam/fmn10_umap_5000_1000_3/kernel/m/Read/ReadVariableOp6Adam/fmn10_umap_5000_1000_3/bias/m/Read/ReadVariableOp:Adam/fmn10_umap_5000_1000_out/kernel/m/Read/ReadVariableOp8Adam/fmn10_umap_5000_1000_out/bias/m/Read/ReadVariableOp8Adam/fmn10_umap_5000_1000_1/kernel/v/Read/ReadVariableOp6Adam/fmn10_umap_5000_1000_1/bias/v/Read/ReadVariableOp8Adam/fmn10_umap_5000_1000_2/kernel/v/Read/ReadVariableOp6Adam/fmn10_umap_5000_1000_2/bias/v/Read/ReadVariableOp8Adam/fmn10_umap_5000_1000_3/kernel/v/Read/ReadVariableOp6Adam/fmn10_umap_5000_1000_3/bias/v/Read/ReadVariableOp:Adam/fmn10_umap_5000_1000_out/kernel/v/Read/ReadVariableOp8Adam/fmn10_umap_5000_1000_out/bias/v/Read/ReadVariableOpConst*.
Tin'
%2#	*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *)
f$R"
 __inference__traced_save_2365411
�	
StatefulPartitionedCall_2StatefulPartitionedCallsaver_filenamefmn10_umap_5000_1000_1/kernelfmn10_umap_5000_1000_1/biasfmn10_umap_5000_1000_2/kernelfmn10_umap_5000_1000_2/biasfmn10_umap_5000_1000_3/kernelfmn10_umap_5000_1000_3/biasfmn10_umap_5000_1000_out/kernelfmn10_umap_5000_1000_out/bias	Adam/iterAdam/beta_1Adam/beta_2
Adam/decayAdam/learning_ratetotalcounttotal_1count_1$Adam/fmn10_umap_5000_1000_1/kernel/m"Adam/fmn10_umap_5000_1000_1/bias/m$Adam/fmn10_umap_5000_1000_2/kernel/m"Adam/fmn10_umap_5000_1000_2/bias/m$Adam/fmn10_umap_5000_1000_3/kernel/m"Adam/fmn10_umap_5000_1000_3/bias/m&Adam/fmn10_umap_5000_1000_out/kernel/m$Adam/fmn10_umap_5000_1000_out/bias/m$Adam/fmn10_umap_5000_1000_1/kernel/v"Adam/fmn10_umap_5000_1000_1/bias/v$Adam/fmn10_umap_5000_1000_2/kernel/v"Adam/fmn10_umap_5000_1000_2/bias/v$Adam/fmn10_umap_5000_1000_3/kernel/v"Adam/fmn10_umap_5000_1000_3/bias/v&Adam/fmn10_umap_5000_1000_out/kernel/v$Adam/fmn10_umap_5000_1000_out/bias/v*-
Tin&
$2"*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *,
f'R%
#__inference__traced_restore_2365520�
�G
�

"__inference__wrapped_model_2364804
fmn10_umap_5000_1000_input^
Jfmn10_umap_5000_1000_fmn10_umap_5000_1000_1_matmul_readvariableop_resource:
��Z
Kfmn10_umap_5000_1000_fmn10_umap_5000_1000_1_biasadd_readvariableop_resource:	�^
Jfmn10_umap_5000_1000_fmn10_umap_5000_1000_2_matmul_readvariableop_resource:
��Z
Kfmn10_umap_5000_1000_fmn10_umap_5000_1000_2_biasadd_readvariableop_resource:	�^
Jfmn10_umap_5000_1000_fmn10_umap_5000_1000_3_matmul_readvariableop_resource:
��Z
Kfmn10_umap_5000_1000_fmn10_umap_5000_1000_3_biasadd_readvariableop_resource:	�_
Lfmn10_umap_5000_1000_fmn10_umap_5000_1000_out_matmul_readvariableop_resource:	�[
Mfmn10_umap_5000_1000_fmn10_umap_5000_1000_out_biasadd_readvariableop_resource:
identity��Bfmn10_umap_5000_1000/fmn10_umap_5000_1000_1/BiasAdd/ReadVariableOp�Afmn10_umap_5000_1000/fmn10_umap_5000_1000_1/MatMul/ReadVariableOp�Bfmn10_umap_5000_1000/fmn10_umap_5000_1000_2/BiasAdd/ReadVariableOp�Afmn10_umap_5000_1000/fmn10_umap_5000_1000_2/MatMul/ReadVariableOp�Bfmn10_umap_5000_1000/fmn10_umap_5000_1000_3/BiasAdd/ReadVariableOp�Afmn10_umap_5000_1000/fmn10_umap_5000_1000_3/MatMul/ReadVariableOp�Dfmn10_umap_5000_1000/fmn10_umap_5000_1000_out/BiasAdd/ReadVariableOp�Cfmn10_umap_5000_1000/fmn10_umap_5000_1000_out/MatMul/ReadVariableOp�
Afmn10_umap_5000_1000/fmn10_umap_5000_1000_1/MatMul/ReadVariableOpReadVariableOpJfmn10_umap_5000_1000_fmn10_umap_5000_1000_1_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02C
Afmn10_umap_5000_1000/fmn10_umap_5000_1000_1/MatMul/ReadVariableOp�
2fmn10_umap_5000_1000/fmn10_umap_5000_1000_1/MatMulMatMulfmn10_umap_5000_1000_inputIfmn10_umap_5000_1000/fmn10_umap_5000_1000_1/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������24
2fmn10_umap_5000_1000/fmn10_umap_5000_1000_1/MatMul�
Bfmn10_umap_5000_1000/fmn10_umap_5000_1000_1/BiasAdd/ReadVariableOpReadVariableOpKfmn10_umap_5000_1000_fmn10_umap_5000_1000_1_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02D
Bfmn10_umap_5000_1000/fmn10_umap_5000_1000_1/BiasAdd/ReadVariableOp�
3fmn10_umap_5000_1000/fmn10_umap_5000_1000_1/BiasAddBiasAdd<fmn10_umap_5000_1000/fmn10_umap_5000_1000_1/MatMul:product:0Jfmn10_umap_5000_1000/fmn10_umap_5000_1000_1/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������25
3fmn10_umap_5000_1000/fmn10_umap_5000_1000_1/BiasAdd�
0fmn10_umap_5000_1000/fmn10_umap_5000_1000_1/ReluRelu<fmn10_umap_5000_1000/fmn10_umap_5000_1000_1/BiasAdd:output:0*
T0*(
_output_shapes
:����������22
0fmn10_umap_5000_1000/fmn10_umap_5000_1000_1/Relu�
Afmn10_umap_5000_1000/fmn10_umap_5000_1000_2/MatMul/ReadVariableOpReadVariableOpJfmn10_umap_5000_1000_fmn10_umap_5000_1000_2_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02C
Afmn10_umap_5000_1000/fmn10_umap_5000_1000_2/MatMul/ReadVariableOp�
2fmn10_umap_5000_1000/fmn10_umap_5000_1000_2/MatMulMatMul>fmn10_umap_5000_1000/fmn10_umap_5000_1000_1/Relu:activations:0Ifmn10_umap_5000_1000/fmn10_umap_5000_1000_2/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������24
2fmn10_umap_5000_1000/fmn10_umap_5000_1000_2/MatMul�
Bfmn10_umap_5000_1000/fmn10_umap_5000_1000_2/BiasAdd/ReadVariableOpReadVariableOpKfmn10_umap_5000_1000_fmn10_umap_5000_1000_2_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02D
Bfmn10_umap_5000_1000/fmn10_umap_5000_1000_2/BiasAdd/ReadVariableOp�
3fmn10_umap_5000_1000/fmn10_umap_5000_1000_2/BiasAddBiasAdd<fmn10_umap_5000_1000/fmn10_umap_5000_1000_2/MatMul:product:0Jfmn10_umap_5000_1000/fmn10_umap_5000_1000_2/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������25
3fmn10_umap_5000_1000/fmn10_umap_5000_1000_2/BiasAdd�
0fmn10_umap_5000_1000/fmn10_umap_5000_1000_2/ReluRelu<fmn10_umap_5000_1000/fmn10_umap_5000_1000_2/BiasAdd:output:0*
T0*(
_output_shapes
:����������22
0fmn10_umap_5000_1000/fmn10_umap_5000_1000_2/Relu�
Afmn10_umap_5000_1000/fmn10_umap_5000_1000_3/MatMul/ReadVariableOpReadVariableOpJfmn10_umap_5000_1000_fmn10_umap_5000_1000_3_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02C
Afmn10_umap_5000_1000/fmn10_umap_5000_1000_3/MatMul/ReadVariableOp�
2fmn10_umap_5000_1000/fmn10_umap_5000_1000_3/MatMulMatMul>fmn10_umap_5000_1000/fmn10_umap_5000_1000_2/Relu:activations:0Ifmn10_umap_5000_1000/fmn10_umap_5000_1000_3/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������24
2fmn10_umap_5000_1000/fmn10_umap_5000_1000_3/MatMul�
Bfmn10_umap_5000_1000/fmn10_umap_5000_1000_3/BiasAdd/ReadVariableOpReadVariableOpKfmn10_umap_5000_1000_fmn10_umap_5000_1000_3_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02D
Bfmn10_umap_5000_1000/fmn10_umap_5000_1000_3/BiasAdd/ReadVariableOp�
3fmn10_umap_5000_1000/fmn10_umap_5000_1000_3/BiasAddBiasAdd<fmn10_umap_5000_1000/fmn10_umap_5000_1000_3/MatMul:product:0Jfmn10_umap_5000_1000/fmn10_umap_5000_1000_3/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������25
3fmn10_umap_5000_1000/fmn10_umap_5000_1000_3/BiasAdd�
0fmn10_umap_5000_1000/fmn10_umap_5000_1000_3/ReluRelu<fmn10_umap_5000_1000/fmn10_umap_5000_1000_3/BiasAdd:output:0*
T0*(
_output_shapes
:����������22
0fmn10_umap_5000_1000/fmn10_umap_5000_1000_3/Relu�
Cfmn10_umap_5000_1000/fmn10_umap_5000_1000_out/MatMul/ReadVariableOpReadVariableOpLfmn10_umap_5000_1000_fmn10_umap_5000_1000_out_matmul_readvariableop_resource*
_output_shapes
:	�*
dtype02E
Cfmn10_umap_5000_1000/fmn10_umap_5000_1000_out/MatMul/ReadVariableOp�
4fmn10_umap_5000_1000/fmn10_umap_5000_1000_out/MatMulMatMul>fmn10_umap_5000_1000/fmn10_umap_5000_1000_3/Relu:activations:0Kfmn10_umap_5000_1000/fmn10_umap_5000_1000_out/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������26
4fmn10_umap_5000_1000/fmn10_umap_5000_1000_out/MatMul�
Dfmn10_umap_5000_1000/fmn10_umap_5000_1000_out/BiasAdd/ReadVariableOpReadVariableOpMfmn10_umap_5000_1000_fmn10_umap_5000_1000_out_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02F
Dfmn10_umap_5000_1000/fmn10_umap_5000_1000_out/BiasAdd/ReadVariableOp�
5fmn10_umap_5000_1000/fmn10_umap_5000_1000_out/BiasAddBiasAdd>fmn10_umap_5000_1000/fmn10_umap_5000_1000_out/MatMul:product:0Lfmn10_umap_5000_1000/fmn10_umap_5000_1000_out/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������27
5fmn10_umap_5000_1000/fmn10_umap_5000_1000_out/BiasAdd�
5fmn10_umap_5000_1000/fmn10_umap_5000_1000_out/SigmoidSigmoid>fmn10_umap_5000_1000/fmn10_umap_5000_1000_out/BiasAdd:output:0*
T0*'
_output_shapes
:���������27
5fmn10_umap_5000_1000/fmn10_umap_5000_1000_out/Sigmoid�
IdentityIdentity9fmn10_umap_5000_1000/fmn10_umap_5000_1000_out/Sigmoid:y:0^NoOp*
T0*'
_output_shapes
:���������2

Identity�
NoOpNoOpC^fmn10_umap_5000_1000/fmn10_umap_5000_1000_1/BiasAdd/ReadVariableOpB^fmn10_umap_5000_1000/fmn10_umap_5000_1000_1/MatMul/ReadVariableOpC^fmn10_umap_5000_1000/fmn10_umap_5000_1000_2/BiasAdd/ReadVariableOpB^fmn10_umap_5000_1000/fmn10_umap_5000_1000_2/MatMul/ReadVariableOpC^fmn10_umap_5000_1000/fmn10_umap_5000_1000_3/BiasAdd/ReadVariableOpB^fmn10_umap_5000_1000/fmn10_umap_5000_1000_3/MatMul/ReadVariableOpE^fmn10_umap_5000_1000/fmn10_umap_5000_1000_out/BiasAdd/ReadVariableOpD^fmn10_umap_5000_1000/fmn10_umap_5000_1000_out/MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*7
_input_shapes&
$:����������: : : : : : : : 2�
Bfmn10_umap_5000_1000/fmn10_umap_5000_1000_1/BiasAdd/ReadVariableOpBfmn10_umap_5000_1000/fmn10_umap_5000_1000_1/BiasAdd/ReadVariableOp2�
Afmn10_umap_5000_1000/fmn10_umap_5000_1000_1/MatMul/ReadVariableOpAfmn10_umap_5000_1000/fmn10_umap_5000_1000_1/MatMul/ReadVariableOp2�
Bfmn10_umap_5000_1000/fmn10_umap_5000_1000_2/BiasAdd/ReadVariableOpBfmn10_umap_5000_1000/fmn10_umap_5000_1000_2/BiasAdd/ReadVariableOp2�
Afmn10_umap_5000_1000/fmn10_umap_5000_1000_2/MatMul/ReadVariableOpAfmn10_umap_5000_1000/fmn10_umap_5000_1000_2/MatMul/ReadVariableOp2�
Bfmn10_umap_5000_1000/fmn10_umap_5000_1000_3/BiasAdd/ReadVariableOpBfmn10_umap_5000_1000/fmn10_umap_5000_1000_3/BiasAdd/ReadVariableOp2�
Afmn10_umap_5000_1000/fmn10_umap_5000_1000_3/MatMul/ReadVariableOpAfmn10_umap_5000_1000/fmn10_umap_5000_1000_3/MatMul/ReadVariableOp2�
Dfmn10_umap_5000_1000/fmn10_umap_5000_1000_out/BiasAdd/ReadVariableOpDfmn10_umap_5000_1000/fmn10_umap_5000_1000_out/BiasAdd/ReadVariableOp2�
Cfmn10_umap_5000_1000/fmn10_umap_5000_1000_out/MatMul/ReadVariableOpCfmn10_umap_5000_1000/fmn10_umap_5000_1000_out/MatMul/ReadVariableOp:d `
(
_output_shapes
:����������
4
_user_specified_namefmn10_umap_5000_1000_input
�
�
Q__inference_fmn10_umap_5000_1000_layer_call_and_return_conditional_losses_2364986

inputs2
fmn10_umap_5000_1000_1_2364965:
��-
fmn10_umap_5000_1000_1_2364967:	�2
fmn10_umap_5000_1000_2_2364970:
��-
fmn10_umap_5000_1000_2_2364972:	�2
fmn10_umap_5000_1000_3_2364975:
��-
fmn10_umap_5000_1000_3_2364977:	�3
 fmn10_umap_5000_1000_out_2364980:	�.
 fmn10_umap_5000_1000_out_2364982:
identity��.fmn10_umap_5000_1000_1/StatefulPartitionedCall�.fmn10_umap_5000_1000_2/StatefulPartitionedCall�.fmn10_umap_5000_1000_3/StatefulPartitionedCall�0fmn10_umap_5000_1000_out/StatefulPartitionedCall�
.fmn10_umap_5000_1000_1/StatefulPartitionedCallStatefulPartitionedCallinputsfmn10_umap_5000_1000_1_2364965fmn10_umap_5000_1000_1_2364967*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *\
fWRU
S__inference_fmn10_umap_5000_1000_1_layer_call_and_return_conditional_losses_236482220
.fmn10_umap_5000_1000_1/StatefulPartitionedCall�
.fmn10_umap_5000_1000_2/StatefulPartitionedCallStatefulPartitionedCall7fmn10_umap_5000_1000_1/StatefulPartitionedCall:output:0fmn10_umap_5000_1000_2_2364970fmn10_umap_5000_1000_2_2364972*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *\
fWRU
S__inference_fmn10_umap_5000_1000_2_layer_call_and_return_conditional_losses_236483920
.fmn10_umap_5000_1000_2/StatefulPartitionedCall�
.fmn10_umap_5000_1000_3/StatefulPartitionedCallStatefulPartitionedCall7fmn10_umap_5000_1000_2/StatefulPartitionedCall:output:0fmn10_umap_5000_1000_3_2364975fmn10_umap_5000_1000_3_2364977*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *\
fWRU
S__inference_fmn10_umap_5000_1000_3_layer_call_and_return_conditional_losses_236485620
.fmn10_umap_5000_1000_3/StatefulPartitionedCall�
0fmn10_umap_5000_1000_out/StatefulPartitionedCallStatefulPartitionedCall7fmn10_umap_5000_1000_3/StatefulPartitionedCall:output:0 fmn10_umap_5000_1000_out_2364980 fmn10_umap_5000_1000_out_2364982*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *^
fYRW
U__inference_fmn10_umap_5000_1000_out_layer_call_and_return_conditional_losses_236487322
0fmn10_umap_5000_1000_out/StatefulPartitionedCall�
IdentityIdentity9fmn10_umap_5000_1000_out/StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identity�
NoOpNoOp/^fmn10_umap_5000_1000_1/StatefulPartitionedCall/^fmn10_umap_5000_1000_2/StatefulPartitionedCall/^fmn10_umap_5000_1000_3/StatefulPartitionedCall1^fmn10_umap_5000_1000_out/StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*7
_input_shapes&
$:����������: : : : : : : : 2`
.fmn10_umap_5000_1000_1/StatefulPartitionedCall.fmn10_umap_5000_1000_1/StatefulPartitionedCall2`
.fmn10_umap_5000_1000_2/StatefulPartitionedCall.fmn10_umap_5000_1000_2/StatefulPartitionedCall2`
.fmn10_umap_5000_1000_3/StatefulPartitionedCall.fmn10_umap_5000_1000_3/StatefulPartitionedCall2d
0fmn10_umap_5000_1000_out/StatefulPartitionedCall0fmn10_umap_5000_1000_out/StatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
S__inference_fmn10_umap_5000_1000_3_layer_call_and_return_conditional_losses_2365269

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�

�
6__inference_fmn10_umap_5000_1000_layer_call_fn_2365124

inputs
unknown:
��
	unknown_0:	�
	unknown_1:
��
	unknown_2:	�
	unknown_3:
��
	unknown_4:	�
	unknown_5:	�
	unknown_6:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6*
Tin
2	*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������**
_read_only_resource_inputs

*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_fmn10_umap_5000_1000_layer_call_and_return_conditional_losses_23648802
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*7
_input_shapes&
$:����������: : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�5
�
Q__inference_fmn10_umap_5000_1000_layer_call_and_return_conditional_losses_2365209

inputsI
5fmn10_umap_5000_1000_1_matmul_readvariableop_resource:
��E
6fmn10_umap_5000_1000_1_biasadd_readvariableop_resource:	�I
5fmn10_umap_5000_1000_2_matmul_readvariableop_resource:
��E
6fmn10_umap_5000_1000_2_biasadd_readvariableop_resource:	�I
5fmn10_umap_5000_1000_3_matmul_readvariableop_resource:
��E
6fmn10_umap_5000_1000_3_biasadd_readvariableop_resource:	�J
7fmn10_umap_5000_1000_out_matmul_readvariableop_resource:	�F
8fmn10_umap_5000_1000_out_biasadd_readvariableop_resource:
identity��-fmn10_umap_5000_1000_1/BiasAdd/ReadVariableOp�,fmn10_umap_5000_1000_1/MatMul/ReadVariableOp�-fmn10_umap_5000_1000_2/BiasAdd/ReadVariableOp�,fmn10_umap_5000_1000_2/MatMul/ReadVariableOp�-fmn10_umap_5000_1000_3/BiasAdd/ReadVariableOp�,fmn10_umap_5000_1000_3/MatMul/ReadVariableOp�/fmn10_umap_5000_1000_out/BiasAdd/ReadVariableOp�.fmn10_umap_5000_1000_out/MatMul/ReadVariableOp�
,fmn10_umap_5000_1000_1/MatMul/ReadVariableOpReadVariableOp5fmn10_umap_5000_1000_1_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02.
,fmn10_umap_5000_1000_1/MatMul/ReadVariableOp�
fmn10_umap_5000_1000_1/MatMulMatMulinputs4fmn10_umap_5000_1000_1/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn10_umap_5000_1000_1/MatMul�
-fmn10_umap_5000_1000_1/BiasAdd/ReadVariableOpReadVariableOp6fmn10_umap_5000_1000_1_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02/
-fmn10_umap_5000_1000_1/BiasAdd/ReadVariableOp�
fmn10_umap_5000_1000_1/BiasAddBiasAdd'fmn10_umap_5000_1000_1/MatMul:product:05fmn10_umap_5000_1000_1/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2 
fmn10_umap_5000_1000_1/BiasAdd�
fmn10_umap_5000_1000_1/ReluRelu'fmn10_umap_5000_1000_1/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
fmn10_umap_5000_1000_1/Relu�
,fmn10_umap_5000_1000_2/MatMul/ReadVariableOpReadVariableOp5fmn10_umap_5000_1000_2_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02.
,fmn10_umap_5000_1000_2/MatMul/ReadVariableOp�
fmn10_umap_5000_1000_2/MatMulMatMul)fmn10_umap_5000_1000_1/Relu:activations:04fmn10_umap_5000_1000_2/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn10_umap_5000_1000_2/MatMul�
-fmn10_umap_5000_1000_2/BiasAdd/ReadVariableOpReadVariableOp6fmn10_umap_5000_1000_2_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02/
-fmn10_umap_5000_1000_2/BiasAdd/ReadVariableOp�
fmn10_umap_5000_1000_2/BiasAddBiasAdd'fmn10_umap_5000_1000_2/MatMul:product:05fmn10_umap_5000_1000_2/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2 
fmn10_umap_5000_1000_2/BiasAdd�
fmn10_umap_5000_1000_2/ReluRelu'fmn10_umap_5000_1000_2/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
fmn10_umap_5000_1000_2/Relu�
,fmn10_umap_5000_1000_3/MatMul/ReadVariableOpReadVariableOp5fmn10_umap_5000_1000_3_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02.
,fmn10_umap_5000_1000_3/MatMul/ReadVariableOp�
fmn10_umap_5000_1000_3/MatMulMatMul)fmn10_umap_5000_1000_2/Relu:activations:04fmn10_umap_5000_1000_3/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn10_umap_5000_1000_3/MatMul�
-fmn10_umap_5000_1000_3/BiasAdd/ReadVariableOpReadVariableOp6fmn10_umap_5000_1000_3_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02/
-fmn10_umap_5000_1000_3/BiasAdd/ReadVariableOp�
fmn10_umap_5000_1000_3/BiasAddBiasAdd'fmn10_umap_5000_1000_3/MatMul:product:05fmn10_umap_5000_1000_3/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2 
fmn10_umap_5000_1000_3/BiasAdd�
fmn10_umap_5000_1000_3/ReluRelu'fmn10_umap_5000_1000_3/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
fmn10_umap_5000_1000_3/Relu�
.fmn10_umap_5000_1000_out/MatMul/ReadVariableOpReadVariableOp7fmn10_umap_5000_1000_out_matmul_readvariableop_resource*
_output_shapes
:	�*
dtype020
.fmn10_umap_5000_1000_out/MatMul/ReadVariableOp�
fmn10_umap_5000_1000_out/MatMulMatMul)fmn10_umap_5000_1000_3/Relu:activations:06fmn10_umap_5000_1000_out/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2!
fmn10_umap_5000_1000_out/MatMul�
/fmn10_umap_5000_1000_out/BiasAdd/ReadVariableOpReadVariableOp8fmn10_umap_5000_1000_out_biasadd_readvariableop_resource*
_output_shapes
:*
dtype021
/fmn10_umap_5000_1000_out/BiasAdd/ReadVariableOp�
 fmn10_umap_5000_1000_out/BiasAddBiasAdd)fmn10_umap_5000_1000_out/MatMul:product:07fmn10_umap_5000_1000_out/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2"
 fmn10_umap_5000_1000_out/BiasAdd�
 fmn10_umap_5000_1000_out/SigmoidSigmoid)fmn10_umap_5000_1000_out/BiasAdd:output:0*
T0*'
_output_shapes
:���������2"
 fmn10_umap_5000_1000_out/Sigmoid
IdentityIdentity$fmn10_umap_5000_1000_out/Sigmoid:y:0^NoOp*
T0*'
_output_shapes
:���������2

Identity�
NoOpNoOp.^fmn10_umap_5000_1000_1/BiasAdd/ReadVariableOp-^fmn10_umap_5000_1000_1/MatMul/ReadVariableOp.^fmn10_umap_5000_1000_2/BiasAdd/ReadVariableOp-^fmn10_umap_5000_1000_2/MatMul/ReadVariableOp.^fmn10_umap_5000_1000_3/BiasAdd/ReadVariableOp-^fmn10_umap_5000_1000_3/MatMul/ReadVariableOp0^fmn10_umap_5000_1000_out/BiasAdd/ReadVariableOp/^fmn10_umap_5000_1000_out/MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*7
_input_shapes&
$:����������: : : : : : : : 2^
-fmn10_umap_5000_1000_1/BiasAdd/ReadVariableOp-fmn10_umap_5000_1000_1/BiasAdd/ReadVariableOp2\
,fmn10_umap_5000_1000_1/MatMul/ReadVariableOp,fmn10_umap_5000_1000_1/MatMul/ReadVariableOp2^
-fmn10_umap_5000_1000_2/BiasAdd/ReadVariableOp-fmn10_umap_5000_1000_2/BiasAdd/ReadVariableOp2\
,fmn10_umap_5000_1000_2/MatMul/ReadVariableOp,fmn10_umap_5000_1000_2/MatMul/ReadVariableOp2^
-fmn10_umap_5000_1000_3/BiasAdd/ReadVariableOp-fmn10_umap_5000_1000_3/BiasAdd/ReadVariableOp2\
,fmn10_umap_5000_1000_3/MatMul/ReadVariableOp,fmn10_umap_5000_1000_3/MatMul/ReadVariableOp2b
/fmn10_umap_5000_1000_out/BiasAdd/ReadVariableOp/fmn10_umap_5000_1000_out/BiasAdd/ReadVariableOp2`
.fmn10_umap_5000_1000_out/MatMul/ReadVariableOp.fmn10_umap_5000_1000_out/MatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
��
�
#__inference__traced_restore_2365520
file_prefixB
.assignvariableop_fmn10_umap_5000_1000_1_kernel:
��=
.assignvariableop_1_fmn10_umap_5000_1000_1_bias:	�D
0assignvariableop_2_fmn10_umap_5000_1000_2_kernel:
��=
.assignvariableop_3_fmn10_umap_5000_1000_2_bias:	�D
0assignvariableop_4_fmn10_umap_5000_1000_3_kernel:
��=
.assignvariableop_5_fmn10_umap_5000_1000_3_bias:	�E
2assignvariableop_6_fmn10_umap_5000_1000_out_kernel:	�>
0assignvariableop_7_fmn10_umap_5000_1000_out_bias:&
assignvariableop_8_adam_iter:	 (
assignvariableop_9_adam_beta_1: )
assignvariableop_10_adam_beta_2: (
assignvariableop_11_adam_decay: 0
&assignvariableop_12_adam_learning_rate: #
assignvariableop_13_total: #
assignvariableop_14_count: %
assignvariableop_15_total_1: %
assignvariableop_16_count_1: L
8assignvariableop_17_adam_fmn10_umap_5000_1000_1_kernel_m:
��E
6assignvariableop_18_adam_fmn10_umap_5000_1000_1_bias_m:	�L
8assignvariableop_19_adam_fmn10_umap_5000_1000_2_kernel_m:
��E
6assignvariableop_20_adam_fmn10_umap_5000_1000_2_bias_m:	�L
8assignvariableop_21_adam_fmn10_umap_5000_1000_3_kernel_m:
��E
6assignvariableop_22_adam_fmn10_umap_5000_1000_3_bias_m:	�M
:assignvariableop_23_adam_fmn10_umap_5000_1000_out_kernel_m:	�F
8assignvariableop_24_adam_fmn10_umap_5000_1000_out_bias_m:L
8assignvariableop_25_adam_fmn10_umap_5000_1000_1_kernel_v:
��E
6assignvariableop_26_adam_fmn10_umap_5000_1000_1_bias_v:	�L
8assignvariableop_27_adam_fmn10_umap_5000_1000_2_kernel_v:
��E
6assignvariableop_28_adam_fmn10_umap_5000_1000_2_bias_v:	�L
8assignvariableop_29_adam_fmn10_umap_5000_1000_3_kernel_v:
��E
6assignvariableop_30_adam_fmn10_umap_5000_1000_3_bias_v:	�M
:assignvariableop_31_adam_fmn10_umap_5000_1000_out_kernel_v:	�F
8assignvariableop_32_adam_fmn10_umap_5000_1000_out_bias_v:
identity_34��AssignVariableOp�AssignVariableOp_1�AssignVariableOp_10�AssignVariableOp_11�AssignVariableOp_12�AssignVariableOp_13�AssignVariableOp_14�AssignVariableOp_15�AssignVariableOp_16�AssignVariableOp_17�AssignVariableOp_18�AssignVariableOp_19�AssignVariableOp_2�AssignVariableOp_20�AssignVariableOp_21�AssignVariableOp_22�AssignVariableOp_23�AssignVariableOp_24�AssignVariableOp_25�AssignVariableOp_26�AssignVariableOp_27�AssignVariableOp_28�AssignVariableOp_29�AssignVariableOp_3�AssignVariableOp_30�AssignVariableOp_31�AssignVariableOp_32�AssignVariableOp_4�AssignVariableOp_5�AssignVariableOp_6�AssignVariableOp_7�AssignVariableOp_8�AssignVariableOp_9�
RestoreV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:"*
dtype0*�
value�B�"B6layer_with_weights-0/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-0/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-1/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-1/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-3/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-3/bias/.ATTRIBUTES/VARIABLE_VALUEB)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUEB*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/count/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-0/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-0/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-1/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-1/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-3/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-3/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-0/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-0/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-1/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-1/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-3/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-3/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEB_CHECKPOINTABLE_OBJECT_GRAPH2
RestoreV2/tensor_names�
RestoreV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:"*
dtype0*W
valueNBL"B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B 2
RestoreV2/shape_and_slices�
	RestoreV2	RestoreV2file_prefixRestoreV2/tensor_names:output:0#RestoreV2/shape_and_slices:output:0"/device:CPU:0*�
_output_shapes�
�::::::::::::::::::::::::::::::::::*0
dtypes&
$2"	2
	RestoreV2g
IdentityIdentityRestoreV2:tensors:0"/device:CPU:0*
T0*
_output_shapes
:2

Identity�
AssignVariableOpAssignVariableOp.assignvariableop_fmn10_umap_5000_1000_1_kernelIdentity:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOpk

Identity_1IdentityRestoreV2:tensors:1"/device:CPU:0*
T0*
_output_shapes
:2

Identity_1�
AssignVariableOp_1AssignVariableOp.assignvariableop_1_fmn10_umap_5000_1000_1_biasIdentity_1:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_1k

Identity_2IdentityRestoreV2:tensors:2"/device:CPU:0*
T0*
_output_shapes
:2

Identity_2�
AssignVariableOp_2AssignVariableOp0assignvariableop_2_fmn10_umap_5000_1000_2_kernelIdentity_2:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_2k

Identity_3IdentityRestoreV2:tensors:3"/device:CPU:0*
T0*
_output_shapes
:2

Identity_3�
AssignVariableOp_3AssignVariableOp.assignvariableop_3_fmn10_umap_5000_1000_2_biasIdentity_3:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_3k

Identity_4IdentityRestoreV2:tensors:4"/device:CPU:0*
T0*
_output_shapes
:2

Identity_4�
AssignVariableOp_4AssignVariableOp0assignvariableop_4_fmn10_umap_5000_1000_3_kernelIdentity_4:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_4k

Identity_5IdentityRestoreV2:tensors:5"/device:CPU:0*
T0*
_output_shapes
:2

Identity_5�
AssignVariableOp_5AssignVariableOp.assignvariableop_5_fmn10_umap_5000_1000_3_biasIdentity_5:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_5k

Identity_6IdentityRestoreV2:tensors:6"/device:CPU:0*
T0*
_output_shapes
:2

Identity_6�
AssignVariableOp_6AssignVariableOp2assignvariableop_6_fmn10_umap_5000_1000_out_kernelIdentity_6:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_6k

Identity_7IdentityRestoreV2:tensors:7"/device:CPU:0*
T0*
_output_shapes
:2

Identity_7�
AssignVariableOp_7AssignVariableOp0assignvariableop_7_fmn10_umap_5000_1000_out_biasIdentity_7:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_7k

Identity_8IdentityRestoreV2:tensors:8"/device:CPU:0*
T0	*
_output_shapes
:2

Identity_8�
AssignVariableOp_8AssignVariableOpassignvariableop_8_adam_iterIdentity_8:output:0"/device:CPU:0*
_output_shapes
 *
dtype0	2
AssignVariableOp_8k

Identity_9IdentityRestoreV2:tensors:9"/device:CPU:0*
T0*
_output_shapes
:2

Identity_9�
AssignVariableOp_9AssignVariableOpassignvariableop_9_adam_beta_1Identity_9:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_9n
Identity_10IdentityRestoreV2:tensors:10"/device:CPU:0*
T0*
_output_shapes
:2
Identity_10�
AssignVariableOp_10AssignVariableOpassignvariableop_10_adam_beta_2Identity_10:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_10n
Identity_11IdentityRestoreV2:tensors:11"/device:CPU:0*
T0*
_output_shapes
:2
Identity_11�
AssignVariableOp_11AssignVariableOpassignvariableop_11_adam_decayIdentity_11:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_11n
Identity_12IdentityRestoreV2:tensors:12"/device:CPU:0*
T0*
_output_shapes
:2
Identity_12�
AssignVariableOp_12AssignVariableOp&assignvariableop_12_adam_learning_rateIdentity_12:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_12n
Identity_13IdentityRestoreV2:tensors:13"/device:CPU:0*
T0*
_output_shapes
:2
Identity_13�
AssignVariableOp_13AssignVariableOpassignvariableop_13_totalIdentity_13:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_13n
Identity_14IdentityRestoreV2:tensors:14"/device:CPU:0*
T0*
_output_shapes
:2
Identity_14�
AssignVariableOp_14AssignVariableOpassignvariableop_14_countIdentity_14:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_14n
Identity_15IdentityRestoreV2:tensors:15"/device:CPU:0*
T0*
_output_shapes
:2
Identity_15�
AssignVariableOp_15AssignVariableOpassignvariableop_15_total_1Identity_15:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_15n
Identity_16IdentityRestoreV2:tensors:16"/device:CPU:0*
T0*
_output_shapes
:2
Identity_16�
AssignVariableOp_16AssignVariableOpassignvariableop_16_count_1Identity_16:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_16n
Identity_17IdentityRestoreV2:tensors:17"/device:CPU:0*
T0*
_output_shapes
:2
Identity_17�
AssignVariableOp_17AssignVariableOp8assignvariableop_17_adam_fmn10_umap_5000_1000_1_kernel_mIdentity_17:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_17n
Identity_18IdentityRestoreV2:tensors:18"/device:CPU:0*
T0*
_output_shapes
:2
Identity_18�
AssignVariableOp_18AssignVariableOp6assignvariableop_18_adam_fmn10_umap_5000_1000_1_bias_mIdentity_18:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_18n
Identity_19IdentityRestoreV2:tensors:19"/device:CPU:0*
T0*
_output_shapes
:2
Identity_19�
AssignVariableOp_19AssignVariableOp8assignvariableop_19_adam_fmn10_umap_5000_1000_2_kernel_mIdentity_19:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_19n
Identity_20IdentityRestoreV2:tensors:20"/device:CPU:0*
T0*
_output_shapes
:2
Identity_20�
AssignVariableOp_20AssignVariableOp6assignvariableop_20_adam_fmn10_umap_5000_1000_2_bias_mIdentity_20:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_20n
Identity_21IdentityRestoreV2:tensors:21"/device:CPU:0*
T0*
_output_shapes
:2
Identity_21�
AssignVariableOp_21AssignVariableOp8assignvariableop_21_adam_fmn10_umap_5000_1000_3_kernel_mIdentity_21:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_21n
Identity_22IdentityRestoreV2:tensors:22"/device:CPU:0*
T0*
_output_shapes
:2
Identity_22�
AssignVariableOp_22AssignVariableOp6assignvariableop_22_adam_fmn10_umap_5000_1000_3_bias_mIdentity_22:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_22n
Identity_23IdentityRestoreV2:tensors:23"/device:CPU:0*
T0*
_output_shapes
:2
Identity_23�
AssignVariableOp_23AssignVariableOp:assignvariableop_23_adam_fmn10_umap_5000_1000_out_kernel_mIdentity_23:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_23n
Identity_24IdentityRestoreV2:tensors:24"/device:CPU:0*
T0*
_output_shapes
:2
Identity_24�
AssignVariableOp_24AssignVariableOp8assignvariableop_24_adam_fmn10_umap_5000_1000_out_bias_mIdentity_24:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_24n
Identity_25IdentityRestoreV2:tensors:25"/device:CPU:0*
T0*
_output_shapes
:2
Identity_25�
AssignVariableOp_25AssignVariableOp8assignvariableop_25_adam_fmn10_umap_5000_1000_1_kernel_vIdentity_25:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_25n
Identity_26IdentityRestoreV2:tensors:26"/device:CPU:0*
T0*
_output_shapes
:2
Identity_26�
AssignVariableOp_26AssignVariableOp6assignvariableop_26_adam_fmn10_umap_5000_1000_1_bias_vIdentity_26:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_26n
Identity_27IdentityRestoreV2:tensors:27"/device:CPU:0*
T0*
_output_shapes
:2
Identity_27�
AssignVariableOp_27AssignVariableOp8assignvariableop_27_adam_fmn10_umap_5000_1000_2_kernel_vIdentity_27:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_27n
Identity_28IdentityRestoreV2:tensors:28"/device:CPU:0*
T0*
_output_shapes
:2
Identity_28�
AssignVariableOp_28AssignVariableOp6assignvariableop_28_adam_fmn10_umap_5000_1000_2_bias_vIdentity_28:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_28n
Identity_29IdentityRestoreV2:tensors:29"/device:CPU:0*
T0*
_output_shapes
:2
Identity_29�
AssignVariableOp_29AssignVariableOp8assignvariableop_29_adam_fmn10_umap_5000_1000_3_kernel_vIdentity_29:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_29n
Identity_30IdentityRestoreV2:tensors:30"/device:CPU:0*
T0*
_output_shapes
:2
Identity_30�
AssignVariableOp_30AssignVariableOp6assignvariableop_30_adam_fmn10_umap_5000_1000_3_bias_vIdentity_30:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_30n
Identity_31IdentityRestoreV2:tensors:31"/device:CPU:0*
T0*
_output_shapes
:2
Identity_31�
AssignVariableOp_31AssignVariableOp:assignvariableop_31_adam_fmn10_umap_5000_1000_out_kernel_vIdentity_31:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_31n
Identity_32IdentityRestoreV2:tensors:32"/device:CPU:0*
T0*
_output_shapes
:2
Identity_32�
AssignVariableOp_32AssignVariableOp8assignvariableop_32_adam_fmn10_umap_5000_1000_out_bias_vIdentity_32:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_329
NoOpNoOp"/device:CPU:0*
_output_shapes
 2
NoOp�
Identity_33Identityfile_prefix^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_12^AssignVariableOp_13^AssignVariableOp_14^AssignVariableOp_15^AssignVariableOp_16^AssignVariableOp_17^AssignVariableOp_18^AssignVariableOp_19^AssignVariableOp_2^AssignVariableOp_20^AssignVariableOp_21^AssignVariableOp_22^AssignVariableOp_23^AssignVariableOp_24^AssignVariableOp_25^AssignVariableOp_26^AssignVariableOp_27^AssignVariableOp_28^AssignVariableOp_29^AssignVariableOp_3^AssignVariableOp_30^AssignVariableOp_31^AssignVariableOp_32^AssignVariableOp_4^AssignVariableOp_5^AssignVariableOp_6^AssignVariableOp_7^AssignVariableOp_8^AssignVariableOp_9^NoOp"/device:CPU:0*
T0*
_output_shapes
: 2
Identity_33f
Identity_34IdentityIdentity_33:output:0^NoOp_1*
T0*
_output_shapes
: 2
Identity_34�
NoOp_1NoOp^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_12^AssignVariableOp_13^AssignVariableOp_14^AssignVariableOp_15^AssignVariableOp_16^AssignVariableOp_17^AssignVariableOp_18^AssignVariableOp_19^AssignVariableOp_2^AssignVariableOp_20^AssignVariableOp_21^AssignVariableOp_22^AssignVariableOp_23^AssignVariableOp_24^AssignVariableOp_25^AssignVariableOp_26^AssignVariableOp_27^AssignVariableOp_28^AssignVariableOp_29^AssignVariableOp_3^AssignVariableOp_30^AssignVariableOp_31^AssignVariableOp_32^AssignVariableOp_4^AssignVariableOp_5^AssignVariableOp_6^AssignVariableOp_7^AssignVariableOp_8^AssignVariableOp_9*"
_acd_function_control_output(*
_output_shapes
 2
NoOp_1"#
identity_34Identity_34:output:0*W
_input_shapesF
D: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 2$
AssignVariableOpAssignVariableOp2(
AssignVariableOp_1AssignVariableOp_12*
AssignVariableOp_10AssignVariableOp_102*
AssignVariableOp_11AssignVariableOp_112*
AssignVariableOp_12AssignVariableOp_122*
AssignVariableOp_13AssignVariableOp_132*
AssignVariableOp_14AssignVariableOp_142*
AssignVariableOp_15AssignVariableOp_152*
AssignVariableOp_16AssignVariableOp_162*
AssignVariableOp_17AssignVariableOp_172*
AssignVariableOp_18AssignVariableOp_182*
AssignVariableOp_19AssignVariableOp_192(
AssignVariableOp_2AssignVariableOp_22*
AssignVariableOp_20AssignVariableOp_202*
AssignVariableOp_21AssignVariableOp_212*
AssignVariableOp_22AssignVariableOp_222*
AssignVariableOp_23AssignVariableOp_232*
AssignVariableOp_24AssignVariableOp_242*
AssignVariableOp_25AssignVariableOp_252*
AssignVariableOp_26AssignVariableOp_262*
AssignVariableOp_27AssignVariableOp_272*
AssignVariableOp_28AssignVariableOp_282*
AssignVariableOp_29AssignVariableOp_292(
AssignVariableOp_3AssignVariableOp_32*
AssignVariableOp_30AssignVariableOp_302*
AssignVariableOp_31AssignVariableOp_312*
AssignVariableOp_32AssignVariableOp_322(
AssignVariableOp_4AssignVariableOp_42(
AssignVariableOp_5AssignVariableOp_52(
AssignVariableOp_6AssignVariableOp_62(
AssignVariableOp_7AssignVariableOp_72(
AssignVariableOp_8AssignVariableOp_82(
AssignVariableOp_9AssignVariableOp_9:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix
�
�
S__inference_fmn10_umap_5000_1000_1_layer_call_and_return_conditional_losses_2365229

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�

�
%__inference_signature_wrapper_2365103
fmn10_umap_5000_1000_input
unknown:
��
	unknown_0:	�
	unknown_1:
��
	unknown_2:	�
	unknown_3:
��
	unknown_4:	�
	unknown_5:	�
	unknown_6:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallfmn10_umap_5000_1000_inputunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6*
Tin
2	*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������**
_read_only_resource_inputs

*0
config_proto 

CPU

GPU2*0J 8� *+
f&R$
"__inference__wrapped_model_23648042
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*7
_input_shapes&
$:����������: : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:d `
(
_output_shapes
:����������
4
_user_specified_namefmn10_umap_5000_1000_input
�
�
Q__inference_fmn10_umap_5000_1000_layer_call_and_return_conditional_losses_2365050
fmn10_umap_5000_1000_input2
fmn10_umap_5000_1000_1_2365029:
��-
fmn10_umap_5000_1000_1_2365031:	�2
fmn10_umap_5000_1000_2_2365034:
��-
fmn10_umap_5000_1000_2_2365036:	�2
fmn10_umap_5000_1000_3_2365039:
��-
fmn10_umap_5000_1000_3_2365041:	�3
 fmn10_umap_5000_1000_out_2365044:	�.
 fmn10_umap_5000_1000_out_2365046:
identity��.fmn10_umap_5000_1000_1/StatefulPartitionedCall�.fmn10_umap_5000_1000_2/StatefulPartitionedCall�.fmn10_umap_5000_1000_3/StatefulPartitionedCall�0fmn10_umap_5000_1000_out/StatefulPartitionedCall�
.fmn10_umap_5000_1000_1/StatefulPartitionedCallStatefulPartitionedCallfmn10_umap_5000_1000_inputfmn10_umap_5000_1000_1_2365029fmn10_umap_5000_1000_1_2365031*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *\
fWRU
S__inference_fmn10_umap_5000_1000_1_layer_call_and_return_conditional_losses_236482220
.fmn10_umap_5000_1000_1/StatefulPartitionedCall�
.fmn10_umap_5000_1000_2/StatefulPartitionedCallStatefulPartitionedCall7fmn10_umap_5000_1000_1/StatefulPartitionedCall:output:0fmn10_umap_5000_1000_2_2365034fmn10_umap_5000_1000_2_2365036*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *\
fWRU
S__inference_fmn10_umap_5000_1000_2_layer_call_and_return_conditional_losses_236483920
.fmn10_umap_5000_1000_2/StatefulPartitionedCall�
.fmn10_umap_5000_1000_3/StatefulPartitionedCallStatefulPartitionedCall7fmn10_umap_5000_1000_2/StatefulPartitionedCall:output:0fmn10_umap_5000_1000_3_2365039fmn10_umap_5000_1000_3_2365041*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *\
fWRU
S__inference_fmn10_umap_5000_1000_3_layer_call_and_return_conditional_losses_236485620
.fmn10_umap_5000_1000_3/StatefulPartitionedCall�
0fmn10_umap_5000_1000_out/StatefulPartitionedCallStatefulPartitionedCall7fmn10_umap_5000_1000_3/StatefulPartitionedCall:output:0 fmn10_umap_5000_1000_out_2365044 fmn10_umap_5000_1000_out_2365046*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *^
fYRW
U__inference_fmn10_umap_5000_1000_out_layer_call_and_return_conditional_losses_236487322
0fmn10_umap_5000_1000_out/StatefulPartitionedCall�
IdentityIdentity9fmn10_umap_5000_1000_out/StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identity�
NoOpNoOp/^fmn10_umap_5000_1000_1/StatefulPartitionedCall/^fmn10_umap_5000_1000_2/StatefulPartitionedCall/^fmn10_umap_5000_1000_3/StatefulPartitionedCall1^fmn10_umap_5000_1000_out/StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*7
_input_shapes&
$:����������: : : : : : : : 2`
.fmn10_umap_5000_1000_1/StatefulPartitionedCall.fmn10_umap_5000_1000_1/StatefulPartitionedCall2`
.fmn10_umap_5000_1000_2/StatefulPartitionedCall.fmn10_umap_5000_1000_2/StatefulPartitionedCall2`
.fmn10_umap_5000_1000_3/StatefulPartitionedCall.fmn10_umap_5000_1000_3/StatefulPartitionedCall2d
0fmn10_umap_5000_1000_out/StatefulPartitionedCall0fmn10_umap_5000_1000_out/StatefulPartitionedCall:d `
(
_output_shapes
:����������
4
_user_specified_namefmn10_umap_5000_1000_input
�
�
8__inference_fmn10_umap_5000_1000_2_layer_call_fn_2365238

inputs
unknown:
��
	unknown_0:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *\
fWRU
S__inference_fmn10_umap_5000_1000_2_layer_call_and_return_conditional_losses_23648392
StatefulPartitionedCall|
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:����������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�

�
6__inference_fmn10_umap_5000_1000_layer_call_fn_2365145

inputs
unknown:
��
	unknown_0:	�
	unknown_1:
��
	unknown_2:	�
	unknown_3:
��
	unknown_4:	�
	unknown_5:	�
	unknown_6:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6*
Tin
2	*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������**
_read_only_resource_inputs

*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_fmn10_umap_5000_1000_layer_call_and_return_conditional_losses_23649862
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*7
_input_shapes&
$:����������: : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
S__inference_fmn10_umap_5000_1000_2_layer_call_and_return_conditional_losses_2364839

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
Q__inference_fmn10_umap_5000_1000_layer_call_and_return_conditional_losses_2365074
fmn10_umap_5000_1000_input2
fmn10_umap_5000_1000_1_2365053:
��-
fmn10_umap_5000_1000_1_2365055:	�2
fmn10_umap_5000_1000_2_2365058:
��-
fmn10_umap_5000_1000_2_2365060:	�2
fmn10_umap_5000_1000_3_2365063:
��-
fmn10_umap_5000_1000_3_2365065:	�3
 fmn10_umap_5000_1000_out_2365068:	�.
 fmn10_umap_5000_1000_out_2365070:
identity��.fmn10_umap_5000_1000_1/StatefulPartitionedCall�.fmn10_umap_5000_1000_2/StatefulPartitionedCall�.fmn10_umap_5000_1000_3/StatefulPartitionedCall�0fmn10_umap_5000_1000_out/StatefulPartitionedCall�
.fmn10_umap_5000_1000_1/StatefulPartitionedCallStatefulPartitionedCallfmn10_umap_5000_1000_inputfmn10_umap_5000_1000_1_2365053fmn10_umap_5000_1000_1_2365055*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *\
fWRU
S__inference_fmn10_umap_5000_1000_1_layer_call_and_return_conditional_losses_236482220
.fmn10_umap_5000_1000_1/StatefulPartitionedCall�
.fmn10_umap_5000_1000_2/StatefulPartitionedCallStatefulPartitionedCall7fmn10_umap_5000_1000_1/StatefulPartitionedCall:output:0fmn10_umap_5000_1000_2_2365058fmn10_umap_5000_1000_2_2365060*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *\
fWRU
S__inference_fmn10_umap_5000_1000_2_layer_call_and_return_conditional_losses_236483920
.fmn10_umap_5000_1000_2/StatefulPartitionedCall�
.fmn10_umap_5000_1000_3/StatefulPartitionedCallStatefulPartitionedCall7fmn10_umap_5000_1000_2/StatefulPartitionedCall:output:0fmn10_umap_5000_1000_3_2365063fmn10_umap_5000_1000_3_2365065*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *\
fWRU
S__inference_fmn10_umap_5000_1000_3_layer_call_and_return_conditional_losses_236485620
.fmn10_umap_5000_1000_3/StatefulPartitionedCall�
0fmn10_umap_5000_1000_out/StatefulPartitionedCallStatefulPartitionedCall7fmn10_umap_5000_1000_3/StatefulPartitionedCall:output:0 fmn10_umap_5000_1000_out_2365068 fmn10_umap_5000_1000_out_2365070*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *^
fYRW
U__inference_fmn10_umap_5000_1000_out_layer_call_and_return_conditional_losses_236487322
0fmn10_umap_5000_1000_out/StatefulPartitionedCall�
IdentityIdentity9fmn10_umap_5000_1000_out/StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identity�
NoOpNoOp/^fmn10_umap_5000_1000_1/StatefulPartitionedCall/^fmn10_umap_5000_1000_2/StatefulPartitionedCall/^fmn10_umap_5000_1000_3/StatefulPartitionedCall1^fmn10_umap_5000_1000_out/StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*7
_input_shapes&
$:����������: : : : : : : : 2`
.fmn10_umap_5000_1000_1/StatefulPartitionedCall.fmn10_umap_5000_1000_1/StatefulPartitionedCall2`
.fmn10_umap_5000_1000_2/StatefulPartitionedCall.fmn10_umap_5000_1000_2/StatefulPartitionedCall2`
.fmn10_umap_5000_1000_3/StatefulPartitionedCall.fmn10_umap_5000_1000_3/StatefulPartitionedCall2d
0fmn10_umap_5000_1000_out/StatefulPartitionedCall0fmn10_umap_5000_1000_out/StatefulPartitionedCall:d `
(
_output_shapes
:����������
4
_user_specified_namefmn10_umap_5000_1000_input
�N
�
 __inference__traced_save_2365411
file_prefix<
8savev2_fmn10_umap_5000_1000_1_kernel_read_readvariableop:
6savev2_fmn10_umap_5000_1000_1_bias_read_readvariableop<
8savev2_fmn10_umap_5000_1000_2_kernel_read_readvariableop:
6savev2_fmn10_umap_5000_1000_2_bias_read_readvariableop<
8savev2_fmn10_umap_5000_1000_3_kernel_read_readvariableop:
6savev2_fmn10_umap_5000_1000_3_bias_read_readvariableop>
:savev2_fmn10_umap_5000_1000_out_kernel_read_readvariableop<
8savev2_fmn10_umap_5000_1000_out_bias_read_readvariableop(
$savev2_adam_iter_read_readvariableop	*
&savev2_adam_beta_1_read_readvariableop*
&savev2_adam_beta_2_read_readvariableop)
%savev2_adam_decay_read_readvariableop1
-savev2_adam_learning_rate_read_readvariableop$
 savev2_total_read_readvariableop$
 savev2_count_read_readvariableop&
"savev2_total_1_read_readvariableop&
"savev2_count_1_read_readvariableopC
?savev2_adam_fmn10_umap_5000_1000_1_kernel_m_read_readvariableopA
=savev2_adam_fmn10_umap_5000_1000_1_bias_m_read_readvariableopC
?savev2_adam_fmn10_umap_5000_1000_2_kernel_m_read_readvariableopA
=savev2_adam_fmn10_umap_5000_1000_2_bias_m_read_readvariableopC
?savev2_adam_fmn10_umap_5000_1000_3_kernel_m_read_readvariableopA
=savev2_adam_fmn10_umap_5000_1000_3_bias_m_read_readvariableopE
Asavev2_adam_fmn10_umap_5000_1000_out_kernel_m_read_readvariableopC
?savev2_adam_fmn10_umap_5000_1000_out_bias_m_read_readvariableopC
?savev2_adam_fmn10_umap_5000_1000_1_kernel_v_read_readvariableopA
=savev2_adam_fmn10_umap_5000_1000_1_bias_v_read_readvariableopC
?savev2_adam_fmn10_umap_5000_1000_2_kernel_v_read_readvariableopA
=savev2_adam_fmn10_umap_5000_1000_2_bias_v_read_readvariableopC
?savev2_adam_fmn10_umap_5000_1000_3_kernel_v_read_readvariableopA
=savev2_adam_fmn10_umap_5000_1000_3_bias_v_read_readvariableopE
Asavev2_adam_fmn10_umap_5000_1000_out_kernel_v_read_readvariableopC
?savev2_adam_fmn10_umap_5000_1000_out_bias_v_read_readvariableop
savev2_const

identity_1��MergeV2Checkpoints�
StaticRegexFullMatchStaticRegexFullMatchfile_prefix"/device:CPU:**
_output_shapes
: *
pattern
^s3://.*2
StaticRegexFullMatchc
ConstConst"/device:CPU:**
_output_shapes
: *
dtype0*
valueB B.part2
Constl
Const_1Const"/device:CPU:**
_output_shapes
: *
dtype0*
valueB B
_temp/part2	
Const_1�
SelectSelectStaticRegexFullMatch:output:0Const:output:0Const_1:output:0"/device:CPU:**
T0*
_output_shapes
: 2
Selectt

StringJoin
StringJoinfile_prefixSelect:output:0"/device:CPU:**
N*
_output_shapes
: 2

StringJoinZ

num_shardsConst*
_output_shapes
: *
dtype0*
value	B :2

num_shards
ShardedFilename/shardConst"/device:CPU:0*
_output_shapes
: *
dtype0*
value	B : 2
ShardedFilename/shard�
ShardedFilenameShardedFilenameStringJoin:output:0ShardedFilename/shard:output:0num_shards:output:0"/device:CPU:0*
_output_shapes
: 2
ShardedFilename�
SaveV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:"*
dtype0*�
value�B�"B6layer_with_weights-0/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-0/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-1/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-1/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-3/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-3/bias/.ATTRIBUTES/VARIABLE_VALUEB)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUEB*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/count/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-0/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-0/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-1/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-1/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-3/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-3/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-0/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-0/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-1/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-1/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-3/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-3/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEB_CHECKPOINTABLE_OBJECT_GRAPH2
SaveV2/tensor_names�
SaveV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:"*
dtype0*W
valueNBL"B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B 2
SaveV2/shape_and_slices�
SaveV2SaveV2ShardedFilename:filename:0SaveV2/tensor_names:output:0 SaveV2/shape_and_slices:output:08savev2_fmn10_umap_5000_1000_1_kernel_read_readvariableop6savev2_fmn10_umap_5000_1000_1_bias_read_readvariableop8savev2_fmn10_umap_5000_1000_2_kernel_read_readvariableop6savev2_fmn10_umap_5000_1000_2_bias_read_readvariableop8savev2_fmn10_umap_5000_1000_3_kernel_read_readvariableop6savev2_fmn10_umap_5000_1000_3_bias_read_readvariableop:savev2_fmn10_umap_5000_1000_out_kernel_read_readvariableop8savev2_fmn10_umap_5000_1000_out_bias_read_readvariableop$savev2_adam_iter_read_readvariableop&savev2_adam_beta_1_read_readvariableop&savev2_adam_beta_2_read_readvariableop%savev2_adam_decay_read_readvariableop-savev2_adam_learning_rate_read_readvariableop savev2_total_read_readvariableop savev2_count_read_readvariableop"savev2_total_1_read_readvariableop"savev2_count_1_read_readvariableop?savev2_adam_fmn10_umap_5000_1000_1_kernel_m_read_readvariableop=savev2_adam_fmn10_umap_5000_1000_1_bias_m_read_readvariableop?savev2_adam_fmn10_umap_5000_1000_2_kernel_m_read_readvariableop=savev2_adam_fmn10_umap_5000_1000_2_bias_m_read_readvariableop?savev2_adam_fmn10_umap_5000_1000_3_kernel_m_read_readvariableop=savev2_adam_fmn10_umap_5000_1000_3_bias_m_read_readvariableopAsavev2_adam_fmn10_umap_5000_1000_out_kernel_m_read_readvariableop?savev2_adam_fmn10_umap_5000_1000_out_bias_m_read_readvariableop?savev2_adam_fmn10_umap_5000_1000_1_kernel_v_read_readvariableop=savev2_adam_fmn10_umap_5000_1000_1_bias_v_read_readvariableop?savev2_adam_fmn10_umap_5000_1000_2_kernel_v_read_readvariableop=savev2_adam_fmn10_umap_5000_1000_2_bias_v_read_readvariableop?savev2_adam_fmn10_umap_5000_1000_3_kernel_v_read_readvariableop=savev2_adam_fmn10_umap_5000_1000_3_bias_v_read_readvariableopAsavev2_adam_fmn10_umap_5000_1000_out_kernel_v_read_readvariableop?savev2_adam_fmn10_umap_5000_1000_out_bias_v_read_readvariableopsavev2_const"/device:CPU:0*
_output_shapes
 *0
dtypes&
$2"	2
SaveV2�
&MergeV2Checkpoints/checkpoint_prefixesPackShardedFilename:filename:0^SaveV2"/device:CPU:0*
N*
T0*
_output_shapes
:2(
&MergeV2Checkpoints/checkpoint_prefixes�
MergeV2CheckpointsMergeV2Checkpoints/MergeV2Checkpoints/checkpoint_prefixes:output:0file_prefix"/device:CPU:0*
_output_shapes
 2
MergeV2Checkpointsr
IdentityIdentityfile_prefix^MergeV2Checkpoints"/device:CPU:0*
T0*
_output_shapes
: 2

Identity_

Identity_1IdentityIdentity:output:0^NoOp*
T0*
_output_shapes
: 2

Identity_1c
NoOpNoOp^MergeV2Checkpoints*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"!

identity_1Identity_1:output:0*�
_input_shapes�
�: :
��:�:
��:�:
��:�:	�:: : : : : : : : : :
��:�:
��:�:
��:�:	�::
��:�:
��:�:
��:�:	�:: 2(
MergeV2CheckpointsMergeV2Checkpoints:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix:&"
 
_output_shapes
:
��:!

_output_shapes	
:�:&"
 
_output_shapes
:
��:!

_output_shapes	
:�:&"
 
_output_shapes
:
��:!

_output_shapes	
:�:%!

_output_shapes
:	�: 

_output_shapes
::	

_output_shapes
: :


_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :&"
 
_output_shapes
:
��:!

_output_shapes	
:�:&"
 
_output_shapes
:
��:!

_output_shapes	
:�:&"
 
_output_shapes
:
��:!

_output_shapes	
:�:%!

_output_shapes
:	�: 

_output_shapes
::&"
 
_output_shapes
:
��:!

_output_shapes	
:�:&"
 
_output_shapes
:
��:!

_output_shapes	
:�:&"
 
_output_shapes
:
��:!

_output_shapes	
:�:% !

_output_shapes
:	�: !

_output_shapes
::"

_output_shapes
: 
�

�
6__inference_fmn10_umap_5000_1000_layer_call_fn_2365026
fmn10_umap_5000_1000_input
unknown:
��
	unknown_0:	�
	unknown_1:
��
	unknown_2:	�
	unknown_3:
��
	unknown_4:	�
	unknown_5:	�
	unknown_6:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallfmn10_umap_5000_1000_inputunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6*
Tin
2	*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������**
_read_only_resource_inputs

*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_fmn10_umap_5000_1000_layer_call_and_return_conditional_losses_23649862
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*7
_input_shapes&
$:����������: : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:d `
(
_output_shapes
:����������
4
_user_specified_namefmn10_umap_5000_1000_input
�
�
Q__inference_fmn10_umap_5000_1000_layer_call_and_return_conditional_losses_2364880

inputs2
fmn10_umap_5000_1000_1_2364823:
��-
fmn10_umap_5000_1000_1_2364825:	�2
fmn10_umap_5000_1000_2_2364840:
��-
fmn10_umap_5000_1000_2_2364842:	�2
fmn10_umap_5000_1000_3_2364857:
��-
fmn10_umap_5000_1000_3_2364859:	�3
 fmn10_umap_5000_1000_out_2364874:	�.
 fmn10_umap_5000_1000_out_2364876:
identity��.fmn10_umap_5000_1000_1/StatefulPartitionedCall�.fmn10_umap_5000_1000_2/StatefulPartitionedCall�.fmn10_umap_5000_1000_3/StatefulPartitionedCall�0fmn10_umap_5000_1000_out/StatefulPartitionedCall�
.fmn10_umap_5000_1000_1/StatefulPartitionedCallStatefulPartitionedCallinputsfmn10_umap_5000_1000_1_2364823fmn10_umap_5000_1000_1_2364825*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *\
fWRU
S__inference_fmn10_umap_5000_1000_1_layer_call_and_return_conditional_losses_236482220
.fmn10_umap_5000_1000_1/StatefulPartitionedCall�
.fmn10_umap_5000_1000_2/StatefulPartitionedCallStatefulPartitionedCall7fmn10_umap_5000_1000_1/StatefulPartitionedCall:output:0fmn10_umap_5000_1000_2_2364840fmn10_umap_5000_1000_2_2364842*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *\
fWRU
S__inference_fmn10_umap_5000_1000_2_layer_call_and_return_conditional_losses_236483920
.fmn10_umap_5000_1000_2/StatefulPartitionedCall�
.fmn10_umap_5000_1000_3/StatefulPartitionedCallStatefulPartitionedCall7fmn10_umap_5000_1000_2/StatefulPartitionedCall:output:0fmn10_umap_5000_1000_3_2364857fmn10_umap_5000_1000_3_2364859*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *\
fWRU
S__inference_fmn10_umap_5000_1000_3_layer_call_and_return_conditional_losses_236485620
.fmn10_umap_5000_1000_3/StatefulPartitionedCall�
0fmn10_umap_5000_1000_out/StatefulPartitionedCallStatefulPartitionedCall7fmn10_umap_5000_1000_3/StatefulPartitionedCall:output:0 fmn10_umap_5000_1000_out_2364874 fmn10_umap_5000_1000_out_2364876*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *^
fYRW
U__inference_fmn10_umap_5000_1000_out_layer_call_and_return_conditional_losses_236487322
0fmn10_umap_5000_1000_out/StatefulPartitionedCall�
IdentityIdentity9fmn10_umap_5000_1000_out/StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identity�
NoOpNoOp/^fmn10_umap_5000_1000_1/StatefulPartitionedCall/^fmn10_umap_5000_1000_2/StatefulPartitionedCall/^fmn10_umap_5000_1000_3/StatefulPartitionedCall1^fmn10_umap_5000_1000_out/StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*7
_input_shapes&
$:����������: : : : : : : : 2`
.fmn10_umap_5000_1000_1/StatefulPartitionedCall.fmn10_umap_5000_1000_1/StatefulPartitionedCall2`
.fmn10_umap_5000_1000_2/StatefulPartitionedCall.fmn10_umap_5000_1000_2/StatefulPartitionedCall2`
.fmn10_umap_5000_1000_3/StatefulPartitionedCall.fmn10_umap_5000_1000_3/StatefulPartitionedCall2d
0fmn10_umap_5000_1000_out/StatefulPartitionedCall0fmn10_umap_5000_1000_out/StatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
S__inference_fmn10_umap_5000_1000_2_layer_call_and_return_conditional_losses_2365249

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
8__inference_fmn10_umap_5000_1000_3_layer_call_fn_2365258

inputs
unknown:
��
	unknown_0:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *\
fWRU
S__inference_fmn10_umap_5000_1000_3_layer_call_and_return_conditional_losses_23648562
StatefulPartitionedCall|
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:����������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
U__inference_fmn10_umap_5000_1000_out_layer_call_and_return_conditional_losses_2365289

inputs1
matmul_readvariableop_resource:	�-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes
:	�*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2	
BiasAdda
SigmoidSigmoidBiasAdd:output:0*
T0*'
_output_shapes
:���������2	
Sigmoidf
IdentityIdentitySigmoid:y:0^NoOp*
T0*'
_output_shapes
:���������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
:__inference_fmn10_umap_5000_1000_out_layer_call_fn_2365278

inputs
unknown:	�
	unknown_0:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *^
fYRW
U__inference_fmn10_umap_5000_1000_out_layer_call_and_return_conditional_losses_23648732
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
U__inference_fmn10_umap_5000_1000_out_layer_call_and_return_conditional_losses_2364873

inputs1
matmul_readvariableop_resource:	�-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes
:	�*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2	
BiasAdda
SigmoidSigmoidBiasAdd:output:0*
T0*'
_output_shapes
:���������2	
Sigmoidf
IdentityIdentitySigmoid:y:0^NoOp*
T0*'
_output_shapes
:���������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�

�
6__inference_fmn10_umap_5000_1000_layer_call_fn_2364899
fmn10_umap_5000_1000_input
unknown:
��
	unknown_0:	�
	unknown_1:
��
	unknown_2:	�
	unknown_3:
��
	unknown_4:	�
	unknown_5:	�
	unknown_6:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallfmn10_umap_5000_1000_inputunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6*
Tin
2	*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������**
_read_only_resource_inputs

*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_fmn10_umap_5000_1000_layer_call_and_return_conditional_losses_23648802
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*7
_input_shapes&
$:����������: : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:d `
(
_output_shapes
:����������
4
_user_specified_namefmn10_umap_5000_1000_input
�
�
S__inference_fmn10_umap_5000_1000_1_layer_call_and_return_conditional_losses_2364822

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�5
�
Q__inference_fmn10_umap_5000_1000_layer_call_and_return_conditional_losses_2365177

inputsI
5fmn10_umap_5000_1000_1_matmul_readvariableop_resource:
��E
6fmn10_umap_5000_1000_1_biasadd_readvariableop_resource:	�I
5fmn10_umap_5000_1000_2_matmul_readvariableop_resource:
��E
6fmn10_umap_5000_1000_2_biasadd_readvariableop_resource:	�I
5fmn10_umap_5000_1000_3_matmul_readvariableop_resource:
��E
6fmn10_umap_5000_1000_3_biasadd_readvariableop_resource:	�J
7fmn10_umap_5000_1000_out_matmul_readvariableop_resource:	�F
8fmn10_umap_5000_1000_out_biasadd_readvariableop_resource:
identity��-fmn10_umap_5000_1000_1/BiasAdd/ReadVariableOp�,fmn10_umap_5000_1000_1/MatMul/ReadVariableOp�-fmn10_umap_5000_1000_2/BiasAdd/ReadVariableOp�,fmn10_umap_5000_1000_2/MatMul/ReadVariableOp�-fmn10_umap_5000_1000_3/BiasAdd/ReadVariableOp�,fmn10_umap_5000_1000_3/MatMul/ReadVariableOp�/fmn10_umap_5000_1000_out/BiasAdd/ReadVariableOp�.fmn10_umap_5000_1000_out/MatMul/ReadVariableOp�
,fmn10_umap_5000_1000_1/MatMul/ReadVariableOpReadVariableOp5fmn10_umap_5000_1000_1_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02.
,fmn10_umap_5000_1000_1/MatMul/ReadVariableOp�
fmn10_umap_5000_1000_1/MatMulMatMulinputs4fmn10_umap_5000_1000_1/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn10_umap_5000_1000_1/MatMul�
-fmn10_umap_5000_1000_1/BiasAdd/ReadVariableOpReadVariableOp6fmn10_umap_5000_1000_1_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02/
-fmn10_umap_5000_1000_1/BiasAdd/ReadVariableOp�
fmn10_umap_5000_1000_1/BiasAddBiasAdd'fmn10_umap_5000_1000_1/MatMul:product:05fmn10_umap_5000_1000_1/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2 
fmn10_umap_5000_1000_1/BiasAdd�
fmn10_umap_5000_1000_1/ReluRelu'fmn10_umap_5000_1000_1/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
fmn10_umap_5000_1000_1/Relu�
,fmn10_umap_5000_1000_2/MatMul/ReadVariableOpReadVariableOp5fmn10_umap_5000_1000_2_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02.
,fmn10_umap_5000_1000_2/MatMul/ReadVariableOp�
fmn10_umap_5000_1000_2/MatMulMatMul)fmn10_umap_5000_1000_1/Relu:activations:04fmn10_umap_5000_1000_2/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn10_umap_5000_1000_2/MatMul�
-fmn10_umap_5000_1000_2/BiasAdd/ReadVariableOpReadVariableOp6fmn10_umap_5000_1000_2_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02/
-fmn10_umap_5000_1000_2/BiasAdd/ReadVariableOp�
fmn10_umap_5000_1000_2/BiasAddBiasAdd'fmn10_umap_5000_1000_2/MatMul:product:05fmn10_umap_5000_1000_2/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2 
fmn10_umap_5000_1000_2/BiasAdd�
fmn10_umap_5000_1000_2/ReluRelu'fmn10_umap_5000_1000_2/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
fmn10_umap_5000_1000_2/Relu�
,fmn10_umap_5000_1000_3/MatMul/ReadVariableOpReadVariableOp5fmn10_umap_5000_1000_3_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02.
,fmn10_umap_5000_1000_3/MatMul/ReadVariableOp�
fmn10_umap_5000_1000_3/MatMulMatMul)fmn10_umap_5000_1000_2/Relu:activations:04fmn10_umap_5000_1000_3/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn10_umap_5000_1000_3/MatMul�
-fmn10_umap_5000_1000_3/BiasAdd/ReadVariableOpReadVariableOp6fmn10_umap_5000_1000_3_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02/
-fmn10_umap_5000_1000_3/BiasAdd/ReadVariableOp�
fmn10_umap_5000_1000_3/BiasAddBiasAdd'fmn10_umap_5000_1000_3/MatMul:product:05fmn10_umap_5000_1000_3/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2 
fmn10_umap_5000_1000_3/BiasAdd�
fmn10_umap_5000_1000_3/ReluRelu'fmn10_umap_5000_1000_3/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
fmn10_umap_5000_1000_3/Relu�
.fmn10_umap_5000_1000_out/MatMul/ReadVariableOpReadVariableOp7fmn10_umap_5000_1000_out_matmul_readvariableop_resource*
_output_shapes
:	�*
dtype020
.fmn10_umap_5000_1000_out/MatMul/ReadVariableOp�
fmn10_umap_5000_1000_out/MatMulMatMul)fmn10_umap_5000_1000_3/Relu:activations:06fmn10_umap_5000_1000_out/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2!
fmn10_umap_5000_1000_out/MatMul�
/fmn10_umap_5000_1000_out/BiasAdd/ReadVariableOpReadVariableOp8fmn10_umap_5000_1000_out_biasadd_readvariableop_resource*
_output_shapes
:*
dtype021
/fmn10_umap_5000_1000_out/BiasAdd/ReadVariableOp�
 fmn10_umap_5000_1000_out/BiasAddBiasAdd)fmn10_umap_5000_1000_out/MatMul:product:07fmn10_umap_5000_1000_out/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2"
 fmn10_umap_5000_1000_out/BiasAdd�
 fmn10_umap_5000_1000_out/SigmoidSigmoid)fmn10_umap_5000_1000_out/BiasAdd:output:0*
T0*'
_output_shapes
:���������2"
 fmn10_umap_5000_1000_out/Sigmoid
IdentityIdentity$fmn10_umap_5000_1000_out/Sigmoid:y:0^NoOp*
T0*'
_output_shapes
:���������2

Identity�
NoOpNoOp.^fmn10_umap_5000_1000_1/BiasAdd/ReadVariableOp-^fmn10_umap_5000_1000_1/MatMul/ReadVariableOp.^fmn10_umap_5000_1000_2/BiasAdd/ReadVariableOp-^fmn10_umap_5000_1000_2/MatMul/ReadVariableOp.^fmn10_umap_5000_1000_3/BiasAdd/ReadVariableOp-^fmn10_umap_5000_1000_3/MatMul/ReadVariableOp0^fmn10_umap_5000_1000_out/BiasAdd/ReadVariableOp/^fmn10_umap_5000_1000_out/MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*7
_input_shapes&
$:����������: : : : : : : : 2^
-fmn10_umap_5000_1000_1/BiasAdd/ReadVariableOp-fmn10_umap_5000_1000_1/BiasAdd/ReadVariableOp2\
,fmn10_umap_5000_1000_1/MatMul/ReadVariableOp,fmn10_umap_5000_1000_1/MatMul/ReadVariableOp2^
-fmn10_umap_5000_1000_2/BiasAdd/ReadVariableOp-fmn10_umap_5000_1000_2/BiasAdd/ReadVariableOp2\
,fmn10_umap_5000_1000_2/MatMul/ReadVariableOp,fmn10_umap_5000_1000_2/MatMul/ReadVariableOp2^
-fmn10_umap_5000_1000_3/BiasAdd/ReadVariableOp-fmn10_umap_5000_1000_3/BiasAdd/ReadVariableOp2\
,fmn10_umap_5000_1000_3/MatMul/ReadVariableOp,fmn10_umap_5000_1000_3/MatMul/ReadVariableOp2b
/fmn10_umap_5000_1000_out/BiasAdd/ReadVariableOp/fmn10_umap_5000_1000_out/BiasAdd/ReadVariableOp2`
.fmn10_umap_5000_1000_out/MatMul/ReadVariableOp.fmn10_umap_5000_1000_out/MatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
S__inference_fmn10_umap_5000_1000_3_layer_call_and_return_conditional_losses_2364856

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
8__inference_fmn10_umap_5000_1000_1_layer_call_fn_2365218

inputs
unknown:
��
	unknown_0:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *\
fWRU
S__inference_fmn10_umap_5000_1000_1_layer_call_and_return_conditional_losses_23648222
StatefulPartitionedCall|
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:����������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs"�L
saver_filename:0StatefulPartitionedCall_1:0StatefulPartitionedCall_28"
saved_model_main_op

NoOp*>
__saved_model_init_op%#
__saved_model_init_op

NoOp*�
serving_default�
b
fmn10_umap_5000_1000_inputD
,serving_default_fmn10_umap_5000_1000_input:0����������L
fmn10_umap_5000_1000_out0
StatefulPartitionedCall:0���������tensorflow/serving/predict:�f
�
layer-0
layer_with_weights-0
layer-1
layer_with_weights-1
layer-2
layer_with_weights-2
layer-3
layer_with_weights-3
layer-4
	optimizer
	variables
regularization_losses
	trainable_variables

	keras_api

signatures
]__call__
^_default_save_signature
*_&call_and_return_all_conditional_losses"
_tf_keras_network
"
_tf_keras_input_layer
�

kernel
bias
	variables
regularization_losses
trainable_variables
	keras_api
`__call__
*a&call_and_return_all_conditional_losses"
_tf_keras_layer
�

kernel
bias
	variables
regularization_losses
trainable_variables
	keras_api
b__call__
*c&call_and_return_all_conditional_losses"
_tf_keras_layer
�

kernel
bias
	variables
regularization_losses
trainable_variables
	keras_api
d__call__
*e&call_and_return_all_conditional_losses"
_tf_keras_layer
�

kernel
bias
 	variables
!regularization_losses
"trainable_variables
#	keras_api
f__call__
*g&call_and_return_all_conditional_losses"
_tf_keras_layer
�
$iter

%beta_1

&beta_2
	'decay
(learning_ratemMmNmOmPmQmRmSmTvUvVvWvXvYvZv[v\"
	optimizer
X
0
1
2
3
4
5
6
7"
trackable_list_wrapper
 "
trackable_list_wrapper
X
0
1
2
3
4
5
6
7"
trackable_list_wrapper
�
)metrics

*layers
	variables
+layer_metrics
,layer_regularization_losses
regularization_losses
	trainable_variables
-non_trainable_variables
]__call__
^_default_save_signature
*_&call_and_return_all_conditional_losses
&_"call_and_return_conditional_losses"
_generic_user_object
,
hserving_default"
signature_map
1:/
��2fmn10_umap_5000_1000_1/kernel
*:(�2fmn10_umap_5000_1000_1/bias
.
0
1"
trackable_list_wrapper
 "
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
�
.metrics

/layers
	variables
regularization_losses
0layer_metrics
1layer_regularization_losses
trainable_variables
2non_trainable_variables
`__call__
*a&call_and_return_all_conditional_losses
&a"call_and_return_conditional_losses"
_generic_user_object
1:/
��2fmn10_umap_5000_1000_2/kernel
*:(�2fmn10_umap_5000_1000_2/bias
.
0
1"
trackable_list_wrapper
 "
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
�
3metrics

4layers
	variables
regularization_losses
5layer_metrics
6layer_regularization_losses
trainable_variables
7non_trainable_variables
b__call__
*c&call_and_return_all_conditional_losses
&c"call_and_return_conditional_losses"
_generic_user_object
1:/
��2fmn10_umap_5000_1000_3/kernel
*:(�2fmn10_umap_5000_1000_3/bias
.
0
1"
trackable_list_wrapper
 "
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
�
8metrics

9layers
	variables
regularization_losses
:layer_metrics
;layer_regularization_losses
trainable_variables
<non_trainable_variables
d__call__
*e&call_and_return_all_conditional_losses
&e"call_and_return_conditional_losses"
_generic_user_object
2:0	�2fmn10_umap_5000_1000_out/kernel
+:)2fmn10_umap_5000_1000_out/bias
.
0
1"
trackable_list_wrapper
 "
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
�
=metrics

>layers
 	variables
!regularization_losses
?layer_metrics
@layer_regularization_losses
"trainable_variables
Anon_trainable_variables
f__call__
*g&call_and_return_all_conditional_losses
&g"call_and_return_conditional_losses"
_generic_user_object
:	 (2	Adam/iter
: (2Adam/beta_1
: (2Adam/beta_2
: (2
Adam/decay
: (2Adam/learning_rate
.
B0
C1"
trackable_list_wrapper
C
0
1
2
3
4"
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
N
	Dtotal
	Ecount
F	variables
G	keras_api"
_tf_keras_metric
^
	Htotal
	Icount
J
_fn_kwargs
K	variables
L	keras_api"
_tf_keras_metric
:  (2total
:  (2count
.
D0
E1"
trackable_list_wrapper
-
F	variables"
_generic_user_object
:  (2total
:  (2count
 "
trackable_dict_wrapper
.
H0
I1"
trackable_list_wrapper
-
K	variables"
_generic_user_object
6:4
��2$Adam/fmn10_umap_5000_1000_1/kernel/m
/:-�2"Adam/fmn10_umap_5000_1000_1/bias/m
6:4
��2$Adam/fmn10_umap_5000_1000_2/kernel/m
/:-�2"Adam/fmn10_umap_5000_1000_2/bias/m
6:4
��2$Adam/fmn10_umap_5000_1000_3/kernel/m
/:-�2"Adam/fmn10_umap_5000_1000_3/bias/m
7:5	�2&Adam/fmn10_umap_5000_1000_out/kernel/m
0:.2$Adam/fmn10_umap_5000_1000_out/bias/m
6:4
��2$Adam/fmn10_umap_5000_1000_1/kernel/v
/:-�2"Adam/fmn10_umap_5000_1000_1/bias/v
6:4
��2$Adam/fmn10_umap_5000_1000_2/kernel/v
/:-�2"Adam/fmn10_umap_5000_1000_2/bias/v
6:4
��2$Adam/fmn10_umap_5000_1000_3/kernel/v
/:-�2"Adam/fmn10_umap_5000_1000_3/bias/v
7:5	�2&Adam/fmn10_umap_5000_1000_out/kernel/v
0:.2$Adam/fmn10_umap_5000_1000_out/bias/v
�2�
6__inference_fmn10_umap_5000_1000_layer_call_fn_2364899
6__inference_fmn10_umap_5000_1000_layer_call_fn_2365124
6__inference_fmn10_umap_5000_1000_layer_call_fn_2365145
6__inference_fmn10_umap_5000_1000_layer_call_fn_2365026�
���
FullArgSpec1
args)�&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�B�
"__inference__wrapped_model_2364804fmn10_umap_5000_1000_input"�
���
FullArgSpec
args� 
varargsjargs
varkwjkwargs
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
Q__inference_fmn10_umap_5000_1000_layer_call_and_return_conditional_losses_2365177
Q__inference_fmn10_umap_5000_1000_layer_call_and_return_conditional_losses_2365209
Q__inference_fmn10_umap_5000_1000_layer_call_and_return_conditional_losses_2365050
Q__inference_fmn10_umap_5000_1000_layer_call_and_return_conditional_losses_2365074�
���
FullArgSpec1
args)�&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2�
8__inference_fmn10_umap_5000_1000_1_layer_call_fn_2365218�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
S__inference_fmn10_umap_5000_1000_1_layer_call_and_return_conditional_losses_2365229�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
8__inference_fmn10_umap_5000_1000_2_layer_call_fn_2365238�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
S__inference_fmn10_umap_5000_1000_2_layer_call_and_return_conditional_losses_2365249�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
8__inference_fmn10_umap_5000_1000_3_layer_call_fn_2365258�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
S__inference_fmn10_umap_5000_1000_3_layer_call_and_return_conditional_losses_2365269�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
:__inference_fmn10_umap_5000_1000_out_layer_call_fn_2365278�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
U__inference_fmn10_umap_5000_1000_out_layer_call_and_return_conditional_losses_2365289�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
%__inference_signature_wrapper_2365103fmn10_umap_5000_1000_input"�
���
FullArgSpec
args� 
varargs
 
varkwjkwargs
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 �
"__inference__wrapped_model_2364804�D�A
:�7
5�2
fmn10_umap_5000_1000_input����������
� "S�P
N
fmn10_umap_5000_1000_out2�/
fmn10_umap_5000_1000_out����������
S__inference_fmn10_umap_5000_1000_1_layer_call_and_return_conditional_losses_2365229^0�-
&�#
!�
inputs����������
� "&�#
�
0����������
� �
8__inference_fmn10_umap_5000_1000_1_layer_call_fn_2365218Q0�-
&�#
!�
inputs����������
� "������������
S__inference_fmn10_umap_5000_1000_2_layer_call_and_return_conditional_losses_2365249^0�-
&�#
!�
inputs����������
� "&�#
�
0����������
� �
8__inference_fmn10_umap_5000_1000_2_layer_call_fn_2365238Q0�-
&�#
!�
inputs����������
� "������������
S__inference_fmn10_umap_5000_1000_3_layer_call_and_return_conditional_losses_2365269^0�-
&�#
!�
inputs����������
� "&�#
�
0����������
� �
8__inference_fmn10_umap_5000_1000_3_layer_call_fn_2365258Q0�-
&�#
!�
inputs����������
� "������������
Q__inference_fmn10_umap_5000_1000_layer_call_and_return_conditional_losses_2365050L�I
B�?
5�2
fmn10_umap_5000_1000_input����������
p 

 
� "%�"
�
0���������
� �
Q__inference_fmn10_umap_5000_1000_layer_call_and_return_conditional_losses_2365074L�I
B�?
5�2
fmn10_umap_5000_1000_input����������
p

 
� "%�"
�
0���������
� �
Q__inference_fmn10_umap_5000_1000_layer_call_and_return_conditional_losses_2365177k8�5
.�+
!�
inputs����������
p 

 
� "%�"
�
0���������
� �
Q__inference_fmn10_umap_5000_1000_layer_call_and_return_conditional_losses_2365209k8�5
.�+
!�
inputs����������
p

 
� "%�"
�
0���������
� �
6__inference_fmn10_umap_5000_1000_layer_call_fn_2364899rL�I
B�?
5�2
fmn10_umap_5000_1000_input����������
p 

 
� "�����������
6__inference_fmn10_umap_5000_1000_layer_call_fn_2365026rL�I
B�?
5�2
fmn10_umap_5000_1000_input����������
p

 
� "�����������
6__inference_fmn10_umap_5000_1000_layer_call_fn_2365124^8�5
.�+
!�
inputs����������
p 

 
� "�����������
6__inference_fmn10_umap_5000_1000_layer_call_fn_2365145^8�5
.�+
!�
inputs����������
p

 
� "�����������
U__inference_fmn10_umap_5000_1000_out_layer_call_and_return_conditional_losses_2365289]0�-
&�#
!�
inputs����������
� "%�"
�
0���������
� �
:__inference_fmn10_umap_5000_1000_out_layer_call_fn_2365278P0�-
&�#
!�
inputs����������
� "�����������
%__inference_signature_wrapper_2365103�b�_
� 
X�U
S
fmn10_umap_5000_1000_input5�2
fmn10_umap_5000_1000_input����������"S�P
N
fmn10_umap_5000_1000_out2�/
fmn10_umap_5000_1000_out���������