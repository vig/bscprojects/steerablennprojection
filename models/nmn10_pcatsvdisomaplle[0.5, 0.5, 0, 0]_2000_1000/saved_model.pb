�
��
B
AssignVariableOp
resource
value"dtype"
dtypetype�
~
BiasAdd

value"T	
bias"T
output"T" 
Ttype:
2	"-
data_formatstringNHWC:
NHWCNCHW
h
ConcatV2
values"T*N
axis"Tidx
output"T"
Nint(0"	
Ttype"
Tidxtype0:
2	
8
Const
output"dtype"
valuetensor"
dtypetype
.
Identity

input"T
output"T"	
Ttype
q
MatMul
a"T
b"T
product"T"
transpose_abool( "
transpose_bbool( "
Ttype:

2	
e
MergeV2Checkpoints
checkpoint_prefixes
destination_prefix"
delete_old_dirsbool(�

NoOp
M
Pack
values"T*N
output"T"
Nint(0"	
Ttype"
axisint 
C
Placeholder
output"dtype"
dtypetype"
shapeshape:
@
ReadVariableOp
resource
value"dtype"
dtypetype�
E
Relu
features"T
activations"T"
Ttype:
2	
o
	RestoreV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0�
l
SaveV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0�
?
Select
	condition

t"T
e"T
output"T"	
Ttype
H
ShardedFilename
basename	
shard

num_shards
filename
0
Sigmoid
x"T
y"T"
Ttype:

2
�
StatefulPartitionedCall
args2Tin
output2Tout"
Tin
list(type)("
Tout
list(type)("	
ffunc"
configstring "
config_protostring "
executor_typestring �
@
StaticRegexFullMatch	
input

output
"
patternstring
N

StringJoin
inputs*N

output"
Nint(0"
	separatorstring 
�
VarHandleOp
resource"
	containerstring "
shared_namestring "
dtypetype"
shapeshape"#
allowed_deviceslist(string)
 �"serve*2.6.02unknown8ۻ
�
nmn10_pca_2000_1000_1/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*-
shared_namenmn10_pca_2000_1000_1/kernel
�
0nmn10_pca_2000_1000_1/kernel/Read/ReadVariableOpReadVariableOpnmn10_pca_2000_1000_1/kernel* 
_output_shapes
:
��*
dtype0
�
nmn10_pca_2000_1000_1/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*+
shared_namenmn10_pca_2000_1000_1/bias
�
.nmn10_pca_2000_1000_1/bias/Read/ReadVariableOpReadVariableOpnmn10_pca_2000_1000_1/bias*
_output_shapes	
:�*
dtype0
�
nmn10_tsvd_2000_1000_1/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*.
shared_namenmn10_tsvd_2000_1000_1/kernel
�
1nmn10_tsvd_2000_1000_1/kernel/Read/ReadVariableOpReadVariableOpnmn10_tsvd_2000_1000_1/kernel* 
_output_shapes
:
��*
dtype0
�
nmn10_tsvd_2000_1000_1/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*,
shared_namenmn10_tsvd_2000_1000_1/bias
�
/nmn10_tsvd_2000_1000_1/bias/Read/ReadVariableOpReadVariableOpnmn10_tsvd_2000_1000_1/bias*
_output_shapes	
:�*
dtype0
�
nmn10_isomap_2000_1000_1/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*0
shared_name!nmn10_isomap_2000_1000_1/kernel
�
3nmn10_isomap_2000_1000_1/kernel/Read/ReadVariableOpReadVariableOpnmn10_isomap_2000_1000_1/kernel* 
_output_shapes
:
��*
dtype0
�
nmn10_isomap_2000_1000_1/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*.
shared_namenmn10_isomap_2000_1000_1/bias
�
1nmn10_isomap_2000_1000_1/bias/Read/ReadVariableOpReadVariableOpnmn10_isomap_2000_1000_1/bias*
_output_shapes	
:�*
dtype0
�
nmn10_lle_2000_1000_1/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*-
shared_namenmn10_lle_2000_1000_1/kernel
�
0nmn10_lle_2000_1000_1/kernel/Read/ReadVariableOpReadVariableOpnmn10_lle_2000_1000_1/kernel* 
_output_shapes
:
��*
dtype0
�
nmn10_lle_2000_1000_1/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*+
shared_namenmn10_lle_2000_1000_1/bias
�
.nmn10_lle_2000_1000_1/bias/Read/ReadVariableOpReadVariableOpnmn10_lle_2000_1000_1/bias*
_output_shapes	
:�*
dtype0
�
nmn10_pca_2000_1000_2/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*-
shared_namenmn10_pca_2000_1000_2/kernel
�
0nmn10_pca_2000_1000_2/kernel/Read/ReadVariableOpReadVariableOpnmn10_pca_2000_1000_2/kernel* 
_output_shapes
:
��*
dtype0
�
nmn10_pca_2000_1000_2/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*+
shared_namenmn10_pca_2000_1000_2/bias
�
.nmn10_pca_2000_1000_2/bias/Read/ReadVariableOpReadVariableOpnmn10_pca_2000_1000_2/bias*
_output_shapes	
:�*
dtype0
�
nmn10_tsvd_2000_1000_2/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*.
shared_namenmn10_tsvd_2000_1000_2/kernel
�
1nmn10_tsvd_2000_1000_2/kernel/Read/ReadVariableOpReadVariableOpnmn10_tsvd_2000_1000_2/kernel* 
_output_shapes
:
��*
dtype0
�
nmn10_tsvd_2000_1000_2/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*,
shared_namenmn10_tsvd_2000_1000_2/bias
�
/nmn10_tsvd_2000_1000_2/bias/Read/ReadVariableOpReadVariableOpnmn10_tsvd_2000_1000_2/bias*
_output_shapes	
:�*
dtype0
�
nmn10_isomap_2000_1000_2/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*0
shared_name!nmn10_isomap_2000_1000_2/kernel
�
3nmn10_isomap_2000_1000_2/kernel/Read/ReadVariableOpReadVariableOpnmn10_isomap_2000_1000_2/kernel* 
_output_shapes
:
��*
dtype0
�
nmn10_isomap_2000_1000_2/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*.
shared_namenmn10_isomap_2000_1000_2/bias
�
1nmn10_isomap_2000_1000_2/bias/Read/ReadVariableOpReadVariableOpnmn10_isomap_2000_1000_2/bias*
_output_shapes	
:�*
dtype0
�
nmn10_lle_2000_1000_2/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*-
shared_namenmn10_lle_2000_1000_2/kernel
�
0nmn10_lle_2000_1000_2/kernel/Read/ReadVariableOpReadVariableOpnmn10_lle_2000_1000_2/kernel* 
_output_shapes
:
��*
dtype0
�
nmn10_lle_2000_1000_2/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*+
shared_namenmn10_lle_2000_1000_2/bias
�
.nmn10_lle_2000_1000_2/bias/Read/ReadVariableOpReadVariableOpnmn10_lle_2000_1000_2/bias*
_output_shapes	
:�*
dtype0
�
nmn10_pca_2000_1000_3/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*-
shared_namenmn10_pca_2000_1000_3/kernel
�
0nmn10_pca_2000_1000_3/kernel/Read/ReadVariableOpReadVariableOpnmn10_pca_2000_1000_3/kernel* 
_output_shapes
:
��*
dtype0
�
nmn10_pca_2000_1000_3/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*+
shared_namenmn10_pca_2000_1000_3/bias
�
.nmn10_pca_2000_1000_3/bias/Read/ReadVariableOpReadVariableOpnmn10_pca_2000_1000_3/bias*
_output_shapes	
:�*
dtype0
�
nmn10_tsvd_2000_1000_3/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*.
shared_namenmn10_tsvd_2000_1000_3/kernel
�
1nmn10_tsvd_2000_1000_3/kernel/Read/ReadVariableOpReadVariableOpnmn10_tsvd_2000_1000_3/kernel* 
_output_shapes
:
��*
dtype0
�
nmn10_tsvd_2000_1000_3/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*,
shared_namenmn10_tsvd_2000_1000_3/bias
�
/nmn10_tsvd_2000_1000_3/bias/Read/ReadVariableOpReadVariableOpnmn10_tsvd_2000_1000_3/bias*
_output_shapes	
:�*
dtype0
�
nmn10_isomap_2000_1000_3/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*0
shared_name!nmn10_isomap_2000_1000_3/kernel
�
3nmn10_isomap_2000_1000_3/kernel/Read/ReadVariableOpReadVariableOpnmn10_isomap_2000_1000_3/kernel* 
_output_shapes
:
��*
dtype0
�
nmn10_isomap_2000_1000_3/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*.
shared_namenmn10_isomap_2000_1000_3/bias
�
1nmn10_isomap_2000_1000_3/bias/Read/ReadVariableOpReadVariableOpnmn10_isomap_2000_1000_3/bias*
_output_shapes	
:�*
dtype0
�
nmn10_lle_2000_1000_3/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*-
shared_namenmn10_lle_2000_1000_3/kernel
�
0nmn10_lle_2000_1000_3/kernel/Read/ReadVariableOpReadVariableOpnmn10_lle_2000_1000_3/kernel* 
_output_shapes
:
��*
dtype0
�
nmn10_lle_2000_1000_3/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*+
shared_namenmn10_lle_2000_1000_3/bias
�
.nmn10_lle_2000_1000_3/bias/Read/ReadVariableOpReadVariableOpnmn10_lle_2000_1000_3/bias*
_output_shapes	
:�*
dtype0
�
nmn10_pca_2000_1000_out/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:	�*/
shared_name nmn10_pca_2000_1000_out/kernel
�
2nmn10_pca_2000_1000_out/kernel/Read/ReadVariableOpReadVariableOpnmn10_pca_2000_1000_out/kernel*
_output_shapes
:	�*
dtype0
�
nmn10_pca_2000_1000_out/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:*-
shared_namenmn10_pca_2000_1000_out/bias
�
0nmn10_pca_2000_1000_out/bias/Read/ReadVariableOpReadVariableOpnmn10_pca_2000_1000_out/bias*
_output_shapes
:*
dtype0
�
nmn10_tsvd_2000_1000_out/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:	�*0
shared_name!nmn10_tsvd_2000_1000_out/kernel
�
3nmn10_tsvd_2000_1000_out/kernel/Read/ReadVariableOpReadVariableOpnmn10_tsvd_2000_1000_out/kernel*
_output_shapes
:	�*
dtype0
�
nmn10_tsvd_2000_1000_out/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:*.
shared_namenmn10_tsvd_2000_1000_out/bias
�
1nmn10_tsvd_2000_1000_out/bias/Read/ReadVariableOpReadVariableOpnmn10_tsvd_2000_1000_out/bias*
_output_shapes
:*
dtype0
�
!nmn10_isomap_2000_1000_out/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:	�*2
shared_name#!nmn10_isomap_2000_1000_out/kernel
�
5nmn10_isomap_2000_1000_out/kernel/Read/ReadVariableOpReadVariableOp!nmn10_isomap_2000_1000_out/kernel*
_output_shapes
:	�*
dtype0
�
nmn10_isomap_2000_1000_out/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:*0
shared_name!nmn10_isomap_2000_1000_out/bias
�
3nmn10_isomap_2000_1000_out/bias/Read/ReadVariableOpReadVariableOpnmn10_isomap_2000_1000_out/bias*
_output_shapes
:*
dtype0
�
nmn10_lle_2000_1000_out/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:	�*/
shared_name nmn10_lle_2000_1000_out/kernel
�
2nmn10_lle_2000_1000_out/kernel/Read/ReadVariableOpReadVariableOpnmn10_lle_2000_1000_out/kernel*
_output_shapes
:	�*
dtype0
�
nmn10_lle_2000_1000_out/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:*-
shared_namenmn10_lle_2000_1000_out/bias
�
0nmn10_lle_2000_1000_out/bias/Read/ReadVariableOpReadVariableOpnmn10_lle_2000_1000_out/bias*
_output_shapes
:*
dtype0
v
joined/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:*
shared_namejoined/kernel
o
!joined/kernel/Read/ReadVariableOpReadVariableOpjoined/kernel*
_output_shapes

:*
dtype0
n
joined/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:*
shared_namejoined/bias
g
joined/bias/Read/ReadVariableOpReadVariableOpjoined/bias*
_output_shapes
:*
dtype0
f
	Adam/iterVarHandleOp*
_output_shapes
: *
dtype0	*
shape: *
shared_name	Adam/iter
_
Adam/iter/Read/ReadVariableOpReadVariableOp	Adam/iter*
_output_shapes
: *
dtype0	
j
Adam/beta_1VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nameAdam/beta_1
c
Adam/beta_1/Read/ReadVariableOpReadVariableOpAdam/beta_1*
_output_shapes
: *
dtype0
j
Adam/beta_2VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nameAdam/beta_2
c
Adam/beta_2/Read/ReadVariableOpReadVariableOpAdam/beta_2*
_output_shapes
: *
dtype0
h

Adam/decayVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name
Adam/decay
a
Adam/decay/Read/ReadVariableOpReadVariableOp
Adam/decay*
_output_shapes
: *
dtype0
x
Adam/learning_rateVarHandleOp*
_output_shapes
: *
dtype0*
shape: *#
shared_nameAdam/learning_rate
q
&Adam/learning_rate/Read/ReadVariableOpReadVariableOpAdam/learning_rate*
_output_shapes
: *
dtype0
^
totalVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nametotal
W
total/Read/ReadVariableOpReadVariableOptotal*
_output_shapes
: *
dtype0
^
countVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_namecount
W
count/Read/ReadVariableOpReadVariableOpcount*
_output_shapes
: *
dtype0
b
total_1VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name	total_1
[
total_1/Read/ReadVariableOpReadVariableOptotal_1*
_output_shapes
: *
dtype0
b
count_1VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name	count_1
[
count_1/Read/ReadVariableOpReadVariableOpcount_1*
_output_shapes
: *
dtype0
�
Adam/joined/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape
:*%
shared_nameAdam/joined/kernel/m
}
(Adam/joined/kernel/m/Read/ReadVariableOpReadVariableOpAdam/joined/kernel/m*
_output_shapes

:*
dtype0
|
Adam/joined/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:*#
shared_nameAdam/joined/bias/m
u
&Adam/joined/bias/m/Read/ReadVariableOpReadVariableOpAdam/joined/bias/m*
_output_shapes
:*
dtype0
�
Adam/joined/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape
:*%
shared_nameAdam/joined/kernel/v
}
(Adam/joined/kernel/v/Read/ReadVariableOpReadVariableOpAdam/joined/kernel/v*
_output_shapes

:*
dtype0
|
Adam/joined/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:*#
shared_nameAdam/joined/bias/v
u
&Adam/joined/bias/v/Read/ReadVariableOpReadVariableOpAdam/joined/bias/v*
_output_shapes
:*
dtype0

NoOpNoOp
�k
ConstConst"/device:CPU:0*
_output_shapes
: *
dtype0*�j
value�jB�j B�j
�
layer-0
layer-1
layer-2
layer-3
layer_with_weights-0
layer-4
layer_with_weights-1
layer-5
layer_with_weights-2
layer-6
layer_with_weights-3
layer-7
	layer_with_weights-4
	layer-8

layer_with_weights-5

layer-9
layer_with_weights-6
layer-10
layer_with_weights-7
layer-11
layer_with_weights-8
layer-12
layer_with_weights-9
layer-13
layer_with_weights-10
layer-14
layer_with_weights-11
layer-15
layer_with_weights-12
layer-16
layer_with_weights-13
layer-17
layer_with_weights-14
layer-18
layer_with_weights-15
layer-19
layer-20
layer_with_weights-16
layer-21
	optimizer
	variables
regularization_losses
trainable_variables
	keras_api

signatures
%
#_self_saveable_object_factories
%
#_self_saveable_object_factories
%
#_self_saveable_object_factories
%
# _self_saveable_object_factories
�

!kernel
"bias
##_self_saveable_object_factories
$	variables
%regularization_losses
&trainable_variables
'	keras_api
�

(kernel
)bias
#*_self_saveable_object_factories
+	variables
,regularization_losses
-trainable_variables
.	keras_api
�

/kernel
0bias
#1_self_saveable_object_factories
2	variables
3regularization_losses
4trainable_variables
5	keras_api
�

6kernel
7bias
#8_self_saveable_object_factories
9	variables
:regularization_losses
;trainable_variables
<	keras_api
�

=kernel
>bias
#?_self_saveable_object_factories
@	variables
Aregularization_losses
Btrainable_variables
C	keras_api
�

Dkernel
Ebias
#F_self_saveable_object_factories
G	variables
Hregularization_losses
Itrainable_variables
J	keras_api
�

Kkernel
Lbias
#M_self_saveable_object_factories
N	variables
Oregularization_losses
Ptrainable_variables
Q	keras_api
�

Rkernel
Sbias
#T_self_saveable_object_factories
U	variables
Vregularization_losses
Wtrainable_variables
X	keras_api
�

Ykernel
Zbias
#[_self_saveable_object_factories
\	variables
]regularization_losses
^trainable_variables
_	keras_api
�

`kernel
abias
#b_self_saveable_object_factories
c	variables
dregularization_losses
etrainable_variables
f	keras_api
�

gkernel
hbias
#i_self_saveable_object_factories
j	variables
kregularization_losses
ltrainable_variables
m	keras_api
�

nkernel
obias
#p_self_saveable_object_factories
q	variables
rregularization_losses
strainable_variables
t	keras_api
�

ukernel
vbias
#w_self_saveable_object_factories
x	variables
yregularization_losses
ztrainable_variables
{	keras_api
�

|kernel
}bias
#~_self_saveable_object_factories
	variables
�regularization_losses
�trainable_variables
�	keras_api
�
�kernel
	�bias
$�_self_saveable_object_factories
�	variables
�regularization_losses
�trainable_variables
�	keras_api
�
�kernel
	�bias
$�_self_saveable_object_factories
�	variables
�regularization_losses
�trainable_variables
�	keras_api
V
�	variables
�regularization_losses
�trainable_variables
�	keras_api
n
�kernel
	�bias
�	variables
�regularization_losses
�trainable_variables
�	keras_api
q
	�iter
�beta_1
�beta_2

�decay
�learning_rate	�m�	�m�	�v�	�v�
�
!0
"1
(2
)3
/4
05
66
77
=8
>9
D10
E11
K12
L13
R14
S15
Y16
Z17
`18
a19
g20
h21
n22
o23
u24
v25
|26
}27
�28
�29
�30
�31
�32
�33
 

�0
�1
�
�metrics
�layers
	variables
�layer_metrics
 �layer_regularization_losses
regularization_losses
trainable_variables
�non_trainable_variables
 
 
 
 
 
hf
VARIABLE_VALUEnmn10_pca_2000_1000_1/kernel6layer_with_weights-0/kernel/.ATTRIBUTES/VARIABLE_VALUE
db
VARIABLE_VALUEnmn10_pca_2000_1000_1/bias4layer_with_weights-0/bias/.ATTRIBUTES/VARIABLE_VALUE
 

!0
"1
 
 
�
�metrics
�layers
$	variables
%regularization_losses
�layer_metrics
 �layer_regularization_losses
&trainable_variables
�non_trainable_variables
ig
VARIABLE_VALUEnmn10_tsvd_2000_1000_1/kernel6layer_with_weights-1/kernel/.ATTRIBUTES/VARIABLE_VALUE
ec
VARIABLE_VALUEnmn10_tsvd_2000_1000_1/bias4layer_with_weights-1/bias/.ATTRIBUTES/VARIABLE_VALUE
 

(0
)1
 
 
�
�metrics
�layers
+	variables
,regularization_losses
�layer_metrics
 �layer_regularization_losses
-trainable_variables
�non_trainable_variables
ki
VARIABLE_VALUEnmn10_isomap_2000_1000_1/kernel6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUE
ge
VARIABLE_VALUEnmn10_isomap_2000_1000_1/bias4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUE
 

/0
01
 
 
�
�metrics
�layers
2	variables
3regularization_losses
�layer_metrics
 �layer_regularization_losses
4trainable_variables
�non_trainable_variables
hf
VARIABLE_VALUEnmn10_lle_2000_1000_1/kernel6layer_with_weights-3/kernel/.ATTRIBUTES/VARIABLE_VALUE
db
VARIABLE_VALUEnmn10_lle_2000_1000_1/bias4layer_with_weights-3/bias/.ATTRIBUTES/VARIABLE_VALUE
 

60
71
 
 
�
�metrics
�layers
9	variables
:regularization_losses
�layer_metrics
 �layer_regularization_losses
;trainable_variables
�non_trainable_variables
hf
VARIABLE_VALUEnmn10_pca_2000_1000_2/kernel6layer_with_weights-4/kernel/.ATTRIBUTES/VARIABLE_VALUE
db
VARIABLE_VALUEnmn10_pca_2000_1000_2/bias4layer_with_weights-4/bias/.ATTRIBUTES/VARIABLE_VALUE
 

=0
>1
 
 
�
�metrics
�layers
@	variables
Aregularization_losses
�layer_metrics
 �layer_regularization_losses
Btrainable_variables
�non_trainable_variables
ig
VARIABLE_VALUEnmn10_tsvd_2000_1000_2/kernel6layer_with_weights-5/kernel/.ATTRIBUTES/VARIABLE_VALUE
ec
VARIABLE_VALUEnmn10_tsvd_2000_1000_2/bias4layer_with_weights-5/bias/.ATTRIBUTES/VARIABLE_VALUE
 

D0
E1
 
 
�
�metrics
�layers
G	variables
Hregularization_losses
�layer_metrics
 �layer_regularization_losses
Itrainable_variables
�non_trainable_variables
ki
VARIABLE_VALUEnmn10_isomap_2000_1000_2/kernel6layer_with_weights-6/kernel/.ATTRIBUTES/VARIABLE_VALUE
ge
VARIABLE_VALUEnmn10_isomap_2000_1000_2/bias4layer_with_weights-6/bias/.ATTRIBUTES/VARIABLE_VALUE
 

K0
L1
 
 
�
�metrics
�layers
N	variables
Oregularization_losses
�layer_metrics
 �layer_regularization_losses
Ptrainable_variables
�non_trainable_variables
hf
VARIABLE_VALUEnmn10_lle_2000_1000_2/kernel6layer_with_weights-7/kernel/.ATTRIBUTES/VARIABLE_VALUE
db
VARIABLE_VALUEnmn10_lle_2000_1000_2/bias4layer_with_weights-7/bias/.ATTRIBUTES/VARIABLE_VALUE
 

R0
S1
 
 
�
�metrics
�layers
U	variables
Vregularization_losses
�layer_metrics
 �layer_regularization_losses
Wtrainable_variables
�non_trainable_variables
hf
VARIABLE_VALUEnmn10_pca_2000_1000_3/kernel6layer_with_weights-8/kernel/.ATTRIBUTES/VARIABLE_VALUE
db
VARIABLE_VALUEnmn10_pca_2000_1000_3/bias4layer_with_weights-8/bias/.ATTRIBUTES/VARIABLE_VALUE
 

Y0
Z1
 
 
�
�metrics
�layers
\	variables
]regularization_losses
�layer_metrics
 �layer_regularization_losses
^trainable_variables
�non_trainable_variables
ig
VARIABLE_VALUEnmn10_tsvd_2000_1000_3/kernel6layer_with_weights-9/kernel/.ATTRIBUTES/VARIABLE_VALUE
ec
VARIABLE_VALUEnmn10_tsvd_2000_1000_3/bias4layer_with_weights-9/bias/.ATTRIBUTES/VARIABLE_VALUE
 

`0
a1
 
 
�
�metrics
�layers
c	variables
dregularization_losses
�layer_metrics
 �layer_regularization_losses
etrainable_variables
�non_trainable_variables
lj
VARIABLE_VALUEnmn10_isomap_2000_1000_3/kernel7layer_with_weights-10/kernel/.ATTRIBUTES/VARIABLE_VALUE
hf
VARIABLE_VALUEnmn10_isomap_2000_1000_3/bias5layer_with_weights-10/bias/.ATTRIBUTES/VARIABLE_VALUE
 

g0
h1
 
 
�
�metrics
�layers
j	variables
kregularization_losses
�layer_metrics
 �layer_regularization_losses
ltrainable_variables
�non_trainable_variables
ig
VARIABLE_VALUEnmn10_lle_2000_1000_3/kernel7layer_with_weights-11/kernel/.ATTRIBUTES/VARIABLE_VALUE
ec
VARIABLE_VALUEnmn10_lle_2000_1000_3/bias5layer_with_weights-11/bias/.ATTRIBUTES/VARIABLE_VALUE
 

n0
o1
 
 
�
�metrics
�layers
q	variables
rregularization_losses
�layer_metrics
 �layer_regularization_losses
strainable_variables
�non_trainable_variables
ki
VARIABLE_VALUEnmn10_pca_2000_1000_out/kernel7layer_with_weights-12/kernel/.ATTRIBUTES/VARIABLE_VALUE
ge
VARIABLE_VALUEnmn10_pca_2000_1000_out/bias5layer_with_weights-12/bias/.ATTRIBUTES/VARIABLE_VALUE
 

u0
v1
 
 
�
�metrics
�layers
x	variables
yregularization_losses
�layer_metrics
 �layer_regularization_losses
ztrainable_variables
�non_trainable_variables
lj
VARIABLE_VALUEnmn10_tsvd_2000_1000_out/kernel7layer_with_weights-13/kernel/.ATTRIBUTES/VARIABLE_VALUE
hf
VARIABLE_VALUEnmn10_tsvd_2000_1000_out/bias5layer_with_weights-13/bias/.ATTRIBUTES/VARIABLE_VALUE
 

|0
}1
 
 
�
�metrics
�layers
	variables
�regularization_losses
�layer_metrics
 �layer_regularization_losses
�trainable_variables
�non_trainable_variables
nl
VARIABLE_VALUE!nmn10_isomap_2000_1000_out/kernel7layer_with_weights-14/kernel/.ATTRIBUTES/VARIABLE_VALUE
jh
VARIABLE_VALUEnmn10_isomap_2000_1000_out/bias5layer_with_weights-14/bias/.ATTRIBUTES/VARIABLE_VALUE
 

�0
�1
 
 
�
�metrics
�layers
�	variables
�regularization_losses
�layer_metrics
 �layer_regularization_losses
�trainable_variables
�non_trainable_variables
ki
VARIABLE_VALUEnmn10_lle_2000_1000_out/kernel7layer_with_weights-15/kernel/.ATTRIBUTES/VARIABLE_VALUE
ge
VARIABLE_VALUEnmn10_lle_2000_1000_out/bias5layer_with_weights-15/bias/.ATTRIBUTES/VARIABLE_VALUE
 

�0
�1
 
 
�
�metrics
�layers
�	variables
�regularization_losses
�layer_metrics
 �layer_regularization_losses
�trainable_variables
�non_trainable_variables
 
 
 
�
�metrics
�layers
�	variables
�regularization_losses
�layer_metrics
 �layer_regularization_losses
�trainable_variables
�non_trainable_variables
ZX
VARIABLE_VALUEjoined/kernel7layer_with_weights-16/kernel/.ATTRIBUTES/VARIABLE_VALUE
VT
VARIABLE_VALUEjoined/bias5layer_with_weights-16/bias/.ATTRIBUTES/VARIABLE_VALUE

�0
�1
 

�0
�1
�
�metrics
�layers
�	variables
�regularization_losses
�layer_metrics
 �layer_regularization_losses
�trainable_variables
�non_trainable_variables
HF
VARIABLE_VALUE	Adam/iter)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUE
LJ
VARIABLE_VALUEAdam/beta_1+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUE
LJ
VARIABLE_VALUEAdam/beta_2+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUE
JH
VARIABLE_VALUE
Adam/decay*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUE
ZX
VARIABLE_VALUEAdam/learning_rate2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUE

�0
�1
�
0
1
2
3
4
5
6
7
	8

9
10
11
12
13
14
15
16
17
18
19
20
21
 
 
�
!0
"1
(2
)3
/4
05
66
77
=8
>9
D10
E11
K12
L13
R14
S15
Y16
Z17
`18
a19
g20
h21
n22
o23
u24
v25
|26
}27
�28
�29
�30
�31
 
 
 
 

!0
"1
 
 
 
 

(0
)1
 
 
 
 

/0
01
 
 
 
 

60
71
 
 
 
 

=0
>1
 
 
 
 

D0
E1
 
 
 
 

K0
L1
 
 
 
 

R0
S1
 
 
 
 

Y0
Z1
 
 
 
 

`0
a1
 
 
 
 

g0
h1
 
 
 
 

n0
o1
 
 
 
 

u0
v1
 
 
 
 

|0
}1
 
 
 
 

�0
�1
 
 
 
 

�0
�1
 
 
 
 
 
 
 
 
 
 
8

�total

�count
�	variables
�	keras_api
I

�total

�count
�
_fn_kwargs
�	variables
�	keras_api
OM
VARIABLE_VALUEtotal4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUE
OM
VARIABLE_VALUEcount4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUE

�0
�1

�	variables
QO
VARIABLE_VALUEtotal_14keras_api/metrics/1/total/.ATTRIBUTES/VARIABLE_VALUE
QO
VARIABLE_VALUEcount_14keras_api/metrics/1/count/.ATTRIBUTES/VARIABLE_VALUE
 

�0
�1

�	variables
}{
VARIABLE_VALUEAdam/joined/kernel/mSlayer_with_weights-16/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
yw
VARIABLE_VALUEAdam/joined/bias/mQlayer_with_weights-16/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
}{
VARIABLE_VALUEAdam/joined/kernel/vSlayer_with_weights-16/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
yw
VARIABLE_VALUEAdam/joined/bias/vQlayer_with_weights-16/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
�
,serving_default_nmn10_isomap_2000_1000_inputPlaceholder*(
_output_shapes
:����������*
dtype0*
shape:����������
�
)serving_default_nmn10_lle_2000_1000_inputPlaceholder*(
_output_shapes
:����������*
dtype0*
shape:����������
�
)serving_default_nmn10_pca_2000_1000_inputPlaceholder*(
_output_shapes
:����������*
dtype0*
shape:����������
�
*serving_default_nmn10_tsvd_2000_1000_inputPlaceholder*(
_output_shapes
:����������*
dtype0*
shape:����������
�
StatefulPartitionedCallStatefulPartitionedCall,serving_default_nmn10_isomap_2000_1000_input)serving_default_nmn10_lle_2000_1000_input)serving_default_nmn10_pca_2000_1000_input*serving_default_nmn10_tsvd_2000_1000_inputnmn10_lle_2000_1000_1/kernelnmn10_lle_2000_1000_1/biasnmn10_isomap_2000_1000_1/kernelnmn10_isomap_2000_1000_1/biasnmn10_tsvd_2000_1000_1/kernelnmn10_tsvd_2000_1000_1/biasnmn10_pca_2000_1000_1/kernelnmn10_pca_2000_1000_1/biasnmn10_lle_2000_1000_2/kernelnmn10_lle_2000_1000_2/biasnmn10_isomap_2000_1000_2/kernelnmn10_isomap_2000_1000_2/biasnmn10_tsvd_2000_1000_2/kernelnmn10_tsvd_2000_1000_2/biasnmn10_pca_2000_1000_2/kernelnmn10_pca_2000_1000_2/biasnmn10_lle_2000_1000_3/kernelnmn10_lle_2000_1000_3/biasnmn10_isomap_2000_1000_3/kernelnmn10_isomap_2000_1000_3/biasnmn10_tsvd_2000_1000_3/kernelnmn10_tsvd_2000_1000_3/biasnmn10_pca_2000_1000_3/kernelnmn10_pca_2000_1000_3/biasnmn10_pca_2000_1000_out/kernelnmn10_pca_2000_1000_out/biasnmn10_tsvd_2000_1000_out/kernelnmn10_tsvd_2000_1000_out/bias!nmn10_isomap_2000_1000_out/kernelnmn10_isomap_2000_1000_out/biasnmn10_lle_2000_1000_out/kernelnmn10_lle_2000_1000_out/biasjoined/kerneljoined/bias*1
Tin*
(2&*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*D
_read_only_resource_inputs&
$"	
 !"#$%*0
config_proto 

CPU

GPU2*0J 8� *-
f(R&
$__inference_signature_wrapper_782233
O
saver_filenamePlaceholder*
_output_shapes
: *
dtype0*
shape: 
�
StatefulPartitionedCall_1StatefulPartitionedCallsaver_filename0nmn10_pca_2000_1000_1/kernel/Read/ReadVariableOp.nmn10_pca_2000_1000_1/bias/Read/ReadVariableOp1nmn10_tsvd_2000_1000_1/kernel/Read/ReadVariableOp/nmn10_tsvd_2000_1000_1/bias/Read/ReadVariableOp3nmn10_isomap_2000_1000_1/kernel/Read/ReadVariableOp1nmn10_isomap_2000_1000_1/bias/Read/ReadVariableOp0nmn10_lle_2000_1000_1/kernel/Read/ReadVariableOp.nmn10_lle_2000_1000_1/bias/Read/ReadVariableOp0nmn10_pca_2000_1000_2/kernel/Read/ReadVariableOp.nmn10_pca_2000_1000_2/bias/Read/ReadVariableOp1nmn10_tsvd_2000_1000_2/kernel/Read/ReadVariableOp/nmn10_tsvd_2000_1000_2/bias/Read/ReadVariableOp3nmn10_isomap_2000_1000_2/kernel/Read/ReadVariableOp1nmn10_isomap_2000_1000_2/bias/Read/ReadVariableOp0nmn10_lle_2000_1000_2/kernel/Read/ReadVariableOp.nmn10_lle_2000_1000_2/bias/Read/ReadVariableOp0nmn10_pca_2000_1000_3/kernel/Read/ReadVariableOp.nmn10_pca_2000_1000_3/bias/Read/ReadVariableOp1nmn10_tsvd_2000_1000_3/kernel/Read/ReadVariableOp/nmn10_tsvd_2000_1000_3/bias/Read/ReadVariableOp3nmn10_isomap_2000_1000_3/kernel/Read/ReadVariableOp1nmn10_isomap_2000_1000_3/bias/Read/ReadVariableOp0nmn10_lle_2000_1000_3/kernel/Read/ReadVariableOp.nmn10_lle_2000_1000_3/bias/Read/ReadVariableOp2nmn10_pca_2000_1000_out/kernel/Read/ReadVariableOp0nmn10_pca_2000_1000_out/bias/Read/ReadVariableOp3nmn10_tsvd_2000_1000_out/kernel/Read/ReadVariableOp1nmn10_tsvd_2000_1000_out/bias/Read/ReadVariableOp5nmn10_isomap_2000_1000_out/kernel/Read/ReadVariableOp3nmn10_isomap_2000_1000_out/bias/Read/ReadVariableOp2nmn10_lle_2000_1000_out/kernel/Read/ReadVariableOp0nmn10_lle_2000_1000_out/bias/Read/ReadVariableOp!joined/kernel/Read/ReadVariableOpjoined/bias/Read/ReadVariableOpAdam/iter/Read/ReadVariableOpAdam/beta_1/Read/ReadVariableOpAdam/beta_2/Read/ReadVariableOpAdam/decay/Read/ReadVariableOp&Adam/learning_rate/Read/ReadVariableOptotal/Read/ReadVariableOpcount/Read/ReadVariableOptotal_1/Read/ReadVariableOpcount_1/Read/ReadVariableOp(Adam/joined/kernel/m/Read/ReadVariableOp&Adam/joined/bias/m/Read/ReadVariableOp(Adam/joined/kernel/v/Read/ReadVariableOp&Adam/joined/bias/v/Read/ReadVariableOpConst*<
Tin5
321	*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *(
f#R!
__inference__traced_save_783165
�
StatefulPartitionedCall_2StatefulPartitionedCallsaver_filenamenmn10_pca_2000_1000_1/kernelnmn10_pca_2000_1000_1/biasnmn10_tsvd_2000_1000_1/kernelnmn10_tsvd_2000_1000_1/biasnmn10_isomap_2000_1000_1/kernelnmn10_isomap_2000_1000_1/biasnmn10_lle_2000_1000_1/kernelnmn10_lle_2000_1000_1/biasnmn10_pca_2000_1000_2/kernelnmn10_pca_2000_1000_2/biasnmn10_tsvd_2000_1000_2/kernelnmn10_tsvd_2000_1000_2/biasnmn10_isomap_2000_1000_2/kernelnmn10_isomap_2000_1000_2/biasnmn10_lle_2000_1000_2/kernelnmn10_lle_2000_1000_2/biasnmn10_pca_2000_1000_3/kernelnmn10_pca_2000_1000_3/biasnmn10_tsvd_2000_1000_3/kernelnmn10_tsvd_2000_1000_3/biasnmn10_isomap_2000_1000_3/kernelnmn10_isomap_2000_1000_3/biasnmn10_lle_2000_1000_3/kernelnmn10_lle_2000_1000_3/biasnmn10_pca_2000_1000_out/kernelnmn10_pca_2000_1000_out/biasnmn10_tsvd_2000_1000_out/kernelnmn10_tsvd_2000_1000_out/bias!nmn10_isomap_2000_1000_out/kernelnmn10_isomap_2000_1000_out/biasnmn10_lle_2000_1000_out/kernelnmn10_lle_2000_1000_out/biasjoined/kerneljoined/bias	Adam/iterAdam/beta_1Adam/beta_2
Adam/decayAdam/learning_ratetotalcounttotal_1count_1Adam/joined/kernel/mAdam/joined/bias/mAdam/joined/kernel/vAdam/joined/bias/v*;
Tin4
220*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *+
f&R$
"__inference__traced_restore_783316��
�
�
R__inference_nmn10_tsvd_2000_1000_3_layer_call_and_return_conditional_losses_781272

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
&__inference_model_layer_call_fn_782309
inputs_0
inputs_1
inputs_2
inputs_3
unknown:
��
	unknown_0:	�
	unknown_1:
��
	unknown_2:	�
	unknown_3:
��
	unknown_4:	�
	unknown_5:
��
	unknown_6:	�
	unknown_7:
��
	unknown_8:	�
	unknown_9:
��

unknown_10:	�

unknown_11:
��

unknown_12:	�

unknown_13:
��

unknown_14:	�

unknown_15:
��

unknown_16:	�

unknown_17:
��

unknown_18:	�

unknown_19:
��

unknown_20:	�

unknown_21:
��

unknown_22:	�

unknown_23:	�

unknown_24:

unknown_25:	�

unknown_26:

unknown_27:	�

unknown_28:

unknown_29:	�

unknown_30:

unknown_31:

unknown_32:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputs_0inputs_1inputs_2inputs_3unknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10
unknown_11
unknown_12
unknown_13
unknown_14
unknown_15
unknown_16
unknown_17
unknown_18
unknown_19
unknown_20
unknown_21
unknown_22
unknown_23
unknown_24
unknown_25
unknown_26
unknown_27
unknown_28
unknown_29
unknown_30
unknown_31
unknown_32*1
Tin*
(2&*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*D
_read_only_resource_inputs&
$"	
 !"#$%*0
config_proto 

CPU

GPU2*0J 8� *J
fERC
A__inference_model_layer_call_and_return_conditional_losses_7813922
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*�
_input_shapes�
�:����������:����������:����������:����������: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:R N
(
_output_shapes
:����������
"
_user_specified_name
inputs/0:RN
(
_output_shapes
:����������
"
_user_specified_name
inputs/1:RN
(
_output_shapes
:����������
"
_user_specified_name
inputs/2:RN
(
_output_shapes
:����������
"
_user_specified_name
inputs/3
�
�
;__inference_nmn10_isomap_2000_1000_out_layer_call_fn_782930

inputs
unknown:	�
	unknown_0:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *_
fZRX
V__inference_nmn10_isomap_2000_1000_out_layer_call_and_return_conditional_losses_7813402
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�	
&__inference_model_layer_call_fn_781463
nmn10_pca_2000_1000_input
nmn10_tsvd_2000_1000_input 
nmn10_isomap_2000_1000_input
nmn10_lle_2000_1000_input
unknown:
��
	unknown_0:	�
	unknown_1:
��
	unknown_2:	�
	unknown_3:
��
	unknown_4:	�
	unknown_5:
��
	unknown_6:	�
	unknown_7:
��
	unknown_8:	�
	unknown_9:
��

unknown_10:	�

unknown_11:
��

unknown_12:	�

unknown_13:
��

unknown_14:	�

unknown_15:
��

unknown_16:	�

unknown_17:
��

unknown_18:	�

unknown_19:
��

unknown_20:	�

unknown_21:
��

unknown_22:	�

unknown_23:	�

unknown_24:

unknown_25:	�

unknown_26:

unknown_27:	�

unknown_28:

unknown_29:	�

unknown_30:

unknown_31:

unknown_32:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallnmn10_pca_2000_1000_inputnmn10_tsvd_2000_1000_inputnmn10_isomap_2000_1000_inputnmn10_lle_2000_1000_inputunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10
unknown_11
unknown_12
unknown_13
unknown_14
unknown_15
unknown_16
unknown_17
unknown_18
unknown_19
unknown_20
unknown_21
unknown_22
unknown_23
unknown_24
unknown_25
unknown_26
unknown_27
unknown_28
unknown_29
unknown_30
unknown_31
unknown_32*1
Tin*
(2&*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*D
_read_only_resource_inputs&
$"	
 !"#$%*0
config_proto 

CPU

GPU2*0J 8� *J
fERC
A__inference_model_layer_call_and_return_conditional_losses_7813922
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*�
_input_shapes�
�:����������:����������:����������:����������: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:c _
(
_output_shapes
:����������
3
_user_specified_namenmn10_pca_2000_1000_input:d`
(
_output_shapes
:����������
4
_user_specified_namenmn10_tsvd_2000_1000_input:fb
(
_output_shapes
:����������
6
_user_specified_namenmn10_isomap_2000_1000_input:c_
(
_output_shapes
:����������
3
_user_specified_namenmn10_lle_2000_1000_input
�
�
6__inference_nmn10_pca_2000_1000_2_layer_call_fn_782730

inputs
unknown:
��
	unknown_0:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_nmn10_pca_2000_1000_2_layer_call_and_return_conditional_losses_7812212
StatefulPartitionedCall|
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:����������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
Q__inference_nmn10_lle_2000_1000_1_layer_call_and_return_conditional_losses_781102

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
6__inference_nmn10_lle_2000_1000_1_layer_call_fn_782710

inputs
unknown:
��
	unknown_0:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_nmn10_lle_2000_1000_1_layer_call_and_return_conditional_losses_7811022
StatefulPartitionedCall|
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:����������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
8__inference_nmn10_pca_2000_1000_out_layer_call_fn_782890

inputs
unknown:	�
	unknown_0:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *\
fWRU
S__inference_nmn10_pca_2000_1000_out_layer_call_and_return_conditional_losses_7813062
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
��
� 
A__inference_model_layer_call_and_return_conditional_losses_782641
inputs_0
inputs_1
inputs_2
inputs_3H
4nmn10_lle_2000_1000_1_matmul_readvariableop_resource:
��D
5nmn10_lle_2000_1000_1_biasadd_readvariableop_resource:	�K
7nmn10_isomap_2000_1000_1_matmul_readvariableop_resource:
��G
8nmn10_isomap_2000_1000_1_biasadd_readvariableop_resource:	�I
5nmn10_tsvd_2000_1000_1_matmul_readvariableop_resource:
��E
6nmn10_tsvd_2000_1000_1_biasadd_readvariableop_resource:	�H
4nmn10_pca_2000_1000_1_matmul_readvariableop_resource:
��D
5nmn10_pca_2000_1000_1_biasadd_readvariableop_resource:	�H
4nmn10_lle_2000_1000_2_matmul_readvariableop_resource:
��D
5nmn10_lle_2000_1000_2_biasadd_readvariableop_resource:	�K
7nmn10_isomap_2000_1000_2_matmul_readvariableop_resource:
��G
8nmn10_isomap_2000_1000_2_biasadd_readvariableop_resource:	�I
5nmn10_tsvd_2000_1000_2_matmul_readvariableop_resource:
��E
6nmn10_tsvd_2000_1000_2_biasadd_readvariableop_resource:	�H
4nmn10_pca_2000_1000_2_matmul_readvariableop_resource:
��D
5nmn10_pca_2000_1000_2_biasadd_readvariableop_resource:	�H
4nmn10_lle_2000_1000_3_matmul_readvariableop_resource:
��D
5nmn10_lle_2000_1000_3_biasadd_readvariableop_resource:	�K
7nmn10_isomap_2000_1000_3_matmul_readvariableop_resource:
��G
8nmn10_isomap_2000_1000_3_biasadd_readvariableop_resource:	�I
5nmn10_tsvd_2000_1000_3_matmul_readvariableop_resource:
��E
6nmn10_tsvd_2000_1000_3_biasadd_readvariableop_resource:	�H
4nmn10_pca_2000_1000_3_matmul_readvariableop_resource:
��D
5nmn10_pca_2000_1000_3_biasadd_readvariableop_resource:	�I
6nmn10_pca_2000_1000_out_matmul_readvariableop_resource:	�E
7nmn10_pca_2000_1000_out_biasadd_readvariableop_resource:J
7nmn10_tsvd_2000_1000_out_matmul_readvariableop_resource:	�F
8nmn10_tsvd_2000_1000_out_biasadd_readvariableop_resource:L
9nmn10_isomap_2000_1000_out_matmul_readvariableop_resource:	�H
:nmn10_isomap_2000_1000_out_biasadd_readvariableop_resource:I
6nmn10_lle_2000_1000_out_matmul_readvariableop_resource:	�E
7nmn10_lle_2000_1000_out_biasadd_readvariableop_resource:7
%joined_matmul_readvariableop_resource:4
&joined_biasadd_readvariableop_resource:
identity��joined/BiasAdd/ReadVariableOp�joined/MatMul/ReadVariableOp�/nmn10_isomap_2000_1000_1/BiasAdd/ReadVariableOp�.nmn10_isomap_2000_1000_1/MatMul/ReadVariableOp�/nmn10_isomap_2000_1000_2/BiasAdd/ReadVariableOp�.nmn10_isomap_2000_1000_2/MatMul/ReadVariableOp�/nmn10_isomap_2000_1000_3/BiasAdd/ReadVariableOp�.nmn10_isomap_2000_1000_3/MatMul/ReadVariableOp�1nmn10_isomap_2000_1000_out/BiasAdd/ReadVariableOp�0nmn10_isomap_2000_1000_out/MatMul/ReadVariableOp�,nmn10_lle_2000_1000_1/BiasAdd/ReadVariableOp�+nmn10_lle_2000_1000_1/MatMul/ReadVariableOp�,nmn10_lle_2000_1000_2/BiasAdd/ReadVariableOp�+nmn10_lle_2000_1000_2/MatMul/ReadVariableOp�,nmn10_lle_2000_1000_3/BiasAdd/ReadVariableOp�+nmn10_lle_2000_1000_3/MatMul/ReadVariableOp�.nmn10_lle_2000_1000_out/BiasAdd/ReadVariableOp�-nmn10_lle_2000_1000_out/MatMul/ReadVariableOp�,nmn10_pca_2000_1000_1/BiasAdd/ReadVariableOp�+nmn10_pca_2000_1000_1/MatMul/ReadVariableOp�,nmn10_pca_2000_1000_2/BiasAdd/ReadVariableOp�+nmn10_pca_2000_1000_2/MatMul/ReadVariableOp�,nmn10_pca_2000_1000_3/BiasAdd/ReadVariableOp�+nmn10_pca_2000_1000_3/MatMul/ReadVariableOp�.nmn10_pca_2000_1000_out/BiasAdd/ReadVariableOp�-nmn10_pca_2000_1000_out/MatMul/ReadVariableOp�-nmn10_tsvd_2000_1000_1/BiasAdd/ReadVariableOp�,nmn10_tsvd_2000_1000_1/MatMul/ReadVariableOp�-nmn10_tsvd_2000_1000_2/BiasAdd/ReadVariableOp�,nmn10_tsvd_2000_1000_2/MatMul/ReadVariableOp�-nmn10_tsvd_2000_1000_3/BiasAdd/ReadVariableOp�,nmn10_tsvd_2000_1000_3/MatMul/ReadVariableOp�/nmn10_tsvd_2000_1000_out/BiasAdd/ReadVariableOp�.nmn10_tsvd_2000_1000_out/MatMul/ReadVariableOp�
+nmn10_lle_2000_1000_1/MatMul/ReadVariableOpReadVariableOp4nmn10_lle_2000_1000_1_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02-
+nmn10_lle_2000_1000_1/MatMul/ReadVariableOp�
nmn10_lle_2000_1000_1/MatMulMatMulinputs_33nmn10_lle_2000_1000_1/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
nmn10_lle_2000_1000_1/MatMul�
,nmn10_lle_2000_1000_1/BiasAdd/ReadVariableOpReadVariableOp5nmn10_lle_2000_1000_1_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02.
,nmn10_lle_2000_1000_1/BiasAdd/ReadVariableOp�
nmn10_lle_2000_1000_1/BiasAddBiasAdd&nmn10_lle_2000_1000_1/MatMul:product:04nmn10_lle_2000_1000_1/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
nmn10_lle_2000_1000_1/BiasAdd�
nmn10_lle_2000_1000_1/ReluRelu&nmn10_lle_2000_1000_1/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
nmn10_lle_2000_1000_1/Relu�
.nmn10_isomap_2000_1000_1/MatMul/ReadVariableOpReadVariableOp7nmn10_isomap_2000_1000_1_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype020
.nmn10_isomap_2000_1000_1/MatMul/ReadVariableOp�
nmn10_isomap_2000_1000_1/MatMulMatMulinputs_26nmn10_isomap_2000_1000_1/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2!
nmn10_isomap_2000_1000_1/MatMul�
/nmn10_isomap_2000_1000_1/BiasAdd/ReadVariableOpReadVariableOp8nmn10_isomap_2000_1000_1_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype021
/nmn10_isomap_2000_1000_1/BiasAdd/ReadVariableOp�
 nmn10_isomap_2000_1000_1/BiasAddBiasAdd)nmn10_isomap_2000_1000_1/MatMul:product:07nmn10_isomap_2000_1000_1/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2"
 nmn10_isomap_2000_1000_1/BiasAdd�
nmn10_isomap_2000_1000_1/ReluRelu)nmn10_isomap_2000_1000_1/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
nmn10_isomap_2000_1000_1/Relu�
,nmn10_tsvd_2000_1000_1/MatMul/ReadVariableOpReadVariableOp5nmn10_tsvd_2000_1000_1_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02.
,nmn10_tsvd_2000_1000_1/MatMul/ReadVariableOp�
nmn10_tsvd_2000_1000_1/MatMulMatMulinputs_14nmn10_tsvd_2000_1000_1/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
nmn10_tsvd_2000_1000_1/MatMul�
-nmn10_tsvd_2000_1000_1/BiasAdd/ReadVariableOpReadVariableOp6nmn10_tsvd_2000_1000_1_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02/
-nmn10_tsvd_2000_1000_1/BiasAdd/ReadVariableOp�
nmn10_tsvd_2000_1000_1/BiasAddBiasAdd'nmn10_tsvd_2000_1000_1/MatMul:product:05nmn10_tsvd_2000_1000_1/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2 
nmn10_tsvd_2000_1000_1/BiasAdd�
nmn10_tsvd_2000_1000_1/ReluRelu'nmn10_tsvd_2000_1000_1/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
nmn10_tsvd_2000_1000_1/Relu�
+nmn10_pca_2000_1000_1/MatMul/ReadVariableOpReadVariableOp4nmn10_pca_2000_1000_1_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02-
+nmn10_pca_2000_1000_1/MatMul/ReadVariableOp�
nmn10_pca_2000_1000_1/MatMulMatMulinputs_03nmn10_pca_2000_1000_1/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
nmn10_pca_2000_1000_1/MatMul�
,nmn10_pca_2000_1000_1/BiasAdd/ReadVariableOpReadVariableOp5nmn10_pca_2000_1000_1_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02.
,nmn10_pca_2000_1000_1/BiasAdd/ReadVariableOp�
nmn10_pca_2000_1000_1/BiasAddBiasAdd&nmn10_pca_2000_1000_1/MatMul:product:04nmn10_pca_2000_1000_1/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
nmn10_pca_2000_1000_1/BiasAdd�
nmn10_pca_2000_1000_1/ReluRelu&nmn10_pca_2000_1000_1/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
nmn10_pca_2000_1000_1/Relu�
+nmn10_lle_2000_1000_2/MatMul/ReadVariableOpReadVariableOp4nmn10_lle_2000_1000_2_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02-
+nmn10_lle_2000_1000_2/MatMul/ReadVariableOp�
nmn10_lle_2000_1000_2/MatMulMatMul(nmn10_lle_2000_1000_1/Relu:activations:03nmn10_lle_2000_1000_2/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
nmn10_lle_2000_1000_2/MatMul�
,nmn10_lle_2000_1000_2/BiasAdd/ReadVariableOpReadVariableOp5nmn10_lle_2000_1000_2_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02.
,nmn10_lle_2000_1000_2/BiasAdd/ReadVariableOp�
nmn10_lle_2000_1000_2/BiasAddBiasAdd&nmn10_lle_2000_1000_2/MatMul:product:04nmn10_lle_2000_1000_2/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
nmn10_lle_2000_1000_2/BiasAdd�
nmn10_lle_2000_1000_2/ReluRelu&nmn10_lle_2000_1000_2/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
nmn10_lle_2000_1000_2/Relu�
.nmn10_isomap_2000_1000_2/MatMul/ReadVariableOpReadVariableOp7nmn10_isomap_2000_1000_2_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype020
.nmn10_isomap_2000_1000_2/MatMul/ReadVariableOp�
nmn10_isomap_2000_1000_2/MatMulMatMul+nmn10_isomap_2000_1000_1/Relu:activations:06nmn10_isomap_2000_1000_2/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2!
nmn10_isomap_2000_1000_2/MatMul�
/nmn10_isomap_2000_1000_2/BiasAdd/ReadVariableOpReadVariableOp8nmn10_isomap_2000_1000_2_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype021
/nmn10_isomap_2000_1000_2/BiasAdd/ReadVariableOp�
 nmn10_isomap_2000_1000_2/BiasAddBiasAdd)nmn10_isomap_2000_1000_2/MatMul:product:07nmn10_isomap_2000_1000_2/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2"
 nmn10_isomap_2000_1000_2/BiasAdd�
nmn10_isomap_2000_1000_2/ReluRelu)nmn10_isomap_2000_1000_2/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
nmn10_isomap_2000_1000_2/Relu�
,nmn10_tsvd_2000_1000_2/MatMul/ReadVariableOpReadVariableOp5nmn10_tsvd_2000_1000_2_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02.
,nmn10_tsvd_2000_1000_2/MatMul/ReadVariableOp�
nmn10_tsvd_2000_1000_2/MatMulMatMul)nmn10_tsvd_2000_1000_1/Relu:activations:04nmn10_tsvd_2000_1000_2/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
nmn10_tsvd_2000_1000_2/MatMul�
-nmn10_tsvd_2000_1000_2/BiasAdd/ReadVariableOpReadVariableOp6nmn10_tsvd_2000_1000_2_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02/
-nmn10_tsvd_2000_1000_2/BiasAdd/ReadVariableOp�
nmn10_tsvd_2000_1000_2/BiasAddBiasAdd'nmn10_tsvd_2000_1000_2/MatMul:product:05nmn10_tsvd_2000_1000_2/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2 
nmn10_tsvd_2000_1000_2/BiasAdd�
nmn10_tsvd_2000_1000_2/ReluRelu'nmn10_tsvd_2000_1000_2/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
nmn10_tsvd_2000_1000_2/Relu�
+nmn10_pca_2000_1000_2/MatMul/ReadVariableOpReadVariableOp4nmn10_pca_2000_1000_2_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02-
+nmn10_pca_2000_1000_2/MatMul/ReadVariableOp�
nmn10_pca_2000_1000_2/MatMulMatMul(nmn10_pca_2000_1000_1/Relu:activations:03nmn10_pca_2000_1000_2/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
nmn10_pca_2000_1000_2/MatMul�
,nmn10_pca_2000_1000_2/BiasAdd/ReadVariableOpReadVariableOp5nmn10_pca_2000_1000_2_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02.
,nmn10_pca_2000_1000_2/BiasAdd/ReadVariableOp�
nmn10_pca_2000_1000_2/BiasAddBiasAdd&nmn10_pca_2000_1000_2/MatMul:product:04nmn10_pca_2000_1000_2/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
nmn10_pca_2000_1000_2/BiasAdd�
nmn10_pca_2000_1000_2/ReluRelu&nmn10_pca_2000_1000_2/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
nmn10_pca_2000_1000_2/Relu�
+nmn10_lle_2000_1000_3/MatMul/ReadVariableOpReadVariableOp4nmn10_lle_2000_1000_3_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02-
+nmn10_lle_2000_1000_3/MatMul/ReadVariableOp�
nmn10_lle_2000_1000_3/MatMulMatMul(nmn10_lle_2000_1000_2/Relu:activations:03nmn10_lle_2000_1000_3/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
nmn10_lle_2000_1000_3/MatMul�
,nmn10_lle_2000_1000_3/BiasAdd/ReadVariableOpReadVariableOp5nmn10_lle_2000_1000_3_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02.
,nmn10_lle_2000_1000_3/BiasAdd/ReadVariableOp�
nmn10_lle_2000_1000_3/BiasAddBiasAdd&nmn10_lle_2000_1000_3/MatMul:product:04nmn10_lle_2000_1000_3/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
nmn10_lle_2000_1000_3/BiasAdd�
nmn10_lle_2000_1000_3/ReluRelu&nmn10_lle_2000_1000_3/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
nmn10_lle_2000_1000_3/Relu�
.nmn10_isomap_2000_1000_3/MatMul/ReadVariableOpReadVariableOp7nmn10_isomap_2000_1000_3_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype020
.nmn10_isomap_2000_1000_3/MatMul/ReadVariableOp�
nmn10_isomap_2000_1000_3/MatMulMatMul+nmn10_isomap_2000_1000_2/Relu:activations:06nmn10_isomap_2000_1000_3/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2!
nmn10_isomap_2000_1000_3/MatMul�
/nmn10_isomap_2000_1000_3/BiasAdd/ReadVariableOpReadVariableOp8nmn10_isomap_2000_1000_3_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype021
/nmn10_isomap_2000_1000_3/BiasAdd/ReadVariableOp�
 nmn10_isomap_2000_1000_3/BiasAddBiasAdd)nmn10_isomap_2000_1000_3/MatMul:product:07nmn10_isomap_2000_1000_3/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2"
 nmn10_isomap_2000_1000_3/BiasAdd�
nmn10_isomap_2000_1000_3/ReluRelu)nmn10_isomap_2000_1000_3/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
nmn10_isomap_2000_1000_3/Relu�
,nmn10_tsvd_2000_1000_3/MatMul/ReadVariableOpReadVariableOp5nmn10_tsvd_2000_1000_3_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02.
,nmn10_tsvd_2000_1000_3/MatMul/ReadVariableOp�
nmn10_tsvd_2000_1000_3/MatMulMatMul)nmn10_tsvd_2000_1000_2/Relu:activations:04nmn10_tsvd_2000_1000_3/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
nmn10_tsvd_2000_1000_3/MatMul�
-nmn10_tsvd_2000_1000_3/BiasAdd/ReadVariableOpReadVariableOp6nmn10_tsvd_2000_1000_3_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02/
-nmn10_tsvd_2000_1000_3/BiasAdd/ReadVariableOp�
nmn10_tsvd_2000_1000_3/BiasAddBiasAdd'nmn10_tsvd_2000_1000_3/MatMul:product:05nmn10_tsvd_2000_1000_3/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2 
nmn10_tsvd_2000_1000_3/BiasAdd�
nmn10_tsvd_2000_1000_3/ReluRelu'nmn10_tsvd_2000_1000_3/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
nmn10_tsvd_2000_1000_3/Relu�
+nmn10_pca_2000_1000_3/MatMul/ReadVariableOpReadVariableOp4nmn10_pca_2000_1000_3_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02-
+nmn10_pca_2000_1000_3/MatMul/ReadVariableOp�
nmn10_pca_2000_1000_3/MatMulMatMul(nmn10_pca_2000_1000_2/Relu:activations:03nmn10_pca_2000_1000_3/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
nmn10_pca_2000_1000_3/MatMul�
,nmn10_pca_2000_1000_3/BiasAdd/ReadVariableOpReadVariableOp5nmn10_pca_2000_1000_3_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02.
,nmn10_pca_2000_1000_3/BiasAdd/ReadVariableOp�
nmn10_pca_2000_1000_3/BiasAddBiasAdd&nmn10_pca_2000_1000_3/MatMul:product:04nmn10_pca_2000_1000_3/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
nmn10_pca_2000_1000_3/BiasAdd�
nmn10_pca_2000_1000_3/ReluRelu&nmn10_pca_2000_1000_3/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
nmn10_pca_2000_1000_3/Relu�
-nmn10_pca_2000_1000_out/MatMul/ReadVariableOpReadVariableOp6nmn10_pca_2000_1000_out_matmul_readvariableop_resource*
_output_shapes
:	�*
dtype02/
-nmn10_pca_2000_1000_out/MatMul/ReadVariableOp�
nmn10_pca_2000_1000_out/MatMulMatMul(nmn10_pca_2000_1000_3/Relu:activations:05nmn10_pca_2000_1000_out/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2 
nmn10_pca_2000_1000_out/MatMul�
.nmn10_pca_2000_1000_out/BiasAdd/ReadVariableOpReadVariableOp7nmn10_pca_2000_1000_out_biasadd_readvariableop_resource*
_output_shapes
:*
dtype020
.nmn10_pca_2000_1000_out/BiasAdd/ReadVariableOp�
nmn10_pca_2000_1000_out/BiasAddBiasAdd(nmn10_pca_2000_1000_out/MatMul:product:06nmn10_pca_2000_1000_out/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2!
nmn10_pca_2000_1000_out/BiasAdd�
nmn10_pca_2000_1000_out/SigmoidSigmoid(nmn10_pca_2000_1000_out/BiasAdd:output:0*
T0*'
_output_shapes
:���������2!
nmn10_pca_2000_1000_out/Sigmoid�
.nmn10_tsvd_2000_1000_out/MatMul/ReadVariableOpReadVariableOp7nmn10_tsvd_2000_1000_out_matmul_readvariableop_resource*
_output_shapes
:	�*
dtype020
.nmn10_tsvd_2000_1000_out/MatMul/ReadVariableOp�
nmn10_tsvd_2000_1000_out/MatMulMatMul)nmn10_tsvd_2000_1000_3/Relu:activations:06nmn10_tsvd_2000_1000_out/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2!
nmn10_tsvd_2000_1000_out/MatMul�
/nmn10_tsvd_2000_1000_out/BiasAdd/ReadVariableOpReadVariableOp8nmn10_tsvd_2000_1000_out_biasadd_readvariableop_resource*
_output_shapes
:*
dtype021
/nmn10_tsvd_2000_1000_out/BiasAdd/ReadVariableOp�
 nmn10_tsvd_2000_1000_out/BiasAddBiasAdd)nmn10_tsvd_2000_1000_out/MatMul:product:07nmn10_tsvd_2000_1000_out/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2"
 nmn10_tsvd_2000_1000_out/BiasAdd�
 nmn10_tsvd_2000_1000_out/SigmoidSigmoid)nmn10_tsvd_2000_1000_out/BiasAdd:output:0*
T0*'
_output_shapes
:���������2"
 nmn10_tsvd_2000_1000_out/Sigmoid�
0nmn10_isomap_2000_1000_out/MatMul/ReadVariableOpReadVariableOp9nmn10_isomap_2000_1000_out_matmul_readvariableop_resource*
_output_shapes
:	�*
dtype022
0nmn10_isomap_2000_1000_out/MatMul/ReadVariableOp�
!nmn10_isomap_2000_1000_out/MatMulMatMul+nmn10_isomap_2000_1000_3/Relu:activations:08nmn10_isomap_2000_1000_out/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2#
!nmn10_isomap_2000_1000_out/MatMul�
1nmn10_isomap_2000_1000_out/BiasAdd/ReadVariableOpReadVariableOp:nmn10_isomap_2000_1000_out_biasadd_readvariableop_resource*
_output_shapes
:*
dtype023
1nmn10_isomap_2000_1000_out/BiasAdd/ReadVariableOp�
"nmn10_isomap_2000_1000_out/BiasAddBiasAdd+nmn10_isomap_2000_1000_out/MatMul:product:09nmn10_isomap_2000_1000_out/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2$
"nmn10_isomap_2000_1000_out/BiasAdd�
"nmn10_isomap_2000_1000_out/SigmoidSigmoid+nmn10_isomap_2000_1000_out/BiasAdd:output:0*
T0*'
_output_shapes
:���������2$
"nmn10_isomap_2000_1000_out/Sigmoid�
-nmn10_lle_2000_1000_out/MatMul/ReadVariableOpReadVariableOp6nmn10_lle_2000_1000_out_matmul_readvariableop_resource*
_output_shapes
:	�*
dtype02/
-nmn10_lle_2000_1000_out/MatMul/ReadVariableOp�
nmn10_lle_2000_1000_out/MatMulMatMul(nmn10_lle_2000_1000_3/Relu:activations:05nmn10_lle_2000_1000_out/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2 
nmn10_lle_2000_1000_out/MatMul�
.nmn10_lle_2000_1000_out/BiasAdd/ReadVariableOpReadVariableOp7nmn10_lle_2000_1000_out_biasadd_readvariableop_resource*
_output_shapes
:*
dtype020
.nmn10_lle_2000_1000_out/BiasAdd/ReadVariableOp�
nmn10_lle_2000_1000_out/BiasAddBiasAdd(nmn10_lle_2000_1000_out/MatMul:product:06nmn10_lle_2000_1000_out/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2!
nmn10_lle_2000_1000_out/BiasAdd�
nmn10_lle_2000_1000_out/SigmoidSigmoid(nmn10_lle_2000_1000_out/BiasAdd:output:0*
T0*'
_output_shapes
:���������2!
nmn10_lle_2000_1000_out/Sigmoidz
concatenate_24/concat/axisConst*
_output_shapes
: *
dtype0*
value	B :2
concatenate_24/concat/axis�
concatenate_24/concatConcatV2#nmn10_pca_2000_1000_out/Sigmoid:y:0$nmn10_tsvd_2000_1000_out/Sigmoid:y:0&nmn10_isomap_2000_1000_out/Sigmoid:y:0#nmn10_lle_2000_1000_out/Sigmoid:y:0#concatenate_24/concat/axis:output:0*
N*
T0*'
_output_shapes
:���������2
concatenate_24/concat�
joined/MatMul/ReadVariableOpReadVariableOp%joined_matmul_readvariableop_resource*
_output_shapes

:*
dtype02
joined/MatMul/ReadVariableOp�
joined/MatMulMatMulconcatenate_24/concat:output:0$joined/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
joined/MatMul�
joined/BiasAdd/ReadVariableOpReadVariableOp&joined_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02
joined/BiasAdd/ReadVariableOp�
joined/BiasAddBiasAddjoined/MatMul:product:0%joined/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
joined/BiasAddv
joined/SigmoidSigmoidjoined/BiasAdd:output:0*
T0*'
_output_shapes
:���������2
joined/Sigmoidm
IdentityIdentityjoined/Sigmoid:y:0^NoOp*
T0*'
_output_shapes
:���������2

Identity�
NoOpNoOp^joined/BiasAdd/ReadVariableOp^joined/MatMul/ReadVariableOp0^nmn10_isomap_2000_1000_1/BiasAdd/ReadVariableOp/^nmn10_isomap_2000_1000_1/MatMul/ReadVariableOp0^nmn10_isomap_2000_1000_2/BiasAdd/ReadVariableOp/^nmn10_isomap_2000_1000_2/MatMul/ReadVariableOp0^nmn10_isomap_2000_1000_3/BiasAdd/ReadVariableOp/^nmn10_isomap_2000_1000_3/MatMul/ReadVariableOp2^nmn10_isomap_2000_1000_out/BiasAdd/ReadVariableOp1^nmn10_isomap_2000_1000_out/MatMul/ReadVariableOp-^nmn10_lle_2000_1000_1/BiasAdd/ReadVariableOp,^nmn10_lle_2000_1000_1/MatMul/ReadVariableOp-^nmn10_lle_2000_1000_2/BiasAdd/ReadVariableOp,^nmn10_lle_2000_1000_2/MatMul/ReadVariableOp-^nmn10_lle_2000_1000_3/BiasAdd/ReadVariableOp,^nmn10_lle_2000_1000_3/MatMul/ReadVariableOp/^nmn10_lle_2000_1000_out/BiasAdd/ReadVariableOp.^nmn10_lle_2000_1000_out/MatMul/ReadVariableOp-^nmn10_pca_2000_1000_1/BiasAdd/ReadVariableOp,^nmn10_pca_2000_1000_1/MatMul/ReadVariableOp-^nmn10_pca_2000_1000_2/BiasAdd/ReadVariableOp,^nmn10_pca_2000_1000_2/MatMul/ReadVariableOp-^nmn10_pca_2000_1000_3/BiasAdd/ReadVariableOp,^nmn10_pca_2000_1000_3/MatMul/ReadVariableOp/^nmn10_pca_2000_1000_out/BiasAdd/ReadVariableOp.^nmn10_pca_2000_1000_out/MatMul/ReadVariableOp.^nmn10_tsvd_2000_1000_1/BiasAdd/ReadVariableOp-^nmn10_tsvd_2000_1000_1/MatMul/ReadVariableOp.^nmn10_tsvd_2000_1000_2/BiasAdd/ReadVariableOp-^nmn10_tsvd_2000_1000_2/MatMul/ReadVariableOp.^nmn10_tsvd_2000_1000_3/BiasAdd/ReadVariableOp-^nmn10_tsvd_2000_1000_3/MatMul/ReadVariableOp0^nmn10_tsvd_2000_1000_out/BiasAdd/ReadVariableOp/^nmn10_tsvd_2000_1000_out/MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*�
_input_shapes�
�:����������:����������:����������:����������: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 2>
joined/BiasAdd/ReadVariableOpjoined/BiasAdd/ReadVariableOp2<
joined/MatMul/ReadVariableOpjoined/MatMul/ReadVariableOp2b
/nmn10_isomap_2000_1000_1/BiasAdd/ReadVariableOp/nmn10_isomap_2000_1000_1/BiasAdd/ReadVariableOp2`
.nmn10_isomap_2000_1000_1/MatMul/ReadVariableOp.nmn10_isomap_2000_1000_1/MatMul/ReadVariableOp2b
/nmn10_isomap_2000_1000_2/BiasAdd/ReadVariableOp/nmn10_isomap_2000_1000_2/BiasAdd/ReadVariableOp2`
.nmn10_isomap_2000_1000_2/MatMul/ReadVariableOp.nmn10_isomap_2000_1000_2/MatMul/ReadVariableOp2b
/nmn10_isomap_2000_1000_3/BiasAdd/ReadVariableOp/nmn10_isomap_2000_1000_3/BiasAdd/ReadVariableOp2`
.nmn10_isomap_2000_1000_3/MatMul/ReadVariableOp.nmn10_isomap_2000_1000_3/MatMul/ReadVariableOp2f
1nmn10_isomap_2000_1000_out/BiasAdd/ReadVariableOp1nmn10_isomap_2000_1000_out/BiasAdd/ReadVariableOp2d
0nmn10_isomap_2000_1000_out/MatMul/ReadVariableOp0nmn10_isomap_2000_1000_out/MatMul/ReadVariableOp2\
,nmn10_lle_2000_1000_1/BiasAdd/ReadVariableOp,nmn10_lle_2000_1000_1/BiasAdd/ReadVariableOp2Z
+nmn10_lle_2000_1000_1/MatMul/ReadVariableOp+nmn10_lle_2000_1000_1/MatMul/ReadVariableOp2\
,nmn10_lle_2000_1000_2/BiasAdd/ReadVariableOp,nmn10_lle_2000_1000_2/BiasAdd/ReadVariableOp2Z
+nmn10_lle_2000_1000_2/MatMul/ReadVariableOp+nmn10_lle_2000_1000_2/MatMul/ReadVariableOp2\
,nmn10_lle_2000_1000_3/BiasAdd/ReadVariableOp,nmn10_lle_2000_1000_3/BiasAdd/ReadVariableOp2Z
+nmn10_lle_2000_1000_3/MatMul/ReadVariableOp+nmn10_lle_2000_1000_3/MatMul/ReadVariableOp2`
.nmn10_lle_2000_1000_out/BiasAdd/ReadVariableOp.nmn10_lle_2000_1000_out/BiasAdd/ReadVariableOp2^
-nmn10_lle_2000_1000_out/MatMul/ReadVariableOp-nmn10_lle_2000_1000_out/MatMul/ReadVariableOp2\
,nmn10_pca_2000_1000_1/BiasAdd/ReadVariableOp,nmn10_pca_2000_1000_1/BiasAdd/ReadVariableOp2Z
+nmn10_pca_2000_1000_1/MatMul/ReadVariableOp+nmn10_pca_2000_1000_1/MatMul/ReadVariableOp2\
,nmn10_pca_2000_1000_2/BiasAdd/ReadVariableOp,nmn10_pca_2000_1000_2/BiasAdd/ReadVariableOp2Z
+nmn10_pca_2000_1000_2/MatMul/ReadVariableOp+nmn10_pca_2000_1000_2/MatMul/ReadVariableOp2\
,nmn10_pca_2000_1000_3/BiasAdd/ReadVariableOp,nmn10_pca_2000_1000_3/BiasAdd/ReadVariableOp2Z
+nmn10_pca_2000_1000_3/MatMul/ReadVariableOp+nmn10_pca_2000_1000_3/MatMul/ReadVariableOp2`
.nmn10_pca_2000_1000_out/BiasAdd/ReadVariableOp.nmn10_pca_2000_1000_out/BiasAdd/ReadVariableOp2^
-nmn10_pca_2000_1000_out/MatMul/ReadVariableOp-nmn10_pca_2000_1000_out/MatMul/ReadVariableOp2^
-nmn10_tsvd_2000_1000_1/BiasAdd/ReadVariableOp-nmn10_tsvd_2000_1000_1/BiasAdd/ReadVariableOp2\
,nmn10_tsvd_2000_1000_1/MatMul/ReadVariableOp,nmn10_tsvd_2000_1000_1/MatMul/ReadVariableOp2^
-nmn10_tsvd_2000_1000_2/BiasAdd/ReadVariableOp-nmn10_tsvd_2000_1000_2/BiasAdd/ReadVariableOp2\
,nmn10_tsvd_2000_1000_2/MatMul/ReadVariableOp,nmn10_tsvd_2000_1000_2/MatMul/ReadVariableOp2^
-nmn10_tsvd_2000_1000_3/BiasAdd/ReadVariableOp-nmn10_tsvd_2000_1000_3/BiasAdd/ReadVariableOp2\
,nmn10_tsvd_2000_1000_3/MatMul/ReadVariableOp,nmn10_tsvd_2000_1000_3/MatMul/ReadVariableOp2b
/nmn10_tsvd_2000_1000_out/BiasAdd/ReadVariableOp/nmn10_tsvd_2000_1000_out/BiasAdd/ReadVariableOp2`
.nmn10_tsvd_2000_1000_out/MatMul/ReadVariableOp.nmn10_tsvd_2000_1000_out/MatMul/ReadVariableOp:R N
(
_output_shapes
:����������
"
_user_specified_name
inputs/0:RN
(
_output_shapes
:����������
"
_user_specified_name
inputs/1:RN
(
_output_shapes
:����������
"
_user_specified_name
inputs/2:RN
(
_output_shapes
:����������
"
_user_specified_name
inputs/3
��
�
"__inference__traced_restore_783316
file_prefixA
-assignvariableop_nmn10_pca_2000_1000_1_kernel:
��<
-assignvariableop_1_nmn10_pca_2000_1000_1_bias:	�D
0assignvariableop_2_nmn10_tsvd_2000_1000_1_kernel:
��=
.assignvariableop_3_nmn10_tsvd_2000_1000_1_bias:	�F
2assignvariableop_4_nmn10_isomap_2000_1000_1_kernel:
��?
0assignvariableop_5_nmn10_isomap_2000_1000_1_bias:	�C
/assignvariableop_6_nmn10_lle_2000_1000_1_kernel:
��<
-assignvariableop_7_nmn10_lle_2000_1000_1_bias:	�C
/assignvariableop_8_nmn10_pca_2000_1000_2_kernel:
��<
-assignvariableop_9_nmn10_pca_2000_1000_2_bias:	�E
1assignvariableop_10_nmn10_tsvd_2000_1000_2_kernel:
��>
/assignvariableop_11_nmn10_tsvd_2000_1000_2_bias:	�G
3assignvariableop_12_nmn10_isomap_2000_1000_2_kernel:
��@
1assignvariableop_13_nmn10_isomap_2000_1000_2_bias:	�D
0assignvariableop_14_nmn10_lle_2000_1000_2_kernel:
��=
.assignvariableop_15_nmn10_lle_2000_1000_2_bias:	�D
0assignvariableop_16_nmn10_pca_2000_1000_3_kernel:
��=
.assignvariableop_17_nmn10_pca_2000_1000_3_bias:	�E
1assignvariableop_18_nmn10_tsvd_2000_1000_3_kernel:
��>
/assignvariableop_19_nmn10_tsvd_2000_1000_3_bias:	�G
3assignvariableop_20_nmn10_isomap_2000_1000_3_kernel:
��@
1assignvariableop_21_nmn10_isomap_2000_1000_3_bias:	�D
0assignvariableop_22_nmn10_lle_2000_1000_3_kernel:
��=
.assignvariableop_23_nmn10_lle_2000_1000_3_bias:	�E
2assignvariableop_24_nmn10_pca_2000_1000_out_kernel:	�>
0assignvariableop_25_nmn10_pca_2000_1000_out_bias:F
3assignvariableop_26_nmn10_tsvd_2000_1000_out_kernel:	�?
1assignvariableop_27_nmn10_tsvd_2000_1000_out_bias:H
5assignvariableop_28_nmn10_isomap_2000_1000_out_kernel:	�A
3assignvariableop_29_nmn10_isomap_2000_1000_out_bias:E
2assignvariableop_30_nmn10_lle_2000_1000_out_kernel:	�>
0assignvariableop_31_nmn10_lle_2000_1000_out_bias:3
!assignvariableop_32_joined_kernel:-
assignvariableop_33_joined_bias:'
assignvariableop_34_adam_iter:	 )
assignvariableop_35_adam_beta_1: )
assignvariableop_36_adam_beta_2: (
assignvariableop_37_adam_decay: 0
&assignvariableop_38_adam_learning_rate: #
assignvariableop_39_total: #
assignvariableop_40_count: %
assignvariableop_41_total_1: %
assignvariableop_42_count_1: :
(assignvariableop_43_adam_joined_kernel_m:4
&assignvariableop_44_adam_joined_bias_m::
(assignvariableop_45_adam_joined_kernel_v:4
&assignvariableop_46_adam_joined_bias_v:
identity_48��AssignVariableOp�AssignVariableOp_1�AssignVariableOp_10�AssignVariableOp_11�AssignVariableOp_12�AssignVariableOp_13�AssignVariableOp_14�AssignVariableOp_15�AssignVariableOp_16�AssignVariableOp_17�AssignVariableOp_18�AssignVariableOp_19�AssignVariableOp_2�AssignVariableOp_20�AssignVariableOp_21�AssignVariableOp_22�AssignVariableOp_23�AssignVariableOp_24�AssignVariableOp_25�AssignVariableOp_26�AssignVariableOp_27�AssignVariableOp_28�AssignVariableOp_29�AssignVariableOp_3�AssignVariableOp_30�AssignVariableOp_31�AssignVariableOp_32�AssignVariableOp_33�AssignVariableOp_34�AssignVariableOp_35�AssignVariableOp_36�AssignVariableOp_37�AssignVariableOp_38�AssignVariableOp_39�AssignVariableOp_4�AssignVariableOp_40�AssignVariableOp_41�AssignVariableOp_42�AssignVariableOp_43�AssignVariableOp_44�AssignVariableOp_45�AssignVariableOp_46�AssignVariableOp_5�AssignVariableOp_6�AssignVariableOp_7�AssignVariableOp_8�AssignVariableOp_9�
RestoreV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:0*
dtype0*�
value�B�0B6layer_with_weights-0/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-0/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-1/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-1/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-3/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-3/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-4/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-4/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-5/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-5/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-6/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-6/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-7/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-7/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-8/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-8/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-9/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-9/bias/.ATTRIBUTES/VARIABLE_VALUEB7layer_with_weights-10/kernel/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-10/bias/.ATTRIBUTES/VARIABLE_VALUEB7layer_with_weights-11/kernel/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-11/bias/.ATTRIBUTES/VARIABLE_VALUEB7layer_with_weights-12/kernel/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-12/bias/.ATTRIBUTES/VARIABLE_VALUEB7layer_with_weights-13/kernel/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-13/bias/.ATTRIBUTES/VARIABLE_VALUEB7layer_with_weights-14/kernel/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-14/bias/.ATTRIBUTES/VARIABLE_VALUEB7layer_with_weights-15/kernel/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-15/bias/.ATTRIBUTES/VARIABLE_VALUEB7layer_with_weights-16/kernel/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-16/bias/.ATTRIBUTES/VARIABLE_VALUEB)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUEB*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/count/.ATTRIBUTES/VARIABLE_VALUEBSlayer_with_weights-16/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-16/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBSlayer_with_weights-16/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-16/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEB_CHECKPOINTABLE_OBJECT_GRAPH2
RestoreV2/tensor_names�
RestoreV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:0*
dtype0*s
valuejBh0B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B 2
RestoreV2/shape_and_slices�
	RestoreV2	RestoreV2file_prefixRestoreV2/tensor_names:output:0#RestoreV2/shape_and_slices:output:0"/device:CPU:0*�
_output_shapes�
�::::::::::::::::::::::::::::::::::::::::::::::::*>
dtypes4
220	2
	RestoreV2g
IdentityIdentityRestoreV2:tensors:0"/device:CPU:0*
T0*
_output_shapes
:2

Identity�
AssignVariableOpAssignVariableOp-assignvariableop_nmn10_pca_2000_1000_1_kernelIdentity:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOpk

Identity_1IdentityRestoreV2:tensors:1"/device:CPU:0*
T0*
_output_shapes
:2

Identity_1�
AssignVariableOp_1AssignVariableOp-assignvariableop_1_nmn10_pca_2000_1000_1_biasIdentity_1:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_1k

Identity_2IdentityRestoreV2:tensors:2"/device:CPU:0*
T0*
_output_shapes
:2

Identity_2�
AssignVariableOp_2AssignVariableOp0assignvariableop_2_nmn10_tsvd_2000_1000_1_kernelIdentity_2:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_2k

Identity_3IdentityRestoreV2:tensors:3"/device:CPU:0*
T0*
_output_shapes
:2

Identity_3�
AssignVariableOp_3AssignVariableOp.assignvariableop_3_nmn10_tsvd_2000_1000_1_biasIdentity_3:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_3k

Identity_4IdentityRestoreV2:tensors:4"/device:CPU:0*
T0*
_output_shapes
:2

Identity_4�
AssignVariableOp_4AssignVariableOp2assignvariableop_4_nmn10_isomap_2000_1000_1_kernelIdentity_4:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_4k

Identity_5IdentityRestoreV2:tensors:5"/device:CPU:0*
T0*
_output_shapes
:2

Identity_5�
AssignVariableOp_5AssignVariableOp0assignvariableop_5_nmn10_isomap_2000_1000_1_biasIdentity_5:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_5k

Identity_6IdentityRestoreV2:tensors:6"/device:CPU:0*
T0*
_output_shapes
:2

Identity_6�
AssignVariableOp_6AssignVariableOp/assignvariableop_6_nmn10_lle_2000_1000_1_kernelIdentity_6:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_6k

Identity_7IdentityRestoreV2:tensors:7"/device:CPU:0*
T0*
_output_shapes
:2

Identity_7�
AssignVariableOp_7AssignVariableOp-assignvariableop_7_nmn10_lle_2000_1000_1_biasIdentity_7:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_7k

Identity_8IdentityRestoreV2:tensors:8"/device:CPU:0*
T0*
_output_shapes
:2

Identity_8�
AssignVariableOp_8AssignVariableOp/assignvariableop_8_nmn10_pca_2000_1000_2_kernelIdentity_8:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_8k

Identity_9IdentityRestoreV2:tensors:9"/device:CPU:0*
T0*
_output_shapes
:2

Identity_9�
AssignVariableOp_9AssignVariableOp-assignvariableop_9_nmn10_pca_2000_1000_2_biasIdentity_9:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_9n
Identity_10IdentityRestoreV2:tensors:10"/device:CPU:0*
T0*
_output_shapes
:2
Identity_10�
AssignVariableOp_10AssignVariableOp1assignvariableop_10_nmn10_tsvd_2000_1000_2_kernelIdentity_10:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_10n
Identity_11IdentityRestoreV2:tensors:11"/device:CPU:0*
T0*
_output_shapes
:2
Identity_11�
AssignVariableOp_11AssignVariableOp/assignvariableop_11_nmn10_tsvd_2000_1000_2_biasIdentity_11:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_11n
Identity_12IdentityRestoreV2:tensors:12"/device:CPU:0*
T0*
_output_shapes
:2
Identity_12�
AssignVariableOp_12AssignVariableOp3assignvariableop_12_nmn10_isomap_2000_1000_2_kernelIdentity_12:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_12n
Identity_13IdentityRestoreV2:tensors:13"/device:CPU:0*
T0*
_output_shapes
:2
Identity_13�
AssignVariableOp_13AssignVariableOp1assignvariableop_13_nmn10_isomap_2000_1000_2_biasIdentity_13:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_13n
Identity_14IdentityRestoreV2:tensors:14"/device:CPU:0*
T0*
_output_shapes
:2
Identity_14�
AssignVariableOp_14AssignVariableOp0assignvariableop_14_nmn10_lle_2000_1000_2_kernelIdentity_14:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_14n
Identity_15IdentityRestoreV2:tensors:15"/device:CPU:0*
T0*
_output_shapes
:2
Identity_15�
AssignVariableOp_15AssignVariableOp.assignvariableop_15_nmn10_lle_2000_1000_2_biasIdentity_15:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_15n
Identity_16IdentityRestoreV2:tensors:16"/device:CPU:0*
T0*
_output_shapes
:2
Identity_16�
AssignVariableOp_16AssignVariableOp0assignvariableop_16_nmn10_pca_2000_1000_3_kernelIdentity_16:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_16n
Identity_17IdentityRestoreV2:tensors:17"/device:CPU:0*
T0*
_output_shapes
:2
Identity_17�
AssignVariableOp_17AssignVariableOp.assignvariableop_17_nmn10_pca_2000_1000_3_biasIdentity_17:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_17n
Identity_18IdentityRestoreV2:tensors:18"/device:CPU:0*
T0*
_output_shapes
:2
Identity_18�
AssignVariableOp_18AssignVariableOp1assignvariableop_18_nmn10_tsvd_2000_1000_3_kernelIdentity_18:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_18n
Identity_19IdentityRestoreV2:tensors:19"/device:CPU:0*
T0*
_output_shapes
:2
Identity_19�
AssignVariableOp_19AssignVariableOp/assignvariableop_19_nmn10_tsvd_2000_1000_3_biasIdentity_19:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_19n
Identity_20IdentityRestoreV2:tensors:20"/device:CPU:0*
T0*
_output_shapes
:2
Identity_20�
AssignVariableOp_20AssignVariableOp3assignvariableop_20_nmn10_isomap_2000_1000_3_kernelIdentity_20:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_20n
Identity_21IdentityRestoreV2:tensors:21"/device:CPU:0*
T0*
_output_shapes
:2
Identity_21�
AssignVariableOp_21AssignVariableOp1assignvariableop_21_nmn10_isomap_2000_1000_3_biasIdentity_21:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_21n
Identity_22IdentityRestoreV2:tensors:22"/device:CPU:0*
T0*
_output_shapes
:2
Identity_22�
AssignVariableOp_22AssignVariableOp0assignvariableop_22_nmn10_lle_2000_1000_3_kernelIdentity_22:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_22n
Identity_23IdentityRestoreV2:tensors:23"/device:CPU:0*
T0*
_output_shapes
:2
Identity_23�
AssignVariableOp_23AssignVariableOp.assignvariableop_23_nmn10_lle_2000_1000_3_biasIdentity_23:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_23n
Identity_24IdentityRestoreV2:tensors:24"/device:CPU:0*
T0*
_output_shapes
:2
Identity_24�
AssignVariableOp_24AssignVariableOp2assignvariableop_24_nmn10_pca_2000_1000_out_kernelIdentity_24:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_24n
Identity_25IdentityRestoreV2:tensors:25"/device:CPU:0*
T0*
_output_shapes
:2
Identity_25�
AssignVariableOp_25AssignVariableOp0assignvariableop_25_nmn10_pca_2000_1000_out_biasIdentity_25:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_25n
Identity_26IdentityRestoreV2:tensors:26"/device:CPU:0*
T0*
_output_shapes
:2
Identity_26�
AssignVariableOp_26AssignVariableOp3assignvariableop_26_nmn10_tsvd_2000_1000_out_kernelIdentity_26:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_26n
Identity_27IdentityRestoreV2:tensors:27"/device:CPU:0*
T0*
_output_shapes
:2
Identity_27�
AssignVariableOp_27AssignVariableOp1assignvariableop_27_nmn10_tsvd_2000_1000_out_biasIdentity_27:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_27n
Identity_28IdentityRestoreV2:tensors:28"/device:CPU:0*
T0*
_output_shapes
:2
Identity_28�
AssignVariableOp_28AssignVariableOp5assignvariableop_28_nmn10_isomap_2000_1000_out_kernelIdentity_28:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_28n
Identity_29IdentityRestoreV2:tensors:29"/device:CPU:0*
T0*
_output_shapes
:2
Identity_29�
AssignVariableOp_29AssignVariableOp3assignvariableop_29_nmn10_isomap_2000_1000_out_biasIdentity_29:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_29n
Identity_30IdentityRestoreV2:tensors:30"/device:CPU:0*
T0*
_output_shapes
:2
Identity_30�
AssignVariableOp_30AssignVariableOp2assignvariableop_30_nmn10_lle_2000_1000_out_kernelIdentity_30:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_30n
Identity_31IdentityRestoreV2:tensors:31"/device:CPU:0*
T0*
_output_shapes
:2
Identity_31�
AssignVariableOp_31AssignVariableOp0assignvariableop_31_nmn10_lle_2000_1000_out_biasIdentity_31:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_31n
Identity_32IdentityRestoreV2:tensors:32"/device:CPU:0*
T0*
_output_shapes
:2
Identity_32�
AssignVariableOp_32AssignVariableOp!assignvariableop_32_joined_kernelIdentity_32:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_32n
Identity_33IdentityRestoreV2:tensors:33"/device:CPU:0*
T0*
_output_shapes
:2
Identity_33�
AssignVariableOp_33AssignVariableOpassignvariableop_33_joined_biasIdentity_33:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_33n
Identity_34IdentityRestoreV2:tensors:34"/device:CPU:0*
T0	*
_output_shapes
:2
Identity_34�
AssignVariableOp_34AssignVariableOpassignvariableop_34_adam_iterIdentity_34:output:0"/device:CPU:0*
_output_shapes
 *
dtype0	2
AssignVariableOp_34n
Identity_35IdentityRestoreV2:tensors:35"/device:CPU:0*
T0*
_output_shapes
:2
Identity_35�
AssignVariableOp_35AssignVariableOpassignvariableop_35_adam_beta_1Identity_35:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_35n
Identity_36IdentityRestoreV2:tensors:36"/device:CPU:0*
T0*
_output_shapes
:2
Identity_36�
AssignVariableOp_36AssignVariableOpassignvariableop_36_adam_beta_2Identity_36:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_36n
Identity_37IdentityRestoreV2:tensors:37"/device:CPU:0*
T0*
_output_shapes
:2
Identity_37�
AssignVariableOp_37AssignVariableOpassignvariableop_37_adam_decayIdentity_37:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_37n
Identity_38IdentityRestoreV2:tensors:38"/device:CPU:0*
T0*
_output_shapes
:2
Identity_38�
AssignVariableOp_38AssignVariableOp&assignvariableop_38_adam_learning_rateIdentity_38:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_38n
Identity_39IdentityRestoreV2:tensors:39"/device:CPU:0*
T0*
_output_shapes
:2
Identity_39�
AssignVariableOp_39AssignVariableOpassignvariableop_39_totalIdentity_39:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_39n
Identity_40IdentityRestoreV2:tensors:40"/device:CPU:0*
T0*
_output_shapes
:2
Identity_40�
AssignVariableOp_40AssignVariableOpassignvariableop_40_countIdentity_40:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_40n
Identity_41IdentityRestoreV2:tensors:41"/device:CPU:0*
T0*
_output_shapes
:2
Identity_41�
AssignVariableOp_41AssignVariableOpassignvariableop_41_total_1Identity_41:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_41n
Identity_42IdentityRestoreV2:tensors:42"/device:CPU:0*
T0*
_output_shapes
:2
Identity_42�
AssignVariableOp_42AssignVariableOpassignvariableop_42_count_1Identity_42:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_42n
Identity_43IdentityRestoreV2:tensors:43"/device:CPU:0*
T0*
_output_shapes
:2
Identity_43�
AssignVariableOp_43AssignVariableOp(assignvariableop_43_adam_joined_kernel_mIdentity_43:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_43n
Identity_44IdentityRestoreV2:tensors:44"/device:CPU:0*
T0*
_output_shapes
:2
Identity_44�
AssignVariableOp_44AssignVariableOp&assignvariableop_44_adam_joined_bias_mIdentity_44:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_44n
Identity_45IdentityRestoreV2:tensors:45"/device:CPU:0*
T0*
_output_shapes
:2
Identity_45�
AssignVariableOp_45AssignVariableOp(assignvariableop_45_adam_joined_kernel_vIdentity_45:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_45n
Identity_46IdentityRestoreV2:tensors:46"/device:CPU:0*
T0*
_output_shapes
:2
Identity_46�
AssignVariableOp_46AssignVariableOp&assignvariableop_46_adam_joined_bias_vIdentity_46:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_469
NoOpNoOp"/device:CPU:0*
_output_shapes
 2
NoOp�
Identity_47Identityfile_prefix^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_12^AssignVariableOp_13^AssignVariableOp_14^AssignVariableOp_15^AssignVariableOp_16^AssignVariableOp_17^AssignVariableOp_18^AssignVariableOp_19^AssignVariableOp_2^AssignVariableOp_20^AssignVariableOp_21^AssignVariableOp_22^AssignVariableOp_23^AssignVariableOp_24^AssignVariableOp_25^AssignVariableOp_26^AssignVariableOp_27^AssignVariableOp_28^AssignVariableOp_29^AssignVariableOp_3^AssignVariableOp_30^AssignVariableOp_31^AssignVariableOp_32^AssignVariableOp_33^AssignVariableOp_34^AssignVariableOp_35^AssignVariableOp_36^AssignVariableOp_37^AssignVariableOp_38^AssignVariableOp_39^AssignVariableOp_4^AssignVariableOp_40^AssignVariableOp_41^AssignVariableOp_42^AssignVariableOp_43^AssignVariableOp_44^AssignVariableOp_45^AssignVariableOp_46^AssignVariableOp_5^AssignVariableOp_6^AssignVariableOp_7^AssignVariableOp_8^AssignVariableOp_9^NoOp"/device:CPU:0*
T0*
_output_shapes
: 2
Identity_47f
Identity_48IdentityIdentity_47:output:0^NoOp_1*
T0*
_output_shapes
: 2
Identity_48�
NoOp_1NoOp^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_12^AssignVariableOp_13^AssignVariableOp_14^AssignVariableOp_15^AssignVariableOp_16^AssignVariableOp_17^AssignVariableOp_18^AssignVariableOp_19^AssignVariableOp_2^AssignVariableOp_20^AssignVariableOp_21^AssignVariableOp_22^AssignVariableOp_23^AssignVariableOp_24^AssignVariableOp_25^AssignVariableOp_26^AssignVariableOp_27^AssignVariableOp_28^AssignVariableOp_29^AssignVariableOp_3^AssignVariableOp_30^AssignVariableOp_31^AssignVariableOp_32^AssignVariableOp_33^AssignVariableOp_34^AssignVariableOp_35^AssignVariableOp_36^AssignVariableOp_37^AssignVariableOp_38^AssignVariableOp_39^AssignVariableOp_4^AssignVariableOp_40^AssignVariableOp_41^AssignVariableOp_42^AssignVariableOp_43^AssignVariableOp_44^AssignVariableOp_45^AssignVariableOp_46^AssignVariableOp_5^AssignVariableOp_6^AssignVariableOp_7^AssignVariableOp_8^AssignVariableOp_9*"
_acd_function_control_output(*
_output_shapes
 2
NoOp_1"#
identity_48Identity_48:output:0*s
_input_shapesb
`: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 2$
AssignVariableOpAssignVariableOp2(
AssignVariableOp_1AssignVariableOp_12*
AssignVariableOp_10AssignVariableOp_102*
AssignVariableOp_11AssignVariableOp_112*
AssignVariableOp_12AssignVariableOp_122*
AssignVariableOp_13AssignVariableOp_132*
AssignVariableOp_14AssignVariableOp_142*
AssignVariableOp_15AssignVariableOp_152*
AssignVariableOp_16AssignVariableOp_162*
AssignVariableOp_17AssignVariableOp_172*
AssignVariableOp_18AssignVariableOp_182*
AssignVariableOp_19AssignVariableOp_192(
AssignVariableOp_2AssignVariableOp_22*
AssignVariableOp_20AssignVariableOp_202*
AssignVariableOp_21AssignVariableOp_212*
AssignVariableOp_22AssignVariableOp_222*
AssignVariableOp_23AssignVariableOp_232*
AssignVariableOp_24AssignVariableOp_242*
AssignVariableOp_25AssignVariableOp_252*
AssignVariableOp_26AssignVariableOp_262*
AssignVariableOp_27AssignVariableOp_272*
AssignVariableOp_28AssignVariableOp_282*
AssignVariableOp_29AssignVariableOp_292(
AssignVariableOp_3AssignVariableOp_32*
AssignVariableOp_30AssignVariableOp_302*
AssignVariableOp_31AssignVariableOp_312*
AssignVariableOp_32AssignVariableOp_322*
AssignVariableOp_33AssignVariableOp_332*
AssignVariableOp_34AssignVariableOp_342*
AssignVariableOp_35AssignVariableOp_352*
AssignVariableOp_36AssignVariableOp_362*
AssignVariableOp_37AssignVariableOp_372*
AssignVariableOp_38AssignVariableOp_382*
AssignVariableOp_39AssignVariableOp_392(
AssignVariableOp_4AssignVariableOp_42*
AssignVariableOp_40AssignVariableOp_402*
AssignVariableOp_41AssignVariableOp_412*
AssignVariableOp_42AssignVariableOp_422*
AssignVariableOp_43AssignVariableOp_432*
AssignVariableOp_44AssignVariableOp_442*
AssignVariableOp_45AssignVariableOp_452*
AssignVariableOp_46AssignVariableOp_462(
AssignVariableOp_5AssignVariableOp_52(
AssignVariableOp_6AssignVariableOp_62(
AssignVariableOp_7AssignVariableOp_72(
AssignVariableOp_8AssignVariableOp_82(
AssignVariableOp_9AssignVariableOp_9:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix
�
�
7__inference_nmn10_tsvd_2000_1000_2_layer_call_fn_782750

inputs
unknown:
��
	unknown_0:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_nmn10_tsvd_2000_1000_2_layer_call_and_return_conditional_losses_7812042
StatefulPartitionedCall|
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:����������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
6__inference_nmn10_pca_2000_1000_1_layer_call_fn_782650

inputs
unknown:
��
	unknown_0:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_nmn10_pca_2000_1000_1_layer_call_and_return_conditional_losses_7811532
StatefulPartitionedCall|
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:����������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�	
&__inference_model_layer_call_fn_781963
nmn10_pca_2000_1000_input
nmn10_tsvd_2000_1000_input 
nmn10_isomap_2000_1000_input
nmn10_lle_2000_1000_input
unknown:
��
	unknown_0:	�
	unknown_1:
��
	unknown_2:	�
	unknown_3:
��
	unknown_4:	�
	unknown_5:
��
	unknown_6:	�
	unknown_7:
��
	unknown_8:	�
	unknown_9:
��

unknown_10:	�

unknown_11:
��

unknown_12:	�

unknown_13:
��

unknown_14:	�

unknown_15:
��

unknown_16:	�

unknown_17:
��

unknown_18:	�

unknown_19:
��

unknown_20:	�

unknown_21:
��

unknown_22:	�

unknown_23:	�

unknown_24:

unknown_25:	�

unknown_26:

unknown_27:	�

unknown_28:

unknown_29:	�

unknown_30:

unknown_31:

unknown_32:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallnmn10_pca_2000_1000_inputnmn10_tsvd_2000_1000_inputnmn10_isomap_2000_1000_inputnmn10_lle_2000_1000_inputunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10
unknown_11
unknown_12
unknown_13
unknown_14
unknown_15
unknown_16
unknown_17
unknown_18
unknown_19
unknown_20
unknown_21
unknown_22
unknown_23
unknown_24
unknown_25
unknown_26
unknown_27
unknown_28
unknown_29
unknown_30
unknown_31
unknown_32*1
Tin*
(2&*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*D
_read_only_resource_inputs&
$"	
 !"#$%*0
config_proto 

CPU

GPU2*0J 8� *J
fERC
A__inference_model_layer_call_and_return_conditional_losses_7818162
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*�
_input_shapes�
�:����������:����������:����������:����������: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:c _
(
_output_shapes
:����������
3
_user_specified_namenmn10_pca_2000_1000_input:d`
(
_output_shapes
:����������
4
_user_specified_namenmn10_tsvd_2000_1000_input:fb
(
_output_shapes
:����������
6
_user_specified_namenmn10_isomap_2000_1000_input:c_
(
_output_shapes
:����������
3
_user_specified_namenmn10_lle_2000_1000_input
�
�
B__inference_joined_layer_call_and_return_conditional_losses_781385

inputs0
matmul_readvariableop_resource:-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2	
BiasAdda
SigmoidSigmoidBiasAdd:output:0*
T0*'
_output_shapes
:���������2	
Sigmoidf
IdentityIdentitySigmoid:y:0^NoOp*
T0*'
_output_shapes
:���������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs
�
w
/__inference_concatenate_24_layer_call_fn_782969
inputs_0
inputs_1
inputs_2
inputs_3
identity�
PartitionedCallPartitionedCallinputs_0inputs_1inputs_2inputs_3*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *S
fNRL
J__inference_concatenate_24_layer_call_and_return_conditional_losses_7813722
PartitionedCalll
IdentityIdentityPartitionedCall:output:0*
T0*'
_output_shapes
:���������2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*_
_input_shapesN
L:���������:���������:���������:���������:Q M
'
_output_shapes
:���������
"
_user_specified_name
inputs/0:QM
'
_output_shapes
:���������
"
_user_specified_name
inputs/1:QM
'
_output_shapes
:���������
"
_user_specified_name
inputs/2:QM
'
_output_shapes
:���������
"
_user_specified_name
inputs/3
�
�
T__inference_nmn10_tsvd_2000_1000_out_layer_call_and_return_conditional_losses_782921

inputs1
matmul_readvariableop_resource:	�-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes
:	�*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2	
BiasAdda
SigmoidSigmoidBiasAdd:output:0*
T0*'
_output_shapes
:���������2	
Sigmoidf
IdentityIdentitySigmoid:y:0^NoOp*
T0*'
_output_shapes
:���������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
7__inference_nmn10_tsvd_2000_1000_3_layer_call_fn_782830

inputs
unknown:
��
	unknown_0:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_nmn10_tsvd_2000_1000_3_layer_call_and_return_conditional_losses_7812722
StatefulPartitionedCall|
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:����������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�x
�
A__inference_model_layer_call_and_return_conditional_losses_782149
nmn10_pca_2000_1000_input
nmn10_tsvd_2000_1000_input 
nmn10_isomap_2000_1000_input
nmn10_lle_2000_1000_input0
nmn10_lle_2000_1000_1_782062:
��+
nmn10_lle_2000_1000_1_782064:	�3
nmn10_isomap_2000_1000_1_782067:
��.
nmn10_isomap_2000_1000_1_782069:	�1
nmn10_tsvd_2000_1000_1_782072:
��,
nmn10_tsvd_2000_1000_1_782074:	�0
nmn10_pca_2000_1000_1_782077:
��+
nmn10_pca_2000_1000_1_782079:	�0
nmn10_lle_2000_1000_2_782082:
��+
nmn10_lle_2000_1000_2_782084:	�3
nmn10_isomap_2000_1000_2_782087:
��.
nmn10_isomap_2000_1000_2_782089:	�1
nmn10_tsvd_2000_1000_2_782092:
��,
nmn10_tsvd_2000_1000_2_782094:	�0
nmn10_pca_2000_1000_2_782097:
��+
nmn10_pca_2000_1000_2_782099:	�0
nmn10_lle_2000_1000_3_782102:
��+
nmn10_lle_2000_1000_3_782104:	�3
nmn10_isomap_2000_1000_3_782107:
��.
nmn10_isomap_2000_1000_3_782109:	�1
nmn10_tsvd_2000_1000_3_782112:
��,
nmn10_tsvd_2000_1000_3_782114:	�0
nmn10_pca_2000_1000_3_782117:
��+
nmn10_pca_2000_1000_3_782119:	�1
nmn10_pca_2000_1000_out_782122:	�,
nmn10_pca_2000_1000_out_782124:2
nmn10_tsvd_2000_1000_out_782127:	�-
nmn10_tsvd_2000_1000_out_782129:4
!nmn10_isomap_2000_1000_out_782132:	�/
!nmn10_isomap_2000_1000_out_782134:1
nmn10_lle_2000_1000_out_782137:	�,
nmn10_lle_2000_1000_out_782139:
joined_782143:
joined_782145:
identity��joined/StatefulPartitionedCall�0nmn10_isomap_2000_1000_1/StatefulPartitionedCall�0nmn10_isomap_2000_1000_2/StatefulPartitionedCall�0nmn10_isomap_2000_1000_3/StatefulPartitionedCall�2nmn10_isomap_2000_1000_out/StatefulPartitionedCall�-nmn10_lle_2000_1000_1/StatefulPartitionedCall�-nmn10_lle_2000_1000_2/StatefulPartitionedCall�-nmn10_lle_2000_1000_3/StatefulPartitionedCall�/nmn10_lle_2000_1000_out/StatefulPartitionedCall�-nmn10_pca_2000_1000_1/StatefulPartitionedCall�-nmn10_pca_2000_1000_2/StatefulPartitionedCall�-nmn10_pca_2000_1000_3/StatefulPartitionedCall�/nmn10_pca_2000_1000_out/StatefulPartitionedCall�.nmn10_tsvd_2000_1000_1/StatefulPartitionedCall�.nmn10_tsvd_2000_1000_2/StatefulPartitionedCall�.nmn10_tsvd_2000_1000_3/StatefulPartitionedCall�0nmn10_tsvd_2000_1000_out/StatefulPartitionedCall�
-nmn10_lle_2000_1000_1/StatefulPartitionedCallStatefulPartitionedCallnmn10_lle_2000_1000_inputnmn10_lle_2000_1000_1_782062nmn10_lle_2000_1000_1_782064*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_nmn10_lle_2000_1000_1_layer_call_and_return_conditional_losses_7811022/
-nmn10_lle_2000_1000_1/StatefulPartitionedCall�
0nmn10_isomap_2000_1000_1/StatefulPartitionedCallStatefulPartitionedCallnmn10_isomap_2000_1000_inputnmn10_isomap_2000_1000_1_782067nmn10_isomap_2000_1000_1_782069*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *]
fXRV
T__inference_nmn10_isomap_2000_1000_1_layer_call_and_return_conditional_losses_78111922
0nmn10_isomap_2000_1000_1/StatefulPartitionedCall�
.nmn10_tsvd_2000_1000_1/StatefulPartitionedCallStatefulPartitionedCallnmn10_tsvd_2000_1000_inputnmn10_tsvd_2000_1000_1_782072nmn10_tsvd_2000_1000_1_782074*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_nmn10_tsvd_2000_1000_1_layer_call_and_return_conditional_losses_78113620
.nmn10_tsvd_2000_1000_1/StatefulPartitionedCall�
-nmn10_pca_2000_1000_1/StatefulPartitionedCallStatefulPartitionedCallnmn10_pca_2000_1000_inputnmn10_pca_2000_1000_1_782077nmn10_pca_2000_1000_1_782079*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_nmn10_pca_2000_1000_1_layer_call_and_return_conditional_losses_7811532/
-nmn10_pca_2000_1000_1/StatefulPartitionedCall�
-nmn10_lle_2000_1000_2/StatefulPartitionedCallStatefulPartitionedCall6nmn10_lle_2000_1000_1/StatefulPartitionedCall:output:0nmn10_lle_2000_1000_2_782082nmn10_lle_2000_1000_2_782084*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_nmn10_lle_2000_1000_2_layer_call_and_return_conditional_losses_7811702/
-nmn10_lle_2000_1000_2/StatefulPartitionedCall�
0nmn10_isomap_2000_1000_2/StatefulPartitionedCallStatefulPartitionedCall9nmn10_isomap_2000_1000_1/StatefulPartitionedCall:output:0nmn10_isomap_2000_1000_2_782087nmn10_isomap_2000_1000_2_782089*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *]
fXRV
T__inference_nmn10_isomap_2000_1000_2_layer_call_and_return_conditional_losses_78118722
0nmn10_isomap_2000_1000_2/StatefulPartitionedCall�
.nmn10_tsvd_2000_1000_2/StatefulPartitionedCallStatefulPartitionedCall7nmn10_tsvd_2000_1000_1/StatefulPartitionedCall:output:0nmn10_tsvd_2000_1000_2_782092nmn10_tsvd_2000_1000_2_782094*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_nmn10_tsvd_2000_1000_2_layer_call_and_return_conditional_losses_78120420
.nmn10_tsvd_2000_1000_2/StatefulPartitionedCall�
-nmn10_pca_2000_1000_2/StatefulPartitionedCallStatefulPartitionedCall6nmn10_pca_2000_1000_1/StatefulPartitionedCall:output:0nmn10_pca_2000_1000_2_782097nmn10_pca_2000_1000_2_782099*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_nmn10_pca_2000_1000_2_layer_call_and_return_conditional_losses_7812212/
-nmn10_pca_2000_1000_2/StatefulPartitionedCall�
-nmn10_lle_2000_1000_3/StatefulPartitionedCallStatefulPartitionedCall6nmn10_lle_2000_1000_2/StatefulPartitionedCall:output:0nmn10_lle_2000_1000_3_782102nmn10_lle_2000_1000_3_782104*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_nmn10_lle_2000_1000_3_layer_call_and_return_conditional_losses_7812382/
-nmn10_lle_2000_1000_3/StatefulPartitionedCall�
0nmn10_isomap_2000_1000_3/StatefulPartitionedCallStatefulPartitionedCall9nmn10_isomap_2000_1000_2/StatefulPartitionedCall:output:0nmn10_isomap_2000_1000_3_782107nmn10_isomap_2000_1000_3_782109*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *]
fXRV
T__inference_nmn10_isomap_2000_1000_3_layer_call_and_return_conditional_losses_78125522
0nmn10_isomap_2000_1000_3/StatefulPartitionedCall�
.nmn10_tsvd_2000_1000_3/StatefulPartitionedCallStatefulPartitionedCall7nmn10_tsvd_2000_1000_2/StatefulPartitionedCall:output:0nmn10_tsvd_2000_1000_3_782112nmn10_tsvd_2000_1000_3_782114*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_nmn10_tsvd_2000_1000_3_layer_call_and_return_conditional_losses_78127220
.nmn10_tsvd_2000_1000_3/StatefulPartitionedCall�
-nmn10_pca_2000_1000_3/StatefulPartitionedCallStatefulPartitionedCall6nmn10_pca_2000_1000_2/StatefulPartitionedCall:output:0nmn10_pca_2000_1000_3_782117nmn10_pca_2000_1000_3_782119*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_nmn10_pca_2000_1000_3_layer_call_and_return_conditional_losses_7812892/
-nmn10_pca_2000_1000_3/StatefulPartitionedCall�
/nmn10_pca_2000_1000_out/StatefulPartitionedCallStatefulPartitionedCall6nmn10_pca_2000_1000_3/StatefulPartitionedCall:output:0nmn10_pca_2000_1000_out_782122nmn10_pca_2000_1000_out_782124*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *\
fWRU
S__inference_nmn10_pca_2000_1000_out_layer_call_and_return_conditional_losses_78130621
/nmn10_pca_2000_1000_out/StatefulPartitionedCall�
0nmn10_tsvd_2000_1000_out/StatefulPartitionedCallStatefulPartitionedCall7nmn10_tsvd_2000_1000_3/StatefulPartitionedCall:output:0nmn10_tsvd_2000_1000_out_782127nmn10_tsvd_2000_1000_out_782129*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *]
fXRV
T__inference_nmn10_tsvd_2000_1000_out_layer_call_and_return_conditional_losses_78132322
0nmn10_tsvd_2000_1000_out/StatefulPartitionedCall�
2nmn10_isomap_2000_1000_out/StatefulPartitionedCallStatefulPartitionedCall9nmn10_isomap_2000_1000_3/StatefulPartitionedCall:output:0!nmn10_isomap_2000_1000_out_782132!nmn10_isomap_2000_1000_out_782134*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *_
fZRX
V__inference_nmn10_isomap_2000_1000_out_layer_call_and_return_conditional_losses_78134024
2nmn10_isomap_2000_1000_out/StatefulPartitionedCall�
/nmn10_lle_2000_1000_out/StatefulPartitionedCallStatefulPartitionedCall6nmn10_lle_2000_1000_3/StatefulPartitionedCall:output:0nmn10_lle_2000_1000_out_782137nmn10_lle_2000_1000_out_782139*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *\
fWRU
S__inference_nmn10_lle_2000_1000_out_layer_call_and_return_conditional_losses_78135721
/nmn10_lle_2000_1000_out/StatefulPartitionedCall�
concatenate_24/PartitionedCallPartitionedCall8nmn10_pca_2000_1000_out/StatefulPartitionedCall:output:09nmn10_tsvd_2000_1000_out/StatefulPartitionedCall:output:0;nmn10_isomap_2000_1000_out/StatefulPartitionedCall:output:08nmn10_lle_2000_1000_out/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *S
fNRL
J__inference_concatenate_24_layer_call_and_return_conditional_losses_7813722 
concatenate_24/PartitionedCall�
joined/StatefulPartitionedCallStatefulPartitionedCall'concatenate_24/PartitionedCall:output:0joined_782143joined_782145*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *K
fFRD
B__inference_joined_layer_call_and_return_conditional_losses_7813852 
joined/StatefulPartitionedCall�
IdentityIdentity'joined/StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identity�
NoOpNoOp^joined/StatefulPartitionedCall1^nmn10_isomap_2000_1000_1/StatefulPartitionedCall1^nmn10_isomap_2000_1000_2/StatefulPartitionedCall1^nmn10_isomap_2000_1000_3/StatefulPartitionedCall3^nmn10_isomap_2000_1000_out/StatefulPartitionedCall.^nmn10_lle_2000_1000_1/StatefulPartitionedCall.^nmn10_lle_2000_1000_2/StatefulPartitionedCall.^nmn10_lle_2000_1000_3/StatefulPartitionedCall0^nmn10_lle_2000_1000_out/StatefulPartitionedCall.^nmn10_pca_2000_1000_1/StatefulPartitionedCall.^nmn10_pca_2000_1000_2/StatefulPartitionedCall.^nmn10_pca_2000_1000_3/StatefulPartitionedCall0^nmn10_pca_2000_1000_out/StatefulPartitionedCall/^nmn10_tsvd_2000_1000_1/StatefulPartitionedCall/^nmn10_tsvd_2000_1000_2/StatefulPartitionedCall/^nmn10_tsvd_2000_1000_3/StatefulPartitionedCall1^nmn10_tsvd_2000_1000_out/StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*�
_input_shapes�
�:����������:����������:����������:����������: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 2@
joined/StatefulPartitionedCalljoined/StatefulPartitionedCall2d
0nmn10_isomap_2000_1000_1/StatefulPartitionedCall0nmn10_isomap_2000_1000_1/StatefulPartitionedCall2d
0nmn10_isomap_2000_1000_2/StatefulPartitionedCall0nmn10_isomap_2000_1000_2/StatefulPartitionedCall2d
0nmn10_isomap_2000_1000_3/StatefulPartitionedCall0nmn10_isomap_2000_1000_3/StatefulPartitionedCall2h
2nmn10_isomap_2000_1000_out/StatefulPartitionedCall2nmn10_isomap_2000_1000_out/StatefulPartitionedCall2^
-nmn10_lle_2000_1000_1/StatefulPartitionedCall-nmn10_lle_2000_1000_1/StatefulPartitionedCall2^
-nmn10_lle_2000_1000_2/StatefulPartitionedCall-nmn10_lle_2000_1000_2/StatefulPartitionedCall2^
-nmn10_lle_2000_1000_3/StatefulPartitionedCall-nmn10_lle_2000_1000_3/StatefulPartitionedCall2b
/nmn10_lle_2000_1000_out/StatefulPartitionedCall/nmn10_lle_2000_1000_out/StatefulPartitionedCall2^
-nmn10_pca_2000_1000_1/StatefulPartitionedCall-nmn10_pca_2000_1000_1/StatefulPartitionedCall2^
-nmn10_pca_2000_1000_2/StatefulPartitionedCall-nmn10_pca_2000_1000_2/StatefulPartitionedCall2^
-nmn10_pca_2000_1000_3/StatefulPartitionedCall-nmn10_pca_2000_1000_3/StatefulPartitionedCall2b
/nmn10_pca_2000_1000_out/StatefulPartitionedCall/nmn10_pca_2000_1000_out/StatefulPartitionedCall2`
.nmn10_tsvd_2000_1000_1/StatefulPartitionedCall.nmn10_tsvd_2000_1000_1/StatefulPartitionedCall2`
.nmn10_tsvd_2000_1000_2/StatefulPartitionedCall.nmn10_tsvd_2000_1000_2/StatefulPartitionedCall2`
.nmn10_tsvd_2000_1000_3/StatefulPartitionedCall.nmn10_tsvd_2000_1000_3/StatefulPartitionedCall2d
0nmn10_tsvd_2000_1000_out/StatefulPartitionedCall0nmn10_tsvd_2000_1000_out/StatefulPartitionedCall:c _
(
_output_shapes
:����������
3
_user_specified_namenmn10_pca_2000_1000_input:d`
(
_output_shapes
:����������
4
_user_specified_namenmn10_tsvd_2000_1000_input:fb
(
_output_shapes
:����������
6
_user_specified_namenmn10_isomap_2000_1000_input:c_
(
_output_shapes
:����������
3
_user_specified_namenmn10_lle_2000_1000_input
�
�
T__inference_nmn10_isomap_2000_1000_3_layer_call_and_return_conditional_losses_781255

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
T__inference_nmn10_isomap_2000_1000_3_layer_call_and_return_conditional_losses_782861

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
7__inference_nmn10_tsvd_2000_1000_1_layer_call_fn_782670

inputs
unknown:
��
	unknown_0:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_nmn10_tsvd_2000_1000_1_layer_call_and_return_conditional_losses_7811362
StatefulPartitionedCall|
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:����������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
6__inference_nmn10_lle_2000_1000_2_layer_call_fn_782790

inputs
unknown:
��
	unknown_0:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_nmn10_lle_2000_1000_2_layer_call_and_return_conditional_losses_7811702
StatefulPartitionedCall|
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:����������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�v
�
A__inference_model_layer_call_and_return_conditional_losses_781816

inputs
inputs_1
inputs_2
inputs_30
nmn10_lle_2000_1000_1_781729:
��+
nmn10_lle_2000_1000_1_781731:	�3
nmn10_isomap_2000_1000_1_781734:
��.
nmn10_isomap_2000_1000_1_781736:	�1
nmn10_tsvd_2000_1000_1_781739:
��,
nmn10_tsvd_2000_1000_1_781741:	�0
nmn10_pca_2000_1000_1_781744:
��+
nmn10_pca_2000_1000_1_781746:	�0
nmn10_lle_2000_1000_2_781749:
��+
nmn10_lle_2000_1000_2_781751:	�3
nmn10_isomap_2000_1000_2_781754:
��.
nmn10_isomap_2000_1000_2_781756:	�1
nmn10_tsvd_2000_1000_2_781759:
��,
nmn10_tsvd_2000_1000_2_781761:	�0
nmn10_pca_2000_1000_2_781764:
��+
nmn10_pca_2000_1000_2_781766:	�0
nmn10_lle_2000_1000_3_781769:
��+
nmn10_lle_2000_1000_3_781771:	�3
nmn10_isomap_2000_1000_3_781774:
��.
nmn10_isomap_2000_1000_3_781776:	�1
nmn10_tsvd_2000_1000_3_781779:
��,
nmn10_tsvd_2000_1000_3_781781:	�0
nmn10_pca_2000_1000_3_781784:
��+
nmn10_pca_2000_1000_3_781786:	�1
nmn10_pca_2000_1000_out_781789:	�,
nmn10_pca_2000_1000_out_781791:2
nmn10_tsvd_2000_1000_out_781794:	�-
nmn10_tsvd_2000_1000_out_781796:4
!nmn10_isomap_2000_1000_out_781799:	�/
!nmn10_isomap_2000_1000_out_781801:1
nmn10_lle_2000_1000_out_781804:	�,
nmn10_lle_2000_1000_out_781806:
joined_781810:
joined_781812:
identity��joined/StatefulPartitionedCall�0nmn10_isomap_2000_1000_1/StatefulPartitionedCall�0nmn10_isomap_2000_1000_2/StatefulPartitionedCall�0nmn10_isomap_2000_1000_3/StatefulPartitionedCall�2nmn10_isomap_2000_1000_out/StatefulPartitionedCall�-nmn10_lle_2000_1000_1/StatefulPartitionedCall�-nmn10_lle_2000_1000_2/StatefulPartitionedCall�-nmn10_lle_2000_1000_3/StatefulPartitionedCall�/nmn10_lle_2000_1000_out/StatefulPartitionedCall�-nmn10_pca_2000_1000_1/StatefulPartitionedCall�-nmn10_pca_2000_1000_2/StatefulPartitionedCall�-nmn10_pca_2000_1000_3/StatefulPartitionedCall�/nmn10_pca_2000_1000_out/StatefulPartitionedCall�.nmn10_tsvd_2000_1000_1/StatefulPartitionedCall�.nmn10_tsvd_2000_1000_2/StatefulPartitionedCall�.nmn10_tsvd_2000_1000_3/StatefulPartitionedCall�0nmn10_tsvd_2000_1000_out/StatefulPartitionedCall�
-nmn10_lle_2000_1000_1/StatefulPartitionedCallStatefulPartitionedCallinputs_3nmn10_lle_2000_1000_1_781729nmn10_lle_2000_1000_1_781731*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_nmn10_lle_2000_1000_1_layer_call_and_return_conditional_losses_7811022/
-nmn10_lle_2000_1000_1/StatefulPartitionedCall�
0nmn10_isomap_2000_1000_1/StatefulPartitionedCallStatefulPartitionedCallinputs_2nmn10_isomap_2000_1000_1_781734nmn10_isomap_2000_1000_1_781736*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *]
fXRV
T__inference_nmn10_isomap_2000_1000_1_layer_call_and_return_conditional_losses_78111922
0nmn10_isomap_2000_1000_1/StatefulPartitionedCall�
.nmn10_tsvd_2000_1000_1/StatefulPartitionedCallStatefulPartitionedCallinputs_1nmn10_tsvd_2000_1000_1_781739nmn10_tsvd_2000_1000_1_781741*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_nmn10_tsvd_2000_1000_1_layer_call_and_return_conditional_losses_78113620
.nmn10_tsvd_2000_1000_1/StatefulPartitionedCall�
-nmn10_pca_2000_1000_1/StatefulPartitionedCallStatefulPartitionedCallinputsnmn10_pca_2000_1000_1_781744nmn10_pca_2000_1000_1_781746*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_nmn10_pca_2000_1000_1_layer_call_and_return_conditional_losses_7811532/
-nmn10_pca_2000_1000_1/StatefulPartitionedCall�
-nmn10_lle_2000_1000_2/StatefulPartitionedCallStatefulPartitionedCall6nmn10_lle_2000_1000_1/StatefulPartitionedCall:output:0nmn10_lle_2000_1000_2_781749nmn10_lle_2000_1000_2_781751*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_nmn10_lle_2000_1000_2_layer_call_and_return_conditional_losses_7811702/
-nmn10_lle_2000_1000_2/StatefulPartitionedCall�
0nmn10_isomap_2000_1000_2/StatefulPartitionedCallStatefulPartitionedCall9nmn10_isomap_2000_1000_1/StatefulPartitionedCall:output:0nmn10_isomap_2000_1000_2_781754nmn10_isomap_2000_1000_2_781756*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *]
fXRV
T__inference_nmn10_isomap_2000_1000_2_layer_call_and_return_conditional_losses_78118722
0nmn10_isomap_2000_1000_2/StatefulPartitionedCall�
.nmn10_tsvd_2000_1000_2/StatefulPartitionedCallStatefulPartitionedCall7nmn10_tsvd_2000_1000_1/StatefulPartitionedCall:output:0nmn10_tsvd_2000_1000_2_781759nmn10_tsvd_2000_1000_2_781761*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_nmn10_tsvd_2000_1000_2_layer_call_and_return_conditional_losses_78120420
.nmn10_tsvd_2000_1000_2/StatefulPartitionedCall�
-nmn10_pca_2000_1000_2/StatefulPartitionedCallStatefulPartitionedCall6nmn10_pca_2000_1000_1/StatefulPartitionedCall:output:0nmn10_pca_2000_1000_2_781764nmn10_pca_2000_1000_2_781766*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_nmn10_pca_2000_1000_2_layer_call_and_return_conditional_losses_7812212/
-nmn10_pca_2000_1000_2/StatefulPartitionedCall�
-nmn10_lle_2000_1000_3/StatefulPartitionedCallStatefulPartitionedCall6nmn10_lle_2000_1000_2/StatefulPartitionedCall:output:0nmn10_lle_2000_1000_3_781769nmn10_lle_2000_1000_3_781771*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_nmn10_lle_2000_1000_3_layer_call_and_return_conditional_losses_7812382/
-nmn10_lle_2000_1000_3/StatefulPartitionedCall�
0nmn10_isomap_2000_1000_3/StatefulPartitionedCallStatefulPartitionedCall9nmn10_isomap_2000_1000_2/StatefulPartitionedCall:output:0nmn10_isomap_2000_1000_3_781774nmn10_isomap_2000_1000_3_781776*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *]
fXRV
T__inference_nmn10_isomap_2000_1000_3_layer_call_and_return_conditional_losses_78125522
0nmn10_isomap_2000_1000_3/StatefulPartitionedCall�
.nmn10_tsvd_2000_1000_3/StatefulPartitionedCallStatefulPartitionedCall7nmn10_tsvd_2000_1000_2/StatefulPartitionedCall:output:0nmn10_tsvd_2000_1000_3_781779nmn10_tsvd_2000_1000_3_781781*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_nmn10_tsvd_2000_1000_3_layer_call_and_return_conditional_losses_78127220
.nmn10_tsvd_2000_1000_3/StatefulPartitionedCall�
-nmn10_pca_2000_1000_3/StatefulPartitionedCallStatefulPartitionedCall6nmn10_pca_2000_1000_2/StatefulPartitionedCall:output:0nmn10_pca_2000_1000_3_781784nmn10_pca_2000_1000_3_781786*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_nmn10_pca_2000_1000_3_layer_call_and_return_conditional_losses_7812892/
-nmn10_pca_2000_1000_3/StatefulPartitionedCall�
/nmn10_pca_2000_1000_out/StatefulPartitionedCallStatefulPartitionedCall6nmn10_pca_2000_1000_3/StatefulPartitionedCall:output:0nmn10_pca_2000_1000_out_781789nmn10_pca_2000_1000_out_781791*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *\
fWRU
S__inference_nmn10_pca_2000_1000_out_layer_call_and_return_conditional_losses_78130621
/nmn10_pca_2000_1000_out/StatefulPartitionedCall�
0nmn10_tsvd_2000_1000_out/StatefulPartitionedCallStatefulPartitionedCall7nmn10_tsvd_2000_1000_3/StatefulPartitionedCall:output:0nmn10_tsvd_2000_1000_out_781794nmn10_tsvd_2000_1000_out_781796*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *]
fXRV
T__inference_nmn10_tsvd_2000_1000_out_layer_call_and_return_conditional_losses_78132322
0nmn10_tsvd_2000_1000_out/StatefulPartitionedCall�
2nmn10_isomap_2000_1000_out/StatefulPartitionedCallStatefulPartitionedCall9nmn10_isomap_2000_1000_3/StatefulPartitionedCall:output:0!nmn10_isomap_2000_1000_out_781799!nmn10_isomap_2000_1000_out_781801*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *_
fZRX
V__inference_nmn10_isomap_2000_1000_out_layer_call_and_return_conditional_losses_78134024
2nmn10_isomap_2000_1000_out/StatefulPartitionedCall�
/nmn10_lle_2000_1000_out/StatefulPartitionedCallStatefulPartitionedCall6nmn10_lle_2000_1000_3/StatefulPartitionedCall:output:0nmn10_lle_2000_1000_out_781804nmn10_lle_2000_1000_out_781806*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *\
fWRU
S__inference_nmn10_lle_2000_1000_out_layer_call_and_return_conditional_losses_78135721
/nmn10_lle_2000_1000_out/StatefulPartitionedCall�
concatenate_24/PartitionedCallPartitionedCall8nmn10_pca_2000_1000_out/StatefulPartitionedCall:output:09nmn10_tsvd_2000_1000_out/StatefulPartitionedCall:output:0;nmn10_isomap_2000_1000_out/StatefulPartitionedCall:output:08nmn10_lle_2000_1000_out/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *S
fNRL
J__inference_concatenate_24_layer_call_and_return_conditional_losses_7813722 
concatenate_24/PartitionedCall�
joined/StatefulPartitionedCallStatefulPartitionedCall'concatenate_24/PartitionedCall:output:0joined_781810joined_781812*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *K
fFRD
B__inference_joined_layer_call_and_return_conditional_losses_7813852 
joined/StatefulPartitionedCall�
IdentityIdentity'joined/StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identity�
NoOpNoOp^joined/StatefulPartitionedCall1^nmn10_isomap_2000_1000_1/StatefulPartitionedCall1^nmn10_isomap_2000_1000_2/StatefulPartitionedCall1^nmn10_isomap_2000_1000_3/StatefulPartitionedCall3^nmn10_isomap_2000_1000_out/StatefulPartitionedCall.^nmn10_lle_2000_1000_1/StatefulPartitionedCall.^nmn10_lle_2000_1000_2/StatefulPartitionedCall.^nmn10_lle_2000_1000_3/StatefulPartitionedCall0^nmn10_lle_2000_1000_out/StatefulPartitionedCall.^nmn10_pca_2000_1000_1/StatefulPartitionedCall.^nmn10_pca_2000_1000_2/StatefulPartitionedCall.^nmn10_pca_2000_1000_3/StatefulPartitionedCall0^nmn10_pca_2000_1000_out/StatefulPartitionedCall/^nmn10_tsvd_2000_1000_1/StatefulPartitionedCall/^nmn10_tsvd_2000_1000_2/StatefulPartitionedCall/^nmn10_tsvd_2000_1000_3/StatefulPartitionedCall1^nmn10_tsvd_2000_1000_out/StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*�
_input_shapes�
�:����������:����������:����������:����������: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 2@
joined/StatefulPartitionedCalljoined/StatefulPartitionedCall2d
0nmn10_isomap_2000_1000_1/StatefulPartitionedCall0nmn10_isomap_2000_1000_1/StatefulPartitionedCall2d
0nmn10_isomap_2000_1000_2/StatefulPartitionedCall0nmn10_isomap_2000_1000_2/StatefulPartitionedCall2d
0nmn10_isomap_2000_1000_3/StatefulPartitionedCall0nmn10_isomap_2000_1000_3/StatefulPartitionedCall2h
2nmn10_isomap_2000_1000_out/StatefulPartitionedCall2nmn10_isomap_2000_1000_out/StatefulPartitionedCall2^
-nmn10_lle_2000_1000_1/StatefulPartitionedCall-nmn10_lle_2000_1000_1/StatefulPartitionedCall2^
-nmn10_lle_2000_1000_2/StatefulPartitionedCall-nmn10_lle_2000_1000_2/StatefulPartitionedCall2^
-nmn10_lle_2000_1000_3/StatefulPartitionedCall-nmn10_lle_2000_1000_3/StatefulPartitionedCall2b
/nmn10_lle_2000_1000_out/StatefulPartitionedCall/nmn10_lle_2000_1000_out/StatefulPartitionedCall2^
-nmn10_pca_2000_1000_1/StatefulPartitionedCall-nmn10_pca_2000_1000_1/StatefulPartitionedCall2^
-nmn10_pca_2000_1000_2/StatefulPartitionedCall-nmn10_pca_2000_1000_2/StatefulPartitionedCall2^
-nmn10_pca_2000_1000_3/StatefulPartitionedCall-nmn10_pca_2000_1000_3/StatefulPartitionedCall2b
/nmn10_pca_2000_1000_out/StatefulPartitionedCall/nmn10_pca_2000_1000_out/StatefulPartitionedCall2`
.nmn10_tsvd_2000_1000_1/StatefulPartitionedCall.nmn10_tsvd_2000_1000_1/StatefulPartitionedCall2`
.nmn10_tsvd_2000_1000_2/StatefulPartitionedCall.nmn10_tsvd_2000_1000_2/StatefulPartitionedCall2`
.nmn10_tsvd_2000_1000_3/StatefulPartitionedCall.nmn10_tsvd_2000_1000_3/StatefulPartitionedCall2d
0nmn10_tsvd_2000_1000_out/StatefulPartitionedCall0nmn10_tsvd_2000_1000_out/StatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs:PL
(
_output_shapes
:����������
 
_user_specified_nameinputs:PL
(
_output_shapes
:����������
 
_user_specified_nameinputs:PL
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
J__inference_concatenate_24_layer_call_and_return_conditional_losses_782978
inputs_0
inputs_1
inputs_2
inputs_3
identity\
concat/axisConst*
_output_shapes
: *
dtype0*
value	B :2
concat/axis�
concatConcatV2inputs_0inputs_1inputs_2inputs_3concat/axis:output:0*
N*
T0*'
_output_shapes
:���������2
concatc
IdentityIdentityconcat:output:0*
T0*'
_output_shapes
:���������2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*_
_input_shapesN
L:���������:���������:���������:���������:Q M
'
_output_shapes
:���������
"
_user_specified_name
inputs/0:QM
'
_output_shapes
:���������
"
_user_specified_name
inputs/1:QM
'
_output_shapes
:���������
"
_user_specified_name
inputs/2:QM
'
_output_shapes
:���������
"
_user_specified_name
inputs/3
�
�
9__inference_nmn10_isomap_2000_1000_1_layer_call_fn_782690

inputs
unknown:
��
	unknown_0:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *]
fXRV
T__inference_nmn10_isomap_2000_1000_1_layer_call_and_return_conditional_losses_7811192
StatefulPartitionedCall|
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:����������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�a
�
__inference__traced_save_783165
file_prefix;
7savev2_nmn10_pca_2000_1000_1_kernel_read_readvariableop9
5savev2_nmn10_pca_2000_1000_1_bias_read_readvariableop<
8savev2_nmn10_tsvd_2000_1000_1_kernel_read_readvariableop:
6savev2_nmn10_tsvd_2000_1000_1_bias_read_readvariableop>
:savev2_nmn10_isomap_2000_1000_1_kernel_read_readvariableop<
8savev2_nmn10_isomap_2000_1000_1_bias_read_readvariableop;
7savev2_nmn10_lle_2000_1000_1_kernel_read_readvariableop9
5savev2_nmn10_lle_2000_1000_1_bias_read_readvariableop;
7savev2_nmn10_pca_2000_1000_2_kernel_read_readvariableop9
5savev2_nmn10_pca_2000_1000_2_bias_read_readvariableop<
8savev2_nmn10_tsvd_2000_1000_2_kernel_read_readvariableop:
6savev2_nmn10_tsvd_2000_1000_2_bias_read_readvariableop>
:savev2_nmn10_isomap_2000_1000_2_kernel_read_readvariableop<
8savev2_nmn10_isomap_2000_1000_2_bias_read_readvariableop;
7savev2_nmn10_lle_2000_1000_2_kernel_read_readvariableop9
5savev2_nmn10_lle_2000_1000_2_bias_read_readvariableop;
7savev2_nmn10_pca_2000_1000_3_kernel_read_readvariableop9
5savev2_nmn10_pca_2000_1000_3_bias_read_readvariableop<
8savev2_nmn10_tsvd_2000_1000_3_kernel_read_readvariableop:
6savev2_nmn10_tsvd_2000_1000_3_bias_read_readvariableop>
:savev2_nmn10_isomap_2000_1000_3_kernel_read_readvariableop<
8savev2_nmn10_isomap_2000_1000_3_bias_read_readvariableop;
7savev2_nmn10_lle_2000_1000_3_kernel_read_readvariableop9
5savev2_nmn10_lle_2000_1000_3_bias_read_readvariableop=
9savev2_nmn10_pca_2000_1000_out_kernel_read_readvariableop;
7savev2_nmn10_pca_2000_1000_out_bias_read_readvariableop>
:savev2_nmn10_tsvd_2000_1000_out_kernel_read_readvariableop<
8savev2_nmn10_tsvd_2000_1000_out_bias_read_readvariableop@
<savev2_nmn10_isomap_2000_1000_out_kernel_read_readvariableop>
:savev2_nmn10_isomap_2000_1000_out_bias_read_readvariableop=
9savev2_nmn10_lle_2000_1000_out_kernel_read_readvariableop;
7savev2_nmn10_lle_2000_1000_out_bias_read_readvariableop,
(savev2_joined_kernel_read_readvariableop*
&savev2_joined_bias_read_readvariableop(
$savev2_adam_iter_read_readvariableop	*
&savev2_adam_beta_1_read_readvariableop*
&savev2_adam_beta_2_read_readvariableop)
%savev2_adam_decay_read_readvariableop1
-savev2_adam_learning_rate_read_readvariableop$
 savev2_total_read_readvariableop$
 savev2_count_read_readvariableop&
"savev2_total_1_read_readvariableop&
"savev2_count_1_read_readvariableop3
/savev2_adam_joined_kernel_m_read_readvariableop1
-savev2_adam_joined_bias_m_read_readvariableop3
/savev2_adam_joined_kernel_v_read_readvariableop1
-savev2_adam_joined_bias_v_read_readvariableop
savev2_const

identity_1��MergeV2Checkpoints�
StaticRegexFullMatchStaticRegexFullMatchfile_prefix"/device:CPU:**
_output_shapes
: *
pattern
^s3://.*2
StaticRegexFullMatchc
ConstConst"/device:CPU:**
_output_shapes
: *
dtype0*
valueB B.part2
Constl
Const_1Const"/device:CPU:**
_output_shapes
: *
dtype0*
valueB B
_temp/part2	
Const_1�
SelectSelectStaticRegexFullMatch:output:0Const:output:0Const_1:output:0"/device:CPU:**
T0*
_output_shapes
: 2
Selectt

StringJoin
StringJoinfile_prefixSelect:output:0"/device:CPU:**
N*
_output_shapes
: 2

StringJoinZ

num_shardsConst*
_output_shapes
: *
dtype0*
value	B :2

num_shards
ShardedFilename/shardConst"/device:CPU:0*
_output_shapes
: *
dtype0*
value	B : 2
ShardedFilename/shard�
ShardedFilenameShardedFilenameStringJoin:output:0ShardedFilename/shard:output:0num_shards:output:0"/device:CPU:0*
_output_shapes
: 2
ShardedFilename�
SaveV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:0*
dtype0*�
value�B�0B6layer_with_weights-0/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-0/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-1/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-1/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-3/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-3/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-4/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-4/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-5/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-5/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-6/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-6/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-7/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-7/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-8/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-8/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-9/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-9/bias/.ATTRIBUTES/VARIABLE_VALUEB7layer_with_weights-10/kernel/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-10/bias/.ATTRIBUTES/VARIABLE_VALUEB7layer_with_weights-11/kernel/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-11/bias/.ATTRIBUTES/VARIABLE_VALUEB7layer_with_weights-12/kernel/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-12/bias/.ATTRIBUTES/VARIABLE_VALUEB7layer_with_weights-13/kernel/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-13/bias/.ATTRIBUTES/VARIABLE_VALUEB7layer_with_weights-14/kernel/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-14/bias/.ATTRIBUTES/VARIABLE_VALUEB7layer_with_weights-15/kernel/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-15/bias/.ATTRIBUTES/VARIABLE_VALUEB7layer_with_weights-16/kernel/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-16/bias/.ATTRIBUTES/VARIABLE_VALUEB)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUEB*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/count/.ATTRIBUTES/VARIABLE_VALUEBSlayer_with_weights-16/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-16/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBSlayer_with_weights-16/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-16/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEB_CHECKPOINTABLE_OBJECT_GRAPH2
SaveV2/tensor_names�
SaveV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:0*
dtype0*s
valuejBh0B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B 2
SaveV2/shape_and_slices�
SaveV2SaveV2ShardedFilename:filename:0SaveV2/tensor_names:output:0 SaveV2/shape_and_slices:output:07savev2_nmn10_pca_2000_1000_1_kernel_read_readvariableop5savev2_nmn10_pca_2000_1000_1_bias_read_readvariableop8savev2_nmn10_tsvd_2000_1000_1_kernel_read_readvariableop6savev2_nmn10_tsvd_2000_1000_1_bias_read_readvariableop:savev2_nmn10_isomap_2000_1000_1_kernel_read_readvariableop8savev2_nmn10_isomap_2000_1000_1_bias_read_readvariableop7savev2_nmn10_lle_2000_1000_1_kernel_read_readvariableop5savev2_nmn10_lle_2000_1000_1_bias_read_readvariableop7savev2_nmn10_pca_2000_1000_2_kernel_read_readvariableop5savev2_nmn10_pca_2000_1000_2_bias_read_readvariableop8savev2_nmn10_tsvd_2000_1000_2_kernel_read_readvariableop6savev2_nmn10_tsvd_2000_1000_2_bias_read_readvariableop:savev2_nmn10_isomap_2000_1000_2_kernel_read_readvariableop8savev2_nmn10_isomap_2000_1000_2_bias_read_readvariableop7savev2_nmn10_lle_2000_1000_2_kernel_read_readvariableop5savev2_nmn10_lle_2000_1000_2_bias_read_readvariableop7savev2_nmn10_pca_2000_1000_3_kernel_read_readvariableop5savev2_nmn10_pca_2000_1000_3_bias_read_readvariableop8savev2_nmn10_tsvd_2000_1000_3_kernel_read_readvariableop6savev2_nmn10_tsvd_2000_1000_3_bias_read_readvariableop:savev2_nmn10_isomap_2000_1000_3_kernel_read_readvariableop8savev2_nmn10_isomap_2000_1000_3_bias_read_readvariableop7savev2_nmn10_lle_2000_1000_3_kernel_read_readvariableop5savev2_nmn10_lle_2000_1000_3_bias_read_readvariableop9savev2_nmn10_pca_2000_1000_out_kernel_read_readvariableop7savev2_nmn10_pca_2000_1000_out_bias_read_readvariableop:savev2_nmn10_tsvd_2000_1000_out_kernel_read_readvariableop8savev2_nmn10_tsvd_2000_1000_out_bias_read_readvariableop<savev2_nmn10_isomap_2000_1000_out_kernel_read_readvariableop:savev2_nmn10_isomap_2000_1000_out_bias_read_readvariableop9savev2_nmn10_lle_2000_1000_out_kernel_read_readvariableop7savev2_nmn10_lle_2000_1000_out_bias_read_readvariableop(savev2_joined_kernel_read_readvariableop&savev2_joined_bias_read_readvariableop$savev2_adam_iter_read_readvariableop&savev2_adam_beta_1_read_readvariableop&savev2_adam_beta_2_read_readvariableop%savev2_adam_decay_read_readvariableop-savev2_adam_learning_rate_read_readvariableop savev2_total_read_readvariableop savev2_count_read_readvariableop"savev2_total_1_read_readvariableop"savev2_count_1_read_readvariableop/savev2_adam_joined_kernel_m_read_readvariableop-savev2_adam_joined_bias_m_read_readvariableop/savev2_adam_joined_kernel_v_read_readvariableop-savev2_adam_joined_bias_v_read_readvariableopsavev2_const"/device:CPU:0*
_output_shapes
 *>
dtypes4
220	2
SaveV2�
&MergeV2Checkpoints/checkpoint_prefixesPackShardedFilename:filename:0^SaveV2"/device:CPU:0*
N*
T0*
_output_shapes
:2(
&MergeV2Checkpoints/checkpoint_prefixes�
MergeV2CheckpointsMergeV2Checkpoints/MergeV2Checkpoints/checkpoint_prefixes:output:0file_prefix"/device:CPU:0*
_output_shapes
 2
MergeV2Checkpointsr
IdentityIdentityfile_prefix^MergeV2Checkpoints"/device:CPU:0*
T0*
_output_shapes
: 2

Identity_

Identity_1IdentityIdentity:output:0^NoOp*
T0*
_output_shapes
: 2

Identity_1c
NoOpNoOp^MergeV2Checkpoints*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"!

identity_1Identity_1:output:0*�
_input_shapes�
�: :
��:�:
��:�:
��:�:
��:�:
��:�:
��:�:
��:�:
��:�:
��:�:
��:�:
��:�:
��:�:	�::	�::	�::	�:::: : : : : : : : : ::::: 2(
MergeV2CheckpointsMergeV2Checkpoints:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix:&"
 
_output_shapes
:
��:!

_output_shapes	
:�:&"
 
_output_shapes
:
��:!

_output_shapes	
:�:&"
 
_output_shapes
:
��:!

_output_shapes	
:�:&"
 
_output_shapes
:
��:!

_output_shapes	
:�:&	"
 
_output_shapes
:
��:!


_output_shapes	
:�:&"
 
_output_shapes
:
��:!

_output_shapes	
:�:&"
 
_output_shapes
:
��:!

_output_shapes	
:�:&"
 
_output_shapes
:
��:!

_output_shapes	
:�:&"
 
_output_shapes
:
��:!

_output_shapes	
:�:&"
 
_output_shapes
:
��:!

_output_shapes	
:�:&"
 
_output_shapes
:
��:!

_output_shapes	
:�:&"
 
_output_shapes
:
��:!

_output_shapes	
:�:%!

_output_shapes
:	�: 

_output_shapes
::%!

_output_shapes
:	�: 

_output_shapes
::%!

_output_shapes
:	�: 

_output_shapes
::%!

_output_shapes
:	�:  

_output_shapes
::$! 

_output_shapes

:: "

_output_shapes
::#

_output_shapes
: :$

_output_shapes
: :%

_output_shapes
: :&

_output_shapes
: :'

_output_shapes
: :(

_output_shapes
: :)

_output_shapes
: :*

_output_shapes
: :+

_output_shapes
: :$, 

_output_shapes

:: -

_output_shapes
::$. 

_output_shapes

:: /

_output_shapes
::0

_output_shapes
: 
��
� 
A__inference_model_layer_call_and_return_conditional_losses_782513
inputs_0
inputs_1
inputs_2
inputs_3H
4nmn10_lle_2000_1000_1_matmul_readvariableop_resource:
��D
5nmn10_lle_2000_1000_1_biasadd_readvariableop_resource:	�K
7nmn10_isomap_2000_1000_1_matmul_readvariableop_resource:
��G
8nmn10_isomap_2000_1000_1_biasadd_readvariableop_resource:	�I
5nmn10_tsvd_2000_1000_1_matmul_readvariableop_resource:
��E
6nmn10_tsvd_2000_1000_1_biasadd_readvariableop_resource:	�H
4nmn10_pca_2000_1000_1_matmul_readvariableop_resource:
��D
5nmn10_pca_2000_1000_1_biasadd_readvariableop_resource:	�H
4nmn10_lle_2000_1000_2_matmul_readvariableop_resource:
��D
5nmn10_lle_2000_1000_2_biasadd_readvariableop_resource:	�K
7nmn10_isomap_2000_1000_2_matmul_readvariableop_resource:
��G
8nmn10_isomap_2000_1000_2_biasadd_readvariableop_resource:	�I
5nmn10_tsvd_2000_1000_2_matmul_readvariableop_resource:
��E
6nmn10_tsvd_2000_1000_2_biasadd_readvariableop_resource:	�H
4nmn10_pca_2000_1000_2_matmul_readvariableop_resource:
��D
5nmn10_pca_2000_1000_2_biasadd_readvariableop_resource:	�H
4nmn10_lle_2000_1000_3_matmul_readvariableop_resource:
��D
5nmn10_lle_2000_1000_3_biasadd_readvariableop_resource:	�K
7nmn10_isomap_2000_1000_3_matmul_readvariableop_resource:
��G
8nmn10_isomap_2000_1000_3_biasadd_readvariableop_resource:	�I
5nmn10_tsvd_2000_1000_3_matmul_readvariableop_resource:
��E
6nmn10_tsvd_2000_1000_3_biasadd_readvariableop_resource:	�H
4nmn10_pca_2000_1000_3_matmul_readvariableop_resource:
��D
5nmn10_pca_2000_1000_3_biasadd_readvariableop_resource:	�I
6nmn10_pca_2000_1000_out_matmul_readvariableop_resource:	�E
7nmn10_pca_2000_1000_out_biasadd_readvariableop_resource:J
7nmn10_tsvd_2000_1000_out_matmul_readvariableop_resource:	�F
8nmn10_tsvd_2000_1000_out_biasadd_readvariableop_resource:L
9nmn10_isomap_2000_1000_out_matmul_readvariableop_resource:	�H
:nmn10_isomap_2000_1000_out_biasadd_readvariableop_resource:I
6nmn10_lle_2000_1000_out_matmul_readvariableop_resource:	�E
7nmn10_lle_2000_1000_out_biasadd_readvariableop_resource:7
%joined_matmul_readvariableop_resource:4
&joined_biasadd_readvariableop_resource:
identity��joined/BiasAdd/ReadVariableOp�joined/MatMul/ReadVariableOp�/nmn10_isomap_2000_1000_1/BiasAdd/ReadVariableOp�.nmn10_isomap_2000_1000_1/MatMul/ReadVariableOp�/nmn10_isomap_2000_1000_2/BiasAdd/ReadVariableOp�.nmn10_isomap_2000_1000_2/MatMul/ReadVariableOp�/nmn10_isomap_2000_1000_3/BiasAdd/ReadVariableOp�.nmn10_isomap_2000_1000_3/MatMul/ReadVariableOp�1nmn10_isomap_2000_1000_out/BiasAdd/ReadVariableOp�0nmn10_isomap_2000_1000_out/MatMul/ReadVariableOp�,nmn10_lle_2000_1000_1/BiasAdd/ReadVariableOp�+nmn10_lle_2000_1000_1/MatMul/ReadVariableOp�,nmn10_lle_2000_1000_2/BiasAdd/ReadVariableOp�+nmn10_lle_2000_1000_2/MatMul/ReadVariableOp�,nmn10_lle_2000_1000_3/BiasAdd/ReadVariableOp�+nmn10_lle_2000_1000_3/MatMul/ReadVariableOp�.nmn10_lle_2000_1000_out/BiasAdd/ReadVariableOp�-nmn10_lle_2000_1000_out/MatMul/ReadVariableOp�,nmn10_pca_2000_1000_1/BiasAdd/ReadVariableOp�+nmn10_pca_2000_1000_1/MatMul/ReadVariableOp�,nmn10_pca_2000_1000_2/BiasAdd/ReadVariableOp�+nmn10_pca_2000_1000_2/MatMul/ReadVariableOp�,nmn10_pca_2000_1000_3/BiasAdd/ReadVariableOp�+nmn10_pca_2000_1000_3/MatMul/ReadVariableOp�.nmn10_pca_2000_1000_out/BiasAdd/ReadVariableOp�-nmn10_pca_2000_1000_out/MatMul/ReadVariableOp�-nmn10_tsvd_2000_1000_1/BiasAdd/ReadVariableOp�,nmn10_tsvd_2000_1000_1/MatMul/ReadVariableOp�-nmn10_tsvd_2000_1000_2/BiasAdd/ReadVariableOp�,nmn10_tsvd_2000_1000_2/MatMul/ReadVariableOp�-nmn10_tsvd_2000_1000_3/BiasAdd/ReadVariableOp�,nmn10_tsvd_2000_1000_3/MatMul/ReadVariableOp�/nmn10_tsvd_2000_1000_out/BiasAdd/ReadVariableOp�.nmn10_tsvd_2000_1000_out/MatMul/ReadVariableOp�
+nmn10_lle_2000_1000_1/MatMul/ReadVariableOpReadVariableOp4nmn10_lle_2000_1000_1_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02-
+nmn10_lle_2000_1000_1/MatMul/ReadVariableOp�
nmn10_lle_2000_1000_1/MatMulMatMulinputs_33nmn10_lle_2000_1000_1/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
nmn10_lle_2000_1000_1/MatMul�
,nmn10_lle_2000_1000_1/BiasAdd/ReadVariableOpReadVariableOp5nmn10_lle_2000_1000_1_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02.
,nmn10_lle_2000_1000_1/BiasAdd/ReadVariableOp�
nmn10_lle_2000_1000_1/BiasAddBiasAdd&nmn10_lle_2000_1000_1/MatMul:product:04nmn10_lle_2000_1000_1/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
nmn10_lle_2000_1000_1/BiasAdd�
nmn10_lle_2000_1000_1/ReluRelu&nmn10_lle_2000_1000_1/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
nmn10_lle_2000_1000_1/Relu�
.nmn10_isomap_2000_1000_1/MatMul/ReadVariableOpReadVariableOp7nmn10_isomap_2000_1000_1_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype020
.nmn10_isomap_2000_1000_1/MatMul/ReadVariableOp�
nmn10_isomap_2000_1000_1/MatMulMatMulinputs_26nmn10_isomap_2000_1000_1/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2!
nmn10_isomap_2000_1000_1/MatMul�
/nmn10_isomap_2000_1000_1/BiasAdd/ReadVariableOpReadVariableOp8nmn10_isomap_2000_1000_1_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype021
/nmn10_isomap_2000_1000_1/BiasAdd/ReadVariableOp�
 nmn10_isomap_2000_1000_1/BiasAddBiasAdd)nmn10_isomap_2000_1000_1/MatMul:product:07nmn10_isomap_2000_1000_1/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2"
 nmn10_isomap_2000_1000_1/BiasAdd�
nmn10_isomap_2000_1000_1/ReluRelu)nmn10_isomap_2000_1000_1/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
nmn10_isomap_2000_1000_1/Relu�
,nmn10_tsvd_2000_1000_1/MatMul/ReadVariableOpReadVariableOp5nmn10_tsvd_2000_1000_1_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02.
,nmn10_tsvd_2000_1000_1/MatMul/ReadVariableOp�
nmn10_tsvd_2000_1000_1/MatMulMatMulinputs_14nmn10_tsvd_2000_1000_1/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
nmn10_tsvd_2000_1000_1/MatMul�
-nmn10_tsvd_2000_1000_1/BiasAdd/ReadVariableOpReadVariableOp6nmn10_tsvd_2000_1000_1_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02/
-nmn10_tsvd_2000_1000_1/BiasAdd/ReadVariableOp�
nmn10_tsvd_2000_1000_1/BiasAddBiasAdd'nmn10_tsvd_2000_1000_1/MatMul:product:05nmn10_tsvd_2000_1000_1/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2 
nmn10_tsvd_2000_1000_1/BiasAdd�
nmn10_tsvd_2000_1000_1/ReluRelu'nmn10_tsvd_2000_1000_1/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
nmn10_tsvd_2000_1000_1/Relu�
+nmn10_pca_2000_1000_1/MatMul/ReadVariableOpReadVariableOp4nmn10_pca_2000_1000_1_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02-
+nmn10_pca_2000_1000_1/MatMul/ReadVariableOp�
nmn10_pca_2000_1000_1/MatMulMatMulinputs_03nmn10_pca_2000_1000_1/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
nmn10_pca_2000_1000_1/MatMul�
,nmn10_pca_2000_1000_1/BiasAdd/ReadVariableOpReadVariableOp5nmn10_pca_2000_1000_1_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02.
,nmn10_pca_2000_1000_1/BiasAdd/ReadVariableOp�
nmn10_pca_2000_1000_1/BiasAddBiasAdd&nmn10_pca_2000_1000_1/MatMul:product:04nmn10_pca_2000_1000_1/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
nmn10_pca_2000_1000_1/BiasAdd�
nmn10_pca_2000_1000_1/ReluRelu&nmn10_pca_2000_1000_1/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
nmn10_pca_2000_1000_1/Relu�
+nmn10_lle_2000_1000_2/MatMul/ReadVariableOpReadVariableOp4nmn10_lle_2000_1000_2_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02-
+nmn10_lle_2000_1000_2/MatMul/ReadVariableOp�
nmn10_lle_2000_1000_2/MatMulMatMul(nmn10_lle_2000_1000_1/Relu:activations:03nmn10_lle_2000_1000_2/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
nmn10_lle_2000_1000_2/MatMul�
,nmn10_lle_2000_1000_2/BiasAdd/ReadVariableOpReadVariableOp5nmn10_lle_2000_1000_2_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02.
,nmn10_lle_2000_1000_2/BiasAdd/ReadVariableOp�
nmn10_lle_2000_1000_2/BiasAddBiasAdd&nmn10_lle_2000_1000_2/MatMul:product:04nmn10_lle_2000_1000_2/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
nmn10_lle_2000_1000_2/BiasAdd�
nmn10_lle_2000_1000_2/ReluRelu&nmn10_lle_2000_1000_2/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
nmn10_lle_2000_1000_2/Relu�
.nmn10_isomap_2000_1000_2/MatMul/ReadVariableOpReadVariableOp7nmn10_isomap_2000_1000_2_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype020
.nmn10_isomap_2000_1000_2/MatMul/ReadVariableOp�
nmn10_isomap_2000_1000_2/MatMulMatMul+nmn10_isomap_2000_1000_1/Relu:activations:06nmn10_isomap_2000_1000_2/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2!
nmn10_isomap_2000_1000_2/MatMul�
/nmn10_isomap_2000_1000_2/BiasAdd/ReadVariableOpReadVariableOp8nmn10_isomap_2000_1000_2_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype021
/nmn10_isomap_2000_1000_2/BiasAdd/ReadVariableOp�
 nmn10_isomap_2000_1000_2/BiasAddBiasAdd)nmn10_isomap_2000_1000_2/MatMul:product:07nmn10_isomap_2000_1000_2/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2"
 nmn10_isomap_2000_1000_2/BiasAdd�
nmn10_isomap_2000_1000_2/ReluRelu)nmn10_isomap_2000_1000_2/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
nmn10_isomap_2000_1000_2/Relu�
,nmn10_tsvd_2000_1000_2/MatMul/ReadVariableOpReadVariableOp5nmn10_tsvd_2000_1000_2_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02.
,nmn10_tsvd_2000_1000_2/MatMul/ReadVariableOp�
nmn10_tsvd_2000_1000_2/MatMulMatMul)nmn10_tsvd_2000_1000_1/Relu:activations:04nmn10_tsvd_2000_1000_2/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
nmn10_tsvd_2000_1000_2/MatMul�
-nmn10_tsvd_2000_1000_2/BiasAdd/ReadVariableOpReadVariableOp6nmn10_tsvd_2000_1000_2_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02/
-nmn10_tsvd_2000_1000_2/BiasAdd/ReadVariableOp�
nmn10_tsvd_2000_1000_2/BiasAddBiasAdd'nmn10_tsvd_2000_1000_2/MatMul:product:05nmn10_tsvd_2000_1000_2/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2 
nmn10_tsvd_2000_1000_2/BiasAdd�
nmn10_tsvd_2000_1000_2/ReluRelu'nmn10_tsvd_2000_1000_2/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
nmn10_tsvd_2000_1000_2/Relu�
+nmn10_pca_2000_1000_2/MatMul/ReadVariableOpReadVariableOp4nmn10_pca_2000_1000_2_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02-
+nmn10_pca_2000_1000_2/MatMul/ReadVariableOp�
nmn10_pca_2000_1000_2/MatMulMatMul(nmn10_pca_2000_1000_1/Relu:activations:03nmn10_pca_2000_1000_2/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
nmn10_pca_2000_1000_2/MatMul�
,nmn10_pca_2000_1000_2/BiasAdd/ReadVariableOpReadVariableOp5nmn10_pca_2000_1000_2_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02.
,nmn10_pca_2000_1000_2/BiasAdd/ReadVariableOp�
nmn10_pca_2000_1000_2/BiasAddBiasAdd&nmn10_pca_2000_1000_2/MatMul:product:04nmn10_pca_2000_1000_2/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
nmn10_pca_2000_1000_2/BiasAdd�
nmn10_pca_2000_1000_2/ReluRelu&nmn10_pca_2000_1000_2/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
nmn10_pca_2000_1000_2/Relu�
+nmn10_lle_2000_1000_3/MatMul/ReadVariableOpReadVariableOp4nmn10_lle_2000_1000_3_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02-
+nmn10_lle_2000_1000_3/MatMul/ReadVariableOp�
nmn10_lle_2000_1000_3/MatMulMatMul(nmn10_lle_2000_1000_2/Relu:activations:03nmn10_lle_2000_1000_3/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
nmn10_lle_2000_1000_3/MatMul�
,nmn10_lle_2000_1000_3/BiasAdd/ReadVariableOpReadVariableOp5nmn10_lle_2000_1000_3_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02.
,nmn10_lle_2000_1000_3/BiasAdd/ReadVariableOp�
nmn10_lle_2000_1000_3/BiasAddBiasAdd&nmn10_lle_2000_1000_3/MatMul:product:04nmn10_lle_2000_1000_3/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
nmn10_lle_2000_1000_3/BiasAdd�
nmn10_lle_2000_1000_3/ReluRelu&nmn10_lle_2000_1000_3/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
nmn10_lle_2000_1000_3/Relu�
.nmn10_isomap_2000_1000_3/MatMul/ReadVariableOpReadVariableOp7nmn10_isomap_2000_1000_3_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype020
.nmn10_isomap_2000_1000_3/MatMul/ReadVariableOp�
nmn10_isomap_2000_1000_3/MatMulMatMul+nmn10_isomap_2000_1000_2/Relu:activations:06nmn10_isomap_2000_1000_3/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2!
nmn10_isomap_2000_1000_3/MatMul�
/nmn10_isomap_2000_1000_3/BiasAdd/ReadVariableOpReadVariableOp8nmn10_isomap_2000_1000_3_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype021
/nmn10_isomap_2000_1000_3/BiasAdd/ReadVariableOp�
 nmn10_isomap_2000_1000_3/BiasAddBiasAdd)nmn10_isomap_2000_1000_3/MatMul:product:07nmn10_isomap_2000_1000_3/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2"
 nmn10_isomap_2000_1000_3/BiasAdd�
nmn10_isomap_2000_1000_3/ReluRelu)nmn10_isomap_2000_1000_3/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
nmn10_isomap_2000_1000_3/Relu�
,nmn10_tsvd_2000_1000_3/MatMul/ReadVariableOpReadVariableOp5nmn10_tsvd_2000_1000_3_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02.
,nmn10_tsvd_2000_1000_3/MatMul/ReadVariableOp�
nmn10_tsvd_2000_1000_3/MatMulMatMul)nmn10_tsvd_2000_1000_2/Relu:activations:04nmn10_tsvd_2000_1000_3/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
nmn10_tsvd_2000_1000_3/MatMul�
-nmn10_tsvd_2000_1000_3/BiasAdd/ReadVariableOpReadVariableOp6nmn10_tsvd_2000_1000_3_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02/
-nmn10_tsvd_2000_1000_3/BiasAdd/ReadVariableOp�
nmn10_tsvd_2000_1000_3/BiasAddBiasAdd'nmn10_tsvd_2000_1000_3/MatMul:product:05nmn10_tsvd_2000_1000_3/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2 
nmn10_tsvd_2000_1000_3/BiasAdd�
nmn10_tsvd_2000_1000_3/ReluRelu'nmn10_tsvd_2000_1000_3/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
nmn10_tsvd_2000_1000_3/Relu�
+nmn10_pca_2000_1000_3/MatMul/ReadVariableOpReadVariableOp4nmn10_pca_2000_1000_3_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02-
+nmn10_pca_2000_1000_3/MatMul/ReadVariableOp�
nmn10_pca_2000_1000_3/MatMulMatMul(nmn10_pca_2000_1000_2/Relu:activations:03nmn10_pca_2000_1000_3/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
nmn10_pca_2000_1000_3/MatMul�
,nmn10_pca_2000_1000_3/BiasAdd/ReadVariableOpReadVariableOp5nmn10_pca_2000_1000_3_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02.
,nmn10_pca_2000_1000_3/BiasAdd/ReadVariableOp�
nmn10_pca_2000_1000_3/BiasAddBiasAdd&nmn10_pca_2000_1000_3/MatMul:product:04nmn10_pca_2000_1000_3/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
nmn10_pca_2000_1000_3/BiasAdd�
nmn10_pca_2000_1000_3/ReluRelu&nmn10_pca_2000_1000_3/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
nmn10_pca_2000_1000_3/Relu�
-nmn10_pca_2000_1000_out/MatMul/ReadVariableOpReadVariableOp6nmn10_pca_2000_1000_out_matmul_readvariableop_resource*
_output_shapes
:	�*
dtype02/
-nmn10_pca_2000_1000_out/MatMul/ReadVariableOp�
nmn10_pca_2000_1000_out/MatMulMatMul(nmn10_pca_2000_1000_3/Relu:activations:05nmn10_pca_2000_1000_out/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2 
nmn10_pca_2000_1000_out/MatMul�
.nmn10_pca_2000_1000_out/BiasAdd/ReadVariableOpReadVariableOp7nmn10_pca_2000_1000_out_biasadd_readvariableop_resource*
_output_shapes
:*
dtype020
.nmn10_pca_2000_1000_out/BiasAdd/ReadVariableOp�
nmn10_pca_2000_1000_out/BiasAddBiasAdd(nmn10_pca_2000_1000_out/MatMul:product:06nmn10_pca_2000_1000_out/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2!
nmn10_pca_2000_1000_out/BiasAdd�
nmn10_pca_2000_1000_out/SigmoidSigmoid(nmn10_pca_2000_1000_out/BiasAdd:output:0*
T0*'
_output_shapes
:���������2!
nmn10_pca_2000_1000_out/Sigmoid�
.nmn10_tsvd_2000_1000_out/MatMul/ReadVariableOpReadVariableOp7nmn10_tsvd_2000_1000_out_matmul_readvariableop_resource*
_output_shapes
:	�*
dtype020
.nmn10_tsvd_2000_1000_out/MatMul/ReadVariableOp�
nmn10_tsvd_2000_1000_out/MatMulMatMul)nmn10_tsvd_2000_1000_3/Relu:activations:06nmn10_tsvd_2000_1000_out/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2!
nmn10_tsvd_2000_1000_out/MatMul�
/nmn10_tsvd_2000_1000_out/BiasAdd/ReadVariableOpReadVariableOp8nmn10_tsvd_2000_1000_out_biasadd_readvariableop_resource*
_output_shapes
:*
dtype021
/nmn10_tsvd_2000_1000_out/BiasAdd/ReadVariableOp�
 nmn10_tsvd_2000_1000_out/BiasAddBiasAdd)nmn10_tsvd_2000_1000_out/MatMul:product:07nmn10_tsvd_2000_1000_out/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2"
 nmn10_tsvd_2000_1000_out/BiasAdd�
 nmn10_tsvd_2000_1000_out/SigmoidSigmoid)nmn10_tsvd_2000_1000_out/BiasAdd:output:0*
T0*'
_output_shapes
:���������2"
 nmn10_tsvd_2000_1000_out/Sigmoid�
0nmn10_isomap_2000_1000_out/MatMul/ReadVariableOpReadVariableOp9nmn10_isomap_2000_1000_out_matmul_readvariableop_resource*
_output_shapes
:	�*
dtype022
0nmn10_isomap_2000_1000_out/MatMul/ReadVariableOp�
!nmn10_isomap_2000_1000_out/MatMulMatMul+nmn10_isomap_2000_1000_3/Relu:activations:08nmn10_isomap_2000_1000_out/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2#
!nmn10_isomap_2000_1000_out/MatMul�
1nmn10_isomap_2000_1000_out/BiasAdd/ReadVariableOpReadVariableOp:nmn10_isomap_2000_1000_out_biasadd_readvariableop_resource*
_output_shapes
:*
dtype023
1nmn10_isomap_2000_1000_out/BiasAdd/ReadVariableOp�
"nmn10_isomap_2000_1000_out/BiasAddBiasAdd+nmn10_isomap_2000_1000_out/MatMul:product:09nmn10_isomap_2000_1000_out/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2$
"nmn10_isomap_2000_1000_out/BiasAdd�
"nmn10_isomap_2000_1000_out/SigmoidSigmoid+nmn10_isomap_2000_1000_out/BiasAdd:output:0*
T0*'
_output_shapes
:���������2$
"nmn10_isomap_2000_1000_out/Sigmoid�
-nmn10_lle_2000_1000_out/MatMul/ReadVariableOpReadVariableOp6nmn10_lle_2000_1000_out_matmul_readvariableop_resource*
_output_shapes
:	�*
dtype02/
-nmn10_lle_2000_1000_out/MatMul/ReadVariableOp�
nmn10_lle_2000_1000_out/MatMulMatMul(nmn10_lle_2000_1000_3/Relu:activations:05nmn10_lle_2000_1000_out/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2 
nmn10_lle_2000_1000_out/MatMul�
.nmn10_lle_2000_1000_out/BiasAdd/ReadVariableOpReadVariableOp7nmn10_lle_2000_1000_out_biasadd_readvariableop_resource*
_output_shapes
:*
dtype020
.nmn10_lle_2000_1000_out/BiasAdd/ReadVariableOp�
nmn10_lle_2000_1000_out/BiasAddBiasAdd(nmn10_lle_2000_1000_out/MatMul:product:06nmn10_lle_2000_1000_out/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2!
nmn10_lle_2000_1000_out/BiasAdd�
nmn10_lle_2000_1000_out/SigmoidSigmoid(nmn10_lle_2000_1000_out/BiasAdd:output:0*
T0*'
_output_shapes
:���������2!
nmn10_lle_2000_1000_out/Sigmoidz
concatenate_24/concat/axisConst*
_output_shapes
: *
dtype0*
value	B :2
concatenate_24/concat/axis�
concatenate_24/concatConcatV2#nmn10_pca_2000_1000_out/Sigmoid:y:0$nmn10_tsvd_2000_1000_out/Sigmoid:y:0&nmn10_isomap_2000_1000_out/Sigmoid:y:0#nmn10_lle_2000_1000_out/Sigmoid:y:0#concatenate_24/concat/axis:output:0*
N*
T0*'
_output_shapes
:���������2
concatenate_24/concat�
joined/MatMul/ReadVariableOpReadVariableOp%joined_matmul_readvariableop_resource*
_output_shapes

:*
dtype02
joined/MatMul/ReadVariableOp�
joined/MatMulMatMulconcatenate_24/concat:output:0$joined/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
joined/MatMul�
joined/BiasAdd/ReadVariableOpReadVariableOp&joined_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02
joined/BiasAdd/ReadVariableOp�
joined/BiasAddBiasAddjoined/MatMul:product:0%joined/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
joined/BiasAddv
joined/SigmoidSigmoidjoined/BiasAdd:output:0*
T0*'
_output_shapes
:���������2
joined/Sigmoidm
IdentityIdentityjoined/Sigmoid:y:0^NoOp*
T0*'
_output_shapes
:���������2

Identity�
NoOpNoOp^joined/BiasAdd/ReadVariableOp^joined/MatMul/ReadVariableOp0^nmn10_isomap_2000_1000_1/BiasAdd/ReadVariableOp/^nmn10_isomap_2000_1000_1/MatMul/ReadVariableOp0^nmn10_isomap_2000_1000_2/BiasAdd/ReadVariableOp/^nmn10_isomap_2000_1000_2/MatMul/ReadVariableOp0^nmn10_isomap_2000_1000_3/BiasAdd/ReadVariableOp/^nmn10_isomap_2000_1000_3/MatMul/ReadVariableOp2^nmn10_isomap_2000_1000_out/BiasAdd/ReadVariableOp1^nmn10_isomap_2000_1000_out/MatMul/ReadVariableOp-^nmn10_lle_2000_1000_1/BiasAdd/ReadVariableOp,^nmn10_lle_2000_1000_1/MatMul/ReadVariableOp-^nmn10_lle_2000_1000_2/BiasAdd/ReadVariableOp,^nmn10_lle_2000_1000_2/MatMul/ReadVariableOp-^nmn10_lle_2000_1000_3/BiasAdd/ReadVariableOp,^nmn10_lle_2000_1000_3/MatMul/ReadVariableOp/^nmn10_lle_2000_1000_out/BiasAdd/ReadVariableOp.^nmn10_lle_2000_1000_out/MatMul/ReadVariableOp-^nmn10_pca_2000_1000_1/BiasAdd/ReadVariableOp,^nmn10_pca_2000_1000_1/MatMul/ReadVariableOp-^nmn10_pca_2000_1000_2/BiasAdd/ReadVariableOp,^nmn10_pca_2000_1000_2/MatMul/ReadVariableOp-^nmn10_pca_2000_1000_3/BiasAdd/ReadVariableOp,^nmn10_pca_2000_1000_3/MatMul/ReadVariableOp/^nmn10_pca_2000_1000_out/BiasAdd/ReadVariableOp.^nmn10_pca_2000_1000_out/MatMul/ReadVariableOp.^nmn10_tsvd_2000_1000_1/BiasAdd/ReadVariableOp-^nmn10_tsvd_2000_1000_1/MatMul/ReadVariableOp.^nmn10_tsvd_2000_1000_2/BiasAdd/ReadVariableOp-^nmn10_tsvd_2000_1000_2/MatMul/ReadVariableOp.^nmn10_tsvd_2000_1000_3/BiasAdd/ReadVariableOp-^nmn10_tsvd_2000_1000_3/MatMul/ReadVariableOp0^nmn10_tsvd_2000_1000_out/BiasAdd/ReadVariableOp/^nmn10_tsvd_2000_1000_out/MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*�
_input_shapes�
�:����������:����������:����������:����������: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 2>
joined/BiasAdd/ReadVariableOpjoined/BiasAdd/ReadVariableOp2<
joined/MatMul/ReadVariableOpjoined/MatMul/ReadVariableOp2b
/nmn10_isomap_2000_1000_1/BiasAdd/ReadVariableOp/nmn10_isomap_2000_1000_1/BiasAdd/ReadVariableOp2`
.nmn10_isomap_2000_1000_1/MatMul/ReadVariableOp.nmn10_isomap_2000_1000_1/MatMul/ReadVariableOp2b
/nmn10_isomap_2000_1000_2/BiasAdd/ReadVariableOp/nmn10_isomap_2000_1000_2/BiasAdd/ReadVariableOp2`
.nmn10_isomap_2000_1000_2/MatMul/ReadVariableOp.nmn10_isomap_2000_1000_2/MatMul/ReadVariableOp2b
/nmn10_isomap_2000_1000_3/BiasAdd/ReadVariableOp/nmn10_isomap_2000_1000_3/BiasAdd/ReadVariableOp2`
.nmn10_isomap_2000_1000_3/MatMul/ReadVariableOp.nmn10_isomap_2000_1000_3/MatMul/ReadVariableOp2f
1nmn10_isomap_2000_1000_out/BiasAdd/ReadVariableOp1nmn10_isomap_2000_1000_out/BiasAdd/ReadVariableOp2d
0nmn10_isomap_2000_1000_out/MatMul/ReadVariableOp0nmn10_isomap_2000_1000_out/MatMul/ReadVariableOp2\
,nmn10_lle_2000_1000_1/BiasAdd/ReadVariableOp,nmn10_lle_2000_1000_1/BiasAdd/ReadVariableOp2Z
+nmn10_lle_2000_1000_1/MatMul/ReadVariableOp+nmn10_lle_2000_1000_1/MatMul/ReadVariableOp2\
,nmn10_lle_2000_1000_2/BiasAdd/ReadVariableOp,nmn10_lle_2000_1000_2/BiasAdd/ReadVariableOp2Z
+nmn10_lle_2000_1000_2/MatMul/ReadVariableOp+nmn10_lle_2000_1000_2/MatMul/ReadVariableOp2\
,nmn10_lle_2000_1000_3/BiasAdd/ReadVariableOp,nmn10_lle_2000_1000_3/BiasAdd/ReadVariableOp2Z
+nmn10_lle_2000_1000_3/MatMul/ReadVariableOp+nmn10_lle_2000_1000_3/MatMul/ReadVariableOp2`
.nmn10_lle_2000_1000_out/BiasAdd/ReadVariableOp.nmn10_lle_2000_1000_out/BiasAdd/ReadVariableOp2^
-nmn10_lle_2000_1000_out/MatMul/ReadVariableOp-nmn10_lle_2000_1000_out/MatMul/ReadVariableOp2\
,nmn10_pca_2000_1000_1/BiasAdd/ReadVariableOp,nmn10_pca_2000_1000_1/BiasAdd/ReadVariableOp2Z
+nmn10_pca_2000_1000_1/MatMul/ReadVariableOp+nmn10_pca_2000_1000_1/MatMul/ReadVariableOp2\
,nmn10_pca_2000_1000_2/BiasAdd/ReadVariableOp,nmn10_pca_2000_1000_2/BiasAdd/ReadVariableOp2Z
+nmn10_pca_2000_1000_2/MatMul/ReadVariableOp+nmn10_pca_2000_1000_2/MatMul/ReadVariableOp2\
,nmn10_pca_2000_1000_3/BiasAdd/ReadVariableOp,nmn10_pca_2000_1000_3/BiasAdd/ReadVariableOp2Z
+nmn10_pca_2000_1000_3/MatMul/ReadVariableOp+nmn10_pca_2000_1000_3/MatMul/ReadVariableOp2`
.nmn10_pca_2000_1000_out/BiasAdd/ReadVariableOp.nmn10_pca_2000_1000_out/BiasAdd/ReadVariableOp2^
-nmn10_pca_2000_1000_out/MatMul/ReadVariableOp-nmn10_pca_2000_1000_out/MatMul/ReadVariableOp2^
-nmn10_tsvd_2000_1000_1/BiasAdd/ReadVariableOp-nmn10_tsvd_2000_1000_1/BiasAdd/ReadVariableOp2\
,nmn10_tsvd_2000_1000_1/MatMul/ReadVariableOp,nmn10_tsvd_2000_1000_1/MatMul/ReadVariableOp2^
-nmn10_tsvd_2000_1000_2/BiasAdd/ReadVariableOp-nmn10_tsvd_2000_1000_2/BiasAdd/ReadVariableOp2\
,nmn10_tsvd_2000_1000_2/MatMul/ReadVariableOp,nmn10_tsvd_2000_1000_2/MatMul/ReadVariableOp2^
-nmn10_tsvd_2000_1000_3/BiasAdd/ReadVariableOp-nmn10_tsvd_2000_1000_3/BiasAdd/ReadVariableOp2\
,nmn10_tsvd_2000_1000_3/MatMul/ReadVariableOp,nmn10_tsvd_2000_1000_3/MatMul/ReadVariableOp2b
/nmn10_tsvd_2000_1000_out/BiasAdd/ReadVariableOp/nmn10_tsvd_2000_1000_out/BiasAdd/ReadVariableOp2`
.nmn10_tsvd_2000_1000_out/MatMul/ReadVariableOp.nmn10_tsvd_2000_1000_out/MatMul/ReadVariableOp:R N
(
_output_shapes
:����������
"
_user_specified_name
inputs/0:RN
(
_output_shapes
:����������
"
_user_specified_name
inputs/1:RN
(
_output_shapes
:����������
"
_user_specified_name
inputs/2:RN
(
_output_shapes
:����������
"
_user_specified_name
inputs/3
�
�
9__inference_nmn10_isomap_2000_1000_2_layer_call_fn_782770

inputs
unknown:
��
	unknown_0:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *]
fXRV
T__inference_nmn10_isomap_2000_1000_2_layer_call_and_return_conditional_losses_7811872
StatefulPartitionedCall|
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:����������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
Q__inference_nmn10_pca_2000_1000_3_layer_call_and_return_conditional_losses_782821

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
Q__inference_nmn10_lle_2000_1000_2_layer_call_and_return_conditional_losses_782801

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
T__inference_nmn10_isomap_2000_1000_2_layer_call_and_return_conditional_losses_781187

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
8__inference_nmn10_lle_2000_1000_out_layer_call_fn_782950

inputs
unknown:	�
	unknown_0:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *\
fWRU
S__inference_nmn10_lle_2000_1000_out_layer_call_and_return_conditional_losses_7813572
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
Q__inference_nmn10_pca_2000_1000_2_layer_call_and_return_conditional_losses_781221

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
Q__inference_nmn10_lle_2000_1000_2_layer_call_and_return_conditional_losses_781170

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
S__inference_nmn10_lle_2000_1000_out_layer_call_and_return_conditional_losses_782961

inputs1
matmul_readvariableop_resource:	�-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes
:	�*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2	
BiasAdda
SigmoidSigmoidBiasAdd:output:0*
T0*'
_output_shapes
:���������2	
Sigmoidf
IdentityIdentitySigmoid:y:0^NoOp*
T0*'
_output_shapes
:���������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
T__inference_nmn10_isomap_2000_1000_2_layer_call_and_return_conditional_losses_782781

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
S__inference_nmn10_pca_2000_1000_out_layer_call_and_return_conditional_losses_781306

inputs1
matmul_readvariableop_resource:	�-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes
:	�*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2	
BiasAdda
SigmoidSigmoidBiasAdd:output:0*
T0*'
_output_shapes
:���������2	
Sigmoidf
IdentityIdentitySigmoid:y:0^NoOp*
T0*'
_output_shapes
:���������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
��
�$
!__inference__wrapped_model_781078
nmn10_pca_2000_1000_input
nmn10_tsvd_2000_1000_input 
nmn10_isomap_2000_1000_input
nmn10_lle_2000_1000_inputN
:model_nmn10_lle_2000_1000_1_matmul_readvariableop_resource:
��J
;model_nmn10_lle_2000_1000_1_biasadd_readvariableop_resource:	�Q
=model_nmn10_isomap_2000_1000_1_matmul_readvariableop_resource:
��M
>model_nmn10_isomap_2000_1000_1_biasadd_readvariableop_resource:	�O
;model_nmn10_tsvd_2000_1000_1_matmul_readvariableop_resource:
��K
<model_nmn10_tsvd_2000_1000_1_biasadd_readvariableop_resource:	�N
:model_nmn10_pca_2000_1000_1_matmul_readvariableop_resource:
��J
;model_nmn10_pca_2000_1000_1_biasadd_readvariableop_resource:	�N
:model_nmn10_lle_2000_1000_2_matmul_readvariableop_resource:
��J
;model_nmn10_lle_2000_1000_2_biasadd_readvariableop_resource:	�Q
=model_nmn10_isomap_2000_1000_2_matmul_readvariableop_resource:
��M
>model_nmn10_isomap_2000_1000_2_biasadd_readvariableop_resource:	�O
;model_nmn10_tsvd_2000_1000_2_matmul_readvariableop_resource:
��K
<model_nmn10_tsvd_2000_1000_2_biasadd_readvariableop_resource:	�N
:model_nmn10_pca_2000_1000_2_matmul_readvariableop_resource:
��J
;model_nmn10_pca_2000_1000_2_biasadd_readvariableop_resource:	�N
:model_nmn10_lle_2000_1000_3_matmul_readvariableop_resource:
��J
;model_nmn10_lle_2000_1000_3_biasadd_readvariableop_resource:	�Q
=model_nmn10_isomap_2000_1000_3_matmul_readvariableop_resource:
��M
>model_nmn10_isomap_2000_1000_3_biasadd_readvariableop_resource:	�O
;model_nmn10_tsvd_2000_1000_3_matmul_readvariableop_resource:
��K
<model_nmn10_tsvd_2000_1000_3_biasadd_readvariableop_resource:	�N
:model_nmn10_pca_2000_1000_3_matmul_readvariableop_resource:
��J
;model_nmn10_pca_2000_1000_3_biasadd_readvariableop_resource:	�O
<model_nmn10_pca_2000_1000_out_matmul_readvariableop_resource:	�K
=model_nmn10_pca_2000_1000_out_biasadd_readvariableop_resource:P
=model_nmn10_tsvd_2000_1000_out_matmul_readvariableop_resource:	�L
>model_nmn10_tsvd_2000_1000_out_biasadd_readvariableop_resource:R
?model_nmn10_isomap_2000_1000_out_matmul_readvariableop_resource:	�N
@model_nmn10_isomap_2000_1000_out_biasadd_readvariableop_resource:O
<model_nmn10_lle_2000_1000_out_matmul_readvariableop_resource:	�K
=model_nmn10_lle_2000_1000_out_biasadd_readvariableop_resource:=
+model_joined_matmul_readvariableop_resource::
,model_joined_biasadd_readvariableop_resource:
identity��#model/joined/BiasAdd/ReadVariableOp�"model/joined/MatMul/ReadVariableOp�5model/nmn10_isomap_2000_1000_1/BiasAdd/ReadVariableOp�4model/nmn10_isomap_2000_1000_1/MatMul/ReadVariableOp�5model/nmn10_isomap_2000_1000_2/BiasAdd/ReadVariableOp�4model/nmn10_isomap_2000_1000_2/MatMul/ReadVariableOp�5model/nmn10_isomap_2000_1000_3/BiasAdd/ReadVariableOp�4model/nmn10_isomap_2000_1000_3/MatMul/ReadVariableOp�7model/nmn10_isomap_2000_1000_out/BiasAdd/ReadVariableOp�6model/nmn10_isomap_2000_1000_out/MatMul/ReadVariableOp�2model/nmn10_lle_2000_1000_1/BiasAdd/ReadVariableOp�1model/nmn10_lle_2000_1000_1/MatMul/ReadVariableOp�2model/nmn10_lle_2000_1000_2/BiasAdd/ReadVariableOp�1model/nmn10_lle_2000_1000_2/MatMul/ReadVariableOp�2model/nmn10_lle_2000_1000_3/BiasAdd/ReadVariableOp�1model/nmn10_lle_2000_1000_3/MatMul/ReadVariableOp�4model/nmn10_lle_2000_1000_out/BiasAdd/ReadVariableOp�3model/nmn10_lle_2000_1000_out/MatMul/ReadVariableOp�2model/nmn10_pca_2000_1000_1/BiasAdd/ReadVariableOp�1model/nmn10_pca_2000_1000_1/MatMul/ReadVariableOp�2model/nmn10_pca_2000_1000_2/BiasAdd/ReadVariableOp�1model/nmn10_pca_2000_1000_2/MatMul/ReadVariableOp�2model/nmn10_pca_2000_1000_3/BiasAdd/ReadVariableOp�1model/nmn10_pca_2000_1000_3/MatMul/ReadVariableOp�4model/nmn10_pca_2000_1000_out/BiasAdd/ReadVariableOp�3model/nmn10_pca_2000_1000_out/MatMul/ReadVariableOp�3model/nmn10_tsvd_2000_1000_1/BiasAdd/ReadVariableOp�2model/nmn10_tsvd_2000_1000_1/MatMul/ReadVariableOp�3model/nmn10_tsvd_2000_1000_2/BiasAdd/ReadVariableOp�2model/nmn10_tsvd_2000_1000_2/MatMul/ReadVariableOp�3model/nmn10_tsvd_2000_1000_3/BiasAdd/ReadVariableOp�2model/nmn10_tsvd_2000_1000_3/MatMul/ReadVariableOp�5model/nmn10_tsvd_2000_1000_out/BiasAdd/ReadVariableOp�4model/nmn10_tsvd_2000_1000_out/MatMul/ReadVariableOp�
1model/nmn10_lle_2000_1000_1/MatMul/ReadVariableOpReadVariableOp:model_nmn10_lle_2000_1000_1_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype023
1model/nmn10_lle_2000_1000_1/MatMul/ReadVariableOp�
"model/nmn10_lle_2000_1000_1/MatMulMatMulnmn10_lle_2000_1000_input9model/nmn10_lle_2000_1000_1/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2$
"model/nmn10_lle_2000_1000_1/MatMul�
2model/nmn10_lle_2000_1000_1/BiasAdd/ReadVariableOpReadVariableOp;model_nmn10_lle_2000_1000_1_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype024
2model/nmn10_lle_2000_1000_1/BiasAdd/ReadVariableOp�
#model/nmn10_lle_2000_1000_1/BiasAddBiasAdd,model/nmn10_lle_2000_1000_1/MatMul:product:0:model/nmn10_lle_2000_1000_1/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2%
#model/nmn10_lle_2000_1000_1/BiasAdd�
 model/nmn10_lle_2000_1000_1/ReluRelu,model/nmn10_lle_2000_1000_1/BiasAdd:output:0*
T0*(
_output_shapes
:����������2"
 model/nmn10_lle_2000_1000_1/Relu�
4model/nmn10_isomap_2000_1000_1/MatMul/ReadVariableOpReadVariableOp=model_nmn10_isomap_2000_1000_1_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype026
4model/nmn10_isomap_2000_1000_1/MatMul/ReadVariableOp�
%model/nmn10_isomap_2000_1000_1/MatMulMatMulnmn10_isomap_2000_1000_input<model/nmn10_isomap_2000_1000_1/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2'
%model/nmn10_isomap_2000_1000_1/MatMul�
5model/nmn10_isomap_2000_1000_1/BiasAdd/ReadVariableOpReadVariableOp>model_nmn10_isomap_2000_1000_1_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype027
5model/nmn10_isomap_2000_1000_1/BiasAdd/ReadVariableOp�
&model/nmn10_isomap_2000_1000_1/BiasAddBiasAdd/model/nmn10_isomap_2000_1000_1/MatMul:product:0=model/nmn10_isomap_2000_1000_1/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2(
&model/nmn10_isomap_2000_1000_1/BiasAdd�
#model/nmn10_isomap_2000_1000_1/ReluRelu/model/nmn10_isomap_2000_1000_1/BiasAdd:output:0*
T0*(
_output_shapes
:����������2%
#model/nmn10_isomap_2000_1000_1/Relu�
2model/nmn10_tsvd_2000_1000_1/MatMul/ReadVariableOpReadVariableOp;model_nmn10_tsvd_2000_1000_1_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype024
2model/nmn10_tsvd_2000_1000_1/MatMul/ReadVariableOp�
#model/nmn10_tsvd_2000_1000_1/MatMulMatMulnmn10_tsvd_2000_1000_input:model/nmn10_tsvd_2000_1000_1/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2%
#model/nmn10_tsvd_2000_1000_1/MatMul�
3model/nmn10_tsvd_2000_1000_1/BiasAdd/ReadVariableOpReadVariableOp<model_nmn10_tsvd_2000_1000_1_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype025
3model/nmn10_tsvd_2000_1000_1/BiasAdd/ReadVariableOp�
$model/nmn10_tsvd_2000_1000_1/BiasAddBiasAdd-model/nmn10_tsvd_2000_1000_1/MatMul:product:0;model/nmn10_tsvd_2000_1000_1/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2&
$model/nmn10_tsvd_2000_1000_1/BiasAdd�
!model/nmn10_tsvd_2000_1000_1/ReluRelu-model/nmn10_tsvd_2000_1000_1/BiasAdd:output:0*
T0*(
_output_shapes
:����������2#
!model/nmn10_tsvd_2000_1000_1/Relu�
1model/nmn10_pca_2000_1000_1/MatMul/ReadVariableOpReadVariableOp:model_nmn10_pca_2000_1000_1_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype023
1model/nmn10_pca_2000_1000_1/MatMul/ReadVariableOp�
"model/nmn10_pca_2000_1000_1/MatMulMatMulnmn10_pca_2000_1000_input9model/nmn10_pca_2000_1000_1/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2$
"model/nmn10_pca_2000_1000_1/MatMul�
2model/nmn10_pca_2000_1000_1/BiasAdd/ReadVariableOpReadVariableOp;model_nmn10_pca_2000_1000_1_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype024
2model/nmn10_pca_2000_1000_1/BiasAdd/ReadVariableOp�
#model/nmn10_pca_2000_1000_1/BiasAddBiasAdd,model/nmn10_pca_2000_1000_1/MatMul:product:0:model/nmn10_pca_2000_1000_1/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2%
#model/nmn10_pca_2000_1000_1/BiasAdd�
 model/nmn10_pca_2000_1000_1/ReluRelu,model/nmn10_pca_2000_1000_1/BiasAdd:output:0*
T0*(
_output_shapes
:����������2"
 model/nmn10_pca_2000_1000_1/Relu�
1model/nmn10_lle_2000_1000_2/MatMul/ReadVariableOpReadVariableOp:model_nmn10_lle_2000_1000_2_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype023
1model/nmn10_lle_2000_1000_2/MatMul/ReadVariableOp�
"model/nmn10_lle_2000_1000_2/MatMulMatMul.model/nmn10_lle_2000_1000_1/Relu:activations:09model/nmn10_lle_2000_1000_2/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2$
"model/nmn10_lle_2000_1000_2/MatMul�
2model/nmn10_lle_2000_1000_2/BiasAdd/ReadVariableOpReadVariableOp;model_nmn10_lle_2000_1000_2_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype024
2model/nmn10_lle_2000_1000_2/BiasAdd/ReadVariableOp�
#model/nmn10_lle_2000_1000_2/BiasAddBiasAdd,model/nmn10_lle_2000_1000_2/MatMul:product:0:model/nmn10_lle_2000_1000_2/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2%
#model/nmn10_lle_2000_1000_2/BiasAdd�
 model/nmn10_lle_2000_1000_2/ReluRelu,model/nmn10_lle_2000_1000_2/BiasAdd:output:0*
T0*(
_output_shapes
:����������2"
 model/nmn10_lle_2000_1000_2/Relu�
4model/nmn10_isomap_2000_1000_2/MatMul/ReadVariableOpReadVariableOp=model_nmn10_isomap_2000_1000_2_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype026
4model/nmn10_isomap_2000_1000_2/MatMul/ReadVariableOp�
%model/nmn10_isomap_2000_1000_2/MatMulMatMul1model/nmn10_isomap_2000_1000_1/Relu:activations:0<model/nmn10_isomap_2000_1000_2/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2'
%model/nmn10_isomap_2000_1000_2/MatMul�
5model/nmn10_isomap_2000_1000_2/BiasAdd/ReadVariableOpReadVariableOp>model_nmn10_isomap_2000_1000_2_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype027
5model/nmn10_isomap_2000_1000_2/BiasAdd/ReadVariableOp�
&model/nmn10_isomap_2000_1000_2/BiasAddBiasAdd/model/nmn10_isomap_2000_1000_2/MatMul:product:0=model/nmn10_isomap_2000_1000_2/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2(
&model/nmn10_isomap_2000_1000_2/BiasAdd�
#model/nmn10_isomap_2000_1000_2/ReluRelu/model/nmn10_isomap_2000_1000_2/BiasAdd:output:0*
T0*(
_output_shapes
:����������2%
#model/nmn10_isomap_2000_1000_2/Relu�
2model/nmn10_tsvd_2000_1000_2/MatMul/ReadVariableOpReadVariableOp;model_nmn10_tsvd_2000_1000_2_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype024
2model/nmn10_tsvd_2000_1000_2/MatMul/ReadVariableOp�
#model/nmn10_tsvd_2000_1000_2/MatMulMatMul/model/nmn10_tsvd_2000_1000_1/Relu:activations:0:model/nmn10_tsvd_2000_1000_2/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2%
#model/nmn10_tsvd_2000_1000_2/MatMul�
3model/nmn10_tsvd_2000_1000_2/BiasAdd/ReadVariableOpReadVariableOp<model_nmn10_tsvd_2000_1000_2_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype025
3model/nmn10_tsvd_2000_1000_2/BiasAdd/ReadVariableOp�
$model/nmn10_tsvd_2000_1000_2/BiasAddBiasAdd-model/nmn10_tsvd_2000_1000_2/MatMul:product:0;model/nmn10_tsvd_2000_1000_2/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2&
$model/nmn10_tsvd_2000_1000_2/BiasAdd�
!model/nmn10_tsvd_2000_1000_2/ReluRelu-model/nmn10_tsvd_2000_1000_2/BiasAdd:output:0*
T0*(
_output_shapes
:����������2#
!model/nmn10_tsvd_2000_1000_2/Relu�
1model/nmn10_pca_2000_1000_2/MatMul/ReadVariableOpReadVariableOp:model_nmn10_pca_2000_1000_2_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype023
1model/nmn10_pca_2000_1000_2/MatMul/ReadVariableOp�
"model/nmn10_pca_2000_1000_2/MatMulMatMul.model/nmn10_pca_2000_1000_1/Relu:activations:09model/nmn10_pca_2000_1000_2/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2$
"model/nmn10_pca_2000_1000_2/MatMul�
2model/nmn10_pca_2000_1000_2/BiasAdd/ReadVariableOpReadVariableOp;model_nmn10_pca_2000_1000_2_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype024
2model/nmn10_pca_2000_1000_2/BiasAdd/ReadVariableOp�
#model/nmn10_pca_2000_1000_2/BiasAddBiasAdd,model/nmn10_pca_2000_1000_2/MatMul:product:0:model/nmn10_pca_2000_1000_2/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2%
#model/nmn10_pca_2000_1000_2/BiasAdd�
 model/nmn10_pca_2000_1000_2/ReluRelu,model/nmn10_pca_2000_1000_2/BiasAdd:output:0*
T0*(
_output_shapes
:����������2"
 model/nmn10_pca_2000_1000_2/Relu�
1model/nmn10_lle_2000_1000_3/MatMul/ReadVariableOpReadVariableOp:model_nmn10_lle_2000_1000_3_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype023
1model/nmn10_lle_2000_1000_3/MatMul/ReadVariableOp�
"model/nmn10_lle_2000_1000_3/MatMulMatMul.model/nmn10_lle_2000_1000_2/Relu:activations:09model/nmn10_lle_2000_1000_3/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2$
"model/nmn10_lle_2000_1000_3/MatMul�
2model/nmn10_lle_2000_1000_3/BiasAdd/ReadVariableOpReadVariableOp;model_nmn10_lle_2000_1000_3_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype024
2model/nmn10_lle_2000_1000_3/BiasAdd/ReadVariableOp�
#model/nmn10_lle_2000_1000_3/BiasAddBiasAdd,model/nmn10_lle_2000_1000_3/MatMul:product:0:model/nmn10_lle_2000_1000_3/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2%
#model/nmn10_lle_2000_1000_3/BiasAdd�
 model/nmn10_lle_2000_1000_3/ReluRelu,model/nmn10_lle_2000_1000_3/BiasAdd:output:0*
T0*(
_output_shapes
:����������2"
 model/nmn10_lle_2000_1000_3/Relu�
4model/nmn10_isomap_2000_1000_3/MatMul/ReadVariableOpReadVariableOp=model_nmn10_isomap_2000_1000_3_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype026
4model/nmn10_isomap_2000_1000_3/MatMul/ReadVariableOp�
%model/nmn10_isomap_2000_1000_3/MatMulMatMul1model/nmn10_isomap_2000_1000_2/Relu:activations:0<model/nmn10_isomap_2000_1000_3/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2'
%model/nmn10_isomap_2000_1000_3/MatMul�
5model/nmn10_isomap_2000_1000_3/BiasAdd/ReadVariableOpReadVariableOp>model_nmn10_isomap_2000_1000_3_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype027
5model/nmn10_isomap_2000_1000_3/BiasAdd/ReadVariableOp�
&model/nmn10_isomap_2000_1000_3/BiasAddBiasAdd/model/nmn10_isomap_2000_1000_3/MatMul:product:0=model/nmn10_isomap_2000_1000_3/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2(
&model/nmn10_isomap_2000_1000_3/BiasAdd�
#model/nmn10_isomap_2000_1000_3/ReluRelu/model/nmn10_isomap_2000_1000_3/BiasAdd:output:0*
T0*(
_output_shapes
:����������2%
#model/nmn10_isomap_2000_1000_3/Relu�
2model/nmn10_tsvd_2000_1000_3/MatMul/ReadVariableOpReadVariableOp;model_nmn10_tsvd_2000_1000_3_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype024
2model/nmn10_tsvd_2000_1000_3/MatMul/ReadVariableOp�
#model/nmn10_tsvd_2000_1000_3/MatMulMatMul/model/nmn10_tsvd_2000_1000_2/Relu:activations:0:model/nmn10_tsvd_2000_1000_3/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2%
#model/nmn10_tsvd_2000_1000_3/MatMul�
3model/nmn10_tsvd_2000_1000_3/BiasAdd/ReadVariableOpReadVariableOp<model_nmn10_tsvd_2000_1000_3_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype025
3model/nmn10_tsvd_2000_1000_3/BiasAdd/ReadVariableOp�
$model/nmn10_tsvd_2000_1000_3/BiasAddBiasAdd-model/nmn10_tsvd_2000_1000_3/MatMul:product:0;model/nmn10_tsvd_2000_1000_3/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2&
$model/nmn10_tsvd_2000_1000_3/BiasAdd�
!model/nmn10_tsvd_2000_1000_3/ReluRelu-model/nmn10_tsvd_2000_1000_3/BiasAdd:output:0*
T0*(
_output_shapes
:����������2#
!model/nmn10_tsvd_2000_1000_3/Relu�
1model/nmn10_pca_2000_1000_3/MatMul/ReadVariableOpReadVariableOp:model_nmn10_pca_2000_1000_3_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype023
1model/nmn10_pca_2000_1000_3/MatMul/ReadVariableOp�
"model/nmn10_pca_2000_1000_3/MatMulMatMul.model/nmn10_pca_2000_1000_2/Relu:activations:09model/nmn10_pca_2000_1000_3/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2$
"model/nmn10_pca_2000_1000_3/MatMul�
2model/nmn10_pca_2000_1000_3/BiasAdd/ReadVariableOpReadVariableOp;model_nmn10_pca_2000_1000_3_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype024
2model/nmn10_pca_2000_1000_3/BiasAdd/ReadVariableOp�
#model/nmn10_pca_2000_1000_3/BiasAddBiasAdd,model/nmn10_pca_2000_1000_3/MatMul:product:0:model/nmn10_pca_2000_1000_3/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2%
#model/nmn10_pca_2000_1000_3/BiasAdd�
 model/nmn10_pca_2000_1000_3/ReluRelu,model/nmn10_pca_2000_1000_3/BiasAdd:output:0*
T0*(
_output_shapes
:����������2"
 model/nmn10_pca_2000_1000_3/Relu�
3model/nmn10_pca_2000_1000_out/MatMul/ReadVariableOpReadVariableOp<model_nmn10_pca_2000_1000_out_matmul_readvariableop_resource*
_output_shapes
:	�*
dtype025
3model/nmn10_pca_2000_1000_out/MatMul/ReadVariableOp�
$model/nmn10_pca_2000_1000_out/MatMulMatMul.model/nmn10_pca_2000_1000_3/Relu:activations:0;model/nmn10_pca_2000_1000_out/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2&
$model/nmn10_pca_2000_1000_out/MatMul�
4model/nmn10_pca_2000_1000_out/BiasAdd/ReadVariableOpReadVariableOp=model_nmn10_pca_2000_1000_out_biasadd_readvariableop_resource*
_output_shapes
:*
dtype026
4model/nmn10_pca_2000_1000_out/BiasAdd/ReadVariableOp�
%model/nmn10_pca_2000_1000_out/BiasAddBiasAdd.model/nmn10_pca_2000_1000_out/MatMul:product:0<model/nmn10_pca_2000_1000_out/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2'
%model/nmn10_pca_2000_1000_out/BiasAdd�
%model/nmn10_pca_2000_1000_out/SigmoidSigmoid.model/nmn10_pca_2000_1000_out/BiasAdd:output:0*
T0*'
_output_shapes
:���������2'
%model/nmn10_pca_2000_1000_out/Sigmoid�
4model/nmn10_tsvd_2000_1000_out/MatMul/ReadVariableOpReadVariableOp=model_nmn10_tsvd_2000_1000_out_matmul_readvariableop_resource*
_output_shapes
:	�*
dtype026
4model/nmn10_tsvd_2000_1000_out/MatMul/ReadVariableOp�
%model/nmn10_tsvd_2000_1000_out/MatMulMatMul/model/nmn10_tsvd_2000_1000_3/Relu:activations:0<model/nmn10_tsvd_2000_1000_out/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2'
%model/nmn10_tsvd_2000_1000_out/MatMul�
5model/nmn10_tsvd_2000_1000_out/BiasAdd/ReadVariableOpReadVariableOp>model_nmn10_tsvd_2000_1000_out_biasadd_readvariableop_resource*
_output_shapes
:*
dtype027
5model/nmn10_tsvd_2000_1000_out/BiasAdd/ReadVariableOp�
&model/nmn10_tsvd_2000_1000_out/BiasAddBiasAdd/model/nmn10_tsvd_2000_1000_out/MatMul:product:0=model/nmn10_tsvd_2000_1000_out/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2(
&model/nmn10_tsvd_2000_1000_out/BiasAdd�
&model/nmn10_tsvd_2000_1000_out/SigmoidSigmoid/model/nmn10_tsvd_2000_1000_out/BiasAdd:output:0*
T0*'
_output_shapes
:���������2(
&model/nmn10_tsvd_2000_1000_out/Sigmoid�
6model/nmn10_isomap_2000_1000_out/MatMul/ReadVariableOpReadVariableOp?model_nmn10_isomap_2000_1000_out_matmul_readvariableop_resource*
_output_shapes
:	�*
dtype028
6model/nmn10_isomap_2000_1000_out/MatMul/ReadVariableOp�
'model/nmn10_isomap_2000_1000_out/MatMulMatMul1model/nmn10_isomap_2000_1000_3/Relu:activations:0>model/nmn10_isomap_2000_1000_out/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2)
'model/nmn10_isomap_2000_1000_out/MatMul�
7model/nmn10_isomap_2000_1000_out/BiasAdd/ReadVariableOpReadVariableOp@model_nmn10_isomap_2000_1000_out_biasadd_readvariableop_resource*
_output_shapes
:*
dtype029
7model/nmn10_isomap_2000_1000_out/BiasAdd/ReadVariableOp�
(model/nmn10_isomap_2000_1000_out/BiasAddBiasAdd1model/nmn10_isomap_2000_1000_out/MatMul:product:0?model/nmn10_isomap_2000_1000_out/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2*
(model/nmn10_isomap_2000_1000_out/BiasAdd�
(model/nmn10_isomap_2000_1000_out/SigmoidSigmoid1model/nmn10_isomap_2000_1000_out/BiasAdd:output:0*
T0*'
_output_shapes
:���������2*
(model/nmn10_isomap_2000_1000_out/Sigmoid�
3model/nmn10_lle_2000_1000_out/MatMul/ReadVariableOpReadVariableOp<model_nmn10_lle_2000_1000_out_matmul_readvariableop_resource*
_output_shapes
:	�*
dtype025
3model/nmn10_lle_2000_1000_out/MatMul/ReadVariableOp�
$model/nmn10_lle_2000_1000_out/MatMulMatMul.model/nmn10_lle_2000_1000_3/Relu:activations:0;model/nmn10_lle_2000_1000_out/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2&
$model/nmn10_lle_2000_1000_out/MatMul�
4model/nmn10_lle_2000_1000_out/BiasAdd/ReadVariableOpReadVariableOp=model_nmn10_lle_2000_1000_out_biasadd_readvariableop_resource*
_output_shapes
:*
dtype026
4model/nmn10_lle_2000_1000_out/BiasAdd/ReadVariableOp�
%model/nmn10_lle_2000_1000_out/BiasAddBiasAdd.model/nmn10_lle_2000_1000_out/MatMul:product:0<model/nmn10_lle_2000_1000_out/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2'
%model/nmn10_lle_2000_1000_out/BiasAdd�
%model/nmn10_lle_2000_1000_out/SigmoidSigmoid.model/nmn10_lle_2000_1000_out/BiasAdd:output:0*
T0*'
_output_shapes
:���������2'
%model/nmn10_lle_2000_1000_out/Sigmoid�
 model/concatenate_24/concat/axisConst*
_output_shapes
: *
dtype0*
value	B :2"
 model/concatenate_24/concat/axis�
model/concatenate_24/concatConcatV2)model/nmn10_pca_2000_1000_out/Sigmoid:y:0*model/nmn10_tsvd_2000_1000_out/Sigmoid:y:0,model/nmn10_isomap_2000_1000_out/Sigmoid:y:0)model/nmn10_lle_2000_1000_out/Sigmoid:y:0)model/concatenate_24/concat/axis:output:0*
N*
T0*'
_output_shapes
:���������2
model/concatenate_24/concat�
"model/joined/MatMul/ReadVariableOpReadVariableOp+model_joined_matmul_readvariableop_resource*
_output_shapes

:*
dtype02$
"model/joined/MatMul/ReadVariableOp�
model/joined/MatMulMatMul$model/concatenate_24/concat:output:0*model/joined/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
model/joined/MatMul�
#model/joined/BiasAdd/ReadVariableOpReadVariableOp,model_joined_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02%
#model/joined/BiasAdd/ReadVariableOp�
model/joined/BiasAddBiasAddmodel/joined/MatMul:product:0+model/joined/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
model/joined/BiasAdd�
model/joined/SigmoidSigmoidmodel/joined/BiasAdd:output:0*
T0*'
_output_shapes
:���������2
model/joined/Sigmoids
IdentityIdentitymodel/joined/Sigmoid:y:0^NoOp*
T0*'
_output_shapes
:���������2

Identity�
NoOpNoOp$^model/joined/BiasAdd/ReadVariableOp#^model/joined/MatMul/ReadVariableOp6^model/nmn10_isomap_2000_1000_1/BiasAdd/ReadVariableOp5^model/nmn10_isomap_2000_1000_1/MatMul/ReadVariableOp6^model/nmn10_isomap_2000_1000_2/BiasAdd/ReadVariableOp5^model/nmn10_isomap_2000_1000_2/MatMul/ReadVariableOp6^model/nmn10_isomap_2000_1000_3/BiasAdd/ReadVariableOp5^model/nmn10_isomap_2000_1000_3/MatMul/ReadVariableOp8^model/nmn10_isomap_2000_1000_out/BiasAdd/ReadVariableOp7^model/nmn10_isomap_2000_1000_out/MatMul/ReadVariableOp3^model/nmn10_lle_2000_1000_1/BiasAdd/ReadVariableOp2^model/nmn10_lle_2000_1000_1/MatMul/ReadVariableOp3^model/nmn10_lle_2000_1000_2/BiasAdd/ReadVariableOp2^model/nmn10_lle_2000_1000_2/MatMul/ReadVariableOp3^model/nmn10_lle_2000_1000_3/BiasAdd/ReadVariableOp2^model/nmn10_lle_2000_1000_3/MatMul/ReadVariableOp5^model/nmn10_lle_2000_1000_out/BiasAdd/ReadVariableOp4^model/nmn10_lle_2000_1000_out/MatMul/ReadVariableOp3^model/nmn10_pca_2000_1000_1/BiasAdd/ReadVariableOp2^model/nmn10_pca_2000_1000_1/MatMul/ReadVariableOp3^model/nmn10_pca_2000_1000_2/BiasAdd/ReadVariableOp2^model/nmn10_pca_2000_1000_2/MatMul/ReadVariableOp3^model/nmn10_pca_2000_1000_3/BiasAdd/ReadVariableOp2^model/nmn10_pca_2000_1000_3/MatMul/ReadVariableOp5^model/nmn10_pca_2000_1000_out/BiasAdd/ReadVariableOp4^model/nmn10_pca_2000_1000_out/MatMul/ReadVariableOp4^model/nmn10_tsvd_2000_1000_1/BiasAdd/ReadVariableOp3^model/nmn10_tsvd_2000_1000_1/MatMul/ReadVariableOp4^model/nmn10_tsvd_2000_1000_2/BiasAdd/ReadVariableOp3^model/nmn10_tsvd_2000_1000_2/MatMul/ReadVariableOp4^model/nmn10_tsvd_2000_1000_3/BiasAdd/ReadVariableOp3^model/nmn10_tsvd_2000_1000_3/MatMul/ReadVariableOp6^model/nmn10_tsvd_2000_1000_out/BiasAdd/ReadVariableOp5^model/nmn10_tsvd_2000_1000_out/MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*�
_input_shapes�
�:����������:����������:����������:����������: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 2J
#model/joined/BiasAdd/ReadVariableOp#model/joined/BiasAdd/ReadVariableOp2H
"model/joined/MatMul/ReadVariableOp"model/joined/MatMul/ReadVariableOp2n
5model/nmn10_isomap_2000_1000_1/BiasAdd/ReadVariableOp5model/nmn10_isomap_2000_1000_1/BiasAdd/ReadVariableOp2l
4model/nmn10_isomap_2000_1000_1/MatMul/ReadVariableOp4model/nmn10_isomap_2000_1000_1/MatMul/ReadVariableOp2n
5model/nmn10_isomap_2000_1000_2/BiasAdd/ReadVariableOp5model/nmn10_isomap_2000_1000_2/BiasAdd/ReadVariableOp2l
4model/nmn10_isomap_2000_1000_2/MatMul/ReadVariableOp4model/nmn10_isomap_2000_1000_2/MatMul/ReadVariableOp2n
5model/nmn10_isomap_2000_1000_3/BiasAdd/ReadVariableOp5model/nmn10_isomap_2000_1000_3/BiasAdd/ReadVariableOp2l
4model/nmn10_isomap_2000_1000_3/MatMul/ReadVariableOp4model/nmn10_isomap_2000_1000_3/MatMul/ReadVariableOp2r
7model/nmn10_isomap_2000_1000_out/BiasAdd/ReadVariableOp7model/nmn10_isomap_2000_1000_out/BiasAdd/ReadVariableOp2p
6model/nmn10_isomap_2000_1000_out/MatMul/ReadVariableOp6model/nmn10_isomap_2000_1000_out/MatMul/ReadVariableOp2h
2model/nmn10_lle_2000_1000_1/BiasAdd/ReadVariableOp2model/nmn10_lle_2000_1000_1/BiasAdd/ReadVariableOp2f
1model/nmn10_lle_2000_1000_1/MatMul/ReadVariableOp1model/nmn10_lle_2000_1000_1/MatMul/ReadVariableOp2h
2model/nmn10_lle_2000_1000_2/BiasAdd/ReadVariableOp2model/nmn10_lle_2000_1000_2/BiasAdd/ReadVariableOp2f
1model/nmn10_lle_2000_1000_2/MatMul/ReadVariableOp1model/nmn10_lle_2000_1000_2/MatMul/ReadVariableOp2h
2model/nmn10_lle_2000_1000_3/BiasAdd/ReadVariableOp2model/nmn10_lle_2000_1000_3/BiasAdd/ReadVariableOp2f
1model/nmn10_lle_2000_1000_3/MatMul/ReadVariableOp1model/nmn10_lle_2000_1000_3/MatMul/ReadVariableOp2l
4model/nmn10_lle_2000_1000_out/BiasAdd/ReadVariableOp4model/nmn10_lle_2000_1000_out/BiasAdd/ReadVariableOp2j
3model/nmn10_lle_2000_1000_out/MatMul/ReadVariableOp3model/nmn10_lle_2000_1000_out/MatMul/ReadVariableOp2h
2model/nmn10_pca_2000_1000_1/BiasAdd/ReadVariableOp2model/nmn10_pca_2000_1000_1/BiasAdd/ReadVariableOp2f
1model/nmn10_pca_2000_1000_1/MatMul/ReadVariableOp1model/nmn10_pca_2000_1000_1/MatMul/ReadVariableOp2h
2model/nmn10_pca_2000_1000_2/BiasAdd/ReadVariableOp2model/nmn10_pca_2000_1000_2/BiasAdd/ReadVariableOp2f
1model/nmn10_pca_2000_1000_2/MatMul/ReadVariableOp1model/nmn10_pca_2000_1000_2/MatMul/ReadVariableOp2h
2model/nmn10_pca_2000_1000_3/BiasAdd/ReadVariableOp2model/nmn10_pca_2000_1000_3/BiasAdd/ReadVariableOp2f
1model/nmn10_pca_2000_1000_3/MatMul/ReadVariableOp1model/nmn10_pca_2000_1000_3/MatMul/ReadVariableOp2l
4model/nmn10_pca_2000_1000_out/BiasAdd/ReadVariableOp4model/nmn10_pca_2000_1000_out/BiasAdd/ReadVariableOp2j
3model/nmn10_pca_2000_1000_out/MatMul/ReadVariableOp3model/nmn10_pca_2000_1000_out/MatMul/ReadVariableOp2j
3model/nmn10_tsvd_2000_1000_1/BiasAdd/ReadVariableOp3model/nmn10_tsvd_2000_1000_1/BiasAdd/ReadVariableOp2h
2model/nmn10_tsvd_2000_1000_1/MatMul/ReadVariableOp2model/nmn10_tsvd_2000_1000_1/MatMul/ReadVariableOp2j
3model/nmn10_tsvd_2000_1000_2/BiasAdd/ReadVariableOp3model/nmn10_tsvd_2000_1000_2/BiasAdd/ReadVariableOp2h
2model/nmn10_tsvd_2000_1000_2/MatMul/ReadVariableOp2model/nmn10_tsvd_2000_1000_2/MatMul/ReadVariableOp2j
3model/nmn10_tsvd_2000_1000_3/BiasAdd/ReadVariableOp3model/nmn10_tsvd_2000_1000_3/BiasAdd/ReadVariableOp2h
2model/nmn10_tsvd_2000_1000_3/MatMul/ReadVariableOp2model/nmn10_tsvd_2000_1000_3/MatMul/ReadVariableOp2n
5model/nmn10_tsvd_2000_1000_out/BiasAdd/ReadVariableOp5model/nmn10_tsvd_2000_1000_out/BiasAdd/ReadVariableOp2l
4model/nmn10_tsvd_2000_1000_out/MatMul/ReadVariableOp4model/nmn10_tsvd_2000_1000_out/MatMul/ReadVariableOp:c _
(
_output_shapes
:����������
3
_user_specified_namenmn10_pca_2000_1000_input:d`
(
_output_shapes
:����������
4
_user_specified_namenmn10_tsvd_2000_1000_input:fb
(
_output_shapes
:����������
6
_user_specified_namenmn10_isomap_2000_1000_input:c_
(
_output_shapes
:����������
3
_user_specified_namenmn10_lle_2000_1000_input
�
�
Q__inference_nmn10_lle_2000_1000_3_layer_call_and_return_conditional_losses_781238

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
6__inference_nmn10_pca_2000_1000_3_layer_call_fn_782810

inputs
unknown:
��
	unknown_0:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_nmn10_pca_2000_1000_3_layer_call_and_return_conditional_losses_7812892
StatefulPartitionedCall|
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:����������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�x
�
A__inference_model_layer_call_and_return_conditional_losses_782056
nmn10_pca_2000_1000_input
nmn10_tsvd_2000_1000_input 
nmn10_isomap_2000_1000_input
nmn10_lle_2000_1000_input0
nmn10_lle_2000_1000_1_781969:
��+
nmn10_lle_2000_1000_1_781971:	�3
nmn10_isomap_2000_1000_1_781974:
��.
nmn10_isomap_2000_1000_1_781976:	�1
nmn10_tsvd_2000_1000_1_781979:
��,
nmn10_tsvd_2000_1000_1_781981:	�0
nmn10_pca_2000_1000_1_781984:
��+
nmn10_pca_2000_1000_1_781986:	�0
nmn10_lle_2000_1000_2_781989:
��+
nmn10_lle_2000_1000_2_781991:	�3
nmn10_isomap_2000_1000_2_781994:
��.
nmn10_isomap_2000_1000_2_781996:	�1
nmn10_tsvd_2000_1000_2_781999:
��,
nmn10_tsvd_2000_1000_2_782001:	�0
nmn10_pca_2000_1000_2_782004:
��+
nmn10_pca_2000_1000_2_782006:	�0
nmn10_lle_2000_1000_3_782009:
��+
nmn10_lle_2000_1000_3_782011:	�3
nmn10_isomap_2000_1000_3_782014:
��.
nmn10_isomap_2000_1000_3_782016:	�1
nmn10_tsvd_2000_1000_3_782019:
��,
nmn10_tsvd_2000_1000_3_782021:	�0
nmn10_pca_2000_1000_3_782024:
��+
nmn10_pca_2000_1000_3_782026:	�1
nmn10_pca_2000_1000_out_782029:	�,
nmn10_pca_2000_1000_out_782031:2
nmn10_tsvd_2000_1000_out_782034:	�-
nmn10_tsvd_2000_1000_out_782036:4
!nmn10_isomap_2000_1000_out_782039:	�/
!nmn10_isomap_2000_1000_out_782041:1
nmn10_lle_2000_1000_out_782044:	�,
nmn10_lle_2000_1000_out_782046:
joined_782050:
joined_782052:
identity��joined/StatefulPartitionedCall�0nmn10_isomap_2000_1000_1/StatefulPartitionedCall�0nmn10_isomap_2000_1000_2/StatefulPartitionedCall�0nmn10_isomap_2000_1000_3/StatefulPartitionedCall�2nmn10_isomap_2000_1000_out/StatefulPartitionedCall�-nmn10_lle_2000_1000_1/StatefulPartitionedCall�-nmn10_lle_2000_1000_2/StatefulPartitionedCall�-nmn10_lle_2000_1000_3/StatefulPartitionedCall�/nmn10_lle_2000_1000_out/StatefulPartitionedCall�-nmn10_pca_2000_1000_1/StatefulPartitionedCall�-nmn10_pca_2000_1000_2/StatefulPartitionedCall�-nmn10_pca_2000_1000_3/StatefulPartitionedCall�/nmn10_pca_2000_1000_out/StatefulPartitionedCall�.nmn10_tsvd_2000_1000_1/StatefulPartitionedCall�.nmn10_tsvd_2000_1000_2/StatefulPartitionedCall�.nmn10_tsvd_2000_1000_3/StatefulPartitionedCall�0nmn10_tsvd_2000_1000_out/StatefulPartitionedCall�
-nmn10_lle_2000_1000_1/StatefulPartitionedCallStatefulPartitionedCallnmn10_lle_2000_1000_inputnmn10_lle_2000_1000_1_781969nmn10_lle_2000_1000_1_781971*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_nmn10_lle_2000_1000_1_layer_call_and_return_conditional_losses_7811022/
-nmn10_lle_2000_1000_1/StatefulPartitionedCall�
0nmn10_isomap_2000_1000_1/StatefulPartitionedCallStatefulPartitionedCallnmn10_isomap_2000_1000_inputnmn10_isomap_2000_1000_1_781974nmn10_isomap_2000_1000_1_781976*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *]
fXRV
T__inference_nmn10_isomap_2000_1000_1_layer_call_and_return_conditional_losses_78111922
0nmn10_isomap_2000_1000_1/StatefulPartitionedCall�
.nmn10_tsvd_2000_1000_1/StatefulPartitionedCallStatefulPartitionedCallnmn10_tsvd_2000_1000_inputnmn10_tsvd_2000_1000_1_781979nmn10_tsvd_2000_1000_1_781981*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_nmn10_tsvd_2000_1000_1_layer_call_and_return_conditional_losses_78113620
.nmn10_tsvd_2000_1000_1/StatefulPartitionedCall�
-nmn10_pca_2000_1000_1/StatefulPartitionedCallStatefulPartitionedCallnmn10_pca_2000_1000_inputnmn10_pca_2000_1000_1_781984nmn10_pca_2000_1000_1_781986*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_nmn10_pca_2000_1000_1_layer_call_and_return_conditional_losses_7811532/
-nmn10_pca_2000_1000_1/StatefulPartitionedCall�
-nmn10_lle_2000_1000_2/StatefulPartitionedCallStatefulPartitionedCall6nmn10_lle_2000_1000_1/StatefulPartitionedCall:output:0nmn10_lle_2000_1000_2_781989nmn10_lle_2000_1000_2_781991*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_nmn10_lle_2000_1000_2_layer_call_and_return_conditional_losses_7811702/
-nmn10_lle_2000_1000_2/StatefulPartitionedCall�
0nmn10_isomap_2000_1000_2/StatefulPartitionedCallStatefulPartitionedCall9nmn10_isomap_2000_1000_1/StatefulPartitionedCall:output:0nmn10_isomap_2000_1000_2_781994nmn10_isomap_2000_1000_2_781996*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *]
fXRV
T__inference_nmn10_isomap_2000_1000_2_layer_call_and_return_conditional_losses_78118722
0nmn10_isomap_2000_1000_2/StatefulPartitionedCall�
.nmn10_tsvd_2000_1000_2/StatefulPartitionedCallStatefulPartitionedCall7nmn10_tsvd_2000_1000_1/StatefulPartitionedCall:output:0nmn10_tsvd_2000_1000_2_781999nmn10_tsvd_2000_1000_2_782001*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_nmn10_tsvd_2000_1000_2_layer_call_and_return_conditional_losses_78120420
.nmn10_tsvd_2000_1000_2/StatefulPartitionedCall�
-nmn10_pca_2000_1000_2/StatefulPartitionedCallStatefulPartitionedCall6nmn10_pca_2000_1000_1/StatefulPartitionedCall:output:0nmn10_pca_2000_1000_2_782004nmn10_pca_2000_1000_2_782006*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_nmn10_pca_2000_1000_2_layer_call_and_return_conditional_losses_7812212/
-nmn10_pca_2000_1000_2/StatefulPartitionedCall�
-nmn10_lle_2000_1000_3/StatefulPartitionedCallStatefulPartitionedCall6nmn10_lle_2000_1000_2/StatefulPartitionedCall:output:0nmn10_lle_2000_1000_3_782009nmn10_lle_2000_1000_3_782011*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_nmn10_lle_2000_1000_3_layer_call_and_return_conditional_losses_7812382/
-nmn10_lle_2000_1000_3/StatefulPartitionedCall�
0nmn10_isomap_2000_1000_3/StatefulPartitionedCallStatefulPartitionedCall9nmn10_isomap_2000_1000_2/StatefulPartitionedCall:output:0nmn10_isomap_2000_1000_3_782014nmn10_isomap_2000_1000_3_782016*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *]
fXRV
T__inference_nmn10_isomap_2000_1000_3_layer_call_and_return_conditional_losses_78125522
0nmn10_isomap_2000_1000_3/StatefulPartitionedCall�
.nmn10_tsvd_2000_1000_3/StatefulPartitionedCallStatefulPartitionedCall7nmn10_tsvd_2000_1000_2/StatefulPartitionedCall:output:0nmn10_tsvd_2000_1000_3_782019nmn10_tsvd_2000_1000_3_782021*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_nmn10_tsvd_2000_1000_3_layer_call_and_return_conditional_losses_78127220
.nmn10_tsvd_2000_1000_3/StatefulPartitionedCall�
-nmn10_pca_2000_1000_3/StatefulPartitionedCallStatefulPartitionedCall6nmn10_pca_2000_1000_2/StatefulPartitionedCall:output:0nmn10_pca_2000_1000_3_782024nmn10_pca_2000_1000_3_782026*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_nmn10_pca_2000_1000_3_layer_call_and_return_conditional_losses_7812892/
-nmn10_pca_2000_1000_3/StatefulPartitionedCall�
/nmn10_pca_2000_1000_out/StatefulPartitionedCallStatefulPartitionedCall6nmn10_pca_2000_1000_3/StatefulPartitionedCall:output:0nmn10_pca_2000_1000_out_782029nmn10_pca_2000_1000_out_782031*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *\
fWRU
S__inference_nmn10_pca_2000_1000_out_layer_call_and_return_conditional_losses_78130621
/nmn10_pca_2000_1000_out/StatefulPartitionedCall�
0nmn10_tsvd_2000_1000_out/StatefulPartitionedCallStatefulPartitionedCall7nmn10_tsvd_2000_1000_3/StatefulPartitionedCall:output:0nmn10_tsvd_2000_1000_out_782034nmn10_tsvd_2000_1000_out_782036*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *]
fXRV
T__inference_nmn10_tsvd_2000_1000_out_layer_call_and_return_conditional_losses_78132322
0nmn10_tsvd_2000_1000_out/StatefulPartitionedCall�
2nmn10_isomap_2000_1000_out/StatefulPartitionedCallStatefulPartitionedCall9nmn10_isomap_2000_1000_3/StatefulPartitionedCall:output:0!nmn10_isomap_2000_1000_out_782039!nmn10_isomap_2000_1000_out_782041*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *_
fZRX
V__inference_nmn10_isomap_2000_1000_out_layer_call_and_return_conditional_losses_78134024
2nmn10_isomap_2000_1000_out/StatefulPartitionedCall�
/nmn10_lle_2000_1000_out/StatefulPartitionedCallStatefulPartitionedCall6nmn10_lle_2000_1000_3/StatefulPartitionedCall:output:0nmn10_lle_2000_1000_out_782044nmn10_lle_2000_1000_out_782046*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *\
fWRU
S__inference_nmn10_lle_2000_1000_out_layer_call_and_return_conditional_losses_78135721
/nmn10_lle_2000_1000_out/StatefulPartitionedCall�
concatenate_24/PartitionedCallPartitionedCall8nmn10_pca_2000_1000_out/StatefulPartitionedCall:output:09nmn10_tsvd_2000_1000_out/StatefulPartitionedCall:output:0;nmn10_isomap_2000_1000_out/StatefulPartitionedCall:output:08nmn10_lle_2000_1000_out/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *S
fNRL
J__inference_concatenate_24_layer_call_and_return_conditional_losses_7813722 
concatenate_24/PartitionedCall�
joined/StatefulPartitionedCallStatefulPartitionedCall'concatenate_24/PartitionedCall:output:0joined_782050joined_782052*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *K
fFRD
B__inference_joined_layer_call_and_return_conditional_losses_7813852 
joined/StatefulPartitionedCall�
IdentityIdentity'joined/StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identity�
NoOpNoOp^joined/StatefulPartitionedCall1^nmn10_isomap_2000_1000_1/StatefulPartitionedCall1^nmn10_isomap_2000_1000_2/StatefulPartitionedCall1^nmn10_isomap_2000_1000_3/StatefulPartitionedCall3^nmn10_isomap_2000_1000_out/StatefulPartitionedCall.^nmn10_lle_2000_1000_1/StatefulPartitionedCall.^nmn10_lle_2000_1000_2/StatefulPartitionedCall.^nmn10_lle_2000_1000_3/StatefulPartitionedCall0^nmn10_lle_2000_1000_out/StatefulPartitionedCall.^nmn10_pca_2000_1000_1/StatefulPartitionedCall.^nmn10_pca_2000_1000_2/StatefulPartitionedCall.^nmn10_pca_2000_1000_3/StatefulPartitionedCall0^nmn10_pca_2000_1000_out/StatefulPartitionedCall/^nmn10_tsvd_2000_1000_1/StatefulPartitionedCall/^nmn10_tsvd_2000_1000_2/StatefulPartitionedCall/^nmn10_tsvd_2000_1000_3/StatefulPartitionedCall1^nmn10_tsvd_2000_1000_out/StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*�
_input_shapes�
�:����������:����������:����������:����������: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 2@
joined/StatefulPartitionedCalljoined/StatefulPartitionedCall2d
0nmn10_isomap_2000_1000_1/StatefulPartitionedCall0nmn10_isomap_2000_1000_1/StatefulPartitionedCall2d
0nmn10_isomap_2000_1000_2/StatefulPartitionedCall0nmn10_isomap_2000_1000_2/StatefulPartitionedCall2d
0nmn10_isomap_2000_1000_3/StatefulPartitionedCall0nmn10_isomap_2000_1000_3/StatefulPartitionedCall2h
2nmn10_isomap_2000_1000_out/StatefulPartitionedCall2nmn10_isomap_2000_1000_out/StatefulPartitionedCall2^
-nmn10_lle_2000_1000_1/StatefulPartitionedCall-nmn10_lle_2000_1000_1/StatefulPartitionedCall2^
-nmn10_lle_2000_1000_2/StatefulPartitionedCall-nmn10_lle_2000_1000_2/StatefulPartitionedCall2^
-nmn10_lle_2000_1000_3/StatefulPartitionedCall-nmn10_lle_2000_1000_3/StatefulPartitionedCall2b
/nmn10_lle_2000_1000_out/StatefulPartitionedCall/nmn10_lle_2000_1000_out/StatefulPartitionedCall2^
-nmn10_pca_2000_1000_1/StatefulPartitionedCall-nmn10_pca_2000_1000_1/StatefulPartitionedCall2^
-nmn10_pca_2000_1000_2/StatefulPartitionedCall-nmn10_pca_2000_1000_2/StatefulPartitionedCall2^
-nmn10_pca_2000_1000_3/StatefulPartitionedCall-nmn10_pca_2000_1000_3/StatefulPartitionedCall2b
/nmn10_pca_2000_1000_out/StatefulPartitionedCall/nmn10_pca_2000_1000_out/StatefulPartitionedCall2`
.nmn10_tsvd_2000_1000_1/StatefulPartitionedCall.nmn10_tsvd_2000_1000_1/StatefulPartitionedCall2`
.nmn10_tsvd_2000_1000_2/StatefulPartitionedCall.nmn10_tsvd_2000_1000_2/StatefulPartitionedCall2`
.nmn10_tsvd_2000_1000_3/StatefulPartitionedCall.nmn10_tsvd_2000_1000_3/StatefulPartitionedCall2d
0nmn10_tsvd_2000_1000_out/StatefulPartitionedCall0nmn10_tsvd_2000_1000_out/StatefulPartitionedCall:c _
(
_output_shapes
:����������
3
_user_specified_namenmn10_pca_2000_1000_input:d`
(
_output_shapes
:����������
4
_user_specified_namenmn10_tsvd_2000_1000_input:fb
(
_output_shapes
:����������
6
_user_specified_namenmn10_isomap_2000_1000_input:c_
(
_output_shapes
:����������
3
_user_specified_namenmn10_lle_2000_1000_input
�
�
R__inference_nmn10_tsvd_2000_1000_2_layer_call_and_return_conditional_losses_781204

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
Q__inference_nmn10_pca_2000_1000_3_layer_call_and_return_conditional_losses_781289

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
R__inference_nmn10_tsvd_2000_1000_1_layer_call_and_return_conditional_losses_782681

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
V__inference_nmn10_isomap_2000_1000_out_layer_call_and_return_conditional_losses_781340

inputs1
matmul_readvariableop_resource:	�-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes
:	�*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2	
BiasAdda
SigmoidSigmoidBiasAdd:output:0*
T0*'
_output_shapes
:���������2	
Sigmoidf
IdentityIdentitySigmoid:y:0^NoOp*
T0*'
_output_shapes
:���������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
V__inference_nmn10_isomap_2000_1000_out_layer_call_and_return_conditional_losses_782941

inputs1
matmul_readvariableop_resource:	�-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes
:	�*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2	
BiasAdda
SigmoidSigmoidBiasAdd:output:0*
T0*'
_output_shapes
:���������2	
Sigmoidf
IdentityIdentitySigmoid:y:0^NoOp*
T0*'
_output_shapes
:���������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
Q__inference_nmn10_pca_2000_1000_1_layer_call_and_return_conditional_losses_782661

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
T__inference_nmn10_isomap_2000_1000_1_layer_call_and_return_conditional_losses_781119

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
'__inference_joined_layer_call_fn_782987

inputs
unknown:
	unknown_0:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *K
fFRD
B__inference_joined_layer_call_and_return_conditional_losses_7813852
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������: : 22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs
�
�
S__inference_nmn10_pca_2000_1000_out_layer_call_and_return_conditional_losses_782901

inputs1
matmul_readvariableop_resource:	�-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes
:	�*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2	
BiasAdda
SigmoidSigmoidBiasAdd:output:0*
T0*'
_output_shapes
:���������2	
Sigmoidf
IdentityIdentitySigmoid:y:0^NoOp*
T0*'
_output_shapes
:���������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
T__inference_nmn10_isomap_2000_1000_1_layer_call_and_return_conditional_losses_782701

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�	
$__inference_signature_wrapper_782233 
nmn10_isomap_2000_1000_input
nmn10_lle_2000_1000_input
nmn10_pca_2000_1000_input
nmn10_tsvd_2000_1000_input
unknown:
��
	unknown_0:	�
	unknown_1:
��
	unknown_2:	�
	unknown_3:
��
	unknown_4:	�
	unknown_5:
��
	unknown_6:	�
	unknown_7:
��
	unknown_8:	�
	unknown_9:
��

unknown_10:	�

unknown_11:
��

unknown_12:	�

unknown_13:
��

unknown_14:	�

unknown_15:
��

unknown_16:	�

unknown_17:
��

unknown_18:	�

unknown_19:
��

unknown_20:	�

unknown_21:
��

unknown_22:	�

unknown_23:	�

unknown_24:

unknown_25:	�

unknown_26:

unknown_27:	�

unknown_28:

unknown_29:	�

unknown_30:

unknown_31:

unknown_32:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallnmn10_pca_2000_1000_inputnmn10_tsvd_2000_1000_inputnmn10_isomap_2000_1000_inputnmn10_lle_2000_1000_inputunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10
unknown_11
unknown_12
unknown_13
unknown_14
unknown_15
unknown_16
unknown_17
unknown_18
unknown_19
unknown_20
unknown_21
unknown_22
unknown_23
unknown_24
unknown_25
unknown_26
unknown_27
unknown_28
unknown_29
unknown_30
unknown_31
unknown_32*1
Tin*
(2&*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*D
_read_only_resource_inputs&
$"	
 !"#$%*0
config_proto 

CPU

GPU2*0J 8� **
f%R#
!__inference__wrapped_model_7810782
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*�
_input_shapes�
�:����������:����������:����������:����������: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:f b
(
_output_shapes
:����������
6
_user_specified_namenmn10_isomap_2000_1000_input:c_
(
_output_shapes
:����������
3
_user_specified_namenmn10_lle_2000_1000_input:c_
(
_output_shapes
:����������
3
_user_specified_namenmn10_pca_2000_1000_input:d`
(
_output_shapes
:����������
4
_user_specified_namenmn10_tsvd_2000_1000_input
�v
�
A__inference_model_layer_call_and_return_conditional_losses_781392

inputs
inputs_1
inputs_2
inputs_30
nmn10_lle_2000_1000_1_781103:
��+
nmn10_lle_2000_1000_1_781105:	�3
nmn10_isomap_2000_1000_1_781120:
��.
nmn10_isomap_2000_1000_1_781122:	�1
nmn10_tsvd_2000_1000_1_781137:
��,
nmn10_tsvd_2000_1000_1_781139:	�0
nmn10_pca_2000_1000_1_781154:
��+
nmn10_pca_2000_1000_1_781156:	�0
nmn10_lle_2000_1000_2_781171:
��+
nmn10_lle_2000_1000_2_781173:	�3
nmn10_isomap_2000_1000_2_781188:
��.
nmn10_isomap_2000_1000_2_781190:	�1
nmn10_tsvd_2000_1000_2_781205:
��,
nmn10_tsvd_2000_1000_2_781207:	�0
nmn10_pca_2000_1000_2_781222:
��+
nmn10_pca_2000_1000_2_781224:	�0
nmn10_lle_2000_1000_3_781239:
��+
nmn10_lle_2000_1000_3_781241:	�3
nmn10_isomap_2000_1000_3_781256:
��.
nmn10_isomap_2000_1000_3_781258:	�1
nmn10_tsvd_2000_1000_3_781273:
��,
nmn10_tsvd_2000_1000_3_781275:	�0
nmn10_pca_2000_1000_3_781290:
��+
nmn10_pca_2000_1000_3_781292:	�1
nmn10_pca_2000_1000_out_781307:	�,
nmn10_pca_2000_1000_out_781309:2
nmn10_tsvd_2000_1000_out_781324:	�-
nmn10_tsvd_2000_1000_out_781326:4
!nmn10_isomap_2000_1000_out_781341:	�/
!nmn10_isomap_2000_1000_out_781343:1
nmn10_lle_2000_1000_out_781358:	�,
nmn10_lle_2000_1000_out_781360:
joined_781386:
joined_781388:
identity��joined/StatefulPartitionedCall�0nmn10_isomap_2000_1000_1/StatefulPartitionedCall�0nmn10_isomap_2000_1000_2/StatefulPartitionedCall�0nmn10_isomap_2000_1000_3/StatefulPartitionedCall�2nmn10_isomap_2000_1000_out/StatefulPartitionedCall�-nmn10_lle_2000_1000_1/StatefulPartitionedCall�-nmn10_lle_2000_1000_2/StatefulPartitionedCall�-nmn10_lle_2000_1000_3/StatefulPartitionedCall�/nmn10_lle_2000_1000_out/StatefulPartitionedCall�-nmn10_pca_2000_1000_1/StatefulPartitionedCall�-nmn10_pca_2000_1000_2/StatefulPartitionedCall�-nmn10_pca_2000_1000_3/StatefulPartitionedCall�/nmn10_pca_2000_1000_out/StatefulPartitionedCall�.nmn10_tsvd_2000_1000_1/StatefulPartitionedCall�.nmn10_tsvd_2000_1000_2/StatefulPartitionedCall�.nmn10_tsvd_2000_1000_3/StatefulPartitionedCall�0nmn10_tsvd_2000_1000_out/StatefulPartitionedCall�
-nmn10_lle_2000_1000_1/StatefulPartitionedCallStatefulPartitionedCallinputs_3nmn10_lle_2000_1000_1_781103nmn10_lle_2000_1000_1_781105*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_nmn10_lle_2000_1000_1_layer_call_and_return_conditional_losses_7811022/
-nmn10_lle_2000_1000_1/StatefulPartitionedCall�
0nmn10_isomap_2000_1000_1/StatefulPartitionedCallStatefulPartitionedCallinputs_2nmn10_isomap_2000_1000_1_781120nmn10_isomap_2000_1000_1_781122*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *]
fXRV
T__inference_nmn10_isomap_2000_1000_1_layer_call_and_return_conditional_losses_78111922
0nmn10_isomap_2000_1000_1/StatefulPartitionedCall�
.nmn10_tsvd_2000_1000_1/StatefulPartitionedCallStatefulPartitionedCallinputs_1nmn10_tsvd_2000_1000_1_781137nmn10_tsvd_2000_1000_1_781139*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_nmn10_tsvd_2000_1000_1_layer_call_and_return_conditional_losses_78113620
.nmn10_tsvd_2000_1000_1/StatefulPartitionedCall�
-nmn10_pca_2000_1000_1/StatefulPartitionedCallStatefulPartitionedCallinputsnmn10_pca_2000_1000_1_781154nmn10_pca_2000_1000_1_781156*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_nmn10_pca_2000_1000_1_layer_call_and_return_conditional_losses_7811532/
-nmn10_pca_2000_1000_1/StatefulPartitionedCall�
-nmn10_lle_2000_1000_2/StatefulPartitionedCallStatefulPartitionedCall6nmn10_lle_2000_1000_1/StatefulPartitionedCall:output:0nmn10_lle_2000_1000_2_781171nmn10_lle_2000_1000_2_781173*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_nmn10_lle_2000_1000_2_layer_call_and_return_conditional_losses_7811702/
-nmn10_lle_2000_1000_2/StatefulPartitionedCall�
0nmn10_isomap_2000_1000_2/StatefulPartitionedCallStatefulPartitionedCall9nmn10_isomap_2000_1000_1/StatefulPartitionedCall:output:0nmn10_isomap_2000_1000_2_781188nmn10_isomap_2000_1000_2_781190*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *]
fXRV
T__inference_nmn10_isomap_2000_1000_2_layer_call_and_return_conditional_losses_78118722
0nmn10_isomap_2000_1000_2/StatefulPartitionedCall�
.nmn10_tsvd_2000_1000_2/StatefulPartitionedCallStatefulPartitionedCall7nmn10_tsvd_2000_1000_1/StatefulPartitionedCall:output:0nmn10_tsvd_2000_1000_2_781205nmn10_tsvd_2000_1000_2_781207*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_nmn10_tsvd_2000_1000_2_layer_call_and_return_conditional_losses_78120420
.nmn10_tsvd_2000_1000_2/StatefulPartitionedCall�
-nmn10_pca_2000_1000_2/StatefulPartitionedCallStatefulPartitionedCall6nmn10_pca_2000_1000_1/StatefulPartitionedCall:output:0nmn10_pca_2000_1000_2_781222nmn10_pca_2000_1000_2_781224*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_nmn10_pca_2000_1000_2_layer_call_and_return_conditional_losses_7812212/
-nmn10_pca_2000_1000_2/StatefulPartitionedCall�
-nmn10_lle_2000_1000_3/StatefulPartitionedCallStatefulPartitionedCall6nmn10_lle_2000_1000_2/StatefulPartitionedCall:output:0nmn10_lle_2000_1000_3_781239nmn10_lle_2000_1000_3_781241*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_nmn10_lle_2000_1000_3_layer_call_and_return_conditional_losses_7812382/
-nmn10_lle_2000_1000_3/StatefulPartitionedCall�
0nmn10_isomap_2000_1000_3/StatefulPartitionedCallStatefulPartitionedCall9nmn10_isomap_2000_1000_2/StatefulPartitionedCall:output:0nmn10_isomap_2000_1000_3_781256nmn10_isomap_2000_1000_3_781258*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *]
fXRV
T__inference_nmn10_isomap_2000_1000_3_layer_call_and_return_conditional_losses_78125522
0nmn10_isomap_2000_1000_3/StatefulPartitionedCall�
.nmn10_tsvd_2000_1000_3/StatefulPartitionedCallStatefulPartitionedCall7nmn10_tsvd_2000_1000_2/StatefulPartitionedCall:output:0nmn10_tsvd_2000_1000_3_781273nmn10_tsvd_2000_1000_3_781275*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_nmn10_tsvd_2000_1000_3_layer_call_and_return_conditional_losses_78127220
.nmn10_tsvd_2000_1000_3/StatefulPartitionedCall�
-nmn10_pca_2000_1000_3/StatefulPartitionedCallStatefulPartitionedCall6nmn10_pca_2000_1000_2/StatefulPartitionedCall:output:0nmn10_pca_2000_1000_3_781290nmn10_pca_2000_1000_3_781292*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_nmn10_pca_2000_1000_3_layer_call_and_return_conditional_losses_7812892/
-nmn10_pca_2000_1000_3/StatefulPartitionedCall�
/nmn10_pca_2000_1000_out/StatefulPartitionedCallStatefulPartitionedCall6nmn10_pca_2000_1000_3/StatefulPartitionedCall:output:0nmn10_pca_2000_1000_out_781307nmn10_pca_2000_1000_out_781309*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *\
fWRU
S__inference_nmn10_pca_2000_1000_out_layer_call_and_return_conditional_losses_78130621
/nmn10_pca_2000_1000_out/StatefulPartitionedCall�
0nmn10_tsvd_2000_1000_out/StatefulPartitionedCallStatefulPartitionedCall7nmn10_tsvd_2000_1000_3/StatefulPartitionedCall:output:0nmn10_tsvd_2000_1000_out_781324nmn10_tsvd_2000_1000_out_781326*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *]
fXRV
T__inference_nmn10_tsvd_2000_1000_out_layer_call_and_return_conditional_losses_78132322
0nmn10_tsvd_2000_1000_out/StatefulPartitionedCall�
2nmn10_isomap_2000_1000_out/StatefulPartitionedCallStatefulPartitionedCall9nmn10_isomap_2000_1000_3/StatefulPartitionedCall:output:0!nmn10_isomap_2000_1000_out_781341!nmn10_isomap_2000_1000_out_781343*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *_
fZRX
V__inference_nmn10_isomap_2000_1000_out_layer_call_and_return_conditional_losses_78134024
2nmn10_isomap_2000_1000_out/StatefulPartitionedCall�
/nmn10_lle_2000_1000_out/StatefulPartitionedCallStatefulPartitionedCall6nmn10_lle_2000_1000_3/StatefulPartitionedCall:output:0nmn10_lle_2000_1000_out_781358nmn10_lle_2000_1000_out_781360*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *\
fWRU
S__inference_nmn10_lle_2000_1000_out_layer_call_and_return_conditional_losses_78135721
/nmn10_lle_2000_1000_out/StatefulPartitionedCall�
concatenate_24/PartitionedCallPartitionedCall8nmn10_pca_2000_1000_out/StatefulPartitionedCall:output:09nmn10_tsvd_2000_1000_out/StatefulPartitionedCall:output:0;nmn10_isomap_2000_1000_out/StatefulPartitionedCall:output:08nmn10_lle_2000_1000_out/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *S
fNRL
J__inference_concatenate_24_layer_call_and_return_conditional_losses_7813722 
concatenate_24/PartitionedCall�
joined/StatefulPartitionedCallStatefulPartitionedCall'concatenate_24/PartitionedCall:output:0joined_781386joined_781388*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *K
fFRD
B__inference_joined_layer_call_and_return_conditional_losses_7813852 
joined/StatefulPartitionedCall�
IdentityIdentity'joined/StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identity�
NoOpNoOp^joined/StatefulPartitionedCall1^nmn10_isomap_2000_1000_1/StatefulPartitionedCall1^nmn10_isomap_2000_1000_2/StatefulPartitionedCall1^nmn10_isomap_2000_1000_3/StatefulPartitionedCall3^nmn10_isomap_2000_1000_out/StatefulPartitionedCall.^nmn10_lle_2000_1000_1/StatefulPartitionedCall.^nmn10_lle_2000_1000_2/StatefulPartitionedCall.^nmn10_lle_2000_1000_3/StatefulPartitionedCall0^nmn10_lle_2000_1000_out/StatefulPartitionedCall.^nmn10_pca_2000_1000_1/StatefulPartitionedCall.^nmn10_pca_2000_1000_2/StatefulPartitionedCall.^nmn10_pca_2000_1000_3/StatefulPartitionedCall0^nmn10_pca_2000_1000_out/StatefulPartitionedCall/^nmn10_tsvd_2000_1000_1/StatefulPartitionedCall/^nmn10_tsvd_2000_1000_2/StatefulPartitionedCall/^nmn10_tsvd_2000_1000_3/StatefulPartitionedCall1^nmn10_tsvd_2000_1000_out/StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*�
_input_shapes�
�:����������:����������:����������:����������: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 2@
joined/StatefulPartitionedCalljoined/StatefulPartitionedCall2d
0nmn10_isomap_2000_1000_1/StatefulPartitionedCall0nmn10_isomap_2000_1000_1/StatefulPartitionedCall2d
0nmn10_isomap_2000_1000_2/StatefulPartitionedCall0nmn10_isomap_2000_1000_2/StatefulPartitionedCall2d
0nmn10_isomap_2000_1000_3/StatefulPartitionedCall0nmn10_isomap_2000_1000_3/StatefulPartitionedCall2h
2nmn10_isomap_2000_1000_out/StatefulPartitionedCall2nmn10_isomap_2000_1000_out/StatefulPartitionedCall2^
-nmn10_lle_2000_1000_1/StatefulPartitionedCall-nmn10_lle_2000_1000_1/StatefulPartitionedCall2^
-nmn10_lle_2000_1000_2/StatefulPartitionedCall-nmn10_lle_2000_1000_2/StatefulPartitionedCall2^
-nmn10_lle_2000_1000_3/StatefulPartitionedCall-nmn10_lle_2000_1000_3/StatefulPartitionedCall2b
/nmn10_lle_2000_1000_out/StatefulPartitionedCall/nmn10_lle_2000_1000_out/StatefulPartitionedCall2^
-nmn10_pca_2000_1000_1/StatefulPartitionedCall-nmn10_pca_2000_1000_1/StatefulPartitionedCall2^
-nmn10_pca_2000_1000_2/StatefulPartitionedCall-nmn10_pca_2000_1000_2/StatefulPartitionedCall2^
-nmn10_pca_2000_1000_3/StatefulPartitionedCall-nmn10_pca_2000_1000_3/StatefulPartitionedCall2b
/nmn10_pca_2000_1000_out/StatefulPartitionedCall/nmn10_pca_2000_1000_out/StatefulPartitionedCall2`
.nmn10_tsvd_2000_1000_1/StatefulPartitionedCall.nmn10_tsvd_2000_1000_1/StatefulPartitionedCall2`
.nmn10_tsvd_2000_1000_2/StatefulPartitionedCall.nmn10_tsvd_2000_1000_2/StatefulPartitionedCall2`
.nmn10_tsvd_2000_1000_3/StatefulPartitionedCall.nmn10_tsvd_2000_1000_3/StatefulPartitionedCall2d
0nmn10_tsvd_2000_1000_out/StatefulPartitionedCall0nmn10_tsvd_2000_1000_out/StatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs:PL
(
_output_shapes
:����������
 
_user_specified_nameinputs:PL
(
_output_shapes
:����������
 
_user_specified_nameinputs:PL
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
Q__inference_nmn10_pca_2000_1000_1_layer_call_and_return_conditional_losses_781153

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
&__inference_model_layer_call_fn_782385
inputs_0
inputs_1
inputs_2
inputs_3
unknown:
��
	unknown_0:	�
	unknown_1:
��
	unknown_2:	�
	unknown_3:
��
	unknown_4:	�
	unknown_5:
��
	unknown_6:	�
	unknown_7:
��
	unknown_8:	�
	unknown_9:
��

unknown_10:	�

unknown_11:
��

unknown_12:	�

unknown_13:
��

unknown_14:	�

unknown_15:
��

unknown_16:	�

unknown_17:
��

unknown_18:	�

unknown_19:
��

unknown_20:	�

unknown_21:
��

unknown_22:	�

unknown_23:	�

unknown_24:

unknown_25:	�

unknown_26:

unknown_27:	�

unknown_28:

unknown_29:	�

unknown_30:

unknown_31:

unknown_32:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputs_0inputs_1inputs_2inputs_3unknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10
unknown_11
unknown_12
unknown_13
unknown_14
unknown_15
unknown_16
unknown_17
unknown_18
unknown_19
unknown_20
unknown_21
unknown_22
unknown_23
unknown_24
unknown_25
unknown_26
unknown_27
unknown_28
unknown_29
unknown_30
unknown_31
unknown_32*1
Tin*
(2&*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*D
_read_only_resource_inputs&
$"	
 !"#$%*0
config_proto 

CPU

GPU2*0J 8� *J
fERC
A__inference_model_layer_call_and_return_conditional_losses_7818162
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*�
_input_shapes�
�:����������:����������:����������:����������: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:R N
(
_output_shapes
:����������
"
_user_specified_name
inputs/0:RN
(
_output_shapes
:����������
"
_user_specified_name
inputs/1:RN
(
_output_shapes
:����������
"
_user_specified_name
inputs/2:RN
(
_output_shapes
:����������
"
_user_specified_name
inputs/3
�
�
S__inference_nmn10_lle_2000_1000_out_layer_call_and_return_conditional_losses_781357

inputs1
matmul_readvariableop_resource:	�-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes
:	�*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2	
BiasAdda
SigmoidSigmoidBiasAdd:output:0*
T0*'
_output_shapes
:���������2	
Sigmoidf
IdentityIdentitySigmoid:y:0^NoOp*
T0*'
_output_shapes
:���������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
T__inference_nmn10_tsvd_2000_1000_out_layer_call_and_return_conditional_losses_781323

inputs1
matmul_readvariableop_resource:	�-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes
:	�*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2	
BiasAdda
SigmoidSigmoidBiasAdd:output:0*
T0*'
_output_shapes
:���������2	
Sigmoidf
IdentityIdentitySigmoid:y:0^NoOp*
T0*'
_output_shapes
:���������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
Q__inference_nmn10_pca_2000_1000_2_layer_call_and_return_conditional_losses_782741

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
R__inference_nmn10_tsvd_2000_1000_2_layer_call_and_return_conditional_losses_782761

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
9__inference_nmn10_isomap_2000_1000_3_layer_call_fn_782850

inputs
unknown:
��
	unknown_0:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *]
fXRV
T__inference_nmn10_isomap_2000_1000_3_layer_call_and_return_conditional_losses_7812552
StatefulPartitionedCall|
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:����������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
6__inference_nmn10_lle_2000_1000_3_layer_call_fn_782870

inputs
unknown:
��
	unknown_0:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_nmn10_lle_2000_1000_3_layer_call_and_return_conditional_losses_7812382
StatefulPartitionedCall|
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:����������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
B__inference_joined_layer_call_and_return_conditional_losses_782998

inputs0
matmul_readvariableop_resource:-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2	
BiasAdda
SigmoidSigmoidBiasAdd:output:0*
T0*'
_output_shapes
:���������2	
Sigmoidf
IdentityIdentitySigmoid:y:0^NoOp*
T0*'
_output_shapes
:���������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs
�
�
R__inference_nmn10_tsvd_2000_1000_1_layer_call_and_return_conditional_losses_781136

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
J__inference_concatenate_24_layer_call_and_return_conditional_losses_781372

inputs
inputs_1
inputs_2
inputs_3
identity\
concat/axisConst*
_output_shapes
: *
dtype0*
value	B :2
concat/axis�
concatConcatV2inputsinputs_1inputs_2inputs_3concat/axis:output:0*
N*
T0*'
_output_shapes
:���������2
concatc
IdentityIdentityconcat:output:0*
T0*'
_output_shapes
:���������2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*_
_input_shapesN
L:���������:���������:���������:���������:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs:OK
'
_output_shapes
:���������
 
_user_specified_nameinputs:OK
'
_output_shapes
:���������
 
_user_specified_nameinputs:OK
'
_output_shapes
:���������
 
_user_specified_nameinputs
�
�
Q__inference_nmn10_lle_2000_1000_3_layer_call_and_return_conditional_losses_782881

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
9__inference_nmn10_tsvd_2000_1000_out_layer_call_fn_782910

inputs
unknown:	�
	unknown_0:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *]
fXRV
T__inference_nmn10_tsvd_2000_1000_out_layer_call_and_return_conditional_losses_7813232
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
Q__inference_nmn10_lle_2000_1000_1_layer_call_and_return_conditional_losses_782721

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
R__inference_nmn10_tsvd_2000_1000_3_layer_call_and_return_conditional_losses_782841

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs"�L
saver_filename:0StatefulPartitionedCall_1:0StatefulPartitionedCall_28"
saved_model_main_op

NoOp*>
__saved_model_init_op%#
__saved_model_init_op

NoOp*�
serving_default�
f
nmn10_isomap_2000_1000_inputF
.serving_default_nmn10_isomap_2000_1000_input:0����������
`
nmn10_lle_2000_1000_inputC
+serving_default_nmn10_lle_2000_1000_input:0����������
`
nmn10_pca_2000_1000_inputC
+serving_default_nmn10_pca_2000_1000_input:0����������
b
nmn10_tsvd_2000_1000_inputD
,serving_default_nmn10_tsvd_2000_1000_input:0����������:
joined0
StatefulPartitionedCall:0���������tensorflow/serving/predict:��
�
layer-0
layer-1
layer-2
layer-3
layer_with_weights-0
layer-4
layer_with_weights-1
layer-5
layer_with_weights-2
layer-6
layer_with_weights-3
layer-7
	layer_with_weights-4
	layer-8

layer_with_weights-5

layer-9
layer_with_weights-6
layer-10
layer_with_weights-7
layer-11
layer_with_weights-8
layer-12
layer_with_weights-9
layer-13
layer_with_weights-10
layer-14
layer_with_weights-11
layer-15
layer_with_weights-12
layer-16
layer_with_weights-13
layer-17
layer_with_weights-14
layer-18
layer_with_weights-15
layer-19
layer-20
layer_with_weights-16
layer-21
	optimizer
	variables
regularization_losses
trainable_variables
	keras_api

signatures
�__call__
�_default_save_signature
+�&call_and_return_all_conditional_losses"
_tf_keras_network
D
#_self_saveable_object_factories"
_tf_keras_input_layer
D
#_self_saveable_object_factories"
_tf_keras_input_layer
D
#_self_saveable_object_factories"
_tf_keras_input_layer
D
# _self_saveable_object_factories"
_tf_keras_input_layer
�

!kernel
"bias
##_self_saveable_object_factories
$	variables
%regularization_losses
&trainable_variables
'	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�

(kernel
)bias
#*_self_saveable_object_factories
+	variables
,regularization_losses
-trainable_variables
.	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�

/kernel
0bias
#1_self_saveable_object_factories
2	variables
3regularization_losses
4trainable_variables
5	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�

6kernel
7bias
#8_self_saveable_object_factories
9	variables
:regularization_losses
;trainable_variables
<	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�

=kernel
>bias
#?_self_saveable_object_factories
@	variables
Aregularization_losses
Btrainable_variables
C	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�

Dkernel
Ebias
#F_self_saveable_object_factories
G	variables
Hregularization_losses
Itrainable_variables
J	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�

Kkernel
Lbias
#M_self_saveable_object_factories
N	variables
Oregularization_losses
Ptrainable_variables
Q	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�

Rkernel
Sbias
#T_self_saveable_object_factories
U	variables
Vregularization_losses
Wtrainable_variables
X	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�

Ykernel
Zbias
#[_self_saveable_object_factories
\	variables
]regularization_losses
^trainable_variables
_	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�

`kernel
abias
#b_self_saveable_object_factories
c	variables
dregularization_losses
etrainable_variables
f	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�

gkernel
hbias
#i_self_saveable_object_factories
j	variables
kregularization_losses
ltrainable_variables
m	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�

nkernel
obias
#p_self_saveable_object_factories
q	variables
rregularization_losses
strainable_variables
t	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�

ukernel
vbias
#w_self_saveable_object_factories
x	variables
yregularization_losses
ztrainable_variables
{	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�

|kernel
}bias
#~_self_saveable_object_factories
	variables
�regularization_losses
�trainable_variables
�	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�
�kernel
	�bias
$�_self_saveable_object_factories
�	variables
�regularization_losses
�trainable_variables
�	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�
�kernel
	�bias
$�_self_saveable_object_factories
�	variables
�regularization_losses
�trainable_variables
�	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�
�	variables
�regularization_losses
�trainable_variables
�	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�
�kernel
	�bias
�	variables
�regularization_losses
�trainable_variables
�	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�
	�iter
�beta_1
�beta_2

�decay
�learning_rate	�m�	�m�	�v�	�v�"
	optimizer
�
!0
"1
(2
)3
/4
05
66
77
=8
>9
D10
E11
K12
L13
R14
S15
Y16
Z17
`18
a19
g20
h21
n22
o23
u24
v25
|26
}27
�28
�29
�30
�31
�32
�33"
trackable_list_wrapper
 "
trackable_list_wrapper
0
�0
�1"
trackable_list_wrapper
�
�metrics
�layers
	variables
�layer_metrics
 �layer_regularization_losses
regularization_losses
trainable_variables
�non_trainable_variables
�__call__
�_default_save_signature
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
-
�serving_default"
signature_map
 "
trackable_dict_wrapper
 "
trackable_dict_wrapper
 "
trackable_dict_wrapper
 "
trackable_dict_wrapper
0:.
��2nmn10_pca_2000_1000_1/kernel
):'�2nmn10_pca_2000_1000_1/bias
 "
trackable_dict_wrapper
.
!0
"1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�metrics
�layers
$	variables
%regularization_losses
�layer_metrics
 �layer_regularization_losses
&trainable_variables
�non_trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
1:/
��2nmn10_tsvd_2000_1000_1/kernel
*:(�2nmn10_tsvd_2000_1000_1/bias
 "
trackable_dict_wrapper
.
(0
)1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�metrics
�layers
+	variables
,regularization_losses
�layer_metrics
 �layer_regularization_losses
-trainable_variables
�non_trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
3:1
��2nmn10_isomap_2000_1000_1/kernel
,:*�2nmn10_isomap_2000_1000_1/bias
 "
trackable_dict_wrapper
.
/0
01"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�metrics
�layers
2	variables
3regularization_losses
�layer_metrics
 �layer_regularization_losses
4trainable_variables
�non_trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
0:.
��2nmn10_lle_2000_1000_1/kernel
):'�2nmn10_lle_2000_1000_1/bias
 "
trackable_dict_wrapper
.
60
71"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�metrics
�layers
9	variables
:regularization_losses
�layer_metrics
 �layer_regularization_losses
;trainable_variables
�non_trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
0:.
��2nmn10_pca_2000_1000_2/kernel
):'�2nmn10_pca_2000_1000_2/bias
 "
trackable_dict_wrapper
.
=0
>1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�metrics
�layers
@	variables
Aregularization_losses
�layer_metrics
 �layer_regularization_losses
Btrainable_variables
�non_trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
1:/
��2nmn10_tsvd_2000_1000_2/kernel
*:(�2nmn10_tsvd_2000_1000_2/bias
 "
trackable_dict_wrapper
.
D0
E1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�metrics
�layers
G	variables
Hregularization_losses
�layer_metrics
 �layer_regularization_losses
Itrainable_variables
�non_trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
3:1
��2nmn10_isomap_2000_1000_2/kernel
,:*�2nmn10_isomap_2000_1000_2/bias
 "
trackable_dict_wrapper
.
K0
L1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�metrics
�layers
N	variables
Oregularization_losses
�layer_metrics
 �layer_regularization_losses
Ptrainable_variables
�non_trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
0:.
��2nmn10_lle_2000_1000_2/kernel
):'�2nmn10_lle_2000_1000_2/bias
 "
trackable_dict_wrapper
.
R0
S1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�metrics
�layers
U	variables
Vregularization_losses
�layer_metrics
 �layer_regularization_losses
Wtrainable_variables
�non_trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
0:.
��2nmn10_pca_2000_1000_3/kernel
):'�2nmn10_pca_2000_1000_3/bias
 "
trackable_dict_wrapper
.
Y0
Z1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�metrics
�layers
\	variables
]regularization_losses
�layer_metrics
 �layer_regularization_losses
^trainable_variables
�non_trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
1:/
��2nmn10_tsvd_2000_1000_3/kernel
*:(�2nmn10_tsvd_2000_1000_3/bias
 "
trackable_dict_wrapper
.
`0
a1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�metrics
�layers
c	variables
dregularization_losses
�layer_metrics
 �layer_regularization_losses
etrainable_variables
�non_trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
3:1
��2nmn10_isomap_2000_1000_3/kernel
,:*�2nmn10_isomap_2000_1000_3/bias
 "
trackable_dict_wrapper
.
g0
h1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�metrics
�layers
j	variables
kregularization_losses
�layer_metrics
 �layer_regularization_losses
ltrainable_variables
�non_trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
0:.
��2nmn10_lle_2000_1000_3/kernel
):'�2nmn10_lle_2000_1000_3/bias
 "
trackable_dict_wrapper
.
n0
o1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�metrics
�layers
q	variables
rregularization_losses
�layer_metrics
 �layer_regularization_losses
strainable_variables
�non_trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
1:/	�2nmn10_pca_2000_1000_out/kernel
*:(2nmn10_pca_2000_1000_out/bias
 "
trackable_dict_wrapper
.
u0
v1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�metrics
�layers
x	variables
yregularization_losses
�layer_metrics
 �layer_regularization_losses
ztrainable_variables
�non_trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
2:0	�2nmn10_tsvd_2000_1000_out/kernel
+:)2nmn10_tsvd_2000_1000_out/bias
 "
trackable_dict_wrapper
.
|0
}1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�metrics
�layers
	variables
�regularization_losses
�layer_metrics
 �layer_regularization_losses
�trainable_variables
�non_trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
4:2	�2!nmn10_isomap_2000_1000_out/kernel
-:+2nmn10_isomap_2000_1000_out/bias
 "
trackable_dict_wrapper
0
�0
�1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�metrics
�layers
�	variables
�regularization_losses
�layer_metrics
 �layer_regularization_losses
�trainable_variables
�non_trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
1:/	�2nmn10_lle_2000_1000_out/kernel
*:(2nmn10_lle_2000_1000_out/bias
 "
trackable_dict_wrapper
0
�0
�1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�metrics
�layers
�	variables
�regularization_losses
�layer_metrics
 �layer_regularization_losses
�trainable_variables
�non_trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�metrics
�layers
�	variables
�regularization_losses
�layer_metrics
 �layer_regularization_losses
�trainable_variables
�non_trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
:2joined/kernel
:2joined/bias
0
�0
�1"
trackable_list_wrapper
 "
trackable_list_wrapper
0
�0
�1"
trackable_list_wrapper
�
�metrics
�layers
�	variables
�regularization_losses
�layer_metrics
 �layer_regularization_losses
�trainable_variables
�non_trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
:	 (2	Adam/iter
: (2Adam/beta_1
: (2Adam/beta_2
: (2
Adam/decay
: (2Adam/learning_rate
0
�0
�1"
trackable_list_wrapper
�
0
1
2
3
4
5
6
7
	8

9
10
11
12
13
14
15
16
17
18
19
20
21"
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
�
!0
"1
(2
)3
/4
05
66
77
=8
>9
D10
E11
K12
L13
R14
S15
Y16
Z17
`18
a19
g20
h21
n22
o23
u24
v25
|26
}27
�28
�29
�30
�31"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
.
!0
"1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
.
(0
)1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
.
/0
01"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
.
60
71"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
.
=0
>1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
.
D0
E1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
.
K0
L1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
.
R0
S1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
.
Y0
Z1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
.
`0
a1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
.
g0
h1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
.
n0
o1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
.
u0
v1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
.
|0
}1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
0
�0
�1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
0
�0
�1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
R

�total

�count
�	variables
�	keras_api"
_tf_keras_metric
c

�total

�count
�
_fn_kwargs
�	variables
�	keras_api"
_tf_keras_metric
:  (2total
:  (2count
0
�0
�1"
trackable_list_wrapper
.
�	variables"
_generic_user_object
:  (2total
:  (2count
 "
trackable_dict_wrapper
0
�0
�1"
trackable_list_wrapper
.
�	variables"
_generic_user_object
$:"2Adam/joined/kernel/m
:2Adam/joined/bias/m
$:"2Adam/joined/kernel/v
:2Adam/joined/bias/v
�2�
&__inference_model_layer_call_fn_781463
&__inference_model_layer_call_fn_782309
&__inference_model_layer_call_fn_782385
&__inference_model_layer_call_fn_781963�
���
FullArgSpec1
args)�&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�B�
!__inference__wrapped_model_781078nmn10_pca_2000_1000_inputnmn10_tsvd_2000_1000_inputnmn10_isomap_2000_1000_inputnmn10_lle_2000_1000_input"�
���
FullArgSpec
args� 
varargsjargs
varkwjkwargs
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
A__inference_model_layer_call_and_return_conditional_losses_782513
A__inference_model_layer_call_and_return_conditional_losses_782641
A__inference_model_layer_call_and_return_conditional_losses_782056
A__inference_model_layer_call_and_return_conditional_losses_782149�
���
FullArgSpec1
args)�&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2�
6__inference_nmn10_pca_2000_1000_1_layer_call_fn_782650�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
Q__inference_nmn10_pca_2000_1000_1_layer_call_and_return_conditional_losses_782661�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
7__inference_nmn10_tsvd_2000_1000_1_layer_call_fn_782670�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
R__inference_nmn10_tsvd_2000_1000_1_layer_call_and_return_conditional_losses_782681�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
9__inference_nmn10_isomap_2000_1000_1_layer_call_fn_782690�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
T__inference_nmn10_isomap_2000_1000_1_layer_call_and_return_conditional_losses_782701�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
6__inference_nmn10_lle_2000_1000_1_layer_call_fn_782710�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
Q__inference_nmn10_lle_2000_1000_1_layer_call_and_return_conditional_losses_782721�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
6__inference_nmn10_pca_2000_1000_2_layer_call_fn_782730�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
Q__inference_nmn10_pca_2000_1000_2_layer_call_and_return_conditional_losses_782741�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
7__inference_nmn10_tsvd_2000_1000_2_layer_call_fn_782750�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
R__inference_nmn10_tsvd_2000_1000_2_layer_call_and_return_conditional_losses_782761�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
9__inference_nmn10_isomap_2000_1000_2_layer_call_fn_782770�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
T__inference_nmn10_isomap_2000_1000_2_layer_call_and_return_conditional_losses_782781�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
6__inference_nmn10_lle_2000_1000_2_layer_call_fn_782790�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
Q__inference_nmn10_lle_2000_1000_2_layer_call_and_return_conditional_losses_782801�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
6__inference_nmn10_pca_2000_1000_3_layer_call_fn_782810�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
Q__inference_nmn10_pca_2000_1000_3_layer_call_and_return_conditional_losses_782821�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
7__inference_nmn10_tsvd_2000_1000_3_layer_call_fn_782830�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
R__inference_nmn10_tsvd_2000_1000_3_layer_call_and_return_conditional_losses_782841�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
9__inference_nmn10_isomap_2000_1000_3_layer_call_fn_782850�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
T__inference_nmn10_isomap_2000_1000_3_layer_call_and_return_conditional_losses_782861�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
6__inference_nmn10_lle_2000_1000_3_layer_call_fn_782870�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
Q__inference_nmn10_lle_2000_1000_3_layer_call_and_return_conditional_losses_782881�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
8__inference_nmn10_pca_2000_1000_out_layer_call_fn_782890�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
S__inference_nmn10_pca_2000_1000_out_layer_call_and_return_conditional_losses_782901�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
9__inference_nmn10_tsvd_2000_1000_out_layer_call_fn_782910�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
T__inference_nmn10_tsvd_2000_1000_out_layer_call_and_return_conditional_losses_782921�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
;__inference_nmn10_isomap_2000_1000_out_layer_call_fn_782930�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
V__inference_nmn10_isomap_2000_1000_out_layer_call_and_return_conditional_losses_782941�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
8__inference_nmn10_lle_2000_1000_out_layer_call_fn_782950�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
S__inference_nmn10_lle_2000_1000_out_layer_call_and_return_conditional_losses_782961�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
/__inference_concatenate_24_layer_call_fn_782969�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
J__inference_concatenate_24_layer_call_and_return_conditional_losses_782978�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
'__inference_joined_layer_call_fn_782987�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
B__inference_joined_layer_call_and_return_conditional_losses_782998�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
$__inference_signature_wrapper_782233nmn10_isomap_2000_1000_inputnmn10_lle_2000_1000_inputnmn10_pca_2000_1000_inputnmn10_tsvd_2000_1000_input"�
���
FullArgSpec
args� 
varargs
 
varkwjkwargs
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 �
!__inference__wrapped_model_781078�(67/0()!"RSKLDE=>nogh`aYZuv|}���������
���
���
4�1
nmn10_pca_2000_1000_input����������
5�2
nmn10_tsvd_2000_1000_input����������
7�4
nmn10_isomap_2000_1000_input����������
4�1
nmn10_lle_2000_1000_input����������
� "/�,
*
joined �
joined����������
J__inference_concatenate_24_layer_call_and_return_conditional_losses_782978����
���
���
"�
inputs/0���������
"�
inputs/1���������
"�
inputs/2���������
"�
inputs/3���������
� "%�"
�
0���������
� �
/__inference_concatenate_24_layer_call_fn_782969����
���
���
"�
inputs/0���������
"�
inputs/1���������
"�
inputs/2���������
"�
inputs/3���������
� "�����������
B__inference_joined_layer_call_and_return_conditional_losses_782998^��/�,
%�"
 �
inputs���������
� "%�"
�
0���������
� |
'__inference_joined_layer_call_fn_782987Q��/�,
%�"
 �
inputs���������
� "�����������
A__inference_model_layer_call_and_return_conditional_losses_782056�(67/0()!"RSKLDE=>nogh`aYZuv|}���������
���
���
4�1
nmn10_pca_2000_1000_input����������
5�2
nmn10_tsvd_2000_1000_input����������
7�4
nmn10_isomap_2000_1000_input����������
4�1
nmn10_lle_2000_1000_input����������
p 

 
� "%�"
�
0���������
� �
A__inference_model_layer_call_and_return_conditional_losses_782149�(67/0()!"RSKLDE=>nogh`aYZuv|}���������
���
���
4�1
nmn10_pca_2000_1000_input����������
5�2
nmn10_tsvd_2000_1000_input����������
7�4
nmn10_isomap_2000_1000_input����������
4�1
nmn10_lle_2000_1000_input����������
p

 
� "%�"
�
0���������
� �
A__inference_model_layer_call_and_return_conditional_losses_782513�(67/0()!"RSKLDE=>nogh`aYZuv|}���������
���
���
#� 
inputs/0����������
#� 
inputs/1����������
#� 
inputs/2����������
#� 
inputs/3����������
p 

 
� "%�"
�
0���������
� �
A__inference_model_layer_call_and_return_conditional_losses_782641�(67/0()!"RSKLDE=>nogh`aYZuv|}���������
���
���
#� 
inputs/0����������
#� 
inputs/1����������
#� 
inputs/2����������
#� 
inputs/3����������
p

 
� "%�"
�
0���������
� �
&__inference_model_layer_call_fn_781463�(67/0()!"RSKLDE=>nogh`aYZuv|}���������
���
���
4�1
nmn10_pca_2000_1000_input����������
5�2
nmn10_tsvd_2000_1000_input����������
7�4
nmn10_isomap_2000_1000_input����������
4�1
nmn10_lle_2000_1000_input����������
p 

 
� "�����������
&__inference_model_layer_call_fn_781963�(67/0()!"RSKLDE=>nogh`aYZuv|}���������
���
���
4�1
nmn10_pca_2000_1000_input����������
5�2
nmn10_tsvd_2000_1000_input����������
7�4
nmn10_isomap_2000_1000_input����������
4�1
nmn10_lle_2000_1000_input����������
p

 
� "�����������
&__inference_model_layer_call_fn_782309�(67/0()!"RSKLDE=>nogh`aYZuv|}���������
���
���
#� 
inputs/0����������
#� 
inputs/1����������
#� 
inputs/2����������
#� 
inputs/3����������
p 

 
� "�����������
&__inference_model_layer_call_fn_782385�(67/0()!"RSKLDE=>nogh`aYZuv|}���������
���
���
#� 
inputs/0����������
#� 
inputs/1����������
#� 
inputs/2����������
#� 
inputs/3����������
p

 
� "�����������
T__inference_nmn10_isomap_2000_1000_1_layer_call_and_return_conditional_losses_782701^/00�-
&�#
!�
inputs����������
� "&�#
�
0����������
� �
9__inference_nmn10_isomap_2000_1000_1_layer_call_fn_782690Q/00�-
&�#
!�
inputs����������
� "������������
T__inference_nmn10_isomap_2000_1000_2_layer_call_and_return_conditional_losses_782781^KL0�-
&�#
!�
inputs����������
� "&�#
�
0����������
� �
9__inference_nmn10_isomap_2000_1000_2_layer_call_fn_782770QKL0�-
&�#
!�
inputs����������
� "������������
T__inference_nmn10_isomap_2000_1000_3_layer_call_and_return_conditional_losses_782861^gh0�-
&�#
!�
inputs����������
� "&�#
�
0����������
� �
9__inference_nmn10_isomap_2000_1000_3_layer_call_fn_782850Qgh0�-
&�#
!�
inputs����������
� "������������
V__inference_nmn10_isomap_2000_1000_out_layer_call_and_return_conditional_losses_782941_��0�-
&�#
!�
inputs����������
� "%�"
�
0���������
� �
;__inference_nmn10_isomap_2000_1000_out_layer_call_fn_782930R��0�-
&�#
!�
inputs����������
� "�����������
Q__inference_nmn10_lle_2000_1000_1_layer_call_and_return_conditional_losses_782721^670�-
&�#
!�
inputs����������
� "&�#
�
0����������
� �
6__inference_nmn10_lle_2000_1000_1_layer_call_fn_782710Q670�-
&�#
!�
inputs����������
� "������������
Q__inference_nmn10_lle_2000_1000_2_layer_call_and_return_conditional_losses_782801^RS0�-
&�#
!�
inputs����������
� "&�#
�
0����������
� �
6__inference_nmn10_lle_2000_1000_2_layer_call_fn_782790QRS0�-
&�#
!�
inputs����������
� "������������
Q__inference_nmn10_lle_2000_1000_3_layer_call_and_return_conditional_losses_782881^no0�-
&�#
!�
inputs����������
� "&�#
�
0����������
� �
6__inference_nmn10_lle_2000_1000_3_layer_call_fn_782870Qno0�-
&�#
!�
inputs����������
� "������������
S__inference_nmn10_lle_2000_1000_out_layer_call_and_return_conditional_losses_782961_��0�-
&�#
!�
inputs����������
� "%�"
�
0���������
� �
8__inference_nmn10_lle_2000_1000_out_layer_call_fn_782950R��0�-
&�#
!�
inputs����������
� "�����������
Q__inference_nmn10_pca_2000_1000_1_layer_call_and_return_conditional_losses_782661^!"0�-
&�#
!�
inputs����������
� "&�#
�
0����������
� �
6__inference_nmn10_pca_2000_1000_1_layer_call_fn_782650Q!"0�-
&�#
!�
inputs����������
� "������������
Q__inference_nmn10_pca_2000_1000_2_layer_call_and_return_conditional_losses_782741^=>0�-
&�#
!�
inputs����������
� "&�#
�
0����������
� �
6__inference_nmn10_pca_2000_1000_2_layer_call_fn_782730Q=>0�-
&�#
!�
inputs����������
� "������������
Q__inference_nmn10_pca_2000_1000_3_layer_call_and_return_conditional_losses_782821^YZ0�-
&�#
!�
inputs����������
� "&�#
�
0����������
� �
6__inference_nmn10_pca_2000_1000_3_layer_call_fn_782810QYZ0�-
&�#
!�
inputs����������
� "������������
S__inference_nmn10_pca_2000_1000_out_layer_call_and_return_conditional_losses_782901]uv0�-
&�#
!�
inputs����������
� "%�"
�
0���������
� �
8__inference_nmn10_pca_2000_1000_out_layer_call_fn_782890Puv0�-
&�#
!�
inputs����������
� "�����������
R__inference_nmn10_tsvd_2000_1000_1_layer_call_and_return_conditional_losses_782681^()0�-
&�#
!�
inputs����������
� "&�#
�
0����������
� �
7__inference_nmn10_tsvd_2000_1000_1_layer_call_fn_782670Q()0�-
&�#
!�
inputs����������
� "������������
R__inference_nmn10_tsvd_2000_1000_2_layer_call_and_return_conditional_losses_782761^DE0�-
&�#
!�
inputs����������
� "&�#
�
0����������
� �
7__inference_nmn10_tsvd_2000_1000_2_layer_call_fn_782750QDE0�-
&�#
!�
inputs����������
� "������������
R__inference_nmn10_tsvd_2000_1000_3_layer_call_and_return_conditional_losses_782841^`a0�-
&�#
!�
inputs����������
� "&�#
�
0����������
� �
7__inference_nmn10_tsvd_2000_1000_3_layer_call_fn_782830Q`a0�-
&�#
!�
inputs����������
� "������������
T__inference_nmn10_tsvd_2000_1000_out_layer_call_and_return_conditional_losses_782921]|}0�-
&�#
!�
inputs����������
� "%�"
�
0���������
� �
9__inference_nmn10_tsvd_2000_1000_out_layer_call_fn_782910P|}0�-
&�#
!�
inputs����������
� "�����������
$__inference_signature_wrapper_782233�(67/0()!"RSKLDE=>nogh`aYZuv|}���������
� 
���
W
nmn10_isomap_2000_1000_input7�4
nmn10_isomap_2000_1000_input����������
Q
nmn10_lle_2000_1000_input4�1
nmn10_lle_2000_1000_input����������
Q
nmn10_pca_2000_1000_input4�1
nmn10_pca_2000_1000_input����������
S
nmn10_tsvd_2000_1000_input5�2
nmn10_tsvd_2000_1000_input����������"/�,
*
joined �
joined���������