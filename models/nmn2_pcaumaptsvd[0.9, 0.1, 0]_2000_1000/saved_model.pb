��
��
B
AssignVariableOp
resource
value"dtype"
dtypetype�
~
BiasAdd

value"T	
bias"T
output"T" 
Ttype:
2	"-
data_formatstringNHWC:
NHWCNCHW
h
ConcatV2
values"T*N
axis"Tidx
output"T"
Nint(0"	
Ttype"
Tidxtype0:
2	
8
Const
output"dtype"
valuetensor"
dtypetype
.
Identity

input"T
output"T"	
Ttype
q
MatMul
a"T
b"T
product"T"
transpose_abool( "
transpose_bbool( "
Ttype:

2	
e
MergeV2Checkpoints
checkpoint_prefixes
destination_prefix"
delete_old_dirsbool(�

NoOp
M
Pack
values"T*N
output"T"
Nint(0"	
Ttype"
axisint 
C
Placeholder
output"dtype"
dtypetype"
shapeshape:
@
ReadVariableOp
resource
value"dtype"
dtypetype�
E
Relu
features"T
activations"T"
Ttype:
2	
o
	RestoreV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0�
l
SaveV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0�
?
Select
	condition

t"T
e"T
output"T"	
Ttype
H
ShardedFilename
basename	
shard

num_shards
filename
0
Sigmoid
x"T
y"T"
Ttype:

2
�
StatefulPartitionedCall
args2Tin
output2Tout"
Tin
list(type)("
Tout
list(type)("	
ffunc"
configstring "
config_protostring "
executor_typestring �
@
StaticRegexFullMatch	
input

output
"
patternstring
N

StringJoin
inputs*N

output"
Nint(0"
	separatorstring 
�
VarHandleOp
resource"
	containerstring "
shared_namestring "
dtypetype"
shapeshape"#
allowed_deviceslist(string)
 �"serve*2.6.02unknown8�
�
nmn2_pca_2000_1000_1/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*,
shared_namenmn2_pca_2000_1000_1/kernel
�
/nmn2_pca_2000_1000_1/kernel/Read/ReadVariableOpReadVariableOpnmn2_pca_2000_1000_1/kernel* 
_output_shapes
:
��*
dtype0
�
nmn2_pca_2000_1000_1/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�**
shared_namenmn2_pca_2000_1000_1/bias
�
-nmn2_pca_2000_1000_1/bias/Read/ReadVariableOpReadVariableOpnmn2_pca_2000_1000_1/bias*
_output_shapes	
:�*
dtype0
�
nmn2_umap_2000_1000_1/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*-
shared_namenmn2_umap_2000_1000_1/kernel
�
0nmn2_umap_2000_1000_1/kernel/Read/ReadVariableOpReadVariableOpnmn2_umap_2000_1000_1/kernel* 
_output_shapes
:
��*
dtype0
�
nmn2_umap_2000_1000_1/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*+
shared_namenmn2_umap_2000_1000_1/bias
�
.nmn2_umap_2000_1000_1/bias/Read/ReadVariableOpReadVariableOpnmn2_umap_2000_1000_1/bias*
_output_shapes	
:�*
dtype0
�
nmn2_tsvd_2000_1000_1/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*-
shared_namenmn2_tsvd_2000_1000_1/kernel
�
0nmn2_tsvd_2000_1000_1/kernel/Read/ReadVariableOpReadVariableOpnmn2_tsvd_2000_1000_1/kernel* 
_output_shapes
:
��*
dtype0
�
nmn2_tsvd_2000_1000_1/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*+
shared_namenmn2_tsvd_2000_1000_1/bias
�
.nmn2_tsvd_2000_1000_1/bias/Read/ReadVariableOpReadVariableOpnmn2_tsvd_2000_1000_1/bias*
_output_shapes	
:�*
dtype0
�
nmn2_pca_2000_1000_2/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*,
shared_namenmn2_pca_2000_1000_2/kernel
�
/nmn2_pca_2000_1000_2/kernel/Read/ReadVariableOpReadVariableOpnmn2_pca_2000_1000_2/kernel* 
_output_shapes
:
��*
dtype0
�
nmn2_pca_2000_1000_2/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�**
shared_namenmn2_pca_2000_1000_2/bias
�
-nmn2_pca_2000_1000_2/bias/Read/ReadVariableOpReadVariableOpnmn2_pca_2000_1000_2/bias*
_output_shapes	
:�*
dtype0
�
nmn2_umap_2000_1000_2/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*-
shared_namenmn2_umap_2000_1000_2/kernel
�
0nmn2_umap_2000_1000_2/kernel/Read/ReadVariableOpReadVariableOpnmn2_umap_2000_1000_2/kernel* 
_output_shapes
:
��*
dtype0
�
nmn2_umap_2000_1000_2/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*+
shared_namenmn2_umap_2000_1000_2/bias
�
.nmn2_umap_2000_1000_2/bias/Read/ReadVariableOpReadVariableOpnmn2_umap_2000_1000_2/bias*
_output_shapes	
:�*
dtype0
�
nmn2_tsvd_2000_1000_2/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*-
shared_namenmn2_tsvd_2000_1000_2/kernel
�
0nmn2_tsvd_2000_1000_2/kernel/Read/ReadVariableOpReadVariableOpnmn2_tsvd_2000_1000_2/kernel* 
_output_shapes
:
��*
dtype0
�
nmn2_tsvd_2000_1000_2/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*+
shared_namenmn2_tsvd_2000_1000_2/bias
�
.nmn2_tsvd_2000_1000_2/bias/Read/ReadVariableOpReadVariableOpnmn2_tsvd_2000_1000_2/bias*
_output_shapes	
:�*
dtype0
�
nmn2_pca_2000_1000_3/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*,
shared_namenmn2_pca_2000_1000_3/kernel
�
/nmn2_pca_2000_1000_3/kernel/Read/ReadVariableOpReadVariableOpnmn2_pca_2000_1000_3/kernel* 
_output_shapes
:
��*
dtype0
�
nmn2_pca_2000_1000_3/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�**
shared_namenmn2_pca_2000_1000_3/bias
�
-nmn2_pca_2000_1000_3/bias/Read/ReadVariableOpReadVariableOpnmn2_pca_2000_1000_3/bias*
_output_shapes	
:�*
dtype0
�
nmn2_umap_2000_1000_3/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*-
shared_namenmn2_umap_2000_1000_3/kernel
�
0nmn2_umap_2000_1000_3/kernel/Read/ReadVariableOpReadVariableOpnmn2_umap_2000_1000_3/kernel* 
_output_shapes
:
��*
dtype0
�
nmn2_umap_2000_1000_3/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*+
shared_namenmn2_umap_2000_1000_3/bias
�
.nmn2_umap_2000_1000_3/bias/Read/ReadVariableOpReadVariableOpnmn2_umap_2000_1000_3/bias*
_output_shapes	
:�*
dtype0
�
nmn2_tsvd_2000_1000_3/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*-
shared_namenmn2_tsvd_2000_1000_3/kernel
�
0nmn2_tsvd_2000_1000_3/kernel/Read/ReadVariableOpReadVariableOpnmn2_tsvd_2000_1000_3/kernel* 
_output_shapes
:
��*
dtype0
�
nmn2_tsvd_2000_1000_3/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*+
shared_namenmn2_tsvd_2000_1000_3/bias
�
.nmn2_tsvd_2000_1000_3/bias/Read/ReadVariableOpReadVariableOpnmn2_tsvd_2000_1000_3/bias*
_output_shapes	
:�*
dtype0
�
nmn2_pca_2000_1000_out/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:	�*.
shared_namenmn2_pca_2000_1000_out/kernel
�
1nmn2_pca_2000_1000_out/kernel/Read/ReadVariableOpReadVariableOpnmn2_pca_2000_1000_out/kernel*
_output_shapes
:	�*
dtype0
�
nmn2_pca_2000_1000_out/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:*,
shared_namenmn2_pca_2000_1000_out/bias
�
/nmn2_pca_2000_1000_out/bias/Read/ReadVariableOpReadVariableOpnmn2_pca_2000_1000_out/bias*
_output_shapes
:*
dtype0
�
nmn2_umap_2000_1000_out/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:	�*/
shared_name nmn2_umap_2000_1000_out/kernel
�
2nmn2_umap_2000_1000_out/kernel/Read/ReadVariableOpReadVariableOpnmn2_umap_2000_1000_out/kernel*
_output_shapes
:	�*
dtype0
�
nmn2_umap_2000_1000_out/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:*-
shared_namenmn2_umap_2000_1000_out/bias
�
0nmn2_umap_2000_1000_out/bias/Read/ReadVariableOpReadVariableOpnmn2_umap_2000_1000_out/bias*
_output_shapes
:*
dtype0
�
nmn2_tsvd_2000_1000_out/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:	�*/
shared_name nmn2_tsvd_2000_1000_out/kernel
�
2nmn2_tsvd_2000_1000_out/kernel/Read/ReadVariableOpReadVariableOpnmn2_tsvd_2000_1000_out/kernel*
_output_shapes
:	�*
dtype0
�
nmn2_tsvd_2000_1000_out/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:*-
shared_namenmn2_tsvd_2000_1000_out/bias
�
0nmn2_tsvd_2000_1000_out/bias/Read/ReadVariableOpReadVariableOpnmn2_tsvd_2000_1000_out/bias*
_output_shapes
:*
dtype0
v
joined/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:*
shared_namejoined/kernel
o
!joined/kernel/Read/ReadVariableOpReadVariableOpjoined/kernel*
_output_shapes

:*
dtype0
n
joined/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:*
shared_namejoined/bias
g
joined/bias/Read/ReadVariableOpReadVariableOpjoined/bias*
_output_shapes
:*
dtype0
f
	Adam/iterVarHandleOp*
_output_shapes
: *
dtype0	*
shape: *
shared_name	Adam/iter
_
Adam/iter/Read/ReadVariableOpReadVariableOp	Adam/iter*
_output_shapes
: *
dtype0	
j
Adam/beta_1VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nameAdam/beta_1
c
Adam/beta_1/Read/ReadVariableOpReadVariableOpAdam/beta_1*
_output_shapes
: *
dtype0
j
Adam/beta_2VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nameAdam/beta_2
c
Adam/beta_2/Read/ReadVariableOpReadVariableOpAdam/beta_2*
_output_shapes
: *
dtype0
h

Adam/decayVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name
Adam/decay
a
Adam/decay/Read/ReadVariableOpReadVariableOp
Adam/decay*
_output_shapes
: *
dtype0
x
Adam/learning_rateVarHandleOp*
_output_shapes
: *
dtype0*
shape: *#
shared_nameAdam/learning_rate
q
&Adam/learning_rate/Read/ReadVariableOpReadVariableOpAdam/learning_rate*
_output_shapes
: *
dtype0
^
totalVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nametotal
W
total/Read/ReadVariableOpReadVariableOptotal*
_output_shapes
: *
dtype0
^
countVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_namecount
W
count/Read/ReadVariableOpReadVariableOpcount*
_output_shapes
: *
dtype0
b
total_1VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name	total_1
[
total_1/Read/ReadVariableOpReadVariableOptotal_1*
_output_shapes
: *
dtype0
b
count_1VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name	count_1
[
count_1/Read/ReadVariableOpReadVariableOpcount_1*
_output_shapes
: *
dtype0
�
Adam/joined/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape
:*%
shared_nameAdam/joined/kernel/m
}
(Adam/joined/kernel/m/Read/ReadVariableOpReadVariableOpAdam/joined/kernel/m*
_output_shapes

:*
dtype0
|
Adam/joined/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:*#
shared_nameAdam/joined/bias/m
u
&Adam/joined/bias/m/Read/ReadVariableOpReadVariableOpAdam/joined/bias/m*
_output_shapes
:*
dtype0
�
Adam/joined/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape
:*%
shared_nameAdam/joined/kernel/v
}
(Adam/joined/kernel/v/Read/ReadVariableOpReadVariableOpAdam/joined/kernel/v*
_output_shapes

:*
dtype0
|
Adam/joined/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:*#
shared_nameAdam/joined/bias/v
u
&Adam/joined/bias/v/Read/ReadVariableOpReadVariableOpAdam/joined/bias/v*
_output_shapes
:*
dtype0

NoOpNoOp
�U
ConstConst"/device:CPU:0*
_output_shapes
: *
dtype0*�T
value�TB�T B�T
�
layer-0
layer-1
layer-2
layer_with_weights-0
layer-3
layer_with_weights-1
layer-4
layer_with_weights-2
layer-5
layer_with_weights-3
layer-6
layer_with_weights-4
layer-7
	layer_with_weights-5
	layer-8

layer_with_weights-6

layer-9
layer_with_weights-7
layer-10
layer_with_weights-8
layer-11
layer_with_weights-9
layer-12
layer_with_weights-10
layer-13
layer_with_weights-11
layer-14
layer-15
layer_with_weights-12
layer-16
	optimizer
	variables
trainable_variables
regularization_losses
	keras_api

signatures
%
#_self_saveable_object_factories
%
#_self_saveable_object_factories
%
#_self_saveable_object_factories
�

kernel
bias
#_self_saveable_object_factories
	variables
trainable_variables
 regularization_losses
!	keras_api
�

"kernel
#bias
#$_self_saveable_object_factories
%	variables
&trainable_variables
'regularization_losses
(	keras_api
�

)kernel
*bias
#+_self_saveable_object_factories
,	variables
-trainable_variables
.regularization_losses
/	keras_api
�

0kernel
1bias
#2_self_saveable_object_factories
3	variables
4trainable_variables
5regularization_losses
6	keras_api
�

7kernel
8bias
#9_self_saveable_object_factories
:	variables
;trainable_variables
<regularization_losses
=	keras_api
�

>kernel
?bias
#@_self_saveable_object_factories
A	variables
Btrainable_variables
Cregularization_losses
D	keras_api
�

Ekernel
Fbias
#G_self_saveable_object_factories
H	variables
Itrainable_variables
Jregularization_losses
K	keras_api
�

Lkernel
Mbias
#N_self_saveable_object_factories
O	variables
Ptrainable_variables
Qregularization_losses
R	keras_api
�

Skernel
Tbias
#U_self_saveable_object_factories
V	variables
Wtrainable_variables
Xregularization_losses
Y	keras_api
�

Zkernel
[bias
#\_self_saveable_object_factories
]	variables
^trainable_variables
_regularization_losses
`	keras_api
�

akernel
bbias
#c_self_saveable_object_factories
d	variables
etrainable_variables
fregularization_losses
g	keras_api
�

hkernel
ibias
#j_self_saveable_object_factories
k	variables
ltrainable_variables
mregularization_losses
n	keras_api
R
o	variables
ptrainable_variables
qregularization_losses
r	keras_api
h

skernel
tbias
u	variables
vtrainable_variables
wregularization_losses
x	keras_api
h
yiter

zbeta_1

{beta_2
	|decay
}learning_ratesm�tm�sv�tv�
�
0
1
"2
#3
)4
*5
06
17
78
89
>10
?11
E12
F13
L14
M15
S16
T17
Z18
[19
a20
b21
h22
i23
s24
t25

s0
t1
 
�
~layer_regularization_losses
metrics
�layer_metrics
	variables
�layers
trainable_variables
�non_trainable_variables
regularization_losses
 
 
 
 
ge
VARIABLE_VALUEnmn2_pca_2000_1000_1/kernel6layer_with_weights-0/kernel/.ATTRIBUTES/VARIABLE_VALUE
ca
VARIABLE_VALUEnmn2_pca_2000_1000_1/bias4layer_with_weights-0/bias/.ATTRIBUTES/VARIABLE_VALUE
 

0
1
 
 
�
 �layer_regularization_losses
�metrics
�layer_metrics
	variables
�layers
trainable_variables
�non_trainable_variables
 regularization_losses
hf
VARIABLE_VALUEnmn2_umap_2000_1000_1/kernel6layer_with_weights-1/kernel/.ATTRIBUTES/VARIABLE_VALUE
db
VARIABLE_VALUEnmn2_umap_2000_1000_1/bias4layer_with_weights-1/bias/.ATTRIBUTES/VARIABLE_VALUE
 

"0
#1
 
 
�
 �layer_regularization_losses
�metrics
�layer_metrics
%	variables
�layers
&trainable_variables
�non_trainable_variables
'regularization_losses
hf
VARIABLE_VALUEnmn2_tsvd_2000_1000_1/kernel6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUE
db
VARIABLE_VALUEnmn2_tsvd_2000_1000_1/bias4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUE
 

)0
*1
 
 
�
 �layer_regularization_losses
�metrics
�layer_metrics
,	variables
�layers
-trainable_variables
�non_trainable_variables
.regularization_losses
ge
VARIABLE_VALUEnmn2_pca_2000_1000_2/kernel6layer_with_weights-3/kernel/.ATTRIBUTES/VARIABLE_VALUE
ca
VARIABLE_VALUEnmn2_pca_2000_1000_2/bias4layer_with_weights-3/bias/.ATTRIBUTES/VARIABLE_VALUE
 

00
11
 
 
�
 �layer_regularization_losses
�metrics
�layer_metrics
3	variables
�layers
4trainable_variables
�non_trainable_variables
5regularization_losses
hf
VARIABLE_VALUEnmn2_umap_2000_1000_2/kernel6layer_with_weights-4/kernel/.ATTRIBUTES/VARIABLE_VALUE
db
VARIABLE_VALUEnmn2_umap_2000_1000_2/bias4layer_with_weights-4/bias/.ATTRIBUTES/VARIABLE_VALUE
 

70
81
 
 
�
 �layer_regularization_losses
�metrics
�layer_metrics
:	variables
�layers
;trainable_variables
�non_trainable_variables
<regularization_losses
hf
VARIABLE_VALUEnmn2_tsvd_2000_1000_2/kernel6layer_with_weights-5/kernel/.ATTRIBUTES/VARIABLE_VALUE
db
VARIABLE_VALUEnmn2_tsvd_2000_1000_2/bias4layer_with_weights-5/bias/.ATTRIBUTES/VARIABLE_VALUE
 

>0
?1
 
 
�
 �layer_regularization_losses
�metrics
�layer_metrics
A	variables
�layers
Btrainable_variables
�non_trainable_variables
Cregularization_losses
ge
VARIABLE_VALUEnmn2_pca_2000_1000_3/kernel6layer_with_weights-6/kernel/.ATTRIBUTES/VARIABLE_VALUE
ca
VARIABLE_VALUEnmn2_pca_2000_1000_3/bias4layer_with_weights-6/bias/.ATTRIBUTES/VARIABLE_VALUE
 

E0
F1
 
 
�
 �layer_regularization_losses
�metrics
�layer_metrics
H	variables
�layers
Itrainable_variables
�non_trainable_variables
Jregularization_losses
hf
VARIABLE_VALUEnmn2_umap_2000_1000_3/kernel6layer_with_weights-7/kernel/.ATTRIBUTES/VARIABLE_VALUE
db
VARIABLE_VALUEnmn2_umap_2000_1000_3/bias4layer_with_weights-7/bias/.ATTRIBUTES/VARIABLE_VALUE
 

L0
M1
 
 
�
 �layer_regularization_losses
�metrics
�layer_metrics
O	variables
�layers
Ptrainable_variables
�non_trainable_variables
Qregularization_losses
hf
VARIABLE_VALUEnmn2_tsvd_2000_1000_3/kernel6layer_with_weights-8/kernel/.ATTRIBUTES/VARIABLE_VALUE
db
VARIABLE_VALUEnmn2_tsvd_2000_1000_3/bias4layer_with_weights-8/bias/.ATTRIBUTES/VARIABLE_VALUE
 

S0
T1
 
 
�
 �layer_regularization_losses
�metrics
�layer_metrics
V	variables
�layers
Wtrainable_variables
�non_trainable_variables
Xregularization_losses
ig
VARIABLE_VALUEnmn2_pca_2000_1000_out/kernel6layer_with_weights-9/kernel/.ATTRIBUTES/VARIABLE_VALUE
ec
VARIABLE_VALUEnmn2_pca_2000_1000_out/bias4layer_with_weights-9/bias/.ATTRIBUTES/VARIABLE_VALUE
 

Z0
[1
 
 
�
 �layer_regularization_losses
�metrics
�layer_metrics
]	variables
�layers
^trainable_variables
�non_trainable_variables
_regularization_losses
ki
VARIABLE_VALUEnmn2_umap_2000_1000_out/kernel7layer_with_weights-10/kernel/.ATTRIBUTES/VARIABLE_VALUE
ge
VARIABLE_VALUEnmn2_umap_2000_1000_out/bias5layer_with_weights-10/bias/.ATTRIBUTES/VARIABLE_VALUE
 

a0
b1
 
 
�
 �layer_regularization_losses
�metrics
�layer_metrics
d	variables
�layers
etrainable_variables
�non_trainable_variables
fregularization_losses
ki
VARIABLE_VALUEnmn2_tsvd_2000_1000_out/kernel7layer_with_weights-11/kernel/.ATTRIBUTES/VARIABLE_VALUE
ge
VARIABLE_VALUEnmn2_tsvd_2000_1000_out/bias5layer_with_weights-11/bias/.ATTRIBUTES/VARIABLE_VALUE
 

h0
i1
 
 
�
 �layer_regularization_losses
�metrics
�layer_metrics
k	variables
�layers
ltrainable_variables
�non_trainable_variables
mregularization_losses
 
 
 
�
 �layer_regularization_losses
�metrics
�layer_metrics
o	variables
�layers
ptrainable_variables
�non_trainable_variables
qregularization_losses
ZX
VARIABLE_VALUEjoined/kernel7layer_with_weights-12/kernel/.ATTRIBUTES/VARIABLE_VALUE
VT
VARIABLE_VALUEjoined/bias5layer_with_weights-12/bias/.ATTRIBUTES/VARIABLE_VALUE

s0
t1

s0
t1
 
�
 �layer_regularization_losses
�metrics
�layer_metrics
u	variables
�layers
vtrainable_variables
�non_trainable_variables
wregularization_losses
HF
VARIABLE_VALUE	Adam/iter)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUE
LJ
VARIABLE_VALUEAdam/beta_1+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUE
LJ
VARIABLE_VALUEAdam/beta_2+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUE
JH
VARIABLE_VALUE
Adam/decay*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUE
ZX
VARIABLE_VALUEAdam/learning_rate2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUE
 

�0
�1
 
~
0
1
2
3
4
5
6
7
	8

9
10
11
12
13
14
15
16
�
0
1
"2
#3
)4
*5
06
17
78
89
>10
?11
E12
F13
L14
M15
S16
T17
Z18
[19
a20
b21
h22
i23
 
 
 
 

0
1
 
 
 
 

"0
#1
 
 
 
 

)0
*1
 
 
 
 

00
11
 
 
 
 

70
81
 
 
 
 

>0
?1
 
 
 
 

E0
F1
 
 
 
 

L0
M1
 
 
 
 

S0
T1
 
 
 
 

Z0
[1
 
 
 
 

a0
b1
 
 
 
 

h0
i1
 
 
 
 
 
 
 
 
 
 
8

�total

�count
�	variables
�	keras_api
I

�total

�count
�
_fn_kwargs
�	variables
�	keras_api
OM
VARIABLE_VALUEtotal4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUE
OM
VARIABLE_VALUEcount4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUE

�0
�1

�	variables
QO
VARIABLE_VALUEtotal_14keras_api/metrics/1/total/.ATTRIBUTES/VARIABLE_VALUE
QO
VARIABLE_VALUEcount_14keras_api/metrics/1/count/.ATTRIBUTES/VARIABLE_VALUE
 

�0
�1

�	variables
}{
VARIABLE_VALUEAdam/joined/kernel/mSlayer_with_weights-12/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
yw
VARIABLE_VALUEAdam/joined/bias/mQlayer_with_weights-12/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
}{
VARIABLE_VALUEAdam/joined/kernel/vSlayer_with_weights-12/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
yw
VARIABLE_VALUEAdam/joined/bias/vQlayer_with_weights-12/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
�
(serving_default_nmn2_pca_2000_1000_inputPlaceholder*(
_output_shapes
:����������*
dtype0*
shape:����������
�
)serving_default_nmn2_tsvd_2000_1000_inputPlaceholder*(
_output_shapes
:����������*
dtype0*
shape:����������
�
)serving_default_nmn2_umap_2000_1000_inputPlaceholder*(
_output_shapes
:����������*
dtype0*
shape:����������
�	
StatefulPartitionedCallStatefulPartitionedCall(serving_default_nmn2_pca_2000_1000_input)serving_default_nmn2_tsvd_2000_1000_input)serving_default_nmn2_umap_2000_1000_inputnmn2_tsvd_2000_1000_1/kernelnmn2_tsvd_2000_1000_1/biasnmn2_umap_2000_1000_1/kernelnmn2_umap_2000_1000_1/biasnmn2_pca_2000_1000_1/kernelnmn2_pca_2000_1000_1/biasnmn2_tsvd_2000_1000_2/kernelnmn2_tsvd_2000_1000_2/biasnmn2_umap_2000_1000_2/kernelnmn2_umap_2000_1000_2/biasnmn2_pca_2000_1000_2/kernelnmn2_pca_2000_1000_2/biasnmn2_tsvd_2000_1000_3/kernelnmn2_tsvd_2000_1000_3/biasnmn2_umap_2000_1000_3/kernelnmn2_umap_2000_1000_3/biasnmn2_pca_2000_1000_3/kernelnmn2_pca_2000_1000_3/biasnmn2_pca_2000_1000_out/kernelnmn2_pca_2000_1000_out/biasnmn2_umap_2000_1000_out/kernelnmn2_umap_2000_1000_out/biasnmn2_tsvd_2000_1000_out/kernelnmn2_tsvd_2000_1000_out/biasjoined/kerneljoined/bias*(
Tin!
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*<
_read_only_resource_inputs
	
*0
config_proto 

CPU

GPU2*0J 8� *,
f'R%
#__inference_signature_wrapper_43262
O
saver_filenamePlaceholder*
_output_shapes
: *
dtype0*
shape: 
�
StatefulPartitionedCall_1StatefulPartitionedCallsaver_filename/nmn2_pca_2000_1000_1/kernel/Read/ReadVariableOp-nmn2_pca_2000_1000_1/bias/Read/ReadVariableOp0nmn2_umap_2000_1000_1/kernel/Read/ReadVariableOp.nmn2_umap_2000_1000_1/bias/Read/ReadVariableOp0nmn2_tsvd_2000_1000_1/kernel/Read/ReadVariableOp.nmn2_tsvd_2000_1000_1/bias/Read/ReadVariableOp/nmn2_pca_2000_1000_2/kernel/Read/ReadVariableOp-nmn2_pca_2000_1000_2/bias/Read/ReadVariableOp0nmn2_umap_2000_1000_2/kernel/Read/ReadVariableOp.nmn2_umap_2000_1000_2/bias/Read/ReadVariableOp0nmn2_tsvd_2000_1000_2/kernel/Read/ReadVariableOp.nmn2_tsvd_2000_1000_2/bias/Read/ReadVariableOp/nmn2_pca_2000_1000_3/kernel/Read/ReadVariableOp-nmn2_pca_2000_1000_3/bias/Read/ReadVariableOp0nmn2_umap_2000_1000_3/kernel/Read/ReadVariableOp.nmn2_umap_2000_1000_3/bias/Read/ReadVariableOp0nmn2_tsvd_2000_1000_3/kernel/Read/ReadVariableOp.nmn2_tsvd_2000_1000_3/bias/Read/ReadVariableOp1nmn2_pca_2000_1000_out/kernel/Read/ReadVariableOp/nmn2_pca_2000_1000_out/bias/Read/ReadVariableOp2nmn2_umap_2000_1000_out/kernel/Read/ReadVariableOp0nmn2_umap_2000_1000_out/bias/Read/ReadVariableOp2nmn2_tsvd_2000_1000_out/kernel/Read/ReadVariableOp0nmn2_tsvd_2000_1000_out/bias/Read/ReadVariableOp!joined/kernel/Read/ReadVariableOpjoined/bias/Read/ReadVariableOpAdam/iter/Read/ReadVariableOpAdam/beta_1/Read/ReadVariableOpAdam/beta_2/Read/ReadVariableOpAdam/decay/Read/ReadVariableOp&Adam/learning_rate/Read/ReadVariableOptotal/Read/ReadVariableOpcount/Read/ReadVariableOptotal_1/Read/ReadVariableOpcount_1/Read/ReadVariableOp(Adam/joined/kernel/m/Read/ReadVariableOp&Adam/joined/bias/m/Read/ReadVariableOp(Adam/joined/kernel/v/Read/ReadVariableOp&Adam/joined/bias/v/Read/ReadVariableOpConst*4
Tin-
+2)	*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *'
f"R 
__inference__traced_save_43995
�	
StatefulPartitionedCall_2StatefulPartitionedCallsaver_filenamenmn2_pca_2000_1000_1/kernelnmn2_pca_2000_1000_1/biasnmn2_umap_2000_1000_1/kernelnmn2_umap_2000_1000_1/biasnmn2_tsvd_2000_1000_1/kernelnmn2_tsvd_2000_1000_1/biasnmn2_pca_2000_1000_2/kernelnmn2_pca_2000_1000_2/biasnmn2_umap_2000_1000_2/kernelnmn2_umap_2000_1000_2/biasnmn2_tsvd_2000_1000_2/kernelnmn2_tsvd_2000_1000_2/biasnmn2_pca_2000_1000_3/kernelnmn2_pca_2000_1000_3/biasnmn2_umap_2000_1000_3/kernelnmn2_umap_2000_1000_3/biasnmn2_tsvd_2000_1000_3/kernelnmn2_tsvd_2000_1000_3/biasnmn2_pca_2000_1000_out/kernelnmn2_pca_2000_1000_out/biasnmn2_umap_2000_1000_out/kernelnmn2_umap_2000_1000_out/biasnmn2_tsvd_2000_1000_out/kernelnmn2_tsvd_2000_1000_out/biasjoined/kerneljoined/bias	Adam/iterAdam/beta_1Adam/beta_2
Adam/decayAdam/learning_ratetotalcounttotal_1count_1Adam/joined/kernel/mAdam/joined/bias/mAdam/joined/kernel/vAdam/joined/bias/v*3
Tin,
*2(*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� **
f%R#
!__inference__traced_restore_44122��
�
�
4__inference_nmn2_pca_2000_1000_2_layer_call_fn_43658

inputs
unknown:
��
	unknown_0:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *X
fSRQ
O__inference_nmn2_pca_2000_1000_2_layer_call_and_return_conditional_losses_424732
StatefulPartitionedCall|
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:����������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
ҡ
�
@__inference_model_layer_call_and_return_conditional_losses_43460
inputs_0
inputs_1
inputs_2H
4nmn2_tsvd_2000_1000_1_matmul_readvariableop_resource:
��D
5nmn2_tsvd_2000_1000_1_biasadd_readvariableop_resource:	�H
4nmn2_umap_2000_1000_1_matmul_readvariableop_resource:
��D
5nmn2_umap_2000_1000_1_biasadd_readvariableop_resource:	�G
3nmn2_pca_2000_1000_1_matmul_readvariableop_resource:
��C
4nmn2_pca_2000_1000_1_biasadd_readvariableop_resource:	�H
4nmn2_tsvd_2000_1000_2_matmul_readvariableop_resource:
��D
5nmn2_tsvd_2000_1000_2_biasadd_readvariableop_resource:	�H
4nmn2_umap_2000_1000_2_matmul_readvariableop_resource:
��D
5nmn2_umap_2000_1000_2_biasadd_readvariableop_resource:	�G
3nmn2_pca_2000_1000_2_matmul_readvariableop_resource:
��C
4nmn2_pca_2000_1000_2_biasadd_readvariableop_resource:	�H
4nmn2_tsvd_2000_1000_3_matmul_readvariableop_resource:
��D
5nmn2_tsvd_2000_1000_3_biasadd_readvariableop_resource:	�H
4nmn2_umap_2000_1000_3_matmul_readvariableop_resource:
��D
5nmn2_umap_2000_1000_3_biasadd_readvariableop_resource:	�G
3nmn2_pca_2000_1000_3_matmul_readvariableop_resource:
��C
4nmn2_pca_2000_1000_3_biasadd_readvariableop_resource:	�H
5nmn2_pca_2000_1000_out_matmul_readvariableop_resource:	�D
6nmn2_pca_2000_1000_out_biasadd_readvariableop_resource:I
6nmn2_umap_2000_1000_out_matmul_readvariableop_resource:	�E
7nmn2_umap_2000_1000_out_biasadd_readvariableop_resource:I
6nmn2_tsvd_2000_1000_out_matmul_readvariableop_resource:	�E
7nmn2_tsvd_2000_1000_out_biasadd_readvariableop_resource:7
%joined_matmul_readvariableop_resource:4
&joined_biasadd_readvariableop_resource:
identity��joined/BiasAdd/ReadVariableOp�joined/MatMul/ReadVariableOp�+nmn2_pca_2000_1000_1/BiasAdd/ReadVariableOp�*nmn2_pca_2000_1000_1/MatMul/ReadVariableOp�+nmn2_pca_2000_1000_2/BiasAdd/ReadVariableOp�*nmn2_pca_2000_1000_2/MatMul/ReadVariableOp�+nmn2_pca_2000_1000_3/BiasAdd/ReadVariableOp�*nmn2_pca_2000_1000_3/MatMul/ReadVariableOp�-nmn2_pca_2000_1000_out/BiasAdd/ReadVariableOp�,nmn2_pca_2000_1000_out/MatMul/ReadVariableOp�,nmn2_tsvd_2000_1000_1/BiasAdd/ReadVariableOp�+nmn2_tsvd_2000_1000_1/MatMul/ReadVariableOp�,nmn2_tsvd_2000_1000_2/BiasAdd/ReadVariableOp�+nmn2_tsvd_2000_1000_2/MatMul/ReadVariableOp�,nmn2_tsvd_2000_1000_3/BiasAdd/ReadVariableOp�+nmn2_tsvd_2000_1000_3/MatMul/ReadVariableOp�.nmn2_tsvd_2000_1000_out/BiasAdd/ReadVariableOp�-nmn2_tsvd_2000_1000_out/MatMul/ReadVariableOp�,nmn2_umap_2000_1000_1/BiasAdd/ReadVariableOp�+nmn2_umap_2000_1000_1/MatMul/ReadVariableOp�,nmn2_umap_2000_1000_2/BiasAdd/ReadVariableOp�+nmn2_umap_2000_1000_2/MatMul/ReadVariableOp�,nmn2_umap_2000_1000_3/BiasAdd/ReadVariableOp�+nmn2_umap_2000_1000_3/MatMul/ReadVariableOp�.nmn2_umap_2000_1000_out/BiasAdd/ReadVariableOp�-nmn2_umap_2000_1000_out/MatMul/ReadVariableOp�
+nmn2_tsvd_2000_1000_1/MatMul/ReadVariableOpReadVariableOp4nmn2_tsvd_2000_1000_1_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02-
+nmn2_tsvd_2000_1000_1/MatMul/ReadVariableOp�
nmn2_tsvd_2000_1000_1/MatMulMatMulinputs_23nmn2_tsvd_2000_1000_1/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
nmn2_tsvd_2000_1000_1/MatMul�
,nmn2_tsvd_2000_1000_1/BiasAdd/ReadVariableOpReadVariableOp5nmn2_tsvd_2000_1000_1_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02.
,nmn2_tsvd_2000_1000_1/BiasAdd/ReadVariableOp�
nmn2_tsvd_2000_1000_1/BiasAddBiasAdd&nmn2_tsvd_2000_1000_1/MatMul:product:04nmn2_tsvd_2000_1000_1/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
nmn2_tsvd_2000_1000_1/BiasAdd�
nmn2_tsvd_2000_1000_1/ReluRelu&nmn2_tsvd_2000_1000_1/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
nmn2_tsvd_2000_1000_1/Relu�
+nmn2_umap_2000_1000_1/MatMul/ReadVariableOpReadVariableOp4nmn2_umap_2000_1000_1_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02-
+nmn2_umap_2000_1000_1/MatMul/ReadVariableOp�
nmn2_umap_2000_1000_1/MatMulMatMulinputs_13nmn2_umap_2000_1000_1/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
nmn2_umap_2000_1000_1/MatMul�
,nmn2_umap_2000_1000_1/BiasAdd/ReadVariableOpReadVariableOp5nmn2_umap_2000_1000_1_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02.
,nmn2_umap_2000_1000_1/BiasAdd/ReadVariableOp�
nmn2_umap_2000_1000_1/BiasAddBiasAdd&nmn2_umap_2000_1000_1/MatMul:product:04nmn2_umap_2000_1000_1/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
nmn2_umap_2000_1000_1/BiasAdd�
nmn2_umap_2000_1000_1/ReluRelu&nmn2_umap_2000_1000_1/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
nmn2_umap_2000_1000_1/Relu�
*nmn2_pca_2000_1000_1/MatMul/ReadVariableOpReadVariableOp3nmn2_pca_2000_1000_1_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02,
*nmn2_pca_2000_1000_1/MatMul/ReadVariableOp�
nmn2_pca_2000_1000_1/MatMulMatMulinputs_02nmn2_pca_2000_1000_1/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
nmn2_pca_2000_1000_1/MatMul�
+nmn2_pca_2000_1000_1/BiasAdd/ReadVariableOpReadVariableOp4nmn2_pca_2000_1000_1_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02-
+nmn2_pca_2000_1000_1/BiasAdd/ReadVariableOp�
nmn2_pca_2000_1000_1/BiasAddBiasAdd%nmn2_pca_2000_1000_1/MatMul:product:03nmn2_pca_2000_1000_1/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
nmn2_pca_2000_1000_1/BiasAdd�
nmn2_pca_2000_1000_1/ReluRelu%nmn2_pca_2000_1000_1/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
nmn2_pca_2000_1000_1/Relu�
+nmn2_tsvd_2000_1000_2/MatMul/ReadVariableOpReadVariableOp4nmn2_tsvd_2000_1000_2_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02-
+nmn2_tsvd_2000_1000_2/MatMul/ReadVariableOp�
nmn2_tsvd_2000_1000_2/MatMulMatMul(nmn2_tsvd_2000_1000_1/Relu:activations:03nmn2_tsvd_2000_1000_2/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
nmn2_tsvd_2000_1000_2/MatMul�
,nmn2_tsvd_2000_1000_2/BiasAdd/ReadVariableOpReadVariableOp5nmn2_tsvd_2000_1000_2_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02.
,nmn2_tsvd_2000_1000_2/BiasAdd/ReadVariableOp�
nmn2_tsvd_2000_1000_2/BiasAddBiasAdd&nmn2_tsvd_2000_1000_2/MatMul:product:04nmn2_tsvd_2000_1000_2/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
nmn2_tsvd_2000_1000_2/BiasAdd�
nmn2_tsvd_2000_1000_2/ReluRelu&nmn2_tsvd_2000_1000_2/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
nmn2_tsvd_2000_1000_2/Relu�
+nmn2_umap_2000_1000_2/MatMul/ReadVariableOpReadVariableOp4nmn2_umap_2000_1000_2_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02-
+nmn2_umap_2000_1000_2/MatMul/ReadVariableOp�
nmn2_umap_2000_1000_2/MatMulMatMul(nmn2_umap_2000_1000_1/Relu:activations:03nmn2_umap_2000_1000_2/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
nmn2_umap_2000_1000_2/MatMul�
,nmn2_umap_2000_1000_2/BiasAdd/ReadVariableOpReadVariableOp5nmn2_umap_2000_1000_2_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02.
,nmn2_umap_2000_1000_2/BiasAdd/ReadVariableOp�
nmn2_umap_2000_1000_2/BiasAddBiasAdd&nmn2_umap_2000_1000_2/MatMul:product:04nmn2_umap_2000_1000_2/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
nmn2_umap_2000_1000_2/BiasAdd�
nmn2_umap_2000_1000_2/ReluRelu&nmn2_umap_2000_1000_2/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
nmn2_umap_2000_1000_2/Relu�
*nmn2_pca_2000_1000_2/MatMul/ReadVariableOpReadVariableOp3nmn2_pca_2000_1000_2_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02,
*nmn2_pca_2000_1000_2/MatMul/ReadVariableOp�
nmn2_pca_2000_1000_2/MatMulMatMul'nmn2_pca_2000_1000_1/Relu:activations:02nmn2_pca_2000_1000_2/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
nmn2_pca_2000_1000_2/MatMul�
+nmn2_pca_2000_1000_2/BiasAdd/ReadVariableOpReadVariableOp4nmn2_pca_2000_1000_2_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02-
+nmn2_pca_2000_1000_2/BiasAdd/ReadVariableOp�
nmn2_pca_2000_1000_2/BiasAddBiasAdd%nmn2_pca_2000_1000_2/MatMul:product:03nmn2_pca_2000_1000_2/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
nmn2_pca_2000_1000_2/BiasAdd�
nmn2_pca_2000_1000_2/ReluRelu%nmn2_pca_2000_1000_2/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
nmn2_pca_2000_1000_2/Relu�
+nmn2_tsvd_2000_1000_3/MatMul/ReadVariableOpReadVariableOp4nmn2_tsvd_2000_1000_3_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02-
+nmn2_tsvd_2000_1000_3/MatMul/ReadVariableOp�
nmn2_tsvd_2000_1000_3/MatMulMatMul(nmn2_tsvd_2000_1000_2/Relu:activations:03nmn2_tsvd_2000_1000_3/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
nmn2_tsvd_2000_1000_3/MatMul�
,nmn2_tsvd_2000_1000_3/BiasAdd/ReadVariableOpReadVariableOp5nmn2_tsvd_2000_1000_3_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02.
,nmn2_tsvd_2000_1000_3/BiasAdd/ReadVariableOp�
nmn2_tsvd_2000_1000_3/BiasAddBiasAdd&nmn2_tsvd_2000_1000_3/MatMul:product:04nmn2_tsvd_2000_1000_3/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
nmn2_tsvd_2000_1000_3/BiasAdd�
nmn2_tsvd_2000_1000_3/ReluRelu&nmn2_tsvd_2000_1000_3/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
nmn2_tsvd_2000_1000_3/Relu�
+nmn2_umap_2000_1000_3/MatMul/ReadVariableOpReadVariableOp4nmn2_umap_2000_1000_3_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02-
+nmn2_umap_2000_1000_3/MatMul/ReadVariableOp�
nmn2_umap_2000_1000_3/MatMulMatMul(nmn2_umap_2000_1000_2/Relu:activations:03nmn2_umap_2000_1000_3/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
nmn2_umap_2000_1000_3/MatMul�
,nmn2_umap_2000_1000_3/BiasAdd/ReadVariableOpReadVariableOp5nmn2_umap_2000_1000_3_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02.
,nmn2_umap_2000_1000_3/BiasAdd/ReadVariableOp�
nmn2_umap_2000_1000_3/BiasAddBiasAdd&nmn2_umap_2000_1000_3/MatMul:product:04nmn2_umap_2000_1000_3/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
nmn2_umap_2000_1000_3/BiasAdd�
nmn2_umap_2000_1000_3/ReluRelu&nmn2_umap_2000_1000_3/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
nmn2_umap_2000_1000_3/Relu�
*nmn2_pca_2000_1000_3/MatMul/ReadVariableOpReadVariableOp3nmn2_pca_2000_1000_3_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02,
*nmn2_pca_2000_1000_3/MatMul/ReadVariableOp�
nmn2_pca_2000_1000_3/MatMulMatMul'nmn2_pca_2000_1000_2/Relu:activations:02nmn2_pca_2000_1000_3/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
nmn2_pca_2000_1000_3/MatMul�
+nmn2_pca_2000_1000_3/BiasAdd/ReadVariableOpReadVariableOp4nmn2_pca_2000_1000_3_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02-
+nmn2_pca_2000_1000_3/BiasAdd/ReadVariableOp�
nmn2_pca_2000_1000_3/BiasAddBiasAdd%nmn2_pca_2000_1000_3/MatMul:product:03nmn2_pca_2000_1000_3/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
nmn2_pca_2000_1000_3/BiasAdd�
nmn2_pca_2000_1000_3/ReluRelu%nmn2_pca_2000_1000_3/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
nmn2_pca_2000_1000_3/Relu�
,nmn2_pca_2000_1000_out/MatMul/ReadVariableOpReadVariableOp5nmn2_pca_2000_1000_out_matmul_readvariableop_resource*
_output_shapes
:	�*
dtype02.
,nmn2_pca_2000_1000_out/MatMul/ReadVariableOp�
nmn2_pca_2000_1000_out/MatMulMatMul'nmn2_pca_2000_1000_3/Relu:activations:04nmn2_pca_2000_1000_out/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
nmn2_pca_2000_1000_out/MatMul�
-nmn2_pca_2000_1000_out/BiasAdd/ReadVariableOpReadVariableOp6nmn2_pca_2000_1000_out_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02/
-nmn2_pca_2000_1000_out/BiasAdd/ReadVariableOp�
nmn2_pca_2000_1000_out/BiasAddBiasAdd'nmn2_pca_2000_1000_out/MatMul:product:05nmn2_pca_2000_1000_out/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2 
nmn2_pca_2000_1000_out/BiasAdd�
nmn2_pca_2000_1000_out/SigmoidSigmoid'nmn2_pca_2000_1000_out/BiasAdd:output:0*
T0*'
_output_shapes
:���������2 
nmn2_pca_2000_1000_out/Sigmoid�
-nmn2_umap_2000_1000_out/MatMul/ReadVariableOpReadVariableOp6nmn2_umap_2000_1000_out_matmul_readvariableop_resource*
_output_shapes
:	�*
dtype02/
-nmn2_umap_2000_1000_out/MatMul/ReadVariableOp�
nmn2_umap_2000_1000_out/MatMulMatMul(nmn2_umap_2000_1000_3/Relu:activations:05nmn2_umap_2000_1000_out/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2 
nmn2_umap_2000_1000_out/MatMul�
.nmn2_umap_2000_1000_out/BiasAdd/ReadVariableOpReadVariableOp7nmn2_umap_2000_1000_out_biasadd_readvariableop_resource*
_output_shapes
:*
dtype020
.nmn2_umap_2000_1000_out/BiasAdd/ReadVariableOp�
nmn2_umap_2000_1000_out/BiasAddBiasAdd(nmn2_umap_2000_1000_out/MatMul:product:06nmn2_umap_2000_1000_out/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2!
nmn2_umap_2000_1000_out/BiasAdd�
nmn2_umap_2000_1000_out/SigmoidSigmoid(nmn2_umap_2000_1000_out/BiasAdd:output:0*
T0*'
_output_shapes
:���������2!
nmn2_umap_2000_1000_out/Sigmoid�
-nmn2_tsvd_2000_1000_out/MatMul/ReadVariableOpReadVariableOp6nmn2_tsvd_2000_1000_out_matmul_readvariableop_resource*
_output_shapes
:	�*
dtype02/
-nmn2_tsvd_2000_1000_out/MatMul/ReadVariableOp�
nmn2_tsvd_2000_1000_out/MatMulMatMul(nmn2_tsvd_2000_1000_3/Relu:activations:05nmn2_tsvd_2000_1000_out/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2 
nmn2_tsvd_2000_1000_out/MatMul�
.nmn2_tsvd_2000_1000_out/BiasAdd/ReadVariableOpReadVariableOp7nmn2_tsvd_2000_1000_out_biasadd_readvariableop_resource*
_output_shapes
:*
dtype020
.nmn2_tsvd_2000_1000_out/BiasAdd/ReadVariableOp�
nmn2_tsvd_2000_1000_out/BiasAddBiasAdd(nmn2_tsvd_2000_1000_out/MatMul:product:06nmn2_tsvd_2000_1000_out/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2!
nmn2_tsvd_2000_1000_out/BiasAdd�
nmn2_tsvd_2000_1000_out/SigmoidSigmoid(nmn2_tsvd_2000_1000_out/BiasAdd:output:0*
T0*'
_output_shapes
:���������2!
nmn2_tsvd_2000_1000_out/Sigmoidx
concatenate_1/concat/axisConst*
_output_shapes
: *
dtype0*
value	B :2
concatenate_1/concat/axis�
concatenate_1/concatConcatV2"nmn2_pca_2000_1000_out/Sigmoid:y:0#nmn2_umap_2000_1000_out/Sigmoid:y:0#nmn2_tsvd_2000_1000_out/Sigmoid:y:0"concatenate_1/concat/axis:output:0*
N*
T0*'
_output_shapes
:���������2
concatenate_1/concat�
joined/MatMul/ReadVariableOpReadVariableOp%joined_matmul_readvariableop_resource*
_output_shapes

:*
dtype02
joined/MatMul/ReadVariableOp�
joined/MatMulMatMulconcatenate_1/concat:output:0$joined/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
joined/MatMul�
joined/BiasAdd/ReadVariableOpReadVariableOp&joined_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02
joined/BiasAdd/ReadVariableOp�
joined/BiasAddBiasAddjoined/MatMul:product:0%joined/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
joined/BiasAddv
joined/SigmoidSigmoidjoined/BiasAdd:output:0*
T0*'
_output_shapes
:���������2
joined/Sigmoidm
IdentityIdentityjoined/Sigmoid:y:0^NoOp*
T0*'
_output_shapes
:���������2

Identity�	
NoOpNoOp^joined/BiasAdd/ReadVariableOp^joined/MatMul/ReadVariableOp,^nmn2_pca_2000_1000_1/BiasAdd/ReadVariableOp+^nmn2_pca_2000_1000_1/MatMul/ReadVariableOp,^nmn2_pca_2000_1000_2/BiasAdd/ReadVariableOp+^nmn2_pca_2000_1000_2/MatMul/ReadVariableOp,^nmn2_pca_2000_1000_3/BiasAdd/ReadVariableOp+^nmn2_pca_2000_1000_3/MatMul/ReadVariableOp.^nmn2_pca_2000_1000_out/BiasAdd/ReadVariableOp-^nmn2_pca_2000_1000_out/MatMul/ReadVariableOp-^nmn2_tsvd_2000_1000_1/BiasAdd/ReadVariableOp,^nmn2_tsvd_2000_1000_1/MatMul/ReadVariableOp-^nmn2_tsvd_2000_1000_2/BiasAdd/ReadVariableOp,^nmn2_tsvd_2000_1000_2/MatMul/ReadVariableOp-^nmn2_tsvd_2000_1000_3/BiasAdd/ReadVariableOp,^nmn2_tsvd_2000_1000_3/MatMul/ReadVariableOp/^nmn2_tsvd_2000_1000_out/BiasAdd/ReadVariableOp.^nmn2_tsvd_2000_1000_out/MatMul/ReadVariableOp-^nmn2_umap_2000_1000_1/BiasAdd/ReadVariableOp,^nmn2_umap_2000_1000_1/MatMul/ReadVariableOp-^nmn2_umap_2000_1000_2/BiasAdd/ReadVariableOp,^nmn2_umap_2000_1000_2/MatMul/ReadVariableOp-^nmn2_umap_2000_1000_3/BiasAdd/ReadVariableOp,^nmn2_umap_2000_1000_3/MatMul/ReadVariableOp/^nmn2_umap_2000_1000_out/BiasAdd/ReadVariableOp.^nmn2_umap_2000_1000_out/MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*�
_input_shapesr
p:����������:����������:����������: : : : : : : : : : : : : : : : : : : : : : : : : : 2>
joined/BiasAdd/ReadVariableOpjoined/BiasAdd/ReadVariableOp2<
joined/MatMul/ReadVariableOpjoined/MatMul/ReadVariableOp2Z
+nmn2_pca_2000_1000_1/BiasAdd/ReadVariableOp+nmn2_pca_2000_1000_1/BiasAdd/ReadVariableOp2X
*nmn2_pca_2000_1000_1/MatMul/ReadVariableOp*nmn2_pca_2000_1000_1/MatMul/ReadVariableOp2Z
+nmn2_pca_2000_1000_2/BiasAdd/ReadVariableOp+nmn2_pca_2000_1000_2/BiasAdd/ReadVariableOp2X
*nmn2_pca_2000_1000_2/MatMul/ReadVariableOp*nmn2_pca_2000_1000_2/MatMul/ReadVariableOp2Z
+nmn2_pca_2000_1000_3/BiasAdd/ReadVariableOp+nmn2_pca_2000_1000_3/BiasAdd/ReadVariableOp2X
*nmn2_pca_2000_1000_3/MatMul/ReadVariableOp*nmn2_pca_2000_1000_3/MatMul/ReadVariableOp2^
-nmn2_pca_2000_1000_out/BiasAdd/ReadVariableOp-nmn2_pca_2000_1000_out/BiasAdd/ReadVariableOp2\
,nmn2_pca_2000_1000_out/MatMul/ReadVariableOp,nmn2_pca_2000_1000_out/MatMul/ReadVariableOp2\
,nmn2_tsvd_2000_1000_1/BiasAdd/ReadVariableOp,nmn2_tsvd_2000_1000_1/BiasAdd/ReadVariableOp2Z
+nmn2_tsvd_2000_1000_1/MatMul/ReadVariableOp+nmn2_tsvd_2000_1000_1/MatMul/ReadVariableOp2\
,nmn2_tsvd_2000_1000_2/BiasAdd/ReadVariableOp,nmn2_tsvd_2000_1000_2/BiasAdd/ReadVariableOp2Z
+nmn2_tsvd_2000_1000_2/MatMul/ReadVariableOp+nmn2_tsvd_2000_1000_2/MatMul/ReadVariableOp2\
,nmn2_tsvd_2000_1000_3/BiasAdd/ReadVariableOp,nmn2_tsvd_2000_1000_3/BiasAdd/ReadVariableOp2Z
+nmn2_tsvd_2000_1000_3/MatMul/ReadVariableOp+nmn2_tsvd_2000_1000_3/MatMul/ReadVariableOp2`
.nmn2_tsvd_2000_1000_out/BiasAdd/ReadVariableOp.nmn2_tsvd_2000_1000_out/BiasAdd/ReadVariableOp2^
-nmn2_tsvd_2000_1000_out/MatMul/ReadVariableOp-nmn2_tsvd_2000_1000_out/MatMul/ReadVariableOp2\
,nmn2_umap_2000_1000_1/BiasAdd/ReadVariableOp,nmn2_umap_2000_1000_1/BiasAdd/ReadVariableOp2Z
+nmn2_umap_2000_1000_1/MatMul/ReadVariableOp+nmn2_umap_2000_1000_1/MatMul/ReadVariableOp2\
,nmn2_umap_2000_1000_2/BiasAdd/ReadVariableOp,nmn2_umap_2000_1000_2/BiasAdd/ReadVariableOp2Z
+nmn2_umap_2000_1000_2/MatMul/ReadVariableOp+nmn2_umap_2000_1000_2/MatMul/ReadVariableOp2\
,nmn2_umap_2000_1000_3/BiasAdd/ReadVariableOp,nmn2_umap_2000_1000_3/BiasAdd/ReadVariableOp2Z
+nmn2_umap_2000_1000_3/MatMul/ReadVariableOp+nmn2_umap_2000_1000_3/MatMul/ReadVariableOp2`
.nmn2_umap_2000_1000_out/BiasAdd/ReadVariableOp.nmn2_umap_2000_1000_out/BiasAdd/ReadVariableOp2^
-nmn2_umap_2000_1000_out/MatMul/ReadVariableOp-nmn2_umap_2000_1000_out/MatMul/ReadVariableOp:R N
(
_output_shapes
:����������
"
_user_specified_name
inputs/0:RN
(
_output_shapes
:����������
"
_user_specified_name
inputs/1:RN
(
_output_shapes
:����������
"
_user_specified_name
inputs/2
�
�
O__inference_nmn2_pca_2000_1000_1_layer_call_and_return_conditional_losses_42422

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
A__inference_joined_layer_call_and_return_conditional_losses_43844

inputs0
matmul_readvariableop_resource:-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2	
BiasAdda
SigmoidSigmoidBiasAdd:output:0*
T0*'
_output_shapes
:���������2	
Sigmoidf
IdentityIdentitySigmoid:y:0^NoOp*
T0*'
_output_shapes
:���������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs
�
�
R__inference_nmn2_umap_2000_1000_out_layer_call_and_return_conditional_losses_43789

inputs1
matmul_readvariableop_resource:	�-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes
:	�*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2	
BiasAdda
SigmoidSigmoidBiasAdd:output:0*
T0*'
_output_shapes
:���������2	
Sigmoidf
IdentityIdentitySigmoid:y:0^NoOp*
T0*'
_output_shapes
:���������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
P__inference_nmn2_umap_2000_1000_3_layer_call_and_return_conditional_losses_42507

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
R__inference_nmn2_umap_2000_1000_out_layer_call_and_return_conditional_losses_42558

inputs1
matmul_readvariableop_resource:	�-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes
:	�*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2	
BiasAdda
SigmoidSigmoidBiasAdd:output:0*
T0*'
_output_shapes
:���������2	
Sigmoidf
IdentityIdentitySigmoid:y:0^NoOp*
T0*'
_output_shapes
:���������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
R__inference_nmn2_tsvd_2000_1000_out_layer_call_and_return_conditional_losses_42575

inputs1
matmul_readvariableop_resource:	�-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes
:	�*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2	
BiasAdda
SigmoidSigmoidBiasAdd:output:0*
T0*'
_output_shapes
:���������2	
Sigmoidf
IdentityIdentitySigmoid:y:0^NoOp*
T0*'
_output_shapes
:���������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
5__inference_nmn2_umap_2000_1000_3_layer_call_fn_43738

inputs
unknown:
��
	unknown_0:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Y
fTRR
P__inference_nmn2_umap_2000_1000_3_layer_call_and_return_conditional_losses_425072
StatefulPartitionedCall|
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:����������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
P__inference_nmn2_tsvd_2000_1000_3_layer_call_and_return_conditional_losses_43749

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
&__inference_joined_layer_call_fn_43853

inputs
unknown:
	unknown_0:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *J
fERC
A__inference_joined_layer_call_and_return_conditional_losses_426022
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������: : 22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs
�
�
O__inference_nmn2_pca_2000_1000_1_layer_call_and_return_conditional_losses_43589

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
7__inference_nmn2_tsvd_2000_1000_out_layer_call_fn_43818

inputs
unknown:	�
	unknown_0:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_nmn2_tsvd_2000_1000_out_layer_call_and_return_conditional_losses_425752
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
P__inference_nmn2_tsvd_2000_1000_1_layer_call_and_return_conditional_losses_42388

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
5__inference_nmn2_tsvd_2000_1000_1_layer_call_fn_43638

inputs
unknown:
��
	unknown_0:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Y
fTRR
P__inference_nmn2_tsvd_2000_1000_1_layer_call_and_return_conditional_losses_423882
StatefulPartitionedCall|
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:����������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�Z
�
@__inference_model_layer_call_and_return_conditional_losses_43123
nmn2_pca_2000_1000_input
nmn2_umap_2000_1000_input
nmn2_tsvd_2000_1000_input/
nmn2_tsvd_2000_1000_1_43056:
��*
nmn2_tsvd_2000_1000_1_43058:	�/
nmn2_umap_2000_1000_1_43061:
��*
nmn2_umap_2000_1000_1_43063:	�.
nmn2_pca_2000_1000_1_43066:
��)
nmn2_pca_2000_1000_1_43068:	�/
nmn2_tsvd_2000_1000_2_43071:
��*
nmn2_tsvd_2000_1000_2_43073:	�/
nmn2_umap_2000_1000_2_43076:
��*
nmn2_umap_2000_1000_2_43078:	�.
nmn2_pca_2000_1000_2_43081:
��)
nmn2_pca_2000_1000_2_43083:	�/
nmn2_tsvd_2000_1000_3_43086:
��*
nmn2_tsvd_2000_1000_3_43088:	�/
nmn2_umap_2000_1000_3_43091:
��*
nmn2_umap_2000_1000_3_43093:	�.
nmn2_pca_2000_1000_3_43096:
��)
nmn2_pca_2000_1000_3_43098:	�/
nmn2_pca_2000_1000_out_43101:	�*
nmn2_pca_2000_1000_out_43103:0
nmn2_umap_2000_1000_out_43106:	�+
nmn2_umap_2000_1000_out_43108:0
nmn2_tsvd_2000_1000_out_43111:	�+
nmn2_tsvd_2000_1000_out_43113:
joined_43117:
joined_43119:
identity��joined/StatefulPartitionedCall�,nmn2_pca_2000_1000_1/StatefulPartitionedCall�,nmn2_pca_2000_1000_2/StatefulPartitionedCall�,nmn2_pca_2000_1000_3/StatefulPartitionedCall�.nmn2_pca_2000_1000_out/StatefulPartitionedCall�-nmn2_tsvd_2000_1000_1/StatefulPartitionedCall�-nmn2_tsvd_2000_1000_2/StatefulPartitionedCall�-nmn2_tsvd_2000_1000_3/StatefulPartitionedCall�/nmn2_tsvd_2000_1000_out/StatefulPartitionedCall�-nmn2_umap_2000_1000_1/StatefulPartitionedCall�-nmn2_umap_2000_1000_2/StatefulPartitionedCall�-nmn2_umap_2000_1000_3/StatefulPartitionedCall�/nmn2_umap_2000_1000_out/StatefulPartitionedCall�
-nmn2_tsvd_2000_1000_1/StatefulPartitionedCallStatefulPartitionedCallnmn2_tsvd_2000_1000_inputnmn2_tsvd_2000_1000_1_43056nmn2_tsvd_2000_1000_1_43058*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Y
fTRR
P__inference_nmn2_tsvd_2000_1000_1_layer_call_and_return_conditional_losses_423882/
-nmn2_tsvd_2000_1000_1/StatefulPartitionedCall�
-nmn2_umap_2000_1000_1/StatefulPartitionedCallStatefulPartitionedCallnmn2_umap_2000_1000_inputnmn2_umap_2000_1000_1_43061nmn2_umap_2000_1000_1_43063*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Y
fTRR
P__inference_nmn2_umap_2000_1000_1_layer_call_and_return_conditional_losses_424052/
-nmn2_umap_2000_1000_1/StatefulPartitionedCall�
,nmn2_pca_2000_1000_1/StatefulPartitionedCallStatefulPartitionedCallnmn2_pca_2000_1000_inputnmn2_pca_2000_1000_1_43066nmn2_pca_2000_1000_1_43068*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *X
fSRQ
O__inference_nmn2_pca_2000_1000_1_layer_call_and_return_conditional_losses_424222.
,nmn2_pca_2000_1000_1/StatefulPartitionedCall�
-nmn2_tsvd_2000_1000_2/StatefulPartitionedCallStatefulPartitionedCall6nmn2_tsvd_2000_1000_1/StatefulPartitionedCall:output:0nmn2_tsvd_2000_1000_2_43071nmn2_tsvd_2000_1000_2_43073*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Y
fTRR
P__inference_nmn2_tsvd_2000_1000_2_layer_call_and_return_conditional_losses_424392/
-nmn2_tsvd_2000_1000_2/StatefulPartitionedCall�
-nmn2_umap_2000_1000_2/StatefulPartitionedCallStatefulPartitionedCall6nmn2_umap_2000_1000_1/StatefulPartitionedCall:output:0nmn2_umap_2000_1000_2_43076nmn2_umap_2000_1000_2_43078*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Y
fTRR
P__inference_nmn2_umap_2000_1000_2_layer_call_and_return_conditional_losses_424562/
-nmn2_umap_2000_1000_2/StatefulPartitionedCall�
,nmn2_pca_2000_1000_2/StatefulPartitionedCallStatefulPartitionedCall5nmn2_pca_2000_1000_1/StatefulPartitionedCall:output:0nmn2_pca_2000_1000_2_43081nmn2_pca_2000_1000_2_43083*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *X
fSRQ
O__inference_nmn2_pca_2000_1000_2_layer_call_and_return_conditional_losses_424732.
,nmn2_pca_2000_1000_2/StatefulPartitionedCall�
-nmn2_tsvd_2000_1000_3/StatefulPartitionedCallStatefulPartitionedCall6nmn2_tsvd_2000_1000_2/StatefulPartitionedCall:output:0nmn2_tsvd_2000_1000_3_43086nmn2_tsvd_2000_1000_3_43088*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Y
fTRR
P__inference_nmn2_tsvd_2000_1000_3_layer_call_and_return_conditional_losses_424902/
-nmn2_tsvd_2000_1000_3/StatefulPartitionedCall�
-nmn2_umap_2000_1000_3/StatefulPartitionedCallStatefulPartitionedCall6nmn2_umap_2000_1000_2/StatefulPartitionedCall:output:0nmn2_umap_2000_1000_3_43091nmn2_umap_2000_1000_3_43093*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Y
fTRR
P__inference_nmn2_umap_2000_1000_3_layer_call_and_return_conditional_losses_425072/
-nmn2_umap_2000_1000_3/StatefulPartitionedCall�
,nmn2_pca_2000_1000_3/StatefulPartitionedCallStatefulPartitionedCall5nmn2_pca_2000_1000_2/StatefulPartitionedCall:output:0nmn2_pca_2000_1000_3_43096nmn2_pca_2000_1000_3_43098*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *X
fSRQ
O__inference_nmn2_pca_2000_1000_3_layer_call_and_return_conditional_losses_425242.
,nmn2_pca_2000_1000_3/StatefulPartitionedCall�
.nmn2_pca_2000_1000_out/StatefulPartitionedCallStatefulPartitionedCall5nmn2_pca_2000_1000_3/StatefulPartitionedCall:output:0nmn2_pca_2000_1000_out_43101nmn2_pca_2000_1000_out_43103*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_nmn2_pca_2000_1000_out_layer_call_and_return_conditional_losses_4254120
.nmn2_pca_2000_1000_out/StatefulPartitionedCall�
/nmn2_umap_2000_1000_out/StatefulPartitionedCallStatefulPartitionedCall6nmn2_umap_2000_1000_3/StatefulPartitionedCall:output:0nmn2_umap_2000_1000_out_43106nmn2_umap_2000_1000_out_43108*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_nmn2_umap_2000_1000_out_layer_call_and_return_conditional_losses_4255821
/nmn2_umap_2000_1000_out/StatefulPartitionedCall�
/nmn2_tsvd_2000_1000_out/StatefulPartitionedCallStatefulPartitionedCall6nmn2_tsvd_2000_1000_3/StatefulPartitionedCall:output:0nmn2_tsvd_2000_1000_out_43111nmn2_tsvd_2000_1000_out_43113*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_nmn2_tsvd_2000_1000_out_layer_call_and_return_conditional_losses_4257521
/nmn2_tsvd_2000_1000_out/StatefulPartitionedCall�
concatenate_1/PartitionedCallPartitionedCall7nmn2_pca_2000_1000_out/StatefulPartitionedCall:output:08nmn2_umap_2000_1000_out/StatefulPartitionedCall:output:08nmn2_tsvd_2000_1000_out/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *Q
fLRJ
H__inference_concatenate_1_layer_call_and_return_conditional_losses_425892
concatenate_1/PartitionedCall�
joined/StatefulPartitionedCallStatefulPartitionedCall&concatenate_1/PartitionedCall:output:0joined_43117joined_43119*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *J
fERC
A__inference_joined_layer_call_and_return_conditional_losses_426022 
joined/StatefulPartitionedCall�
IdentityIdentity'joined/StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identity�
NoOpNoOp^joined/StatefulPartitionedCall-^nmn2_pca_2000_1000_1/StatefulPartitionedCall-^nmn2_pca_2000_1000_2/StatefulPartitionedCall-^nmn2_pca_2000_1000_3/StatefulPartitionedCall/^nmn2_pca_2000_1000_out/StatefulPartitionedCall.^nmn2_tsvd_2000_1000_1/StatefulPartitionedCall.^nmn2_tsvd_2000_1000_2/StatefulPartitionedCall.^nmn2_tsvd_2000_1000_3/StatefulPartitionedCall0^nmn2_tsvd_2000_1000_out/StatefulPartitionedCall.^nmn2_umap_2000_1000_1/StatefulPartitionedCall.^nmn2_umap_2000_1000_2/StatefulPartitionedCall.^nmn2_umap_2000_1000_3/StatefulPartitionedCall0^nmn2_umap_2000_1000_out/StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*�
_input_shapesr
p:����������:����������:����������: : : : : : : : : : : : : : : : : : : : : : : : : : 2@
joined/StatefulPartitionedCalljoined/StatefulPartitionedCall2\
,nmn2_pca_2000_1000_1/StatefulPartitionedCall,nmn2_pca_2000_1000_1/StatefulPartitionedCall2\
,nmn2_pca_2000_1000_2/StatefulPartitionedCall,nmn2_pca_2000_1000_2/StatefulPartitionedCall2\
,nmn2_pca_2000_1000_3/StatefulPartitionedCall,nmn2_pca_2000_1000_3/StatefulPartitionedCall2`
.nmn2_pca_2000_1000_out/StatefulPartitionedCall.nmn2_pca_2000_1000_out/StatefulPartitionedCall2^
-nmn2_tsvd_2000_1000_1/StatefulPartitionedCall-nmn2_tsvd_2000_1000_1/StatefulPartitionedCall2^
-nmn2_tsvd_2000_1000_2/StatefulPartitionedCall-nmn2_tsvd_2000_1000_2/StatefulPartitionedCall2^
-nmn2_tsvd_2000_1000_3/StatefulPartitionedCall-nmn2_tsvd_2000_1000_3/StatefulPartitionedCall2b
/nmn2_tsvd_2000_1000_out/StatefulPartitionedCall/nmn2_tsvd_2000_1000_out/StatefulPartitionedCall2^
-nmn2_umap_2000_1000_1/StatefulPartitionedCall-nmn2_umap_2000_1000_1/StatefulPartitionedCall2^
-nmn2_umap_2000_1000_2/StatefulPartitionedCall-nmn2_umap_2000_1000_2/StatefulPartitionedCall2^
-nmn2_umap_2000_1000_3/StatefulPartitionedCall-nmn2_umap_2000_1000_3/StatefulPartitionedCall2b
/nmn2_umap_2000_1000_out/StatefulPartitionedCall/nmn2_umap_2000_1000_out/StatefulPartitionedCall:b ^
(
_output_shapes
:����������
2
_user_specified_namenmn2_pca_2000_1000_input:c_
(
_output_shapes
:����������
3
_user_specified_namenmn2_umap_2000_1000_input:c_
(
_output_shapes
:����������
3
_user_specified_namenmn2_tsvd_2000_1000_input
�
�
P__inference_nmn2_umap_2000_1000_2_layer_call_and_return_conditional_losses_43669

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
P__inference_nmn2_umap_2000_1000_1_layer_call_and_return_conditional_losses_43609

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
P__inference_nmn2_umap_2000_1000_3_layer_call_and_return_conditional_losses_43729

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�Z
�
@__inference_model_layer_call_and_return_conditional_losses_43195
nmn2_pca_2000_1000_input
nmn2_umap_2000_1000_input
nmn2_tsvd_2000_1000_input/
nmn2_tsvd_2000_1000_1_43128:
��*
nmn2_tsvd_2000_1000_1_43130:	�/
nmn2_umap_2000_1000_1_43133:
��*
nmn2_umap_2000_1000_1_43135:	�.
nmn2_pca_2000_1000_1_43138:
��)
nmn2_pca_2000_1000_1_43140:	�/
nmn2_tsvd_2000_1000_2_43143:
��*
nmn2_tsvd_2000_1000_2_43145:	�/
nmn2_umap_2000_1000_2_43148:
��*
nmn2_umap_2000_1000_2_43150:	�.
nmn2_pca_2000_1000_2_43153:
��)
nmn2_pca_2000_1000_2_43155:	�/
nmn2_tsvd_2000_1000_3_43158:
��*
nmn2_tsvd_2000_1000_3_43160:	�/
nmn2_umap_2000_1000_3_43163:
��*
nmn2_umap_2000_1000_3_43165:	�.
nmn2_pca_2000_1000_3_43168:
��)
nmn2_pca_2000_1000_3_43170:	�/
nmn2_pca_2000_1000_out_43173:	�*
nmn2_pca_2000_1000_out_43175:0
nmn2_umap_2000_1000_out_43178:	�+
nmn2_umap_2000_1000_out_43180:0
nmn2_tsvd_2000_1000_out_43183:	�+
nmn2_tsvd_2000_1000_out_43185:
joined_43189:
joined_43191:
identity��joined/StatefulPartitionedCall�,nmn2_pca_2000_1000_1/StatefulPartitionedCall�,nmn2_pca_2000_1000_2/StatefulPartitionedCall�,nmn2_pca_2000_1000_3/StatefulPartitionedCall�.nmn2_pca_2000_1000_out/StatefulPartitionedCall�-nmn2_tsvd_2000_1000_1/StatefulPartitionedCall�-nmn2_tsvd_2000_1000_2/StatefulPartitionedCall�-nmn2_tsvd_2000_1000_3/StatefulPartitionedCall�/nmn2_tsvd_2000_1000_out/StatefulPartitionedCall�-nmn2_umap_2000_1000_1/StatefulPartitionedCall�-nmn2_umap_2000_1000_2/StatefulPartitionedCall�-nmn2_umap_2000_1000_3/StatefulPartitionedCall�/nmn2_umap_2000_1000_out/StatefulPartitionedCall�
-nmn2_tsvd_2000_1000_1/StatefulPartitionedCallStatefulPartitionedCallnmn2_tsvd_2000_1000_inputnmn2_tsvd_2000_1000_1_43128nmn2_tsvd_2000_1000_1_43130*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Y
fTRR
P__inference_nmn2_tsvd_2000_1000_1_layer_call_and_return_conditional_losses_423882/
-nmn2_tsvd_2000_1000_1/StatefulPartitionedCall�
-nmn2_umap_2000_1000_1/StatefulPartitionedCallStatefulPartitionedCallnmn2_umap_2000_1000_inputnmn2_umap_2000_1000_1_43133nmn2_umap_2000_1000_1_43135*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Y
fTRR
P__inference_nmn2_umap_2000_1000_1_layer_call_and_return_conditional_losses_424052/
-nmn2_umap_2000_1000_1/StatefulPartitionedCall�
,nmn2_pca_2000_1000_1/StatefulPartitionedCallStatefulPartitionedCallnmn2_pca_2000_1000_inputnmn2_pca_2000_1000_1_43138nmn2_pca_2000_1000_1_43140*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *X
fSRQ
O__inference_nmn2_pca_2000_1000_1_layer_call_and_return_conditional_losses_424222.
,nmn2_pca_2000_1000_1/StatefulPartitionedCall�
-nmn2_tsvd_2000_1000_2/StatefulPartitionedCallStatefulPartitionedCall6nmn2_tsvd_2000_1000_1/StatefulPartitionedCall:output:0nmn2_tsvd_2000_1000_2_43143nmn2_tsvd_2000_1000_2_43145*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Y
fTRR
P__inference_nmn2_tsvd_2000_1000_2_layer_call_and_return_conditional_losses_424392/
-nmn2_tsvd_2000_1000_2/StatefulPartitionedCall�
-nmn2_umap_2000_1000_2/StatefulPartitionedCallStatefulPartitionedCall6nmn2_umap_2000_1000_1/StatefulPartitionedCall:output:0nmn2_umap_2000_1000_2_43148nmn2_umap_2000_1000_2_43150*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Y
fTRR
P__inference_nmn2_umap_2000_1000_2_layer_call_and_return_conditional_losses_424562/
-nmn2_umap_2000_1000_2/StatefulPartitionedCall�
,nmn2_pca_2000_1000_2/StatefulPartitionedCallStatefulPartitionedCall5nmn2_pca_2000_1000_1/StatefulPartitionedCall:output:0nmn2_pca_2000_1000_2_43153nmn2_pca_2000_1000_2_43155*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *X
fSRQ
O__inference_nmn2_pca_2000_1000_2_layer_call_and_return_conditional_losses_424732.
,nmn2_pca_2000_1000_2/StatefulPartitionedCall�
-nmn2_tsvd_2000_1000_3/StatefulPartitionedCallStatefulPartitionedCall6nmn2_tsvd_2000_1000_2/StatefulPartitionedCall:output:0nmn2_tsvd_2000_1000_3_43158nmn2_tsvd_2000_1000_3_43160*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Y
fTRR
P__inference_nmn2_tsvd_2000_1000_3_layer_call_and_return_conditional_losses_424902/
-nmn2_tsvd_2000_1000_3/StatefulPartitionedCall�
-nmn2_umap_2000_1000_3/StatefulPartitionedCallStatefulPartitionedCall6nmn2_umap_2000_1000_2/StatefulPartitionedCall:output:0nmn2_umap_2000_1000_3_43163nmn2_umap_2000_1000_3_43165*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Y
fTRR
P__inference_nmn2_umap_2000_1000_3_layer_call_and_return_conditional_losses_425072/
-nmn2_umap_2000_1000_3/StatefulPartitionedCall�
,nmn2_pca_2000_1000_3/StatefulPartitionedCallStatefulPartitionedCall5nmn2_pca_2000_1000_2/StatefulPartitionedCall:output:0nmn2_pca_2000_1000_3_43168nmn2_pca_2000_1000_3_43170*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *X
fSRQ
O__inference_nmn2_pca_2000_1000_3_layer_call_and_return_conditional_losses_425242.
,nmn2_pca_2000_1000_3/StatefulPartitionedCall�
.nmn2_pca_2000_1000_out/StatefulPartitionedCallStatefulPartitionedCall5nmn2_pca_2000_1000_3/StatefulPartitionedCall:output:0nmn2_pca_2000_1000_out_43173nmn2_pca_2000_1000_out_43175*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_nmn2_pca_2000_1000_out_layer_call_and_return_conditional_losses_4254120
.nmn2_pca_2000_1000_out/StatefulPartitionedCall�
/nmn2_umap_2000_1000_out/StatefulPartitionedCallStatefulPartitionedCall6nmn2_umap_2000_1000_3/StatefulPartitionedCall:output:0nmn2_umap_2000_1000_out_43178nmn2_umap_2000_1000_out_43180*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_nmn2_umap_2000_1000_out_layer_call_and_return_conditional_losses_4255821
/nmn2_umap_2000_1000_out/StatefulPartitionedCall�
/nmn2_tsvd_2000_1000_out/StatefulPartitionedCallStatefulPartitionedCall6nmn2_tsvd_2000_1000_3/StatefulPartitionedCall:output:0nmn2_tsvd_2000_1000_out_43183nmn2_tsvd_2000_1000_out_43185*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_nmn2_tsvd_2000_1000_out_layer_call_and_return_conditional_losses_4257521
/nmn2_tsvd_2000_1000_out/StatefulPartitionedCall�
concatenate_1/PartitionedCallPartitionedCall7nmn2_pca_2000_1000_out/StatefulPartitionedCall:output:08nmn2_umap_2000_1000_out/StatefulPartitionedCall:output:08nmn2_tsvd_2000_1000_out/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *Q
fLRJ
H__inference_concatenate_1_layer_call_and_return_conditional_losses_425892
concatenate_1/PartitionedCall�
joined/StatefulPartitionedCallStatefulPartitionedCall&concatenate_1/PartitionedCall:output:0joined_43189joined_43191*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *J
fERC
A__inference_joined_layer_call_and_return_conditional_losses_426022 
joined/StatefulPartitionedCall�
IdentityIdentity'joined/StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identity�
NoOpNoOp^joined/StatefulPartitionedCall-^nmn2_pca_2000_1000_1/StatefulPartitionedCall-^nmn2_pca_2000_1000_2/StatefulPartitionedCall-^nmn2_pca_2000_1000_3/StatefulPartitionedCall/^nmn2_pca_2000_1000_out/StatefulPartitionedCall.^nmn2_tsvd_2000_1000_1/StatefulPartitionedCall.^nmn2_tsvd_2000_1000_2/StatefulPartitionedCall.^nmn2_tsvd_2000_1000_3/StatefulPartitionedCall0^nmn2_tsvd_2000_1000_out/StatefulPartitionedCall.^nmn2_umap_2000_1000_1/StatefulPartitionedCall.^nmn2_umap_2000_1000_2/StatefulPartitionedCall.^nmn2_umap_2000_1000_3/StatefulPartitionedCall0^nmn2_umap_2000_1000_out/StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*�
_input_shapesr
p:����������:����������:����������: : : : : : : : : : : : : : : : : : : : : : : : : : 2@
joined/StatefulPartitionedCalljoined/StatefulPartitionedCall2\
,nmn2_pca_2000_1000_1/StatefulPartitionedCall,nmn2_pca_2000_1000_1/StatefulPartitionedCall2\
,nmn2_pca_2000_1000_2/StatefulPartitionedCall,nmn2_pca_2000_1000_2/StatefulPartitionedCall2\
,nmn2_pca_2000_1000_3/StatefulPartitionedCall,nmn2_pca_2000_1000_3/StatefulPartitionedCall2`
.nmn2_pca_2000_1000_out/StatefulPartitionedCall.nmn2_pca_2000_1000_out/StatefulPartitionedCall2^
-nmn2_tsvd_2000_1000_1/StatefulPartitionedCall-nmn2_tsvd_2000_1000_1/StatefulPartitionedCall2^
-nmn2_tsvd_2000_1000_2/StatefulPartitionedCall-nmn2_tsvd_2000_1000_2/StatefulPartitionedCall2^
-nmn2_tsvd_2000_1000_3/StatefulPartitionedCall-nmn2_tsvd_2000_1000_3/StatefulPartitionedCall2b
/nmn2_tsvd_2000_1000_out/StatefulPartitionedCall/nmn2_tsvd_2000_1000_out/StatefulPartitionedCall2^
-nmn2_umap_2000_1000_1/StatefulPartitionedCall-nmn2_umap_2000_1000_1/StatefulPartitionedCall2^
-nmn2_umap_2000_1000_2/StatefulPartitionedCall-nmn2_umap_2000_1000_2/StatefulPartitionedCall2^
-nmn2_umap_2000_1000_3/StatefulPartitionedCall-nmn2_umap_2000_1000_3/StatefulPartitionedCall2b
/nmn2_umap_2000_1000_out/StatefulPartitionedCall/nmn2_umap_2000_1000_out/StatefulPartitionedCall:b ^
(
_output_shapes
:����������
2
_user_specified_namenmn2_pca_2000_1000_input:c_
(
_output_shapes
:����������
3
_user_specified_namenmn2_umap_2000_1000_input:c_
(
_output_shapes
:����������
3
_user_specified_namenmn2_tsvd_2000_1000_input
է
�
!__inference__traced_restore_44122
file_prefix@
,assignvariableop_nmn2_pca_2000_1000_1_kernel:
��;
,assignvariableop_1_nmn2_pca_2000_1000_1_bias:	�C
/assignvariableop_2_nmn2_umap_2000_1000_1_kernel:
��<
-assignvariableop_3_nmn2_umap_2000_1000_1_bias:	�C
/assignvariableop_4_nmn2_tsvd_2000_1000_1_kernel:
��<
-assignvariableop_5_nmn2_tsvd_2000_1000_1_bias:	�B
.assignvariableop_6_nmn2_pca_2000_1000_2_kernel:
��;
,assignvariableop_7_nmn2_pca_2000_1000_2_bias:	�C
/assignvariableop_8_nmn2_umap_2000_1000_2_kernel:
��<
-assignvariableop_9_nmn2_umap_2000_1000_2_bias:	�D
0assignvariableop_10_nmn2_tsvd_2000_1000_2_kernel:
��=
.assignvariableop_11_nmn2_tsvd_2000_1000_2_bias:	�C
/assignvariableop_12_nmn2_pca_2000_1000_3_kernel:
��<
-assignvariableop_13_nmn2_pca_2000_1000_3_bias:	�D
0assignvariableop_14_nmn2_umap_2000_1000_3_kernel:
��=
.assignvariableop_15_nmn2_umap_2000_1000_3_bias:	�D
0assignvariableop_16_nmn2_tsvd_2000_1000_3_kernel:
��=
.assignvariableop_17_nmn2_tsvd_2000_1000_3_bias:	�D
1assignvariableop_18_nmn2_pca_2000_1000_out_kernel:	�=
/assignvariableop_19_nmn2_pca_2000_1000_out_bias:E
2assignvariableop_20_nmn2_umap_2000_1000_out_kernel:	�>
0assignvariableop_21_nmn2_umap_2000_1000_out_bias:E
2assignvariableop_22_nmn2_tsvd_2000_1000_out_kernel:	�>
0assignvariableop_23_nmn2_tsvd_2000_1000_out_bias:3
!assignvariableop_24_joined_kernel:-
assignvariableop_25_joined_bias:'
assignvariableop_26_adam_iter:	 )
assignvariableop_27_adam_beta_1: )
assignvariableop_28_adam_beta_2: (
assignvariableop_29_adam_decay: 0
&assignvariableop_30_adam_learning_rate: #
assignvariableop_31_total: #
assignvariableop_32_count: %
assignvariableop_33_total_1: %
assignvariableop_34_count_1: :
(assignvariableop_35_adam_joined_kernel_m:4
&assignvariableop_36_adam_joined_bias_m::
(assignvariableop_37_adam_joined_kernel_v:4
&assignvariableop_38_adam_joined_bias_v:
identity_40��AssignVariableOp�AssignVariableOp_1�AssignVariableOp_10�AssignVariableOp_11�AssignVariableOp_12�AssignVariableOp_13�AssignVariableOp_14�AssignVariableOp_15�AssignVariableOp_16�AssignVariableOp_17�AssignVariableOp_18�AssignVariableOp_19�AssignVariableOp_2�AssignVariableOp_20�AssignVariableOp_21�AssignVariableOp_22�AssignVariableOp_23�AssignVariableOp_24�AssignVariableOp_25�AssignVariableOp_26�AssignVariableOp_27�AssignVariableOp_28�AssignVariableOp_29�AssignVariableOp_3�AssignVariableOp_30�AssignVariableOp_31�AssignVariableOp_32�AssignVariableOp_33�AssignVariableOp_34�AssignVariableOp_35�AssignVariableOp_36�AssignVariableOp_37�AssignVariableOp_38�AssignVariableOp_4�AssignVariableOp_5�AssignVariableOp_6�AssignVariableOp_7�AssignVariableOp_8�AssignVariableOp_9�
RestoreV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:(*
dtype0*�
value�B�(B6layer_with_weights-0/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-0/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-1/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-1/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-3/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-3/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-4/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-4/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-5/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-5/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-6/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-6/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-7/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-7/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-8/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-8/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-9/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-9/bias/.ATTRIBUTES/VARIABLE_VALUEB7layer_with_weights-10/kernel/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-10/bias/.ATTRIBUTES/VARIABLE_VALUEB7layer_with_weights-11/kernel/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-11/bias/.ATTRIBUTES/VARIABLE_VALUEB7layer_with_weights-12/kernel/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-12/bias/.ATTRIBUTES/VARIABLE_VALUEB)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUEB*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/count/.ATTRIBUTES/VARIABLE_VALUEBSlayer_with_weights-12/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-12/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBSlayer_with_weights-12/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-12/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEB_CHECKPOINTABLE_OBJECT_GRAPH2
RestoreV2/tensor_names�
RestoreV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:(*
dtype0*c
valueZBX(B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B 2
RestoreV2/shape_and_slices�
	RestoreV2	RestoreV2file_prefixRestoreV2/tensor_names:output:0#RestoreV2/shape_and_slices:output:0"/device:CPU:0*�
_output_shapes�
�::::::::::::::::::::::::::::::::::::::::*6
dtypes,
*2(	2
	RestoreV2g
IdentityIdentityRestoreV2:tensors:0"/device:CPU:0*
T0*
_output_shapes
:2

Identity�
AssignVariableOpAssignVariableOp,assignvariableop_nmn2_pca_2000_1000_1_kernelIdentity:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOpk

Identity_1IdentityRestoreV2:tensors:1"/device:CPU:0*
T0*
_output_shapes
:2

Identity_1�
AssignVariableOp_1AssignVariableOp,assignvariableop_1_nmn2_pca_2000_1000_1_biasIdentity_1:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_1k

Identity_2IdentityRestoreV2:tensors:2"/device:CPU:0*
T0*
_output_shapes
:2

Identity_2�
AssignVariableOp_2AssignVariableOp/assignvariableop_2_nmn2_umap_2000_1000_1_kernelIdentity_2:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_2k

Identity_3IdentityRestoreV2:tensors:3"/device:CPU:0*
T0*
_output_shapes
:2

Identity_3�
AssignVariableOp_3AssignVariableOp-assignvariableop_3_nmn2_umap_2000_1000_1_biasIdentity_3:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_3k

Identity_4IdentityRestoreV2:tensors:4"/device:CPU:0*
T0*
_output_shapes
:2

Identity_4�
AssignVariableOp_4AssignVariableOp/assignvariableop_4_nmn2_tsvd_2000_1000_1_kernelIdentity_4:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_4k

Identity_5IdentityRestoreV2:tensors:5"/device:CPU:0*
T0*
_output_shapes
:2

Identity_5�
AssignVariableOp_5AssignVariableOp-assignvariableop_5_nmn2_tsvd_2000_1000_1_biasIdentity_5:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_5k

Identity_6IdentityRestoreV2:tensors:6"/device:CPU:0*
T0*
_output_shapes
:2

Identity_6�
AssignVariableOp_6AssignVariableOp.assignvariableop_6_nmn2_pca_2000_1000_2_kernelIdentity_6:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_6k

Identity_7IdentityRestoreV2:tensors:7"/device:CPU:0*
T0*
_output_shapes
:2

Identity_7�
AssignVariableOp_7AssignVariableOp,assignvariableop_7_nmn2_pca_2000_1000_2_biasIdentity_7:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_7k

Identity_8IdentityRestoreV2:tensors:8"/device:CPU:0*
T0*
_output_shapes
:2

Identity_8�
AssignVariableOp_8AssignVariableOp/assignvariableop_8_nmn2_umap_2000_1000_2_kernelIdentity_8:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_8k

Identity_9IdentityRestoreV2:tensors:9"/device:CPU:0*
T0*
_output_shapes
:2

Identity_9�
AssignVariableOp_9AssignVariableOp-assignvariableop_9_nmn2_umap_2000_1000_2_biasIdentity_9:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_9n
Identity_10IdentityRestoreV2:tensors:10"/device:CPU:0*
T0*
_output_shapes
:2
Identity_10�
AssignVariableOp_10AssignVariableOp0assignvariableop_10_nmn2_tsvd_2000_1000_2_kernelIdentity_10:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_10n
Identity_11IdentityRestoreV2:tensors:11"/device:CPU:0*
T0*
_output_shapes
:2
Identity_11�
AssignVariableOp_11AssignVariableOp.assignvariableop_11_nmn2_tsvd_2000_1000_2_biasIdentity_11:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_11n
Identity_12IdentityRestoreV2:tensors:12"/device:CPU:0*
T0*
_output_shapes
:2
Identity_12�
AssignVariableOp_12AssignVariableOp/assignvariableop_12_nmn2_pca_2000_1000_3_kernelIdentity_12:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_12n
Identity_13IdentityRestoreV2:tensors:13"/device:CPU:0*
T0*
_output_shapes
:2
Identity_13�
AssignVariableOp_13AssignVariableOp-assignvariableop_13_nmn2_pca_2000_1000_3_biasIdentity_13:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_13n
Identity_14IdentityRestoreV2:tensors:14"/device:CPU:0*
T0*
_output_shapes
:2
Identity_14�
AssignVariableOp_14AssignVariableOp0assignvariableop_14_nmn2_umap_2000_1000_3_kernelIdentity_14:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_14n
Identity_15IdentityRestoreV2:tensors:15"/device:CPU:0*
T0*
_output_shapes
:2
Identity_15�
AssignVariableOp_15AssignVariableOp.assignvariableop_15_nmn2_umap_2000_1000_3_biasIdentity_15:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_15n
Identity_16IdentityRestoreV2:tensors:16"/device:CPU:0*
T0*
_output_shapes
:2
Identity_16�
AssignVariableOp_16AssignVariableOp0assignvariableop_16_nmn2_tsvd_2000_1000_3_kernelIdentity_16:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_16n
Identity_17IdentityRestoreV2:tensors:17"/device:CPU:0*
T0*
_output_shapes
:2
Identity_17�
AssignVariableOp_17AssignVariableOp.assignvariableop_17_nmn2_tsvd_2000_1000_3_biasIdentity_17:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_17n
Identity_18IdentityRestoreV2:tensors:18"/device:CPU:0*
T0*
_output_shapes
:2
Identity_18�
AssignVariableOp_18AssignVariableOp1assignvariableop_18_nmn2_pca_2000_1000_out_kernelIdentity_18:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_18n
Identity_19IdentityRestoreV2:tensors:19"/device:CPU:0*
T0*
_output_shapes
:2
Identity_19�
AssignVariableOp_19AssignVariableOp/assignvariableop_19_nmn2_pca_2000_1000_out_biasIdentity_19:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_19n
Identity_20IdentityRestoreV2:tensors:20"/device:CPU:0*
T0*
_output_shapes
:2
Identity_20�
AssignVariableOp_20AssignVariableOp2assignvariableop_20_nmn2_umap_2000_1000_out_kernelIdentity_20:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_20n
Identity_21IdentityRestoreV2:tensors:21"/device:CPU:0*
T0*
_output_shapes
:2
Identity_21�
AssignVariableOp_21AssignVariableOp0assignvariableop_21_nmn2_umap_2000_1000_out_biasIdentity_21:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_21n
Identity_22IdentityRestoreV2:tensors:22"/device:CPU:0*
T0*
_output_shapes
:2
Identity_22�
AssignVariableOp_22AssignVariableOp2assignvariableop_22_nmn2_tsvd_2000_1000_out_kernelIdentity_22:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_22n
Identity_23IdentityRestoreV2:tensors:23"/device:CPU:0*
T0*
_output_shapes
:2
Identity_23�
AssignVariableOp_23AssignVariableOp0assignvariableop_23_nmn2_tsvd_2000_1000_out_biasIdentity_23:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_23n
Identity_24IdentityRestoreV2:tensors:24"/device:CPU:0*
T0*
_output_shapes
:2
Identity_24�
AssignVariableOp_24AssignVariableOp!assignvariableop_24_joined_kernelIdentity_24:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_24n
Identity_25IdentityRestoreV2:tensors:25"/device:CPU:0*
T0*
_output_shapes
:2
Identity_25�
AssignVariableOp_25AssignVariableOpassignvariableop_25_joined_biasIdentity_25:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_25n
Identity_26IdentityRestoreV2:tensors:26"/device:CPU:0*
T0	*
_output_shapes
:2
Identity_26�
AssignVariableOp_26AssignVariableOpassignvariableop_26_adam_iterIdentity_26:output:0"/device:CPU:0*
_output_shapes
 *
dtype0	2
AssignVariableOp_26n
Identity_27IdentityRestoreV2:tensors:27"/device:CPU:0*
T0*
_output_shapes
:2
Identity_27�
AssignVariableOp_27AssignVariableOpassignvariableop_27_adam_beta_1Identity_27:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_27n
Identity_28IdentityRestoreV2:tensors:28"/device:CPU:0*
T0*
_output_shapes
:2
Identity_28�
AssignVariableOp_28AssignVariableOpassignvariableop_28_adam_beta_2Identity_28:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_28n
Identity_29IdentityRestoreV2:tensors:29"/device:CPU:0*
T0*
_output_shapes
:2
Identity_29�
AssignVariableOp_29AssignVariableOpassignvariableop_29_adam_decayIdentity_29:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_29n
Identity_30IdentityRestoreV2:tensors:30"/device:CPU:0*
T0*
_output_shapes
:2
Identity_30�
AssignVariableOp_30AssignVariableOp&assignvariableop_30_adam_learning_rateIdentity_30:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_30n
Identity_31IdentityRestoreV2:tensors:31"/device:CPU:0*
T0*
_output_shapes
:2
Identity_31�
AssignVariableOp_31AssignVariableOpassignvariableop_31_totalIdentity_31:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_31n
Identity_32IdentityRestoreV2:tensors:32"/device:CPU:0*
T0*
_output_shapes
:2
Identity_32�
AssignVariableOp_32AssignVariableOpassignvariableop_32_countIdentity_32:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_32n
Identity_33IdentityRestoreV2:tensors:33"/device:CPU:0*
T0*
_output_shapes
:2
Identity_33�
AssignVariableOp_33AssignVariableOpassignvariableop_33_total_1Identity_33:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_33n
Identity_34IdentityRestoreV2:tensors:34"/device:CPU:0*
T0*
_output_shapes
:2
Identity_34�
AssignVariableOp_34AssignVariableOpassignvariableop_34_count_1Identity_34:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_34n
Identity_35IdentityRestoreV2:tensors:35"/device:CPU:0*
T0*
_output_shapes
:2
Identity_35�
AssignVariableOp_35AssignVariableOp(assignvariableop_35_adam_joined_kernel_mIdentity_35:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_35n
Identity_36IdentityRestoreV2:tensors:36"/device:CPU:0*
T0*
_output_shapes
:2
Identity_36�
AssignVariableOp_36AssignVariableOp&assignvariableop_36_adam_joined_bias_mIdentity_36:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_36n
Identity_37IdentityRestoreV2:tensors:37"/device:CPU:0*
T0*
_output_shapes
:2
Identity_37�
AssignVariableOp_37AssignVariableOp(assignvariableop_37_adam_joined_kernel_vIdentity_37:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_37n
Identity_38IdentityRestoreV2:tensors:38"/device:CPU:0*
T0*
_output_shapes
:2
Identity_38�
AssignVariableOp_38AssignVariableOp&assignvariableop_38_adam_joined_bias_vIdentity_38:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_389
NoOpNoOp"/device:CPU:0*
_output_shapes
 2
NoOp�
Identity_39Identityfile_prefix^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_12^AssignVariableOp_13^AssignVariableOp_14^AssignVariableOp_15^AssignVariableOp_16^AssignVariableOp_17^AssignVariableOp_18^AssignVariableOp_19^AssignVariableOp_2^AssignVariableOp_20^AssignVariableOp_21^AssignVariableOp_22^AssignVariableOp_23^AssignVariableOp_24^AssignVariableOp_25^AssignVariableOp_26^AssignVariableOp_27^AssignVariableOp_28^AssignVariableOp_29^AssignVariableOp_3^AssignVariableOp_30^AssignVariableOp_31^AssignVariableOp_32^AssignVariableOp_33^AssignVariableOp_34^AssignVariableOp_35^AssignVariableOp_36^AssignVariableOp_37^AssignVariableOp_38^AssignVariableOp_4^AssignVariableOp_5^AssignVariableOp_6^AssignVariableOp_7^AssignVariableOp_8^AssignVariableOp_9^NoOp"/device:CPU:0*
T0*
_output_shapes
: 2
Identity_39f
Identity_40IdentityIdentity_39:output:0^NoOp_1*
T0*
_output_shapes
: 2
Identity_40�
NoOp_1NoOp^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_12^AssignVariableOp_13^AssignVariableOp_14^AssignVariableOp_15^AssignVariableOp_16^AssignVariableOp_17^AssignVariableOp_18^AssignVariableOp_19^AssignVariableOp_2^AssignVariableOp_20^AssignVariableOp_21^AssignVariableOp_22^AssignVariableOp_23^AssignVariableOp_24^AssignVariableOp_25^AssignVariableOp_26^AssignVariableOp_27^AssignVariableOp_28^AssignVariableOp_29^AssignVariableOp_3^AssignVariableOp_30^AssignVariableOp_31^AssignVariableOp_32^AssignVariableOp_33^AssignVariableOp_34^AssignVariableOp_35^AssignVariableOp_36^AssignVariableOp_37^AssignVariableOp_38^AssignVariableOp_4^AssignVariableOp_5^AssignVariableOp_6^AssignVariableOp_7^AssignVariableOp_8^AssignVariableOp_9*"
_acd_function_control_output(*
_output_shapes
 2
NoOp_1"#
identity_40Identity_40:output:0*c
_input_shapesR
P: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 2$
AssignVariableOpAssignVariableOp2(
AssignVariableOp_1AssignVariableOp_12*
AssignVariableOp_10AssignVariableOp_102*
AssignVariableOp_11AssignVariableOp_112*
AssignVariableOp_12AssignVariableOp_122*
AssignVariableOp_13AssignVariableOp_132*
AssignVariableOp_14AssignVariableOp_142*
AssignVariableOp_15AssignVariableOp_152*
AssignVariableOp_16AssignVariableOp_162*
AssignVariableOp_17AssignVariableOp_172*
AssignVariableOp_18AssignVariableOp_182*
AssignVariableOp_19AssignVariableOp_192(
AssignVariableOp_2AssignVariableOp_22*
AssignVariableOp_20AssignVariableOp_202*
AssignVariableOp_21AssignVariableOp_212*
AssignVariableOp_22AssignVariableOp_222*
AssignVariableOp_23AssignVariableOp_232*
AssignVariableOp_24AssignVariableOp_242*
AssignVariableOp_25AssignVariableOp_252*
AssignVariableOp_26AssignVariableOp_262*
AssignVariableOp_27AssignVariableOp_272*
AssignVariableOp_28AssignVariableOp_282*
AssignVariableOp_29AssignVariableOp_292(
AssignVariableOp_3AssignVariableOp_32*
AssignVariableOp_30AssignVariableOp_302*
AssignVariableOp_31AssignVariableOp_312*
AssignVariableOp_32AssignVariableOp_322*
AssignVariableOp_33AssignVariableOp_332*
AssignVariableOp_34AssignVariableOp_342*
AssignVariableOp_35AssignVariableOp_352*
AssignVariableOp_36AssignVariableOp_362*
AssignVariableOp_37AssignVariableOp_372*
AssignVariableOp_38AssignVariableOp_382(
AssignVariableOp_4AssignVariableOp_42(
AssignVariableOp_5AssignVariableOp_52(
AssignVariableOp_6AssignVariableOp_62(
AssignVariableOp_7AssignVariableOp_72(
AssignVariableOp_8AssignVariableOp_82(
AssignVariableOp_9AssignVariableOp_9:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix
�
�
4__inference_nmn2_pca_2000_1000_3_layer_call_fn_43718

inputs
unknown:
��
	unknown_0:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *X
fSRQ
O__inference_nmn2_pca_2000_1000_3_layer_call_and_return_conditional_losses_425242
StatefulPartitionedCall|
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:����������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
R__inference_nmn2_tsvd_2000_1000_out_layer_call_and_return_conditional_losses_43809

inputs1
matmul_readvariableop_resource:	�-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes
:	�*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2	
BiasAdda
SigmoidSigmoidBiasAdd:output:0*
T0*'
_output_shapes
:���������2	
Sigmoidf
IdentityIdentitySigmoid:y:0^NoOp*
T0*'
_output_shapes
:���������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
P__inference_nmn2_umap_2000_1000_2_layer_call_and_return_conditional_losses_42456

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
4__inference_nmn2_pca_2000_1000_1_layer_call_fn_43598

inputs
unknown:
��
	unknown_0:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *X
fSRQ
O__inference_nmn2_pca_2000_1000_1_layer_call_and_return_conditional_losses_424222
StatefulPartitionedCall|
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:����������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
ҡ
�
@__inference_model_layer_call_and_return_conditional_losses_43361
inputs_0
inputs_1
inputs_2H
4nmn2_tsvd_2000_1000_1_matmul_readvariableop_resource:
��D
5nmn2_tsvd_2000_1000_1_biasadd_readvariableop_resource:	�H
4nmn2_umap_2000_1000_1_matmul_readvariableop_resource:
��D
5nmn2_umap_2000_1000_1_biasadd_readvariableop_resource:	�G
3nmn2_pca_2000_1000_1_matmul_readvariableop_resource:
��C
4nmn2_pca_2000_1000_1_biasadd_readvariableop_resource:	�H
4nmn2_tsvd_2000_1000_2_matmul_readvariableop_resource:
��D
5nmn2_tsvd_2000_1000_2_biasadd_readvariableop_resource:	�H
4nmn2_umap_2000_1000_2_matmul_readvariableop_resource:
��D
5nmn2_umap_2000_1000_2_biasadd_readvariableop_resource:	�G
3nmn2_pca_2000_1000_2_matmul_readvariableop_resource:
��C
4nmn2_pca_2000_1000_2_biasadd_readvariableop_resource:	�H
4nmn2_tsvd_2000_1000_3_matmul_readvariableop_resource:
��D
5nmn2_tsvd_2000_1000_3_biasadd_readvariableop_resource:	�H
4nmn2_umap_2000_1000_3_matmul_readvariableop_resource:
��D
5nmn2_umap_2000_1000_3_biasadd_readvariableop_resource:	�G
3nmn2_pca_2000_1000_3_matmul_readvariableop_resource:
��C
4nmn2_pca_2000_1000_3_biasadd_readvariableop_resource:	�H
5nmn2_pca_2000_1000_out_matmul_readvariableop_resource:	�D
6nmn2_pca_2000_1000_out_biasadd_readvariableop_resource:I
6nmn2_umap_2000_1000_out_matmul_readvariableop_resource:	�E
7nmn2_umap_2000_1000_out_biasadd_readvariableop_resource:I
6nmn2_tsvd_2000_1000_out_matmul_readvariableop_resource:	�E
7nmn2_tsvd_2000_1000_out_biasadd_readvariableop_resource:7
%joined_matmul_readvariableop_resource:4
&joined_biasadd_readvariableop_resource:
identity��joined/BiasAdd/ReadVariableOp�joined/MatMul/ReadVariableOp�+nmn2_pca_2000_1000_1/BiasAdd/ReadVariableOp�*nmn2_pca_2000_1000_1/MatMul/ReadVariableOp�+nmn2_pca_2000_1000_2/BiasAdd/ReadVariableOp�*nmn2_pca_2000_1000_2/MatMul/ReadVariableOp�+nmn2_pca_2000_1000_3/BiasAdd/ReadVariableOp�*nmn2_pca_2000_1000_3/MatMul/ReadVariableOp�-nmn2_pca_2000_1000_out/BiasAdd/ReadVariableOp�,nmn2_pca_2000_1000_out/MatMul/ReadVariableOp�,nmn2_tsvd_2000_1000_1/BiasAdd/ReadVariableOp�+nmn2_tsvd_2000_1000_1/MatMul/ReadVariableOp�,nmn2_tsvd_2000_1000_2/BiasAdd/ReadVariableOp�+nmn2_tsvd_2000_1000_2/MatMul/ReadVariableOp�,nmn2_tsvd_2000_1000_3/BiasAdd/ReadVariableOp�+nmn2_tsvd_2000_1000_3/MatMul/ReadVariableOp�.nmn2_tsvd_2000_1000_out/BiasAdd/ReadVariableOp�-nmn2_tsvd_2000_1000_out/MatMul/ReadVariableOp�,nmn2_umap_2000_1000_1/BiasAdd/ReadVariableOp�+nmn2_umap_2000_1000_1/MatMul/ReadVariableOp�,nmn2_umap_2000_1000_2/BiasAdd/ReadVariableOp�+nmn2_umap_2000_1000_2/MatMul/ReadVariableOp�,nmn2_umap_2000_1000_3/BiasAdd/ReadVariableOp�+nmn2_umap_2000_1000_3/MatMul/ReadVariableOp�.nmn2_umap_2000_1000_out/BiasAdd/ReadVariableOp�-nmn2_umap_2000_1000_out/MatMul/ReadVariableOp�
+nmn2_tsvd_2000_1000_1/MatMul/ReadVariableOpReadVariableOp4nmn2_tsvd_2000_1000_1_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02-
+nmn2_tsvd_2000_1000_1/MatMul/ReadVariableOp�
nmn2_tsvd_2000_1000_1/MatMulMatMulinputs_23nmn2_tsvd_2000_1000_1/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
nmn2_tsvd_2000_1000_1/MatMul�
,nmn2_tsvd_2000_1000_1/BiasAdd/ReadVariableOpReadVariableOp5nmn2_tsvd_2000_1000_1_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02.
,nmn2_tsvd_2000_1000_1/BiasAdd/ReadVariableOp�
nmn2_tsvd_2000_1000_1/BiasAddBiasAdd&nmn2_tsvd_2000_1000_1/MatMul:product:04nmn2_tsvd_2000_1000_1/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
nmn2_tsvd_2000_1000_1/BiasAdd�
nmn2_tsvd_2000_1000_1/ReluRelu&nmn2_tsvd_2000_1000_1/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
nmn2_tsvd_2000_1000_1/Relu�
+nmn2_umap_2000_1000_1/MatMul/ReadVariableOpReadVariableOp4nmn2_umap_2000_1000_1_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02-
+nmn2_umap_2000_1000_1/MatMul/ReadVariableOp�
nmn2_umap_2000_1000_1/MatMulMatMulinputs_13nmn2_umap_2000_1000_1/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
nmn2_umap_2000_1000_1/MatMul�
,nmn2_umap_2000_1000_1/BiasAdd/ReadVariableOpReadVariableOp5nmn2_umap_2000_1000_1_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02.
,nmn2_umap_2000_1000_1/BiasAdd/ReadVariableOp�
nmn2_umap_2000_1000_1/BiasAddBiasAdd&nmn2_umap_2000_1000_1/MatMul:product:04nmn2_umap_2000_1000_1/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
nmn2_umap_2000_1000_1/BiasAdd�
nmn2_umap_2000_1000_1/ReluRelu&nmn2_umap_2000_1000_1/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
nmn2_umap_2000_1000_1/Relu�
*nmn2_pca_2000_1000_1/MatMul/ReadVariableOpReadVariableOp3nmn2_pca_2000_1000_1_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02,
*nmn2_pca_2000_1000_1/MatMul/ReadVariableOp�
nmn2_pca_2000_1000_1/MatMulMatMulinputs_02nmn2_pca_2000_1000_1/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
nmn2_pca_2000_1000_1/MatMul�
+nmn2_pca_2000_1000_1/BiasAdd/ReadVariableOpReadVariableOp4nmn2_pca_2000_1000_1_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02-
+nmn2_pca_2000_1000_1/BiasAdd/ReadVariableOp�
nmn2_pca_2000_1000_1/BiasAddBiasAdd%nmn2_pca_2000_1000_1/MatMul:product:03nmn2_pca_2000_1000_1/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
nmn2_pca_2000_1000_1/BiasAdd�
nmn2_pca_2000_1000_1/ReluRelu%nmn2_pca_2000_1000_1/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
nmn2_pca_2000_1000_1/Relu�
+nmn2_tsvd_2000_1000_2/MatMul/ReadVariableOpReadVariableOp4nmn2_tsvd_2000_1000_2_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02-
+nmn2_tsvd_2000_1000_2/MatMul/ReadVariableOp�
nmn2_tsvd_2000_1000_2/MatMulMatMul(nmn2_tsvd_2000_1000_1/Relu:activations:03nmn2_tsvd_2000_1000_2/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
nmn2_tsvd_2000_1000_2/MatMul�
,nmn2_tsvd_2000_1000_2/BiasAdd/ReadVariableOpReadVariableOp5nmn2_tsvd_2000_1000_2_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02.
,nmn2_tsvd_2000_1000_2/BiasAdd/ReadVariableOp�
nmn2_tsvd_2000_1000_2/BiasAddBiasAdd&nmn2_tsvd_2000_1000_2/MatMul:product:04nmn2_tsvd_2000_1000_2/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
nmn2_tsvd_2000_1000_2/BiasAdd�
nmn2_tsvd_2000_1000_2/ReluRelu&nmn2_tsvd_2000_1000_2/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
nmn2_tsvd_2000_1000_2/Relu�
+nmn2_umap_2000_1000_2/MatMul/ReadVariableOpReadVariableOp4nmn2_umap_2000_1000_2_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02-
+nmn2_umap_2000_1000_2/MatMul/ReadVariableOp�
nmn2_umap_2000_1000_2/MatMulMatMul(nmn2_umap_2000_1000_1/Relu:activations:03nmn2_umap_2000_1000_2/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
nmn2_umap_2000_1000_2/MatMul�
,nmn2_umap_2000_1000_2/BiasAdd/ReadVariableOpReadVariableOp5nmn2_umap_2000_1000_2_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02.
,nmn2_umap_2000_1000_2/BiasAdd/ReadVariableOp�
nmn2_umap_2000_1000_2/BiasAddBiasAdd&nmn2_umap_2000_1000_2/MatMul:product:04nmn2_umap_2000_1000_2/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
nmn2_umap_2000_1000_2/BiasAdd�
nmn2_umap_2000_1000_2/ReluRelu&nmn2_umap_2000_1000_2/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
nmn2_umap_2000_1000_2/Relu�
*nmn2_pca_2000_1000_2/MatMul/ReadVariableOpReadVariableOp3nmn2_pca_2000_1000_2_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02,
*nmn2_pca_2000_1000_2/MatMul/ReadVariableOp�
nmn2_pca_2000_1000_2/MatMulMatMul'nmn2_pca_2000_1000_1/Relu:activations:02nmn2_pca_2000_1000_2/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
nmn2_pca_2000_1000_2/MatMul�
+nmn2_pca_2000_1000_2/BiasAdd/ReadVariableOpReadVariableOp4nmn2_pca_2000_1000_2_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02-
+nmn2_pca_2000_1000_2/BiasAdd/ReadVariableOp�
nmn2_pca_2000_1000_2/BiasAddBiasAdd%nmn2_pca_2000_1000_2/MatMul:product:03nmn2_pca_2000_1000_2/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
nmn2_pca_2000_1000_2/BiasAdd�
nmn2_pca_2000_1000_2/ReluRelu%nmn2_pca_2000_1000_2/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
nmn2_pca_2000_1000_2/Relu�
+nmn2_tsvd_2000_1000_3/MatMul/ReadVariableOpReadVariableOp4nmn2_tsvd_2000_1000_3_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02-
+nmn2_tsvd_2000_1000_3/MatMul/ReadVariableOp�
nmn2_tsvd_2000_1000_3/MatMulMatMul(nmn2_tsvd_2000_1000_2/Relu:activations:03nmn2_tsvd_2000_1000_3/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
nmn2_tsvd_2000_1000_3/MatMul�
,nmn2_tsvd_2000_1000_3/BiasAdd/ReadVariableOpReadVariableOp5nmn2_tsvd_2000_1000_3_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02.
,nmn2_tsvd_2000_1000_3/BiasAdd/ReadVariableOp�
nmn2_tsvd_2000_1000_3/BiasAddBiasAdd&nmn2_tsvd_2000_1000_3/MatMul:product:04nmn2_tsvd_2000_1000_3/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
nmn2_tsvd_2000_1000_3/BiasAdd�
nmn2_tsvd_2000_1000_3/ReluRelu&nmn2_tsvd_2000_1000_3/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
nmn2_tsvd_2000_1000_3/Relu�
+nmn2_umap_2000_1000_3/MatMul/ReadVariableOpReadVariableOp4nmn2_umap_2000_1000_3_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02-
+nmn2_umap_2000_1000_3/MatMul/ReadVariableOp�
nmn2_umap_2000_1000_3/MatMulMatMul(nmn2_umap_2000_1000_2/Relu:activations:03nmn2_umap_2000_1000_3/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
nmn2_umap_2000_1000_3/MatMul�
,nmn2_umap_2000_1000_3/BiasAdd/ReadVariableOpReadVariableOp5nmn2_umap_2000_1000_3_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02.
,nmn2_umap_2000_1000_3/BiasAdd/ReadVariableOp�
nmn2_umap_2000_1000_3/BiasAddBiasAdd&nmn2_umap_2000_1000_3/MatMul:product:04nmn2_umap_2000_1000_3/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
nmn2_umap_2000_1000_3/BiasAdd�
nmn2_umap_2000_1000_3/ReluRelu&nmn2_umap_2000_1000_3/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
nmn2_umap_2000_1000_3/Relu�
*nmn2_pca_2000_1000_3/MatMul/ReadVariableOpReadVariableOp3nmn2_pca_2000_1000_3_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02,
*nmn2_pca_2000_1000_3/MatMul/ReadVariableOp�
nmn2_pca_2000_1000_3/MatMulMatMul'nmn2_pca_2000_1000_2/Relu:activations:02nmn2_pca_2000_1000_3/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
nmn2_pca_2000_1000_3/MatMul�
+nmn2_pca_2000_1000_3/BiasAdd/ReadVariableOpReadVariableOp4nmn2_pca_2000_1000_3_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02-
+nmn2_pca_2000_1000_3/BiasAdd/ReadVariableOp�
nmn2_pca_2000_1000_3/BiasAddBiasAdd%nmn2_pca_2000_1000_3/MatMul:product:03nmn2_pca_2000_1000_3/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
nmn2_pca_2000_1000_3/BiasAdd�
nmn2_pca_2000_1000_3/ReluRelu%nmn2_pca_2000_1000_3/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
nmn2_pca_2000_1000_3/Relu�
,nmn2_pca_2000_1000_out/MatMul/ReadVariableOpReadVariableOp5nmn2_pca_2000_1000_out_matmul_readvariableop_resource*
_output_shapes
:	�*
dtype02.
,nmn2_pca_2000_1000_out/MatMul/ReadVariableOp�
nmn2_pca_2000_1000_out/MatMulMatMul'nmn2_pca_2000_1000_3/Relu:activations:04nmn2_pca_2000_1000_out/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
nmn2_pca_2000_1000_out/MatMul�
-nmn2_pca_2000_1000_out/BiasAdd/ReadVariableOpReadVariableOp6nmn2_pca_2000_1000_out_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02/
-nmn2_pca_2000_1000_out/BiasAdd/ReadVariableOp�
nmn2_pca_2000_1000_out/BiasAddBiasAdd'nmn2_pca_2000_1000_out/MatMul:product:05nmn2_pca_2000_1000_out/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2 
nmn2_pca_2000_1000_out/BiasAdd�
nmn2_pca_2000_1000_out/SigmoidSigmoid'nmn2_pca_2000_1000_out/BiasAdd:output:0*
T0*'
_output_shapes
:���������2 
nmn2_pca_2000_1000_out/Sigmoid�
-nmn2_umap_2000_1000_out/MatMul/ReadVariableOpReadVariableOp6nmn2_umap_2000_1000_out_matmul_readvariableop_resource*
_output_shapes
:	�*
dtype02/
-nmn2_umap_2000_1000_out/MatMul/ReadVariableOp�
nmn2_umap_2000_1000_out/MatMulMatMul(nmn2_umap_2000_1000_3/Relu:activations:05nmn2_umap_2000_1000_out/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2 
nmn2_umap_2000_1000_out/MatMul�
.nmn2_umap_2000_1000_out/BiasAdd/ReadVariableOpReadVariableOp7nmn2_umap_2000_1000_out_biasadd_readvariableop_resource*
_output_shapes
:*
dtype020
.nmn2_umap_2000_1000_out/BiasAdd/ReadVariableOp�
nmn2_umap_2000_1000_out/BiasAddBiasAdd(nmn2_umap_2000_1000_out/MatMul:product:06nmn2_umap_2000_1000_out/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2!
nmn2_umap_2000_1000_out/BiasAdd�
nmn2_umap_2000_1000_out/SigmoidSigmoid(nmn2_umap_2000_1000_out/BiasAdd:output:0*
T0*'
_output_shapes
:���������2!
nmn2_umap_2000_1000_out/Sigmoid�
-nmn2_tsvd_2000_1000_out/MatMul/ReadVariableOpReadVariableOp6nmn2_tsvd_2000_1000_out_matmul_readvariableop_resource*
_output_shapes
:	�*
dtype02/
-nmn2_tsvd_2000_1000_out/MatMul/ReadVariableOp�
nmn2_tsvd_2000_1000_out/MatMulMatMul(nmn2_tsvd_2000_1000_3/Relu:activations:05nmn2_tsvd_2000_1000_out/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2 
nmn2_tsvd_2000_1000_out/MatMul�
.nmn2_tsvd_2000_1000_out/BiasAdd/ReadVariableOpReadVariableOp7nmn2_tsvd_2000_1000_out_biasadd_readvariableop_resource*
_output_shapes
:*
dtype020
.nmn2_tsvd_2000_1000_out/BiasAdd/ReadVariableOp�
nmn2_tsvd_2000_1000_out/BiasAddBiasAdd(nmn2_tsvd_2000_1000_out/MatMul:product:06nmn2_tsvd_2000_1000_out/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2!
nmn2_tsvd_2000_1000_out/BiasAdd�
nmn2_tsvd_2000_1000_out/SigmoidSigmoid(nmn2_tsvd_2000_1000_out/BiasAdd:output:0*
T0*'
_output_shapes
:���������2!
nmn2_tsvd_2000_1000_out/Sigmoidx
concatenate_1/concat/axisConst*
_output_shapes
: *
dtype0*
value	B :2
concatenate_1/concat/axis�
concatenate_1/concatConcatV2"nmn2_pca_2000_1000_out/Sigmoid:y:0#nmn2_umap_2000_1000_out/Sigmoid:y:0#nmn2_tsvd_2000_1000_out/Sigmoid:y:0"concatenate_1/concat/axis:output:0*
N*
T0*'
_output_shapes
:���������2
concatenate_1/concat�
joined/MatMul/ReadVariableOpReadVariableOp%joined_matmul_readvariableop_resource*
_output_shapes

:*
dtype02
joined/MatMul/ReadVariableOp�
joined/MatMulMatMulconcatenate_1/concat:output:0$joined/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
joined/MatMul�
joined/BiasAdd/ReadVariableOpReadVariableOp&joined_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02
joined/BiasAdd/ReadVariableOp�
joined/BiasAddBiasAddjoined/MatMul:product:0%joined/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
joined/BiasAddv
joined/SigmoidSigmoidjoined/BiasAdd:output:0*
T0*'
_output_shapes
:���������2
joined/Sigmoidm
IdentityIdentityjoined/Sigmoid:y:0^NoOp*
T0*'
_output_shapes
:���������2

Identity�	
NoOpNoOp^joined/BiasAdd/ReadVariableOp^joined/MatMul/ReadVariableOp,^nmn2_pca_2000_1000_1/BiasAdd/ReadVariableOp+^nmn2_pca_2000_1000_1/MatMul/ReadVariableOp,^nmn2_pca_2000_1000_2/BiasAdd/ReadVariableOp+^nmn2_pca_2000_1000_2/MatMul/ReadVariableOp,^nmn2_pca_2000_1000_3/BiasAdd/ReadVariableOp+^nmn2_pca_2000_1000_3/MatMul/ReadVariableOp.^nmn2_pca_2000_1000_out/BiasAdd/ReadVariableOp-^nmn2_pca_2000_1000_out/MatMul/ReadVariableOp-^nmn2_tsvd_2000_1000_1/BiasAdd/ReadVariableOp,^nmn2_tsvd_2000_1000_1/MatMul/ReadVariableOp-^nmn2_tsvd_2000_1000_2/BiasAdd/ReadVariableOp,^nmn2_tsvd_2000_1000_2/MatMul/ReadVariableOp-^nmn2_tsvd_2000_1000_3/BiasAdd/ReadVariableOp,^nmn2_tsvd_2000_1000_3/MatMul/ReadVariableOp/^nmn2_tsvd_2000_1000_out/BiasAdd/ReadVariableOp.^nmn2_tsvd_2000_1000_out/MatMul/ReadVariableOp-^nmn2_umap_2000_1000_1/BiasAdd/ReadVariableOp,^nmn2_umap_2000_1000_1/MatMul/ReadVariableOp-^nmn2_umap_2000_1000_2/BiasAdd/ReadVariableOp,^nmn2_umap_2000_1000_2/MatMul/ReadVariableOp-^nmn2_umap_2000_1000_3/BiasAdd/ReadVariableOp,^nmn2_umap_2000_1000_3/MatMul/ReadVariableOp/^nmn2_umap_2000_1000_out/BiasAdd/ReadVariableOp.^nmn2_umap_2000_1000_out/MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*�
_input_shapesr
p:����������:����������:����������: : : : : : : : : : : : : : : : : : : : : : : : : : 2>
joined/BiasAdd/ReadVariableOpjoined/BiasAdd/ReadVariableOp2<
joined/MatMul/ReadVariableOpjoined/MatMul/ReadVariableOp2Z
+nmn2_pca_2000_1000_1/BiasAdd/ReadVariableOp+nmn2_pca_2000_1000_1/BiasAdd/ReadVariableOp2X
*nmn2_pca_2000_1000_1/MatMul/ReadVariableOp*nmn2_pca_2000_1000_1/MatMul/ReadVariableOp2Z
+nmn2_pca_2000_1000_2/BiasAdd/ReadVariableOp+nmn2_pca_2000_1000_2/BiasAdd/ReadVariableOp2X
*nmn2_pca_2000_1000_2/MatMul/ReadVariableOp*nmn2_pca_2000_1000_2/MatMul/ReadVariableOp2Z
+nmn2_pca_2000_1000_3/BiasAdd/ReadVariableOp+nmn2_pca_2000_1000_3/BiasAdd/ReadVariableOp2X
*nmn2_pca_2000_1000_3/MatMul/ReadVariableOp*nmn2_pca_2000_1000_3/MatMul/ReadVariableOp2^
-nmn2_pca_2000_1000_out/BiasAdd/ReadVariableOp-nmn2_pca_2000_1000_out/BiasAdd/ReadVariableOp2\
,nmn2_pca_2000_1000_out/MatMul/ReadVariableOp,nmn2_pca_2000_1000_out/MatMul/ReadVariableOp2\
,nmn2_tsvd_2000_1000_1/BiasAdd/ReadVariableOp,nmn2_tsvd_2000_1000_1/BiasAdd/ReadVariableOp2Z
+nmn2_tsvd_2000_1000_1/MatMul/ReadVariableOp+nmn2_tsvd_2000_1000_1/MatMul/ReadVariableOp2\
,nmn2_tsvd_2000_1000_2/BiasAdd/ReadVariableOp,nmn2_tsvd_2000_1000_2/BiasAdd/ReadVariableOp2Z
+nmn2_tsvd_2000_1000_2/MatMul/ReadVariableOp+nmn2_tsvd_2000_1000_2/MatMul/ReadVariableOp2\
,nmn2_tsvd_2000_1000_3/BiasAdd/ReadVariableOp,nmn2_tsvd_2000_1000_3/BiasAdd/ReadVariableOp2Z
+nmn2_tsvd_2000_1000_3/MatMul/ReadVariableOp+nmn2_tsvd_2000_1000_3/MatMul/ReadVariableOp2`
.nmn2_tsvd_2000_1000_out/BiasAdd/ReadVariableOp.nmn2_tsvd_2000_1000_out/BiasAdd/ReadVariableOp2^
-nmn2_tsvd_2000_1000_out/MatMul/ReadVariableOp-nmn2_tsvd_2000_1000_out/MatMul/ReadVariableOp2\
,nmn2_umap_2000_1000_1/BiasAdd/ReadVariableOp,nmn2_umap_2000_1000_1/BiasAdd/ReadVariableOp2Z
+nmn2_umap_2000_1000_1/MatMul/ReadVariableOp+nmn2_umap_2000_1000_1/MatMul/ReadVariableOp2\
,nmn2_umap_2000_1000_2/BiasAdd/ReadVariableOp,nmn2_umap_2000_1000_2/BiasAdd/ReadVariableOp2Z
+nmn2_umap_2000_1000_2/MatMul/ReadVariableOp+nmn2_umap_2000_1000_2/MatMul/ReadVariableOp2\
,nmn2_umap_2000_1000_3/BiasAdd/ReadVariableOp,nmn2_umap_2000_1000_3/BiasAdd/ReadVariableOp2Z
+nmn2_umap_2000_1000_3/MatMul/ReadVariableOp+nmn2_umap_2000_1000_3/MatMul/ReadVariableOp2`
.nmn2_umap_2000_1000_out/BiasAdd/ReadVariableOp.nmn2_umap_2000_1000_out/BiasAdd/ReadVariableOp2^
-nmn2_umap_2000_1000_out/MatMul/ReadVariableOp-nmn2_umap_2000_1000_out/MatMul/ReadVariableOp:R N
(
_output_shapes
:����������
"
_user_specified_name
inputs/0:RN
(
_output_shapes
:����������
"
_user_specified_name
inputs/1:RN
(
_output_shapes
:����������
"
_user_specified_name
inputs/2
�Y
�
@__inference_model_layer_call_and_return_conditional_losses_42937

inputs
inputs_1
inputs_2/
nmn2_tsvd_2000_1000_1_42870:
��*
nmn2_tsvd_2000_1000_1_42872:	�/
nmn2_umap_2000_1000_1_42875:
��*
nmn2_umap_2000_1000_1_42877:	�.
nmn2_pca_2000_1000_1_42880:
��)
nmn2_pca_2000_1000_1_42882:	�/
nmn2_tsvd_2000_1000_2_42885:
��*
nmn2_tsvd_2000_1000_2_42887:	�/
nmn2_umap_2000_1000_2_42890:
��*
nmn2_umap_2000_1000_2_42892:	�.
nmn2_pca_2000_1000_2_42895:
��)
nmn2_pca_2000_1000_2_42897:	�/
nmn2_tsvd_2000_1000_3_42900:
��*
nmn2_tsvd_2000_1000_3_42902:	�/
nmn2_umap_2000_1000_3_42905:
��*
nmn2_umap_2000_1000_3_42907:	�.
nmn2_pca_2000_1000_3_42910:
��)
nmn2_pca_2000_1000_3_42912:	�/
nmn2_pca_2000_1000_out_42915:	�*
nmn2_pca_2000_1000_out_42917:0
nmn2_umap_2000_1000_out_42920:	�+
nmn2_umap_2000_1000_out_42922:0
nmn2_tsvd_2000_1000_out_42925:	�+
nmn2_tsvd_2000_1000_out_42927:
joined_42931:
joined_42933:
identity��joined/StatefulPartitionedCall�,nmn2_pca_2000_1000_1/StatefulPartitionedCall�,nmn2_pca_2000_1000_2/StatefulPartitionedCall�,nmn2_pca_2000_1000_3/StatefulPartitionedCall�.nmn2_pca_2000_1000_out/StatefulPartitionedCall�-nmn2_tsvd_2000_1000_1/StatefulPartitionedCall�-nmn2_tsvd_2000_1000_2/StatefulPartitionedCall�-nmn2_tsvd_2000_1000_3/StatefulPartitionedCall�/nmn2_tsvd_2000_1000_out/StatefulPartitionedCall�-nmn2_umap_2000_1000_1/StatefulPartitionedCall�-nmn2_umap_2000_1000_2/StatefulPartitionedCall�-nmn2_umap_2000_1000_3/StatefulPartitionedCall�/nmn2_umap_2000_1000_out/StatefulPartitionedCall�
-nmn2_tsvd_2000_1000_1/StatefulPartitionedCallStatefulPartitionedCallinputs_2nmn2_tsvd_2000_1000_1_42870nmn2_tsvd_2000_1000_1_42872*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Y
fTRR
P__inference_nmn2_tsvd_2000_1000_1_layer_call_and_return_conditional_losses_423882/
-nmn2_tsvd_2000_1000_1/StatefulPartitionedCall�
-nmn2_umap_2000_1000_1/StatefulPartitionedCallStatefulPartitionedCallinputs_1nmn2_umap_2000_1000_1_42875nmn2_umap_2000_1000_1_42877*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Y
fTRR
P__inference_nmn2_umap_2000_1000_1_layer_call_and_return_conditional_losses_424052/
-nmn2_umap_2000_1000_1/StatefulPartitionedCall�
,nmn2_pca_2000_1000_1/StatefulPartitionedCallStatefulPartitionedCallinputsnmn2_pca_2000_1000_1_42880nmn2_pca_2000_1000_1_42882*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *X
fSRQ
O__inference_nmn2_pca_2000_1000_1_layer_call_and_return_conditional_losses_424222.
,nmn2_pca_2000_1000_1/StatefulPartitionedCall�
-nmn2_tsvd_2000_1000_2/StatefulPartitionedCallStatefulPartitionedCall6nmn2_tsvd_2000_1000_1/StatefulPartitionedCall:output:0nmn2_tsvd_2000_1000_2_42885nmn2_tsvd_2000_1000_2_42887*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Y
fTRR
P__inference_nmn2_tsvd_2000_1000_2_layer_call_and_return_conditional_losses_424392/
-nmn2_tsvd_2000_1000_2/StatefulPartitionedCall�
-nmn2_umap_2000_1000_2/StatefulPartitionedCallStatefulPartitionedCall6nmn2_umap_2000_1000_1/StatefulPartitionedCall:output:0nmn2_umap_2000_1000_2_42890nmn2_umap_2000_1000_2_42892*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Y
fTRR
P__inference_nmn2_umap_2000_1000_2_layer_call_and_return_conditional_losses_424562/
-nmn2_umap_2000_1000_2/StatefulPartitionedCall�
,nmn2_pca_2000_1000_2/StatefulPartitionedCallStatefulPartitionedCall5nmn2_pca_2000_1000_1/StatefulPartitionedCall:output:0nmn2_pca_2000_1000_2_42895nmn2_pca_2000_1000_2_42897*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *X
fSRQ
O__inference_nmn2_pca_2000_1000_2_layer_call_and_return_conditional_losses_424732.
,nmn2_pca_2000_1000_2/StatefulPartitionedCall�
-nmn2_tsvd_2000_1000_3/StatefulPartitionedCallStatefulPartitionedCall6nmn2_tsvd_2000_1000_2/StatefulPartitionedCall:output:0nmn2_tsvd_2000_1000_3_42900nmn2_tsvd_2000_1000_3_42902*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Y
fTRR
P__inference_nmn2_tsvd_2000_1000_3_layer_call_and_return_conditional_losses_424902/
-nmn2_tsvd_2000_1000_3/StatefulPartitionedCall�
-nmn2_umap_2000_1000_3/StatefulPartitionedCallStatefulPartitionedCall6nmn2_umap_2000_1000_2/StatefulPartitionedCall:output:0nmn2_umap_2000_1000_3_42905nmn2_umap_2000_1000_3_42907*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Y
fTRR
P__inference_nmn2_umap_2000_1000_3_layer_call_and_return_conditional_losses_425072/
-nmn2_umap_2000_1000_3/StatefulPartitionedCall�
,nmn2_pca_2000_1000_3/StatefulPartitionedCallStatefulPartitionedCall5nmn2_pca_2000_1000_2/StatefulPartitionedCall:output:0nmn2_pca_2000_1000_3_42910nmn2_pca_2000_1000_3_42912*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *X
fSRQ
O__inference_nmn2_pca_2000_1000_3_layer_call_and_return_conditional_losses_425242.
,nmn2_pca_2000_1000_3/StatefulPartitionedCall�
.nmn2_pca_2000_1000_out/StatefulPartitionedCallStatefulPartitionedCall5nmn2_pca_2000_1000_3/StatefulPartitionedCall:output:0nmn2_pca_2000_1000_out_42915nmn2_pca_2000_1000_out_42917*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_nmn2_pca_2000_1000_out_layer_call_and_return_conditional_losses_4254120
.nmn2_pca_2000_1000_out/StatefulPartitionedCall�
/nmn2_umap_2000_1000_out/StatefulPartitionedCallStatefulPartitionedCall6nmn2_umap_2000_1000_3/StatefulPartitionedCall:output:0nmn2_umap_2000_1000_out_42920nmn2_umap_2000_1000_out_42922*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_nmn2_umap_2000_1000_out_layer_call_and_return_conditional_losses_4255821
/nmn2_umap_2000_1000_out/StatefulPartitionedCall�
/nmn2_tsvd_2000_1000_out/StatefulPartitionedCallStatefulPartitionedCall6nmn2_tsvd_2000_1000_3/StatefulPartitionedCall:output:0nmn2_tsvd_2000_1000_out_42925nmn2_tsvd_2000_1000_out_42927*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_nmn2_tsvd_2000_1000_out_layer_call_and_return_conditional_losses_4257521
/nmn2_tsvd_2000_1000_out/StatefulPartitionedCall�
concatenate_1/PartitionedCallPartitionedCall7nmn2_pca_2000_1000_out/StatefulPartitionedCall:output:08nmn2_umap_2000_1000_out/StatefulPartitionedCall:output:08nmn2_tsvd_2000_1000_out/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *Q
fLRJ
H__inference_concatenate_1_layer_call_and_return_conditional_losses_425892
concatenate_1/PartitionedCall�
joined/StatefulPartitionedCallStatefulPartitionedCall&concatenate_1/PartitionedCall:output:0joined_42931joined_42933*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *J
fERC
A__inference_joined_layer_call_and_return_conditional_losses_426022 
joined/StatefulPartitionedCall�
IdentityIdentity'joined/StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identity�
NoOpNoOp^joined/StatefulPartitionedCall-^nmn2_pca_2000_1000_1/StatefulPartitionedCall-^nmn2_pca_2000_1000_2/StatefulPartitionedCall-^nmn2_pca_2000_1000_3/StatefulPartitionedCall/^nmn2_pca_2000_1000_out/StatefulPartitionedCall.^nmn2_tsvd_2000_1000_1/StatefulPartitionedCall.^nmn2_tsvd_2000_1000_2/StatefulPartitionedCall.^nmn2_tsvd_2000_1000_3/StatefulPartitionedCall0^nmn2_tsvd_2000_1000_out/StatefulPartitionedCall.^nmn2_umap_2000_1000_1/StatefulPartitionedCall.^nmn2_umap_2000_1000_2/StatefulPartitionedCall.^nmn2_umap_2000_1000_3/StatefulPartitionedCall0^nmn2_umap_2000_1000_out/StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*�
_input_shapesr
p:����������:����������:����������: : : : : : : : : : : : : : : : : : : : : : : : : : 2@
joined/StatefulPartitionedCalljoined/StatefulPartitionedCall2\
,nmn2_pca_2000_1000_1/StatefulPartitionedCall,nmn2_pca_2000_1000_1/StatefulPartitionedCall2\
,nmn2_pca_2000_1000_2/StatefulPartitionedCall,nmn2_pca_2000_1000_2/StatefulPartitionedCall2\
,nmn2_pca_2000_1000_3/StatefulPartitionedCall,nmn2_pca_2000_1000_3/StatefulPartitionedCall2`
.nmn2_pca_2000_1000_out/StatefulPartitionedCall.nmn2_pca_2000_1000_out/StatefulPartitionedCall2^
-nmn2_tsvd_2000_1000_1/StatefulPartitionedCall-nmn2_tsvd_2000_1000_1/StatefulPartitionedCall2^
-nmn2_tsvd_2000_1000_2/StatefulPartitionedCall-nmn2_tsvd_2000_1000_2/StatefulPartitionedCall2^
-nmn2_tsvd_2000_1000_3/StatefulPartitionedCall-nmn2_tsvd_2000_1000_3/StatefulPartitionedCall2b
/nmn2_tsvd_2000_1000_out/StatefulPartitionedCall/nmn2_tsvd_2000_1000_out/StatefulPartitionedCall2^
-nmn2_umap_2000_1000_1/StatefulPartitionedCall-nmn2_umap_2000_1000_1/StatefulPartitionedCall2^
-nmn2_umap_2000_1000_2/StatefulPartitionedCall-nmn2_umap_2000_1000_2/StatefulPartitionedCall2^
-nmn2_umap_2000_1000_3/StatefulPartitionedCall-nmn2_umap_2000_1000_3/StatefulPartitionedCall2b
/nmn2_umap_2000_1000_out/StatefulPartitionedCall/nmn2_umap_2000_1000_out/StatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs:PL
(
_output_shapes
:����������
 
_user_specified_nameinputs:PL
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
P__inference_nmn2_tsvd_2000_1000_1_layer_call_and_return_conditional_losses_43629

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
O__inference_nmn2_pca_2000_1000_2_layer_call_and_return_conditional_losses_42473

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
O__inference_nmn2_pca_2000_1000_2_layer_call_and_return_conditional_losses_43649

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
5__inference_nmn2_tsvd_2000_1000_2_layer_call_fn_43698

inputs
unknown:
��
	unknown_0:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Y
fTRR
P__inference_nmn2_tsvd_2000_1000_2_layer_call_and_return_conditional_losses_424392
StatefulPartitionedCall|
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:����������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
5__inference_nmn2_tsvd_2000_1000_3_layer_call_fn_43758

inputs
unknown:
��
	unknown_0:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Y
fTRR
P__inference_nmn2_tsvd_2000_1000_3_layer_call_and_return_conditional_losses_424902
StatefulPartitionedCall|
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:����������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
6__inference_nmn2_pca_2000_1000_out_layer_call_fn_43778

inputs
unknown:	�
	unknown_0:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_nmn2_pca_2000_1000_out_layer_call_and_return_conditional_losses_425412
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
A__inference_joined_layer_call_and_return_conditional_losses_42602

inputs0
matmul_readvariableop_resource:-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2	
BiasAdda
SigmoidSigmoidBiasAdd:output:0*
T0*'
_output_shapes
:���������2	
Sigmoidf
IdentityIdentitySigmoid:y:0^NoOp*
T0*'
_output_shapes
:���������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs
��
�
 __inference__wrapped_model_42366
nmn2_pca_2000_1000_input
nmn2_umap_2000_1000_input
nmn2_tsvd_2000_1000_inputN
:model_nmn2_tsvd_2000_1000_1_matmul_readvariableop_resource:
��J
;model_nmn2_tsvd_2000_1000_1_biasadd_readvariableop_resource:	�N
:model_nmn2_umap_2000_1000_1_matmul_readvariableop_resource:
��J
;model_nmn2_umap_2000_1000_1_biasadd_readvariableop_resource:	�M
9model_nmn2_pca_2000_1000_1_matmul_readvariableop_resource:
��I
:model_nmn2_pca_2000_1000_1_biasadd_readvariableop_resource:	�N
:model_nmn2_tsvd_2000_1000_2_matmul_readvariableop_resource:
��J
;model_nmn2_tsvd_2000_1000_2_biasadd_readvariableop_resource:	�N
:model_nmn2_umap_2000_1000_2_matmul_readvariableop_resource:
��J
;model_nmn2_umap_2000_1000_2_biasadd_readvariableop_resource:	�M
9model_nmn2_pca_2000_1000_2_matmul_readvariableop_resource:
��I
:model_nmn2_pca_2000_1000_2_biasadd_readvariableop_resource:	�N
:model_nmn2_tsvd_2000_1000_3_matmul_readvariableop_resource:
��J
;model_nmn2_tsvd_2000_1000_3_biasadd_readvariableop_resource:	�N
:model_nmn2_umap_2000_1000_3_matmul_readvariableop_resource:
��J
;model_nmn2_umap_2000_1000_3_biasadd_readvariableop_resource:	�M
9model_nmn2_pca_2000_1000_3_matmul_readvariableop_resource:
��I
:model_nmn2_pca_2000_1000_3_biasadd_readvariableop_resource:	�N
;model_nmn2_pca_2000_1000_out_matmul_readvariableop_resource:	�J
<model_nmn2_pca_2000_1000_out_biasadd_readvariableop_resource:O
<model_nmn2_umap_2000_1000_out_matmul_readvariableop_resource:	�K
=model_nmn2_umap_2000_1000_out_biasadd_readvariableop_resource:O
<model_nmn2_tsvd_2000_1000_out_matmul_readvariableop_resource:	�K
=model_nmn2_tsvd_2000_1000_out_biasadd_readvariableop_resource:=
+model_joined_matmul_readvariableop_resource::
,model_joined_biasadd_readvariableop_resource:
identity��#model/joined/BiasAdd/ReadVariableOp�"model/joined/MatMul/ReadVariableOp�1model/nmn2_pca_2000_1000_1/BiasAdd/ReadVariableOp�0model/nmn2_pca_2000_1000_1/MatMul/ReadVariableOp�1model/nmn2_pca_2000_1000_2/BiasAdd/ReadVariableOp�0model/nmn2_pca_2000_1000_2/MatMul/ReadVariableOp�1model/nmn2_pca_2000_1000_3/BiasAdd/ReadVariableOp�0model/nmn2_pca_2000_1000_3/MatMul/ReadVariableOp�3model/nmn2_pca_2000_1000_out/BiasAdd/ReadVariableOp�2model/nmn2_pca_2000_1000_out/MatMul/ReadVariableOp�2model/nmn2_tsvd_2000_1000_1/BiasAdd/ReadVariableOp�1model/nmn2_tsvd_2000_1000_1/MatMul/ReadVariableOp�2model/nmn2_tsvd_2000_1000_2/BiasAdd/ReadVariableOp�1model/nmn2_tsvd_2000_1000_2/MatMul/ReadVariableOp�2model/nmn2_tsvd_2000_1000_3/BiasAdd/ReadVariableOp�1model/nmn2_tsvd_2000_1000_3/MatMul/ReadVariableOp�4model/nmn2_tsvd_2000_1000_out/BiasAdd/ReadVariableOp�3model/nmn2_tsvd_2000_1000_out/MatMul/ReadVariableOp�2model/nmn2_umap_2000_1000_1/BiasAdd/ReadVariableOp�1model/nmn2_umap_2000_1000_1/MatMul/ReadVariableOp�2model/nmn2_umap_2000_1000_2/BiasAdd/ReadVariableOp�1model/nmn2_umap_2000_1000_2/MatMul/ReadVariableOp�2model/nmn2_umap_2000_1000_3/BiasAdd/ReadVariableOp�1model/nmn2_umap_2000_1000_3/MatMul/ReadVariableOp�4model/nmn2_umap_2000_1000_out/BiasAdd/ReadVariableOp�3model/nmn2_umap_2000_1000_out/MatMul/ReadVariableOp�
1model/nmn2_tsvd_2000_1000_1/MatMul/ReadVariableOpReadVariableOp:model_nmn2_tsvd_2000_1000_1_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype023
1model/nmn2_tsvd_2000_1000_1/MatMul/ReadVariableOp�
"model/nmn2_tsvd_2000_1000_1/MatMulMatMulnmn2_tsvd_2000_1000_input9model/nmn2_tsvd_2000_1000_1/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2$
"model/nmn2_tsvd_2000_1000_1/MatMul�
2model/nmn2_tsvd_2000_1000_1/BiasAdd/ReadVariableOpReadVariableOp;model_nmn2_tsvd_2000_1000_1_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype024
2model/nmn2_tsvd_2000_1000_1/BiasAdd/ReadVariableOp�
#model/nmn2_tsvd_2000_1000_1/BiasAddBiasAdd,model/nmn2_tsvd_2000_1000_1/MatMul:product:0:model/nmn2_tsvd_2000_1000_1/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2%
#model/nmn2_tsvd_2000_1000_1/BiasAdd�
 model/nmn2_tsvd_2000_1000_1/ReluRelu,model/nmn2_tsvd_2000_1000_1/BiasAdd:output:0*
T0*(
_output_shapes
:����������2"
 model/nmn2_tsvd_2000_1000_1/Relu�
1model/nmn2_umap_2000_1000_1/MatMul/ReadVariableOpReadVariableOp:model_nmn2_umap_2000_1000_1_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype023
1model/nmn2_umap_2000_1000_1/MatMul/ReadVariableOp�
"model/nmn2_umap_2000_1000_1/MatMulMatMulnmn2_umap_2000_1000_input9model/nmn2_umap_2000_1000_1/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2$
"model/nmn2_umap_2000_1000_1/MatMul�
2model/nmn2_umap_2000_1000_1/BiasAdd/ReadVariableOpReadVariableOp;model_nmn2_umap_2000_1000_1_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype024
2model/nmn2_umap_2000_1000_1/BiasAdd/ReadVariableOp�
#model/nmn2_umap_2000_1000_1/BiasAddBiasAdd,model/nmn2_umap_2000_1000_1/MatMul:product:0:model/nmn2_umap_2000_1000_1/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2%
#model/nmn2_umap_2000_1000_1/BiasAdd�
 model/nmn2_umap_2000_1000_1/ReluRelu,model/nmn2_umap_2000_1000_1/BiasAdd:output:0*
T0*(
_output_shapes
:����������2"
 model/nmn2_umap_2000_1000_1/Relu�
0model/nmn2_pca_2000_1000_1/MatMul/ReadVariableOpReadVariableOp9model_nmn2_pca_2000_1000_1_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype022
0model/nmn2_pca_2000_1000_1/MatMul/ReadVariableOp�
!model/nmn2_pca_2000_1000_1/MatMulMatMulnmn2_pca_2000_1000_input8model/nmn2_pca_2000_1000_1/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2#
!model/nmn2_pca_2000_1000_1/MatMul�
1model/nmn2_pca_2000_1000_1/BiasAdd/ReadVariableOpReadVariableOp:model_nmn2_pca_2000_1000_1_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype023
1model/nmn2_pca_2000_1000_1/BiasAdd/ReadVariableOp�
"model/nmn2_pca_2000_1000_1/BiasAddBiasAdd+model/nmn2_pca_2000_1000_1/MatMul:product:09model/nmn2_pca_2000_1000_1/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2$
"model/nmn2_pca_2000_1000_1/BiasAdd�
model/nmn2_pca_2000_1000_1/ReluRelu+model/nmn2_pca_2000_1000_1/BiasAdd:output:0*
T0*(
_output_shapes
:����������2!
model/nmn2_pca_2000_1000_1/Relu�
1model/nmn2_tsvd_2000_1000_2/MatMul/ReadVariableOpReadVariableOp:model_nmn2_tsvd_2000_1000_2_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype023
1model/nmn2_tsvd_2000_1000_2/MatMul/ReadVariableOp�
"model/nmn2_tsvd_2000_1000_2/MatMulMatMul.model/nmn2_tsvd_2000_1000_1/Relu:activations:09model/nmn2_tsvd_2000_1000_2/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2$
"model/nmn2_tsvd_2000_1000_2/MatMul�
2model/nmn2_tsvd_2000_1000_2/BiasAdd/ReadVariableOpReadVariableOp;model_nmn2_tsvd_2000_1000_2_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype024
2model/nmn2_tsvd_2000_1000_2/BiasAdd/ReadVariableOp�
#model/nmn2_tsvd_2000_1000_2/BiasAddBiasAdd,model/nmn2_tsvd_2000_1000_2/MatMul:product:0:model/nmn2_tsvd_2000_1000_2/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2%
#model/nmn2_tsvd_2000_1000_2/BiasAdd�
 model/nmn2_tsvd_2000_1000_2/ReluRelu,model/nmn2_tsvd_2000_1000_2/BiasAdd:output:0*
T0*(
_output_shapes
:����������2"
 model/nmn2_tsvd_2000_1000_2/Relu�
1model/nmn2_umap_2000_1000_2/MatMul/ReadVariableOpReadVariableOp:model_nmn2_umap_2000_1000_2_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype023
1model/nmn2_umap_2000_1000_2/MatMul/ReadVariableOp�
"model/nmn2_umap_2000_1000_2/MatMulMatMul.model/nmn2_umap_2000_1000_1/Relu:activations:09model/nmn2_umap_2000_1000_2/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2$
"model/nmn2_umap_2000_1000_2/MatMul�
2model/nmn2_umap_2000_1000_2/BiasAdd/ReadVariableOpReadVariableOp;model_nmn2_umap_2000_1000_2_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype024
2model/nmn2_umap_2000_1000_2/BiasAdd/ReadVariableOp�
#model/nmn2_umap_2000_1000_2/BiasAddBiasAdd,model/nmn2_umap_2000_1000_2/MatMul:product:0:model/nmn2_umap_2000_1000_2/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2%
#model/nmn2_umap_2000_1000_2/BiasAdd�
 model/nmn2_umap_2000_1000_2/ReluRelu,model/nmn2_umap_2000_1000_2/BiasAdd:output:0*
T0*(
_output_shapes
:����������2"
 model/nmn2_umap_2000_1000_2/Relu�
0model/nmn2_pca_2000_1000_2/MatMul/ReadVariableOpReadVariableOp9model_nmn2_pca_2000_1000_2_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype022
0model/nmn2_pca_2000_1000_2/MatMul/ReadVariableOp�
!model/nmn2_pca_2000_1000_2/MatMulMatMul-model/nmn2_pca_2000_1000_1/Relu:activations:08model/nmn2_pca_2000_1000_2/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2#
!model/nmn2_pca_2000_1000_2/MatMul�
1model/nmn2_pca_2000_1000_2/BiasAdd/ReadVariableOpReadVariableOp:model_nmn2_pca_2000_1000_2_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype023
1model/nmn2_pca_2000_1000_2/BiasAdd/ReadVariableOp�
"model/nmn2_pca_2000_1000_2/BiasAddBiasAdd+model/nmn2_pca_2000_1000_2/MatMul:product:09model/nmn2_pca_2000_1000_2/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2$
"model/nmn2_pca_2000_1000_2/BiasAdd�
model/nmn2_pca_2000_1000_2/ReluRelu+model/nmn2_pca_2000_1000_2/BiasAdd:output:0*
T0*(
_output_shapes
:����������2!
model/nmn2_pca_2000_1000_2/Relu�
1model/nmn2_tsvd_2000_1000_3/MatMul/ReadVariableOpReadVariableOp:model_nmn2_tsvd_2000_1000_3_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype023
1model/nmn2_tsvd_2000_1000_3/MatMul/ReadVariableOp�
"model/nmn2_tsvd_2000_1000_3/MatMulMatMul.model/nmn2_tsvd_2000_1000_2/Relu:activations:09model/nmn2_tsvd_2000_1000_3/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2$
"model/nmn2_tsvd_2000_1000_3/MatMul�
2model/nmn2_tsvd_2000_1000_3/BiasAdd/ReadVariableOpReadVariableOp;model_nmn2_tsvd_2000_1000_3_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype024
2model/nmn2_tsvd_2000_1000_3/BiasAdd/ReadVariableOp�
#model/nmn2_tsvd_2000_1000_3/BiasAddBiasAdd,model/nmn2_tsvd_2000_1000_3/MatMul:product:0:model/nmn2_tsvd_2000_1000_3/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2%
#model/nmn2_tsvd_2000_1000_3/BiasAdd�
 model/nmn2_tsvd_2000_1000_3/ReluRelu,model/nmn2_tsvd_2000_1000_3/BiasAdd:output:0*
T0*(
_output_shapes
:����������2"
 model/nmn2_tsvd_2000_1000_3/Relu�
1model/nmn2_umap_2000_1000_3/MatMul/ReadVariableOpReadVariableOp:model_nmn2_umap_2000_1000_3_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype023
1model/nmn2_umap_2000_1000_3/MatMul/ReadVariableOp�
"model/nmn2_umap_2000_1000_3/MatMulMatMul.model/nmn2_umap_2000_1000_2/Relu:activations:09model/nmn2_umap_2000_1000_3/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2$
"model/nmn2_umap_2000_1000_3/MatMul�
2model/nmn2_umap_2000_1000_3/BiasAdd/ReadVariableOpReadVariableOp;model_nmn2_umap_2000_1000_3_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype024
2model/nmn2_umap_2000_1000_3/BiasAdd/ReadVariableOp�
#model/nmn2_umap_2000_1000_3/BiasAddBiasAdd,model/nmn2_umap_2000_1000_3/MatMul:product:0:model/nmn2_umap_2000_1000_3/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2%
#model/nmn2_umap_2000_1000_3/BiasAdd�
 model/nmn2_umap_2000_1000_3/ReluRelu,model/nmn2_umap_2000_1000_3/BiasAdd:output:0*
T0*(
_output_shapes
:����������2"
 model/nmn2_umap_2000_1000_3/Relu�
0model/nmn2_pca_2000_1000_3/MatMul/ReadVariableOpReadVariableOp9model_nmn2_pca_2000_1000_3_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype022
0model/nmn2_pca_2000_1000_3/MatMul/ReadVariableOp�
!model/nmn2_pca_2000_1000_3/MatMulMatMul-model/nmn2_pca_2000_1000_2/Relu:activations:08model/nmn2_pca_2000_1000_3/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2#
!model/nmn2_pca_2000_1000_3/MatMul�
1model/nmn2_pca_2000_1000_3/BiasAdd/ReadVariableOpReadVariableOp:model_nmn2_pca_2000_1000_3_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype023
1model/nmn2_pca_2000_1000_3/BiasAdd/ReadVariableOp�
"model/nmn2_pca_2000_1000_3/BiasAddBiasAdd+model/nmn2_pca_2000_1000_3/MatMul:product:09model/nmn2_pca_2000_1000_3/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2$
"model/nmn2_pca_2000_1000_3/BiasAdd�
model/nmn2_pca_2000_1000_3/ReluRelu+model/nmn2_pca_2000_1000_3/BiasAdd:output:0*
T0*(
_output_shapes
:����������2!
model/nmn2_pca_2000_1000_3/Relu�
2model/nmn2_pca_2000_1000_out/MatMul/ReadVariableOpReadVariableOp;model_nmn2_pca_2000_1000_out_matmul_readvariableop_resource*
_output_shapes
:	�*
dtype024
2model/nmn2_pca_2000_1000_out/MatMul/ReadVariableOp�
#model/nmn2_pca_2000_1000_out/MatMulMatMul-model/nmn2_pca_2000_1000_3/Relu:activations:0:model/nmn2_pca_2000_1000_out/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2%
#model/nmn2_pca_2000_1000_out/MatMul�
3model/nmn2_pca_2000_1000_out/BiasAdd/ReadVariableOpReadVariableOp<model_nmn2_pca_2000_1000_out_biasadd_readvariableop_resource*
_output_shapes
:*
dtype025
3model/nmn2_pca_2000_1000_out/BiasAdd/ReadVariableOp�
$model/nmn2_pca_2000_1000_out/BiasAddBiasAdd-model/nmn2_pca_2000_1000_out/MatMul:product:0;model/nmn2_pca_2000_1000_out/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2&
$model/nmn2_pca_2000_1000_out/BiasAdd�
$model/nmn2_pca_2000_1000_out/SigmoidSigmoid-model/nmn2_pca_2000_1000_out/BiasAdd:output:0*
T0*'
_output_shapes
:���������2&
$model/nmn2_pca_2000_1000_out/Sigmoid�
3model/nmn2_umap_2000_1000_out/MatMul/ReadVariableOpReadVariableOp<model_nmn2_umap_2000_1000_out_matmul_readvariableop_resource*
_output_shapes
:	�*
dtype025
3model/nmn2_umap_2000_1000_out/MatMul/ReadVariableOp�
$model/nmn2_umap_2000_1000_out/MatMulMatMul.model/nmn2_umap_2000_1000_3/Relu:activations:0;model/nmn2_umap_2000_1000_out/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2&
$model/nmn2_umap_2000_1000_out/MatMul�
4model/nmn2_umap_2000_1000_out/BiasAdd/ReadVariableOpReadVariableOp=model_nmn2_umap_2000_1000_out_biasadd_readvariableop_resource*
_output_shapes
:*
dtype026
4model/nmn2_umap_2000_1000_out/BiasAdd/ReadVariableOp�
%model/nmn2_umap_2000_1000_out/BiasAddBiasAdd.model/nmn2_umap_2000_1000_out/MatMul:product:0<model/nmn2_umap_2000_1000_out/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2'
%model/nmn2_umap_2000_1000_out/BiasAdd�
%model/nmn2_umap_2000_1000_out/SigmoidSigmoid.model/nmn2_umap_2000_1000_out/BiasAdd:output:0*
T0*'
_output_shapes
:���������2'
%model/nmn2_umap_2000_1000_out/Sigmoid�
3model/nmn2_tsvd_2000_1000_out/MatMul/ReadVariableOpReadVariableOp<model_nmn2_tsvd_2000_1000_out_matmul_readvariableop_resource*
_output_shapes
:	�*
dtype025
3model/nmn2_tsvd_2000_1000_out/MatMul/ReadVariableOp�
$model/nmn2_tsvd_2000_1000_out/MatMulMatMul.model/nmn2_tsvd_2000_1000_3/Relu:activations:0;model/nmn2_tsvd_2000_1000_out/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2&
$model/nmn2_tsvd_2000_1000_out/MatMul�
4model/nmn2_tsvd_2000_1000_out/BiasAdd/ReadVariableOpReadVariableOp=model_nmn2_tsvd_2000_1000_out_biasadd_readvariableop_resource*
_output_shapes
:*
dtype026
4model/nmn2_tsvd_2000_1000_out/BiasAdd/ReadVariableOp�
%model/nmn2_tsvd_2000_1000_out/BiasAddBiasAdd.model/nmn2_tsvd_2000_1000_out/MatMul:product:0<model/nmn2_tsvd_2000_1000_out/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2'
%model/nmn2_tsvd_2000_1000_out/BiasAdd�
%model/nmn2_tsvd_2000_1000_out/SigmoidSigmoid.model/nmn2_tsvd_2000_1000_out/BiasAdd:output:0*
T0*'
_output_shapes
:���������2'
%model/nmn2_tsvd_2000_1000_out/Sigmoid�
model/concatenate_1/concat/axisConst*
_output_shapes
: *
dtype0*
value	B :2!
model/concatenate_1/concat/axis�
model/concatenate_1/concatConcatV2(model/nmn2_pca_2000_1000_out/Sigmoid:y:0)model/nmn2_umap_2000_1000_out/Sigmoid:y:0)model/nmn2_tsvd_2000_1000_out/Sigmoid:y:0(model/concatenate_1/concat/axis:output:0*
N*
T0*'
_output_shapes
:���������2
model/concatenate_1/concat�
"model/joined/MatMul/ReadVariableOpReadVariableOp+model_joined_matmul_readvariableop_resource*
_output_shapes

:*
dtype02$
"model/joined/MatMul/ReadVariableOp�
model/joined/MatMulMatMul#model/concatenate_1/concat:output:0*model/joined/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
model/joined/MatMul�
#model/joined/BiasAdd/ReadVariableOpReadVariableOp,model_joined_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02%
#model/joined/BiasAdd/ReadVariableOp�
model/joined/BiasAddBiasAddmodel/joined/MatMul:product:0+model/joined/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
model/joined/BiasAdd�
model/joined/SigmoidSigmoidmodel/joined/BiasAdd:output:0*
T0*'
_output_shapes
:���������2
model/joined/Sigmoids
IdentityIdentitymodel/joined/Sigmoid:y:0^NoOp*
T0*'
_output_shapes
:���������2

Identity�
NoOpNoOp$^model/joined/BiasAdd/ReadVariableOp#^model/joined/MatMul/ReadVariableOp2^model/nmn2_pca_2000_1000_1/BiasAdd/ReadVariableOp1^model/nmn2_pca_2000_1000_1/MatMul/ReadVariableOp2^model/nmn2_pca_2000_1000_2/BiasAdd/ReadVariableOp1^model/nmn2_pca_2000_1000_2/MatMul/ReadVariableOp2^model/nmn2_pca_2000_1000_3/BiasAdd/ReadVariableOp1^model/nmn2_pca_2000_1000_3/MatMul/ReadVariableOp4^model/nmn2_pca_2000_1000_out/BiasAdd/ReadVariableOp3^model/nmn2_pca_2000_1000_out/MatMul/ReadVariableOp3^model/nmn2_tsvd_2000_1000_1/BiasAdd/ReadVariableOp2^model/nmn2_tsvd_2000_1000_1/MatMul/ReadVariableOp3^model/nmn2_tsvd_2000_1000_2/BiasAdd/ReadVariableOp2^model/nmn2_tsvd_2000_1000_2/MatMul/ReadVariableOp3^model/nmn2_tsvd_2000_1000_3/BiasAdd/ReadVariableOp2^model/nmn2_tsvd_2000_1000_3/MatMul/ReadVariableOp5^model/nmn2_tsvd_2000_1000_out/BiasAdd/ReadVariableOp4^model/nmn2_tsvd_2000_1000_out/MatMul/ReadVariableOp3^model/nmn2_umap_2000_1000_1/BiasAdd/ReadVariableOp2^model/nmn2_umap_2000_1000_1/MatMul/ReadVariableOp3^model/nmn2_umap_2000_1000_2/BiasAdd/ReadVariableOp2^model/nmn2_umap_2000_1000_2/MatMul/ReadVariableOp3^model/nmn2_umap_2000_1000_3/BiasAdd/ReadVariableOp2^model/nmn2_umap_2000_1000_3/MatMul/ReadVariableOp5^model/nmn2_umap_2000_1000_out/BiasAdd/ReadVariableOp4^model/nmn2_umap_2000_1000_out/MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*�
_input_shapesr
p:����������:����������:����������: : : : : : : : : : : : : : : : : : : : : : : : : : 2J
#model/joined/BiasAdd/ReadVariableOp#model/joined/BiasAdd/ReadVariableOp2H
"model/joined/MatMul/ReadVariableOp"model/joined/MatMul/ReadVariableOp2f
1model/nmn2_pca_2000_1000_1/BiasAdd/ReadVariableOp1model/nmn2_pca_2000_1000_1/BiasAdd/ReadVariableOp2d
0model/nmn2_pca_2000_1000_1/MatMul/ReadVariableOp0model/nmn2_pca_2000_1000_1/MatMul/ReadVariableOp2f
1model/nmn2_pca_2000_1000_2/BiasAdd/ReadVariableOp1model/nmn2_pca_2000_1000_2/BiasAdd/ReadVariableOp2d
0model/nmn2_pca_2000_1000_2/MatMul/ReadVariableOp0model/nmn2_pca_2000_1000_2/MatMul/ReadVariableOp2f
1model/nmn2_pca_2000_1000_3/BiasAdd/ReadVariableOp1model/nmn2_pca_2000_1000_3/BiasAdd/ReadVariableOp2d
0model/nmn2_pca_2000_1000_3/MatMul/ReadVariableOp0model/nmn2_pca_2000_1000_3/MatMul/ReadVariableOp2j
3model/nmn2_pca_2000_1000_out/BiasAdd/ReadVariableOp3model/nmn2_pca_2000_1000_out/BiasAdd/ReadVariableOp2h
2model/nmn2_pca_2000_1000_out/MatMul/ReadVariableOp2model/nmn2_pca_2000_1000_out/MatMul/ReadVariableOp2h
2model/nmn2_tsvd_2000_1000_1/BiasAdd/ReadVariableOp2model/nmn2_tsvd_2000_1000_1/BiasAdd/ReadVariableOp2f
1model/nmn2_tsvd_2000_1000_1/MatMul/ReadVariableOp1model/nmn2_tsvd_2000_1000_1/MatMul/ReadVariableOp2h
2model/nmn2_tsvd_2000_1000_2/BiasAdd/ReadVariableOp2model/nmn2_tsvd_2000_1000_2/BiasAdd/ReadVariableOp2f
1model/nmn2_tsvd_2000_1000_2/MatMul/ReadVariableOp1model/nmn2_tsvd_2000_1000_2/MatMul/ReadVariableOp2h
2model/nmn2_tsvd_2000_1000_3/BiasAdd/ReadVariableOp2model/nmn2_tsvd_2000_1000_3/BiasAdd/ReadVariableOp2f
1model/nmn2_tsvd_2000_1000_3/MatMul/ReadVariableOp1model/nmn2_tsvd_2000_1000_3/MatMul/ReadVariableOp2l
4model/nmn2_tsvd_2000_1000_out/BiasAdd/ReadVariableOp4model/nmn2_tsvd_2000_1000_out/BiasAdd/ReadVariableOp2j
3model/nmn2_tsvd_2000_1000_out/MatMul/ReadVariableOp3model/nmn2_tsvd_2000_1000_out/MatMul/ReadVariableOp2h
2model/nmn2_umap_2000_1000_1/BiasAdd/ReadVariableOp2model/nmn2_umap_2000_1000_1/BiasAdd/ReadVariableOp2f
1model/nmn2_umap_2000_1000_1/MatMul/ReadVariableOp1model/nmn2_umap_2000_1000_1/MatMul/ReadVariableOp2h
2model/nmn2_umap_2000_1000_2/BiasAdd/ReadVariableOp2model/nmn2_umap_2000_1000_2/BiasAdd/ReadVariableOp2f
1model/nmn2_umap_2000_1000_2/MatMul/ReadVariableOp1model/nmn2_umap_2000_1000_2/MatMul/ReadVariableOp2h
2model/nmn2_umap_2000_1000_3/BiasAdd/ReadVariableOp2model/nmn2_umap_2000_1000_3/BiasAdd/ReadVariableOp2f
1model/nmn2_umap_2000_1000_3/MatMul/ReadVariableOp1model/nmn2_umap_2000_1000_3/MatMul/ReadVariableOp2l
4model/nmn2_umap_2000_1000_out/BiasAdd/ReadVariableOp4model/nmn2_umap_2000_1000_out/BiasAdd/ReadVariableOp2j
3model/nmn2_umap_2000_1000_out/MatMul/ReadVariableOp3model/nmn2_umap_2000_1000_out/MatMul/ReadVariableOp:b ^
(
_output_shapes
:����������
2
_user_specified_namenmn2_pca_2000_1000_input:c_
(
_output_shapes
:����������
3
_user_specified_namenmn2_umap_2000_1000_input:c_
(
_output_shapes
:����������
3
_user_specified_namenmn2_tsvd_2000_1000_input
�
�
P__inference_nmn2_tsvd_2000_1000_2_layer_call_and_return_conditional_losses_42439

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
%__inference_model_layer_call_fn_43051
nmn2_pca_2000_1000_input
nmn2_umap_2000_1000_input
nmn2_tsvd_2000_1000_input
unknown:
��
	unknown_0:	�
	unknown_1:
��
	unknown_2:	�
	unknown_3:
��
	unknown_4:	�
	unknown_5:
��
	unknown_6:	�
	unknown_7:
��
	unknown_8:	�
	unknown_9:
��

unknown_10:	�

unknown_11:
��

unknown_12:	�

unknown_13:
��

unknown_14:	�

unknown_15:
��

unknown_16:	�

unknown_17:	�

unknown_18:

unknown_19:	�

unknown_20:

unknown_21:	�

unknown_22:

unknown_23:

unknown_24:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallnmn2_pca_2000_1000_inputnmn2_umap_2000_1000_inputnmn2_tsvd_2000_1000_inputunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10
unknown_11
unknown_12
unknown_13
unknown_14
unknown_15
unknown_16
unknown_17
unknown_18
unknown_19
unknown_20
unknown_21
unknown_22
unknown_23
unknown_24*(
Tin!
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*<
_read_only_resource_inputs
	
*0
config_proto 

CPU

GPU2*0J 8� *I
fDRB
@__inference_model_layer_call_and_return_conditional_losses_429372
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*�
_input_shapesr
p:����������:����������:����������: : : : : : : : : : : : : : : : : : : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:b ^
(
_output_shapes
:����������
2
_user_specified_namenmn2_pca_2000_1000_input:c_
(
_output_shapes
:����������
3
_user_specified_namenmn2_umap_2000_1000_input:c_
(
_output_shapes
:����������
3
_user_specified_namenmn2_tsvd_2000_1000_input
�
�
%__inference_model_layer_call_fn_43578
inputs_0
inputs_1
inputs_2
unknown:
��
	unknown_0:	�
	unknown_1:
��
	unknown_2:	�
	unknown_3:
��
	unknown_4:	�
	unknown_5:
��
	unknown_6:	�
	unknown_7:
��
	unknown_8:	�
	unknown_9:
��

unknown_10:	�

unknown_11:
��

unknown_12:	�

unknown_13:
��

unknown_14:	�

unknown_15:
��

unknown_16:	�

unknown_17:	�

unknown_18:

unknown_19:	�

unknown_20:

unknown_21:	�

unknown_22:

unknown_23:

unknown_24:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputs_0inputs_1inputs_2unknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10
unknown_11
unknown_12
unknown_13
unknown_14
unknown_15
unknown_16
unknown_17
unknown_18
unknown_19
unknown_20
unknown_21
unknown_22
unknown_23
unknown_24*(
Tin!
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*<
_read_only_resource_inputs
	
*0
config_proto 

CPU

GPU2*0J 8� *I
fDRB
@__inference_model_layer_call_and_return_conditional_losses_429372
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*�
_input_shapesr
p:����������:����������:����������: : : : : : : : : : : : : : : : : : : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:R N
(
_output_shapes
:����������
"
_user_specified_name
inputs/0:RN
(
_output_shapes
:����������
"
_user_specified_name
inputs/1:RN
(
_output_shapes
:����������
"
_user_specified_name
inputs/2
�
�
P__inference_nmn2_tsvd_2000_1000_2_layer_call_and_return_conditional_losses_43689

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
Q__inference_nmn2_pca_2000_1000_out_layer_call_and_return_conditional_losses_43769

inputs1
matmul_readvariableop_resource:	�-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes
:	�*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2	
BiasAdda
SigmoidSigmoidBiasAdd:output:0*
T0*'
_output_shapes
:���������2	
Sigmoidf
IdentityIdentitySigmoid:y:0^NoOp*
T0*'
_output_shapes
:���������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
Q__inference_nmn2_pca_2000_1000_out_layer_call_and_return_conditional_losses_42541

inputs1
matmul_readvariableop_resource:	�-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes
:	�*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2	
BiasAdda
SigmoidSigmoidBiasAdd:output:0*
T0*'
_output_shapes
:���������2	
Sigmoidf
IdentityIdentitySigmoid:y:0^NoOp*
T0*'
_output_shapes
:���������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
O__inference_nmn2_pca_2000_1000_3_layer_call_and_return_conditional_losses_43709

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
g
-__inference_concatenate_1_layer_call_fn_43833
inputs_0
inputs_1
inputs_2
identity�
PartitionedCallPartitionedCallinputs_0inputs_1inputs_2*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *Q
fLRJ
H__inference_concatenate_1_layer_call_and_return_conditional_losses_425892
PartitionedCalll
IdentityIdentityPartitionedCall:output:0*
T0*'
_output_shapes
:���������2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*L
_input_shapes;
9:���������:���������:���������:Q M
'
_output_shapes
:���������
"
_user_specified_name
inputs/0:QM
'
_output_shapes
:���������
"
_user_specified_name
inputs/1:QM
'
_output_shapes
:���������
"
_user_specified_name
inputs/2
�
�
#__inference_signature_wrapper_43262
nmn2_pca_2000_1000_input
nmn2_tsvd_2000_1000_input
nmn2_umap_2000_1000_input
unknown:
��
	unknown_0:	�
	unknown_1:
��
	unknown_2:	�
	unknown_3:
��
	unknown_4:	�
	unknown_5:
��
	unknown_6:	�
	unknown_7:
��
	unknown_8:	�
	unknown_9:
��

unknown_10:	�

unknown_11:
��

unknown_12:	�

unknown_13:
��

unknown_14:	�

unknown_15:
��

unknown_16:	�

unknown_17:	�

unknown_18:

unknown_19:	�

unknown_20:

unknown_21:	�

unknown_22:

unknown_23:

unknown_24:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallnmn2_pca_2000_1000_inputnmn2_umap_2000_1000_inputnmn2_tsvd_2000_1000_inputunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10
unknown_11
unknown_12
unknown_13
unknown_14
unknown_15
unknown_16
unknown_17
unknown_18
unknown_19
unknown_20
unknown_21
unknown_22
unknown_23
unknown_24*(
Tin!
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*<
_read_only_resource_inputs
	
*0
config_proto 

CPU

GPU2*0J 8� *)
f$R"
 __inference__wrapped_model_423662
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*�
_input_shapesr
p:����������:����������:����������: : : : : : : : : : : : : : : : : : : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:b ^
(
_output_shapes
:����������
2
_user_specified_namenmn2_pca_2000_1000_input:c_
(
_output_shapes
:����������
3
_user_specified_namenmn2_tsvd_2000_1000_input:c_
(
_output_shapes
:����������
3
_user_specified_namenmn2_umap_2000_1000_input
�
�
%__inference_model_layer_call_fn_43519
inputs_0
inputs_1
inputs_2
unknown:
��
	unknown_0:	�
	unknown_1:
��
	unknown_2:	�
	unknown_3:
��
	unknown_4:	�
	unknown_5:
��
	unknown_6:	�
	unknown_7:
��
	unknown_8:	�
	unknown_9:
��

unknown_10:	�

unknown_11:
��

unknown_12:	�

unknown_13:
��

unknown_14:	�

unknown_15:
��

unknown_16:	�

unknown_17:	�

unknown_18:

unknown_19:	�

unknown_20:

unknown_21:	�

unknown_22:

unknown_23:

unknown_24:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputs_0inputs_1inputs_2unknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10
unknown_11
unknown_12
unknown_13
unknown_14
unknown_15
unknown_16
unknown_17
unknown_18
unknown_19
unknown_20
unknown_21
unknown_22
unknown_23
unknown_24*(
Tin!
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*<
_read_only_resource_inputs
	
*0
config_proto 

CPU

GPU2*0J 8� *I
fDRB
@__inference_model_layer_call_and_return_conditional_losses_426092
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*�
_input_shapesr
p:����������:����������:����������: : : : : : : : : : : : : : : : : : : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:R N
(
_output_shapes
:����������
"
_user_specified_name
inputs/0:RN
(
_output_shapes
:����������
"
_user_specified_name
inputs/1:RN
(
_output_shapes
:����������
"
_user_specified_name
inputs/2
�
�
7__inference_nmn2_umap_2000_1000_out_layer_call_fn_43798

inputs
unknown:	�
	unknown_0:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_nmn2_umap_2000_1000_out_layer_call_and_return_conditional_losses_425582
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�R
�
__inference__traced_save_43995
file_prefix:
6savev2_nmn2_pca_2000_1000_1_kernel_read_readvariableop8
4savev2_nmn2_pca_2000_1000_1_bias_read_readvariableop;
7savev2_nmn2_umap_2000_1000_1_kernel_read_readvariableop9
5savev2_nmn2_umap_2000_1000_1_bias_read_readvariableop;
7savev2_nmn2_tsvd_2000_1000_1_kernel_read_readvariableop9
5savev2_nmn2_tsvd_2000_1000_1_bias_read_readvariableop:
6savev2_nmn2_pca_2000_1000_2_kernel_read_readvariableop8
4savev2_nmn2_pca_2000_1000_2_bias_read_readvariableop;
7savev2_nmn2_umap_2000_1000_2_kernel_read_readvariableop9
5savev2_nmn2_umap_2000_1000_2_bias_read_readvariableop;
7savev2_nmn2_tsvd_2000_1000_2_kernel_read_readvariableop9
5savev2_nmn2_tsvd_2000_1000_2_bias_read_readvariableop:
6savev2_nmn2_pca_2000_1000_3_kernel_read_readvariableop8
4savev2_nmn2_pca_2000_1000_3_bias_read_readvariableop;
7savev2_nmn2_umap_2000_1000_3_kernel_read_readvariableop9
5savev2_nmn2_umap_2000_1000_3_bias_read_readvariableop;
7savev2_nmn2_tsvd_2000_1000_3_kernel_read_readvariableop9
5savev2_nmn2_tsvd_2000_1000_3_bias_read_readvariableop<
8savev2_nmn2_pca_2000_1000_out_kernel_read_readvariableop:
6savev2_nmn2_pca_2000_1000_out_bias_read_readvariableop=
9savev2_nmn2_umap_2000_1000_out_kernel_read_readvariableop;
7savev2_nmn2_umap_2000_1000_out_bias_read_readvariableop=
9savev2_nmn2_tsvd_2000_1000_out_kernel_read_readvariableop;
7savev2_nmn2_tsvd_2000_1000_out_bias_read_readvariableop,
(savev2_joined_kernel_read_readvariableop*
&savev2_joined_bias_read_readvariableop(
$savev2_adam_iter_read_readvariableop	*
&savev2_adam_beta_1_read_readvariableop*
&savev2_adam_beta_2_read_readvariableop)
%savev2_adam_decay_read_readvariableop1
-savev2_adam_learning_rate_read_readvariableop$
 savev2_total_read_readvariableop$
 savev2_count_read_readvariableop&
"savev2_total_1_read_readvariableop&
"savev2_count_1_read_readvariableop3
/savev2_adam_joined_kernel_m_read_readvariableop1
-savev2_adam_joined_bias_m_read_readvariableop3
/savev2_adam_joined_kernel_v_read_readvariableop1
-savev2_adam_joined_bias_v_read_readvariableop
savev2_const

identity_1��MergeV2Checkpoints�
StaticRegexFullMatchStaticRegexFullMatchfile_prefix"/device:CPU:**
_output_shapes
: *
pattern
^s3://.*2
StaticRegexFullMatchc
ConstConst"/device:CPU:**
_output_shapes
: *
dtype0*
valueB B.part2
Constl
Const_1Const"/device:CPU:**
_output_shapes
: *
dtype0*
valueB B
_temp/part2	
Const_1�
SelectSelectStaticRegexFullMatch:output:0Const:output:0Const_1:output:0"/device:CPU:**
T0*
_output_shapes
: 2
Selectt

StringJoin
StringJoinfile_prefixSelect:output:0"/device:CPU:**
N*
_output_shapes
: 2

StringJoinZ

num_shardsConst*
_output_shapes
: *
dtype0*
value	B :2

num_shards
ShardedFilename/shardConst"/device:CPU:0*
_output_shapes
: *
dtype0*
value	B : 2
ShardedFilename/shard�
ShardedFilenameShardedFilenameStringJoin:output:0ShardedFilename/shard:output:0num_shards:output:0"/device:CPU:0*
_output_shapes
: 2
ShardedFilename�
SaveV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:(*
dtype0*�
value�B�(B6layer_with_weights-0/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-0/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-1/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-1/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-3/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-3/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-4/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-4/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-5/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-5/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-6/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-6/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-7/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-7/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-8/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-8/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-9/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-9/bias/.ATTRIBUTES/VARIABLE_VALUEB7layer_with_weights-10/kernel/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-10/bias/.ATTRIBUTES/VARIABLE_VALUEB7layer_with_weights-11/kernel/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-11/bias/.ATTRIBUTES/VARIABLE_VALUEB7layer_with_weights-12/kernel/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-12/bias/.ATTRIBUTES/VARIABLE_VALUEB)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUEB*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/count/.ATTRIBUTES/VARIABLE_VALUEBSlayer_with_weights-12/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-12/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBSlayer_with_weights-12/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-12/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEB_CHECKPOINTABLE_OBJECT_GRAPH2
SaveV2/tensor_names�
SaveV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:(*
dtype0*c
valueZBX(B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B 2
SaveV2/shape_and_slices�
SaveV2SaveV2ShardedFilename:filename:0SaveV2/tensor_names:output:0 SaveV2/shape_and_slices:output:06savev2_nmn2_pca_2000_1000_1_kernel_read_readvariableop4savev2_nmn2_pca_2000_1000_1_bias_read_readvariableop7savev2_nmn2_umap_2000_1000_1_kernel_read_readvariableop5savev2_nmn2_umap_2000_1000_1_bias_read_readvariableop7savev2_nmn2_tsvd_2000_1000_1_kernel_read_readvariableop5savev2_nmn2_tsvd_2000_1000_1_bias_read_readvariableop6savev2_nmn2_pca_2000_1000_2_kernel_read_readvariableop4savev2_nmn2_pca_2000_1000_2_bias_read_readvariableop7savev2_nmn2_umap_2000_1000_2_kernel_read_readvariableop5savev2_nmn2_umap_2000_1000_2_bias_read_readvariableop7savev2_nmn2_tsvd_2000_1000_2_kernel_read_readvariableop5savev2_nmn2_tsvd_2000_1000_2_bias_read_readvariableop6savev2_nmn2_pca_2000_1000_3_kernel_read_readvariableop4savev2_nmn2_pca_2000_1000_3_bias_read_readvariableop7savev2_nmn2_umap_2000_1000_3_kernel_read_readvariableop5savev2_nmn2_umap_2000_1000_3_bias_read_readvariableop7savev2_nmn2_tsvd_2000_1000_3_kernel_read_readvariableop5savev2_nmn2_tsvd_2000_1000_3_bias_read_readvariableop8savev2_nmn2_pca_2000_1000_out_kernel_read_readvariableop6savev2_nmn2_pca_2000_1000_out_bias_read_readvariableop9savev2_nmn2_umap_2000_1000_out_kernel_read_readvariableop7savev2_nmn2_umap_2000_1000_out_bias_read_readvariableop9savev2_nmn2_tsvd_2000_1000_out_kernel_read_readvariableop7savev2_nmn2_tsvd_2000_1000_out_bias_read_readvariableop(savev2_joined_kernel_read_readvariableop&savev2_joined_bias_read_readvariableop$savev2_adam_iter_read_readvariableop&savev2_adam_beta_1_read_readvariableop&savev2_adam_beta_2_read_readvariableop%savev2_adam_decay_read_readvariableop-savev2_adam_learning_rate_read_readvariableop savev2_total_read_readvariableop savev2_count_read_readvariableop"savev2_total_1_read_readvariableop"savev2_count_1_read_readvariableop/savev2_adam_joined_kernel_m_read_readvariableop-savev2_adam_joined_bias_m_read_readvariableop/savev2_adam_joined_kernel_v_read_readvariableop-savev2_adam_joined_bias_v_read_readvariableopsavev2_const"/device:CPU:0*
_output_shapes
 *6
dtypes,
*2(	2
SaveV2�
&MergeV2Checkpoints/checkpoint_prefixesPackShardedFilename:filename:0^SaveV2"/device:CPU:0*
N*
T0*
_output_shapes
:2(
&MergeV2Checkpoints/checkpoint_prefixes�
MergeV2CheckpointsMergeV2Checkpoints/MergeV2Checkpoints/checkpoint_prefixes:output:0file_prefix"/device:CPU:0*
_output_shapes
 2
MergeV2Checkpointsr
IdentityIdentityfile_prefix^MergeV2Checkpoints"/device:CPU:0*
T0*
_output_shapes
: 2

Identity_

Identity_1IdentityIdentity:output:0^NoOp*
T0*
_output_shapes
: 2

Identity_1c
NoOpNoOp^MergeV2Checkpoints*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"!

identity_1Identity_1:output:0*�
_input_shapes�
�: :
��:�:
��:�:
��:�:
��:�:
��:�:
��:�:
��:�:
��:�:
��:�:	�::	�::	�:::: : : : : : : : : ::::: 2(
MergeV2CheckpointsMergeV2Checkpoints:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix:&"
 
_output_shapes
:
��:!

_output_shapes	
:�:&"
 
_output_shapes
:
��:!

_output_shapes	
:�:&"
 
_output_shapes
:
��:!

_output_shapes	
:�:&"
 
_output_shapes
:
��:!

_output_shapes	
:�:&	"
 
_output_shapes
:
��:!


_output_shapes	
:�:&"
 
_output_shapes
:
��:!

_output_shapes	
:�:&"
 
_output_shapes
:
��:!

_output_shapes	
:�:&"
 
_output_shapes
:
��:!

_output_shapes	
:�:&"
 
_output_shapes
:
��:!

_output_shapes	
:�:%!

_output_shapes
:	�: 

_output_shapes
::%!

_output_shapes
:	�: 

_output_shapes
::%!

_output_shapes
:	�: 

_output_shapes
::$ 

_output_shapes

:: 

_output_shapes
::

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: : 

_output_shapes
: :!

_output_shapes
: :"

_output_shapes
: :#

_output_shapes
: :$$ 

_output_shapes

:: %

_output_shapes
::$& 

_output_shapes

:: '

_output_shapes
::(

_output_shapes
: 
�
�
%__inference_model_layer_call_fn_42664
nmn2_pca_2000_1000_input
nmn2_umap_2000_1000_input
nmn2_tsvd_2000_1000_input
unknown:
��
	unknown_0:	�
	unknown_1:
��
	unknown_2:	�
	unknown_3:
��
	unknown_4:	�
	unknown_5:
��
	unknown_6:	�
	unknown_7:
��
	unknown_8:	�
	unknown_9:
��

unknown_10:	�

unknown_11:
��

unknown_12:	�

unknown_13:
��

unknown_14:	�

unknown_15:
��

unknown_16:	�

unknown_17:	�

unknown_18:

unknown_19:	�

unknown_20:

unknown_21:	�

unknown_22:

unknown_23:

unknown_24:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallnmn2_pca_2000_1000_inputnmn2_umap_2000_1000_inputnmn2_tsvd_2000_1000_inputunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10
unknown_11
unknown_12
unknown_13
unknown_14
unknown_15
unknown_16
unknown_17
unknown_18
unknown_19
unknown_20
unknown_21
unknown_22
unknown_23
unknown_24*(
Tin!
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*<
_read_only_resource_inputs
	
*0
config_proto 

CPU

GPU2*0J 8� *I
fDRB
@__inference_model_layer_call_and_return_conditional_losses_426092
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*�
_input_shapesr
p:����������:����������:����������: : : : : : : : : : : : : : : : : : : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:b ^
(
_output_shapes
:����������
2
_user_specified_namenmn2_pca_2000_1000_input:c_
(
_output_shapes
:����������
3
_user_specified_namenmn2_umap_2000_1000_input:c_
(
_output_shapes
:����������
3
_user_specified_namenmn2_tsvd_2000_1000_input
�
�
O__inference_nmn2_pca_2000_1000_3_layer_call_and_return_conditional_losses_42524

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
H__inference_concatenate_1_layer_call_and_return_conditional_losses_42589

inputs
inputs_1
inputs_2
identity\
concat/axisConst*
_output_shapes
: *
dtype0*
value	B :2
concat/axis�
concatConcatV2inputsinputs_1inputs_2concat/axis:output:0*
N*
T0*'
_output_shapes
:���������2
concatc
IdentityIdentityconcat:output:0*
T0*'
_output_shapes
:���������2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*L
_input_shapes;
9:���������:���������:���������:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs:OK
'
_output_shapes
:���������
 
_user_specified_nameinputs:OK
'
_output_shapes
:���������
 
_user_specified_nameinputs
�
�
P__inference_nmn2_umap_2000_1000_1_layer_call_and_return_conditional_losses_42405

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
H__inference_concatenate_1_layer_call_and_return_conditional_losses_43826
inputs_0
inputs_1
inputs_2
identity\
concat/axisConst*
_output_shapes
: *
dtype0*
value	B :2
concat/axis�
concatConcatV2inputs_0inputs_1inputs_2concat/axis:output:0*
N*
T0*'
_output_shapes
:���������2
concatc
IdentityIdentityconcat:output:0*
T0*'
_output_shapes
:���������2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*L
_input_shapes;
9:���������:���������:���������:Q M
'
_output_shapes
:���������
"
_user_specified_name
inputs/0:QM
'
_output_shapes
:���������
"
_user_specified_name
inputs/1:QM
'
_output_shapes
:���������
"
_user_specified_name
inputs/2
�
�
5__inference_nmn2_umap_2000_1000_1_layer_call_fn_43618

inputs
unknown:
��
	unknown_0:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Y
fTRR
P__inference_nmn2_umap_2000_1000_1_layer_call_and_return_conditional_losses_424052
StatefulPartitionedCall|
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:����������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
5__inference_nmn2_umap_2000_1000_2_layer_call_fn_43678

inputs
unknown:
��
	unknown_0:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Y
fTRR
P__inference_nmn2_umap_2000_1000_2_layer_call_and_return_conditional_losses_424562
StatefulPartitionedCall|
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:����������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�Y
�
@__inference_model_layer_call_and_return_conditional_losses_42609

inputs
inputs_1
inputs_2/
nmn2_tsvd_2000_1000_1_42389:
��*
nmn2_tsvd_2000_1000_1_42391:	�/
nmn2_umap_2000_1000_1_42406:
��*
nmn2_umap_2000_1000_1_42408:	�.
nmn2_pca_2000_1000_1_42423:
��)
nmn2_pca_2000_1000_1_42425:	�/
nmn2_tsvd_2000_1000_2_42440:
��*
nmn2_tsvd_2000_1000_2_42442:	�/
nmn2_umap_2000_1000_2_42457:
��*
nmn2_umap_2000_1000_2_42459:	�.
nmn2_pca_2000_1000_2_42474:
��)
nmn2_pca_2000_1000_2_42476:	�/
nmn2_tsvd_2000_1000_3_42491:
��*
nmn2_tsvd_2000_1000_3_42493:	�/
nmn2_umap_2000_1000_3_42508:
��*
nmn2_umap_2000_1000_3_42510:	�.
nmn2_pca_2000_1000_3_42525:
��)
nmn2_pca_2000_1000_3_42527:	�/
nmn2_pca_2000_1000_out_42542:	�*
nmn2_pca_2000_1000_out_42544:0
nmn2_umap_2000_1000_out_42559:	�+
nmn2_umap_2000_1000_out_42561:0
nmn2_tsvd_2000_1000_out_42576:	�+
nmn2_tsvd_2000_1000_out_42578:
joined_42603:
joined_42605:
identity��joined/StatefulPartitionedCall�,nmn2_pca_2000_1000_1/StatefulPartitionedCall�,nmn2_pca_2000_1000_2/StatefulPartitionedCall�,nmn2_pca_2000_1000_3/StatefulPartitionedCall�.nmn2_pca_2000_1000_out/StatefulPartitionedCall�-nmn2_tsvd_2000_1000_1/StatefulPartitionedCall�-nmn2_tsvd_2000_1000_2/StatefulPartitionedCall�-nmn2_tsvd_2000_1000_3/StatefulPartitionedCall�/nmn2_tsvd_2000_1000_out/StatefulPartitionedCall�-nmn2_umap_2000_1000_1/StatefulPartitionedCall�-nmn2_umap_2000_1000_2/StatefulPartitionedCall�-nmn2_umap_2000_1000_3/StatefulPartitionedCall�/nmn2_umap_2000_1000_out/StatefulPartitionedCall�
-nmn2_tsvd_2000_1000_1/StatefulPartitionedCallStatefulPartitionedCallinputs_2nmn2_tsvd_2000_1000_1_42389nmn2_tsvd_2000_1000_1_42391*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Y
fTRR
P__inference_nmn2_tsvd_2000_1000_1_layer_call_and_return_conditional_losses_423882/
-nmn2_tsvd_2000_1000_1/StatefulPartitionedCall�
-nmn2_umap_2000_1000_1/StatefulPartitionedCallStatefulPartitionedCallinputs_1nmn2_umap_2000_1000_1_42406nmn2_umap_2000_1000_1_42408*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Y
fTRR
P__inference_nmn2_umap_2000_1000_1_layer_call_and_return_conditional_losses_424052/
-nmn2_umap_2000_1000_1/StatefulPartitionedCall�
,nmn2_pca_2000_1000_1/StatefulPartitionedCallStatefulPartitionedCallinputsnmn2_pca_2000_1000_1_42423nmn2_pca_2000_1000_1_42425*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *X
fSRQ
O__inference_nmn2_pca_2000_1000_1_layer_call_and_return_conditional_losses_424222.
,nmn2_pca_2000_1000_1/StatefulPartitionedCall�
-nmn2_tsvd_2000_1000_2/StatefulPartitionedCallStatefulPartitionedCall6nmn2_tsvd_2000_1000_1/StatefulPartitionedCall:output:0nmn2_tsvd_2000_1000_2_42440nmn2_tsvd_2000_1000_2_42442*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Y
fTRR
P__inference_nmn2_tsvd_2000_1000_2_layer_call_and_return_conditional_losses_424392/
-nmn2_tsvd_2000_1000_2/StatefulPartitionedCall�
-nmn2_umap_2000_1000_2/StatefulPartitionedCallStatefulPartitionedCall6nmn2_umap_2000_1000_1/StatefulPartitionedCall:output:0nmn2_umap_2000_1000_2_42457nmn2_umap_2000_1000_2_42459*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Y
fTRR
P__inference_nmn2_umap_2000_1000_2_layer_call_and_return_conditional_losses_424562/
-nmn2_umap_2000_1000_2/StatefulPartitionedCall�
,nmn2_pca_2000_1000_2/StatefulPartitionedCallStatefulPartitionedCall5nmn2_pca_2000_1000_1/StatefulPartitionedCall:output:0nmn2_pca_2000_1000_2_42474nmn2_pca_2000_1000_2_42476*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *X
fSRQ
O__inference_nmn2_pca_2000_1000_2_layer_call_and_return_conditional_losses_424732.
,nmn2_pca_2000_1000_2/StatefulPartitionedCall�
-nmn2_tsvd_2000_1000_3/StatefulPartitionedCallStatefulPartitionedCall6nmn2_tsvd_2000_1000_2/StatefulPartitionedCall:output:0nmn2_tsvd_2000_1000_3_42491nmn2_tsvd_2000_1000_3_42493*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Y
fTRR
P__inference_nmn2_tsvd_2000_1000_3_layer_call_and_return_conditional_losses_424902/
-nmn2_tsvd_2000_1000_3/StatefulPartitionedCall�
-nmn2_umap_2000_1000_3/StatefulPartitionedCallStatefulPartitionedCall6nmn2_umap_2000_1000_2/StatefulPartitionedCall:output:0nmn2_umap_2000_1000_3_42508nmn2_umap_2000_1000_3_42510*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Y
fTRR
P__inference_nmn2_umap_2000_1000_3_layer_call_and_return_conditional_losses_425072/
-nmn2_umap_2000_1000_3/StatefulPartitionedCall�
,nmn2_pca_2000_1000_3/StatefulPartitionedCallStatefulPartitionedCall5nmn2_pca_2000_1000_2/StatefulPartitionedCall:output:0nmn2_pca_2000_1000_3_42525nmn2_pca_2000_1000_3_42527*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *X
fSRQ
O__inference_nmn2_pca_2000_1000_3_layer_call_and_return_conditional_losses_425242.
,nmn2_pca_2000_1000_3/StatefulPartitionedCall�
.nmn2_pca_2000_1000_out/StatefulPartitionedCallStatefulPartitionedCall5nmn2_pca_2000_1000_3/StatefulPartitionedCall:output:0nmn2_pca_2000_1000_out_42542nmn2_pca_2000_1000_out_42544*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_nmn2_pca_2000_1000_out_layer_call_and_return_conditional_losses_4254120
.nmn2_pca_2000_1000_out/StatefulPartitionedCall�
/nmn2_umap_2000_1000_out/StatefulPartitionedCallStatefulPartitionedCall6nmn2_umap_2000_1000_3/StatefulPartitionedCall:output:0nmn2_umap_2000_1000_out_42559nmn2_umap_2000_1000_out_42561*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_nmn2_umap_2000_1000_out_layer_call_and_return_conditional_losses_4255821
/nmn2_umap_2000_1000_out/StatefulPartitionedCall�
/nmn2_tsvd_2000_1000_out/StatefulPartitionedCallStatefulPartitionedCall6nmn2_tsvd_2000_1000_3/StatefulPartitionedCall:output:0nmn2_tsvd_2000_1000_out_42576nmn2_tsvd_2000_1000_out_42578*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_nmn2_tsvd_2000_1000_out_layer_call_and_return_conditional_losses_4257521
/nmn2_tsvd_2000_1000_out/StatefulPartitionedCall�
concatenate_1/PartitionedCallPartitionedCall7nmn2_pca_2000_1000_out/StatefulPartitionedCall:output:08nmn2_umap_2000_1000_out/StatefulPartitionedCall:output:08nmn2_tsvd_2000_1000_out/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *Q
fLRJ
H__inference_concatenate_1_layer_call_and_return_conditional_losses_425892
concatenate_1/PartitionedCall�
joined/StatefulPartitionedCallStatefulPartitionedCall&concatenate_1/PartitionedCall:output:0joined_42603joined_42605*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *J
fERC
A__inference_joined_layer_call_and_return_conditional_losses_426022 
joined/StatefulPartitionedCall�
IdentityIdentity'joined/StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identity�
NoOpNoOp^joined/StatefulPartitionedCall-^nmn2_pca_2000_1000_1/StatefulPartitionedCall-^nmn2_pca_2000_1000_2/StatefulPartitionedCall-^nmn2_pca_2000_1000_3/StatefulPartitionedCall/^nmn2_pca_2000_1000_out/StatefulPartitionedCall.^nmn2_tsvd_2000_1000_1/StatefulPartitionedCall.^nmn2_tsvd_2000_1000_2/StatefulPartitionedCall.^nmn2_tsvd_2000_1000_3/StatefulPartitionedCall0^nmn2_tsvd_2000_1000_out/StatefulPartitionedCall.^nmn2_umap_2000_1000_1/StatefulPartitionedCall.^nmn2_umap_2000_1000_2/StatefulPartitionedCall.^nmn2_umap_2000_1000_3/StatefulPartitionedCall0^nmn2_umap_2000_1000_out/StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*�
_input_shapesr
p:����������:����������:����������: : : : : : : : : : : : : : : : : : : : : : : : : : 2@
joined/StatefulPartitionedCalljoined/StatefulPartitionedCall2\
,nmn2_pca_2000_1000_1/StatefulPartitionedCall,nmn2_pca_2000_1000_1/StatefulPartitionedCall2\
,nmn2_pca_2000_1000_2/StatefulPartitionedCall,nmn2_pca_2000_1000_2/StatefulPartitionedCall2\
,nmn2_pca_2000_1000_3/StatefulPartitionedCall,nmn2_pca_2000_1000_3/StatefulPartitionedCall2`
.nmn2_pca_2000_1000_out/StatefulPartitionedCall.nmn2_pca_2000_1000_out/StatefulPartitionedCall2^
-nmn2_tsvd_2000_1000_1/StatefulPartitionedCall-nmn2_tsvd_2000_1000_1/StatefulPartitionedCall2^
-nmn2_tsvd_2000_1000_2/StatefulPartitionedCall-nmn2_tsvd_2000_1000_2/StatefulPartitionedCall2^
-nmn2_tsvd_2000_1000_3/StatefulPartitionedCall-nmn2_tsvd_2000_1000_3/StatefulPartitionedCall2b
/nmn2_tsvd_2000_1000_out/StatefulPartitionedCall/nmn2_tsvd_2000_1000_out/StatefulPartitionedCall2^
-nmn2_umap_2000_1000_1/StatefulPartitionedCall-nmn2_umap_2000_1000_1/StatefulPartitionedCall2^
-nmn2_umap_2000_1000_2/StatefulPartitionedCall-nmn2_umap_2000_1000_2/StatefulPartitionedCall2^
-nmn2_umap_2000_1000_3/StatefulPartitionedCall-nmn2_umap_2000_1000_3/StatefulPartitionedCall2b
/nmn2_umap_2000_1000_out/StatefulPartitionedCall/nmn2_umap_2000_1000_out/StatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs:PL
(
_output_shapes
:����������
 
_user_specified_nameinputs:PL
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
P__inference_nmn2_tsvd_2000_1000_3_layer_call_and_return_conditional_losses_42490

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs"�L
saver_filename:0StatefulPartitionedCall_1:0StatefulPartitionedCall_28"
saved_model_main_op

NoOp*>
__saved_model_init_op%#
__saved_model_init_op

NoOp*�
serving_default�
^
nmn2_pca_2000_1000_inputB
*serving_default_nmn2_pca_2000_1000_input:0����������
`
nmn2_tsvd_2000_1000_inputC
+serving_default_nmn2_tsvd_2000_1000_input:0����������
`
nmn2_umap_2000_1000_inputC
+serving_default_nmn2_umap_2000_1000_input:0����������:
joined0
StatefulPartitionedCall:0���������tensorflow/serving/predict:��
�
layer-0
layer-1
layer-2
layer_with_weights-0
layer-3
layer_with_weights-1
layer-4
layer_with_weights-2
layer-5
layer_with_weights-3
layer-6
layer_with_weights-4
layer-7
	layer_with_weights-5
	layer-8

layer_with_weights-6

layer-9
layer_with_weights-7
layer-10
layer_with_weights-8
layer-11
layer_with_weights-9
layer-12
layer_with_weights-10
layer-13
layer_with_weights-11
layer-14
layer-15
layer_with_weights-12
layer-16
	optimizer
	variables
trainable_variables
regularization_losses
	keras_api

signatures
+�&call_and_return_all_conditional_losses
�_default_save_signature
�__call__"
_tf_keras_network
D
#_self_saveable_object_factories"
_tf_keras_input_layer
D
#_self_saveable_object_factories"
_tf_keras_input_layer
D
#_self_saveable_object_factories"
_tf_keras_input_layer
�

kernel
bias
#_self_saveable_object_factories
	variables
trainable_variables
 regularization_losses
!	keras_api
+�&call_and_return_all_conditional_losses
�__call__"
_tf_keras_layer
�

"kernel
#bias
#$_self_saveable_object_factories
%	variables
&trainable_variables
'regularization_losses
(	keras_api
+�&call_and_return_all_conditional_losses
�__call__"
_tf_keras_layer
�

)kernel
*bias
#+_self_saveable_object_factories
,	variables
-trainable_variables
.regularization_losses
/	keras_api
+�&call_and_return_all_conditional_losses
�__call__"
_tf_keras_layer
�

0kernel
1bias
#2_self_saveable_object_factories
3	variables
4trainable_variables
5regularization_losses
6	keras_api
+�&call_and_return_all_conditional_losses
�__call__"
_tf_keras_layer
�

7kernel
8bias
#9_self_saveable_object_factories
:	variables
;trainable_variables
<regularization_losses
=	keras_api
+�&call_and_return_all_conditional_losses
�__call__"
_tf_keras_layer
�

>kernel
?bias
#@_self_saveable_object_factories
A	variables
Btrainable_variables
Cregularization_losses
D	keras_api
+�&call_and_return_all_conditional_losses
�__call__"
_tf_keras_layer
�

Ekernel
Fbias
#G_self_saveable_object_factories
H	variables
Itrainable_variables
Jregularization_losses
K	keras_api
+�&call_and_return_all_conditional_losses
�__call__"
_tf_keras_layer
�

Lkernel
Mbias
#N_self_saveable_object_factories
O	variables
Ptrainable_variables
Qregularization_losses
R	keras_api
+�&call_and_return_all_conditional_losses
�__call__"
_tf_keras_layer
�

Skernel
Tbias
#U_self_saveable_object_factories
V	variables
Wtrainable_variables
Xregularization_losses
Y	keras_api
+�&call_and_return_all_conditional_losses
�__call__"
_tf_keras_layer
�

Zkernel
[bias
#\_self_saveable_object_factories
]	variables
^trainable_variables
_regularization_losses
`	keras_api
+�&call_and_return_all_conditional_losses
�__call__"
_tf_keras_layer
�

akernel
bbias
#c_self_saveable_object_factories
d	variables
etrainable_variables
fregularization_losses
g	keras_api
+�&call_and_return_all_conditional_losses
�__call__"
_tf_keras_layer
�

hkernel
ibias
#j_self_saveable_object_factories
k	variables
ltrainable_variables
mregularization_losses
n	keras_api
+�&call_and_return_all_conditional_losses
�__call__"
_tf_keras_layer
�
o	variables
ptrainable_variables
qregularization_losses
r	keras_api
+�&call_and_return_all_conditional_losses
�__call__"
_tf_keras_layer
�

skernel
tbias
u	variables
vtrainable_variables
wregularization_losses
x	keras_api
+�&call_and_return_all_conditional_losses
�__call__"
_tf_keras_layer
{
yiter

zbeta_1

{beta_2
	|decay
}learning_ratesm�tm�sv�tv�"
	optimizer
�
0
1
"2
#3
)4
*5
06
17
78
89
>10
?11
E12
F13
L14
M15
S16
T17
Z18
[19
a20
b21
h22
i23
s24
t25"
trackable_list_wrapper
.
s0
t1"
trackable_list_wrapper
 "
trackable_list_wrapper
�
~layer_regularization_losses
metrics
�layer_metrics
	variables
�layers
trainable_variables
�non_trainable_variables
regularization_losses
�__call__
�_default_save_signature
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
-
�serving_default"
signature_map
 "
trackable_dict_wrapper
 "
trackable_dict_wrapper
 "
trackable_dict_wrapper
/:-
��2nmn2_pca_2000_1000_1/kernel
(:&�2nmn2_pca_2000_1000_1/bias
 "
trackable_dict_wrapper
.
0
1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
 �layer_regularization_losses
�metrics
�layer_metrics
	variables
�layers
trainable_variables
�non_trainable_variables
 regularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
0:.
��2nmn2_umap_2000_1000_1/kernel
):'�2nmn2_umap_2000_1000_1/bias
 "
trackable_dict_wrapper
.
"0
#1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
 �layer_regularization_losses
�metrics
�layer_metrics
%	variables
�layers
&trainable_variables
�non_trainable_variables
'regularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
0:.
��2nmn2_tsvd_2000_1000_1/kernel
):'�2nmn2_tsvd_2000_1000_1/bias
 "
trackable_dict_wrapper
.
)0
*1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
 �layer_regularization_losses
�metrics
�layer_metrics
,	variables
�layers
-trainable_variables
�non_trainable_variables
.regularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
/:-
��2nmn2_pca_2000_1000_2/kernel
(:&�2nmn2_pca_2000_1000_2/bias
 "
trackable_dict_wrapper
.
00
11"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
 �layer_regularization_losses
�metrics
�layer_metrics
3	variables
�layers
4trainable_variables
�non_trainable_variables
5regularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
0:.
��2nmn2_umap_2000_1000_2/kernel
):'�2nmn2_umap_2000_1000_2/bias
 "
trackable_dict_wrapper
.
70
81"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
 �layer_regularization_losses
�metrics
�layer_metrics
:	variables
�layers
;trainable_variables
�non_trainable_variables
<regularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
0:.
��2nmn2_tsvd_2000_1000_2/kernel
):'�2nmn2_tsvd_2000_1000_2/bias
 "
trackable_dict_wrapper
.
>0
?1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
 �layer_regularization_losses
�metrics
�layer_metrics
A	variables
�layers
Btrainable_variables
�non_trainable_variables
Cregularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
/:-
��2nmn2_pca_2000_1000_3/kernel
(:&�2nmn2_pca_2000_1000_3/bias
 "
trackable_dict_wrapper
.
E0
F1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
 �layer_regularization_losses
�metrics
�layer_metrics
H	variables
�layers
Itrainable_variables
�non_trainable_variables
Jregularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
0:.
��2nmn2_umap_2000_1000_3/kernel
):'�2nmn2_umap_2000_1000_3/bias
 "
trackable_dict_wrapper
.
L0
M1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
 �layer_regularization_losses
�metrics
�layer_metrics
O	variables
�layers
Ptrainable_variables
�non_trainable_variables
Qregularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
0:.
��2nmn2_tsvd_2000_1000_3/kernel
):'�2nmn2_tsvd_2000_1000_3/bias
 "
trackable_dict_wrapper
.
S0
T1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
 �layer_regularization_losses
�metrics
�layer_metrics
V	variables
�layers
Wtrainable_variables
�non_trainable_variables
Xregularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
0:.	�2nmn2_pca_2000_1000_out/kernel
):'2nmn2_pca_2000_1000_out/bias
 "
trackable_dict_wrapper
.
Z0
[1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
 �layer_regularization_losses
�metrics
�layer_metrics
]	variables
�layers
^trainable_variables
�non_trainable_variables
_regularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
1:/	�2nmn2_umap_2000_1000_out/kernel
*:(2nmn2_umap_2000_1000_out/bias
 "
trackable_dict_wrapper
.
a0
b1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
 �layer_regularization_losses
�metrics
�layer_metrics
d	variables
�layers
etrainable_variables
�non_trainable_variables
fregularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
1:/	�2nmn2_tsvd_2000_1000_out/kernel
*:(2nmn2_tsvd_2000_1000_out/bias
 "
trackable_dict_wrapper
.
h0
i1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
 �layer_regularization_losses
�metrics
�layer_metrics
k	variables
�layers
ltrainable_variables
�non_trainable_variables
mregularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
 �layer_regularization_losses
�metrics
�layer_metrics
o	variables
�layers
ptrainable_variables
�non_trainable_variables
qregularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
:2joined/kernel
:2joined/bias
.
s0
t1"
trackable_list_wrapper
.
s0
t1"
trackable_list_wrapper
 "
trackable_list_wrapper
�
 �layer_regularization_losses
�metrics
�layer_metrics
u	variables
�layers
vtrainable_variables
�non_trainable_variables
wregularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
:	 (2	Adam/iter
: (2Adam/beta_1
: (2Adam/beta_2
: (2
Adam/decay
: (2Adam/learning_rate
 "
trackable_list_wrapper
0
�0
�1"
trackable_list_wrapper
 "
trackable_dict_wrapper
�
0
1
2
3
4
5
6
7
	8

9
10
11
12
13
14
15
16"
trackable_list_wrapper
�
0
1
"2
#3
)4
*5
06
17
78
89
>10
?11
E12
F13
L14
M15
S16
T17
Z18
[19
a20
b21
h22
i23"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
.
"0
#1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
.
)0
*1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
.
00
11"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
.
70
81"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
.
>0
?1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
.
E0
F1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
.
L0
M1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
.
S0
T1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
.
Z0
[1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
.
a0
b1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
.
h0
i1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
R

�total

�count
�	variables
�	keras_api"
_tf_keras_metric
c

�total

�count
�
_fn_kwargs
�	variables
�	keras_api"
_tf_keras_metric
:  (2total
:  (2count
0
�0
�1"
trackable_list_wrapper
.
�	variables"
_generic_user_object
:  (2total
:  (2count
 "
trackable_dict_wrapper
0
�0
�1"
trackable_list_wrapper
.
�	variables"
_generic_user_object
$:"2Adam/joined/kernel/m
:2Adam/joined/bias/m
$:"2Adam/joined/kernel/v
:2Adam/joined/bias/v
�2�
@__inference_model_layer_call_and_return_conditional_losses_43361
@__inference_model_layer_call_and_return_conditional_losses_43460
@__inference_model_layer_call_and_return_conditional_losses_43123
@__inference_model_layer_call_and_return_conditional_losses_43195�
���
FullArgSpec1
args)�&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�B�
 __inference__wrapped_model_42366nmn2_pca_2000_1000_inputnmn2_umap_2000_1000_inputnmn2_tsvd_2000_1000_input"�
���
FullArgSpec
args� 
varargsjargs
varkwjkwargs
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
%__inference_model_layer_call_fn_42664
%__inference_model_layer_call_fn_43519
%__inference_model_layer_call_fn_43578
%__inference_model_layer_call_fn_43051�
���
FullArgSpec1
args)�&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2�
O__inference_nmn2_pca_2000_1000_1_layer_call_and_return_conditional_losses_43589�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
4__inference_nmn2_pca_2000_1000_1_layer_call_fn_43598�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
P__inference_nmn2_umap_2000_1000_1_layer_call_and_return_conditional_losses_43609�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
5__inference_nmn2_umap_2000_1000_1_layer_call_fn_43618�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
P__inference_nmn2_tsvd_2000_1000_1_layer_call_and_return_conditional_losses_43629�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
5__inference_nmn2_tsvd_2000_1000_1_layer_call_fn_43638�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
O__inference_nmn2_pca_2000_1000_2_layer_call_and_return_conditional_losses_43649�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
4__inference_nmn2_pca_2000_1000_2_layer_call_fn_43658�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
P__inference_nmn2_umap_2000_1000_2_layer_call_and_return_conditional_losses_43669�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
5__inference_nmn2_umap_2000_1000_2_layer_call_fn_43678�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
P__inference_nmn2_tsvd_2000_1000_2_layer_call_and_return_conditional_losses_43689�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
5__inference_nmn2_tsvd_2000_1000_2_layer_call_fn_43698�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
O__inference_nmn2_pca_2000_1000_3_layer_call_and_return_conditional_losses_43709�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
4__inference_nmn2_pca_2000_1000_3_layer_call_fn_43718�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
P__inference_nmn2_umap_2000_1000_3_layer_call_and_return_conditional_losses_43729�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
5__inference_nmn2_umap_2000_1000_3_layer_call_fn_43738�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
P__inference_nmn2_tsvd_2000_1000_3_layer_call_and_return_conditional_losses_43749�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
5__inference_nmn2_tsvd_2000_1000_3_layer_call_fn_43758�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
Q__inference_nmn2_pca_2000_1000_out_layer_call_and_return_conditional_losses_43769�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
6__inference_nmn2_pca_2000_1000_out_layer_call_fn_43778�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
R__inference_nmn2_umap_2000_1000_out_layer_call_and_return_conditional_losses_43789�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
7__inference_nmn2_umap_2000_1000_out_layer_call_fn_43798�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
R__inference_nmn2_tsvd_2000_1000_out_layer_call_and_return_conditional_losses_43809�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
7__inference_nmn2_tsvd_2000_1000_out_layer_call_fn_43818�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
H__inference_concatenate_1_layer_call_and_return_conditional_losses_43826�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
-__inference_concatenate_1_layer_call_fn_43833�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
A__inference_joined_layer_call_and_return_conditional_losses_43844�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
&__inference_joined_layer_call_fn_43853�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
#__inference_signature_wrapper_43262nmn2_pca_2000_1000_inputnmn2_tsvd_2000_1000_inputnmn2_umap_2000_1000_input"�
���
FullArgSpec
args� 
varargs
 
varkwjkwargs
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 �
 __inference__wrapped_model_42366�)*"#>?7801STLMEFZ[abhist���
���
���
3�0
nmn2_pca_2000_1000_input����������
4�1
nmn2_umap_2000_1000_input����������
4�1
nmn2_tsvd_2000_1000_input����������
� "/�,
*
joined �
joined����������
H__inference_concatenate_1_layer_call_and_return_conditional_losses_43826�~�{
t�q
o�l
"�
inputs/0���������
"�
inputs/1���������
"�
inputs/2���������
� "%�"
�
0���������
� �
-__inference_concatenate_1_layer_call_fn_43833�~�{
t�q
o�l
"�
inputs/0���������
"�
inputs/1���������
"�
inputs/2���������
� "�����������
A__inference_joined_layer_call_and_return_conditional_losses_43844\st/�,
%�"
 �
inputs���������
� "%�"
�
0���������
� y
&__inference_joined_layer_call_fn_43853Ost/�,
%�"
 �
inputs���������
� "�����������
@__inference_model_layer_call_and_return_conditional_losses_43123�)*"#>?7801STLMEFZ[abhist���
���
���
3�0
nmn2_pca_2000_1000_input����������
4�1
nmn2_umap_2000_1000_input����������
4�1
nmn2_tsvd_2000_1000_input����������
p 

 
� "%�"
�
0���������
� �
@__inference_model_layer_call_and_return_conditional_losses_43195�)*"#>?7801STLMEFZ[abhist���
���
���
3�0
nmn2_pca_2000_1000_input����������
4�1
nmn2_umap_2000_1000_input����������
4�1
nmn2_tsvd_2000_1000_input����������
p

 
� "%�"
�
0���������
� �
@__inference_model_layer_call_and_return_conditional_losses_43361�)*"#>?7801STLMEFZ[abhist���
�|
r�o
#� 
inputs/0����������
#� 
inputs/1����������
#� 
inputs/2����������
p 

 
� "%�"
�
0���������
� �
@__inference_model_layer_call_and_return_conditional_losses_43460�)*"#>?7801STLMEFZ[abhist���
�|
r�o
#� 
inputs/0����������
#� 
inputs/1����������
#� 
inputs/2����������
p

 
� "%�"
�
0���������
� �
%__inference_model_layer_call_fn_42664�)*"#>?7801STLMEFZ[abhist���
���
���
3�0
nmn2_pca_2000_1000_input����������
4�1
nmn2_umap_2000_1000_input����������
4�1
nmn2_tsvd_2000_1000_input����������
p 

 
� "�����������
%__inference_model_layer_call_fn_43051�)*"#>?7801STLMEFZ[abhist���
���
���
3�0
nmn2_pca_2000_1000_input����������
4�1
nmn2_umap_2000_1000_input����������
4�1
nmn2_tsvd_2000_1000_input����������
p

 
� "�����������
%__inference_model_layer_call_fn_43519�)*"#>?7801STLMEFZ[abhist���
�|
r�o
#� 
inputs/0����������
#� 
inputs/1����������
#� 
inputs/2����������
p 

 
� "�����������
%__inference_model_layer_call_fn_43578�)*"#>?7801STLMEFZ[abhist���
�|
r�o
#� 
inputs/0����������
#� 
inputs/1����������
#� 
inputs/2����������
p

 
� "�����������
O__inference_nmn2_pca_2000_1000_1_layer_call_and_return_conditional_losses_43589^0�-
&�#
!�
inputs����������
� "&�#
�
0����������
� �
4__inference_nmn2_pca_2000_1000_1_layer_call_fn_43598Q0�-
&�#
!�
inputs����������
� "������������
O__inference_nmn2_pca_2000_1000_2_layer_call_and_return_conditional_losses_43649^010�-
&�#
!�
inputs����������
� "&�#
�
0����������
� �
4__inference_nmn2_pca_2000_1000_2_layer_call_fn_43658Q010�-
&�#
!�
inputs����������
� "������������
O__inference_nmn2_pca_2000_1000_3_layer_call_and_return_conditional_losses_43709^EF0�-
&�#
!�
inputs����������
� "&�#
�
0����������
� �
4__inference_nmn2_pca_2000_1000_3_layer_call_fn_43718QEF0�-
&�#
!�
inputs����������
� "������������
Q__inference_nmn2_pca_2000_1000_out_layer_call_and_return_conditional_losses_43769]Z[0�-
&�#
!�
inputs����������
� "%�"
�
0���������
� �
6__inference_nmn2_pca_2000_1000_out_layer_call_fn_43778PZ[0�-
&�#
!�
inputs����������
� "�����������
P__inference_nmn2_tsvd_2000_1000_1_layer_call_and_return_conditional_losses_43629^)*0�-
&�#
!�
inputs����������
� "&�#
�
0����������
� �
5__inference_nmn2_tsvd_2000_1000_1_layer_call_fn_43638Q)*0�-
&�#
!�
inputs����������
� "������������
P__inference_nmn2_tsvd_2000_1000_2_layer_call_and_return_conditional_losses_43689^>?0�-
&�#
!�
inputs����������
� "&�#
�
0����������
� �
5__inference_nmn2_tsvd_2000_1000_2_layer_call_fn_43698Q>?0�-
&�#
!�
inputs����������
� "������������
P__inference_nmn2_tsvd_2000_1000_3_layer_call_and_return_conditional_losses_43749^ST0�-
&�#
!�
inputs����������
� "&�#
�
0����������
� �
5__inference_nmn2_tsvd_2000_1000_3_layer_call_fn_43758QST0�-
&�#
!�
inputs����������
� "������������
R__inference_nmn2_tsvd_2000_1000_out_layer_call_and_return_conditional_losses_43809]hi0�-
&�#
!�
inputs����������
� "%�"
�
0���������
� �
7__inference_nmn2_tsvd_2000_1000_out_layer_call_fn_43818Phi0�-
&�#
!�
inputs����������
� "�����������
P__inference_nmn2_umap_2000_1000_1_layer_call_and_return_conditional_losses_43609^"#0�-
&�#
!�
inputs����������
� "&�#
�
0����������
� �
5__inference_nmn2_umap_2000_1000_1_layer_call_fn_43618Q"#0�-
&�#
!�
inputs����������
� "������������
P__inference_nmn2_umap_2000_1000_2_layer_call_and_return_conditional_losses_43669^780�-
&�#
!�
inputs����������
� "&�#
�
0����������
� �
5__inference_nmn2_umap_2000_1000_2_layer_call_fn_43678Q780�-
&�#
!�
inputs����������
� "������������
P__inference_nmn2_umap_2000_1000_3_layer_call_and_return_conditional_losses_43729^LM0�-
&�#
!�
inputs����������
� "&�#
�
0����������
� �
5__inference_nmn2_umap_2000_1000_3_layer_call_fn_43738QLM0�-
&�#
!�
inputs����������
� "������������
R__inference_nmn2_umap_2000_1000_out_layer_call_and_return_conditional_losses_43789]ab0�-
&�#
!�
inputs����������
� "%�"
�
0���������
� �
7__inference_nmn2_umap_2000_1000_out_layer_call_fn_43798Pab0�-
&�#
!�
inputs����������
� "�����������
#__inference_signature_wrapper_43262�)*"#>?7801STLMEFZ[abhist���
� 
���
O
nmn2_pca_2000_1000_input3�0
nmn2_pca_2000_1000_input����������
Q
nmn2_tsvd_2000_1000_input4�1
nmn2_tsvd_2000_1000_input����������
Q
nmn2_umap_2000_1000_input4�1
nmn2_umap_2000_1000_input����������"/�,
*
joined �
joined���������