��
��
B
AssignVariableOp
resource
value"dtype"
dtypetype�
~
BiasAdd

value"T	
bias"T
output"T" 
Ttype:
2	"-
data_formatstringNHWC:
NHWCNCHW
h
ConcatV2
values"T*N
axis"Tidx
output"T"
Nint(0"	
Ttype"
Tidxtype0:
2	
8
Const
output"dtype"
valuetensor"
dtypetype
.
Identity

input"T
output"T"	
Ttype
q
MatMul
a"T
b"T
product"T"
transpose_abool( "
transpose_bbool( "
Ttype:

2	
e
MergeV2Checkpoints
checkpoint_prefixes
destination_prefix"
delete_old_dirsbool(�

NoOp
M
Pack
values"T*N
output"T"
Nint(0"	
Ttype"
axisint 
C
Placeholder
output"dtype"
dtypetype"
shapeshape:
@
ReadVariableOp
resource
value"dtype"
dtypetype�
E
Relu
features"T
activations"T"
Ttype:
2	
o
	RestoreV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0�
l
SaveV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0�
?
Select
	condition

t"T
e"T
output"T"	
Ttype
H
ShardedFilename
basename	
shard

num_shards
filename
0
Sigmoid
x"T
y"T"
Ttype:

2
�
StatefulPartitionedCall
args2Tin
output2Tout"
Tin
list(type)("
Tout
list(type)("	
ffunc"
configstring "
config_protostring "
executor_typestring �
@
StaticRegexFullMatch	
input

output
"
patternstring
N

StringJoin
inputs*N

output"
Nint(0"
	separatorstring 
�
VarHandleOp
resource"
	containerstring "
shared_namestring "
dtypetype"
shapeshape"#
allowed_deviceslist(string)
 �"serve*2.6.02unknown8��
�
fmn10_pca_5000_1000_1/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*-
shared_namefmn10_pca_5000_1000_1/kernel
�
0fmn10_pca_5000_1000_1/kernel/Read/ReadVariableOpReadVariableOpfmn10_pca_5000_1000_1/kernel* 
_output_shapes
:
��*
dtype0
�
fmn10_pca_5000_1000_1/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*+
shared_namefmn10_pca_5000_1000_1/bias
�
.fmn10_pca_5000_1000_1/bias/Read/ReadVariableOpReadVariableOpfmn10_pca_5000_1000_1/bias*
_output_shapes	
:�*
dtype0
�
fmn10_tsvd_5000_1000_1/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*.
shared_namefmn10_tsvd_5000_1000_1/kernel
�
1fmn10_tsvd_5000_1000_1/kernel/Read/ReadVariableOpReadVariableOpfmn10_tsvd_5000_1000_1/kernel* 
_output_shapes
:
��*
dtype0
�
fmn10_tsvd_5000_1000_1/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*,
shared_namefmn10_tsvd_5000_1000_1/bias
�
/fmn10_tsvd_5000_1000_1/bias/Read/ReadVariableOpReadVariableOpfmn10_tsvd_5000_1000_1/bias*
_output_shapes	
:�*
dtype0
�
fmn10_isomap_5000_1000_1/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*0
shared_name!fmn10_isomap_5000_1000_1/kernel
�
3fmn10_isomap_5000_1000_1/kernel/Read/ReadVariableOpReadVariableOpfmn10_isomap_5000_1000_1/kernel* 
_output_shapes
:
��*
dtype0
�
fmn10_isomap_5000_1000_1/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*.
shared_namefmn10_isomap_5000_1000_1/bias
�
1fmn10_isomap_5000_1000_1/bias/Read/ReadVariableOpReadVariableOpfmn10_isomap_5000_1000_1/bias*
_output_shapes	
:�*
dtype0
�
fmn10_lle_5000_1000_1/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*-
shared_namefmn10_lle_5000_1000_1/kernel
�
0fmn10_lle_5000_1000_1/kernel/Read/ReadVariableOpReadVariableOpfmn10_lle_5000_1000_1/kernel* 
_output_shapes
:
��*
dtype0
�
fmn10_lle_5000_1000_1/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*+
shared_namefmn10_lle_5000_1000_1/bias
�
.fmn10_lle_5000_1000_1/bias/Read/ReadVariableOpReadVariableOpfmn10_lle_5000_1000_1/bias*
_output_shapes	
:�*
dtype0
�
fmn10_pca_5000_1000_2/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*-
shared_namefmn10_pca_5000_1000_2/kernel
�
0fmn10_pca_5000_1000_2/kernel/Read/ReadVariableOpReadVariableOpfmn10_pca_5000_1000_2/kernel* 
_output_shapes
:
��*
dtype0
�
fmn10_pca_5000_1000_2/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*+
shared_namefmn10_pca_5000_1000_2/bias
�
.fmn10_pca_5000_1000_2/bias/Read/ReadVariableOpReadVariableOpfmn10_pca_5000_1000_2/bias*
_output_shapes	
:�*
dtype0
�
fmn10_tsvd_5000_1000_2/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*.
shared_namefmn10_tsvd_5000_1000_2/kernel
�
1fmn10_tsvd_5000_1000_2/kernel/Read/ReadVariableOpReadVariableOpfmn10_tsvd_5000_1000_2/kernel* 
_output_shapes
:
��*
dtype0
�
fmn10_tsvd_5000_1000_2/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*,
shared_namefmn10_tsvd_5000_1000_2/bias
�
/fmn10_tsvd_5000_1000_2/bias/Read/ReadVariableOpReadVariableOpfmn10_tsvd_5000_1000_2/bias*
_output_shapes	
:�*
dtype0
�
fmn10_isomap_5000_1000_2/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*0
shared_name!fmn10_isomap_5000_1000_2/kernel
�
3fmn10_isomap_5000_1000_2/kernel/Read/ReadVariableOpReadVariableOpfmn10_isomap_5000_1000_2/kernel* 
_output_shapes
:
��*
dtype0
�
fmn10_isomap_5000_1000_2/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*.
shared_namefmn10_isomap_5000_1000_2/bias
�
1fmn10_isomap_5000_1000_2/bias/Read/ReadVariableOpReadVariableOpfmn10_isomap_5000_1000_2/bias*
_output_shapes	
:�*
dtype0
�
fmn10_lle_5000_1000_2/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*-
shared_namefmn10_lle_5000_1000_2/kernel
�
0fmn10_lle_5000_1000_2/kernel/Read/ReadVariableOpReadVariableOpfmn10_lle_5000_1000_2/kernel* 
_output_shapes
:
��*
dtype0
�
fmn10_lle_5000_1000_2/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*+
shared_namefmn10_lle_5000_1000_2/bias
�
.fmn10_lle_5000_1000_2/bias/Read/ReadVariableOpReadVariableOpfmn10_lle_5000_1000_2/bias*
_output_shapes	
:�*
dtype0
�
fmn10_pca_5000_1000_3/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*-
shared_namefmn10_pca_5000_1000_3/kernel
�
0fmn10_pca_5000_1000_3/kernel/Read/ReadVariableOpReadVariableOpfmn10_pca_5000_1000_3/kernel* 
_output_shapes
:
��*
dtype0
�
fmn10_pca_5000_1000_3/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*+
shared_namefmn10_pca_5000_1000_3/bias
�
.fmn10_pca_5000_1000_3/bias/Read/ReadVariableOpReadVariableOpfmn10_pca_5000_1000_3/bias*
_output_shapes	
:�*
dtype0
�
fmn10_tsvd_5000_1000_3/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*.
shared_namefmn10_tsvd_5000_1000_3/kernel
�
1fmn10_tsvd_5000_1000_3/kernel/Read/ReadVariableOpReadVariableOpfmn10_tsvd_5000_1000_3/kernel* 
_output_shapes
:
��*
dtype0
�
fmn10_tsvd_5000_1000_3/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*,
shared_namefmn10_tsvd_5000_1000_3/bias
�
/fmn10_tsvd_5000_1000_3/bias/Read/ReadVariableOpReadVariableOpfmn10_tsvd_5000_1000_3/bias*
_output_shapes	
:�*
dtype0
�
fmn10_isomap_5000_1000_3/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*0
shared_name!fmn10_isomap_5000_1000_3/kernel
�
3fmn10_isomap_5000_1000_3/kernel/Read/ReadVariableOpReadVariableOpfmn10_isomap_5000_1000_3/kernel* 
_output_shapes
:
��*
dtype0
�
fmn10_isomap_5000_1000_3/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*.
shared_namefmn10_isomap_5000_1000_3/bias
�
1fmn10_isomap_5000_1000_3/bias/Read/ReadVariableOpReadVariableOpfmn10_isomap_5000_1000_3/bias*
_output_shapes	
:�*
dtype0
�
fmn10_lle_5000_1000_3/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*-
shared_namefmn10_lle_5000_1000_3/kernel
�
0fmn10_lle_5000_1000_3/kernel/Read/ReadVariableOpReadVariableOpfmn10_lle_5000_1000_3/kernel* 
_output_shapes
:
��*
dtype0
�
fmn10_lle_5000_1000_3/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*+
shared_namefmn10_lle_5000_1000_3/bias
�
.fmn10_lle_5000_1000_3/bias/Read/ReadVariableOpReadVariableOpfmn10_lle_5000_1000_3/bias*
_output_shapes	
:�*
dtype0
�
fmn10_pca_5000_1000_out/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:	�*/
shared_name fmn10_pca_5000_1000_out/kernel
�
2fmn10_pca_5000_1000_out/kernel/Read/ReadVariableOpReadVariableOpfmn10_pca_5000_1000_out/kernel*
_output_shapes
:	�*
dtype0
�
fmn10_pca_5000_1000_out/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:*-
shared_namefmn10_pca_5000_1000_out/bias
�
0fmn10_pca_5000_1000_out/bias/Read/ReadVariableOpReadVariableOpfmn10_pca_5000_1000_out/bias*
_output_shapes
:*
dtype0
�
fmn10_tsvd_5000_1000_out/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:	�*0
shared_name!fmn10_tsvd_5000_1000_out/kernel
�
3fmn10_tsvd_5000_1000_out/kernel/Read/ReadVariableOpReadVariableOpfmn10_tsvd_5000_1000_out/kernel*
_output_shapes
:	�*
dtype0
�
fmn10_tsvd_5000_1000_out/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:*.
shared_namefmn10_tsvd_5000_1000_out/bias
�
1fmn10_tsvd_5000_1000_out/bias/Read/ReadVariableOpReadVariableOpfmn10_tsvd_5000_1000_out/bias*
_output_shapes
:*
dtype0
�
!fmn10_isomap_5000_1000_out/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:	�*2
shared_name#!fmn10_isomap_5000_1000_out/kernel
�
5fmn10_isomap_5000_1000_out/kernel/Read/ReadVariableOpReadVariableOp!fmn10_isomap_5000_1000_out/kernel*
_output_shapes
:	�*
dtype0
�
fmn10_isomap_5000_1000_out/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:*0
shared_name!fmn10_isomap_5000_1000_out/bias
�
3fmn10_isomap_5000_1000_out/bias/Read/ReadVariableOpReadVariableOpfmn10_isomap_5000_1000_out/bias*
_output_shapes
:*
dtype0
�
fmn10_lle_5000_1000_out/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:	�*/
shared_name fmn10_lle_5000_1000_out/kernel
�
2fmn10_lle_5000_1000_out/kernel/Read/ReadVariableOpReadVariableOpfmn10_lle_5000_1000_out/kernel*
_output_shapes
:	�*
dtype0
�
fmn10_lle_5000_1000_out/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:*-
shared_namefmn10_lle_5000_1000_out/bias
�
0fmn10_lle_5000_1000_out/bias/Read/ReadVariableOpReadVariableOpfmn10_lle_5000_1000_out/bias*
_output_shapes
:*
dtype0
v
joined/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:*
shared_namejoined/kernel
o
!joined/kernel/Read/ReadVariableOpReadVariableOpjoined/kernel*
_output_shapes

:*
dtype0
n
joined/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:*
shared_namejoined/bias
g
joined/bias/Read/ReadVariableOpReadVariableOpjoined/bias*
_output_shapes
:*
dtype0
f
	Adam/iterVarHandleOp*
_output_shapes
: *
dtype0	*
shape: *
shared_name	Adam/iter
_
Adam/iter/Read/ReadVariableOpReadVariableOp	Adam/iter*
_output_shapes
: *
dtype0	
j
Adam/beta_1VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nameAdam/beta_1
c
Adam/beta_1/Read/ReadVariableOpReadVariableOpAdam/beta_1*
_output_shapes
: *
dtype0
j
Adam/beta_2VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nameAdam/beta_2
c
Adam/beta_2/Read/ReadVariableOpReadVariableOpAdam/beta_2*
_output_shapes
: *
dtype0
h

Adam/decayVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name
Adam/decay
a
Adam/decay/Read/ReadVariableOpReadVariableOp
Adam/decay*
_output_shapes
: *
dtype0
x
Adam/learning_rateVarHandleOp*
_output_shapes
: *
dtype0*
shape: *#
shared_nameAdam/learning_rate
q
&Adam/learning_rate/Read/ReadVariableOpReadVariableOpAdam/learning_rate*
_output_shapes
: *
dtype0
^
totalVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nametotal
W
total/Read/ReadVariableOpReadVariableOptotal*
_output_shapes
: *
dtype0
^
countVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_namecount
W
count/Read/ReadVariableOpReadVariableOpcount*
_output_shapes
: *
dtype0
b
total_1VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name	total_1
[
total_1/Read/ReadVariableOpReadVariableOptotal_1*
_output_shapes
: *
dtype0
b
count_1VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name	count_1
[
count_1/Read/ReadVariableOpReadVariableOpcount_1*
_output_shapes
: *
dtype0
�
Adam/joined/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape
:*%
shared_nameAdam/joined/kernel/m
}
(Adam/joined/kernel/m/Read/ReadVariableOpReadVariableOpAdam/joined/kernel/m*
_output_shapes

:*
dtype0
|
Adam/joined/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:*#
shared_nameAdam/joined/bias/m
u
&Adam/joined/bias/m/Read/ReadVariableOpReadVariableOpAdam/joined/bias/m*
_output_shapes
:*
dtype0
�
Adam/joined/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape
:*%
shared_nameAdam/joined/kernel/v
}
(Adam/joined/kernel/v/Read/ReadVariableOpReadVariableOpAdam/joined/kernel/v*
_output_shapes

:*
dtype0
|
Adam/joined/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:*#
shared_nameAdam/joined/bias/v
u
&Adam/joined/bias/v/Read/ReadVariableOpReadVariableOpAdam/joined/bias/v*
_output_shapes
:*
dtype0

NoOpNoOp
�i
ConstConst"/device:CPU:0*
_output_shapes
: *
dtype0*�i
value�iB�i B�i
�
layer-0
layer-1
layer-2
layer-3
layer_with_weights-0
layer-4
layer_with_weights-1
layer-5
layer_with_weights-2
layer-6
layer_with_weights-3
layer-7
	layer_with_weights-4
	layer-8

layer_with_weights-5

layer-9
layer_with_weights-6
layer-10
layer_with_weights-7
layer-11
layer_with_weights-8
layer-12
layer_with_weights-9
layer-13
layer_with_weights-10
layer-14
layer_with_weights-11
layer-15
layer_with_weights-12
layer-16
layer_with_weights-13
layer-17
layer_with_weights-14
layer-18
layer_with_weights-15
layer-19
layer-20
layer_with_weights-16
layer-21
	optimizer
	variables
regularization_losses
trainable_variables
	keras_api

signatures
%
#_self_saveable_object_factories
%
#_self_saveable_object_factories
%
#_self_saveable_object_factories
 
�

 kernel
!bias
#"_self_saveable_object_factories
#	variables
$regularization_losses
%trainable_variables
&	keras_api
�

'kernel
(bias
#)_self_saveable_object_factories
*	variables
+regularization_losses
,trainable_variables
-	keras_api
�

.kernel
/bias
#0_self_saveable_object_factories
1	variables
2regularization_losses
3trainable_variables
4	keras_api
h

5kernel
6bias
7	variables
8regularization_losses
9trainable_variables
:	keras_api
�

;kernel
<bias
#=_self_saveable_object_factories
>	variables
?regularization_losses
@trainable_variables
A	keras_api
�

Bkernel
Cbias
#D_self_saveable_object_factories
E	variables
Fregularization_losses
Gtrainable_variables
H	keras_api
�

Ikernel
Jbias
#K_self_saveable_object_factories
L	variables
Mregularization_losses
Ntrainable_variables
O	keras_api
h

Pkernel
Qbias
R	variables
Sregularization_losses
Ttrainable_variables
U	keras_api
�

Vkernel
Wbias
#X_self_saveable_object_factories
Y	variables
Zregularization_losses
[trainable_variables
\	keras_api
�

]kernel
^bias
#__self_saveable_object_factories
`	variables
aregularization_losses
btrainable_variables
c	keras_api
�

dkernel
ebias
#f_self_saveable_object_factories
g	variables
hregularization_losses
itrainable_variables
j	keras_api
h

kkernel
lbias
m	variables
nregularization_losses
otrainable_variables
p	keras_api
�

qkernel
rbias
#s_self_saveable_object_factories
t	variables
uregularization_losses
vtrainable_variables
w	keras_api
�

xkernel
ybias
#z_self_saveable_object_factories
{	variables
|regularization_losses
}trainable_variables
~	keras_api
�

kernel
	�bias
$�_self_saveable_object_factories
�	variables
�regularization_losses
�trainable_variables
�	keras_api
n
�kernel
	�bias
�	variables
�regularization_losses
�trainable_variables
�	keras_api
V
�	variables
�regularization_losses
�trainable_variables
�	keras_api
n
�kernel
	�bias
�	variables
�regularization_losses
�trainable_variables
�	keras_api
q
	�iter
�beta_1
�beta_2

�decay
�learning_rate	�m�	�m�	�v�	�v�
�
 0
!1
'2
(3
.4
/5
56
67
;8
<9
B10
C11
I12
J13
P14
Q15
V16
W17
]18
^19
d20
e21
k22
l23
q24
r25
x26
y27
28
�29
�30
�31
�32
�33
 

�0
�1
�
�metrics
�layers
	variables
�layer_metrics
 �layer_regularization_losses
regularization_losses
trainable_variables
�non_trainable_variables
 
 
 
 
hf
VARIABLE_VALUEfmn10_pca_5000_1000_1/kernel6layer_with_weights-0/kernel/.ATTRIBUTES/VARIABLE_VALUE
db
VARIABLE_VALUEfmn10_pca_5000_1000_1/bias4layer_with_weights-0/bias/.ATTRIBUTES/VARIABLE_VALUE
 

 0
!1
 
 
�
�metrics
�layers
#	variables
$regularization_losses
�layer_metrics
 �layer_regularization_losses
%trainable_variables
�non_trainable_variables
ig
VARIABLE_VALUEfmn10_tsvd_5000_1000_1/kernel6layer_with_weights-1/kernel/.ATTRIBUTES/VARIABLE_VALUE
ec
VARIABLE_VALUEfmn10_tsvd_5000_1000_1/bias4layer_with_weights-1/bias/.ATTRIBUTES/VARIABLE_VALUE
 

'0
(1
 
 
�
�metrics
�layers
*	variables
+regularization_losses
�layer_metrics
 �layer_regularization_losses
,trainable_variables
�non_trainable_variables
ki
VARIABLE_VALUEfmn10_isomap_5000_1000_1/kernel6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUE
ge
VARIABLE_VALUEfmn10_isomap_5000_1000_1/bias4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUE
 

.0
/1
 
 
�
�metrics
�layers
1	variables
2regularization_losses
�layer_metrics
 �layer_regularization_losses
3trainable_variables
�non_trainable_variables
hf
VARIABLE_VALUEfmn10_lle_5000_1000_1/kernel6layer_with_weights-3/kernel/.ATTRIBUTES/VARIABLE_VALUE
db
VARIABLE_VALUEfmn10_lle_5000_1000_1/bias4layer_with_weights-3/bias/.ATTRIBUTES/VARIABLE_VALUE

50
61
 
 
�
�metrics
�layers
7	variables
8regularization_losses
�layer_metrics
 �layer_regularization_losses
9trainable_variables
�non_trainable_variables
hf
VARIABLE_VALUEfmn10_pca_5000_1000_2/kernel6layer_with_weights-4/kernel/.ATTRIBUTES/VARIABLE_VALUE
db
VARIABLE_VALUEfmn10_pca_5000_1000_2/bias4layer_with_weights-4/bias/.ATTRIBUTES/VARIABLE_VALUE
 

;0
<1
 
 
�
�metrics
�layers
>	variables
?regularization_losses
�layer_metrics
 �layer_regularization_losses
@trainable_variables
�non_trainable_variables
ig
VARIABLE_VALUEfmn10_tsvd_5000_1000_2/kernel6layer_with_weights-5/kernel/.ATTRIBUTES/VARIABLE_VALUE
ec
VARIABLE_VALUEfmn10_tsvd_5000_1000_2/bias4layer_with_weights-5/bias/.ATTRIBUTES/VARIABLE_VALUE
 

B0
C1
 
 
�
�metrics
�layers
E	variables
Fregularization_losses
�layer_metrics
 �layer_regularization_losses
Gtrainable_variables
�non_trainable_variables
ki
VARIABLE_VALUEfmn10_isomap_5000_1000_2/kernel6layer_with_weights-6/kernel/.ATTRIBUTES/VARIABLE_VALUE
ge
VARIABLE_VALUEfmn10_isomap_5000_1000_2/bias4layer_with_weights-6/bias/.ATTRIBUTES/VARIABLE_VALUE
 

I0
J1
 
 
�
�metrics
�layers
L	variables
Mregularization_losses
�layer_metrics
 �layer_regularization_losses
Ntrainable_variables
�non_trainable_variables
hf
VARIABLE_VALUEfmn10_lle_5000_1000_2/kernel6layer_with_weights-7/kernel/.ATTRIBUTES/VARIABLE_VALUE
db
VARIABLE_VALUEfmn10_lle_5000_1000_2/bias4layer_with_weights-7/bias/.ATTRIBUTES/VARIABLE_VALUE

P0
Q1
 
 
�
�metrics
�layers
R	variables
Sregularization_losses
�layer_metrics
 �layer_regularization_losses
Ttrainable_variables
�non_trainable_variables
hf
VARIABLE_VALUEfmn10_pca_5000_1000_3/kernel6layer_with_weights-8/kernel/.ATTRIBUTES/VARIABLE_VALUE
db
VARIABLE_VALUEfmn10_pca_5000_1000_3/bias4layer_with_weights-8/bias/.ATTRIBUTES/VARIABLE_VALUE
 

V0
W1
 
 
�
�metrics
�layers
Y	variables
Zregularization_losses
�layer_metrics
 �layer_regularization_losses
[trainable_variables
�non_trainable_variables
ig
VARIABLE_VALUEfmn10_tsvd_5000_1000_3/kernel6layer_with_weights-9/kernel/.ATTRIBUTES/VARIABLE_VALUE
ec
VARIABLE_VALUEfmn10_tsvd_5000_1000_3/bias4layer_with_weights-9/bias/.ATTRIBUTES/VARIABLE_VALUE
 

]0
^1
 
 
�
�metrics
�layers
`	variables
aregularization_losses
�layer_metrics
 �layer_regularization_losses
btrainable_variables
�non_trainable_variables
lj
VARIABLE_VALUEfmn10_isomap_5000_1000_3/kernel7layer_with_weights-10/kernel/.ATTRIBUTES/VARIABLE_VALUE
hf
VARIABLE_VALUEfmn10_isomap_5000_1000_3/bias5layer_with_weights-10/bias/.ATTRIBUTES/VARIABLE_VALUE
 

d0
e1
 
 
�
�metrics
�layers
g	variables
hregularization_losses
�layer_metrics
 �layer_regularization_losses
itrainable_variables
�non_trainable_variables
ig
VARIABLE_VALUEfmn10_lle_5000_1000_3/kernel7layer_with_weights-11/kernel/.ATTRIBUTES/VARIABLE_VALUE
ec
VARIABLE_VALUEfmn10_lle_5000_1000_3/bias5layer_with_weights-11/bias/.ATTRIBUTES/VARIABLE_VALUE

k0
l1
 
 
�
�metrics
�layers
m	variables
nregularization_losses
�layer_metrics
 �layer_regularization_losses
otrainable_variables
�non_trainable_variables
ki
VARIABLE_VALUEfmn10_pca_5000_1000_out/kernel7layer_with_weights-12/kernel/.ATTRIBUTES/VARIABLE_VALUE
ge
VARIABLE_VALUEfmn10_pca_5000_1000_out/bias5layer_with_weights-12/bias/.ATTRIBUTES/VARIABLE_VALUE
 

q0
r1
 
 
�
�metrics
�layers
t	variables
uregularization_losses
�layer_metrics
 �layer_regularization_losses
vtrainable_variables
�non_trainable_variables
lj
VARIABLE_VALUEfmn10_tsvd_5000_1000_out/kernel7layer_with_weights-13/kernel/.ATTRIBUTES/VARIABLE_VALUE
hf
VARIABLE_VALUEfmn10_tsvd_5000_1000_out/bias5layer_with_weights-13/bias/.ATTRIBUTES/VARIABLE_VALUE
 

x0
y1
 
 
�
�metrics
�layers
{	variables
|regularization_losses
�layer_metrics
 �layer_regularization_losses
}trainable_variables
�non_trainable_variables
nl
VARIABLE_VALUE!fmn10_isomap_5000_1000_out/kernel7layer_with_weights-14/kernel/.ATTRIBUTES/VARIABLE_VALUE
jh
VARIABLE_VALUEfmn10_isomap_5000_1000_out/bias5layer_with_weights-14/bias/.ATTRIBUTES/VARIABLE_VALUE
 

0
�1
 
 
�
�metrics
�layers
�	variables
�regularization_losses
�layer_metrics
 �layer_regularization_losses
�trainable_variables
�non_trainable_variables
ki
VARIABLE_VALUEfmn10_lle_5000_1000_out/kernel7layer_with_weights-15/kernel/.ATTRIBUTES/VARIABLE_VALUE
ge
VARIABLE_VALUEfmn10_lle_5000_1000_out/bias5layer_with_weights-15/bias/.ATTRIBUTES/VARIABLE_VALUE

�0
�1
 
 
�
�metrics
�layers
�	variables
�regularization_losses
�layer_metrics
 �layer_regularization_losses
�trainable_variables
�non_trainable_variables
 
 
 
�
�metrics
�layers
�	variables
�regularization_losses
�layer_metrics
 �layer_regularization_losses
�trainable_variables
�non_trainable_variables
ZX
VARIABLE_VALUEjoined/kernel7layer_with_weights-16/kernel/.ATTRIBUTES/VARIABLE_VALUE
VT
VARIABLE_VALUEjoined/bias5layer_with_weights-16/bias/.ATTRIBUTES/VARIABLE_VALUE

�0
�1
 

�0
�1
�
�metrics
�layers
�	variables
�regularization_losses
�layer_metrics
 �layer_regularization_losses
�trainable_variables
�non_trainable_variables
HF
VARIABLE_VALUE	Adam/iter)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUE
LJ
VARIABLE_VALUEAdam/beta_1+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUE
LJ
VARIABLE_VALUEAdam/beta_2+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUE
JH
VARIABLE_VALUE
Adam/decay*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUE
ZX
VARIABLE_VALUEAdam/learning_rate2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUE

�0
�1
�
0
1
2
3
4
5
6
7
	8

9
10
11
12
13
14
15
16
17
18
19
20
21
 
 
�
 0
!1
'2
(3
.4
/5
56
67
;8
<9
B10
C11
I12
J13
P14
Q15
V16
W17
]18
^19
d20
e21
k22
l23
q24
r25
x26
y27
28
�29
�30
�31
 
 
 
 

 0
!1
 
 
 
 

'0
(1
 
 
 
 

.0
/1
 
 
 
 

50
61
 
 
 
 

;0
<1
 
 
 
 

B0
C1
 
 
 
 

I0
J1
 
 
 
 

P0
Q1
 
 
 
 

V0
W1
 
 
 
 

]0
^1
 
 
 
 

d0
e1
 
 
 
 

k0
l1
 
 
 
 

q0
r1
 
 
 
 

x0
y1
 
 
 
 

0
�1
 
 
 
 

�0
�1
 
 
 
 
 
 
 
 
 
 
8

�total

�count
�	variables
�	keras_api
I

�total

�count
�
_fn_kwargs
�	variables
�	keras_api
OM
VARIABLE_VALUEtotal4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUE
OM
VARIABLE_VALUEcount4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUE

�0
�1

�	variables
QO
VARIABLE_VALUEtotal_14keras_api/metrics/1/total/.ATTRIBUTES/VARIABLE_VALUE
QO
VARIABLE_VALUEcount_14keras_api/metrics/1/count/.ATTRIBUTES/VARIABLE_VALUE
 

�0
�1

�	variables
}{
VARIABLE_VALUEAdam/joined/kernel/mSlayer_with_weights-16/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
yw
VARIABLE_VALUEAdam/joined/bias/mQlayer_with_weights-16/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
}{
VARIABLE_VALUEAdam/joined/kernel/vSlayer_with_weights-16/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
yw
VARIABLE_VALUEAdam/joined/bias/vQlayer_with_weights-16/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
�
,serving_default_fmn10_isomap_5000_1000_inputPlaceholder*(
_output_shapes
:����������*
dtype0*
shape:����������
�
)serving_default_fmn10_lle_5000_1000_inputPlaceholder*(
_output_shapes
:����������*
dtype0*
shape:����������
�
)serving_default_fmn10_pca_5000_1000_inputPlaceholder*(
_output_shapes
:����������*
dtype0*
shape:����������
�
*serving_default_fmn10_tsvd_5000_1000_inputPlaceholder*(
_output_shapes
:����������*
dtype0*
shape:����������
�
StatefulPartitionedCallStatefulPartitionedCall,serving_default_fmn10_isomap_5000_1000_input)serving_default_fmn10_lle_5000_1000_input)serving_default_fmn10_pca_5000_1000_input*serving_default_fmn10_tsvd_5000_1000_inputfmn10_lle_5000_1000_1/kernelfmn10_lle_5000_1000_1/biasfmn10_isomap_5000_1000_1/kernelfmn10_isomap_5000_1000_1/biasfmn10_tsvd_5000_1000_1/kernelfmn10_tsvd_5000_1000_1/biasfmn10_pca_5000_1000_1/kernelfmn10_pca_5000_1000_1/biasfmn10_lle_5000_1000_2/kernelfmn10_lle_5000_1000_2/biasfmn10_isomap_5000_1000_2/kernelfmn10_isomap_5000_1000_2/biasfmn10_tsvd_5000_1000_2/kernelfmn10_tsvd_5000_1000_2/biasfmn10_pca_5000_1000_2/kernelfmn10_pca_5000_1000_2/biasfmn10_lle_5000_1000_3/kernelfmn10_lle_5000_1000_3/biasfmn10_isomap_5000_1000_3/kernelfmn10_isomap_5000_1000_3/biasfmn10_tsvd_5000_1000_3/kernelfmn10_tsvd_5000_1000_3/biasfmn10_pca_5000_1000_3/kernelfmn10_pca_5000_1000_3/biasfmn10_pca_5000_1000_out/kernelfmn10_pca_5000_1000_out/biasfmn10_tsvd_5000_1000_out/kernelfmn10_tsvd_5000_1000_out/bias!fmn10_isomap_5000_1000_out/kernelfmn10_isomap_5000_1000_out/biasfmn10_lle_5000_1000_out/kernelfmn10_lle_5000_1000_out/biasjoined/kerneljoined/bias*1
Tin*
(2&*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*D
_read_only_resource_inputs&
$"	
 !"#$%*0
config_proto 

CPU

GPU2*0J 8� *.
f)R'
%__inference_signature_wrapper_2309527
O
saver_filenamePlaceholder*
_output_shapes
: *
dtype0*
shape: 
�
StatefulPartitionedCall_1StatefulPartitionedCallsaver_filename0fmn10_pca_5000_1000_1/kernel/Read/ReadVariableOp.fmn10_pca_5000_1000_1/bias/Read/ReadVariableOp1fmn10_tsvd_5000_1000_1/kernel/Read/ReadVariableOp/fmn10_tsvd_5000_1000_1/bias/Read/ReadVariableOp3fmn10_isomap_5000_1000_1/kernel/Read/ReadVariableOp1fmn10_isomap_5000_1000_1/bias/Read/ReadVariableOp0fmn10_lle_5000_1000_1/kernel/Read/ReadVariableOp.fmn10_lle_5000_1000_1/bias/Read/ReadVariableOp0fmn10_pca_5000_1000_2/kernel/Read/ReadVariableOp.fmn10_pca_5000_1000_2/bias/Read/ReadVariableOp1fmn10_tsvd_5000_1000_2/kernel/Read/ReadVariableOp/fmn10_tsvd_5000_1000_2/bias/Read/ReadVariableOp3fmn10_isomap_5000_1000_2/kernel/Read/ReadVariableOp1fmn10_isomap_5000_1000_2/bias/Read/ReadVariableOp0fmn10_lle_5000_1000_2/kernel/Read/ReadVariableOp.fmn10_lle_5000_1000_2/bias/Read/ReadVariableOp0fmn10_pca_5000_1000_3/kernel/Read/ReadVariableOp.fmn10_pca_5000_1000_3/bias/Read/ReadVariableOp1fmn10_tsvd_5000_1000_3/kernel/Read/ReadVariableOp/fmn10_tsvd_5000_1000_3/bias/Read/ReadVariableOp3fmn10_isomap_5000_1000_3/kernel/Read/ReadVariableOp1fmn10_isomap_5000_1000_3/bias/Read/ReadVariableOp0fmn10_lle_5000_1000_3/kernel/Read/ReadVariableOp.fmn10_lle_5000_1000_3/bias/Read/ReadVariableOp2fmn10_pca_5000_1000_out/kernel/Read/ReadVariableOp0fmn10_pca_5000_1000_out/bias/Read/ReadVariableOp3fmn10_tsvd_5000_1000_out/kernel/Read/ReadVariableOp1fmn10_tsvd_5000_1000_out/bias/Read/ReadVariableOp5fmn10_isomap_5000_1000_out/kernel/Read/ReadVariableOp3fmn10_isomap_5000_1000_out/bias/Read/ReadVariableOp2fmn10_lle_5000_1000_out/kernel/Read/ReadVariableOp0fmn10_lle_5000_1000_out/bias/Read/ReadVariableOp!joined/kernel/Read/ReadVariableOpjoined/bias/Read/ReadVariableOpAdam/iter/Read/ReadVariableOpAdam/beta_1/Read/ReadVariableOpAdam/beta_2/Read/ReadVariableOpAdam/decay/Read/ReadVariableOp&Adam/learning_rate/Read/ReadVariableOptotal/Read/ReadVariableOpcount/Read/ReadVariableOptotal_1/Read/ReadVariableOpcount_1/Read/ReadVariableOp(Adam/joined/kernel/m/Read/ReadVariableOp&Adam/joined/bias/m/Read/ReadVariableOp(Adam/joined/kernel/v/Read/ReadVariableOp&Adam/joined/bias/v/Read/ReadVariableOpConst*<
Tin5
321	*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *)
f$R"
 __inference__traced_save_2310459
�
StatefulPartitionedCall_2StatefulPartitionedCallsaver_filenamefmn10_pca_5000_1000_1/kernelfmn10_pca_5000_1000_1/biasfmn10_tsvd_5000_1000_1/kernelfmn10_tsvd_5000_1000_1/biasfmn10_isomap_5000_1000_1/kernelfmn10_isomap_5000_1000_1/biasfmn10_lle_5000_1000_1/kernelfmn10_lle_5000_1000_1/biasfmn10_pca_5000_1000_2/kernelfmn10_pca_5000_1000_2/biasfmn10_tsvd_5000_1000_2/kernelfmn10_tsvd_5000_1000_2/biasfmn10_isomap_5000_1000_2/kernelfmn10_isomap_5000_1000_2/biasfmn10_lle_5000_1000_2/kernelfmn10_lle_5000_1000_2/biasfmn10_pca_5000_1000_3/kernelfmn10_pca_5000_1000_3/biasfmn10_tsvd_5000_1000_3/kernelfmn10_tsvd_5000_1000_3/biasfmn10_isomap_5000_1000_3/kernelfmn10_isomap_5000_1000_3/biasfmn10_lle_5000_1000_3/kernelfmn10_lle_5000_1000_3/biasfmn10_pca_5000_1000_out/kernelfmn10_pca_5000_1000_out/biasfmn10_tsvd_5000_1000_out/kernelfmn10_tsvd_5000_1000_out/bias!fmn10_isomap_5000_1000_out/kernelfmn10_isomap_5000_1000_out/biasfmn10_lle_5000_1000_out/kernelfmn10_lle_5000_1000_out/biasjoined/kerneljoined/bias	Adam/iterAdam/beta_1Adam/beta_2
Adam/decayAdam/learning_ratetotalcounttotal_1count_1Adam/joined/kernel/mAdam/joined/bias/mAdam/joined/kernel/vAdam/joined/bias/v*;
Tin4
220*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *,
f'R%
#__inference__traced_restore_2310610ο
�
�
'__inference_model_layer_call_fn_2309679
inputs_0
inputs_1
inputs_2
inputs_3
unknown:
��
	unknown_0:	�
	unknown_1:
��
	unknown_2:	�
	unknown_3:
��
	unknown_4:	�
	unknown_5:
��
	unknown_6:	�
	unknown_7:
��
	unknown_8:	�
	unknown_9:
��

unknown_10:	�

unknown_11:
��

unknown_12:	�

unknown_13:
��

unknown_14:	�

unknown_15:
��

unknown_16:	�

unknown_17:
��

unknown_18:	�

unknown_19:
��

unknown_20:	�

unknown_21:
��

unknown_22:	�

unknown_23:	�

unknown_24:

unknown_25:	�

unknown_26:

unknown_27:	�

unknown_28:

unknown_29:	�

unknown_30:

unknown_31:

unknown_32:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputs_0inputs_1inputs_2inputs_3unknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10
unknown_11
unknown_12
unknown_13
unknown_14
unknown_15
unknown_16
unknown_17
unknown_18
unknown_19
unknown_20
unknown_21
unknown_22
unknown_23
unknown_24
unknown_25
unknown_26
unknown_27
unknown_28
unknown_29
unknown_30
unknown_31
unknown_32*1
Tin*
(2&*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*D
_read_only_resource_inputs&
$"	
 !"#$%*0
config_proto 

CPU

GPU2*0J 8� *K
fFRD
B__inference_model_layer_call_and_return_conditional_losses_23091102
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*�
_input_shapes�
�:����������:����������:����������:����������: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:R N
(
_output_shapes
:����������
"
_user_specified_name
inputs/0:RN
(
_output_shapes
:����������
"
_user_specified_name
inputs/1:RN
(
_output_shapes
:����������
"
_user_specified_name
inputs/2:RN
(
_output_shapes
:����������
"
_user_specified_name
inputs/3
�
�
R__inference_fmn10_lle_5000_1000_3_layer_call_and_return_conditional_losses_2310175

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
��
�
#__inference__traced_restore_2310610
file_prefixA
-assignvariableop_fmn10_pca_5000_1000_1_kernel:
��<
-assignvariableop_1_fmn10_pca_5000_1000_1_bias:	�D
0assignvariableop_2_fmn10_tsvd_5000_1000_1_kernel:
��=
.assignvariableop_3_fmn10_tsvd_5000_1000_1_bias:	�F
2assignvariableop_4_fmn10_isomap_5000_1000_1_kernel:
��?
0assignvariableop_5_fmn10_isomap_5000_1000_1_bias:	�C
/assignvariableop_6_fmn10_lle_5000_1000_1_kernel:
��<
-assignvariableop_7_fmn10_lle_5000_1000_1_bias:	�C
/assignvariableop_8_fmn10_pca_5000_1000_2_kernel:
��<
-assignvariableop_9_fmn10_pca_5000_1000_2_bias:	�E
1assignvariableop_10_fmn10_tsvd_5000_1000_2_kernel:
��>
/assignvariableop_11_fmn10_tsvd_5000_1000_2_bias:	�G
3assignvariableop_12_fmn10_isomap_5000_1000_2_kernel:
��@
1assignvariableop_13_fmn10_isomap_5000_1000_2_bias:	�D
0assignvariableop_14_fmn10_lle_5000_1000_2_kernel:
��=
.assignvariableop_15_fmn10_lle_5000_1000_2_bias:	�D
0assignvariableop_16_fmn10_pca_5000_1000_3_kernel:
��=
.assignvariableop_17_fmn10_pca_5000_1000_3_bias:	�E
1assignvariableop_18_fmn10_tsvd_5000_1000_3_kernel:
��>
/assignvariableop_19_fmn10_tsvd_5000_1000_3_bias:	�G
3assignvariableop_20_fmn10_isomap_5000_1000_3_kernel:
��@
1assignvariableop_21_fmn10_isomap_5000_1000_3_bias:	�D
0assignvariableop_22_fmn10_lle_5000_1000_3_kernel:
��=
.assignvariableop_23_fmn10_lle_5000_1000_3_bias:	�E
2assignvariableop_24_fmn10_pca_5000_1000_out_kernel:	�>
0assignvariableop_25_fmn10_pca_5000_1000_out_bias:F
3assignvariableop_26_fmn10_tsvd_5000_1000_out_kernel:	�?
1assignvariableop_27_fmn10_tsvd_5000_1000_out_bias:H
5assignvariableop_28_fmn10_isomap_5000_1000_out_kernel:	�A
3assignvariableop_29_fmn10_isomap_5000_1000_out_bias:E
2assignvariableop_30_fmn10_lle_5000_1000_out_kernel:	�>
0assignvariableop_31_fmn10_lle_5000_1000_out_bias:3
!assignvariableop_32_joined_kernel:-
assignvariableop_33_joined_bias:'
assignvariableop_34_adam_iter:	 )
assignvariableop_35_adam_beta_1: )
assignvariableop_36_adam_beta_2: (
assignvariableop_37_adam_decay: 0
&assignvariableop_38_adam_learning_rate: #
assignvariableop_39_total: #
assignvariableop_40_count: %
assignvariableop_41_total_1: %
assignvariableop_42_count_1: :
(assignvariableop_43_adam_joined_kernel_m:4
&assignvariableop_44_adam_joined_bias_m::
(assignvariableop_45_adam_joined_kernel_v:4
&assignvariableop_46_adam_joined_bias_v:
identity_48��AssignVariableOp�AssignVariableOp_1�AssignVariableOp_10�AssignVariableOp_11�AssignVariableOp_12�AssignVariableOp_13�AssignVariableOp_14�AssignVariableOp_15�AssignVariableOp_16�AssignVariableOp_17�AssignVariableOp_18�AssignVariableOp_19�AssignVariableOp_2�AssignVariableOp_20�AssignVariableOp_21�AssignVariableOp_22�AssignVariableOp_23�AssignVariableOp_24�AssignVariableOp_25�AssignVariableOp_26�AssignVariableOp_27�AssignVariableOp_28�AssignVariableOp_29�AssignVariableOp_3�AssignVariableOp_30�AssignVariableOp_31�AssignVariableOp_32�AssignVariableOp_33�AssignVariableOp_34�AssignVariableOp_35�AssignVariableOp_36�AssignVariableOp_37�AssignVariableOp_38�AssignVariableOp_39�AssignVariableOp_4�AssignVariableOp_40�AssignVariableOp_41�AssignVariableOp_42�AssignVariableOp_43�AssignVariableOp_44�AssignVariableOp_45�AssignVariableOp_46�AssignVariableOp_5�AssignVariableOp_6�AssignVariableOp_7�AssignVariableOp_8�AssignVariableOp_9�
RestoreV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:0*
dtype0*�
value�B�0B6layer_with_weights-0/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-0/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-1/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-1/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-3/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-3/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-4/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-4/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-5/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-5/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-6/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-6/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-7/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-7/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-8/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-8/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-9/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-9/bias/.ATTRIBUTES/VARIABLE_VALUEB7layer_with_weights-10/kernel/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-10/bias/.ATTRIBUTES/VARIABLE_VALUEB7layer_with_weights-11/kernel/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-11/bias/.ATTRIBUTES/VARIABLE_VALUEB7layer_with_weights-12/kernel/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-12/bias/.ATTRIBUTES/VARIABLE_VALUEB7layer_with_weights-13/kernel/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-13/bias/.ATTRIBUTES/VARIABLE_VALUEB7layer_with_weights-14/kernel/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-14/bias/.ATTRIBUTES/VARIABLE_VALUEB7layer_with_weights-15/kernel/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-15/bias/.ATTRIBUTES/VARIABLE_VALUEB7layer_with_weights-16/kernel/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-16/bias/.ATTRIBUTES/VARIABLE_VALUEB)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUEB*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/count/.ATTRIBUTES/VARIABLE_VALUEBSlayer_with_weights-16/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-16/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBSlayer_with_weights-16/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-16/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEB_CHECKPOINTABLE_OBJECT_GRAPH2
RestoreV2/tensor_names�
RestoreV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:0*
dtype0*s
valuejBh0B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B 2
RestoreV2/shape_and_slices�
	RestoreV2	RestoreV2file_prefixRestoreV2/tensor_names:output:0#RestoreV2/shape_and_slices:output:0"/device:CPU:0*�
_output_shapes�
�::::::::::::::::::::::::::::::::::::::::::::::::*>
dtypes4
220	2
	RestoreV2g
IdentityIdentityRestoreV2:tensors:0"/device:CPU:0*
T0*
_output_shapes
:2

Identity�
AssignVariableOpAssignVariableOp-assignvariableop_fmn10_pca_5000_1000_1_kernelIdentity:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOpk

Identity_1IdentityRestoreV2:tensors:1"/device:CPU:0*
T0*
_output_shapes
:2

Identity_1�
AssignVariableOp_1AssignVariableOp-assignvariableop_1_fmn10_pca_5000_1000_1_biasIdentity_1:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_1k

Identity_2IdentityRestoreV2:tensors:2"/device:CPU:0*
T0*
_output_shapes
:2

Identity_2�
AssignVariableOp_2AssignVariableOp0assignvariableop_2_fmn10_tsvd_5000_1000_1_kernelIdentity_2:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_2k

Identity_3IdentityRestoreV2:tensors:3"/device:CPU:0*
T0*
_output_shapes
:2

Identity_3�
AssignVariableOp_3AssignVariableOp.assignvariableop_3_fmn10_tsvd_5000_1000_1_biasIdentity_3:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_3k

Identity_4IdentityRestoreV2:tensors:4"/device:CPU:0*
T0*
_output_shapes
:2

Identity_4�
AssignVariableOp_4AssignVariableOp2assignvariableop_4_fmn10_isomap_5000_1000_1_kernelIdentity_4:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_4k

Identity_5IdentityRestoreV2:tensors:5"/device:CPU:0*
T0*
_output_shapes
:2

Identity_5�
AssignVariableOp_5AssignVariableOp0assignvariableop_5_fmn10_isomap_5000_1000_1_biasIdentity_5:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_5k

Identity_6IdentityRestoreV2:tensors:6"/device:CPU:0*
T0*
_output_shapes
:2

Identity_6�
AssignVariableOp_6AssignVariableOp/assignvariableop_6_fmn10_lle_5000_1000_1_kernelIdentity_6:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_6k

Identity_7IdentityRestoreV2:tensors:7"/device:CPU:0*
T0*
_output_shapes
:2

Identity_7�
AssignVariableOp_7AssignVariableOp-assignvariableop_7_fmn10_lle_5000_1000_1_biasIdentity_7:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_7k

Identity_8IdentityRestoreV2:tensors:8"/device:CPU:0*
T0*
_output_shapes
:2

Identity_8�
AssignVariableOp_8AssignVariableOp/assignvariableop_8_fmn10_pca_5000_1000_2_kernelIdentity_8:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_8k

Identity_9IdentityRestoreV2:tensors:9"/device:CPU:0*
T0*
_output_shapes
:2

Identity_9�
AssignVariableOp_9AssignVariableOp-assignvariableop_9_fmn10_pca_5000_1000_2_biasIdentity_9:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_9n
Identity_10IdentityRestoreV2:tensors:10"/device:CPU:0*
T0*
_output_shapes
:2
Identity_10�
AssignVariableOp_10AssignVariableOp1assignvariableop_10_fmn10_tsvd_5000_1000_2_kernelIdentity_10:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_10n
Identity_11IdentityRestoreV2:tensors:11"/device:CPU:0*
T0*
_output_shapes
:2
Identity_11�
AssignVariableOp_11AssignVariableOp/assignvariableop_11_fmn10_tsvd_5000_1000_2_biasIdentity_11:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_11n
Identity_12IdentityRestoreV2:tensors:12"/device:CPU:0*
T0*
_output_shapes
:2
Identity_12�
AssignVariableOp_12AssignVariableOp3assignvariableop_12_fmn10_isomap_5000_1000_2_kernelIdentity_12:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_12n
Identity_13IdentityRestoreV2:tensors:13"/device:CPU:0*
T0*
_output_shapes
:2
Identity_13�
AssignVariableOp_13AssignVariableOp1assignvariableop_13_fmn10_isomap_5000_1000_2_biasIdentity_13:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_13n
Identity_14IdentityRestoreV2:tensors:14"/device:CPU:0*
T0*
_output_shapes
:2
Identity_14�
AssignVariableOp_14AssignVariableOp0assignvariableop_14_fmn10_lle_5000_1000_2_kernelIdentity_14:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_14n
Identity_15IdentityRestoreV2:tensors:15"/device:CPU:0*
T0*
_output_shapes
:2
Identity_15�
AssignVariableOp_15AssignVariableOp.assignvariableop_15_fmn10_lle_5000_1000_2_biasIdentity_15:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_15n
Identity_16IdentityRestoreV2:tensors:16"/device:CPU:0*
T0*
_output_shapes
:2
Identity_16�
AssignVariableOp_16AssignVariableOp0assignvariableop_16_fmn10_pca_5000_1000_3_kernelIdentity_16:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_16n
Identity_17IdentityRestoreV2:tensors:17"/device:CPU:0*
T0*
_output_shapes
:2
Identity_17�
AssignVariableOp_17AssignVariableOp.assignvariableop_17_fmn10_pca_5000_1000_3_biasIdentity_17:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_17n
Identity_18IdentityRestoreV2:tensors:18"/device:CPU:0*
T0*
_output_shapes
:2
Identity_18�
AssignVariableOp_18AssignVariableOp1assignvariableop_18_fmn10_tsvd_5000_1000_3_kernelIdentity_18:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_18n
Identity_19IdentityRestoreV2:tensors:19"/device:CPU:0*
T0*
_output_shapes
:2
Identity_19�
AssignVariableOp_19AssignVariableOp/assignvariableop_19_fmn10_tsvd_5000_1000_3_biasIdentity_19:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_19n
Identity_20IdentityRestoreV2:tensors:20"/device:CPU:0*
T0*
_output_shapes
:2
Identity_20�
AssignVariableOp_20AssignVariableOp3assignvariableop_20_fmn10_isomap_5000_1000_3_kernelIdentity_20:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_20n
Identity_21IdentityRestoreV2:tensors:21"/device:CPU:0*
T0*
_output_shapes
:2
Identity_21�
AssignVariableOp_21AssignVariableOp1assignvariableop_21_fmn10_isomap_5000_1000_3_biasIdentity_21:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_21n
Identity_22IdentityRestoreV2:tensors:22"/device:CPU:0*
T0*
_output_shapes
:2
Identity_22�
AssignVariableOp_22AssignVariableOp0assignvariableop_22_fmn10_lle_5000_1000_3_kernelIdentity_22:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_22n
Identity_23IdentityRestoreV2:tensors:23"/device:CPU:0*
T0*
_output_shapes
:2
Identity_23�
AssignVariableOp_23AssignVariableOp.assignvariableop_23_fmn10_lle_5000_1000_3_biasIdentity_23:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_23n
Identity_24IdentityRestoreV2:tensors:24"/device:CPU:0*
T0*
_output_shapes
:2
Identity_24�
AssignVariableOp_24AssignVariableOp2assignvariableop_24_fmn10_pca_5000_1000_out_kernelIdentity_24:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_24n
Identity_25IdentityRestoreV2:tensors:25"/device:CPU:0*
T0*
_output_shapes
:2
Identity_25�
AssignVariableOp_25AssignVariableOp0assignvariableop_25_fmn10_pca_5000_1000_out_biasIdentity_25:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_25n
Identity_26IdentityRestoreV2:tensors:26"/device:CPU:0*
T0*
_output_shapes
:2
Identity_26�
AssignVariableOp_26AssignVariableOp3assignvariableop_26_fmn10_tsvd_5000_1000_out_kernelIdentity_26:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_26n
Identity_27IdentityRestoreV2:tensors:27"/device:CPU:0*
T0*
_output_shapes
:2
Identity_27�
AssignVariableOp_27AssignVariableOp1assignvariableop_27_fmn10_tsvd_5000_1000_out_biasIdentity_27:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_27n
Identity_28IdentityRestoreV2:tensors:28"/device:CPU:0*
T0*
_output_shapes
:2
Identity_28�
AssignVariableOp_28AssignVariableOp5assignvariableop_28_fmn10_isomap_5000_1000_out_kernelIdentity_28:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_28n
Identity_29IdentityRestoreV2:tensors:29"/device:CPU:0*
T0*
_output_shapes
:2
Identity_29�
AssignVariableOp_29AssignVariableOp3assignvariableop_29_fmn10_isomap_5000_1000_out_biasIdentity_29:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_29n
Identity_30IdentityRestoreV2:tensors:30"/device:CPU:0*
T0*
_output_shapes
:2
Identity_30�
AssignVariableOp_30AssignVariableOp2assignvariableop_30_fmn10_lle_5000_1000_out_kernelIdentity_30:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_30n
Identity_31IdentityRestoreV2:tensors:31"/device:CPU:0*
T0*
_output_shapes
:2
Identity_31�
AssignVariableOp_31AssignVariableOp0assignvariableop_31_fmn10_lle_5000_1000_out_biasIdentity_31:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_31n
Identity_32IdentityRestoreV2:tensors:32"/device:CPU:0*
T0*
_output_shapes
:2
Identity_32�
AssignVariableOp_32AssignVariableOp!assignvariableop_32_joined_kernelIdentity_32:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_32n
Identity_33IdentityRestoreV2:tensors:33"/device:CPU:0*
T0*
_output_shapes
:2
Identity_33�
AssignVariableOp_33AssignVariableOpassignvariableop_33_joined_biasIdentity_33:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_33n
Identity_34IdentityRestoreV2:tensors:34"/device:CPU:0*
T0	*
_output_shapes
:2
Identity_34�
AssignVariableOp_34AssignVariableOpassignvariableop_34_adam_iterIdentity_34:output:0"/device:CPU:0*
_output_shapes
 *
dtype0	2
AssignVariableOp_34n
Identity_35IdentityRestoreV2:tensors:35"/device:CPU:0*
T0*
_output_shapes
:2
Identity_35�
AssignVariableOp_35AssignVariableOpassignvariableop_35_adam_beta_1Identity_35:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_35n
Identity_36IdentityRestoreV2:tensors:36"/device:CPU:0*
T0*
_output_shapes
:2
Identity_36�
AssignVariableOp_36AssignVariableOpassignvariableop_36_adam_beta_2Identity_36:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_36n
Identity_37IdentityRestoreV2:tensors:37"/device:CPU:0*
T0*
_output_shapes
:2
Identity_37�
AssignVariableOp_37AssignVariableOpassignvariableop_37_adam_decayIdentity_37:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_37n
Identity_38IdentityRestoreV2:tensors:38"/device:CPU:0*
T0*
_output_shapes
:2
Identity_38�
AssignVariableOp_38AssignVariableOp&assignvariableop_38_adam_learning_rateIdentity_38:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_38n
Identity_39IdentityRestoreV2:tensors:39"/device:CPU:0*
T0*
_output_shapes
:2
Identity_39�
AssignVariableOp_39AssignVariableOpassignvariableop_39_totalIdentity_39:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_39n
Identity_40IdentityRestoreV2:tensors:40"/device:CPU:0*
T0*
_output_shapes
:2
Identity_40�
AssignVariableOp_40AssignVariableOpassignvariableop_40_countIdentity_40:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_40n
Identity_41IdentityRestoreV2:tensors:41"/device:CPU:0*
T0*
_output_shapes
:2
Identity_41�
AssignVariableOp_41AssignVariableOpassignvariableop_41_total_1Identity_41:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_41n
Identity_42IdentityRestoreV2:tensors:42"/device:CPU:0*
T0*
_output_shapes
:2
Identity_42�
AssignVariableOp_42AssignVariableOpassignvariableop_42_count_1Identity_42:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_42n
Identity_43IdentityRestoreV2:tensors:43"/device:CPU:0*
T0*
_output_shapes
:2
Identity_43�
AssignVariableOp_43AssignVariableOp(assignvariableop_43_adam_joined_kernel_mIdentity_43:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_43n
Identity_44IdentityRestoreV2:tensors:44"/device:CPU:0*
T0*
_output_shapes
:2
Identity_44�
AssignVariableOp_44AssignVariableOp&assignvariableop_44_adam_joined_bias_mIdentity_44:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_44n
Identity_45IdentityRestoreV2:tensors:45"/device:CPU:0*
T0*
_output_shapes
:2
Identity_45�
AssignVariableOp_45AssignVariableOp(assignvariableop_45_adam_joined_kernel_vIdentity_45:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_45n
Identity_46IdentityRestoreV2:tensors:46"/device:CPU:0*
T0*
_output_shapes
:2
Identity_46�
AssignVariableOp_46AssignVariableOp&assignvariableop_46_adam_joined_bias_vIdentity_46:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_469
NoOpNoOp"/device:CPU:0*
_output_shapes
 2
NoOp�
Identity_47Identityfile_prefix^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_12^AssignVariableOp_13^AssignVariableOp_14^AssignVariableOp_15^AssignVariableOp_16^AssignVariableOp_17^AssignVariableOp_18^AssignVariableOp_19^AssignVariableOp_2^AssignVariableOp_20^AssignVariableOp_21^AssignVariableOp_22^AssignVariableOp_23^AssignVariableOp_24^AssignVariableOp_25^AssignVariableOp_26^AssignVariableOp_27^AssignVariableOp_28^AssignVariableOp_29^AssignVariableOp_3^AssignVariableOp_30^AssignVariableOp_31^AssignVariableOp_32^AssignVariableOp_33^AssignVariableOp_34^AssignVariableOp_35^AssignVariableOp_36^AssignVariableOp_37^AssignVariableOp_38^AssignVariableOp_39^AssignVariableOp_4^AssignVariableOp_40^AssignVariableOp_41^AssignVariableOp_42^AssignVariableOp_43^AssignVariableOp_44^AssignVariableOp_45^AssignVariableOp_46^AssignVariableOp_5^AssignVariableOp_6^AssignVariableOp_7^AssignVariableOp_8^AssignVariableOp_9^NoOp"/device:CPU:0*
T0*
_output_shapes
: 2
Identity_47f
Identity_48IdentityIdentity_47:output:0^NoOp_1*
T0*
_output_shapes
: 2
Identity_48�
NoOp_1NoOp^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_12^AssignVariableOp_13^AssignVariableOp_14^AssignVariableOp_15^AssignVariableOp_16^AssignVariableOp_17^AssignVariableOp_18^AssignVariableOp_19^AssignVariableOp_2^AssignVariableOp_20^AssignVariableOp_21^AssignVariableOp_22^AssignVariableOp_23^AssignVariableOp_24^AssignVariableOp_25^AssignVariableOp_26^AssignVariableOp_27^AssignVariableOp_28^AssignVariableOp_29^AssignVariableOp_3^AssignVariableOp_30^AssignVariableOp_31^AssignVariableOp_32^AssignVariableOp_33^AssignVariableOp_34^AssignVariableOp_35^AssignVariableOp_36^AssignVariableOp_37^AssignVariableOp_38^AssignVariableOp_39^AssignVariableOp_4^AssignVariableOp_40^AssignVariableOp_41^AssignVariableOp_42^AssignVariableOp_43^AssignVariableOp_44^AssignVariableOp_45^AssignVariableOp_46^AssignVariableOp_5^AssignVariableOp_6^AssignVariableOp_7^AssignVariableOp_8^AssignVariableOp_9*"
_acd_function_control_output(*
_output_shapes
 2
NoOp_1"#
identity_48Identity_48:output:0*s
_input_shapesb
`: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 2$
AssignVariableOpAssignVariableOp2(
AssignVariableOp_1AssignVariableOp_12*
AssignVariableOp_10AssignVariableOp_102*
AssignVariableOp_11AssignVariableOp_112*
AssignVariableOp_12AssignVariableOp_122*
AssignVariableOp_13AssignVariableOp_132*
AssignVariableOp_14AssignVariableOp_142*
AssignVariableOp_15AssignVariableOp_152*
AssignVariableOp_16AssignVariableOp_162*
AssignVariableOp_17AssignVariableOp_172*
AssignVariableOp_18AssignVariableOp_182*
AssignVariableOp_19AssignVariableOp_192(
AssignVariableOp_2AssignVariableOp_22*
AssignVariableOp_20AssignVariableOp_202*
AssignVariableOp_21AssignVariableOp_212*
AssignVariableOp_22AssignVariableOp_222*
AssignVariableOp_23AssignVariableOp_232*
AssignVariableOp_24AssignVariableOp_242*
AssignVariableOp_25AssignVariableOp_252*
AssignVariableOp_26AssignVariableOp_262*
AssignVariableOp_27AssignVariableOp_272*
AssignVariableOp_28AssignVariableOp_282*
AssignVariableOp_29AssignVariableOp_292(
AssignVariableOp_3AssignVariableOp_32*
AssignVariableOp_30AssignVariableOp_302*
AssignVariableOp_31AssignVariableOp_312*
AssignVariableOp_32AssignVariableOp_322*
AssignVariableOp_33AssignVariableOp_332*
AssignVariableOp_34AssignVariableOp_342*
AssignVariableOp_35AssignVariableOp_352*
AssignVariableOp_36AssignVariableOp_362*
AssignVariableOp_37AssignVariableOp_372*
AssignVariableOp_38AssignVariableOp_382*
AssignVariableOp_39AssignVariableOp_392(
AssignVariableOp_4AssignVariableOp_42*
AssignVariableOp_40AssignVariableOp_402*
AssignVariableOp_41AssignVariableOp_412*
AssignVariableOp_42AssignVariableOp_422*
AssignVariableOp_43AssignVariableOp_432*
AssignVariableOp_44AssignVariableOp_442*
AssignVariableOp_45AssignVariableOp_452*
AssignVariableOp_46AssignVariableOp_462(
AssignVariableOp_5AssignVariableOp_52(
AssignVariableOp_6AssignVariableOp_62(
AssignVariableOp_7AssignVariableOp_72(
AssignVariableOp_8AssignVariableOp_82(
AssignVariableOp_9AssignVariableOp_9:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix
�
�
:__inference_fmn10_isomap_5000_1000_2_layer_call_fn_2310064

inputs
unknown:
��
	unknown_0:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *^
fYRW
U__inference_fmn10_isomap_5000_1000_2_layer_call_and_return_conditional_losses_23084812
StatefulPartitionedCall|
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:����������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
S__inference_fmn10_tsvd_5000_1000_2_layer_call_and_return_conditional_losses_2308498

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
R__inference_fmn10_pca_5000_1000_3_layer_call_and_return_conditional_losses_2308583

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
T__inference_fmn10_pca_5000_1000_out_layer_call_and_return_conditional_losses_2308600

inputs1
matmul_readvariableop_resource:	�-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes
:	�*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2	
BiasAdda
SigmoidSigmoidBiasAdd:output:0*
T0*'
_output_shapes
:���������2	
Sigmoidf
IdentityIdentitySigmoid:y:0^NoOp*
T0*'
_output_shapes
:���������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
R__inference_fmn10_lle_5000_1000_2_layer_call_and_return_conditional_losses_2308464

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
U__inference_fmn10_isomap_5000_1000_3_layer_call_and_return_conditional_losses_2308549

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
R__inference_fmn10_pca_5000_1000_3_layer_call_and_return_conditional_losses_2310115

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
7__inference_fmn10_pca_5000_1000_1_layer_call_fn_2309944

inputs
unknown:
��
	unknown_0:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_fmn10_pca_5000_1000_1_layer_call_and_return_conditional_losses_23084472
StatefulPartitionedCall|
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:����������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�	
'__inference_model_layer_call_fn_2309257
fmn10_pca_5000_1000_input
fmn10_tsvd_5000_1000_input 
fmn10_isomap_5000_1000_input
fmn10_lle_5000_1000_input
unknown:
��
	unknown_0:	�
	unknown_1:
��
	unknown_2:	�
	unknown_3:
��
	unknown_4:	�
	unknown_5:
��
	unknown_6:	�
	unknown_7:
��
	unknown_8:	�
	unknown_9:
��

unknown_10:	�

unknown_11:
��

unknown_12:	�

unknown_13:
��

unknown_14:	�

unknown_15:
��

unknown_16:	�

unknown_17:
��

unknown_18:	�

unknown_19:
��

unknown_20:	�

unknown_21:
��

unknown_22:	�

unknown_23:	�

unknown_24:

unknown_25:	�

unknown_26:

unknown_27:	�

unknown_28:

unknown_29:	�

unknown_30:

unknown_31:

unknown_32:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallfmn10_pca_5000_1000_inputfmn10_tsvd_5000_1000_inputfmn10_isomap_5000_1000_inputfmn10_lle_5000_1000_inputunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10
unknown_11
unknown_12
unknown_13
unknown_14
unknown_15
unknown_16
unknown_17
unknown_18
unknown_19
unknown_20
unknown_21
unknown_22
unknown_23
unknown_24
unknown_25
unknown_26
unknown_27
unknown_28
unknown_29
unknown_30
unknown_31
unknown_32*1
Tin*
(2&*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*D
_read_only_resource_inputs&
$"	
 !"#$%*0
config_proto 

CPU

GPU2*0J 8� *K
fFRD
B__inference_model_layer_call_and_return_conditional_losses_23091102
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*�
_input_shapes�
�:����������:����������:����������:����������: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:c _
(
_output_shapes
:����������
3
_user_specified_namefmn10_pca_5000_1000_input:d`
(
_output_shapes
:����������
4
_user_specified_namefmn10_tsvd_5000_1000_input:fb
(
_output_shapes
:����������
6
_user_specified_namefmn10_isomap_5000_1000_input:c_
(
_output_shapes
:����������
3
_user_specified_namefmn10_lle_5000_1000_input
�
�
7__inference_fmn10_lle_5000_1000_1_layer_call_fn_2310004

inputs
unknown:
��
	unknown_0:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_fmn10_lle_5000_1000_1_layer_call_and_return_conditional_losses_23083962
StatefulPartitionedCall|
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:����������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
R__inference_fmn10_pca_5000_1000_1_layer_call_and_return_conditional_losses_2309955

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
:__inference_fmn10_isomap_5000_1000_3_layer_call_fn_2310144

inputs
unknown:
��
	unknown_0:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *^
fYRW
U__inference_fmn10_isomap_5000_1000_3_layer_call_and_return_conditional_losses_23085492
StatefulPartitionedCall|
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:����������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
S__inference_fmn10_tsvd_5000_1000_3_layer_call_and_return_conditional_losses_2310135

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
S__inference_fmn10_tsvd_5000_1000_2_layer_call_and_return_conditional_losses_2310055

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
U__inference_fmn10_tsvd_5000_1000_out_layer_call_and_return_conditional_losses_2310215

inputs1
matmul_readvariableop_resource:	�-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes
:	�*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2	
BiasAdda
SigmoidSigmoidBiasAdd:output:0*
T0*'
_output_shapes
:���������2	
Sigmoidf
IdentityIdentitySigmoid:y:0^NoOp*
T0*'
_output_shapes
:���������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
K__inference_concatenate_68_layer_call_and_return_conditional_losses_2310272
inputs_0
inputs_1
inputs_2
inputs_3
identity\
concat/axisConst*
_output_shapes
: *
dtype0*
value	B :2
concat/axis�
concatConcatV2inputs_0inputs_1inputs_2inputs_3concat/axis:output:0*
N*
T0*'
_output_shapes
:���������2
concatc
IdentityIdentityconcat:output:0*
T0*'
_output_shapes
:���������2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*_
_input_shapesN
L:���������:���������:���������:���������:Q M
'
_output_shapes
:���������
"
_user_specified_name
inputs/0:QM
'
_output_shapes
:���������
"
_user_specified_name
inputs/1:QM
'
_output_shapes
:���������
"
_user_specified_name
inputs/2:QM
'
_output_shapes
:���������
"
_user_specified_name
inputs/3
�
�
S__inference_fmn10_tsvd_5000_1000_1_layer_call_and_return_conditional_losses_2309975

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
R__inference_fmn10_lle_5000_1000_1_layer_call_and_return_conditional_losses_2308396

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
W__inference_fmn10_isomap_5000_1000_out_layer_call_and_return_conditional_losses_2308634

inputs1
matmul_readvariableop_resource:	�-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes
:	�*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2	
BiasAdda
SigmoidSigmoidBiasAdd:output:0*
T0*'
_output_shapes
:���������2	
Sigmoidf
IdentityIdentitySigmoid:y:0^NoOp*
T0*'
_output_shapes
:���������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�w
�
B__inference_model_layer_call_and_return_conditional_losses_2309110

inputs
inputs_1
inputs_2
inputs_31
fmn10_lle_5000_1000_1_2309023:
��,
fmn10_lle_5000_1000_1_2309025:	�4
 fmn10_isomap_5000_1000_1_2309028:
��/
 fmn10_isomap_5000_1000_1_2309030:	�2
fmn10_tsvd_5000_1000_1_2309033:
��-
fmn10_tsvd_5000_1000_1_2309035:	�1
fmn10_pca_5000_1000_1_2309038:
��,
fmn10_pca_5000_1000_1_2309040:	�1
fmn10_lle_5000_1000_2_2309043:
��,
fmn10_lle_5000_1000_2_2309045:	�4
 fmn10_isomap_5000_1000_2_2309048:
��/
 fmn10_isomap_5000_1000_2_2309050:	�2
fmn10_tsvd_5000_1000_2_2309053:
��-
fmn10_tsvd_5000_1000_2_2309055:	�1
fmn10_pca_5000_1000_2_2309058:
��,
fmn10_pca_5000_1000_2_2309060:	�1
fmn10_lle_5000_1000_3_2309063:
��,
fmn10_lle_5000_1000_3_2309065:	�4
 fmn10_isomap_5000_1000_3_2309068:
��/
 fmn10_isomap_5000_1000_3_2309070:	�2
fmn10_tsvd_5000_1000_3_2309073:
��-
fmn10_tsvd_5000_1000_3_2309075:	�1
fmn10_pca_5000_1000_3_2309078:
��,
fmn10_pca_5000_1000_3_2309080:	�2
fmn10_pca_5000_1000_out_2309083:	�-
fmn10_pca_5000_1000_out_2309085:3
 fmn10_tsvd_5000_1000_out_2309088:	�.
 fmn10_tsvd_5000_1000_out_2309090:5
"fmn10_isomap_5000_1000_out_2309093:	�0
"fmn10_isomap_5000_1000_out_2309095:2
fmn10_lle_5000_1000_out_2309098:	�-
fmn10_lle_5000_1000_out_2309100: 
joined_2309104:
joined_2309106:
identity��0fmn10_isomap_5000_1000_1/StatefulPartitionedCall�0fmn10_isomap_5000_1000_2/StatefulPartitionedCall�0fmn10_isomap_5000_1000_3/StatefulPartitionedCall�2fmn10_isomap_5000_1000_out/StatefulPartitionedCall�-fmn10_lle_5000_1000_1/StatefulPartitionedCall�-fmn10_lle_5000_1000_2/StatefulPartitionedCall�-fmn10_lle_5000_1000_3/StatefulPartitionedCall�/fmn10_lle_5000_1000_out/StatefulPartitionedCall�-fmn10_pca_5000_1000_1/StatefulPartitionedCall�-fmn10_pca_5000_1000_2/StatefulPartitionedCall�-fmn10_pca_5000_1000_3/StatefulPartitionedCall�/fmn10_pca_5000_1000_out/StatefulPartitionedCall�.fmn10_tsvd_5000_1000_1/StatefulPartitionedCall�.fmn10_tsvd_5000_1000_2/StatefulPartitionedCall�.fmn10_tsvd_5000_1000_3/StatefulPartitionedCall�0fmn10_tsvd_5000_1000_out/StatefulPartitionedCall�joined/StatefulPartitionedCall�
-fmn10_lle_5000_1000_1/StatefulPartitionedCallStatefulPartitionedCallinputs_3fmn10_lle_5000_1000_1_2309023fmn10_lle_5000_1000_1_2309025*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_fmn10_lle_5000_1000_1_layer_call_and_return_conditional_losses_23083962/
-fmn10_lle_5000_1000_1/StatefulPartitionedCall�
0fmn10_isomap_5000_1000_1/StatefulPartitionedCallStatefulPartitionedCallinputs_2 fmn10_isomap_5000_1000_1_2309028 fmn10_isomap_5000_1000_1_2309030*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *^
fYRW
U__inference_fmn10_isomap_5000_1000_1_layer_call_and_return_conditional_losses_230841322
0fmn10_isomap_5000_1000_1/StatefulPartitionedCall�
.fmn10_tsvd_5000_1000_1/StatefulPartitionedCallStatefulPartitionedCallinputs_1fmn10_tsvd_5000_1000_1_2309033fmn10_tsvd_5000_1000_1_2309035*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *\
fWRU
S__inference_fmn10_tsvd_5000_1000_1_layer_call_and_return_conditional_losses_230843020
.fmn10_tsvd_5000_1000_1/StatefulPartitionedCall�
-fmn10_pca_5000_1000_1/StatefulPartitionedCallStatefulPartitionedCallinputsfmn10_pca_5000_1000_1_2309038fmn10_pca_5000_1000_1_2309040*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_fmn10_pca_5000_1000_1_layer_call_and_return_conditional_losses_23084472/
-fmn10_pca_5000_1000_1/StatefulPartitionedCall�
-fmn10_lle_5000_1000_2/StatefulPartitionedCallStatefulPartitionedCall6fmn10_lle_5000_1000_1/StatefulPartitionedCall:output:0fmn10_lle_5000_1000_2_2309043fmn10_lle_5000_1000_2_2309045*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_fmn10_lle_5000_1000_2_layer_call_and_return_conditional_losses_23084642/
-fmn10_lle_5000_1000_2/StatefulPartitionedCall�
0fmn10_isomap_5000_1000_2/StatefulPartitionedCallStatefulPartitionedCall9fmn10_isomap_5000_1000_1/StatefulPartitionedCall:output:0 fmn10_isomap_5000_1000_2_2309048 fmn10_isomap_5000_1000_2_2309050*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *^
fYRW
U__inference_fmn10_isomap_5000_1000_2_layer_call_and_return_conditional_losses_230848122
0fmn10_isomap_5000_1000_2/StatefulPartitionedCall�
.fmn10_tsvd_5000_1000_2/StatefulPartitionedCallStatefulPartitionedCall7fmn10_tsvd_5000_1000_1/StatefulPartitionedCall:output:0fmn10_tsvd_5000_1000_2_2309053fmn10_tsvd_5000_1000_2_2309055*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *\
fWRU
S__inference_fmn10_tsvd_5000_1000_2_layer_call_and_return_conditional_losses_230849820
.fmn10_tsvd_5000_1000_2/StatefulPartitionedCall�
-fmn10_pca_5000_1000_2/StatefulPartitionedCallStatefulPartitionedCall6fmn10_pca_5000_1000_1/StatefulPartitionedCall:output:0fmn10_pca_5000_1000_2_2309058fmn10_pca_5000_1000_2_2309060*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_fmn10_pca_5000_1000_2_layer_call_and_return_conditional_losses_23085152/
-fmn10_pca_5000_1000_2/StatefulPartitionedCall�
-fmn10_lle_5000_1000_3/StatefulPartitionedCallStatefulPartitionedCall6fmn10_lle_5000_1000_2/StatefulPartitionedCall:output:0fmn10_lle_5000_1000_3_2309063fmn10_lle_5000_1000_3_2309065*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_fmn10_lle_5000_1000_3_layer_call_and_return_conditional_losses_23085322/
-fmn10_lle_5000_1000_3/StatefulPartitionedCall�
0fmn10_isomap_5000_1000_3/StatefulPartitionedCallStatefulPartitionedCall9fmn10_isomap_5000_1000_2/StatefulPartitionedCall:output:0 fmn10_isomap_5000_1000_3_2309068 fmn10_isomap_5000_1000_3_2309070*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *^
fYRW
U__inference_fmn10_isomap_5000_1000_3_layer_call_and_return_conditional_losses_230854922
0fmn10_isomap_5000_1000_3/StatefulPartitionedCall�
.fmn10_tsvd_5000_1000_3/StatefulPartitionedCallStatefulPartitionedCall7fmn10_tsvd_5000_1000_2/StatefulPartitionedCall:output:0fmn10_tsvd_5000_1000_3_2309073fmn10_tsvd_5000_1000_3_2309075*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *\
fWRU
S__inference_fmn10_tsvd_5000_1000_3_layer_call_and_return_conditional_losses_230856620
.fmn10_tsvd_5000_1000_3/StatefulPartitionedCall�
-fmn10_pca_5000_1000_3/StatefulPartitionedCallStatefulPartitionedCall6fmn10_pca_5000_1000_2/StatefulPartitionedCall:output:0fmn10_pca_5000_1000_3_2309078fmn10_pca_5000_1000_3_2309080*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_fmn10_pca_5000_1000_3_layer_call_and_return_conditional_losses_23085832/
-fmn10_pca_5000_1000_3/StatefulPartitionedCall�
/fmn10_pca_5000_1000_out/StatefulPartitionedCallStatefulPartitionedCall6fmn10_pca_5000_1000_3/StatefulPartitionedCall:output:0fmn10_pca_5000_1000_out_2309083fmn10_pca_5000_1000_out_2309085*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *]
fXRV
T__inference_fmn10_pca_5000_1000_out_layer_call_and_return_conditional_losses_230860021
/fmn10_pca_5000_1000_out/StatefulPartitionedCall�
0fmn10_tsvd_5000_1000_out/StatefulPartitionedCallStatefulPartitionedCall7fmn10_tsvd_5000_1000_3/StatefulPartitionedCall:output:0 fmn10_tsvd_5000_1000_out_2309088 fmn10_tsvd_5000_1000_out_2309090*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *^
fYRW
U__inference_fmn10_tsvd_5000_1000_out_layer_call_and_return_conditional_losses_230861722
0fmn10_tsvd_5000_1000_out/StatefulPartitionedCall�
2fmn10_isomap_5000_1000_out/StatefulPartitionedCallStatefulPartitionedCall9fmn10_isomap_5000_1000_3/StatefulPartitionedCall:output:0"fmn10_isomap_5000_1000_out_2309093"fmn10_isomap_5000_1000_out_2309095*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *`
f[RY
W__inference_fmn10_isomap_5000_1000_out_layer_call_and_return_conditional_losses_230863424
2fmn10_isomap_5000_1000_out/StatefulPartitionedCall�
/fmn10_lle_5000_1000_out/StatefulPartitionedCallStatefulPartitionedCall6fmn10_lle_5000_1000_3/StatefulPartitionedCall:output:0fmn10_lle_5000_1000_out_2309098fmn10_lle_5000_1000_out_2309100*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *]
fXRV
T__inference_fmn10_lle_5000_1000_out_layer_call_and_return_conditional_losses_230865121
/fmn10_lle_5000_1000_out/StatefulPartitionedCall�
concatenate_68/PartitionedCallPartitionedCall8fmn10_pca_5000_1000_out/StatefulPartitionedCall:output:09fmn10_tsvd_5000_1000_out/StatefulPartitionedCall:output:0;fmn10_isomap_5000_1000_out/StatefulPartitionedCall:output:08fmn10_lle_5000_1000_out/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *T
fORM
K__inference_concatenate_68_layer_call_and_return_conditional_losses_23086662 
concatenate_68/PartitionedCall�
joined/StatefulPartitionedCallStatefulPartitionedCall'concatenate_68/PartitionedCall:output:0joined_2309104joined_2309106*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *L
fGRE
C__inference_joined_layer_call_and_return_conditional_losses_23086792 
joined/StatefulPartitionedCall�
IdentityIdentity'joined/StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identity�
NoOpNoOp1^fmn10_isomap_5000_1000_1/StatefulPartitionedCall1^fmn10_isomap_5000_1000_2/StatefulPartitionedCall1^fmn10_isomap_5000_1000_3/StatefulPartitionedCall3^fmn10_isomap_5000_1000_out/StatefulPartitionedCall.^fmn10_lle_5000_1000_1/StatefulPartitionedCall.^fmn10_lle_5000_1000_2/StatefulPartitionedCall.^fmn10_lle_5000_1000_3/StatefulPartitionedCall0^fmn10_lle_5000_1000_out/StatefulPartitionedCall.^fmn10_pca_5000_1000_1/StatefulPartitionedCall.^fmn10_pca_5000_1000_2/StatefulPartitionedCall.^fmn10_pca_5000_1000_3/StatefulPartitionedCall0^fmn10_pca_5000_1000_out/StatefulPartitionedCall/^fmn10_tsvd_5000_1000_1/StatefulPartitionedCall/^fmn10_tsvd_5000_1000_2/StatefulPartitionedCall/^fmn10_tsvd_5000_1000_3/StatefulPartitionedCall1^fmn10_tsvd_5000_1000_out/StatefulPartitionedCall^joined/StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*�
_input_shapes�
�:����������:����������:����������:����������: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 2d
0fmn10_isomap_5000_1000_1/StatefulPartitionedCall0fmn10_isomap_5000_1000_1/StatefulPartitionedCall2d
0fmn10_isomap_5000_1000_2/StatefulPartitionedCall0fmn10_isomap_5000_1000_2/StatefulPartitionedCall2d
0fmn10_isomap_5000_1000_3/StatefulPartitionedCall0fmn10_isomap_5000_1000_3/StatefulPartitionedCall2h
2fmn10_isomap_5000_1000_out/StatefulPartitionedCall2fmn10_isomap_5000_1000_out/StatefulPartitionedCall2^
-fmn10_lle_5000_1000_1/StatefulPartitionedCall-fmn10_lle_5000_1000_1/StatefulPartitionedCall2^
-fmn10_lle_5000_1000_2/StatefulPartitionedCall-fmn10_lle_5000_1000_2/StatefulPartitionedCall2^
-fmn10_lle_5000_1000_3/StatefulPartitionedCall-fmn10_lle_5000_1000_3/StatefulPartitionedCall2b
/fmn10_lle_5000_1000_out/StatefulPartitionedCall/fmn10_lle_5000_1000_out/StatefulPartitionedCall2^
-fmn10_pca_5000_1000_1/StatefulPartitionedCall-fmn10_pca_5000_1000_1/StatefulPartitionedCall2^
-fmn10_pca_5000_1000_2/StatefulPartitionedCall-fmn10_pca_5000_1000_2/StatefulPartitionedCall2^
-fmn10_pca_5000_1000_3/StatefulPartitionedCall-fmn10_pca_5000_1000_3/StatefulPartitionedCall2b
/fmn10_pca_5000_1000_out/StatefulPartitionedCall/fmn10_pca_5000_1000_out/StatefulPartitionedCall2`
.fmn10_tsvd_5000_1000_1/StatefulPartitionedCall.fmn10_tsvd_5000_1000_1/StatefulPartitionedCall2`
.fmn10_tsvd_5000_1000_2/StatefulPartitionedCall.fmn10_tsvd_5000_1000_2/StatefulPartitionedCall2`
.fmn10_tsvd_5000_1000_3/StatefulPartitionedCall.fmn10_tsvd_5000_1000_3/StatefulPartitionedCall2d
0fmn10_tsvd_5000_1000_out/StatefulPartitionedCall0fmn10_tsvd_5000_1000_out/StatefulPartitionedCall2@
joined/StatefulPartitionedCalljoined/StatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs:PL
(
_output_shapes
:����������
 
_user_specified_nameinputs:PL
(
_output_shapes
:����������
 
_user_specified_nameinputs:PL
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�	
%__inference_signature_wrapper_2309527 
fmn10_isomap_5000_1000_input
fmn10_lle_5000_1000_input
fmn10_pca_5000_1000_input
fmn10_tsvd_5000_1000_input
unknown:
��
	unknown_0:	�
	unknown_1:
��
	unknown_2:	�
	unknown_3:
��
	unknown_4:	�
	unknown_5:
��
	unknown_6:	�
	unknown_7:
��
	unknown_8:	�
	unknown_9:
��

unknown_10:	�

unknown_11:
��

unknown_12:	�

unknown_13:
��

unknown_14:	�

unknown_15:
��

unknown_16:	�

unknown_17:
��

unknown_18:	�

unknown_19:
��

unknown_20:	�

unknown_21:
��

unknown_22:	�

unknown_23:	�

unknown_24:

unknown_25:	�

unknown_26:

unknown_27:	�

unknown_28:

unknown_29:	�

unknown_30:

unknown_31:

unknown_32:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallfmn10_pca_5000_1000_inputfmn10_tsvd_5000_1000_inputfmn10_isomap_5000_1000_inputfmn10_lle_5000_1000_inputunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10
unknown_11
unknown_12
unknown_13
unknown_14
unknown_15
unknown_16
unknown_17
unknown_18
unknown_19
unknown_20
unknown_21
unknown_22
unknown_23
unknown_24
unknown_25
unknown_26
unknown_27
unknown_28
unknown_29
unknown_30
unknown_31
unknown_32*1
Tin*
(2&*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*D
_read_only_resource_inputs&
$"	
 !"#$%*0
config_proto 

CPU

GPU2*0J 8� *+
f&R$
"__inference__wrapped_model_23083722
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*�
_input_shapes�
�:����������:����������:����������:����������: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:f b
(
_output_shapes
:����������
6
_user_specified_namefmn10_isomap_5000_1000_input:c_
(
_output_shapes
:����������
3
_user_specified_namefmn10_lle_5000_1000_input:c_
(
_output_shapes
:����������
3
_user_specified_namefmn10_pca_5000_1000_input:d`
(
_output_shapes
:����������
4
_user_specified_namefmn10_tsvd_5000_1000_input
�
�
8__inference_fmn10_tsvd_5000_1000_1_layer_call_fn_2309964

inputs
unknown:
��
	unknown_0:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *\
fWRU
S__inference_fmn10_tsvd_5000_1000_1_layer_call_and_return_conditional_losses_23084302
StatefulPartitionedCall|
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:����������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
'__inference_model_layer_call_fn_2309603
inputs_0
inputs_1
inputs_2
inputs_3
unknown:
��
	unknown_0:	�
	unknown_1:
��
	unknown_2:	�
	unknown_3:
��
	unknown_4:	�
	unknown_5:
��
	unknown_6:	�
	unknown_7:
��
	unknown_8:	�
	unknown_9:
��

unknown_10:	�

unknown_11:
��

unknown_12:	�

unknown_13:
��

unknown_14:	�

unknown_15:
��

unknown_16:	�

unknown_17:
��

unknown_18:	�

unknown_19:
��

unknown_20:	�

unknown_21:
��

unknown_22:	�

unknown_23:	�

unknown_24:

unknown_25:	�

unknown_26:

unknown_27:	�

unknown_28:

unknown_29:	�

unknown_30:

unknown_31:

unknown_32:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputs_0inputs_1inputs_2inputs_3unknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10
unknown_11
unknown_12
unknown_13
unknown_14
unknown_15
unknown_16
unknown_17
unknown_18
unknown_19
unknown_20
unknown_21
unknown_22
unknown_23
unknown_24
unknown_25
unknown_26
unknown_27
unknown_28
unknown_29
unknown_30
unknown_31
unknown_32*1
Tin*
(2&*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*D
_read_only_resource_inputs&
$"	
 !"#$%*0
config_proto 

CPU

GPU2*0J 8� *K
fFRD
B__inference_model_layer_call_and_return_conditional_losses_23086862
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*�
_input_shapes�
�:����������:����������:����������:����������: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:R N
(
_output_shapes
:����������
"
_user_specified_name
inputs/0:RN
(
_output_shapes
:����������
"
_user_specified_name
inputs/1:RN
(
_output_shapes
:����������
"
_user_specified_name
inputs/2:RN
(
_output_shapes
:����������
"
_user_specified_name
inputs/3
�
x
0__inference_concatenate_68_layer_call_fn_2310263
inputs_0
inputs_1
inputs_2
inputs_3
identity�
PartitionedCallPartitionedCallinputs_0inputs_1inputs_2inputs_3*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *T
fORM
K__inference_concatenate_68_layer_call_and_return_conditional_losses_23086662
PartitionedCalll
IdentityIdentityPartitionedCall:output:0*
T0*'
_output_shapes
:���������2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*_
_input_shapesN
L:���������:���������:���������:���������:Q M
'
_output_shapes
:���������
"
_user_specified_name
inputs/0:QM
'
_output_shapes
:���������
"
_user_specified_name
inputs/1:QM
'
_output_shapes
:���������
"
_user_specified_name
inputs/2:QM
'
_output_shapes
:���������
"
_user_specified_name
inputs/3
�
�
U__inference_fmn10_isomap_5000_1000_2_layer_call_and_return_conditional_losses_2308481

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
7__inference_fmn10_lle_5000_1000_3_layer_call_fn_2310164

inputs
unknown:
��
	unknown_0:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_fmn10_lle_5000_1000_3_layer_call_and_return_conditional_losses_23085322
StatefulPartitionedCall|
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:����������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
9__inference_fmn10_pca_5000_1000_out_layer_call_fn_2310184

inputs
unknown:	�
	unknown_0:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *]
fXRV
T__inference_fmn10_pca_5000_1000_out_layer_call_and_return_conditional_losses_23086002
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
U__inference_fmn10_isomap_5000_1000_1_layer_call_and_return_conditional_losses_2309995

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
��
� 
B__inference_model_layer_call_and_return_conditional_losses_2309935
inputs_0
inputs_1
inputs_2
inputs_3H
4fmn10_lle_5000_1000_1_matmul_readvariableop_resource:
��D
5fmn10_lle_5000_1000_1_biasadd_readvariableop_resource:	�K
7fmn10_isomap_5000_1000_1_matmul_readvariableop_resource:
��G
8fmn10_isomap_5000_1000_1_biasadd_readvariableop_resource:	�I
5fmn10_tsvd_5000_1000_1_matmul_readvariableop_resource:
��E
6fmn10_tsvd_5000_1000_1_biasadd_readvariableop_resource:	�H
4fmn10_pca_5000_1000_1_matmul_readvariableop_resource:
��D
5fmn10_pca_5000_1000_1_biasadd_readvariableop_resource:	�H
4fmn10_lle_5000_1000_2_matmul_readvariableop_resource:
��D
5fmn10_lle_5000_1000_2_biasadd_readvariableop_resource:	�K
7fmn10_isomap_5000_1000_2_matmul_readvariableop_resource:
��G
8fmn10_isomap_5000_1000_2_biasadd_readvariableop_resource:	�I
5fmn10_tsvd_5000_1000_2_matmul_readvariableop_resource:
��E
6fmn10_tsvd_5000_1000_2_biasadd_readvariableop_resource:	�H
4fmn10_pca_5000_1000_2_matmul_readvariableop_resource:
��D
5fmn10_pca_5000_1000_2_biasadd_readvariableop_resource:	�H
4fmn10_lle_5000_1000_3_matmul_readvariableop_resource:
��D
5fmn10_lle_5000_1000_3_biasadd_readvariableop_resource:	�K
7fmn10_isomap_5000_1000_3_matmul_readvariableop_resource:
��G
8fmn10_isomap_5000_1000_3_biasadd_readvariableop_resource:	�I
5fmn10_tsvd_5000_1000_3_matmul_readvariableop_resource:
��E
6fmn10_tsvd_5000_1000_3_biasadd_readvariableop_resource:	�H
4fmn10_pca_5000_1000_3_matmul_readvariableop_resource:
��D
5fmn10_pca_5000_1000_3_biasadd_readvariableop_resource:	�I
6fmn10_pca_5000_1000_out_matmul_readvariableop_resource:	�E
7fmn10_pca_5000_1000_out_biasadd_readvariableop_resource:J
7fmn10_tsvd_5000_1000_out_matmul_readvariableop_resource:	�F
8fmn10_tsvd_5000_1000_out_biasadd_readvariableop_resource:L
9fmn10_isomap_5000_1000_out_matmul_readvariableop_resource:	�H
:fmn10_isomap_5000_1000_out_biasadd_readvariableop_resource:I
6fmn10_lle_5000_1000_out_matmul_readvariableop_resource:	�E
7fmn10_lle_5000_1000_out_biasadd_readvariableop_resource:7
%joined_matmul_readvariableop_resource:4
&joined_biasadd_readvariableop_resource:
identity��/fmn10_isomap_5000_1000_1/BiasAdd/ReadVariableOp�.fmn10_isomap_5000_1000_1/MatMul/ReadVariableOp�/fmn10_isomap_5000_1000_2/BiasAdd/ReadVariableOp�.fmn10_isomap_5000_1000_2/MatMul/ReadVariableOp�/fmn10_isomap_5000_1000_3/BiasAdd/ReadVariableOp�.fmn10_isomap_5000_1000_3/MatMul/ReadVariableOp�1fmn10_isomap_5000_1000_out/BiasAdd/ReadVariableOp�0fmn10_isomap_5000_1000_out/MatMul/ReadVariableOp�,fmn10_lle_5000_1000_1/BiasAdd/ReadVariableOp�+fmn10_lle_5000_1000_1/MatMul/ReadVariableOp�,fmn10_lle_5000_1000_2/BiasAdd/ReadVariableOp�+fmn10_lle_5000_1000_2/MatMul/ReadVariableOp�,fmn10_lle_5000_1000_3/BiasAdd/ReadVariableOp�+fmn10_lle_5000_1000_3/MatMul/ReadVariableOp�.fmn10_lle_5000_1000_out/BiasAdd/ReadVariableOp�-fmn10_lle_5000_1000_out/MatMul/ReadVariableOp�,fmn10_pca_5000_1000_1/BiasAdd/ReadVariableOp�+fmn10_pca_5000_1000_1/MatMul/ReadVariableOp�,fmn10_pca_5000_1000_2/BiasAdd/ReadVariableOp�+fmn10_pca_5000_1000_2/MatMul/ReadVariableOp�,fmn10_pca_5000_1000_3/BiasAdd/ReadVariableOp�+fmn10_pca_5000_1000_3/MatMul/ReadVariableOp�.fmn10_pca_5000_1000_out/BiasAdd/ReadVariableOp�-fmn10_pca_5000_1000_out/MatMul/ReadVariableOp�-fmn10_tsvd_5000_1000_1/BiasAdd/ReadVariableOp�,fmn10_tsvd_5000_1000_1/MatMul/ReadVariableOp�-fmn10_tsvd_5000_1000_2/BiasAdd/ReadVariableOp�,fmn10_tsvd_5000_1000_2/MatMul/ReadVariableOp�-fmn10_tsvd_5000_1000_3/BiasAdd/ReadVariableOp�,fmn10_tsvd_5000_1000_3/MatMul/ReadVariableOp�/fmn10_tsvd_5000_1000_out/BiasAdd/ReadVariableOp�.fmn10_tsvd_5000_1000_out/MatMul/ReadVariableOp�joined/BiasAdd/ReadVariableOp�joined/MatMul/ReadVariableOp�
+fmn10_lle_5000_1000_1/MatMul/ReadVariableOpReadVariableOp4fmn10_lle_5000_1000_1_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02-
+fmn10_lle_5000_1000_1/MatMul/ReadVariableOp�
fmn10_lle_5000_1000_1/MatMulMatMulinputs_33fmn10_lle_5000_1000_1/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn10_lle_5000_1000_1/MatMul�
,fmn10_lle_5000_1000_1/BiasAdd/ReadVariableOpReadVariableOp5fmn10_lle_5000_1000_1_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02.
,fmn10_lle_5000_1000_1/BiasAdd/ReadVariableOp�
fmn10_lle_5000_1000_1/BiasAddBiasAdd&fmn10_lle_5000_1000_1/MatMul:product:04fmn10_lle_5000_1000_1/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn10_lle_5000_1000_1/BiasAdd�
fmn10_lle_5000_1000_1/ReluRelu&fmn10_lle_5000_1000_1/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
fmn10_lle_5000_1000_1/Relu�
.fmn10_isomap_5000_1000_1/MatMul/ReadVariableOpReadVariableOp7fmn10_isomap_5000_1000_1_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype020
.fmn10_isomap_5000_1000_1/MatMul/ReadVariableOp�
fmn10_isomap_5000_1000_1/MatMulMatMulinputs_26fmn10_isomap_5000_1000_1/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2!
fmn10_isomap_5000_1000_1/MatMul�
/fmn10_isomap_5000_1000_1/BiasAdd/ReadVariableOpReadVariableOp8fmn10_isomap_5000_1000_1_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype021
/fmn10_isomap_5000_1000_1/BiasAdd/ReadVariableOp�
 fmn10_isomap_5000_1000_1/BiasAddBiasAdd)fmn10_isomap_5000_1000_1/MatMul:product:07fmn10_isomap_5000_1000_1/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2"
 fmn10_isomap_5000_1000_1/BiasAdd�
fmn10_isomap_5000_1000_1/ReluRelu)fmn10_isomap_5000_1000_1/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
fmn10_isomap_5000_1000_1/Relu�
,fmn10_tsvd_5000_1000_1/MatMul/ReadVariableOpReadVariableOp5fmn10_tsvd_5000_1000_1_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02.
,fmn10_tsvd_5000_1000_1/MatMul/ReadVariableOp�
fmn10_tsvd_5000_1000_1/MatMulMatMulinputs_14fmn10_tsvd_5000_1000_1/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn10_tsvd_5000_1000_1/MatMul�
-fmn10_tsvd_5000_1000_1/BiasAdd/ReadVariableOpReadVariableOp6fmn10_tsvd_5000_1000_1_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02/
-fmn10_tsvd_5000_1000_1/BiasAdd/ReadVariableOp�
fmn10_tsvd_5000_1000_1/BiasAddBiasAdd'fmn10_tsvd_5000_1000_1/MatMul:product:05fmn10_tsvd_5000_1000_1/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2 
fmn10_tsvd_5000_1000_1/BiasAdd�
fmn10_tsvd_5000_1000_1/ReluRelu'fmn10_tsvd_5000_1000_1/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
fmn10_tsvd_5000_1000_1/Relu�
+fmn10_pca_5000_1000_1/MatMul/ReadVariableOpReadVariableOp4fmn10_pca_5000_1000_1_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02-
+fmn10_pca_5000_1000_1/MatMul/ReadVariableOp�
fmn10_pca_5000_1000_1/MatMulMatMulinputs_03fmn10_pca_5000_1000_1/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn10_pca_5000_1000_1/MatMul�
,fmn10_pca_5000_1000_1/BiasAdd/ReadVariableOpReadVariableOp5fmn10_pca_5000_1000_1_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02.
,fmn10_pca_5000_1000_1/BiasAdd/ReadVariableOp�
fmn10_pca_5000_1000_1/BiasAddBiasAdd&fmn10_pca_5000_1000_1/MatMul:product:04fmn10_pca_5000_1000_1/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn10_pca_5000_1000_1/BiasAdd�
fmn10_pca_5000_1000_1/ReluRelu&fmn10_pca_5000_1000_1/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
fmn10_pca_5000_1000_1/Relu�
+fmn10_lle_5000_1000_2/MatMul/ReadVariableOpReadVariableOp4fmn10_lle_5000_1000_2_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02-
+fmn10_lle_5000_1000_2/MatMul/ReadVariableOp�
fmn10_lle_5000_1000_2/MatMulMatMul(fmn10_lle_5000_1000_1/Relu:activations:03fmn10_lle_5000_1000_2/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn10_lle_5000_1000_2/MatMul�
,fmn10_lle_5000_1000_2/BiasAdd/ReadVariableOpReadVariableOp5fmn10_lle_5000_1000_2_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02.
,fmn10_lle_5000_1000_2/BiasAdd/ReadVariableOp�
fmn10_lle_5000_1000_2/BiasAddBiasAdd&fmn10_lle_5000_1000_2/MatMul:product:04fmn10_lle_5000_1000_2/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn10_lle_5000_1000_2/BiasAdd�
fmn10_lle_5000_1000_2/ReluRelu&fmn10_lle_5000_1000_2/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
fmn10_lle_5000_1000_2/Relu�
.fmn10_isomap_5000_1000_2/MatMul/ReadVariableOpReadVariableOp7fmn10_isomap_5000_1000_2_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype020
.fmn10_isomap_5000_1000_2/MatMul/ReadVariableOp�
fmn10_isomap_5000_1000_2/MatMulMatMul+fmn10_isomap_5000_1000_1/Relu:activations:06fmn10_isomap_5000_1000_2/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2!
fmn10_isomap_5000_1000_2/MatMul�
/fmn10_isomap_5000_1000_2/BiasAdd/ReadVariableOpReadVariableOp8fmn10_isomap_5000_1000_2_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype021
/fmn10_isomap_5000_1000_2/BiasAdd/ReadVariableOp�
 fmn10_isomap_5000_1000_2/BiasAddBiasAdd)fmn10_isomap_5000_1000_2/MatMul:product:07fmn10_isomap_5000_1000_2/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2"
 fmn10_isomap_5000_1000_2/BiasAdd�
fmn10_isomap_5000_1000_2/ReluRelu)fmn10_isomap_5000_1000_2/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
fmn10_isomap_5000_1000_2/Relu�
,fmn10_tsvd_5000_1000_2/MatMul/ReadVariableOpReadVariableOp5fmn10_tsvd_5000_1000_2_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02.
,fmn10_tsvd_5000_1000_2/MatMul/ReadVariableOp�
fmn10_tsvd_5000_1000_2/MatMulMatMul)fmn10_tsvd_5000_1000_1/Relu:activations:04fmn10_tsvd_5000_1000_2/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn10_tsvd_5000_1000_2/MatMul�
-fmn10_tsvd_5000_1000_2/BiasAdd/ReadVariableOpReadVariableOp6fmn10_tsvd_5000_1000_2_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02/
-fmn10_tsvd_5000_1000_2/BiasAdd/ReadVariableOp�
fmn10_tsvd_5000_1000_2/BiasAddBiasAdd'fmn10_tsvd_5000_1000_2/MatMul:product:05fmn10_tsvd_5000_1000_2/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2 
fmn10_tsvd_5000_1000_2/BiasAdd�
fmn10_tsvd_5000_1000_2/ReluRelu'fmn10_tsvd_5000_1000_2/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
fmn10_tsvd_5000_1000_2/Relu�
+fmn10_pca_5000_1000_2/MatMul/ReadVariableOpReadVariableOp4fmn10_pca_5000_1000_2_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02-
+fmn10_pca_5000_1000_2/MatMul/ReadVariableOp�
fmn10_pca_5000_1000_2/MatMulMatMul(fmn10_pca_5000_1000_1/Relu:activations:03fmn10_pca_5000_1000_2/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn10_pca_5000_1000_2/MatMul�
,fmn10_pca_5000_1000_2/BiasAdd/ReadVariableOpReadVariableOp5fmn10_pca_5000_1000_2_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02.
,fmn10_pca_5000_1000_2/BiasAdd/ReadVariableOp�
fmn10_pca_5000_1000_2/BiasAddBiasAdd&fmn10_pca_5000_1000_2/MatMul:product:04fmn10_pca_5000_1000_2/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn10_pca_5000_1000_2/BiasAdd�
fmn10_pca_5000_1000_2/ReluRelu&fmn10_pca_5000_1000_2/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
fmn10_pca_5000_1000_2/Relu�
+fmn10_lle_5000_1000_3/MatMul/ReadVariableOpReadVariableOp4fmn10_lle_5000_1000_3_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02-
+fmn10_lle_5000_1000_3/MatMul/ReadVariableOp�
fmn10_lle_5000_1000_3/MatMulMatMul(fmn10_lle_5000_1000_2/Relu:activations:03fmn10_lle_5000_1000_3/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn10_lle_5000_1000_3/MatMul�
,fmn10_lle_5000_1000_3/BiasAdd/ReadVariableOpReadVariableOp5fmn10_lle_5000_1000_3_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02.
,fmn10_lle_5000_1000_3/BiasAdd/ReadVariableOp�
fmn10_lle_5000_1000_3/BiasAddBiasAdd&fmn10_lle_5000_1000_3/MatMul:product:04fmn10_lle_5000_1000_3/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn10_lle_5000_1000_3/BiasAdd�
fmn10_lle_5000_1000_3/ReluRelu&fmn10_lle_5000_1000_3/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
fmn10_lle_5000_1000_3/Relu�
.fmn10_isomap_5000_1000_3/MatMul/ReadVariableOpReadVariableOp7fmn10_isomap_5000_1000_3_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype020
.fmn10_isomap_5000_1000_3/MatMul/ReadVariableOp�
fmn10_isomap_5000_1000_3/MatMulMatMul+fmn10_isomap_5000_1000_2/Relu:activations:06fmn10_isomap_5000_1000_3/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2!
fmn10_isomap_5000_1000_3/MatMul�
/fmn10_isomap_5000_1000_3/BiasAdd/ReadVariableOpReadVariableOp8fmn10_isomap_5000_1000_3_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype021
/fmn10_isomap_5000_1000_3/BiasAdd/ReadVariableOp�
 fmn10_isomap_5000_1000_3/BiasAddBiasAdd)fmn10_isomap_5000_1000_3/MatMul:product:07fmn10_isomap_5000_1000_3/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2"
 fmn10_isomap_5000_1000_3/BiasAdd�
fmn10_isomap_5000_1000_3/ReluRelu)fmn10_isomap_5000_1000_3/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
fmn10_isomap_5000_1000_3/Relu�
,fmn10_tsvd_5000_1000_3/MatMul/ReadVariableOpReadVariableOp5fmn10_tsvd_5000_1000_3_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02.
,fmn10_tsvd_5000_1000_3/MatMul/ReadVariableOp�
fmn10_tsvd_5000_1000_3/MatMulMatMul)fmn10_tsvd_5000_1000_2/Relu:activations:04fmn10_tsvd_5000_1000_3/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn10_tsvd_5000_1000_3/MatMul�
-fmn10_tsvd_5000_1000_3/BiasAdd/ReadVariableOpReadVariableOp6fmn10_tsvd_5000_1000_3_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02/
-fmn10_tsvd_5000_1000_3/BiasAdd/ReadVariableOp�
fmn10_tsvd_5000_1000_3/BiasAddBiasAdd'fmn10_tsvd_5000_1000_3/MatMul:product:05fmn10_tsvd_5000_1000_3/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2 
fmn10_tsvd_5000_1000_3/BiasAdd�
fmn10_tsvd_5000_1000_3/ReluRelu'fmn10_tsvd_5000_1000_3/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
fmn10_tsvd_5000_1000_3/Relu�
+fmn10_pca_5000_1000_3/MatMul/ReadVariableOpReadVariableOp4fmn10_pca_5000_1000_3_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02-
+fmn10_pca_5000_1000_3/MatMul/ReadVariableOp�
fmn10_pca_5000_1000_3/MatMulMatMul(fmn10_pca_5000_1000_2/Relu:activations:03fmn10_pca_5000_1000_3/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn10_pca_5000_1000_3/MatMul�
,fmn10_pca_5000_1000_3/BiasAdd/ReadVariableOpReadVariableOp5fmn10_pca_5000_1000_3_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02.
,fmn10_pca_5000_1000_3/BiasAdd/ReadVariableOp�
fmn10_pca_5000_1000_3/BiasAddBiasAdd&fmn10_pca_5000_1000_3/MatMul:product:04fmn10_pca_5000_1000_3/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn10_pca_5000_1000_3/BiasAdd�
fmn10_pca_5000_1000_3/ReluRelu&fmn10_pca_5000_1000_3/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
fmn10_pca_5000_1000_3/Relu�
-fmn10_pca_5000_1000_out/MatMul/ReadVariableOpReadVariableOp6fmn10_pca_5000_1000_out_matmul_readvariableop_resource*
_output_shapes
:	�*
dtype02/
-fmn10_pca_5000_1000_out/MatMul/ReadVariableOp�
fmn10_pca_5000_1000_out/MatMulMatMul(fmn10_pca_5000_1000_3/Relu:activations:05fmn10_pca_5000_1000_out/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2 
fmn10_pca_5000_1000_out/MatMul�
.fmn10_pca_5000_1000_out/BiasAdd/ReadVariableOpReadVariableOp7fmn10_pca_5000_1000_out_biasadd_readvariableop_resource*
_output_shapes
:*
dtype020
.fmn10_pca_5000_1000_out/BiasAdd/ReadVariableOp�
fmn10_pca_5000_1000_out/BiasAddBiasAdd(fmn10_pca_5000_1000_out/MatMul:product:06fmn10_pca_5000_1000_out/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2!
fmn10_pca_5000_1000_out/BiasAdd�
fmn10_pca_5000_1000_out/SigmoidSigmoid(fmn10_pca_5000_1000_out/BiasAdd:output:0*
T0*'
_output_shapes
:���������2!
fmn10_pca_5000_1000_out/Sigmoid�
.fmn10_tsvd_5000_1000_out/MatMul/ReadVariableOpReadVariableOp7fmn10_tsvd_5000_1000_out_matmul_readvariableop_resource*
_output_shapes
:	�*
dtype020
.fmn10_tsvd_5000_1000_out/MatMul/ReadVariableOp�
fmn10_tsvd_5000_1000_out/MatMulMatMul)fmn10_tsvd_5000_1000_3/Relu:activations:06fmn10_tsvd_5000_1000_out/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2!
fmn10_tsvd_5000_1000_out/MatMul�
/fmn10_tsvd_5000_1000_out/BiasAdd/ReadVariableOpReadVariableOp8fmn10_tsvd_5000_1000_out_biasadd_readvariableop_resource*
_output_shapes
:*
dtype021
/fmn10_tsvd_5000_1000_out/BiasAdd/ReadVariableOp�
 fmn10_tsvd_5000_1000_out/BiasAddBiasAdd)fmn10_tsvd_5000_1000_out/MatMul:product:07fmn10_tsvd_5000_1000_out/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2"
 fmn10_tsvd_5000_1000_out/BiasAdd�
 fmn10_tsvd_5000_1000_out/SigmoidSigmoid)fmn10_tsvd_5000_1000_out/BiasAdd:output:0*
T0*'
_output_shapes
:���������2"
 fmn10_tsvd_5000_1000_out/Sigmoid�
0fmn10_isomap_5000_1000_out/MatMul/ReadVariableOpReadVariableOp9fmn10_isomap_5000_1000_out_matmul_readvariableop_resource*
_output_shapes
:	�*
dtype022
0fmn10_isomap_5000_1000_out/MatMul/ReadVariableOp�
!fmn10_isomap_5000_1000_out/MatMulMatMul+fmn10_isomap_5000_1000_3/Relu:activations:08fmn10_isomap_5000_1000_out/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2#
!fmn10_isomap_5000_1000_out/MatMul�
1fmn10_isomap_5000_1000_out/BiasAdd/ReadVariableOpReadVariableOp:fmn10_isomap_5000_1000_out_biasadd_readvariableop_resource*
_output_shapes
:*
dtype023
1fmn10_isomap_5000_1000_out/BiasAdd/ReadVariableOp�
"fmn10_isomap_5000_1000_out/BiasAddBiasAdd+fmn10_isomap_5000_1000_out/MatMul:product:09fmn10_isomap_5000_1000_out/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2$
"fmn10_isomap_5000_1000_out/BiasAdd�
"fmn10_isomap_5000_1000_out/SigmoidSigmoid+fmn10_isomap_5000_1000_out/BiasAdd:output:0*
T0*'
_output_shapes
:���������2$
"fmn10_isomap_5000_1000_out/Sigmoid�
-fmn10_lle_5000_1000_out/MatMul/ReadVariableOpReadVariableOp6fmn10_lle_5000_1000_out_matmul_readvariableop_resource*
_output_shapes
:	�*
dtype02/
-fmn10_lle_5000_1000_out/MatMul/ReadVariableOp�
fmn10_lle_5000_1000_out/MatMulMatMul(fmn10_lle_5000_1000_3/Relu:activations:05fmn10_lle_5000_1000_out/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2 
fmn10_lle_5000_1000_out/MatMul�
.fmn10_lle_5000_1000_out/BiasAdd/ReadVariableOpReadVariableOp7fmn10_lle_5000_1000_out_biasadd_readvariableop_resource*
_output_shapes
:*
dtype020
.fmn10_lle_5000_1000_out/BiasAdd/ReadVariableOp�
fmn10_lle_5000_1000_out/BiasAddBiasAdd(fmn10_lle_5000_1000_out/MatMul:product:06fmn10_lle_5000_1000_out/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2!
fmn10_lle_5000_1000_out/BiasAdd�
fmn10_lle_5000_1000_out/SigmoidSigmoid(fmn10_lle_5000_1000_out/BiasAdd:output:0*
T0*'
_output_shapes
:���������2!
fmn10_lle_5000_1000_out/Sigmoidz
concatenate_68/concat/axisConst*
_output_shapes
: *
dtype0*
value	B :2
concatenate_68/concat/axis�
concatenate_68/concatConcatV2#fmn10_pca_5000_1000_out/Sigmoid:y:0$fmn10_tsvd_5000_1000_out/Sigmoid:y:0&fmn10_isomap_5000_1000_out/Sigmoid:y:0#fmn10_lle_5000_1000_out/Sigmoid:y:0#concatenate_68/concat/axis:output:0*
N*
T0*'
_output_shapes
:���������2
concatenate_68/concat�
joined/MatMul/ReadVariableOpReadVariableOp%joined_matmul_readvariableop_resource*
_output_shapes

:*
dtype02
joined/MatMul/ReadVariableOp�
joined/MatMulMatMulconcatenate_68/concat:output:0$joined/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
joined/MatMul�
joined/BiasAdd/ReadVariableOpReadVariableOp&joined_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02
joined/BiasAdd/ReadVariableOp�
joined/BiasAddBiasAddjoined/MatMul:product:0%joined/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
joined/BiasAddv
joined/SigmoidSigmoidjoined/BiasAdd:output:0*
T0*'
_output_shapes
:���������2
joined/Sigmoidm
IdentityIdentityjoined/Sigmoid:y:0^NoOp*
T0*'
_output_shapes
:���������2

Identity�
NoOpNoOp0^fmn10_isomap_5000_1000_1/BiasAdd/ReadVariableOp/^fmn10_isomap_5000_1000_1/MatMul/ReadVariableOp0^fmn10_isomap_5000_1000_2/BiasAdd/ReadVariableOp/^fmn10_isomap_5000_1000_2/MatMul/ReadVariableOp0^fmn10_isomap_5000_1000_3/BiasAdd/ReadVariableOp/^fmn10_isomap_5000_1000_3/MatMul/ReadVariableOp2^fmn10_isomap_5000_1000_out/BiasAdd/ReadVariableOp1^fmn10_isomap_5000_1000_out/MatMul/ReadVariableOp-^fmn10_lle_5000_1000_1/BiasAdd/ReadVariableOp,^fmn10_lle_5000_1000_1/MatMul/ReadVariableOp-^fmn10_lle_5000_1000_2/BiasAdd/ReadVariableOp,^fmn10_lle_5000_1000_2/MatMul/ReadVariableOp-^fmn10_lle_5000_1000_3/BiasAdd/ReadVariableOp,^fmn10_lle_5000_1000_3/MatMul/ReadVariableOp/^fmn10_lle_5000_1000_out/BiasAdd/ReadVariableOp.^fmn10_lle_5000_1000_out/MatMul/ReadVariableOp-^fmn10_pca_5000_1000_1/BiasAdd/ReadVariableOp,^fmn10_pca_5000_1000_1/MatMul/ReadVariableOp-^fmn10_pca_5000_1000_2/BiasAdd/ReadVariableOp,^fmn10_pca_5000_1000_2/MatMul/ReadVariableOp-^fmn10_pca_5000_1000_3/BiasAdd/ReadVariableOp,^fmn10_pca_5000_1000_3/MatMul/ReadVariableOp/^fmn10_pca_5000_1000_out/BiasAdd/ReadVariableOp.^fmn10_pca_5000_1000_out/MatMul/ReadVariableOp.^fmn10_tsvd_5000_1000_1/BiasAdd/ReadVariableOp-^fmn10_tsvd_5000_1000_1/MatMul/ReadVariableOp.^fmn10_tsvd_5000_1000_2/BiasAdd/ReadVariableOp-^fmn10_tsvd_5000_1000_2/MatMul/ReadVariableOp.^fmn10_tsvd_5000_1000_3/BiasAdd/ReadVariableOp-^fmn10_tsvd_5000_1000_3/MatMul/ReadVariableOp0^fmn10_tsvd_5000_1000_out/BiasAdd/ReadVariableOp/^fmn10_tsvd_5000_1000_out/MatMul/ReadVariableOp^joined/BiasAdd/ReadVariableOp^joined/MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*�
_input_shapes�
�:����������:����������:����������:����������: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 2b
/fmn10_isomap_5000_1000_1/BiasAdd/ReadVariableOp/fmn10_isomap_5000_1000_1/BiasAdd/ReadVariableOp2`
.fmn10_isomap_5000_1000_1/MatMul/ReadVariableOp.fmn10_isomap_5000_1000_1/MatMul/ReadVariableOp2b
/fmn10_isomap_5000_1000_2/BiasAdd/ReadVariableOp/fmn10_isomap_5000_1000_2/BiasAdd/ReadVariableOp2`
.fmn10_isomap_5000_1000_2/MatMul/ReadVariableOp.fmn10_isomap_5000_1000_2/MatMul/ReadVariableOp2b
/fmn10_isomap_5000_1000_3/BiasAdd/ReadVariableOp/fmn10_isomap_5000_1000_3/BiasAdd/ReadVariableOp2`
.fmn10_isomap_5000_1000_3/MatMul/ReadVariableOp.fmn10_isomap_5000_1000_3/MatMul/ReadVariableOp2f
1fmn10_isomap_5000_1000_out/BiasAdd/ReadVariableOp1fmn10_isomap_5000_1000_out/BiasAdd/ReadVariableOp2d
0fmn10_isomap_5000_1000_out/MatMul/ReadVariableOp0fmn10_isomap_5000_1000_out/MatMul/ReadVariableOp2\
,fmn10_lle_5000_1000_1/BiasAdd/ReadVariableOp,fmn10_lle_5000_1000_1/BiasAdd/ReadVariableOp2Z
+fmn10_lle_5000_1000_1/MatMul/ReadVariableOp+fmn10_lle_5000_1000_1/MatMul/ReadVariableOp2\
,fmn10_lle_5000_1000_2/BiasAdd/ReadVariableOp,fmn10_lle_5000_1000_2/BiasAdd/ReadVariableOp2Z
+fmn10_lle_5000_1000_2/MatMul/ReadVariableOp+fmn10_lle_5000_1000_2/MatMul/ReadVariableOp2\
,fmn10_lle_5000_1000_3/BiasAdd/ReadVariableOp,fmn10_lle_5000_1000_3/BiasAdd/ReadVariableOp2Z
+fmn10_lle_5000_1000_3/MatMul/ReadVariableOp+fmn10_lle_5000_1000_3/MatMul/ReadVariableOp2`
.fmn10_lle_5000_1000_out/BiasAdd/ReadVariableOp.fmn10_lle_5000_1000_out/BiasAdd/ReadVariableOp2^
-fmn10_lle_5000_1000_out/MatMul/ReadVariableOp-fmn10_lle_5000_1000_out/MatMul/ReadVariableOp2\
,fmn10_pca_5000_1000_1/BiasAdd/ReadVariableOp,fmn10_pca_5000_1000_1/BiasAdd/ReadVariableOp2Z
+fmn10_pca_5000_1000_1/MatMul/ReadVariableOp+fmn10_pca_5000_1000_1/MatMul/ReadVariableOp2\
,fmn10_pca_5000_1000_2/BiasAdd/ReadVariableOp,fmn10_pca_5000_1000_2/BiasAdd/ReadVariableOp2Z
+fmn10_pca_5000_1000_2/MatMul/ReadVariableOp+fmn10_pca_5000_1000_2/MatMul/ReadVariableOp2\
,fmn10_pca_5000_1000_3/BiasAdd/ReadVariableOp,fmn10_pca_5000_1000_3/BiasAdd/ReadVariableOp2Z
+fmn10_pca_5000_1000_3/MatMul/ReadVariableOp+fmn10_pca_5000_1000_3/MatMul/ReadVariableOp2`
.fmn10_pca_5000_1000_out/BiasAdd/ReadVariableOp.fmn10_pca_5000_1000_out/BiasAdd/ReadVariableOp2^
-fmn10_pca_5000_1000_out/MatMul/ReadVariableOp-fmn10_pca_5000_1000_out/MatMul/ReadVariableOp2^
-fmn10_tsvd_5000_1000_1/BiasAdd/ReadVariableOp-fmn10_tsvd_5000_1000_1/BiasAdd/ReadVariableOp2\
,fmn10_tsvd_5000_1000_1/MatMul/ReadVariableOp,fmn10_tsvd_5000_1000_1/MatMul/ReadVariableOp2^
-fmn10_tsvd_5000_1000_2/BiasAdd/ReadVariableOp-fmn10_tsvd_5000_1000_2/BiasAdd/ReadVariableOp2\
,fmn10_tsvd_5000_1000_2/MatMul/ReadVariableOp,fmn10_tsvd_5000_1000_2/MatMul/ReadVariableOp2^
-fmn10_tsvd_5000_1000_3/BiasAdd/ReadVariableOp-fmn10_tsvd_5000_1000_3/BiasAdd/ReadVariableOp2\
,fmn10_tsvd_5000_1000_3/MatMul/ReadVariableOp,fmn10_tsvd_5000_1000_3/MatMul/ReadVariableOp2b
/fmn10_tsvd_5000_1000_out/BiasAdd/ReadVariableOp/fmn10_tsvd_5000_1000_out/BiasAdd/ReadVariableOp2`
.fmn10_tsvd_5000_1000_out/MatMul/ReadVariableOp.fmn10_tsvd_5000_1000_out/MatMul/ReadVariableOp2>
joined/BiasAdd/ReadVariableOpjoined/BiasAdd/ReadVariableOp2<
joined/MatMul/ReadVariableOpjoined/MatMul/ReadVariableOp:R N
(
_output_shapes
:����������
"
_user_specified_name
inputs/0:RN
(
_output_shapes
:����������
"
_user_specified_name
inputs/1:RN
(
_output_shapes
:����������
"
_user_specified_name
inputs/2:RN
(
_output_shapes
:����������
"
_user_specified_name
inputs/3
�
�
7__inference_fmn10_pca_5000_1000_3_layer_call_fn_2310104

inputs
unknown:
��
	unknown_0:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_fmn10_pca_5000_1000_3_layer_call_and_return_conditional_losses_23085832
StatefulPartitionedCall|
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:����������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
R__inference_fmn10_lle_5000_1000_1_layer_call_and_return_conditional_losses_2310015

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
T__inference_fmn10_pca_5000_1000_out_layer_call_and_return_conditional_losses_2310195

inputs1
matmul_readvariableop_resource:	�-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes
:	�*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2	
BiasAdda
SigmoidSigmoidBiasAdd:output:0*
T0*'
_output_shapes
:���������2	
Sigmoidf
IdentityIdentitySigmoid:y:0^NoOp*
T0*'
_output_shapes
:���������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
T__inference_fmn10_lle_5000_1000_out_layer_call_and_return_conditional_losses_2310255

inputs1
matmul_readvariableop_resource:	�-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes
:	�*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2	
BiasAdda
SigmoidSigmoidBiasAdd:output:0*
T0*'
_output_shapes
:���������2	
Sigmoidf
IdentityIdentitySigmoid:y:0^NoOp*
T0*'
_output_shapes
:���������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
R__inference_fmn10_pca_5000_1000_2_layer_call_and_return_conditional_losses_2310035

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
R__inference_fmn10_pca_5000_1000_1_layer_call_and_return_conditional_losses_2308447

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
S__inference_fmn10_tsvd_5000_1000_1_layer_call_and_return_conditional_losses_2308430

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
U__inference_fmn10_isomap_5000_1000_1_layer_call_and_return_conditional_losses_2308413

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�	
'__inference_model_layer_call_fn_2308757
fmn10_pca_5000_1000_input
fmn10_tsvd_5000_1000_input 
fmn10_isomap_5000_1000_input
fmn10_lle_5000_1000_input
unknown:
��
	unknown_0:	�
	unknown_1:
��
	unknown_2:	�
	unknown_3:
��
	unknown_4:	�
	unknown_5:
��
	unknown_6:	�
	unknown_7:
��
	unknown_8:	�
	unknown_9:
��

unknown_10:	�

unknown_11:
��

unknown_12:	�

unknown_13:
��

unknown_14:	�

unknown_15:
��

unknown_16:	�

unknown_17:
��

unknown_18:	�

unknown_19:
��

unknown_20:	�

unknown_21:
��

unknown_22:	�

unknown_23:	�

unknown_24:

unknown_25:	�

unknown_26:

unknown_27:	�

unknown_28:

unknown_29:	�

unknown_30:

unknown_31:

unknown_32:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallfmn10_pca_5000_1000_inputfmn10_tsvd_5000_1000_inputfmn10_isomap_5000_1000_inputfmn10_lle_5000_1000_inputunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10
unknown_11
unknown_12
unknown_13
unknown_14
unknown_15
unknown_16
unknown_17
unknown_18
unknown_19
unknown_20
unknown_21
unknown_22
unknown_23
unknown_24
unknown_25
unknown_26
unknown_27
unknown_28
unknown_29
unknown_30
unknown_31
unknown_32*1
Tin*
(2&*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*D
_read_only_resource_inputs&
$"	
 !"#$%*0
config_proto 

CPU

GPU2*0J 8� *K
fFRD
B__inference_model_layer_call_and_return_conditional_losses_23086862
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*�
_input_shapes�
�:����������:����������:����������:����������: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:c _
(
_output_shapes
:����������
3
_user_specified_namefmn10_pca_5000_1000_input:d`
(
_output_shapes
:����������
4
_user_specified_namefmn10_tsvd_5000_1000_input:fb
(
_output_shapes
:����������
6
_user_specified_namefmn10_isomap_5000_1000_input:c_
(
_output_shapes
:����������
3
_user_specified_namefmn10_lle_5000_1000_input
�y
�
B__inference_model_layer_call_and_return_conditional_losses_2309443
fmn10_pca_5000_1000_input
fmn10_tsvd_5000_1000_input 
fmn10_isomap_5000_1000_input
fmn10_lle_5000_1000_input1
fmn10_lle_5000_1000_1_2309356:
��,
fmn10_lle_5000_1000_1_2309358:	�4
 fmn10_isomap_5000_1000_1_2309361:
��/
 fmn10_isomap_5000_1000_1_2309363:	�2
fmn10_tsvd_5000_1000_1_2309366:
��-
fmn10_tsvd_5000_1000_1_2309368:	�1
fmn10_pca_5000_1000_1_2309371:
��,
fmn10_pca_5000_1000_1_2309373:	�1
fmn10_lle_5000_1000_2_2309376:
��,
fmn10_lle_5000_1000_2_2309378:	�4
 fmn10_isomap_5000_1000_2_2309381:
��/
 fmn10_isomap_5000_1000_2_2309383:	�2
fmn10_tsvd_5000_1000_2_2309386:
��-
fmn10_tsvd_5000_1000_2_2309388:	�1
fmn10_pca_5000_1000_2_2309391:
��,
fmn10_pca_5000_1000_2_2309393:	�1
fmn10_lle_5000_1000_3_2309396:
��,
fmn10_lle_5000_1000_3_2309398:	�4
 fmn10_isomap_5000_1000_3_2309401:
��/
 fmn10_isomap_5000_1000_3_2309403:	�2
fmn10_tsvd_5000_1000_3_2309406:
��-
fmn10_tsvd_5000_1000_3_2309408:	�1
fmn10_pca_5000_1000_3_2309411:
��,
fmn10_pca_5000_1000_3_2309413:	�2
fmn10_pca_5000_1000_out_2309416:	�-
fmn10_pca_5000_1000_out_2309418:3
 fmn10_tsvd_5000_1000_out_2309421:	�.
 fmn10_tsvd_5000_1000_out_2309423:5
"fmn10_isomap_5000_1000_out_2309426:	�0
"fmn10_isomap_5000_1000_out_2309428:2
fmn10_lle_5000_1000_out_2309431:	�-
fmn10_lle_5000_1000_out_2309433: 
joined_2309437:
joined_2309439:
identity��0fmn10_isomap_5000_1000_1/StatefulPartitionedCall�0fmn10_isomap_5000_1000_2/StatefulPartitionedCall�0fmn10_isomap_5000_1000_3/StatefulPartitionedCall�2fmn10_isomap_5000_1000_out/StatefulPartitionedCall�-fmn10_lle_5000_1000_1/StatefulPartitionedCall�-fmn10_lle_5000_1000_2/StatefulPartitionedCall�-fmn10_lle_5000_1000_3/StatefulPartitionedCall�/fmn10_lle_5000_1000_out/StatefulPartitionedCall�-fmn10_pca_5000_1000_1/StatefulPartitionedCall�-fmn10_pca_5000_1000_2/StatefulPartitionedCall�-fmn10_pca_5000_1000_3/StatefulPartitionedCall�/fmn10_pca_5000_1000_out/StatefulPartitionedCall�.fmn10_tsvd_5000_1000_1/StatefulPartitionedCall�.fmn10_tsvd_5000_1000_2/StatefulPartitionedCall�.fmn10_tsvd_5000_1000_3/StatefulPartitionedCall�0fmn10_tsvd_5000_1000_out/StatefulPartitionedCall�joined/StatefulPartitionedCall�
-fmn10_lle_5000_1000_1/StatefulPartitionedCallStatefulPartitionedCallfmn10_lle_5000_1000_inputfmn10_lle_5000_1000_1_2309356fmn10_lle_5000_1000_1_2309358*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_fmn10_lle_5000_1000_1_layer_call_and_return_conditional_losses_23083962/
-fmn10_lle_5000_1000_1/StatefulPartitionedCall�
0fmn10_isomap_5000_1000_1/StatefulPartitionedCallStatefulPartitionedCallfmn10_isomap_5000_1000_input fmn10_isomap_5000_1000_1_2309361 fmn10_isomap_5000_1000_1_2309363*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *^
fYRW
U__inference_fmn10_isomap_5000_1000_1_layer_call_and_return_conditional_losses_230841322
0fmn10_isomap_5000_1000_1/StatefulPartitionedCall�
.fmn10_tsvd_5000_1000_1/StatefulPartitionedCallStatefulPartitionedCallfmn10_tsvd_5000_1000_inputfmn10_tsvd_5000_1000_1_2309366fmn10_tsvd_5000_1000_1_2309368*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *\
fWRU
S__inference_fmn10_tsvd_5000_1000_1_layer_call_and_return_conditional_losses_230843020
.fmn10_tsvd_5000_1000_1/StatefulPartitionedCall�
-fmn10_pca_5000_1000_1/StatefulPartitionedCallStatefulPartitionedCallfmn10_pca_5000_1000_inputfmn10_pca_5000_1000_1_2309371fmn10_pca_5000_1000_1_2309373*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_fmn10_pca_5000_1000_1_layer_call_and_return_conditional_losses_23084472/
-fmn10_pca_5000_1000_1/StatefulPartitionedCall�
-fmn10_lle_5000_1000_2/StatefulPartitionedCallStatefulPartitionedCall6fmn10_lle_5000_1000_1/StatefulPartitionedCall:output:0fmn10_lle_5000_1000_2_2309376fmn10_lle_5000_1000_2_2309378*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_fmn10_lle_5000_1000_2_layer_call_and_return_conditional_losses_23084642/
-fmn10_lle_5000_1000_2/StatefulPartitionedCall�
0fmn10_isomap_5000_1000_2/StatefulPartitionedCallStatefulPartitionedCall9fmn10_isomap_5000_1000_1/StatefulPartitionedCall:output:0 fmn10_isomap_5000_1000_2_2309381 fmn10_isomap_5000_1000_2_2309383*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *^
fYRW
U__inference_fmn10_isomap_5000_1000_2_layer_call_and_return_conditional_losses_230848122
0fmn10_isomap_5000_1000_2/StatefulPartitionedCall�
.fmn10_tsvd_5000_1000_2/StatefulPartitionedCallStatefulPartitionedCall7fmn10_tsvd_5000_1000_1/StatefulPartitionedCall:output:0fmn10_tsvd_5000_1000_2_2309386fmn10_tsvd_5000_1000_2_2309388*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *\
fWRU
S__inference_fmn10_tsvd_5000_1000_2_layer_call_and_return_conditional_losses_230849820
.fmn10_tsvd_5000_1000_2/StatefulPartitionedCall�
-fmn10_pca_5000_1000_2/StatefulPartitionedCallStatefulPartitionedCall6fmn10_pca_5000_1000_1/StatefulPartitionedCall:output:0fmn10_pca_5000_1000_2_2309391fmn10_pca_5000_1000_2_2309393*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_fmn10_pca_5000_1000_2_layer_call_and_return_conditional_losses_23085152/
-fmn10_pca_5000_1000_2/StatefulPartitionedCall�
-fmn10_lle_5000_1000_3/StatefulPartitionedCallStatefulPartitionedCall6fmn10_lle_5000_1000_2/StatefulPartitionedCall:output:0fmn10_lle_5000_1000_3_2309396fmn10_lle_5000_1000_3_2309398*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_fmn10_lle_5000_1000_3_layer_call_and_return_conditional_losses_23085322/
-fmn10_lle_5000_1000_3/StatefulPartitionedCall�
0fmn10_isomap_5000_1000_3/StatefulPartitionedCallStatefulPartitionedCall9fmn10_isomap_5000_1000_2/StatefulPartitionedCall:output:0 fmn10_isomap_5000_1000_3_2309401 fmn10_isomap_5000_1000_3_2309403*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *^
fYRW
U__inference_fmn10_isomap_5000_1000_3_layer_call_and_return_conditional_losses_230854922
0fmn10_isomap_5000_1000_3/StatefulPartitionedCall�
.fmn10_tsvd_5000_1000_3/StatefulPartitionedCallStatefulPartitionedCall7fmn10_tsvd_5000_1000_2/StatefulPartitionedCall:output:0fmn10_tsvd_5000_1000_3_2309406fmn10_tsvd_5000_1000_3_2309408*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *\
fWRU
S__inference_fmn10_tsvd_5000_1000_3_layer_call_and_return_conditional_losses_230856620
.fmn10_tsvd_5000_1000_3/StatefulPartitionedCall�
-fmn10_pca_5000_1000_3/StatefulPartitionedCallStatefulPartitionedCall6fmn10_pca_5000_1000_2/StatefulPartitionedCall:output:0fmn10_pca_5000_1000_3_2309411fmn10_pca_5000_1000_3_2309413*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_fmn10_pca_5000_1000_3_layer_call_and_return_conditional_losses_23085832/
-fmn10_pca_5000_1000_3/StatefulPartitionedCall�
/fmn10_pca_5000_1000_out/StatefulPartitionedCallStatefulPartitionedCall6fmn10_pca_5000_1000_3/StatefulPartitionedCall:output:0fmn10_pca_5000_1000_out_2309416fmn10_pca_5000_1000_out_2309418*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *]
fXRV
T__inference_fmn10_pca_5000_1000_out_layer_call_and_return_conditional_losses_230860021
/fmn10_pca_5000_1000_out/StatefulPartitionedCall�
0fmn10_tsvd_5000_1000_out/StatefulPartitionedCallStatefulPartitionedCall7fmn10_tsvd_5000_1000_3/StatefulPartitionedCall:output:0 fmn10_tsvd_5000_1000_out_2309421 fmn10_tsvd_5000_1000_out_2309423*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *^
fYRW
U__inference_fmn10_tsvd_5000_1000_out_layer_call_and_return_conditional_losses_230861722
0fmn10_tsvd_5000_1000_out/StatefulPartitionedCall�
2fmn10_isomap_5000_1000_out/StatefulPartitionedCallStatefulPartitionedCall9fmn10_isomap_5000_1000_3/StatefulPartitionedCall:output:0"fmn10_isomap_5000_1000_out_2309426"fmn10_isomap_5000_1000_out_2309428*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *`
f[RY
W__inference_fmn10_isomap_5000_1000_out_layer_call_and_return_conditional_losses_230863424
2fmn10_isomap_5000_1000_out/StatefulPartitionedCall�
/fmn10_lle_5000_1000_out/StatefulPartitionedCallStatefulPartitionedCall6fmn10_lle_5000_1000_3/StatefulPartitionedCall:output:0fmn10_lle_5000_1000_out_2309431fmn10_lle_5000_1000_out_2309433*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *]
fXRV
T__inference_fmn10_lle_5000_1000_out_layer_call_and_return_conditional_losses_230865121
/fmn10_lle_5000_1000_out/StatefulPartitionedCall�
concatenate_68/PartitionedCallPartitionedCall8fmn10_pca_5000_1000_out/StatefulPartitionedCall:output:09fmn10_tsvd_5000_1000_out/StatefulPartitionedCall:output:0;fmn10_isomap_5000_1000_out/StatefulPartitionedCall:output:08fmn10_lle_5000_1000_out/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *T
fORM
K__inference_concatenate_68_layer_call_and_return_conditional_losses_23086662 
concatenate_68/PartitionedCall�
joined/StatefulPartitionedCallStatefulPartitionedCall'concatenate_68/PartitionedCall:output:0joined_2309437joined_2309439*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *L
fGRE
C__inference_joined_layer_call_and_return_conditional_losses_23086792 
joined/StatefulPartitionedCall�
IdentityIdentity'joined/StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identity�
NoOpNoOp1^fmn10_isomap_5000_1000_1/StatefulPartitionedCall1^fmn10_isomap_5000_1000_2/StatefulPartitionedCall1^fmn10_isomap_5000_1000_3/StatefulPartitionedCall3^fmn10_isomap_5000_1000_out/StatefulPartitionedCall.^fmn10_lle_5000_1000_1/StatefulPartitionedCall.^fmn10_lle_5000_1000_2/StatefulPartitionedCall.^fmn10_lle_5000_1000_3/StatefulPartitionedCall0^fmn10_lle_5000_1000_out/StatefulPartitionedCall.^fmn10_pca_5000_1000_1/StatefulPartitionedCall.^fmn10_pca_5000_1000_2/StatefulPartitionedCall.^fmn10_pca_5000_1000_3/StatefulPartitionedCall0^fmn10_pca_5000_1000_out/StatefulPartitionedCall/^fmn10_tsvd_5000_1000_1/StatefulPartitionedCall/^fmn10_tsvd_5000_1000_2/StatefulPartitionedCall/^fmn10_tsvd_5000_1000_3/StatefulPartitionedCall1^fmn10_tsvd_5000_1000_out/StatefulPartitionedCall^joined/StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*�
_input_shapes�
�:����������:����������:����������:����������: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 2d
0fmn10_isomap_5000_1000_1/StatefulPartitionedCall0fmn10_isomap_5000_1000_1/StatefulPartitionedCall2d
0fmn10_isomap_5000_1000_2/StatefulPartitionedCall0fmn10_isomap_5000_1000_2/StatefulPartitionedCall2d
0fmn10_isomap_5000_1000_3/StatefulPartitionedCall0fmn10_isomap_5000_1000_3/StatefulPartitionedCall2h
2fmn10_isomap_5000_1000_out/StatefulPartitionedCall2fmn10_isomap_5000_1000_out/StatefulPartitionedCall2^
-fmn10_lle_5000_1000_1/StatefulPartitionedCall-fmn10_lle_5000_1000_1/StatefulPartitionedCall2^
-fmn10_lle_5000_1000_2/StatefulPartitionedCall-fmn10_lle_5000_1000_2/StatefulPartitionedCall2^
-fmn10_lle_5000_1000_3/StatefulPartitionedCall-fmn10_lle_5000_1000_3/StatefulPartitionedCall2b
/fmn10_lle_5000_1000_out/StatefulPartitionedCall/fmn10_lle_5000_1000_out/StatefulPartitionedCall2^
-fmn10_pca_5000_1000_1/StatefulPartitionedCall-fmn10_pca_5000_1000_1/StatefulPartitionedCall2^
-fmn10_pca_5000_1000_2/StatefulPartitionedCall-fmn10_pca_5000_1000_2/StatefulPartitionedCall2^
-fmn10_pca_5000_1000_3/StatefulPartitionedCall-fmn10_pca_5000_1000_3/StatefulPartitionedCall2b
/fmn10_pca_5000_1000_out/StatefulPartitionedCall/fmn10_pca_5000_1000_out/StatefulPartitionedCall2`
.fmn10_tsvd_5000_1000_1/StatefulPartitionedCall.fmn10_tsvd_5000_1000_1/StatefulPartitionedCall2`
.fmn10_tsvd_5000_1000_2/StatefulPartitionedCall.fmn10_tsvd_5000_1000_2/StatefulPartitionedCall2`
.fmn10_tsvd_5000_1000_3/StatefulPartitionedCall.fmn10_tsvd_5000_1000_3/StatefulPartitionedCall2d
0fmn10_tsvd_5000_1000_out/StatefulPartitionedCall0fmn10_tsvd_5000_1000_out/StatefulPartitionedCall2@
joined/StatefulPartitionedCalljoined/StatefulPartitionedCall:c _
(
_output_shapes
:����������
3
_user_specified_namefmn10_pca_5000_1000_input:d`
(
_output_shapes
:����������
4
_user_specified_namefmn10_tsvd_5000_1000_input:fb
(
_output_shapes
:����������
6
_user_specified_namefmn10_isomap_5000_1000_input:c_
(
_output_shapes
:����������
3
_user_specified_namefmn10_lle_5000_1000_input
�w
�
B__inference_model_layer_call_and_return_conditional_losses_2308686

inputs
inputs_1
inputs_2
inputs_31
fmn10_lle_5000_1000_1_2308397:
��,
fmn10_lle_5000_1000_1_2308399:	�4
 fmn10_isomap_5000_1000_1_2308414:
��/
 fmn10_isomap_5000_1000_1_2308416:	�2
fmn10_tsvd_5000_1000_1_2308431:
��-
fmn10_tsvd_5000_1000_1_2308433:	�1
fmn10_pca_5000_1000_1_2308448:
��,
fmn10_pca_5000_1000_1_2308450:	�1
fmn10_lle_5000_1000_2_2308465:
��,
fmn10_lle_5000_1000_2_2308467:	�4
 fmn10_isomap_5000_1000_2_2308482:
��/
 fmn10_isomap_5000_1000_2_2308484:	�2
fmn10_tsvd_5000_1000_2_2308499:
��-
fmn10_tsvd_5000_1000_2_2308501:	�1
fmn10_pca_5000_1000_2_2308516:
��,
fmn10_pca_5000_1000_2_2308518:	�1
fmn10_lle_5000_1000_3_2308533:
��,
fmn10_lle_5000_1000_3_2308535:	�4
 fmn10_isomap_5000_1000_3_2308550:
��/
 fmn10_isomap_5000_1000_3_2308552:	�2
fmn10_tsvd_5000_1000_3_2308567:
��-
fmn10_tsvd_5000_1000_3_2308569:	�1
fmn10_pca_5000_1000_3_2308584:
��,
fmn10_pca_5000_1000_3_2308586:	�2
fmn10_pca_5000_1000_out_2308601:	�-
fmn10_pca_5000_1000_out_2308603:3
 fmn10_tsvd_5000_1000_out_2308618:	�.
 fmn10_tsvd_5000_1000_out_2308620:5
"fmn10_isomap_5000_1000_out_2308635:	�0
"fmn10_isomap_5000_1000_out_2308637:2
fmn10_lle_5000_1000_out_2308652:	�-
fmn10_lle_5000_1000_out_2308654: 
joined_2308680:
joined_2308682:
identity��0fmn10_isomap_5000_1000_1/StatefulPartitionedCall�0fmn10_isomap_5000_1000_2/StatefulPartitionedCall�0fmn10_isomap_5000_1000_3/StatefulPartitionedCall�2fmn10_isomap_5000_1000_out/StatefulPartitionedCall�-fmn10_lle_5000_1000_1/StatefulPartitionedCall�-fmn10_lle_5000_1000_2/StatefulPartitionedCall�-fmn10_lle_5000_1000_3/StatefulPartitionedCall�/fmn10_lle_5000_1000_out/StatefulPartitionedCall�-fmn10_pca_5000_1000_1/StatefulPartitionedCall�-fmn10_pca_5000_1000_2/StatefulPartitionedCall�-fmn10_pca_5000_1000_3/StatefulPartitionedCall�/fmn10_pca_5000_1000_out/StatefulPartitionedCall�.fmn10_tsvd_5000_1000_1/StatefulPartitionedCall�.fmn10_tsvd_5000_1000_2/StatefulPartitionedCall�.fmn10_tsvd_5000_1000_3/StatefulPartitionedCall�0fmn10_tsvd_5000_1000_out/StatefulPartitionedCall�joined/StatefulPartitionedCall�
-fmn10_lle_5000_1000_1/StatefulPartitionedCallStatefulPartitionedCallinputs_3fmn10_lle_5000_1000_1_2308397fmn10_lle_5000_1000_1_2308399*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_fmn10_lle_5000_1000_1_layer_call_and_return_conditional_losses_23083962/
-fmn10_lle_5000_1000_1/StatefulPartitionedCall�
0fmn10_isomap_5000_1000_1/StatefulPartitionedCallStatefulPartitionedCallinputs_2 fmn10_isomap_5000_1000_1_2308414 fmn10_isomap_5000_1000_1_2308416*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *^
fYRW
U__inference_fmn10_isomap_5000_1000_1_layer_call_and_return_conditional_losses_230841322
0fmn10_isomap_5000_1000_1/StatefulPartitionedCall�
.fmn10_tsvd_5000_1000_1/StatefulPartitionedCallStatefulPartitionedCallinputs_1fmn10_tsvd_5000_1000_1_2308431fmn10_tsvd_5000_1000_1_2308433*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *\
fWRU
S__inference_fmn10_tsvd_5000_1000_1_layer_call_and_return_conditional_losses_230843020
.fmn10_tsvd_5000_1000_1/StatefulPartitionedCall�
-fmn10_pca_5000_1000_1/StatefulPartitionedCallStatefulPartitionedCallinputsfmn10_pca_5000_1000_1_2308448fmn10_pca_5000_1000_1_2308450*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_fmn10_pca_5000_1000_1_layer_call_and_return_conditional_losses_23084472/
-fmn10_pca_5000_1000_1/StatefulPartitionedCall�
-fmn10_lle_5000_1000_2/StatefulPartitionedCallStatefulPartitionedCall6fmn10_lle_5000_1000_1/StatefulPartitionedCall:output:0fmn10_lle_5000_1000_2_2308465fmn10_lle_5000_1000_2_2308467*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_fmn10_lle_5000_1000_2_layer_call_and_return_conditional_losses_23084642/
-fmn10_lle_5000_1000_2/StatefulPartitionedCall�
0fmn10_isomap_5000_1000_2/StatefulPartitionedCallStatefulPartitionedCall9fmn10_isomap_5000_1000_1/StatefulPartitionedCall:output:0 fmn10_isomap_5000_1000_2_2308482 fmn10_isomap_5000_1000_2_2308484*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *^
fYRW
U__inference_fmn10_isomap_5000_1000_2_layer_call_and_return_conditional_losses_230848122
0fmn10_isomap_5000_1000_2/StatefulPartitionedCall�
.fmn10_tsvd_5000_1000_2/StatefulPartitionedCallStatefulPartitionedCall7fmn10_tsvd_5000_1000_1/StatefulPartitionedCall:output:0fmn10_tsvd_5000_1000_2_2308499fmn10_tsvd_5000_1000_2_2308501*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *\
fWRU
S__inference_fmn10_tsvd_5000_1000_2_layer_call_and_return_conditional_losses_230849820
.fmn10_tsvd_5000_1000_2/StatefulPartitionedCall�
-fmn10_pca_5000_1000_2/StatefulPartitionedCallStatefulPartitionedCall6fmn10_pca_5000_1000_1/StatefulPartitionedCall:output:0fmn10_pca_5000_1000_2_2308516fmn10_pca_5000_1000_2_2308518*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_fmn10_pca_5000_1000_2_layer_call_and_return_conditional_losses_23085152/
-fmn10_pca_5000_1000_2/StatefulPartitionedCall�
-fmn10_lle_5000_1000_3/StatefulPartitionedCallStatefulPartitionedCall6fmn10_lle_5000_1000_2/StatefulPartitionedCall:output:0fmn10_lle_5000_1000_3_2308533fmn10_lle_5000_1000_3_2308535*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_fmn10_lle_5000_1000_3_layer_call_and_return_conditional_losses_23085322/
-fmn10_lle_5000_1000_3/StatefulPartitionedCall�
0fmn10_isomap_5000_1000_3/StatefulPartitionedCallStatefulPartitionedCall9fmn10_isomap_5000_1000_2/StatefulPartitionedCall:output:0 fmn10_isomap_5000_1000_3_2308550 fmn10_isomap_5000_1000_3_2308552*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *^
fYRW
U__inference_fmn10_isomap_5000_1000_3_layer_call_and_return_conditional_losses_230854922
0fmn10_isomap_5000_1000_3/StatefulPartitionedCall�
.fmn10_tsvd_5000_1000_3/StatefulPartitionedCallStatefulPartitionedCall7fmn10_tsvd_5000_1000_2/StatefulPartitionedCall:output:0fmn10_tsvd_5000_1000_3_2308567fmn10_tsvd_5000_1000_3_2308569*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *\
fWRU
S__inference_fmn10_tsvd_5000_1000_3_layer_call_and_return_conditional_losses_230856620
.fmn10_tsvd_5000_1000_3/StatefulPartitionedCall�
-fmn10_pca_5000_1000_3/StatefulPartitionedCallStatefulPartitionedCall6fmn10_pca_5000_1000_2/StatefulPartitionedCall:output:0fmn10_pca_5000_1000_3_2308584fmn10_pca_5000_1000_3_2308586*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_fmn10_pca_5000_1000_3_layer_call_and_return_conditional_losses_23085832/
-fmn10_pca_5000_1000_3/StatefulPartitionedCall�
/fmn10_pca_5000_1000_out/StatefulPartitionedCallStatefulPartitionedCall6fmn10_pca_5000_1000_3/StatefulPartitionedCall:output:0fmn10_pca_5000_1000_out_2308601fmn10_pca_5000_1000_out_2308603*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *]
fXRV
T__inference_fmn10_pca_5000_1000_out_layer_call_and_return_conditional_losses_230860021
/fmn10_pca_5000_1000_out/StatefulPartitionedCall�
0fmn10_tsvd_5000_1000_out/StatefulPartitionedCallStatefulPartitionedCall7fmn10_tsvd_5000_1000_3/StatefulPartitionedCall:output:0 fmn10_tsvd_5000_1000_out_2308618 fmn10_tsvd_5000_1000_out_2308620*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *^
fYRW
U__inference_fmn10_tsvd_5000_1000_out_layer_call_and_return_conditional_losses_230861722
0fmn10_tsvd_5000_1000_out/StatefulPartitionedCall�
2fmn10_isomap_5000_1000_out/StatefulPartitionedCallStatefulPartitionedCall9fmn10_isomap_5000_1000_3/StatefulPartitionedCall:output:0"fmn10_isomap_5000_1000_out_2308635"fmn10_isomap_5000_1000_out_2308637*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *`
f[RY
W__inference_fmn10_isomap_5000_1000_out_layer_call_and_return_conditional_losses_230863424
2fmn10_isomap_5000_1000_out/StatefulPartitionedCall�
/fmn10_lle_5000_1000_out/StatefulPartitionedCallStatefulPartitionedCall6fmn10_lle_5000_1000_3/StatefulPartitionedCall:output:0fmn10_lle_5000_1000_out_2308652fmn10_lle_5000_1000_out_2308654*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *]
fXRV
T__inference_fmn10_lle_5000_1000_out_layer_call_and_return_conditional_losses_230865121
/fmn10_lle_5000_1000_out/StatefulPartitionedCall�
concatenate_68/PartitionedCallPartitionedCall8fmn10_pca_5000_1000_out/StatefulPartitionedCall:output:09fmn10_tsvd_5000_1000_out/StatefulPartitionedCall:output:0;fmn10_isomap_5000_1000_out/StatefulPartitionedCall:output:08fmn10_lle_5000_1000_out/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *T
fORM
K__inference_concatenate_68_layer_call_and_return_conditional_losses_23086662 
concatenate_68/PartitionedCall�
joined/StatefulPartitionedCallStatefulPartitionedCall'concatenate_68/PartitionedCall:output:0joined_2308680joined_2308682*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *L
fGRE
C__inference_joined_layer_call_and_return_conditional_losses_23086792 
joined/StatefulPartitionedCall�
IdentityIdentity'joined/StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identity�
NoOpNoOp1^fmn10_isomap_5000_1000_1/StatefulPartitionedCall1^fmn10_isomap_5000_1000_2/StatefulPartitionedCall1^fmn10_isomap_5000_1000_3/StatefulPartitionedCall3^fmn10_isomap_5000_1000_out/StatefulPartitionedCall.^fmn10_lle_5000_1000_1/StatefulPartitionedCall.^fmn10_lle_5000_1000_2/StatefulPartitionedCall.^fmn10_lle_5000_1000_3/StatefulPartitionedCall0^fmn10_lle_5000_1000_out/StatefulPartitionedCall.^fmn10_pca_5000_1000_1/StatefulPartitionedCall.^fmn10_pca_5000_1000_2/StatefulPartitionedCall.^fmn10_pca_5000_1000_3/StatefulPartitionedCall0^fmn10_pca_5000_1000_out/StatefulPartitionedCall/^fmn10_tsvd_5000_1000_1/StatefulPartitionedCall/^fmn10_tsvd_5000_1000_2/StatefulPartitionedCall/^fmn10_tsvd_5000_1000_3/StatefulPartitionedCall1^fmn10_tsvd_5000_1000_out/StatefulPartitionedCall^joined/StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*�
_input_shapes�
�:����������:����������:����������:����������: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 2d
0fmn10_isomap_5000_1000_1/StatefulPartitionedCall0fmn10_isomap_5000_1000_1/StatefulPartitionedCall2d
0fmn10_isomap_5000_1000_2/StatefulPartitionedCall0fmn10_isomap_5000_1000_2/StatefulPartitionedCall2d
0fmn10_isomap_5000_1000_3/StatefulPartitionedCall0fmn10_isomap_5000_1000_3/StatefulPartitionedCall2h
2fmn10_isomap_5000_1000_out/StatefulPartitionedCall2fmn10_isomap_5000_1000_out/StatefulPartitionedCall2^
-fmn10_lle_5000_1000_1/StatefulPartitionedCall-fmn10_lle_5000_1000_1/StatefulPartitionedCall2^
-fmn10_lle_5000_1000_2/StatefulPartitionedCall-fmn10_lle_5000_1000_2/StatefulPartitionedCall2^
-fmn10_lle_5000_1000_3/StatefulPartitionedCall-fmn10_lle_5000_1000_3/StatefulPartitionedCall2b
/fmn10_lle_5000_1000_out/StatefulPartitionedCall/fmn10_lle_5000_1000_out/StatefulPartitionedCall2^
-fmn10_pca_5000_1000_1/StatefulPartitionedCall-fmn10_pca_5000_1000_1/StatefulPartitionedCall2^
-fmn10_pca_5000_1000_2/StatefulPartitionedCall-fmn10_pca_5000_1000_2/StatefulPartitionedCall2^
-fmn10_pca_5000_1000_3/StatefulPartitionedCall-fmn10_pca_5000_1000_3/StatefulPartitionedCall2b
/fmn10_pca_5000_1000_out/StatefulPartitionedCall/fmn10_pca_5000_1000_out/StatefulPartitionedCall2`
.fmn10_tsvd_5000_1000_1/StatefulPartitionedCall.fmn10_tsvd_5000_1000_1/StatefulPartitionedCall2`
.fmn10_tsvd_5000_1000_2/StatefulPartitionedCall.fmn10_tsvd_5000_1000_2/StatefulPartitionedCall2`
.fmn10_tsvd_5000_1000_3/StatefulPartitionedCall.fmn10_tsvd_5000_1000_3/StatefulPartitionedCall2d
0fmn10_tsvd_5000_1000_out/StatefulPartitionedCall0fmn10_tsvd_5000_1000_out/StatefulPartitionedCall2@
joined/StatefulPartitionedCalljoined/StatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs:PL
(
_output_shapes
:����������
 
_user_specified_nameinputs:PL
(
_output_shapes
:����������
 
_user_specified_nameinputs:PL
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
U__inference_fmn10_isomap_5000_1000_3_layer_call_and_return_conditional_losses_2310155

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
:__inference_fmn10_tsvd_5000_1000_out_layer_call_fn_2310204

inputs
unknown:	�
	unknown_0:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *^
fYRW
U__inference_fmn10_tsvd_5000_1000_out_layer_call_and_return_conditional_losses_23086172
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
7__inference_fmn10_lle_5000_1000_2_layer_call_fn_2310084

inputs
unknown:
��
	unknown_0:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_fmn10_lle_5000_1000_2_layer_call_and_return_conditional_losses_23084642
StatefulPartitionedCall|
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:����������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
<__inference_fmn10_isomap_5000_1000_out_layer_call_fn_2310224

inputs
unknown:	�
	unknown_0:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *`
f[RY
W__inference_fmn10_isomap_5000_1000_out_layer_call_and_return_conditional_losses_23086342
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
9__inference_fmn10_lle_5000_1000_out_layer_call_fn_2310244

inputs
unknown:	�
	unknown_0:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *]
fXRV
T__inference_fmn10_lle_5000_1000_out_layer_call_and_return_conditional_losses_23086512
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
��
�$
"__inference__wrapped_model_2308372
fmn10_pca_5000_1000_input
fmn10_tsvd_5000_1000_input 
fmn10_isomap_5000_1000_input
fmn10_lle_5000_1000_inputN
:model_fmn10_lle_5000_1000_1_matmul_readvariableop_resource:
��J
;model_fmn10_lle_5000_1000_1_biasadd_readvariableop_resource:	�Q
=model_fmn10_isomap_5000_1000_1_matmul_readvariableop_resource:
��M
>model_fmn10_isomap_5000_1000_1_biasadd_readvariableop_resource:	�O
;model_fmn10_tsvd_5000_1000_1_matmul_readvariableop_resource:
��K
<model_fmn10_tsvd_5000_1000_1_biasadd_readvariableop_resource:	�N
:model_fmn10_pca_5000_1000_1_matmul_readvariableop_resource:
��J
;model_fmn10_pca_5000_1000_1_biasadd_readvariableop_resource:	�N
:model_fmn10_lle_5000_1000_2_matmul_readvariableop_resource:
��J
;model_fmn10_lle_5000_1000_2_biasadd_readvariableop_resource:	�Q
=model_fmn10_isomap_5000_1000_2_matmul_readvariableop_resource:
��M
>model_fmn10_isomap_5000_1000_2_biasadd_readvariableop_resource:	�O
;model_fmn10_tsvd_5000_1000_2_matmul_readvariableop_resource:
��K
<model_fmn10_tsvd_5000_1000_2_biasadd_readvariableop_resource:	�N
:model_fmn10_pca_5000_1000_2_matmul_readvariableop_resource:
��J
;model_fmn10_pca_5000_1000_2_biasadd_readvariableop_resource:	�N
:model_fmn10_lle_5000_1000_3_matmul_readvariableop_resource:
��J
;model_fmn10_lle_5000_1000_3_biasadd_readvariableop_resource:	�Q
=model_fmn10_isomap_5000_1000_3_matmul_readvariableop_resource:
��M
>model_fmn10_isomap_5000_1000_3_biasadd_readvariableop_resource:	�O
;model_fmn10_tsvd_5000_1000_3_matmul_readvariableop_resource:
��K
<model_fmn10_tsvd_5000_1000_3_biasadd_readvariableop_resource:	�N
:model_fmn10_pca_5000_1000_3_matmul_readvariableop_resource:
��J
;model_fmn10_pca_5000_1000_3_biasadd_readvariableop_resource:	�O
<model_fmn10_pca_5000_1000_out_matmul_readvariableop_resource:	�K
=model_fmn10_pca_5000_1000_out_biasadd_readvariableop_resource:P
=model_fmn10_tsvd_5000_1000_out_matmul_readvariableop_resource:	�L
>model_fmn10_tsvd_5000_1000_out_biasadd_readvariableop_resource:R
?model_fmn10_isomap_5000_1000_out_matmul_readvariableop_resource:	�N
@model_fmn10_isomap_5000_1000_out_biasadd_readvariableop_resource:O
<model_fmn10_lle_5000_1000_out_matmul_readvariableop_resource:	�K
=model_fmn10_lle_5000_1000_out_biasadd_readvariableop_resource:=
+model_joined_matmul_readvariableop_resource::
,model_joined_biasadd_readvariableop_resource:
identity��5model/fmn10_isomap_5000_1000_1/BiasAdd/ReadVariableOp�4model/fmn10_isomap_5000_1000_1/MatMul/ReadVariableOp�5model/fmn10_isomap_5000_1000_2/BiasAdd/ReadVariableOp�4model/fmn10_isomap_5000_1000_2/MatMul/ReadVariableOp�5model/fmn10_isomap_5000_1000_3/BiasAdd/ReadVariableOp�4model/fmn10_isomap_5000_1000_3/MatMul/ReadVariableOp�7model/fmn10_isomap_5000_1000_out/BiasAdd/ReadVariableOp�6model/fmn10_isomap_5000_1000_out/MatMul/ReadVariableOp�2model/fmn10_lle_5000_1000_1/BiasAdd/ReadVariableOp�1model/fmn10_lle_5000_1000_1/MatMul/ReadVariableOp�2model/fmn10_lle_5000_1000_2/BiasAdd/ReadVariableOp�1model/fmn10_lle_5000_1000_2/MatMul/ReadVariableOp�2model/fmn10_lle_5000_1000_3/BiasAdd/ReadVariableOp�1model/fmn10_lle_5000_1000_3/MatMul/ReadVariableOp�4model/fmn10_lle_5000_1000_out/BiasAdd/ReadVariableOp�3model/fmn10_lle_5000_1000_out/MatMul/ReadVariableOp�2model/fmn10_pca_5000_1000_1/BiasAdd/ReadVariableOp�1model/fmn10_pca_5000_1000_1/MatMul/ReadVariableOp�2model/fmn10_pca_5000_1000_2/BiasAdd/ReadVariableOp�1model/fmn10_pca_5000_1000_2/MatMul/ReadVariableOp�2model/fmn10_pca_5000_1000_3/BiasAdd/ReadVariableOp�1model/fmn10_pca_5000_1000_3/MatMul/ReadVariableOp�4model/fmn10_pca_5000_1000_out/BiasAdd/ReadVariableOp�3model/fmn10_pca_5000_1000_out/MatMul/ReadVariableOp�3model/fmn10_tsvd_5000_1000_1/BiasAdd/ReadVariableOp�2model/fmn10_tsvd_5000_1000_1/MatMul/ReadVariableOp�3model/fmn10_tsvd_5000_1000_2/BiasAdd/ReadVariableOp�2model/fmn10_tsvd_5000_1000_2/MatMul/ReadVariableOp�3model/fmn10_tsvd_5000_1000_3/BiasAdd/ReadVariableOp�2model/fmn10_tsvd_5000_1000_3/MatMul/ReadVariableOp�5model/fmn10_tsvd_5000_1000_out/BiasAdd/ReadVariableOp�4model/fmn10_tsvd_5000_1000_out/MatMul/ReadVariableOp�#model/joined/BiasAdd/ReadVariableOp�"model/joined/MatMul/ReadVariableOp�
1model/fmn10_lle_5000_1000_1/MatMul/ReadVariableOpReadVariableOp:model_fmn10_lle_5000_1000_1_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype023
1model/fmn10_lle_5000_1000_1/MatMul/ReadVariableOp�
"model/fmn10_lle_5000_1000_1/MatMulMatMulfmn10_lle_5000_1000_input9model/fmn10_lle_5000_1000_1/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2$
"model/fmn10_lle_5000_1000_1/MatMul�
2model/fmn10_lle_5000_1000_1/BiasAdd/ReadVariableOpReadVariableOp;model_fmn10_lle_5000_1000_1_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype024
2model/fmn10_lle_5000_1000_1/BiasAdd/ReadVariableOp�
#model/fmn10_lle_5000_1000_1/BiasAddBiasAdd,model/fmn10_lle_5000_1000_1/MatMul:product:0:model/fmn10_lle_5000_1000_1/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2%
#model/fmn10_lle_5000_1000_1/BiasAdd�
 model/fmn10_lle_5000_1000_1/ReluRelu,model/fmn10_lle_5000_1000_1/BiasAdd:output:0*
T0*(
_output_shapes
:����������2"
 model/fmn10_lle_5000_1000_1/Relu�
4model/fmn10_isomap_5000_1000_1/MatMul/ReadVariableOpReadVariableOp=model_fmn10_isomap_5000_1000_1_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype026
4model/fmn10_isomap_5000_1000_1/MatMul/ReadVariableOp�
%model/fmn10_isomap_5000_1000_1/MatMulMatMulfmn10_isomap_5000_1000_input<model/fmn10_isomap_5000_1000_1/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2'
%model/fmn10_isomap_5000_1000_1/MatMul�
5model/fmn10_isomap_5000_1000_1/BiasAdd/ReadVariableOpReadVariableOp>model_fmn10_isomap_5000_1000_1_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype027
5model/fmn10_isomap_5000_1000_1/BiasAdd/ReadVariableOp�
&model/fmn10_isomap_5000_1000_1/BiasAddBiasAdd/model/fmn10_isomap_5000_1000_1/MatMul:product:0=model/fmn10_isomap_5000_1000_1/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2(
&model/fmn10_isomap_5000_1000_1/BiasAdd�
#model/fmn10_isomap_5000_1000_1/ReluRelu/model/fmn10_isomap_5000_1000_1/BiasAdd:output:0*
T0*(
_output_shapes
:����������2%
#model/fmn10_isomap_5000_1000_1/Relu�
2model/fmn10_tsvd_5000_1000_1/MatMul/ReadVariableOpReadVariableOp;model_fmn10_tsvd_5000_1000_1_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype024
2model/fmn10_tsvd_5000_1000_1/MatMul/ReadVariableOp�
#model/fmn10_tsvd_5000_1000_1/MatMulMatMulfmn10_tsvd_5000_1000_input:model/fmn10_tsvd_5000_1000_1/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2%
#model/fmn10_tsvd_5000_1000_1/MatMul�
3model/fmn10_tsvd_5000_1000_1/BiasAdd/ReadVariableOpReadVariableOp<model_fmn10_tsvd_5000_1000_1_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype025
3model/fmn10_tsvd_5000_1000_1/BiasAdd/ReadVariableOp�
$model/fmn10_tsvd_5000_1000_1/BiasAddBiasAdd-model/fmn10_tsvd_5000_1000_1/MatMul:product:0;model/fmn10_tsvd_5000_1000_1/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2&
$model/fmn10_tsvd_5000_1000_1/BiasAdd�
!model/fmn10_tsvd_5000_1000_1/ReluRelu-model/fmn10_tsvd_5000_1000_1/BiasAdd:output:0*
T0*(
_output_shapes
:����������2#
!model/fmn10_tsvd_5000_1000_1/Relu�
1model/fmn10_pca_5000_1000_1/MatMul/ReadVariableOpReadVariableOp:model_fmn10_pca_5000_1000_1_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype023
1model/fmn10_pca_5000_1000_1/MatMul/ReadVariableOp�
"model/fmn10_pca_5000_1000_1/MatMulMatMulfmn10_pca_5000_1000_input9model/fmn10_pca_5000_1000_1/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2$
"model/fmn10_pca_5000_1000_1/MatMul�
2model/fmn10_pca_5000_1000_1/BiasAdd/ReadVariableOpReadVariableOp;model_fmn10_pca_5000_1000_1_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype024
2model/fmn10_pca_5000_1000_1/BiasAdd/ReadVariableOp�
#model/fmn10_pca_5000_1000_1/BiasAddBiasAdd,model/fmn10_pca_5000_1000_1/MatMul:product:0:model/fmn10_pca_5000_1000_1/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2%
#model/fmn10_pca_5000_1000_1/BiasAdd�
 model/fmn10_pca_5000_1000_1/ReluRelu,model/fmn10_pca_5000_1000_1/BiasAdd:output:0*
T0*(
_output_shapes
:����������2"
 model/fmn10_pca_5000_1000_1/Relu�
1model/fmn10_lle_5000_1000_2/MatMul/ReadVariableOpReadVariableOp:model_fmn10_lle_5000_1000_2_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype023
1model/fmn10_lle_5000_1000_2/MatMul/ReadVariableOp�
"model/fmn10_lle_5000_1000_2/MatMulMatMul.model/fmn10_lle_5000_1000_1/Relu:activations:09model/fmn10_lle_5000_1000_2/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2$
"model/fmn10_lle_5000_1000_2/MatMul�
2model/fmn10_lle_5000_1000_2/BiasAdd/ReadVariableOpReadVariableOp;model_fmn10_lle_5000_1000_2_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype024
2model/fmn10_lle_5000_1000_2/BiasAdd/ReadVariableOp�
#model/fmn10_lle_5000_1000_2/BiasAddBiasAdd,model/fmn10_lle_5000_1000_2/MatMul:product:0:model/fmn10_lle_5000_1000_2/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2%
#model/fmn10_lle_5000_1000_2/BiasAdd�
 model/fmn10_lle_5000_1000_2/ReluRelu,model/fmn10_lle_5000_1000_2/BiasAdd:output:0*
T0*(
_output_shapes
:����������2"
 model/fmn10_lle_5000_1000_2/Relu�
4model/fmn10_isomap_5000_1000_2/MatMul/ReadVariableOpReadVariableOp=model_fmn10_isomap_5000_1000_2_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype026
4model/fmn10_isomap_5000_1000_2/MatMul/ReadVariableOp�
%model/fmn10_isomap_5000_1000_2/MatMulMatMul1model/fmn10_isomap_5000_1000_1/Relu:activations:0<model/fmn10_isomap_5000_1000_2/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2'
%model/fmn10_isomap_5000_1000_2/MatMul�
5model/fmn10_isomap_5000_1000_2/BiasAdd/ReadVariableOpReadVariableOp>model_fmn10_isomap_5000_1000_2_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype027
5model/fmn10_isomap_5000_1000_2/BiasAdd/ReadVariableOp�
&model/fmn10_isomap_5000_1000_2/BiasAddBiasAdd/model/fmn10_isomap_5000_1000_2/MatMul:product:0=model/fmn10_isomap_5000_1000_2/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2(
&model/fmn10_isomap_5000_1000_2/BiasAdd�
#model/fmn10_isomap_5000_1000_2/ReluRelu/model/fmn10_isomap_5000_1000_2/BiasAdd:output:0*
T0*(
_output_shapes
:����������2%
#model/fmn10_isomap_5000_1000_2/Relu�
2model/fmn10_tsvd_5000_1000_2/MatMul/ReadVariableOpReadVariableOp;model_fmn10_tsvd_5000_1000_2_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype024
2model/fmn10_tsvd_5000_1000_2/MatMul/ReadVariableOp�
#model/fmn10_tsvd_5000_1000_2/MatMulMatMul/model/fmn10_tsvd_5000_1000_1/Relu:activations:0:model/fmn10_tsvd_5000_1000_2/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2%
#model/fmn10_tsvd_5000_1000_2/MatMul�
3model/fmn10_tsvd_5000_1000_2/BiasAdd/ReadVariableOpReadVariableOp<model_fmn10_tsvd_5000_1000_2_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype025
3model/fmn10_tsvd_5000_1000_2/BiasAdd/ReadVariableOp�
$model/fmn10_tsvd_5000_1000_2/BiasAddBiasAdd-model/fmn10_tsvd_5000_1000_2/MatMul:product:0;model/fmn10_tsvd_5000_1000_2/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2&
$model/fmn10_tsvd_5000_1000_2/BiasAdd�
!model/fmn10_tsvd_5000_1000_2/ReluRelu-model/fmn10_tsvd_5000_1000_2/BiasAdd:output:0*
T0*(
_output_shapes
:����������2#
!model/fmn10_tsvd_5000_1000_2/Relu�
1model/fmn10_pca_5000_1000_2/MatMul/ReadVariableOpReadVariableOp:model_fmn10_pca_5000_1000_2_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype023
1model/fmn10_pca_5000_1000_2/MatMul/ReadVariableOp�
"model/fmn10_pca_5000_1000_2/MatMulMatMul.model/fmn10_pca_5000_1000_1/Relu:activations:09model/fmn10_pca_5000_1000_2/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2$
"model/fmn10_pca_5000_1000_2/MatMul�
2model/fmn10_pca_5000_1000_2/BiasAdd/ReadVariableOpReadVariableOp;model_fmn10_pca_5000_1000_2_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype024
2model/fmn10_pca_5000_1000_2/BiasAdd/ReadVariableOp�
#model/fmn10_pca_5000_1000_2/BiasAddBiasAdd,model/fmn10_pca_5000_1000_2/MatMul:product:0:model/fmn10_pca_5000_1000_2/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2%
#model/fmn10_pca_5000_1000_2/BiasAdd�
 model/fmn10_pca_5000_1000_2/ReluRelu,model/fmn10_pca_5000_1000_2/BiasAdd:output:0*
T0*(
_output_shapes
:����������2"
 model/fmn10_pca_5000_1000_2/Relu�
1model/fmn10_lle_5000_1000_3/MatMul/ReadVariableOpReadVariableOp:model_fmn10_lle_5000_1000_3_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype023
1model/fmn10_lle_5000_1000_3/MatMul/ReadVariableOp�
"model/fmn10_lle_5000_1000_3/MatMulMatMul.model/fmn10_lle_5000_1000_2/Relu:activations:09model/fmn10_lle_5000_1000_3/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2$
"model/fmn10_lle_5000_1000_3/MatMul�
2model/fmn10_lle_5000_1000_3/BiasAdd/ReadVariableOpReadVariableOp;model_fmn10_lle_5000_1000_3_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype024
2model/fmn10_lle_5000_1000_3/BiasAdd/ReadVariableOp�
#model/fmn10_lle_5000_1000_3/BiasAddBiasAdd,model/fmn10_lle_5000_1000_3/MatMul:product:0:model/fmn10_lle_5000_1000_3/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2%
#model/fmn10_lle_5000_1000_3/BiasAdd�
 model/fmn10_lle_5000_1000_3/ReluRelu,model/fmn10_lle_5000_1000_3/BiasAdd:output:0*
T0*(
_output_shapes
:����������2"
 model/fmn10_lle_5000_1000_3/Relu�
4model/fmn10_isomap_5000_1000_3/MatMul/ReadVariableOpReadVariableOp=model_fmn10_isomap_5000_1000_3_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype026
4model/fmn10_isomap_5000_1000_3/MatMul/ReadVariableOp�
%model/fmn10_isomap_5000_1000_3/MatMulMatMul1model/fmn10_isomap_5000_1000_2/Relu:activations:0<model/fmn10_isomap_5000_1000_3/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2'
%model/fmn10_isomap_5000_1000_3/MatMul�
5model/fmn10_isomap_5000_1000_3/BiasAdd/ReadVariableOpReadVariableOp>model_fmn10_isomap_5000_1000_3_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype027
5model/fmn10_isomap_5000_1000_3/BiasAdd/ReadVariableOp�
&model/fmn10_isomap_5000_1000_3/BiasAddBiasAdd/model/fmn10_isomap_5000_1000_3/MatMul:product:0=model/fmn10_isomap_5000_1000_3/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2(
&model/fmn10_isomap_5000_1000_3/BiasAdd�
#model/fmn10_isomap_5000_1000_3/ReluRelu/model/fmn10_isomap_5000_1000_3/BiasAdd:output:0*
T0*(
_output_shapes
:����������2%
#model/fmn10_isomap_5000_1000_3/Relu�
2model/fmn10_tsvd_5000_1000_3/MatMul/ReadVariableOpReadVariableOp;model_fmn10_tsvd_5000_1000_3_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype024
2model/fmn10_tsvd_5000_1000_3/MatMul/ReadVariableOp�
#model/fmn10_tsvd_5000_1000_3/MatMulMatMul/model/fmn10_tsvd_5000_1000_2/Relu:activations:0:model/fmn10_tsvd_5000_1000_3/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2%
#model/fmn10_tsvd_5000_1000_3/MatMul�
3model/fmn10_tsvd_5000_1000_3/BiasAdd/ReadVariableOpReadVariableOp<model_fmn10_tsvd_5000_1000_3_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype025
3model/fmn10_tsvd_5000_1000_3/BiasAdd/ReadVariableOp�
$model/fmn10_tsvd_5000_1000_3/BiasAddBiasAdd-model/fmn10_tsvd_5000_1000_3/MatMul:product:0;model/fmn10_tsvd_5000_1000_3/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2&
$model/fmn10_tsvd_5000_1000_3/BiasAdd�
!model/fmn10_tsvd_5000_1000_3/ReluRelu-model/fmn10_tsvd_5000_1000_3/BiasAdd:output:0*
T0*(
_output_shapes
:����������2#
!model/fmn10_tsvd_5000_1000_3/Relu�
1model/fmn10_pca_5000_1000_3/MatMul/ReadVariableOpReadVariableOp:model_fmn10_pca_5000_1000_3_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype023
1model/fmn10_pca_5000_1000_3/MatMul/ReadVariableOp�
"model/fmn10_pca_5000_1000_3/MatMulMatMul.model/fmn10_pca_5000_1000_2/Relu:activations:09model/fmn10_pca_5000_1000_3/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2$
"model/fmn10_pca_5000_1000_3/MatMul�
2model/fmn10_pca_5000_1000_3/BiasAdd/ReadVariableOpReadVariableOp;model_fmn10_pca_5000_1000_3_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype024
2model/fmn10_pca_5000_1000_3/BiasAdd/ReadVariableOp�
#model/fmn10_pca_5000_1000_3/BiasAddBiasAdd,model/fmn10_pca_5000_1000_3/MatMul:product:0:model/fmn10_pca_5000_1000_3/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2%
#model/fmn10_pca_5000_1000_3/BiasAdd�
 model/fmn10_pca_5000_1000_3/ReluRelu,model/fmn10_pca_5000_1000_3/BiasAdd:output:0*
T0*(
_output_shapes
:����������2"
 model/fmn10_pca_5000_1000_3/Relu�
3model/fmn10_pca_5000_1000_out/MatMul/ReadVariableOpReadVariableOp<model_fmn10_pca_5000_1000_out_matmul_readvariableop_resource*
_output_shapes
:	�*
dtype025
3model/fmn10_pca_5000_1000_out/MatMul/ReadVariableOp�
$model/fmn10_pca_5000_1000_out/MatMulMatMul.model/fmn10_pca_5000_1000_3/Relu:activations:0;model/fmn10_pca_5000_1000_out/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2&
$model/fmn10_pca_5000_1000_out/MatMul�
4model/fmn10_pca_5000_1000_out/BiasAdd/ReadVariableOpReadVariableOp=model_fmn10_pca_5000_1000_out_biasadd_readvariableop_resource*
_output_shapes
:*
dtype026
4model/fmn10_pca_5000_1000_out/BiasAdd/ReadVariableOp�
%model/fmn10_pca_5000_1000_out/BiasAddBiasAdd.model/fmn10_pca_5000_1000_out/MatMul:product:0<model/fmn10_pca_5000_1000_out/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2'
%model/fmn10_pca_5000_1000_out/BiasAdd�
%model/fmn10_pca_5000_1000_out/SigmoidSigmoid.model/fmn10_pca_5000_1000_out/BiasAdd:output:0*
T0*'
_output_shapes
:���������2'
%model/fmn10_pca_5000_1000_out/Sigmoid�
4model/fmn10_tsvd_5000_1000_out/MatMul/ReadVariableOpReadVariableOp=model_fmn10_tsvd_5000_1000_out_matmul_readvariableop_resource*
_output_shapes
:	�*
dtype026
4model/fmn10_tsvd_5000_1000_out/MatMul/ReadVariableOp�
%model/fmn10_tsvd_5000_1000_out/MatMulMatMul/model/fmn10_tsvd_5000_1000_3/Relu:activations:0<model/fmn10_tsvd_5000_1000_out/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2'
%model/fmn10_tsvd_5000_1000_out/MatMul�
5model/fmn10_tsvd_5000_1000_out/BiasAdd/ReadVariableOpReadVariableOp>model_fmn10_tsvd_5000_1000_out_biasadd_readvariableop_resource*
_output_shapes
:*
dtype027
5model/fmn10_tsvd_5000_1000_out/BiasAdd/ReadVariableOp�
&model/fmn10_tsvd_5000_1000_out/BiasAddBiasAdd/model/fmn10_tsvd_5000_1000_out/MatMul:product:0=model/fmn10_tsvd_5000_1000_out/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2(
&model/fmn10_tsvd_5000_1000_out/BiasAdd�
&model/fmn10_tsvd_5000_1000_out/SigmoidSigmoid/model/fmn10_tsvd_5000_1000_out/BiasAdd:output:0*
T0*'
_output_shapes
:���������2(
&model/fmn10_tsvd_5000_1000_out/Sigmoid�
6model/fmn10_isomap_5000_1000_out/MatMul/ReadVariableOpReadVariableOp?model_fmn10_isomap_5000_1000_out_matmul_readvariableop_resource*
_output_shapes
:	�*
dtype028
6model/fmn10_isomap_5000_1000_out/MatMul/ReadVariableOp�
'model/fmn10_isomap_5000_1000_out/MatMulMatMul1model/fmn10_isomap_5000_1000_3/Relu:activations:0>model/fmn10_isomap_5000_1000_out/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2)
'model/fmn10_isomap_5000_1000_out/MatMul�
7model/fmn10_isomap_5000_1000_out/BiasAdd/ReadVariableOpReadVariableOp@model_fmn10_isomap_5000_1000_out_biasadd_readvariableop_resource*
_output_shapes
:*
dtype029
7model/fmn10_isomap_5000_1000_out/BiasAdd/ReadVariableOp�
(model/fmn10_isomap_5000_1000_out/BiasAddBiasAdd1model/fmn10_isomap_5000_1000_out/MatMul:product:0?model/fmn10_isomap_5000_1000_out/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2*
(model/fmn10_isomap_5000_1000_out/BiasAdd�
(model/fmn10_isomap_5000_1000_out/SigmoidSigmoid1model/fmn10_isomap_5000_1000_out/BiasAdd:output:0*
T0*'
_output_shapes
:���������2*
(model/fmn10_isomap_5000_1000_out/Sigmoid�
3model/fmn10_lle_5000_1000_out/MatMul/ReadVariableOpReadVariableOp<model_fmn10_lle_5000_1000_out_matmul_readvariableop_resource*
_output_shapes
:	�*
dtype025
3model/fmn10_lle_5000_1000_out/MatMul/ReadVariableOp�
$model/fmn10_lle_5000_1000_out/MatMulMatMul.model/fmn10_lle_5000_1000_3/Relu:activations:0;model/fmn10_lle_5000_1000_out/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2&
$model/fmn10_lle_5000_1000_out/MatMul�
4model/fmn10_lle_5000_1000_out/BiasAdd/ReadVariableOpReadVariableOp=model_fmn10_lle_5000_1000_out_biasadd_readvariableop_resource*
_output_shapes
:*
dtype026
4model/fmn10_lle_5000_1000_out/BiasAdd/ReadVariableOp�
%model/fmn10_lle_5000_1000_out/BiasAddBiasAdd.model/fmn10_lle_5000_1000_out/MatMul:product:0<model/fmn10_lle_5000_1000_out/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2'
%model/fmn10_lle_5000_1000_out/BiasAdd�
%model/fmn10_lle_5000_1000_out/SigmoidSigmoid.model/fmn10_lle_5000_1000_out/BiasAdd:output:0*
T0*'
_output_shapes
:���������2'
%model/fmn10_lle_5000_1000_out/Sigmoid�
 model/concatenate_68/concat/axisConst*
_output_shapes
: *
dtype0*
value	B :2"
 model/concatenate_68/concat/axis�
model/concatenate_68/concatConcatV2)model/fmn10_pca_5000_1000_out/Sigmoid:y:0*model/fmn10_tsvd_5000_1000_out/Sigmoid:y:0,model/fmn10_isomap_5000_1000_out/Sigmoid:y:0)model/fmn10_lle_5000_1000_out/Sigmoid:y:0)model/concatenate_68/concat/axis:output:0*
N*
T0*'
_output_shapes
:���������2
model/concatenate_68/concat�
"model/joined/MatMul/ReadVariableOpReadVariableOp+model_joined_matmul_readvariableop_resource*
_output_shapes

:*
dtype02$
"model/joined/MatMul/ReadVariableOp�
model/joined/MatMulMatMul$model/concatenate_68/concat:output:0*model/joined/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
model/joined/MatMul�
#model/joined/BiasAdd/ReadVariableOpReadVariableOp,model_joined_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02%
#model/joined/BiasAdd/ReadVariableOp�
model/joined/BiasAddBiasAddmodel/joined/MatMul:product:0+model/joined/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
model/joined/BiasAdd�
model/joined/SigmoidSigmoidmodel/joined/BiasAdd:output:0*
T0*'
_output_shapes
:���������2
model/joined/Sigmoids
IdentityIdentitymodel/joined/Sigmoid:y:0^NoOp*
T0*'
_output_shapes
:���������2

Identity�
NoOpNoOp6^model/fmn10_isomap_5000_1000_1/BiasAdd/ReadVariableOp5^model/fmn10_isomap_5000_1000_1/MatMul/ReadVariableOp6^model/fmn10_isomap_5000_1000_2/BiasAdd/ReadVariableOp5^model/fmn10_isomap_5000_1000_2/MatMul/ReadVariableOp6^model/fmn10_isomap_5000_1000_3/BiasAdd/ReadVariableOp5^model/fmn10_isomap_5000_1000_3/MatMul/ReadVariableOp8^model/fmn10_isomap_5000_1000_out/BiasAdd/ReadVariableOp7^model/fmn10_isomap_5000_1000_out/MatMul/ReadVariableOp3^model/fmn10_lle_5000_1000_1/BiasAdd/ReadVariableOp2^model/fmn10_lle_5000_1000_1/MatMul/ReadVariableOp3^model/fmn10_lle_5000_1000_2/BiasAdd/ReadVariableOp2^model/fmn10_lle_5000_1000_2/MatMul/ReadVariableOp3^model/fmn10_lle_5000_1000_3/BiasAdd/ReadVariableOp2^model/fmn10_lle_5000_1000_3/MatMul/ReadVariableOp5^model/fmn10_lle_5000_1000_out/BiasAdd/ReadVariableOp4^model/fmn10_lle_5000_1000_out/MatMul/ReadVariableOp3^model/fmn10_pca_5000_1000_1/BiasAdd/ReadVariableOp2^model/fmn10_pca_5000_1000_1/MatMul/ReadVariableOp3^model/fmn10_pca_5000_1000_2/BiasAdd/ReadVariableOp2^model/fmn10_pca_5000_1000_2/MatMul/ReadVariableOp3^model/fmn10_pca_5000_1000_3/BiasAdd/ReadVariableOp2^model/fmn10_pca_5000_1000_3/MatMul/ReadVariableOp5^model/fmn10_pca_5000_1000_out/BiasAdd/ReadVariableOp4^model/fmn10_pca_5000_1000_out/MatMul/ReadVariableOp4^model/fmn10_tsvd_5000_1000_1/BiasAdd/ReadVariableOp3^model/fmn10_tsvd_5000_1000_1/MatMul/ReadVariableOp4^model/fmn10_tsvd_5000_1000_2/BiasAdd/ReadVariableOp3^model/fmn10_tsvd_5000_1000_2/MatMul/ReadVariableOp4^model/fmn10_tsvd_5000_1000_3/BiasAdd/ReadVariableOp3^model/fmn10_tsvd_5000_1000_3/MatMul/ReadVariableOp6^model/fmn10_tsvd_5000_1000_out/BiasAdd/ReadVariableOp5^model/fmn10_tsvd_5000_1000_out/MatMul/ReadVariableOp$^model/joined/BiasAdd/ReadVariableOp#^model/joined/MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*�
_input_shapes�
�:����������:����������:����������:����������: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 2n
5model/fmn10_isomap_5000_1000_1/BiasAdd/ReadVariableOp5model/fmn10_isomap_5000_1000_1/BiasAdd/ReadVariableOp2l
4model/fmn10_isomap_5000_1000_1/MatMul/ReadVariableOp4model/fmn10_isomap_5000_1000_1/MatMul/ReadVariableOp2n
5model/fmn10_isomap_5000_1000_2/BiasAdd/ReadVariableOp5model/fmn10_isomap_5000_1000_2/BiasAdd/ReadVariableOp2l
4model/fmn10_isomap_5000_1000_2/MatMul/ReadVariableOp4model/fmn10_isomap_5000_1000_2/MatMul/ReadVariableOp2n
5model/fmn10_isomap_5000_1000_3/BiasAdd/ReadVariableOp5model/fmn10_isomap_5000_1000_3/BiasAdd/ReadVariableOp2l
4model/fmn10_isomap_5000_1000_3/MatMul/ReadVariableOp4model/fmn10_isomap_5000_1000_3/MatMul/ReadVariableOp2r
7model/fmn10_isomap_5000_1000_out/BiasAdd/ReadVariableOp7model/fmn10_isomap_5000_1000_out/BiasAdd/ReadVariableOp2p
6model/fmn10_isomap_5000_1000_out/MatMul/ReadVariableOp6model/fmn10_isomap_5000_1000_out/MatMul/ReadVariableOp2h
2model/fmn10_lle_5000_1000_1/BiasAdd/ReadVariableOp2model/fmn10_lle_5000_1000_1/BiasAdd/ReadVariableOp2f
1model/fmn10_lle_5000_1000_1/MatMul/ReadVariableOp1model/fmn10_lle_5000_1000_1/MatMul/ReadVariableOp2h
2model/fmn10_lle_5000_1000_2/BiasAdd/ReadVariableOp2model/fmn10_lle_5000_1000_2/BiasAdd/ReadVariableOp2f
1model/fmn10_lle_5000_1000_2/MatMul/ReadVariableOp1model/fmn10_lle_5000_1000_2/MatMul/ReadVariableOp2h
2model/fmn10_lle_5000_1000_3/BiasAdd/ReadVariableOp2model/fmn10_lle_5000_1000_3/BiasAdd/ReadVariableOp2f
1model/fmn10_lle_5000_1000_3/MatMul/ReadVariableOp1model/fmn10_lle_5000_1000_3/MatMul/ReadVariableOp2l
4model/fmn10_lle_5000_1000_out/BiasAdd/ReadVariableOp4model/fmn10_lle_5000_1000_out/BiasAdd/ReadVariableOp2j
3model/fmn10_lle_5000_1000_out/MatMul/ReadVariableOp3model/fmn10_lle_5000_1000_out/MatMul/ReadVariableOp2h
2model/fmn10_pca_5000_1000_1/BiasAdd/ReadVariableOp2model/fmn10_pca_5000_1000_1/BiasAdd/ReadVariableOp2f
1model/fmn10_pca_5000_1000_1/MatMul/ReadVariableOp1model/fmn10_pca_5000_1000_1/MatMul/ReadVariableOp2h
2model/fmn10_pca_5000_1000_2/BiasAdd/ReadVariableOp2model/fmn10_pca_5000_1000_2/BiasAdd/ReadVariableOp2f
1model/fmn10_pca_5000_1000_2/MatMul/ReadVariableOp1model/fmn10_pca_5000_1000_2/MatMul/ReadVariableOp2h
2model/fmn10_pca_5000_1000_3/BiasAdd/ReadVariableOp2model/fmn10_pca_5000_1000_3/BiasAdd/ReadVariableOp2f
1model/fmn10_pca_5000_1000_3/MatMul/ReadVariableOp1model/fmn10_pca_5000_1000_3/MatMul/ReadVariableOp2l
4model/fmn10_pca_5000_1000_out/BiasAdd/ReadVariableOp4model/fmn10_pca_5000_1000_out/BiasAdd/ReadVariableOp2j
3model/fmn10_pca_5000_1000_out/MatMul/ReadVariableOp3model/fmn10_pca_5000_1000_out/MatMul/ReadVariableOp2j
3model/fmn10_tsvd_5000_1000_1/BiasAdd/ReadVariableOp3model/fmn10_tsvd_5000_1000_1/BiasAdd/ReadVariableOp2h
2model/fmn10_tsvd_5000_1000_1/MatMul/ReadVariableOp2model/fmn10_tsvd_5000_1000_1/MatMul/ReadVariableOp2j
3model/fmn10_tsvd_5000_1000_2/BiasAdd/ReadVariableOp3model/fmn10_tsvd_5000_1000_2/BiasAdd/ReadVariableOp2h
2model/fmn10_tsvd_5000_1000_2/MatMul/ReadVariableOp2model/fmn10_tsvd_5000_1000_2/MatMul/ReadVariableOp2j
3model/fmn10_tsvd_5000_1000_3/BiasAdd/ReadVariableOp3model/fmn10_tsvd_5000_1000_3/BiasAdd/ReadVariableOp2h
2model/fmn10_tsvd_5000_1000_3/MatMul/ReadVariableOp2model/fmn10_tsvd_5000_1000_3/MatMul/ReadVariableOp2n
5model/fmn10_tsvd_5000_1000_out/BiasAdd/ReadVariableOp5model/fmn10_tsvd_5000_1000_out/BiasAdd/ReadVariableOp2l
4model/fmn10_tsvd_5000_1000_out/MatMul/ReadVariableOp4model/fmn10_tsvd_5000_1000_out/MatMul/ReadVariableOp2J
#model/joined/BiasAdd/ReadVariableOp#model/joined/BiasAdd/ReadVariableOp2H
"model/joined/MatMul/ReadVariableOp"model/joined/MatMul/ReadVariableOp:c _
(
_output_shapes
:����������
3
_user_specified_namefmn10_pca_5000_1000_input:d`
(
_output_shapes
:����������
4
_user_specified_namefmn10_tsvd_5000_1000_input:fb
(
_output_shapes
:����������
6
_user_specified_namefmn10_isomap_5000_1000_input:c_
(
_output_shapes
:����������
3
_user_specified_namefmn10_lle_5000_1000_input
�
�
S__inference_fmn10_tsvd_5000_1000_3_layer_call_and_return_conditional_losses_2308566

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
W__inference_fmn10_isomap_5000_1000_out_layer_call_and_return_conditional_losses_2310235

inputs1
matmul_readvariableop_resource:	�-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes
:	�*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2	
BiasAdda
SigmoidSigmoidBiasAdd:output:0*
T0*'
_output_shapes
:���������2	
Sigmoidf
IdentityIdentitySigmoid:y:0^NoOp*
T0*'
_output_shapes
:���������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
C__inference_joined_layer_call_and_return_conditional_losses_2310292

inputs0
matmul_readvariableop_resource:-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2	
BiasAdda
SigmoidSigmoidBiasAdd:output:0*
T0*'
_output_shapes
:���������2	
Sigmoidf
IdentityIdentitySigmoid:y:0^NoOp*
T0*'
_output_shapes
:���������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs
�
�
C__inference_joined_layer_call_and_return_conditional_losses_2308679

inputs0
matmul_readvariableop_resource:-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2	
BiasAdda
SigmoidSigmoidBiasAdd:output:0*
T0*'
_output_shapes
:���������2	
Sigmoidf
IdentityIdentitySigmoid:y:0^NoOp*
T0*'
_output_shapes
:���������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs
�
�
(__inference_joined_layer_call_fn_2310281

inputs
unknown:
	unknown_0:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *L
fGRE
C__inference_joined_layer_call_and_return_conditional_losses_23086792
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������: : 22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs
�
�
R__inference_fmn10_pca_5000_1000_2_layer_call_and_return_conditional_losses_2308515

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�a
�
 __inference__traced_save_2310459
file_prefix;
7savev2_fmn10_pca_5000_1000_1_kernel_read_readvariableop9
5savev2_fmn10_pca_5000_1000_1_bias_read_readvariableop<
8savev2_fmn10_tsvd_5000_1000_1_kernel_read_readvariableop:
6savev2_fmn10_tsvd_5000_1000_1_bias_read_readvariableop>
:savev2_fmn10_isomap_5000_1000_1_kernel_read_readvariableop<
8savev2_fmn10_isomap_5000_1000_1_bias_read_readvariableop;
7savev2_fmn10_lle_5000_1000_1_kernel_read_readvariableop9
5savev2_fmn10_lle_5000_1000_1_bias_read_readvariableop;
7savev2_fmn10_pca_5000_1000_2_kernel_read_readvariableop9
5savev2_fmn10_pca_5000_1000_2_bias_read_readvariableop<
8savev2_fmn10_tsvd_5000_1000_2_kernel_read_readvariableop:
6savev2_fmn10_tsvd_5000_1000_2_bias_read_readvariableop>
:savev2_fmn10_isomap_5000_1000_2_kernel_read_readvariableop<
8savev2_fmn10_isomap_5000_1000_2_bias_read_readvariableop;
7savev2_fmn10_lle_5000_1000_2_kernel_read_readvariableop9
5savev2_fmn10_lle_5000_1000_2_bias_read_readvariableop;
7savev2_fmn10_pca_5000_1000_3_kernel_read_readvariableop9
5savev2_fmn10_pca_5000_1000_3_bias_read_readvariableop<
8savev2_fmn10_tsvd_5000_1000_3_kernel_read_readvariableop:
6savev2_fmn10_tsvd_5000_1000_3_bias_read_readvariableop>
:savev2_fmn10_isomap_5000_1000_3_kernel_read_readvariableop<
8savev2_fmn10_isomap_5000_1000_3_bias_read_readvariableop;
7savev2_fmn10_lle_5000_1000_3_kernel_read_readvariableop9
5savev2_fmn10_lle_5000_1000_3_bias_read_readvariableop=
9savev2_fmn10_pca_5000_1000_out_kernel_read_readvariableop;
7savev2_fmn10_pca_5000_1000_out_bias_read_readvariableop>
:savev2_fmn10_tsvd_5000_1000_out_kernel_read_readvariableop<
8savev2_fmn10_tsvd_5000_1000_out_bias_read_readvariableop@
<savev2_fmn10_isomap_5000_1000_out_kernel_read_readvariableop>
:savev2_fmn10_isomap_5000_1000_out_bias_read_readvariableop=
9savev2_fmn10_lle_5000_1000_out_kernel_read_readvariableop;
7savev2_fmn10_lle_5000_1000_out_bias_read_readvariableop,
(savev2_joined_kernel_read_readvariableop*
&savev2_joined_bias_read_readvariableop(
$savev2_adam_iter_read_readvariableop	*
&savev2_adam_beta_1_read_readvariableop*
&savev2_adam_beta_2_read_readvariableop)
%savev2_adam_decay_read_readvariableop1
-savev2_adam_learning_rate_read_readvariableop$
 savev2_total_read_readvariableop$
 savev2_count_read_readvariableop&
"savev2_total_1_read_readvariableop&
"savev2_count_1_read_readvariableop3
/savev2_adam_joined_kernel_m_read_readvariableop1
-savev2_adam_joined_bias_m_read_readvariableop3
/savev2_adam_joined_kernel_v_read_readvariableop1
-savev2_adam_joined_bias_v_read_readvariableop
savev2_const

identity_1��MergeV2Checkpoints�
StaticRegexFullMatchStaticRegexFullMatchfile_prefix"/device:CPU:**
_output_shapes
: *
pattern
^s3://.*2
StaticRegexFullMatchc
ConstConst"/device:CPU:**
_output_shapes
: *
dtype0*
valueB B.part2
Constl
Const_1Const"/device:CPU:**
_output_shapes
: *
dtype0*
valueB B
_temp/part2	
Const_1�
SelectSelectStaticRegexFullMatch:output:0Const:output:0Const_1:output:0"/device:CPU:**
T0*
_output_shapes
: 2
Selectt

StringJoin
StringJoinfile_prefixSelect:output:0"/device:CPU:**
N*
_output_shapes
: 2

StringJoinZ

num_shardsConst*
_output_shapes
: *
dtype0*
value	B :2

num_shards
ShardedFilename/shardConst"/device:CPU:0*
_output_shapes
: *
dtype0*
value	B : 2
ShardedFilename/shard�
ShardedFilenameShardedFilenameStringJoin:output:0ShardedFilename/shard:output:0num_shards:output:0"/device:CPU:0*
_output_shapes
: 2
ShardedFilename�
SaveV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:0*
dtype0*�
value�B�0B6layer_with_weights-0/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-0/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-1/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-1/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-3/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-3/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-4/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-4/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-5/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-5/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-6/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-6/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-7/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-7/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-8/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-8/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-9/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-9/bias/.ATTRIBUTES/VARIABLE_VALUEB7layer_with_weights-10/kernel/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-10/bias/.ATTRIBUTES/VARIABLE_VALUEB7layer_with_weights-11/kernel/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-11/bias/.ATTRIBUTES/VARIABLE_VALUEB7layer_with_weights-12/kernel/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-12/bias/.ATTRIBUTES/VARIABLE_VALUEB7layer_with_weights-13/kernel/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-13/bias/.ATTRIBUTES/VARIABLE_VALUEB7layer_with_weights-14/kernel/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-14/bias/.ATTRIBUTES/VARIABLE_VALUEB7layer_with_weights-15/kernel/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-15/bias/.ATTRIBUTES/VARIABLE_VALUEB7layer_with_weights-16/kernel/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-16/bias/.ATTRIBUTES/VARIABLE_VALUEB)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUEB*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/count/.ATTRIBUTES/VARIABLE_VALUEBSlayer_with_weights-16/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-16/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBSlayer_with_weights-16/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-16/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEB_CHECKPOINTABLE_OBJECT_GRAPH2
SaveV2/tensor_names�
SaveV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:0*
dtype0*s
valuejBh0B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B 2
SaveV2/shape_and_slices�
SaveV2SaveV2ShardedFilename:filename:0SaveV2/tensor_names:output:0 SaveV2/shape_and_slices:output:07savev2_fmn10_pca_5000_1000_1_kernel_read_readvariableop5savev2_fmn10_pca_5000_1000_1_bias_read_readvariableop8savev2_fmn10_tsvd_5000_1000_1_kernel_read_readvariableop6savev2_fmn10_tsvd_5000_1000_1_bias_read_readvariableop:savev2_fmn10_isomap_5000_1000_1_kernel_read_readvariableop8savev2_fmn10_isomap_5000_1000_1_bias_read_readvariableop7savev2_fmn10_lle_5000_1000_1_kernel_read_readvariableop5savev2_fmn10_lle_5000_1000_1_bias_read_readvariableop7savev2_fmn10_pca_5000_1000_2_kernel_read_readvariableop5savev2_fmn10_pca_5000_1000_2_bias_read_readvariableop8savev2_fmn10_tsvd_5000_1000_2_kernel_read_readvariableop6savev2_fmn10_tsvd_5000_1000_2_bias_read_readvariableop:savev2_fmn10_isomap_5000_1000_2_kernel_read_readvariableop8savev2_fmn10_isomap_5000_1000_2_bias_read_readvariableop7savev2_fmn10_lle_5000_1000_2_kernel_read_readvariableop5savev2_fmn10_lle_5000_1000_2_bias_read_readvariableop7savev2_fmn10_pca_5000_1000_3_kernel_read_readvariableop5savev2_fmn10_pca_5000_1000_3_bias_read_readvariableop8savev2_fmn10_tsvd_5000_1000_3_kernel_read_readvariableop6savev2_fmn10_tsvd_5000_1000_3_bias_read_readvariableop:savev2_fmn10_isomap_5000_1000_3_kernel_read_readvariableop8savev2_fmn10_isomap_5000_1000_3_bias_read_readvariableop7savev2_fmn10_lle_5000_1000_3_kernel_read_readvariableop5savev2_fmn10_lle_5000_1000_3_bias_read_readvariableop9savev2_fmn10_pca_5000_1000_out_kernel_read_readvariableop7savev2_fmn10_pca_5000_1000_out_bias_read_readvariableop:savev2_fmn10_tsvd_5000_1000_out_kernel_read_readvariableop8savev2_fmn10_tsvd_5000_1000_out_bias_read_readvariableop<savev2_fmn10_isomap_5000_1000_out_kernel_read_readvariableop:savev2_fmn10_isomap_5000_1000_out_bias_read_readvariableop9savev2_fmn10_lle_5000_1000_out_kernel_read_readvariableop7savev2_fmn10_lle_5000_1000_out_bias_read_readvariableop(savev2_joined_kernel_read_readvariableop&savev2_joined_bias_read_readvariableop$savev2_adam_iter_read_readvariableop&savev2_adam_beta_1_read_readvariableop&savev2_adam_beta_2_read_readvariableop%savev2_adam_decay_read_readvariableop-savev2_adam_learning_rate_read_readvariableop savev2_total_read_readvariableop savev2_count_read_readvariableop"savev2_total_1_read_readvariableop"savev2_count_1_read_readvariableop/savev2_adam_joined_kernel_m_read_readvariableop-savev2_adam_joined_bias_m_read_readvariableop/savev2_adam_joined_kernel_v_read_readvariableop-savev2_adam_joined_bias_v_read_readvariableopsavev2_const"/device:CPU:0*
_output_shapes
 *>
dtypes4
220	2
SaveV2�
&MergeV2Checkpoints/checkpoint_prefixesPackShardedFilename:filename:0^SaveV2"/device:CPU:0*
N*
T0*
_output_shapes
:2(
&MergeV2Checkpoints/checkpoint_prefixes�
MergeV2CheckpointsMergeV2Checkpoints/MergeV2Checkpoints/checkpoint_prefixes:output:0file_prefix"/device:CPU:0*
_output_shapes
 2
MergeV2Checkpointsr
IdentityIdentityfile_prefix^MergeV2Checkpoints"/device:CPU:0*
T0*
_output_shapes
: 2

Identity_

Identity_1IdentityIdentity:output:0^NoOp*
T0*
_output_shapes
: 2

Identity_1c
NoOpNoOp^MergeV2Checkpoints*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"!

identity_1Identity_1:output:0*�
_input_shapes�
�: :
��:�:
��:�:
��:�:
��:�:
��:�:
��:�:
��:�:
��:�:
��:�:
��:�:
��:�:
��:�:	�::	�::	�::	�:::: : : : : : : : : ::::: 2(
MergeV2CheckpointsMergeV2Checkpoints:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix:&"
 
_output_shapes
:
��:!

_output_shapes	
:�:&"
 
_output_shapes
:
��:!

_output_shapes	
:�:&"
 
_output_shapes
:
��:!

_output_shapes	
:�:&"
 
_output_shapes
:
��:!

_output_shapes	
:�:&	"
 
_output_shapes
:
��:!


_output_shapes	
:�:&"
 
_output_shapes
:
��:!

_output_shapes	
:�:&"
 
_output_shapes
:
��:!

_output_shapes	
:�:&"
 
_output_shapes
:
��:!

_output_shapes	
:�:&"
 
_output_shapes
:
��:!

_output_shapes	
:�:&"
 
_output_shapes
:
��:!

_output_shapes	
:�:&"
 
_output_shapes
:
��:!

_output_shapes	
:�:&"
 
_output_shapes
:
��:!

_output_shapes	
:�:%!

_output_shapes
:	�: 

_output_shapes
::%!

_output_shapes
:	�: 

_output_shapes
::%!

_output_shapes
:	�: 

_output_shapes
::%!

_output_shapes
:	�:  

_output_shapes
::$! 

_output_shapes

:: "

_output_shapes
::#

_output_shapes
: :$

_output_shapes
: :%

_output_shapes
: :&

_output_shapes
: :'

_output_shapes
: :(

_output_shapes
: :)

_output_shapes
: :*

_output_shapes
: :+

_output_shapes
: :$, 

_output_shapes

:: -

_output_shapes
::$. 

_output_shapes

:: /

_output_shapes
::0

_output_shapes
: 
�
�
T__inference_fmn10_lle_5000_1000_out_layer_call_and_return_conditional_losses_2308651

inputs1
matmul_readvariableop_resource:	�-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes
:	�*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2	
BiasAdda
SigmoidSigmoidBiasAdd:output:0*
T0*'
_output_shapes
:���������2	
Sigmoidf
IdentityIdentitySigmoid:y:0^NoOp*
T0*'
_output_shapes
:���������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
8__inference_fmn10_tsvd_5000_1000_2_layer_call_fn_2310044

inputs
unknown:
��
	unknown_0:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *\
fWRU
S__inference_fmn10_tsvd_5000_1000_2_layer_call_and_return_conditional_losses_23084982
StatefulPartitionedCall|
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:����������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
K__inference_concatenate_68_layer_call_and_return_conditional_losses_2308666

inputs
inputs_1
inputs_2
inputs_3
identity\
concat/axisConst*
_output_shapes
: *
dtype0*
value	B :2
concat/axis�
concatConcatV2inputsinputs_1inputs_2inputs_3concat/axis:output:0*
N*
T0*'
_output_shapes
:���������2
concatc
IdentityIdentityconcat:output:0*
T0*'
_output_shapes
:���������2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*_
_input_shapesN
L:���������:���������:���������:���������:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs:OK
'
_output_shapes
:���������
 
_user_specified_nameinputs:OK
'
_output_shapes
:���������
 
_user_specified_nameinputs:OK
'
_output_shapes
:���������
 
_user_specified_nameinputs
�y
�
B__inference_model_layer_call_and_return_conditional_losses_2309350
fmn10_pca_5000_1000_input
fmn10_tsvd_5000_1000_input 
fmn10_isomap_5000_1000_input
fmn10_lle_5000_1000_input1
fmn10_lle_5000_1000_1_2309263:
��,
fmn10_lle_5000_1000_1_2309265:	�4
 fmn10_isomap_5000_1000_1_2309268:
��/
 fmn10_isomap_5000_1000_1_2309270:	�2
fmn10_tsvd_5000_1000_1_2309273:
��-
fmn10_tsvd_5000_1000_1_2309275:	�1
fmn10_pca_5000_1000_1_2309278:
��,
fmn10_pca_5000_1000_1_2309280:	�1
fmn10_lle_5000_1000_2_2309283:
��,
fmn10_lle_5000_1000_2_2309285:	�4
 fmn10_isomap_5000_1000_2_2309288:
��/
 fmn10_isomap_5000_1000_2_2309290:	�2
fmn10_tsvd_5000_1000_2_2309293:
��-
fmn10_tsvd_5000_1000_2_2309295:	�1
fmn10_pca_5000_1000_2_2309298:
��,
fmn10_pca_5000_1000_2_2309300:	�1
fmn10_lle_5000_1000_3_2309303:
��,
fmn10_lle_5000_1000_3_2309305:	�4
 fmn10_isomap_5000_1000_3_2309308:
��/
 fmn10_isomap_5000_1000_3_2309310:	�2
fmn10_tsvd_5000_1000_3_2309313:
��-
fmn10_tsvd_5000_1000_3_2309315:	�1
fmn10_pca_5000_1000_3_2309318:
��,
fmn10_pca_5000_1000_3_2309320:	�2
fmn10_pca_5000_1000_out_2309323:	�-
fmn10_pca_5000_1000_out_2309325:3
 fmn10_tsvd_5000_1000_out_2309328:	�.
 fmn10_tsvd_5000_1000_out_2309330:5
"fmn10_isomap_5000_1000_out_2309333:	�0
"fmn10_isomap_5000_1000_out_2309335:2
fmn10_lle_5000_1000_out_2309338:	�-
fmn10_lle_5000_1000_out_2309340: 
joined_2309344:
joined_2309346:
identity��0fmn10_isomap_5000_1000_1/StatefulPartitionedCall�0fmn10_isomap_5000_1000_2/StatefulPartitionedCall�0fmn10_isomap_5000_1000_3/StatefulPartitionedCall�2fmn10_isomap_5000_1000_out/StatefulPartitionedCall�-fmn10_lle_5000_1000_1/StatefulPartitionedCall�-fmn10_lle_5000_1000_2/StatefulPartitionedCall�-fmn10_lle_5000_1000_3/StatefulPartitionedCall�/fmn10_lle_5000_1000_out/StatefulPartitionedCall�-fmn10_pca_5000_1000_1/StatefulPartitionedCall�-fmn10_pca_5000_1000_2/StatefulPartitionedCall�-fmn10_pca_5000_1000_3/StatefulPartitionedCall�/fmn10_pca_5000_1000_out/StatefulPartitionedCall�.fmn10_tsvd_5000_1000_1/StatefulPartitionedCall�.fmn10_tsvd_5000_1000_2/StatefulPartitionedCall�.fmn10_tsvd_5000_1000_3/StatefulPartitionedCall�0fmn10_tsvd_5000_1000_out/StatefulPartitionedCall�joined/StatefulPartitionedCall�
-fmn10_lle_5000_1000_1/StatefulPartitionedCallStatefulPartitionedCallfmn10_lle_5000_1000_inputfmn10_lle_5000_1000_1_2309263fmn10_lle_5000_1000_1_2309265*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_fmn10_lle_5000_1000_1_layer_call_and_return_conditional_losses_23083962/
-fmn10_lle_5000_1000_1/StatefulPartitionedCall�
0fmn10_isomap_5000_1000_1/StatefulPartitionedCallStatefulPartitionedCallfmn10_isomap_5000_1000_input fmn10_isomap_5000_1000_1_2309268 fmn10_isomap_5000_1000_1_2309270*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *^
fYRW
U__inference_fmn10_isomap_5000_1000_1_layer_call_and_return_conditional_losses_230841322
0fmn10_isomap_5000_1000_1/StatefulPartitionedCall�
.fmn10_tsvd_5000_1000_1/StatefulPartitionedCallStatefulPartitionedCallfmn10_tsvd_5000_1000_inputfmn10_tsvd_5000_1000_1_2309273fmn10_tsvd_5000_1000_1_2309275*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *\
fWRU
S__inference_fmn10_tsvd_5000_1000_1_layer_call_and_return_conditional_losses_230843020
.fmn10_tsvd_5000_1000_1/StatefulPartitionedCall�
-fmn10_pca_5000_1000_1/StatefulPartitionedCallStatefulPartitionedCallfmn10_pca_5000_1000_inputfmn10_pca_5000_1000_1_2309278fmn10_pca_5000_1000_1_2309280*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_fmn10_pca_5000_1000_1_layer_call_and_return_conditional_losses_23084472/
-fmn10_pca_5000_1000_1/StatefulPartitionedCall�
-fmn10_lle_5000_1000_2/StatefulPartitionedCallStatefulPartitionedCall6fmn10_lle_5000_1000_1/StatefulPartitionedCall:output:0fmn10_lle_5000_1000_2_2309283fmn10_lle_5000_1000_2_2309285*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_fmn10_lle_5000_1000_2_layer_call_and_return_conditional_losses_23084642/
-fmn10_lle_5000_1000_2/StatefulPartitionedCall�
0fmn10_isomap_5000_1000_2/StatefulPartitionedCallStatefulPartitionedCall9fmn10_isomap_5000_1000_1/StatefulPartitionedCall:output:0 fmn10_isomap_5000_1000_2_2309288 fmn10_isomap_5000_1000_2_2309290*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *^
fYRW
U__inference_fmn10_isomap_5000_1000_2_layer_call_and_return_conditional_losses_230848122
0fmn10_isomap_5000_1000_2/StatefulPartitionedCall�
.fmn10_tsvd_5000_1000_2/StatefulPartitionedCallStatefulPartitionedCall7fmn10_tsvd_5000_1000_1/StatefulPartitionedCall:output:0fmn10_tsvd_5000_1000_2_2309293fmn10_tsvd_5000_1000_2_2309295*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *\
fWRU
S__inference_fmn10_tsvd_5000_1000_2_layer_call_and_return_conditional_losses_230849820
.fmn10_tsvd_5000_1000_2/StatefulPartitionedCall�
-fmn10_pca_5000_1000_2/StatefulPartitionedCallStatefulPartitionedCall6fmn10_pca_5000_1000_1/StatefulPartitionedCall:output:0fmn10_pca_5000_1000_2_2309298fmn10_pca_5000_1000_2_2309300*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_fmn10_pca_5000_1000_2_layer_call_and_return_conditional_losses_23085152/
-fmn10_pca_5000_1000_2/StatefulPartitionedCall�
-fmn10_lle_5000_1000_3/StatefulPartitionedCallStatefulPartitionedCall6fmn10_lle_5000_1000_2/StatefulPartitionedCall:output:0fmn10_lle_5000_1000_3_2309303fmn10_lle_5000_1000_3_2309305*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_fmn10_lle_5000_1000_3_layer_call_and_return_conditional_losses_23085322/
-fmn10_lle_5000_1000_3/StatefulPartitionedCall�
0fmn10_isomap_5000_1000_3/StatefulPartitionedCallStatefulPartitionedCall9fmn10_isomap_5000_1000_2/StatefulPartitionedCall:output:0 fmn10_isomap_5000_1000_3_2309308 fmn10_isomap_5000_1000_3_2309310*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *^
fYRW
U__inference_fmn10_isomap_5000_1000_3_layer_call_and_return_conditional_losses_230854922
0fmn10_isomap_5000_1000_3/StatefulPartitionedCall�
.fmn10_tsvd_5000_1000_3/StatefulPartitionedCallStatefulPartitionedCall7fmn10_tsvd_5000_1000_2/StatefulPartitionedCall:output:0fmn10_tsvd_5000_1000_3_2309313fmn10_tsvd_5000_1000_3_2309315*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *\
fWRU
S__inference_fmn10_tsvd_5000_1000_3_layer_call_and_return_conditional_losses_230856620
.fmn10_tsvd_5000_1000_3/StatefulPartitionedCall�
-fmn10_pca_5000_1000_3/StatefulPartitionedCallStatefulPartitionedCall6fmn10_pca_5000_1000_2/StatefulPartitionedCall:output:0fmn10_pca_5000_1000_3_2309318fmn10_pca_5000_1000_3_2309320*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_fmn10_pca_5000_1000_3_layer_call_and_return_conditional_losses_23085832/
-fmn10_pca_5000_1000_3/StatefulPartitionedCall�
/fmn10_pca_5000_1000_out/StatefulPartitionedCallStatefulPartitionedCall6fmn10_pca_5000_1000_3/StatefulPartitionedCall:output:0fmn10_pca_5000_1000_out_2309323fmn10_pca_5000_1000_out_2309325*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *]
fXRV
T__inference_fmn10_pca_5000_1000_out_layer_call_and_return_conditional_losses_230860021
/fmn10_pca_5000_1000_out/StatefulPartitionedCall�
0fmn10_tsvd_5000_1000_out/StatefulPartitionedCallStatefulPartitionedCall7fmn10_tsvd_5000_1000_3/StatefulPartitionedCall:output:0 fmn10_tsvd_5000_1000_out_2309328 fmn10_tsvd_5000_1000_out_2309330*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *^
fYRW
U__inference_fmn10_tsvd_5000_1000_out_layer_call_and_return_conditional_losses_230861722
0fmn10_tsvd_5000_1000_out/StatefulPartitionedCall�
2fmn10_isomap_5000_1000_out/StatefulPartitionedCallStatefulPartitionedCall9fmn10_isomap_5000_1000_3/StatefulPartitionedCall:output:0"fmn10_isomap_5000_1000_out_2309333"fmn10_isomap_5000_1000_out_2309335*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *`
f[RY
W__inference_fmn10_isomap_5000_1000_out_layer_call_and_return_conditional_losses_230863424
2fmn10_isomap_5000_1000_out/StatefulPartitionedCall�
/fmn10_lle_5000_1000_out/StatefulPartitionedCallStatefulPartitionedCall6fmn10_lle_5000_1000_3/StatefulPartitionedCall:output:0fmn10_lle_5000_1000_out_2309338fmn10_lle_5000_1000_out_2309340*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *]
fXRV
T__inference_fmn10_lle_5000_1000_out_layer_call_and_return_conditional_losses_230865121
/fmn10_lle_5000_1000_out/StatefulPartitionedCall�
concatenate_68/PartitionedCallPartitionedCall8fmn10_pca_5000_1000_out/StatefulPartitionedCall:output:09fmn10_tsvd_5000_1000_out/StatefulPartitionedCall:output:0;fmn10_isomap_5000_1000_out/StatefulPartitionedCall:output:08fmn10_lle_5000_1000_out/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *T
fORM
K__inference_concatenate_68_layer_call_and_return_conditional_losses_23086662 
concatenate_68/PartitionedCall�
joined/StatefulPartitionedCallStatefulPartitionedCall'concatenate_68/PartitionedCall:output:0joined_2309344joined_2309346*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *L
fGRE
C__inference_joined_layer_call_and_return_conditional_losses_23086792 
joined/StatefulPartitionedCall�
IdentityIdentity'joined/StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identity�
NoOpNoOp1^fmn10_isomap_5000_1000_1/StatefulPartitionedCall1^fmn10_isomap_5000_1000_2/StatefulPartitionedCall1^fmn10_isomap_5000_1000_3/StatefulPartitionedCall3^fmn10_isomap_5000_1000_out/StatefulPartitionedCall.^fmn10_lle_5000_1000_1/StatefulPartitionedCall.^fmn10_lle_5000_1000_2/StatefulPartitionedCall.^fmn10_lle_5000_1000_3/StatefulPartitionedCall0^fmn10_lle_5000_1000_out/StatefulPartitionedCall.^fmn10_pca_5000_1000_1/StatefulPartitionedCall.^fmn10_pca_5000_1000_2/StatefulPartitionedCall.^fmn10_pca_5000_1000_3/StatefulPartitionedCall0^fmn10_pca_5000_1000_out/StatefulPartitionedCall/^fmn10_tsvd_5000_1000_1/StatefulPartitionedCall/^fmn10_tsvd_5000_1000_2/StatefulPartitionedCall/^fmn10_tsvd_5000_1000_3/StatefulPartitionedCall1^fmn10_tsvd_5000_1000_out/StatefulPartitionedCall^joined/StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*�
_input_shapes�
�:����������:����������:����������:����������: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 2d
0fmn10_isomap_5000_1000_1/StatefulPartitionedCall0fmn10_isomap_5000_1000_1/StatefulPartitionedCall2d
0fmn10_isomap_5000_1000_2/StatefulPartitionedCall0fmn10_isomap_5000_1000_2/StatefulPartitionedCall2d
0fmn10_isomap_5000_1000_3/StatefulPartitionedCall0fmn10_isomap_5000_1000_3/StatefulPartitionedCall2h
2fmn10_isomap_5000_1000_out/StatefulPartitionedCall2fmn10_isomap_5000_1000_out/StatefulPartitionedCall2^
-fmn10_lle_5000_1000_1/StatefulPartitionedCall-fmn10_lle_5000_1000_1/StatefulPartitionedCall2^
-fmn10_lle_5000_1000_2/StatefulPartitionedCall-fmn10_lle_5000_1000_2/StatefulPartitionedCall2^
-fmn10_lle_5000_1000_3/StatefulPartitionedCall-fmn10_lle_5000_1000_3/StatefulPartitionedCall2b
/fmn10_lle_5000_1000_out/StatefulPartitionedCall/fmn10_lle_5000_1000_out/StatefulPartitionedCall2^
-fmn10_pca_5000_1000_1/StatefulPartitionedCall-fmn10_pca_5000_1000_1/StatefulPartitionedCall2^
-fmn10_pca_5000_1000_2/StatefulPartitionedCall-fmn10_pca_5000_1000_2/StatefulPartitionedCall2^
-fmn10_pca_5000_1000_3/StatefulPartitionedCall-fmn10_pca_5000_1000_3/StatefulPartitionedCall2b
/fmn10_pca_5000_1000_out/StatefulPartitionedCall/fmn10_pca_5000_1000_out/StatefulPartitionedCall2`
.fmn10_tsvd_5000_1000_1/StatefulPartitionedCall.fmn10_tsvd_5000_1000_1/StatefulPartitionedCall2`
.fmn10_tsvd_5000_1000_2/StatefulPartitionedCall.fmn10_tsvd_5000_1000_2/StatefulPartitionedCall2`
.fmn10_tsvd_5000_1000_3/StatefulPartitionedCall.fmn10_tsvd_5000_1000_3/StatefulPartitionedCall2d
0fmn10_tsvd_5000_1000_out/StatefulPartitionedCall0fmn10_tsvd_5000_1000_out/StatefulPartitionedCall2@
joined/StatefulPartitionedCalljoined/StatefulPartitionedCall:c _
(
_output_shapes
:����������
3
_user_specified_namefmn10_pca_5000_1000_input:d`
(
_output_shapes
:����������
4
_user_specified_namefmn10_tsvd_5000_1000_input:fb
(
_output_shapes
:����������
6
_user_specified_namefmn10_isomap_5000_1000_input:c_
(
_output_shapes
:����������
3
_user_specified_namefmn10_lle_5000_1000_input
�
�
7__inference_fmn10_pca_5000_1000_2_layer_call_fn_2310024

inputs
unknown:
��
	unknown_0:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_fmn10_pca_5000_1000_2_layer_call_and_return_conditional_losses_23085152
StatefulPartitionedCall|
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:����������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
R__inference_fmn10_lle_5000_1000_2_layer_call_and_return_conditional_losses_2310095

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
U__inference_fmn10_tsvd_5000_1000_out_layer_call_and_return_conditional_losses_2308617

inputs1
matmul_readvariableop_resource:	�-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes
:	�*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2	
BiasAdda
SigmoidSigmoidBiasAdd:output:0*
T0*'
_output_shapes
:���������2	
Sigmoidf
IdentityIdentitySigmoid:y:0^NoOp*
T0*'
_output_shapes
:���������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
R__inference_fmn10_lle_5000_1000_3_layer_call_and_return_conditional_losses_2308532

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
8__inference_fmn10_tsvd_5000_1000_3_layer_call_fn_2310124

inputs
unknown:
��
	unknown_0:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *\
fWRU
S__inference_fmn10_tsvd_5000_1000_3_layer_call_and_return_conditional_losses_23085662
StatefulPartitionedCall|
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:����������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
��
� 
B__inference_model_layer_call_and_return_conditional_losses_2309807
inputs_0
inputs_1
inputs_2
inputs_3H
4fmn10_lle_5000_1000_1_matmul_readvariableop_resource:
��D
5fmn10_lle_5000_1000_1_biasadd_readvariableop_resource:	�K
7fmn10_isomap_5000_1000_1_matmul_readvariableop_resource:
��G
8fmn10_isomap_5000_1000_1_biasadd_readvariableop_resource:	�I
5fmn10_tsvd_5000_1000_1_matmul_readvariableop_resource:
��E
6fmn10_tsvd_5000_1000_1_biasadd_readvariableop_resource:	�H
4fmn10_pca_5000_1000_1_matmul_readvariableop_resource:
��D
5fmn10_pca_5000_1000_1_biasadd_readvariableop_resource:	�H
4fmn10_lle_5000_1000_2_matmul_readvariableop_resource:
��D
5fmn10_lle_5000_1000_2_biasadd_readvariableop_resource:	�K
7fmn10_isomap_5000_1000_2_matmul_readvariableop_resource:
��G
8fmn10_isomap_5000_1000_2_biasadd_readvariableop_resource:	�I
5fmn10_tsvd_5000_1000_2_matmul_readvariableop_resource:
��E
6fmn10_tsvd_5000_1000_2_biasadd_readvariableop_resource:	�H
4fmn10_pca_5000_1000_2_matmul_readvariableop_resource:
��D
5fmn10_pca_5000_1000_2_biasadd_readvariableop_resource:	�H
4fmn10_lle_5000_1000_3_matmul_readvariableop_resource:
��D
5fmn10_lle_5000_1000_3_biasadd_readvariableop_resource:	�K
7fmn10_isomap_5000_1000_3_matmul_readvariableop_resource:
��G
8fmn10_isomap_5000_1000_3_biasadd_readvariableop_resource:	�I
5fmn10_tsvd_5000_1000_3_matmul_readvariableop_resource:
��E
6fmn10_tsvd_5000_1000_3_biasadd_readvariableop_resource:	�H
4fmn10_pca_5000_1000_3_matmul_readvariableop_resource:
��D
5fmn10_pca_5000_1000_3_biasadd_readvariableop_resource:	�I
6fmn10_pca_5000_1000_out_matmul_readvariableop_resource:	�E
7fmn10_pca_5000_1000_out_biasadd_readvariableop_resource:J
7fmn10_tsvd_5000_1000_out_matmul_readvariableop_resource:	�F
8fmn10_tsvd_5000_1000_out_biasadd_readvariableop_resource:L
9fmn10_isomap_5000_1000_out_matmul_readvariableop_resource:	�H
:fmn10_isomap_5000_1000_out_biasadd_readvariableop_resource:I
6fmn10_lle_5000_1000_out_matmul_readvariableop_resource:	�E
7fmn10_lle_5000_1000_out_biasadd_readvariableop_resource:7
%joined_matmul_readvariableop_resource:4
&joined_biasadd_readvariableop_resource:
identity��/fmn10_isomap_5000_1000_1/BiasAdd/ReadVariableOp�.fmn10_isomap_5000_1000_1/MatMul/ReadVariableOp�/fmn10_isomap_5000_1000_2/BiasAdd/ReadVariableOp�.fmn10_isomap_5000_1000_2/MatMul/ReadVariableOp�/fmn10_isomap_5000_1000_3/BiasAdd/ReadVariableOp�.fmn10_isomap_5000_1000_3/MatMul/ReadVariableOp�1fmn10_isomap_5000_1000_out/BiasAdd/ReadVariableOp�0fmn10_isomap_5000_1000_out/MatMul/ReadVariableOp�,fmn10_lle_5000_1000_1/BiasAdd/ReadVariableOp�+fmn10_lle_5000_1000_1/MatMul/ReadVariableOp�,fmn10_lle_5000_1000_2/BiasAdd/ReadVariableOp�+fmn10_lle_5000_1000_2/MatMul/ReadVariableOp�,fmn10_lle_5000_1000_3/BiasAdd/ReadVariableOp�+fmn10_lle_5000_1000_3/MatMul/ReadVariableOp�.fmn10_lle_5000_1000_out/BiasAdd/ReadVariableOp�-fmn10_lle_5000_1000_out/MatMul/ReadVariableOp�,fmn10_pca_5000_1000_1/BiasAdd/ReadVariableOp�+fmn10_pca_5000_1000_1/MatMul/ReadVariableOp�,fmn10_pca_5000_1000_2/BiasAdd/ReadVariableOp�+fmn10_pca_5000_1000_2/MatMul/ReadVariableOp�,fmn10_pca_5000_1000_3/BiasAdd/ReadVariableOp�+fmn10_pca_5000_1000_3/MatMul/ReadVariableOp�.fmn10_pca_5000_1000_out/BiasAdd/ReadVariableOp�-fmn10_pca_5000_1000_out/MatMul/ReadVariableOp�-fmn10_tsvd_5000_1000_1/BiasAdd/ReadVariableOp�,fmn10_tsvd_5000_1000_1/MatMul/ReadVariableOp�-fmn10_tsvd_5000_1000_2/BiasAdd/ReadVariableOp�,fmn10_tsvd_5000_1000_2/MatMul/ReadVariableOp�-fmn10_tsvd_5000_1000_3/BiasAdd/ReadVariableOp�,fmn10_tsvd_5000_1000_3/MatMul/ReadVariableOp�/fmn10_tsvd_5000_1000_out/BiasAdd/ReadVariableOp�.fmn10_tsvd_5000_1000_out/MatMul/ReadVariableOp�joined/BiasAdd/ReadVariableOp�joined/MatMul/ReadVariableOp�
+fmn10_lle_5000_1000_1/MatMul/ReadVariableOpReadVariableOp4fmn10_lle_5000_1000_1_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02-
+fmn10_lle_5000_1000_1/MatMul/ReadVariableOp�
fmn10_lle_5000_1000_1/MatMulMatMulinputs_33fmn10_lle_5000_1000_1/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn10_lle_5000_1000_1/MatMul�
,fmn10_lle_5000_1000_1/BiasAdd/ReadVariableOpReadVariableOp5fmn10_lle_5000_1000_1_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02.
,fmn10_lle_5000_1000_1/BiasAdd/ReadVariableOp�
fmn10_lle_5000_1000_1/BiasAddBiasAdd&fmn10_lle_5000_1000_1/MatMul:product:04fmn10_lle_5000_1000_1/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn10_lle_5000_1000_1/BiasAdd�
fmn10_lle_5000_1000_1/ReluRelu&fmn10_lle_5000_1000_1/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
fmn10_lle_5000_1000_1/Relu�
.fmn10_isomap_5000_1000_1/MatMul/ReadVariableOpReadVariableOp7fmn10_isomap_5000_1000_1_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype020
.fmn10_isomap_5000_1000_1/MatMul/ReadVariableOp�
fmn10_isomap_5000_1000_1/MatMulMatMulinputs_26fmn10_isomap_5000_1000_1/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2!
fmn10_isomap_5000_1000_1/MatMul�
/fmn10_isomap_5000_1000_1/BiasAdd/ReadVariableOpReadVariableOp8fmn10_isomap_5000_1000_1_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype021
/fmn10_isomap_5000_1000_1/BiasAdd/ReadVariableOp�
 fmn10_isomap_5000_1000_1/BiasAddBiasAdd)fmn10_isomap_5000_1000_1/MatMul:product:07fmn10_isomap_5000_1000_1/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2"
 fmn10_isomap_5000_1000_1/BiasAdd�
fmn10_isomap_5000_1000_1/ReluRelu)fmn10_isomap_5000_1000_1/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
fmn10_isomap_5000_1000_1/Relu�
,fmn10_tsvd_5000_1000_1/MatMul/ReadVariableOpReadVariableOp5fmn10_tsvd_5000_1000_1_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02.
,fmn10_tsvd_5000_1000_1/MatMul/ReadVariableOp�
fmn10_tsvd_5000_1000_1/MatMulMatMulinputs_14fmn10_tsvd_5000_1000_1/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn10_tsvd_5000_1000_1/MatMul�
-fmn10_tsvd_5000_1000_1/BiasAdd/ReadVariableOpReadVariableOp6fmn10_tsvd_5000_1000_1_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02/
-fmn10_tsvd_5000_1000_1/BiasAdd/ReadVariableOp�
fmn10_tsvd_5000_1000_1/BiasAddBiasAdd'fmn10_tsvd_5000_1000_1/MatMul:product:05fmn10_tsvd_5000_1000_1/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2 
fmn10_tsvd_5000_1000_1/BiasAdd�
fmn10_tsvd_5000_1000_1/ReluRelu'fmn10_tsvd_5000_1000_1/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
fmn10_tsvd_5000_1000_1/Relu�
+fmn10_pca_5000_1000_1/MatMul/ReadVariableOpReadVariableOp4fmn10_pca_5000_1000_1_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02-
+fmn10_pca_5000_1000_1/MatMul/ReadVariableOp�
fmn10_pca_5000_1000_1/MatMulMatMulinputs_03fmn10_pca_5000_1000_1/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn10_pca_5000_1000_1/MatMul�
,fmn10_pca_5000_1000_1/BiasAdd/ReadVariableOpReadVariableOp5fmn10_pca_5000_1000_1_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02.
,fmn10_pca_5000_1000_1/BiasAdd/ReadVariableOp�
fmn10_pca_5000_1000_1/BiasAddBiasAdd&fmn10_pca_5000_1000_1/MatMul:product:04fmn10_pca_5000_1000_1/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn10_pca_5000_1000_1/BiasAdd�
fmn10_pca_5000_1000_1/ReluRelu&fmn10_pca_5000_1000_1/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
fmn10_pca_5000_1000_1/Relu�
+fmn10_lle_5000_1000_2/MatMul/ReadVariableOpReadVariableOp4fmn10_lle_5000_1000_2_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02-
+fmn10_lle_5000_1000_2/MatMul/ReadVariableOp�
fmn10_lle_5000_1000_2/MatMulMatMul(fmn10_lle_5000_1000_1/Relu:activations:03fmn10_lle_5000_1000_2/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn10_lle_5000_1000_2/MatMul�
,fmn10_lle_5000_1000_2/BiasAdd/ReadVariableOpReadVariableOp5fmn10_lle_5000_1000_2_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02.
,fmn10_lle_5000_1000_2/BiasAdd/ReadVariableOp�
fmn10_lle_5000_1000_2/BiasAddBiasAdd&fmn10_lle_5000_1000_2/MatMul:product:04fmn10_lle_5000_1000_2/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn10_lle_5000_1000_2/BiasAdd�
fmn10_lle_5000_1000_2/ReluRelu&fmn10_lle_5000_1000_2/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
fmn10_lle_5000_1000_2/Relu�
.fmn10_isomap_5000_1000_2/MatMul/ReadVariableOpReadVariableOp7fmn10_isomap_5000_1000_2_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype020
.fmn10_isomap_5000_1000_2/MatMul/ReadVariableOp�
fmn10_isomap_5000_1000_2/MatMulMatMul+fmn10_isomap_5000_1000_1/Relu:activations:06fmn10_isomap_5000_1000_2/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2!
fmn10_isomap_5000_1000_2/MatMul�
/fmn10_isomap_5000_1000_2/BiasAdd/ReadVariableOpReadVariableOp8fmn10_isomap_5000_1000_2_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype021
/fmn10_isomap_5000_1000_2/BiasAdd/ReadVariableOp�
 fmn10_isomap_5000_1000_2/BiasAddBiasAdd)fmn10_isomap_5000_1000_2/MatMul:product:07fmn10_isomap_5000_1000_2/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2"
 fmn10_isomap_5000_1000_2/BiasAdd�
fmn10_isomap_5000_1000_2/ReluRelu)fmn10_isomap_5000_1000_2/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
fmn10_isomap_5000_1000_2/Relu�
,fmn10_tsvd_5000_1000_2/MatMul/ReadVariableOpReadVariableOp5fmn10_tsvd_5000_1000_2_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02.
,fmn10_tsvd_5000_1000_2/MatMul/ReadVariableOp�
fmn10_tsvd_5000_1000_2/MatMulMatMul)fmn10_tsvd_5000_1000_1/Relu:activations:04fmn10_tsvd_5000_1000_2/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn10_tsvd_5000_1000_2/MatMul�
-fmn10_tsvd_5000_1000_2/BiasAdd/ReadVariableOpReadVariableOp6fmn10_tsvd_5000_1000_2_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02/
-fmn10_tsvd_5000_1000_2/BiasAdd/ReadVariableOp�
fmn10_tsvd_5000_1000_2/BiasAddBiasAdd'fmn10_tsvd_5000_1000_2/MatMul:product:05fmn10_tsvd_5000_1000_2/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2 
fmn10_tsvd_5000_1000_2/BiasAdd�
fmn10_tsvd_5000_1000_2/ReluRelu'fmn10_tsvd_5000_1000_2/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
fmn10_tsvd_5000_1000_2/Relu�
+fmn10_pca_5000_1000_2/MatMul/ReadVariableOpReadVariableOp4fmn10_pca_5000_1000_2_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02-
+fmn10_pca_5000_1000_2/MatMul/ReadVariableOp�
fmn10_pca_5000_1000_2/MatMulMatMul(fmn10_pca_5000_1000_1/Relu:activations:03fmn10_pca_5000_1000_2/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn10_pca_5000_1000_2/MatMul�
,fmn10_pca_5000_1000_2/BiasAdd/ReadVariableOpReadVariableOp5fmn10_pca_5000_1000_2_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02.
,fmn10_pca_5000_1000_2/BiasAdd/ReadVariableOp�
fmn10_pca_5000_1000_2/BiasAddBiasAdd&fmn10_pca_5000_1000_2/MatMul:product:04fmn10_pca_5000_1000_2/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn10_pca_5000_1000_2/BiasAdd�
fmn10_pca_5000_1000_2/ReluRelu&fmn10_pca_5000_1000_2/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
fmn10_pca_5000_1000_2/Relu�
+fmn10_lle_5000_1000_3/MatMul/ReadVariableOpReadVariableOp4fmn10_lle_5000_1000_3_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02-
+fmn10_lle_5000_1000_3/MatMul/ReadVariableOp�
fmn10_lle_5000_1000_3/MatMulMatMul(fmn10_lle_5000_1000_2/Relu:activations:03fmn10_lle_5000_1000_3/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn10_lle_5000_1000_3/MatMul�
,fmn10_lle_5000_1000_3/BiasAdd/ReadVariableOpReadVariableOp5fmn10_lle_5000_1000_3_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02.
,fmn10_lle_5000_1000_3/BiasAdd/ReadVariableOp�
fmn10_lle_5000_1000_3/BiasAddBiasAdd&fmn10_lle_5000_1000_3/MatMul:product:04fmn10_lle_5000_1000_3/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn10_lle_5000_1000_3/BiasAdd�
fmn10_lle_5000_1000_3/ReluRelu&fmn10_lle_5000_1000_3/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
fmn10_lle_5000_1000_3/Relu�
.fmn10_isomap_5000_1000_3/MatMul/ReadVariableOpReadVariableOp7fmn10_isomap_5000_1000_3_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype020
.fmn10_isomap_5000_1000_3/MatMul/ReadVariableOp�
fmn10_isomap_5000_1000_3/MatMulMatMul+fmn10_isomap_5000_1000_2/Relu:activations:06fmn10_isomap_5000_1000_3/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2!
fmn10_isomap_5000_1000_3/MatMul�
/fmn10_isomap_5000_1000_3/BiasAdd/ReadVariableOpReadVariableOp8fmn10_isomap_5000_1000_3_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype021
/fmn10_isomap_5000_1000_3/BiasAdd/ReadVariableOp�
 fmn10_isomap_5000_1000_3/BiasAddBiasAdd)fmn10_isomap_5000_1000_3/MatMul:product:07fmn10_isomap_5000_1000_3/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2"
 fmn10_isomap_5000_1000_3/BiasAdd�
fmn10_isomap_5000_1000_3/ReluRelu)fmn10_isomap_5000_1000_3/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
fmn10_isomap_5000_1000_3/Relu�
,fmn10_tsvd_5000_1000_3/MatMul/ReadVariableOpReadVariableOp5fmn10_tsvd_5000_1000_3_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02.
,fmn10_tsvd_5000_1000_3/MatMul/ReadVariableOp�
fmn10_tsvd_5000_1000_3/MatMulMatMul)fmn10_tsvd_5000_1000_2/Relu:activations:04fmn10_tsvd_5000_1000_3/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn10_tsvd_5000_1000_3/MatMul�
-fmn10_tsvd_5000_1000_3/BiasAdd/ReadVariableOpReadVariableOp6fmn10_tsvd_5000_1000_3_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02/
-fmn10_tsvd_5000_1000_3/BiasAdd/ReadVariableOp�
fmn10_tsvd_5000_1000_3/BiasAddBiasAdd'fmn10_tsvd_5000_1000_3/MatMul:product:05fmn10_tsvd_5000_1000_3/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2 
fmn10_tsvd_5000_1000_3/BiasAdd�
fmn10_tsvd_5000_1000_3/ReluRelu'fmn10_tsvd_5000_1000_3/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
fmn10_tsvd_5000_1000_3/Relu�
+fmn10_pca_5000_1000_3/MatMul/ReadVariableOpReadVariableOp4fmn10_pca_5000_1000_3_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02-
+fmn10_pca_5000_1000_3/MatMul/ReadVariableOp�
fmn10_pca_5000_1000_3/MatMulMatMul(fmn10_pca_5000_1000_2/Relu:activations:03fmn10_pca_5000_1000_3/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn10_pca_5000_1000_3/MatMul�
,fmn10_pca_5000_1000_3/BiasAdd/ReadVariableOpReadVariableOp5fmn10_pca_5000_1000_3_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02.
,fmn10_pca_5000_1000_3/BiasAdd/ReadVariableOp�
fmn10_pca_5000_1000_3/BiasAddBiasAdd&fmn10_pca_5000_1000_3/MatMul:product:04fmn10_pca_5000_1000_3/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn10_pca_5000_1000_3/BiasAdd�
fmn10_pca_5000_1000_3/ReluRelu&fmn10_pca_5000_1000_3/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
fmn10_pca_5000_1000_3/Relu�
-fmn10_pca_5000_1000_out/MatMul/ReadVariableOpReadVariableOp6fmn10_pca_5000_1000_out_matmul_readvariableop_resource*
_output_shapes
:	�*
dtype02/
-fmn10_pca_5000_1000_out/MatMul/ReadVariableOp�
fmn10_pca_5000_1000_out/MatMulMatMul(fmn10_pca_5000_1000_3/Relu:activations:05fmn10_pca_5000_1000_out/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2 
fmn10_pca_5000_1000_out/MatMul�
.fmn10_pca_5000_1000_out/BiasAdd/ReadVariableOpReadVariableOp7fmn10_pca_5000_1000_out_biasadd_readvariableop_resource*
_output_shapes
:*
dtype020
.fmn10_pca_5000_1000_out/BiasAdd/ReadVariableOp�
fmn10_pca_5000_1000_out/BiasAddBiasAdd(fmn10_pca_5000_1000_out/MatMul:product:06fmn10_pca_5000_1000_out/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2!
fmn10_pca_5000_1000_out/BiasAdd�
fmn10_pca_5000_1000_out/SigmoidSigmoid(fmn10_pca_5000_1000_out/BiasAdd:output:0*
T0*'
_output_shapes
:���������2!
fmn10_pca_5000_1000_out/Sigmoid�
.fmn10_tsvd_5000_1000_out/MatMul/ReadVariableOpReadVariableOp7fmn10_tsvd_5000_1000_out_matmul_readvariableop_resource*
_output_shapes
:	�*
dtype020
.fmn10_tsvd_5000_1000_out/MatMul/ReadVariableOp�
fmn10_tsvd_5000_1000_out/MatMulMatMul)fmn10_tsvd_5000_1000_3/Relu:activations:06fmn10_tsvd_5000_1000_out/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2!
fmn10_tsvd_5000_1000_out/MatMul�
/fmn10_tsvd_5000_1000_out/BiasAdd/ReadVariableOpReadVariableOp8fmn10_tsvd_5000_1000_out_biasadd_readvariableop_resource*
_output_shapes
:*
dtype021
/fmn10_tsvd_5000_1000_out/BiasAdd/ReadVariableOp�
 fmn10_tsvd_5000_1000_out/BiasAddBiasAdd)fmn10_tsvd_5000_1000_out/MatMul:product:07fmn10_tsvd_5000_1000_out/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2"
 fmn10_tsvd_5000_1000_out/BiasAdd�
 fmn10_tsvd_5000_1000_out/SigmoidSigmoid)fmn10_tsvd_5000_1000_out/BiasAdd:output:0*
T0*'
_output_shapes
:���������2"
 fmn10_tsvd_5000_1000_out/Sigmoid�
0fmn10_isomap_5000_1000_out/MatMul/ReadVariableOpReadVariableOp9fmn10_isomap_5000_1000_out_matmul_readvariableop_resource*
_output_shapes
:	�*
dtype022
0fmn10_isomap_5000_1000_out/MatMul/ReadVariableOp�
!fmn10_isomap_5000_1000_out/MatMulMatMul+fmn10_isomap_5000_1000_3/Relu:activations:08fmn10_isomap_5000_1000_out/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2#
!fmn10_isomap_5000_1000_out/MatMul�
1fmn10_isomap_5000_1000_out/BiasAdd/ReadVariableOpReadVariableOp:fmn10_isomap_5000_1000_out_biasadd_readvariableop_resource*
_output_shapes
:*
dtype023
1fmn10_isomap_5000_1000_out/BiasAdd/ReadVariableOp�
"fmn10_isomap_5000_1000_out/BiasAddBiasAdd+fmn10_isomap_5000_1000_out/MatMul:product:09fmn10_isomap_5000_1000_out/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2$
"fmn10_isomap_5000_1000_out/BiasAdd�
"fmn10_isomap_5000_1000_out/SigmoidSigmoid+fmn10_isomap_5000_1000_out/BiasAdd:output:0*
T0*'
_output_shapes
:���������2$
"fmn10_isomap_5000_1000_out/Sigmoid�
-fmn10_lle_5000_1000_out/MatMul/ReadVariableOpReadVariableOp6fmn10_lle_5000_1000_out_matmul_readvariableop_resource*
_output_shapes
:	�*
dtype02/
-fmn10_lle_5000_1000_out/MatMul/ReadVariableOp�
fmn10_lle_5000_1000_out/MatMulMatMul(fmn10_lle_5000_1000_3/Relu:activations:05fmn10_lle_5000_1000_out/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2 
fmn10_lle_5000_1000_out/MatMul�
.fmn10_lle_5000_1000_out/BiasAdd/ReadVariableOpReadVariableOp7fmn10_lle_5000_1000_out_biasadd_readvariableop_resource*
_output_shapes
:*
dtype020
.fmn10_lle_5000_1000_out/BiasAdd/ReadVariableOp�
fmn10_lle_5000_1000_out/BiasAddBiasAdd(fmn10_lle_5000_1000_out/MatMul:product:06fmn10_lle_5000_1000_out/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2!
fmn10_lle_5000_1000_out/BiasAdd�
fmn10_lle_5000_1000_out/SigmoidSigmoid(fmn10_lle_5000_1000_out/BiasAdd:output:0*
T0*'
_output_shapes
:���������2!
fmn10_lle_5000_1000_out/Sigmoidz
concatenate_68/concat/axisConst*
_output_shapes
: *
dtype0*
value	B :2
concatenate_68/concat/axis�
concatenate_68/concatConcatV2#fmn10_pca_5000_1000_out/Sigmoid:y:0$fmn10_tsvd_5000_1000_out/Sigmoid:y:0&fmn10_isomap_5000_1000_out/Sigmoid:y:0#fmn10_lle_5000_1000_out/Sigmoid:y:0#concatenate_68/concat/axis:output:0*
N*
T0*'
_output_shapes
:���������2
concatenate_68/concat�
joined/MatMul/ReadVariableOpReadVariableOp%joined_matmul_readvariableop_resource*
_output_shapes

:*
dtype02
joined/MatMul/ReadVariableOp�
joined/MatMulMatMulconcatenate_68/concat:output:0$joined/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
joined/MatMul�
joined/BiasAdd/ReadVariableOpReadVariableOp&joined_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02
joined/BiasAdd/ReadVariableOp�
joined/BiasAddBiasAddjoined/MatMul:product:0%joined/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
joined/BiasAddv
joined/SigmoidSigmoidjoined/BiasAdd:output:0*
T0*'
_output_shapes
:���������2
joined/Sigmoidm
IdentityIdentityjoined/Sigmoid:y:0^NoOp*
T0*'
_output_shapes
:���������2

Identity�
NoOpNoOp0^fmn10_isomap_5000_1000_1/BiasAdd/ReadVariableOp/^fmn10_isomap_5000_1000_1/MatMul/ReadVariableOp0^fmn10_isomap_5000_1000_2/BiasAdd/ReadVariableOp/^fmn10_isomap_5000_1000_2/MatMul/ReadVariableOp0^fmn10_isomap_5000_1000_3/BiasAdd/ReadVariableOp/^fmn10_isomap_5000_1000_3/MatMul/ReadVariableOp2^fmn10_isomap_5000_1000_out/BiasAdd/ReadVariableOp1^fmn10_isomap_5000_1000_out/MatMul/ReadVariableOp-^fmn10_lle_5000_1000_1/BiasAdd/ReadVariableOp,^fmn10_lle_5000_1000_1/MatMul/ReadVariableOp-^fmn10_lle_5000_1000_2/BiasAdd/ReadVariableOp,^fmn10_lle_5000_1000_2/MatMul/ReadVariableOp-^fmn10_lle_5000_1000_3/BiasAdd/ReadVariableOp,^fmn10_lle_5000_1000_3/MatMul/ReadVariableOp/^fmn10_lle_5000_1000_out/BiasAdd/ReadVariableOp.^fmn10_lle_5000_1000_out/MatMul/ReadVariableOp-^fmn10_pca_5000_1000_1/BiasAdd/ReadVariableOp,^fmn10_pca_5000_1000_1/MatMul/ReadVariableOp-^fmn10_pca_5000_1000_2/BiasAdd/ReadVariableOp,^fmn10_pca_5000_1000_2/MatMul/ReadVariableOp-^fmn10_pca_5000_1000_3/BiasAdd/ReadVariableOp,^fmn10_pca_5000_1000_3/MatMul/ReadVariableOp/^fmn10_pca_5000_1000_out/BiasAdd/ReadVariableOp.^fmn10_pca_5000_1000_out/MatMul/ReadVariableOp.^fmn10_tsvd_5000_1000_1/BiasAdd/ReadVariableOp-^fmn10_tsvd_5000_1000_1/MatMul/ReadVariableOp.^fmn10_tsvd_5000_1000_2/BiasAdd/ReadVariableOp-^fmn10_tsvd_5000_1000_2/MatMul/ReadVariableOp.^fmn10_tsvd_5000_1000_3/BiasAdd/ReadVariableOp-^fmn10_tsvd_5000_1000_3/MatMul/ReadVariableOp0^fmn10_tsvd_5000_1000_out/BiasAdd/ReadVariableOp/^fmn10_tsvd_5000_1000_out/MatMul/ReadVariableOp^joined/BiasAdd/ReadVariableOp^joined/MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*�
_input_shapes�
�:����������:����������:����������:����������: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 2b
/fmn10_isomap_5000_1000_1/BiasAdd/ReadVariableOp/fmn10_isomap_5000_1000_1/BiasAdd/ReadVariableOp2`
.fmn10_isomap_5000_1000_1/MatMul/ReadVariableOp.fmn10_isomap_5000_1000_1/MatMul/ReadVariableOp2b
/fmn10_isomap_5000_1000_2/BiasAdd/ReadVariableOp/fmn10_isomap_5000_1000_2/BiasAdd/ReadVariableOp2`
.fmn10_isomap_5000_1000_2/MatMul/ReadVariableOp.fmn10_isomap_5000_1000_2/MatMul/ReadVariableOp2b
/fmn10_isomap_5000_1000_3/BiasAdd/ReadVariableOp/fmn10_isomap_5000_1000_3/BiasAdd/ReadVariableOp2`
.fmn10_isomap_5000_1000_3/MatMul/ReadVariableOp.fmn10_isomap_5000_1000_3/MatMul/ReadVariableOp2f
1fmn10_isomap_5000_1000_out/BiasAdd/ReadVariableOp1fmn10_isomap_5000_1000_out/BiasAdd/ReadVariableOp2d
0fmn10_isomap_5000_1000_out/MatMul/ReadVariableOp0fmn10_isomap_5000_1000_out/MatMul/ReadVariableOp2\
,fmn10_lle_5000_1000_1/BiasAdd/ReadVariableOp,fmn10_lle_5000_1000_1/BiasAdd/ReadVariableOp2Z
+fmn10_lle_5000_1000_1/MatMul/ReadVariableOp+fmn10_lle_5000_1000_1/MatMul/ReadVariableOp2\
,fmn10_lle_5000_1000_2/BiasAdd/ReadVariableOp,fmn10_lle_5000_1000_2/BiasAdd/ReadVariableOp2Z
+fmn10_lle_5000_1000_2/MatMul/ReadVariableOp+fmn10_lle_5000_1000_2/MatMul/ReadVariableOp2\
,fmn10_lle_5000_1000_3/BiasAdd/ReadVariableOp,fmn10_lle_5000_1000_3/BiasAdd/ReadVariableOp2Z
+fmn10_lle_5000_1000_3/MatMul/ReadVariableOp+fmn10_lle_5000_1000_3/MatMul/ReadVariableOp2`
.fmn10_lle_5000_1000_out/BiasAdd/ReadVariableOp.fmn10_lle_5000_1000_out/BiasAdd/ReadVariableOp2^
-fmn10_lle_5000_1000_out/MatMul/ReadVariableOp-fmn10_lle_5000_1000_out/MatMul/ReadVariableOp2\
,fmn10_pca_5000_1000_1/BiasAdd/ReadVariableOp,fmn10_pca_5000_1000_1/BiasAdd/ReadVariableOp2Z
+fmn10_pca_5000_1000_1/MatMul/ReadVariableOp+fmn10_pca_5000_1000_1/MatMul/ReadVariableOp2\
,fmn10_pca_5000_1000_2/BiasAdd/ReadVariableOp,fmn10_pca_5000_1000_2/BiasAdd/ReadVariableOp2Z
+fmn10_pca_5000_1000_2/MatMul/ReadVariableOp+fmn10_pca_5000_1000_2/MatMul/ReadVariableOp2\
,fmn10_pca_5000_1000_3/BiasAdd/ReadVariableOp,fmn10_pca_5000_1000_3/BiasAdd/ReadVariableOp2Z
+fmn10_pca_5000_1000_3/MatMul/ReadVariableOp+fmn10_pca_5000_1000_3/MatMul/ReadVariableOp2`
.fmn10_pca_5000_1000_out/BiasAdd/ReadVariableOp.fmn10_pca_5000_1000_out/BiasAdd/ReadVariableOp2^
-fmn10_pca_5000_1000_out/MatMul/ReadVariableOp-fmn10_pca_5000_1000_out/MatMul/ReadVariableOp2^
-fmn10_tsvd_5000_1000_1/BiasAdd/ReadVariableOp-fmn10_tsvd_5000_1000_1/BiasAdd/ReadVariableOp2\
,fmn10_tsvd_5000_1000_1/MatMul/ReadVariableOp,fmn10_tsvd_5000_1000_1/MatMul/ReadVariableOp2^
-fmn10_tsvd_5000_1000_2/BiasAdd/ReadVariableOp-fmn10_tsvd_5000_1000_2/BiasAdd/ReadVariableOp2\
,fmn10_tsvd_5000_1000_2/MatMul/ReadVariableOp,fmn10_tsvd_5000_1000_2/MatMul/ReadVariableOp2^
-fmn10_tsvd_5000_1000_3/BiasAdd/ReadVariableOp-fmn10_tsvd_5000_1000_3/BiasAdd/ReadVariableOp2\
,fmn10_tsvd_5000_1000_3/MatMul/ReadVariableOp,fmn10_tsvd_5000_1000_3/MatMul/ReadVariableOp2b
/fmn10_tsvd_5000_1000_out/BiasAdd/ReadVariableOp/fmn10_tsvd_5000_1000_out/BiasAdd/ReadVariableOp2`
.fmn10_tsvd_5000_1000_out/MatMul/ReadVariableOp.fmn10_tsvd_5000_1000_out/MatMul/ReadVariableOp2>
joined/BiasAdd/ReadVariableOpjoined/BiasAdd/ReadVariableOp2<
joined/MatMul/ReadVariableOpjoined/MatMul/ReadVariableOp:R N
(
_output_shapes
:����������
"
_user_specified_name
inputs/0:RN
(
_output_shapes
:����������
"
_user_specified_name
inputs/1:RN
(
_output_shapes
:����������
"
_user_specified_name
inputs/2:RN
(
_output_shapes
:����������
"
_user_specified_name
inputs/3
�
�
U__inference_fmn10_isomap_5000_1000_2_layer_call_and_return_conditional_losses_2310075

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
:__inference_fmn10_isomap_5000_1000_1_layer_call_fn_2309984

inputs
unknown:
��
	unknown_0:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *^
fYRW
U__inference_fmn10_isomap_5000_1000_1_layer_call_and_return_conditional_losses_23084132
StatefulPartitionedCall|
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:����������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs"�L
saver_filename:0StatefulPartitionedCall_1:0StatefulPartitionedCall_28"
saved_model_main_op

NoOp*>
__saved_model_init_op%#
__saved_model_init_op

NoOp*�
serving_default�
f
fmn10_isomap_5000_1000_inputF
.serving_default_fmn10_isomap_5000_1000_input:0����������
`
fmn10_lle_5000_1000_inputC
+serving_default_fmn10_lle_5000_1000_input:0����������
`
fmn10_pca_5000_1000_inputC
+serving_default_fmn10_pca_5000_1000_input:0����������
b
fmn10_tsvd_5000_1000_inputD
,serving_default_fmn10_tsvd_5000_1000_input:0����������:
joined0
StatefulPartitionedCall:0���������tensorflow/serving/predict:��
�
layer-0
layer-1
layer-2
layer-3
layer_with_weights-0
layer-4
layer_with_weights-1
layer-5
layer_with_weights-2
layer-6
layer_with_weights-3
layer-7
	layer_with_weights-4
	layer-8

layer_with_weights-5

layer-9
layer_with_weights-6
layer-10
layer_with_weights-7
layer-11
layer_with_weights-8
layer-12
layer_with_weights-9
layer-13
layer_with_weights-10
layer-14
layer_with_weights-11
layer-15
layer_with_weights-12
layer-16
layer_with_weights-13
layer-17
layer_with_weights-14
layer-18
layer_with_weights-15
layer-19
layer-20
layer_with_weights-16
layer-21
	optimizer
	variables
regularization_losses
trainable_variables
	keras_api

signatures
�__call__
�_default_save_signature
+�&call_and_return_all_conditional_losses"
_tf_keras_network
D
#_self_saveable_object_factories"
_tf_keras_input_layer
D
#_self_saveable_object_factories"
_tf_keras_input_layer
D
#_self_saveable_object_factories"
_tf_keras_input_layer
"
_tf_keras_input_layer
�

 kernel
!bias
#"_self_saveable_object_factories
#	variables
$regularization_losses
%trainable_variables
&	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�

'kernel
(bias
#)_self_saveable_object_factories
*	variables
+regularization_losses
,trainable_variables
-	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�

.kernel
/bias
#0_self_saveable_object_factories
1	variables
2regularization_losses
3trainable_variables
4	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�

5kernel
6bias
7	variables
8regularization_losses
9trainable_variables
:	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�

;kernel
<bias
#=_self_saveable_object_factories
>	variables
?regularization_losses
@trainable_variables
A	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�

Bkernel
Cbias
#D_self_saveable_object_factories
E	variables
Fregularization_losses
Gtrainable_variables
H	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�

Ikernel
Jbias
#K_self_saveable_object_factories
L	variables
Mregularization_losses
Ntrainable_variables
O	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�

Pkernel
Qbias
R	variables
Sregularization_losses
Ttrainable_variables
U	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�

Vkernel
Wbias
#X_self_saveable_object_factories
Y	variables
Zregularization_losses
[trainable_variables
\	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�

]kernel
^bias
#__self_saveable_object_factories
`	variables
aregularization_losses
btrainable_variables
c	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�

dkernel
ebias
#f_self_saveable_object_factories
g	variables
hregularization_losses
itrainable_variables
j	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�

kkernel
lbias
m	variables
nregularization_losses
otrainable_variables
p	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�

qkernel
rbias
#s_self_saveable_object_factories
t	variables
uregularization_losses
vtrainable_variables
w	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�

xkernel
ybias
#z_self_saveable_object_factories
{	variables
|regularization_losses
}trainable_variables
~	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�

kernel
	�bias
$�_self_saveable_object_factories
�	variables
�regularization_losses
�trainable_variables
�	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�
�kernel
	�bias
�	variables
�regularization_losses
�trainable_variables
�	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�
�	variables
�regularization_losses
�trainable_variables
�	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�
�kernel
	�bias
�	variables
�regularization_losses
�trainable_variables
�	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�
	�iter
�beta_1
�beta_2

�decay
�learning_rate	�m�	�m�	�v�	�v�"
	optimizer
�
 0
!1
'2
(3
.4
/5
56
67
;8
<9
B10
C11
I12
J13
P14
Q15
V16
W17
]18
^19
d20
e21
k22
l23
q24
r25
x26
y27
28
�29
�30
�31
�32
�33"
trackable_list_wrapper
 "
trackable_list_wrapper
0
�0
�1"
trackable_list_wrapper
�
�metrics
�layers
	variables
�layer_metrics
 �layer_regularization_losses
regularization_losses
trainable_variables
�non_trainable_variables
�__call__
�_default_save_signature
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
-
�serving_default"
signature_map
 "
trackable_dict_wrapper
 "
trackable_dict_wrapper
 "
trackable_dict_wrapper
0:.
��2fmn10_pca_5000_1000_1/kernel
):'�2fmn10_pca_5000_1000_1/bias
 "
trackable_dict_wrapper
.
 0
!1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�metrics
�layers
#	variables
$regularization_losses
�layer_metrics
 �layer_regularization_losses
%trainable_variables
�non_trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
1:/
��2fmn10_tsvd_5000_1000_1/kernel
*:(�2fmn10_tsvd_5000_1000_1/bias
 "
trackable_dict_wrapper
.
'0
(1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�metrics
�layers
*	variables
+regularization_losses
�layer_metrics
 �layer_regularization_losses
,trainable_variables
�non_trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
3:1
��2fmn10_isomap_5000_1000_1/kernel
,:*�2fmn10_isomap_5000_1000_1/bias
 "
trackable_dict_wrapper
.
.0
/1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�metrics
�layers
1	variables
2regularization_losses
�layer_metrics
 �layer_regularization_losses
3trainable_variables
�non_trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
0:.
��2fmn10_lle_5000_1000_1/kernel
):'�2fmn10_lle_5000_1000_1/bias
.
50
61"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�metrics
�layers
7	variables
8regularization_losses
�layer_metrics
 �layer_regularization_losses
9trainable_variables
�non_trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
0:.
��2fmn10_pca_5000_1000_2/kernel
):'�2fmn10_pca_5000_1000_2/bias
 "
trackable_dict_wrapper
.
;0
<1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�metrics
�layers
>	variables
?regularization_losses
�layer_metrics
 �layer_regularization_losses
@trainable_variables
�non_trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
1:/
��2fmn10_tsvd_5000_1000_2/kernel
*:(�2fmn10_tsvd_5000_1000_2/bias
 "
trackable_dict_wrapper
.
B0
C1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�metrics
�layers
E	variables
Fregularization_losses
�layer_metrics
 �layer_regularization_losses
Gtrainable_variables
�non_trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
3:1
��2fmn10_isomap_5000_1000_2/kernel
,:*�2fmn10_isomap_5000_1000_2/bias
 "
trackable_dict_wrapper
.
I0
J1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�metrics
�layers
L	variables
Mregularization_losses
�layer_metrics
 �layer_regularization_losses
Ntrainable_variables
�non_trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
0:.
��2fmn10_lle_5000_1000_2/kernel
):'�2fmn10_lle_5000_1000_2/bias
.
P0
Q1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�metrics
�layers
R	variables
Sregularization_losses
�layer_metrics
 �layer_regularization_losses
Ttrainable_variables
�non_trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
0:.
��2fmn10_pca_5000_1000_3/kernel
):'�2fmn10_pca_5000_1000_3/bias
 "
trackable_dict_wrapper
.
V0
W1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�metrics
�layers
Y	variables
Zregularization_losses
�layer_metrics
 �layer_regularization_losses
[trainable_variables
�non_trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
1:/
��2fmn10_tsvd_5000_1000_3/kernel
*:(�2fmn10_tsvd_5000_1000_3/bias
 "
trackable_dict_wrapper
.
]0
^1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�metrics
�layers
`	variables
aregularization_losses
�layer_metrics
 �layer_regularization_losses
btrainable_variables
�non_trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
3:1
��2fmn10_isomap_5000_1000_3/kernel
,:*�2fmn10_isomap_5000_1000_3/bias
 "
trackable_dict_wrapper
.
d0
e1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�metrics
�layers
g	variables
hregularization_losses
�layer_metrics
 �layer_regularization_losses
itrainable_variables
�non_trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
0:.
��2fmn10_lle_5000_1000_3/kernel
):'�2fmn10_lle_5000_1000_3/bias
.
k0
l1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�metrics
�layers
m	variables
nregularization_losses
�layer_metrics
 �layer_regularization_losses
otrainable_variables
�non_trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
1:/	�2fmn10_pca_5000_1000_out/kernel
*:(2fmn10_pca_5000_1000_out/bias
 "
trackable_dict_wrapper
.
q0
r1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�metrics
�layers
t	variables
uregularization_losses
�layer_metrics
 �layer_regularization_losses
vtrainable_variables
�non_trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
2:0	�2fmn10_tsvd_5000_1000_out/kernel
+:)2fmn10_tsvd_5000_1000_out/bias
 "
trackable_dict_wrapper
.
x0
y1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�metrics
�layers
{	variables
|regularization_losses
�layer_metrics
 �layer_regularization_losses
}trainable_variables
�non_trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
4:2	�2!fmn10_isomap_5000_1000_out/kernel
-:+2fmn10_isomap_5000_1000_out/bias
 "
trackable_dict_wrapper
/
0
�1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�metrics
�layers
�	variables
�regularization_losses
�layer_metrics
 �layer_regularization_losses
�trainable_variables
�non_trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
1:/	�2fmn10_lle_5000_1000_out/kernel
*:(2fmn10_lle_5000_1000_out/bias
0
�0
�1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�metrics
�layers
�	variables
�regularization_losses
�layer_metrics
 �layer_regularization_losses
�trainable_variables
�non_trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�metrics
�layers
�	variables
�regularization_losses
�layer_metrics
 �layer_regularization_losses
�trainable_variables
�non_trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
:2joined/kernel
:2joined/bias
0
�0
�1"
trackable_list_wrapper
 "
trackable_list_wrapper
0
�0
�1"
trackable_list_wrapper
�
�metrics
�layers
�	variables
�regularization_losses
�layer_metrics
 �layer_regularization_losses
�trainable_variables
�non_trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
:	 (2	Adam/iter
: (2Adam/beta_1
: (2Adam/beta_2
: (2
Adam/decay
: (2Adam/learning_rate
0
�0
�1"
trackable_list_wrapper
�
0
1
2
3
4
5
6
7
	8

9
10
11
12
13
14
15
16
17
18
19
20
21"
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
�
 0
!1
'2
(3
.4
/5
56
67
;8
<9
B10
C11
I12
J13
P14
Q15
V16
W17
]18
^19
d20
e21
k22
l23
q24
r25
x26
y27
28
�29
�30
�31"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
.
 0
!1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
.
'0
(1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
.
.0
/1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
.
50
61"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
.
;0
<1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
.
B0
C1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
.
I0
J1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
.
P0
Q1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
.
V0
W1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
.
]0
^1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
.
d0
e1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
.
k0
l1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
.
q0
r1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
.
x0
y1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
/
0
�1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
0
�0
�1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
R

�total

�count
�	variables
�	keras_api"
_tf_keras_metric
c

�total

�count
�
_fn_kwargs
�	variables
�	keras_api"
_tf_keras_metric
:  (2total
:  (2count
0
�0
�1"
trackable_list_wrapper
.
�	variables"
_generic_user_object
:  (2total
:  (2count
 "
trackable_dict_wrapper
0
�0
�1"
trackable_list_wrapper
.
�	variables"
_generic_user_object
$:"2Adam/joined/kernel/m
:2Adam/joined/bias/m
$:"2Adam/joined/kernel/v
:2Adam/joined/bias/v
�2�
'__inference_model_layer_call_fn_2308757
'__inference_model_layer_call_fn_2309603
'__inference_model_layer_call_fn_2309679
'__inference_model_layer_call_fn_2309257�
���
FullArgSpec1
args)�&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�B�
"__inference__wrapped_model_2308372fmn10_pca_5000_1000_inputfmn10_tsvd_5000_1000_inputfmn10_isomap_5000_1000_inputfmn10_lle_5000_1000_input"�
���
FullArgSpec
args� 
varargsjargs
varkwjkwargs
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
B__inference_model_layer_call_and_return_conditional_losses_2309807
B__inference_model_layer_call_and_return_conditional_losses_2309935
B__inference_model_layer_call_and_return_conditional_losses_2309350
B__inference_model_layer_call_and_return_conditional_losses_2309443�
���
FullArgSpec1
args)�&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2�
7__inference_fmn10_pca_5000_1000_1_layer_call_fn_2309944�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
R__inference_fmn10_pca_5000_1000_1_layer_call_and_return_conditional_losses_2309955�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
8__inference_fmn10_tsvd_5000_1000_1_layer_call_fn_2309964�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
S__inference_fmn10_tsvd_5000_1000_1_layer_call_and_return_conditional_losses_2309975�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
:__inference_fmn10_isomap_5000_1000_1_layer_call_fn_2309984�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
U__inference_fmn10_isomap_5000_1000_1_layer_call_and_return_conditional_losses_2309995�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
7__inference_fmn10_lle_5000_1000_1_layer_call_fn_2310004�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
R__inference_fmn10_lle_5000_1000_1_layer_call_and_return_conditional_losses_2310015�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
7__inference_fmn10_pca_5000_1000_2_layer_call_fn_2310024�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
R__inference_fmn10_pca_5000_1000_2_layer_call_and_return_conditional_losses_2310035�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
8__inference_fmn10_tsvd_5000_1000_2_layer_call_fn_2310044�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
S__inference_fmn10_tsvd_5000_1000_2_layer_call_and_return_conditional_losses_2310055�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
:__inference_fmn10_isomap_5000_1000_2_layer_call_fn_2310064�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
U__inference_fmn10_isomap_5000_1000_2_layer_call_and_return_conditional_losses_2310075�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
7__inference_fmn10_lle_5000_1000_2_layer_call_fn_2310084�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
R__inference_fmn10_lle_5000_1000_2_layer_call_and_return_conditional_losses_2310095�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
7__inference_fmn10_pca_5000_1000_3_layer_call_fn_2310104�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
R__inference_fmn10_pca_5000_1000_3_layer_call_and_return_conditional_losses_2310115�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
8__inference_fmn10_tsvd_5000_1000_3_layer_call_fn_2310124�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
S__inference_fmn10_tsvd_5000_1000_3_layer_call_and_return_conditional_losses_2310135�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
:__inference_fmn10_isomap_5000_1000_3_layer_call_fn_2310144�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
U__inference_fmn10_isomap_5000_1000_3_layer_call_and_return_conditional_losses_2310155�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
7__inference_fmn10_lle_5000_1000_3_layer_call_fn_2310164�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
R__inference_fmn10_lle_5000_1000_3_layer_call_and_return_conditional_losses_2310175�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
9__inference_fmn10_pca_5000_1000_out_layer_call_fn_2310184�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
T__inference_fmn10_pca_5000_1000_out_layer_call_and_return_conditional_losses_2310195�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
:__inference_fmn10_tsvd_5000_1000_out_layer_call_fn_2310204�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
U__inference_fmn10_tsvd_5000_1000_out_layer_call_and_return_conditional_losses_2310215�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
<__inference_fmn10_isomap_5000_1000_out_layer_call_fn_2310224�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
W__inference_fmn10_isomap_5000_1000_out_layer_call_and_return_conditional_losses_2310235�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
9__inference_fmn10_lle_5000_1000_out_layer_call_fn_2310244�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
T__inference_fmn10_lle_5000_1000_out_layer_call_and_return_conditional_losses_2310255�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
0__inference_concatenate_68_layer_call_fn_2310263�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
K__inference_concatenate_68_layer_call_and_return_conditional_losses_2310272�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
(__inference_joined_layer_call_fn_2310281�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
C__inference_joined_layer_call_and_return_conditional_losses_2310292�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
%__inference_signature_wrapper_2309527fmn10_isomap_5000_1000_inputfmn10_lle_5000_1000_inputfmn10_pca_5000_1000_inputfmn10_tsvd_5000_1000_input"�
���
FullArgSpec
args� 
varargs
 
varkwjkwargs
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 �
"__inference__wrapped_model_2308372�'56./'( !PQIJBC;<klde]^VWqrxy��������
���
���
4�1
fmn10_pca_5000_1000_input����������
5�2
fmn10_tsvd_5000_1000_input����������
7�4
fmn10_isomap_5000_1000_input����������
4�1
fmn10_lle_5000_1000_input����������
� "/�,
*
joined �
joined����������
K__inference_concatenate_68_layer_call_and_return_conditional_losses_2310272����
���
���
"�
inputs/0���������
"�
inputs/1���������
"�
inputs/2���������
"�
inputs/3���������
� "%�"
�
0���������
� �
0__inference_concatenate_68_layer_call_fn_2310263����
���
���
"�
inputs/0���������
"�
inputs/1���������
"�
inputs/2���������
"�
inputs/3���������
� "�����������
U__inference_fmn10_isomap_5000_1000_1_layer_call_and_return_conditional_losses_2309995^./0�-
&�#
!�
inputs����������
� "&�#
�
0����������
� �
:__inference_fmn10_isomap_5000_1000_1_layer_call_fn_2309984Q./0�-
&�#
!�
inputs����������
� "������������
U__inference_fmn10_isomap_5000_1000_2_layer_call_and_return_conditional_losses_2310075^IJ0�-
&�#
!�
inputs����������
� "&�#
�
0����������
� �
:__inference_fmn10_isomap_5000_1000_2_layer_call_fn_2310064QIJ0�-
&�#
!�
inputs����������
� "������������
U__inference_fmn10_isomap_5000_1000_3_layer_call_and_return_conditional_losses_2310155^de0�-
&�#
!�
inputs����������
� "&�#
�
0����������
� �
:__inference_fmn10_isomap_5000_1000_3_layer_call_fn_2310144Qde0�-
&�#
!�
inputs����������
� "������������
W__inference_fmn10_isomap_5000_1000_out_layer_call_and_return_conditional_losses_2310235^�0�-
&�#
!�
inputs����������
� "%�"
�
0���������
� �
<__inference_fmn10_isomap_5000_1000_out_layer_call_fn_2310224Q�0�-
&�#
!�
inputs����������
� "�����������
R__inference_fmn10_lle_5000_1000_1_layer_call_and_return_conditional_losses_2310015^560�-
&�#
!�
inputs����������
� "&�#
�
0����������
� �
7__inference_fmn10_lle_5000_1000_1_layer_call_fn_2310004Q560�-
&�#
!�
inputs����������
� "������������
R__inference_fmn10_lle_5000_1000_2_layer_call_and_return_conditional_losses_2310095^PQ0�-
&�#
!�
inputs����������
� "&�#
�
0����������
� �
7__inference_fmn10_lle_5000_1000_2_layer_call_fn_2310084QPQ0�-
&�#
!�
inputs����������
� "������������
R__inference_fmn10_lle_5000_1000_3_layer_call_and_return_conditional_losses_2310175^kl0�-
&�#
!�
inputs����������
� "&�#
�
0����������
� �
7__inference_fmn10_lle_5000_1000_3_layer_call_fn_2310164Qkl0�-
&�#
!�
inputs����������
� "������������
T__inference_fmn10_lle_5000_1000_out_layer_call_and_return_conditional_losses_2310255_��0�-
&�#
!�
inputs����������
� "%�"
�
0���������
� �
9__inference_fmn10_lle_5000_1000_out_layer_call_fn_2310244R��0�-
&�#
!�
inputs����������
� "�����������
R__inference_fmn10_pca_5000_1000_1_layer_call_and_return_conditional_losses_2309955^ !0�-
&�#
!�
inputs����������
� "&�#
�
0����������
� �
7__inference_fmn10_pca_5000_1000_1_layer_call_fn_2309944Q !0�-
&�#
!�
inputs����������
� "������������
R__inference_fmn10_pca_5000_1000_2_layer_call_and_return_conditional_losses_2310035^;<0�-
&�#
!�
inputs����������
� "&�#
�
0����������
� �
7__inference_fmn10_pca_5000_1000_2_layer_call_fn_2310024Q;<0�-
&�#
!�
inputs����������
� "������������
R__inference_fmn10_pca_5000_1000_3_layer_call_and_return_conditional_losses_2310115^VW0�-
&�#
!�
inputs����������
� "&�#
�
0����������
� �
7__inference_fmn10_pca_5000_1000_3_layer_call_fn_2310104QVW0�-
&�#
!�
inputs����������
� "������������
T__inference_fmn10_pca_5000_1000_out_layer_call_and_return_conditional_losses_2310195]qr0�-
&�#
!�
inputs����������
� "%�"
�
0���������
� �
9__inference_fmn10_pca_5000_1000_out_layer_call_fn_2310184Pqr0�-
&�#
!�
inputs����������
� "�����������
S__inference_fmn10_tsvd_5000_1000_1_layer_call_and_return_conditional_losses_2309975^'(0�-
&�#
!�
inputs����������
� "&�#
�
0����������
� �
8__inference_fmn10_tsvd_5000_1000_1_layer_call_fn_2309964Q'(0�-
&�#
!�
inputs����������
� "������������
S__inference_fmn10_tsvd_5000_1000_2_layer_call_and_return_conditional_losses_2310055^BC0�-
&�#
!�
inputs����������
� "&�#
�
0����������
� �
8__inference_fmn10_tsvd_5000_1000_2_layer_call_fn_2310044QBC0�-
&�#
!�
inputs����������
� "������������
S__inference_fmn10_tsvd_5000_1000_3_layer_call_and_return_conditional_losses_2310135^]^0�-
&�#
!�
inputs����������
� "&�#
�
0����������
� �
8__inference_fmn10_tsvd_5000_1000_3_layer_call_fn_2310124Q]^0�-
&�#
!�
inputs����������
� "������������
U__inference_fmn10_tsvd_5000_1000_out_layer_call_and_return_conditional_losses_2310215]xy0�-
&�#
!�
inputs����������
� "%�"
�
0���������
� �
:__inference_fmn10_tsvd_5000_1000_out_layer_call_fn_2310204Pxy0�-
&�#
!�
inputs����������
� "�����������
C__inference_joined_layer_call_and_return_conditional_losses_2310292^��/�,
%�"
 �
inputs���������
� "%�"
�
0���������
� }
(__inference_joined_layer_call_fn_2310281Q��/�,
%�"
 �
inputs���������
� "�����������
B__inference_model_layer_call_and_return_conditional_losses_2309350�'56./'( !PQIJBC;<klde]^VWqrxy��������
���
���
4�1
fmn10_pca_5000_1000_input����������
5�2
fmn10_tsvd_5000_1000_input����������
7�4
fmn10_isomap_5000_1000_input����������
4�1
fmn10_lle_5000_1000_input����������
p 

 
� "%�"
�
0���������
� �
B__inference_model_layer_call_and_return_conditional_losses_2309443�'56./'( !PQIJBC;<klde]^VWqrxy��������
���
���
4�1
fmn10_pca_5000_1000_input����������
5�2
fmn10_tsvd_5000_1000_input����������
7�4
fmn10_isomap_5000_1000_input����������
4�1
fmn10_lle_5000_1000_input����������
p

 
� "%�"
�
0���������
� �
B__inference_model_layer_call_and_return_conditional_losses_2309807�'56./'( !PQIJBC;<klde]^VWqrxy��������
���
���
#� 
inputs/0����������
#� 
inputs/1����������
#� 
inputs/2����������
#� 
inputs/3����������
p 

 
� "%�"
�
0���������
� �
B__inference_model_layer_call_and_return_conditional_losses_2309935�'56./'( !PQIJBC;<klde]^VWqrxy��������
���
���
#� 
inputs/0����������
#� 
inputs/1����������
#� 
inputs/2����������
#� 
inputs/3����������
p

 
� "%�"
�
0���������
� �
'__inference_model_layer_call_fn_2308757�'56./'( !PQIJBC;<klde]^VWqrxy��������
���
���
4�1
fmn10_pca_5000_1000_input����������
5�2
fmn10_tsvd_5000_1000_input����������
7�4
fmn10_isomap_5000_1000_input����������
4�1
fmn10_lle_5000_1000_input����������
p 

 
� "�����������
'__inference_model_layer_call_fn_2309257�'56./'( !PQIJBC;<klde]^VWqrxy��������
���
���
4�1
fmn10_pca_5000_1000_input����������
5�2
fmn10_tsvd_5000_1000_input����������
7�4
fmn10_isomap_5000_1000_input����������
4�1
fmn10_lle_5000_1000_input����������
p

 
� "�����������
'__inference_model_layer_call_fn_2309603�'56./'( !PQIJBC;<klde]^VWqrxy��������
���
���
#� 
inputs/0����������
#� 
inputs/1����������
#� 
inputs/2����������
#� 
inputs/3����������
p 

 
� "�����������
'__inference_model_layer_call_fn_2309679�'56./'( !PQIJBC;<klde]^VWqrxy��������
���
���
#� 
inputs/0����������
#� 
inputs/1����������
#� 
inputs/2����������
#� 
inputs/3����������
p

 
� "�����������
%__inference_signature_wrapper_2309527�'56./'( !PQIJBC;<klde]^VWqrxy��������
� 
���
W
fmn10_isomap_5000_1000_input7�4
fmn10_isomap_5000_1000_input����������
Q
fmn10_lle_5000_1000_input4�1
fmn10_lle_5000_1000_input����������
Q
fmn10_pca_5000_1000_input4�1
fmn10_pca_5000_1000_input����������
S
fmn10_tsvd_5000_1000_input5�2
fmn10_tsvd_5000_1000_input����������"/�,
*
joined �
joined���������