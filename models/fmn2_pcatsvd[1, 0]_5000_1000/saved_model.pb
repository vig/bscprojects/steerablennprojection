��
��
B
AssignVariableOp
resource
value"dtype"
dtypetype�
~
BiasAdd

value"T	
bias"T
output"T" 
Ttype:
2	"-
data_formatstringNHWC:
NHWCNCHW
h
ConcatV2
values"T*N
axis"Tidx
output"T"
Nint(0"	
Ttype"
Tidxtype0:
2	
8
Const
output"dtype"
valuetensor"
dtypetype
.
Identity

input"T
output"T"	
Ttype
q
MatMul
a"T
b"T
product"T"
transpose_abool( "
transpose_bbool( "
Ttype:

2	
e
MergeV2Checkpoints
checkpoint_prefixes
destination_prefix"
delete_old_dirsbool(�

NoOp
M
Pack
values"T*N
output"T"
Nint(0"	
Ttype"
axisint 
C
Placeholder
output"dtype"
dtypetype"
shapeshape:
@
ReadVariableOp
resource
value"dtype"
dtypetype�
E
Relu
features"T
activations"T"
Ttype:
2	
o
	RestoreV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0�
l
SaveV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0�
?
Select
	condition

t"T
e"T
output"T"	
Ttype
H
ShardedFilename
basename	
shard

num_shards
filename
0
Sigmoid
x"T
y"T"
Ttype:

2
�
StatefulPartitionedCall
args2Tin
output2Tout"
Tin
list(type)("
Tout
list(type)("	
ffunc"
configstring "
config_protostring "
executor_typestring �
@
StaticRegexFullMatch	
input

output
"
patternstring
N

StringJoin
inputs*N

output"
Nint(0"
	separatorstring 
�
VarHandleOp
resource"
	containerstring "
shared_namestring "
dtypetype"
shapeshape"#
allowed_deviceslist(string)
 �"serve*2.6.02unknown8�

�
fmn2_pca_5000_1000_1/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*,
shared_namefmn2_pca_5000_1000_1/kernel
�
/fmn2_pca_5000_1000_1/kernel/Read/ReadVariableOpReadVariableOpfmn2_pca_5000_1000_1/kernel* 
_output_shapes
:
��*
dtype0
�
fmn2_pca_5000_1000_1/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�**
shared_namefmn2_pca_5000_1000_1/bias
�
-fmn2_pca_5000_1000_1/bias/Read/ReadVariableOpReadVariableOpfmn2_pca_5000_1000_1/bias*
_output_shapes	
:�*
dtype0
�
fmn2_tsvd_5000_1000_1/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*-
shared_namefmn2_tsvd_5000_1000_1/kernel
�
0fmn2_tsvd_5000_1000_1/kernel/Read/ReadVariableOpReadVariableOpfmn2_tsvd_5000_1000_1/kernel* 
_output_shapes
:
��*
dtype0
�
fmn2_tsvd_5000_1000_1/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*+
shared_namefmn2_tsvd_5000_1000_1/bias
�
.fmn2_tsvd_5000_1000_1/bias/Read/ReadVariableOpReadVariableOpfmn2_tsvd_5000_1000_1/bias*
_output_shapes	
:�*
dtype0
�
fmn2_pca_5000_1000_2/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*,
shared_namefmn2_pca_5000_1000_2/kernel
�
/fmn2_pca_5000_1000_2/kernel/Read/ReadVariableOpReadVariableOpfmn2_pca_5000_1000_2/kernel* 
_output_shapes
:
��*
dtype0
�
fmn2_pca_5000_1000_2/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�**
shared_namefmn2_pca_5000_1000_2/bias
�
-fmn2_pca_5000_1000_2/bias/Read/ReadVariableOpReadVariableOpfmn2_pca_5000_1000_2/bias*
_output_shapes	
:�*
dtype0
�
fmn2_tsvd_5000_1000_2/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*-
shared_namefmn2_tsvd_5000_1000_2/kernel
�
0fmn2_tsvd_5000_1000_2/kernel/Read/ReadVariableOpReadVariableOpfmn2_tsvd_5000_1000_2/kernel* 
_output_shapes
:
��*
dtype0
�
fmn2_tsvd_5000_1000_2/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*+
shared_namefmn2_tsvd_5000_1000_2/bias
�
.fmn2_tsvd_5000_1000_2/bias/Read/ReadVariableOpReadVariableOpfmn2_tsvd_5000_1000_2/bias*
_output_shapes	
:�*
dtype0
�
fmn2_pca_5000_1000_3/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*,
shared_namefmn2_pca_5000_1000_3/kernel
�
/fmn2_pca_5000_1000_3/kernel/Read/ReadVariableOpReadVariableOpfmn2_pca_5000_1000_3/kernel* 
_output_shapes
:
��*
dtype0
�
fmn2_pca_5000_1000_3/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�**
shared_namefmn2_pca_5000_1000_3/bias
�
-fmn2_pca_5000_1000_3/bias/Read/ReadVariableOpReadVariableOpfmn2_pca_5000_1000_3/bias*
_output_shapes	
:�*
dtype0
�
fmn2_tsvd_5000_1000_3/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*-
shared_namefmn2_tsvd_5000_1000_3/kernel
�
0fmn2_tsvd_5000_1000_3/kernel/Read/ReadVariableOpReadVariableOpfmn2_tsvd_5000_1000_3/kernel* 
_output_shapes
:
��*
dtype0
�
fmn2_tsvd_5000_1000_3/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*+
shared_namefmn2_tsvd_5000_1000_3/bias
�
.fmn2_tsvd_5000_1000_3/bias/Read/ReadVariableOpReadVariableOpfmn2_tsvd_5000_1000_3/bias*
_output_shapes	
:�*
dtype0
�
fmn2_pca_5000_1000_out/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:	�*.
shared_namefmn2_pca_5000_1000_out/kernel
�
1fmn2_pca_5000_1000_out/kernel/Read/ReadVariableOpReadVariableOpfmn2_pca_5000_1000_out/kernel*
_output_shapes
:	�*
dtype0
�
fmn2_pca_5000_1000_out/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:*,
shared_namefmn2_pca_5000_1000_out/bias
�
/fmn2_pca_5000_1000_out/bias/Read/ReadVariableOpReadVariableOpfmn2_pca_5000_1000_out/bias*
_output_shapes
:*
dtype0
�
fmn2_tsvd_5000_1000_out/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:	�*/
shared_name fmn2_tsvd_5000_1000_out/kernel
�
2fmn2_tsvd_5000_1000_out/kernel/Read/ReadVariableOpReadVariableOpfmn2_tsvd_5000_1000_out/kernel*
_output_shapes
:	�*
dtype0
�
fmn2_tsvd_5000_1000_out/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:*-
shared_namefmn2_tsvd_5000_1000_out/bias
�
0fmn2_tsvd_5000_1000_out/bias/Read/ReadVariableOpReadVariableOpfmn2_tsvd_5000_1000_out/bias*
_output_shapes
:*
dtype0
v
joined/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:*
shared_namejoined/kernel
o
!joined/kernel/Read/ReadVariableOpReadVariableOpjoined/kernel*
_output_shapes

:*
dtype0
n
joined/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:*
shared_namejoined/bias
g
joined/bias/Read/ReadVariableOpReadVariableOpjoined/bias*
_output_shapes
:*
dtype0
f
	Adam/iterVarHandleOp*
_output_shapes
: *
dtype0	*
shape: *
shared_name	Adam/iter
_
Adam/iter/Read/ReadVariableOpReadVariableOp	Adam/iter*
_output_shapes
: *
dtype0	
j
Adam/beta_1VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nameAdam/beta_1
c
Adam/beta_1/Read/ReadVariableOpReadVariableOpAdam/beta_1*
_output_shapes
: *
dtype0
j
Adam/beta_2VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nameAdam/beta_2
c
Adam/beta_2/Read/ReadVariableOpReadVariableOpAdam/beta_2*
_output_shapes
: *
dtype0
h

Adam/decayVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name
Adam/decay
a
Adam/decay/Read/ReadVariableOpReadVariableOp
Adam/decay*
_output_shapes
: *
dtype0
x
Adam/learning_rateVarHandleOp*
_output_shapes
: *
dtype0*
shape: *#
shared_nameAdam/learning_rate
q
&Adam/learning_rate/Read/ReadVariableOpReadVariableOpAdam/learning_rate*
_output_shapes
: *
dtype0
^
totalVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nametotal
W
total/Read/ReadVariableOpReadVariableOptotal*
_output_shapes
: *
dtype0
^
countVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_namecount
W
count/Read/ReadVariableOpReadVariableOpcount*
_output_shapes
: *
dtype0
b
total_1VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name	total_1
[
total_1/Read/ReadVariableOpReadVariableOptotal_1*
_output_shapes
: *
dtype0
b
count_1VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name	count_1
[
count_1/Read/ReadVariableOpReadVariableOpcount_1*
_output_shapes
: *
dtype0
�
Adam/joined/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape
:*%
shared_nameAdam/joined/kernel/m
}
(Adam/joined/kernel/m/Read/ReadVariableOpReadVariableOpAdam/joined/kernel/m*
_output_shapes

:*
dtype0
|
Adam/joined/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:*#
shared_nameAdam/joined/bias/m
u
&Adam/joined/bias/m/Read/ReadVariableOpReadVariableOpAdam/joined/bias/m*
_output_shapes
:*
dtype0
�
Adam/joined/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape
:*%
shared_nameAdam/joined/kernel/v
}
(Adam/joined/kernel/v/Read/ReadVariableOpReadVariableOpAdam/joined/kernel/v*
_output_shapes

:*
dtype0
|
Adam/joined/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:*#
shared_nameAdam/joined/bias/v
u
&Adam/joined/bias/v/Read/ReadVariableOpReadVariableOpAdam/joined/bias/v*
_output_shapes
:*
dtype0

NoOpNoOp
�>
ConstConst"/device:CPU:0*
_output_shapes
: *
dtype0*�=
value�=B�= B�=
�
layer-0
layer-1
layer_with_weights-0
layer-2
layer_with_weights-1
layer-3
layer_with_weights-2
layer-4
layer_with_weights-3
layer-5
layer_with_weights-4
layer-6
layer_with_weights-5
layer-7
	layer_with_weights-6
	layer-8

layer_with_weights-7

layer-9
layer-10
layer_with_weights-8
layer-11
	optimizer
	variables
regularization_losses
trainable_variables
	keras_api

signatures
%
#_self_saveable_object_factories
 
�

kernel
bias
#_self_saveable_object_factories
	variables
regularization_losses
trainable_variables
	keras_api
h

kernel
bias
	variables
regularization_losses
trainable_variables
 	keras_api
�

!kernel
"bias
##_self_saveable_object_factories
$	variables
%regularization_losses
&trainable_variables
'	keras_api
h

(kernel
)bias
*	variables
+regularization_losses
,trainable_variables
-	keras_api
�

.kernel
/bias
#0_self_saveable_object_factories
1	variables
2regularization_losses
3trainable_variables
4	keras_api
h

5kernel
6bias
7	variables
8regularization_losses
9trainable_variables
:	keras_api
�

;kernel
<bias
#=_self_saveable_object_factories
>	variables
?regularization_losses
@trainable_variables
A	keras_api
h

Bkernel
Cbias
D	variables
Eregularization_losses
Ftrainable_variables
G	keras_api
R
H	variables
Iregularization_losses
Jtrainable_variables
K	keras_api
h

Lkernel
Mbias
N	variables
Oregularization_losses
Ptrainable_variables
Q	keras_api
h
Riter

Sbeta_1

Tbeta_2
	Udecay
Vlearning_rateLm�Mm�Lv�Mv�
�
0
1
2
3
!4
"5
(6
)7
.8
/9
510
611
;12
<13
B14
C15
L16
M17
 

L0
M1
�
Wmetrics

Xlayers
	variables
Ylayer_metrics
Zlayer_regularization_losses
regularization_losses
trainable_variables
[non_trainable_variables
 
 
ge
VARIABLE_VALUEfmn2_pca_5000_1000_1/kernel6layer_with_weights-0/kernel/.ATTRIBUTES/VARIABLE_VALUE
ca
VARIABLE_VALUEfmn2_pca_5000_1000_1/bias4layer_with_weights-0/bias/.ATTRIBUTES/VARIABLE_VALUE
 

0
1
 
 
�
\metrics

]layers
	variables
regularization_losses
^layer_metrics
_layer_regularization_losses
trainable_variables
`non_trainable_variables
hf
VARIABLE_VALUEfmn2_tsvd_5000_1000_1/kernel6layer_with_weights-1/kernel/.ATTRIBUTES/VARIABLE_VALUE
db
VARIABLE_VALUEfmn2_tsvd_5000_1000_1/bias4layer_with_weights-1/bias/.ATTRIBUTES/VARIABLE_VALUE

0
1
 
 
�
ametrics

blayers
	variables
regularization_losses
clayer_metrics
dlayer_regularization_losses
trainable_variables
enon_trainable_variables
ge
VARIABLE_VALUEfmn2_pca_5000_1000_2/kernel6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUE
ca
VARIABLE_VALUEfmn2_pca_5000_1000_2/bias4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUE
 

!0
"1
 
 
�
fmetrics

glayers
$	variables
%regularization_losses
hlayer_metrics
ilayer_regularization_losses
&trainable_variables
jnon_trainable_variables
hf
VARIABLE_VALUEfmn2_tsvd_5000_1000_2/kernel6layer_with_weights-3/kernel/.ATTRIBUTES/VARIABLE_VALUE
db
VARIABLE_VALUEfmn2_tsvd_5000_1000_2/bias4layer_with_weights-3/bias/.ATTRIBUTES/VARIABLE_VALUE

(0
)1
 
 
�
kmetrics

llayers
*	variables
+regularization_losses
mlayer_metrics
nlayer_regularization_losses
,trainable_variables
onon_trainable_variables
ge
VARIABLE_VALUEfmn2_pca_5000_1000_3/kernel6layer_with_weights-4/kernel/.ATTRIBUTES/VARIABLE_VALUE
ca
VARIABLE_VALUEfmn2_pca_5000_1000_3/bias4layer_with_weights-4/bias/.ATTRIBUTES/VARIABLE_VALUE
 

.0
/1
 
 
�
pmetrics

qlayers
1	variables
2regularization_losses
rlayer_metrics
slayer_regularization_losses
3trainable_variables
tnon_trainable_variables
hf
VARIABLE_VALUEfmn2_tsvd_5000_1000_3/kernel6layer_with_weights-5/kernel/.ATTRIBUTES/VARIABLE_VALUE
db
VARIABLE_VALUEfmn2_tsvd_5000_1000_3/bias4layer_with_weights-5/bias/.ATTRIBUTES/VARIABLE_VALUE

50
61
 
 
�
umetrics

vlayers
7	variables
8regularization_losses
wlayer_metrics
xlayer_regularization_losses
9trainable_variables
ynon_trainable_variables
ig
VARIABLE_VALUEfmn2_pca_5000_1000_out/kernel6layer_with_weights-6/kernel/.ATTRIBUTES/VARIABLE_VALUE
ec
VARIABLE_VALUEfmn2_pca_5000_1000_out/bias4layer_with_weights-6/bias/.ATTRIBUTES/VARIABLE_VALUE
 

;0
<1
 
 
�
zmetrics

{layers
>	variables
?regularization_losses
|layer_metrics
}layer_regularization_losses
@trainable_variables
~non_trainable_variables
jh
VARIABLE_VALUEfmn2_tsvd_5000_1000_out/kernel6layer_with_weights-7/kernel/.ATTRIBUTES/VARIABLE_VALUE
fd
VARIABLE_VALUEfmn2_tsvd_5000_1000_out/bias4layer_with_weights-7/bias/.ATTRIBUTES/VARIABLE_VALUE

B0
C1
 
 
�
metrics
�layers
D	variables
Eregularization_losses
�layer_metrics
 �layer_regularization_losses
Ftrainable_variables
�non_trainable_variables
 
 
 
�
�metrics
�layers
H	variables
Iregularization_losses
�layer_metrics
 �layer_regularization_losses
Jtrainable_variables
�non_trainable_variables
YW
VARIABLE_VALUEjoined/kernel6layer_with_weights-8/kernel/.ATTRIBUTES/VARIABLE_VALUE
US
VARIABLE_VALUEjoined/bias4layer_with_weights-8/bias/.ATTRIBUTES/VARIABLE_VALUE

L0
M1
 

L0
M1
�
�metrics
�layers
N	variables
Oregularization_losses
�layer_metrics
 �layer_regularization_losses
Ptrainable_variables
�non_trainable_variables
HF
VARIABLE_VALUE	Adam/iter)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUE
LJ
VARIABLE_VALUEAdam/beta_1+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUE
LJ
VARIABLE_VALUEAdam/beta_2+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUE
JH
VARIABLE_VALUE
Adam/decay*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUE
ZX
VARIABLE_VALUEAdam/learning_rate2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUE

�0
�1
V
0
1
2
3
4
5
6
7
	8

9
10
11
 
 
v
0
1
2
3
!4
"5
(6
)7
.8
/9
510
611
;12
<13
B14
C15
 
 
 
 

0
1
 
 
 
 

0
1
 
 
 
 

!0
"1
 
 
 
 

(0
)1
 
 
 
 

.0
/1
 
 
 
 

50
61
 
 
 
 

;0
<1
 
 
 
 

B0
C1
 
 
 
 
 
 
 
 
 
 
8

�total

�count
�	variables
�	keras_api
I

�total

�count
�
_fn_kwargs
�	variables
�	keras_api
OM
VARIABLE_VALUEtotal4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUE
OM
VARIABLE_VALUEcount4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUE

�0
�1

�	variables
QO
VARIABLE_VALUEtotal_14keras_api/metrics/1/total/.ATTRIBUTES/VARIABLE_VALUE
QO
VARIABLE_VALUEcount_14keras_api/metrics/1/count/.ATTRIBUTES/VARIABLE_VALUE
 

�0
�1

�	variables
|z
VARIABLE_VALUEAdam/joined/kernel/mRlayer_with_weights-8/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
xv
VARIABLE_VALUEAdam/joined/bias/mPlayer_with_weights-8/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
|z
VARIABLE_VALUEAdam/joined/kernel/vRlayer_with_weights-8/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
xv
VARIABLE_VALUEAdam/joined/bias/vPlayer_with_weights-8/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
�
(serving_default_fmn2_pca_5000_1000_inputPlaceholder*(
_output_shapes
:����������*
dtype0*
shape:����������
�
)serving_default_fmn2_tsvd_5000_1000_inputPlaceholder*(
_output_shapes
:����������*
dtype0*
shape:����������
�
StatefulPartitionedCallStatefulPartitionedCall(serving_default_fmn2_pca_5000_1000_input)serving_default_fmn2_tsvd_5000_1000_inputfmn2_tsvd_5000_1000_1/kernelfmn2_tsvd_5000_1000_1/biasfmn2_pca_5000_1000_1/kernelfmn2_pca_5000_1000_1/biasfmn2_tsvd_5000_1000_2/kernelfmn2_tsvd_5000_1000_2/biasfmn2_pca_5000_1000_2/kernelfmn2_pca_5000_1000_2/biasfmn2_tsvd_5000_1000_3/kernelfmn2_tsvd_5000_1000_3/biasfmn2_pca_5000_1000_3/kernelfmn2_pca_5000_1000_3/biasfmn2_pca_5000_1000_out/kernelfmn2_pca_5000_1000_out/biasfmn2_tsvd_5000_1000_out/kernelfmn2_tsvd_5000_1000_out/biasjoined/kerneljoined/bias*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*4
_read_only_resource_inputs
	
*0
config_proto 

CPU

GPU2*0J 8� *.
f)R'
%__inference_signature_wrapper_1539301
O
saver_filenamePlaceholder*
_output_shapes
: *
dtype0*
shape: 
�
StatefulPartitionedCall_1StatefulPartitionedCallsaver_filename/fmn2_pca_5000_1000_1/kernel/Read/ReadVariableOp-fmn2_pca_5000_1000_1/bias/Read/ReadVariableOp0fmn2_tsvd_5000_1000_1/kernel/Read/ReadVariableOp.fmn2_tsvd_5000_1000_1/bias/Read/ReadVariableOp/fmn2_pca_5000_1000_2/kernel/Read/ReadVariableOp-fmn2_pca_5000_1000_2/bias/Read/ReadVariableOp0fmn2_tsvd_5000_1000_2/kernel/Read/ReadVariableOp.fmn2_tsvd_5000_1000_2/bias/Read/ReadVariableOp/fmn2_pca_5000_1000_3/kernel/Read/ReadVariableOp-fmn2_pca_5000_1000_3/bias/Read/ReadVariableOp0fmn2_tsvd_5000_1000_3/kernel/Read/ReadVariableOp.fmn2_tsvd_5000_1000_3/bias/Read/ReadVariableOp1fmn2_pca_5000_1000_out/kernel/Read/ReadVariableOp/fmn2_pca_5000_1000_out/bias/Read/ReadVariableOp2fmn2_tsvd_5000_1000_out/kernel/Read/ReadVariableOp0fmn2_tsvd_5000_1000_out/bias/Read/ReadVariableOp!joined/kernel/Read/ReadVariableOpjoined/bias/Read/ReadVariableOpAdam/iter/Read/ReadVariableOpAdam/beta_1/Read/ReadVariableOpAdam/beta_2/Read/ReadVariableOpAdam/decay/Read/ReadVariableOp&Adam/learning_rate/Read/ReadVariableOptotal/Read/ReadVariableOpcount/Read/ReadVariableOptotal_1/Read/ReadVariableOpcount_1/Read/ReadVariableOp(Adam/joined/kernel/m/Read/ReadVariableOp&Adam/joined/bias/m/Read/ReadVariableOp(Adam/joined/kernel/v/Read/ReadVariableOp&Adam/joined/bias/v/Read/ReadVariableOpConst*,
Tin%
#2!	*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *)
f$R"
 __inference__traced_save_1539835
�
StatefulPartitionedCall_2StatefulPartitionedCallsaver_filenamefmn2_pca_5000_1000_1/kernelfmn2_pca_5000_1000_1/biasfmn2_tsvd_5000_1000_1/kernelfmn2_tsvd_5000_1000_1/biasfmn2_pca_5000_1000_2/kernelfmn2_pca_5000_1000_2/biasfmn2_tsvd_5000_1000_2/kernelfmn2_tsvd_5000_1000_2/biasfmn2_pca_5000_1000_3/kernelfmn2_pca_5000_1000_3/biasfmn2_tsvd_5000_1000_3/kernelfmn2_tsvd_5000_1000_3/biasfmn2_pca_5000_1000_out/kernelfmn2_pca_5000_1000_out/biasfmn2_tsvd_5000_1000_out/kernelfmn2_tsvd_5000_1000_out/biasjoined/kerneljoined/bias	Adam/iterAdam/beta_1Adam/beta_2
Adam/decayAdam/learning_ratetotalcounttotal_1count_1Adam/joined/kernel/mAdam/joined/bias/mAdam/joined/kernel/vAdam/joined/bias/v*+
Tin$
"2 *
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *,
f'R%
#__inference__traced_restore_1539938��	
�
�
7__inference_fmn2_tsvd_5000_1000_3_layer_call_fn_1539634

inputs
unknown:
��
	unknown_0:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_fmn2_tsvd_5000_1000_3_layer_call_and_return_conditional_losses_15387522
StatefulPartitionedCall|
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:����������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
w
K__inference_concatenate_46_layer_call_and_return_conditional_losses_1539698
inputs_0
inputs_1
identity\
concat/axisConst*
_output_shapes
: *
dtype0*
value	B :2
concat/axis�
concatConcatV2inputs_0inputs_1concat/axis:output:0*
N*
T0*'
_output_shapes
:���������2
concatc
IdentityIdentityconcat:output:0*
T0*'
_output_shapes
:���������2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*9
_input_shapes(
&:���������:���������:Q M
'
_output_shapes
:���������
"
_user_specified_name
inputs/0:QM
'
_output_shapes
:���������
"
_user_specified_name
inputs/1
�
�
C__inference_joined_layer_call_and_return_conditional_losses_1539718

inputs0
matmul_readvariableop_resource:-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2	
BiasAdda
SigmoidSigmoidBiasAdd:output:0*
T0*'
_output_shapes
:���������2	
Sigmoidf
IdentityIdentitySigmoid:y:0^NoOp*
T0*'
_output_shapes
:���������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs
�
�
R__inference_fmn2_tsvd_5000_1000_3_layer_call_and_return_conditional_losses_1538752

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�@
�

B__inference_model_layer_call_and_return_conditional_losses_1539251
fmn2_pca_5000_1000_input
fmn2_tsvd_5000_1000_input1
fmn2_tsvd_5000_1000_1_1539204:
��,
fmn2_tsvd_5000_1000_1_1539206:	�0
fmn2_pca_5000_1000_1_1539209:
��+
fmn2_pca_5000_1000_1_1539211:	�1
fmn2_tsvd_5000_1000_2_1539214:
��,
fmn2_tsvd_5000_1000_2_1539216:	�0
fmn2_pca_5000_1000_2_1539219:
��+
fmn2_pca_5000_1000_2_1539221:	�1
fmn2_tsvd_5000_1000_3_1539224:
��,
fmn2_tsvd_5000_1000_3_1539226:	�0
fmn2_pca_5000_1000_3_1539229:
��+
fmn2_pca_5000_1000_3_1539231:	�1
fmn2_pca_5000_1000_out_1539234:	�,
fmn2_pca_5000_1000_out_1539236:2
fmn2_tsvd_5000_1000_out_1539239:	�-
fmn2_tsvd_5000_1000_out_1539241: 
joined_1539245:
joined_1539247:
identity��,fmn2_pca_5000_1000_1/StatefulPartitionedCall�,fmn2_pca_5000_1000_2/StatefulPartitionedCall�,fmn2_pca_5000_1000_3/StatefulPartitionedCall�.fmn2_pca_5000_1000_out/StatefulPartitionedCall�-fmn2_tsvd_5000_1000_1/StatefulPartitionedCall�-fmn2_tsvd_5000_1000_2/StatefulPartitionedCall�-fmn2_tsvd_5000_1000_3/StatefulPartitionedCall�/fmn2_tsvd_5000_1000_out/StatefulPartitionedCall�joined/StatefulPartitionedCall�
-fmn2_tsvd_5000_1000_1/StatefulPartitionedCallStatefulPartitionedCallfmn2_tsvd_5000_1000_inputfmn2_tsvd_5000_1000_1_1539204fmn2_tsvd_5000_1000_1_1539206*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_fmn2_tsvd_5000_1000_1_layer_call_and_return_conditional_losses_15386842/
-fmn2_tsvd_5000_1000_1/StatefulPartitionedCall�
,fmn2_pca_5000_1000_1/StatefulPartitionedCallStatefulPartitionedCallfmn2_pca_5000_1000_inputfmn2_pca_5000_1000_1_1539209fmn2_pca_5000_1000_1_1539211*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_fmn2_pca_5000_1000_1_layer_call_and_return_conditional_losses_15387012.
,fmn2_pca_5000_1000_1/StatefulPartitionedCall�
-fmn2_tsvd_5000_1000_2/StatefulPartitionedCallStatefulPartitionedCall6fmn2_tsvd_5000_1000_1/StatefulPartitionedCall:output:0fmn2_tsvd_5000_1000_2_1539214fmn2_tsvd_5000_1000_2_1539216*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_fmn2_tsvd_5000_1000_2_layer_call_and_return_conditional_losses_15387182/
-fmn2_tsvd_5000_1000_2/StatefulPartitionedCall�
,fmn2_pca_5000_1000_2/StatefulPartitionedCallStatefulPartitionedCall5fmn2_pca_5000_1000_1/StatefulPartitionedCall:output:0fmn2_pca_5000_1000_2_1539219fmn2_pca_5000_1000_2_1539221*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_fmn2_pca_5000_1000_2_layer_call_and_return_conditional_losses_15387352.
,fmn2_pca_5000_1000_2/StatefulPartitionedCall�
-fmn2_tsvd_5000_1000_3/StatefulPartitionedCallStatefulPartitionedCall6fmn2_tsvd_5000_1000_2/StatefulPartitionedCall:output:0fmn2_tsvd_5000_1000_3_1539224fmn2_tsvd_5000_1000_3_1539226*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_fmn2_tsvd_5000_1000_3_layer_call_and_return_conditional_losses_15387522/
-fmn2_tsvd_5000_1000_3/StatefulPartitionedCall�
,fmn2_pca_5000_1000_3/StatefulPartitionedCallStatefulPartitionedCall5fmn2_pca_5000_1000_2/StatefulPartitionedCall:output:0fmn2_pca_5000_1000_3_1539229fmn2_pca_5000_1000_3_1539231*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_fmn2_pca_5000_1000_3_layer_call_and_return_conditional_losses_15387692.
,fmn2_pca_5000_1000_3/StatefulPartitionedCall�
.fmn2_pca_5000_1000_out/StatefulPartitionedCallStatefulPartitionedCall5fmn2_pca_5000_1000_3/StatefulPartitionedCall:output:0fmn2_pca_5000_1000_out_1539234fmn2_pca_5000_1000_out_1539236*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *\
fWRU
S__inference_fmn2_pca_5000_1000_out_layer_call_and_return_conditional_losses_153878620
.fmn2_pca_5000_1000_out/StatefulPartitionedCall�
/fmn2_tsvd_5000_1000_out/StatefulPartitionedCallStatefulPartitionedCall6fmn2_tsvd_5000_1000_3/StatefulPartitionedCall:output:0fmn2_tsvd_5000_1000_out_1539239fmn2_tsvd_5000_1000_out_1539241*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *]
fXRV
T__inference_fmn2_tsvd_5000_1000_out_layer_call_and_return_conditional_losses_153880321
/fmn2_tsvd_5000_1000_out/StatefulPartitionedCall�
concatenate_46/PartitionedCallPartitionedCall7fmn2_pca_5000_1000_out/StatefulPartitionedCall:output:08fmn2_tsvd_5000_1000_out/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *T
fORM
K__inference_concatenate_46_layer_call_and_return_conditional_losses_15388162 
concatenate_46/PartitionedCall�
joined/StatefulPartitionedCallStatefulPartitionedCall'concatenate_46/PartitionedCall:output:0joined_1539245joined_1539247*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *L
fGRE
C__inference_joined_layer_call_and_return_conditional_losses_15388292 
joined/StatefulPartitionedCall�
IdentityIdentity'joined/StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identity�
NoOpNoOp-^fmn2_pca_5000_1000_1/StatefulPartitionedCall-^fmn2_pca_5000_1000_2/StatefulPartitionedCall-^fmn2_pca_5000_1000_3/StatefulPartitionedCall/^fmn2_pca_5000_1000_out/StatefulPartitionedCall.^fmn2_tsvd_5000_1000_1/StatefulPartitionedCall.^fmn2_tsvd_5000_1000_2/StatefulPartitionedCall.^fmn2_tsvd_5000_1000_3/StatefulPartitionedCall0^fmn2_tsvd_5000_1000_out/StatefulPartitionedCall^joined/StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*_
_input_shapesN
L:����������:����������: : : : : : : : : : : : : : : : : : 2\
,fmn2_pca_5000_1000_1/StatefulPartitionedCall,fmn2_pca_5000_1000_1/StatefulPartitionedCall2\
,fmn2_pca_5000_1000_2/StatefulPartitionedCall,fmn2_pca_5000_1000_2/StatefulPartitionedCall2\
,fmn2_pca_5000_1000_3/StatefulPartitionedCall,fmn2_pca_5000_1000_3/StatefulPartitionedCall2`
.fmn2_pca_5000_1000_out/StatefulPartitionedCall.fmn2_pca_5000_1000_out/StatefulPartitionedCall2^
-fmn2_tsvd_5000_1000_1/StatefulPartitionedCall-fmn2_tsvd_5000_1000_1/StatefulPartitionedCall2^
-fmn2_tsvd_5000_1000_2/StatefulPartitionedCall-fmn2_tsvd_5000_1000_2/StatefulPartitionedCall2^
-fmn2_tsvd_5000_1000_3/StatefulPartitionedCall-fmn2_tsvd_5000_1000_3/StatefulPartitionedCall2b
/fmn2_tsvd_5000_1000_out/StatefulPartitionedCall/fmn2_tsvd_5000_1000_out/StatefulPartitionedCall2@
joined/StatefulPartitionedCalljoined/StatefulPartitionedCall:b ^
(
_output_shapes
:����������
2
_user_specified_namefmn2_pca_5000_1000_input:c_
(
_output_shapes
:����������
3
_user_specified_namefmn2_tsvd_5000_1000_input
�
�
R__inference_fmn2_tsvd_5000_1000_1_layer_call_and_return_conditional_losses_1539565

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�|
�
"__inference__wrapped_model_1538664
fmn2_pca_5000_1000_input
fmn2_tsvd_5000_1000_inputN
:model_fmn2_tsvd_5000_1000_1_matmul_readvariableop_resource:
��J
;model_fmn2_tsvd_5000_1000_1_biasadd_readvariableop_resource:	�M
9model_fmn2_pca_5000_1000_1_matmul_readvariableop_resource:
��I
:model_fmn2_pca_5000_1000_1_biasadd_readvariableop_resource:	�N
:model_fmn2_tsvd_5000_1000_2_matmul_readvariableop_resource:
��J
;model_fmn2_tsvd_5000_1000_2_biasadd_readvariableop_resource:	�M
9model_fmn2_pca_5000_1000_2_matmul_readvariableop_resource:
��I
:model_fmn2_pca_5000_1000_2_biasadd_readvariableop_resource:	�N
:model_fmn2_tsvd_5000_1000_3_matmul_readvariableop_resource:
��J
;model_fmn2_tsvd_5000_1000_3_biasadd_readvariableop_resource:	�M
9model_fmn2_pca_5000_1000_3_matmul_readvariableop_resource:
��I
:model_fmn2_pca_5000_1000_3_biasadd_readvariableop_resource:	�N
;model_fmn2_pca_5000_1000_out_matmul_readvariableop_resource:	�J
<model_fmn2_pca_5000_1000_out_biasadd_readvariableop_resource:O
<model_fmn2_tsvd_5000_1000_out_matmul_readvariableop_resource:	�K
=model_fmn2_tsvd_5000_1000_out_biasadd_readvariableop_resource:=
+model_joined_matmul_readvariableop_resource::
,model_joined_biasadd_readvariableop_resource:
identity��1model/fmn2_pca_5000_1000_1/BiasAdd/ReadVariableOp�0model/fmn2_pca_5000_1000_1/MatMul/ReadVariableOp�1model/fmn2_pca_5000_1000_2/BiasAdd/ReadVariableOp�0model/fmn2_pca_5000_1000_2/MatMul/ReadVariableOp�1model/fmn2_pca_5000_1000_3/BiasAdd/ReadVariableOp�0model/fmn2_pca_5000_1000_3/MatMul/ReadVariableOp�3model/fmn2_pca_5000_1000_out/BiasAdd/ReadVariableOp�2model/fmn2_pca_5000_1000_out/MatMul/ReadVariableOp�2model/fmn2_tsvd_5000_1000_1/BiasAdd/ReadVariableOp�1model/fmn2_tsvd_5000_1000_1/MatMul/ReadVariableOp�2model/fmn2_tsvd_5000_1000_2/BiasAdd/ReadVariableOp�1model/fmn2_tsvd_5000_1000_2/MatMul/ReadVariableOp�2model/fmn2_tsvd_5000_1000_3/BiasAdd/ReadVariableOp�1model/fmn2_tsvd_5000_1000_3/MatMul/ReadVariableOp�4model/fmn2_tsvd_5000_1000_out/BiasAdd/ReadVariableOp�3model/fmn2_tsvd_5000_1000_out/MatMul/ReadVariableOp�#model/joined/BiasAdd/ReadVariableOp�"model/joined/MatMul/ReadVariableOp�
1model/fmn2_tsvd_5000_1000_1/MatMul/ReadVariableOpReadVariableOp:model_fmn2_tsvd_5000_1000_1_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype023
1model/fmn2_tsvd_5000_1000_1/MatMul/ReadVariableOp�
"model/fmn2_tsvd_5000_1000_1/MatMulMatMulfmn2_tsvd_5000_1000_input9model/fmn2_tsvd_5000_1000_1/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2$
"model/fmn2_tsvd_5000_1000_1/MatMul�
2model/fmn2_tsvd_5000_1000_1/BiasAdd/ReadVariableOpReadVariableOp;model_fmn2_tsvd_5000_1000_1_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype024
2model/fmn2_tsvd_5000_1000_1/BiasAdd/ReadVariableOp�
#model/fmn2_tsvd_5000_1000_1/BiasAddBiasAdd,model/fmn2_tsvd_5000_1000_1/MatMul:product:0:model/fmn2_tsvd_5000_1000_1/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2%
#model/fmn2_tsvd_5000_1000_1/BiasAdd�
 model/fmn2_tsvd_5000_1000_1/ReluRelu,model/fmn2_tsvd_5000_1000_1/BiasAdd:output:0*
T0*(
_output_shapes
:����������2"
 model/fmn2_tsvd_5000_1000_1/Relu�
0model/fmn2_pca_5000_1000_1/MatMul/ReadVariableOpReadVariableOp9model_fmn2_pca_5000_1000_1_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype022
0model/fmn2_pca_5000_1000_1/MatMul/ReadVariableOp�
!model/fmn2_pca_5000_1000_1/MatMulMatMulfmn2_pca_5000_1000_input8model/fmn2_pca_5000_1000_1/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2#
!model/fmn2_pca_5000_1000_1/MatMul�
1model/fmn2_pca_5000_1000_1/BiasAdd/ReadVariableOpReadVariableOp:model_fmn2_pca_5000_1000_1_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype023
1model/fmn2_pca_5000_1000_1/BiasAdd/ReadVariableOp�
"model/fmn2_pca_5000_1000_1/BiasAddBiasAdd+model/fmn2_pca_5000_1000_1/MatMul:product:09model/fmn2_pca_5000_1000_1/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2$
"model/fmn2_pca_5000_1000_1/BiasAdd�
model/fmn2_pca_5000_1000_1/ReluRelu+model/fmn2_pca_5000_1000_1/BiasAdd:output:0*
T0*(
_output_shapes
:����������2!
model/fmn2_pca_5000_1000_1/Relu�
1model/fmn2_tsvd_5000_1000_2/MatMul/ReadVariableOpReadVariableOp:model_fmn2_tsvd_5000_1000_2_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype023
1model/fmn2_tsvd_5000_1000_2/MatMul/ReadVariableOp�
"model/fmn2_tsvd_5000_1000_2/MatMulMatMul.model/fmn2_tsvd_5000_1000_1/Relu:activations:09model/fmn2_tsvd_5000_1000_2/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2$
"model/fmn2_tsvd_5000_1000_2/MatMul�
2model/fmn2_tsvd_5000_1000_2/BiasAdd/ReadVariableOpReadVariableOp;model_fmn2_tsvd_5000_1000_2_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype024
2model/fmn2_tsvd_5000_1000_2/BiasAdd/ReadVariableOp�
#model/fmn2_tsvd_5000_1000_2/BiasAddBiasAdd,model/fmn2_tsvd_5000_1000_2/MatMul:product:0:model/fmn2_tsvd_5000_1000_2/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2%
#model/fmn2_tsvd_5000_1000_2/BiasAdd�
 model/fmn2_tsvd_5000_1000_2/ReluRelu,model/fmn2_tsvd_5000_1000_2/BiasAdd:output:0*
T0*(
_output_shapes
:����������2"
 model/fmn2_tsvd_5000_1000_2/Relu�
0model/fmn2_pca_5000_1000_2/MatMul/ReadVariableOpReadVariableOp9model_fmn2_pca_5000_1000_2_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype022
0model/fmn2_pca_5000_1000_2/MatMul/ReadVariableOp�
!model/fmn2_pca_5000_1000_2/MatMulMatMul-model/fmn2_pca_5000_1000_1/Relu:activations:08model/fmn2_pca_5000_1000_2/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2#
!model/fmn2_pca_5000_1000_2/MatMul�
1model/fmn2_pca_5000_1000_2/BiasAdd/ReadVariableOpReadVariableOp:model_fmn2_pca_5000_1000_2_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype023
1model/fmn2_pca_5000_1000_2/BiasAdd/ReadVariableOp�
"model/fmn2_pca_5000_1000_2/BiasAddBiasAdd+model/fmn2_pca_5000_1000_2/MatMul:product:09model/fmn2_pca_5000_1000_2/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2$
"model/fmn2_pca_5000_1000_2/BiasAdd�
model/fmn2_pca_5000_1000_2/ReluRelu+model/fmn2_pca_5000_1000_2/BiasAdd:output:0*
T0*(
_output_shapes
:����������2!
model/fmn2_pca_5000_1000_2/Relu�
1model/fmn2_tsvd_5000_1000_3/MatMul/ReadVariableOpReadVariableOp:model_fmn2_tsvd_5000_1000_3_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype023
1model/fmn2_tsvd_5000_1000_3/MatMul/ReadVariableOp�
"model/fmn2_tsvd_5000_1000_3/MatMulMatMul.model/fmn2_tsvd_5000_1000_2/Relu:activations:09model/fmn2_tsvd_5000_1000_3/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2$
"model/fmn2_tsvd_5000_1000_3/MatMul�
2model/fmn2_tsvd_5000_1000_3/BiasAdd/ReadVariableOpReadVariableOp;model_fmn2_tsvd_5000_1000_3_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype024
2model/fmn2_tsvd_5000_1000_3/BiasAdd/ReadVariableOp�
#model/fmn2_tsvd_5000_1000_3/BiasAddBiasAdd,model/fmn2_tsvd_5000_1000_3/MatMul:product:0:model/fmn2_tsvd_5000_1000_3/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2%
#model/fmn2_tsvd_5000_1000_3/BiasAdd�
 model/fmn2_tsvd_5000_1000_3/ReluRelu,model/fmn2_tsvd_5000_1000_3/BiasAdd:output:0*
T0*(
_output_shapes
:����������2"
 model/fmn2_tsvd_5000_1000_3/Relu�
0model/fmn2_pca_5000_1000_3/MatMul/ReadVariableOpReadVariableOp9model_fmn2_pca_5000_1000_3_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype022
0model/fmn2_pca_5000_1000_3/MatMul/ReadVariableOp�
!model/fmn2_pca_5000_1000_3/MatMulMatMul-model/fmn2_pca_5000_1000_2/Relu:activations:08model/fmn2_pca_5000_1000_3/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2#
!model/fmn2_pca_5000_1000_3/MatMul�
1model/fmn2_pca_5000_1000_3/BiasAdd/ReadVariableOpReadVariableOp:model_fmn2_pca_5000_1000_3_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype023
1model/fmn2_pca_5000_1000_3/BiasAdd/ReadVariableOp�
"model/fmn2_pca_5000_1000_3/BiasAddBiasAdd+model/fmn2_pca_5000_1000_3/MatMul:product:09model/fmn2_pca_5000_1000_3/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2$
"model/fmn2_pca_5000_1000_3/BiasAdd�
model/fmn2_pca_5000_1000_3/ReluRelu+model/fmn2_pca_5000_1000_3/BiasAdd:output:0*
T0*(
_output_shapes
:����������2!
model/fmn2_pca_5000_1000_3/Relu�
2model/fmn2_pca_5000_1000_out/MatMul/ReadVariableOpReadVariableOp;model_fmn2_pca_5000_1000_out_matmul_readvariableop_resource*
_output_shapes
:	�*
dtype024
2model/fmn2_pca_5000_1000_out/MatMul/ReadVariableOp�
#model/fmn2_pca_5000_1000_out/MatMulMatMul-model/fmn2_pca_5000_1000_3/Relu:activations:0:model/fmn2_pca_5000_1000_out/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2%
#model/fmn2_pca_5000_1000_out/MatMul�
3model/fmn2_pca_5000_1000_out/BiasAdd/ReadVariableOpReadVariableOp<model_fmn2_pca_5000_1000_out_biasadd_readvariableop_resource*
_output_shapes
:*
dtype025
3model/fmn2_pca_5000_1000_out/BiasAdd/ReadVariableOp�
$model/fmn2_pca_5000_1000_out/BiasAddBiasAdd-model/fmn2_pca_5000_1000_out/MatMul:product:0;model/fmn2_pca_5000_1000_out/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2&
$model/fmn2_pca_5000_1000_out/BiasAdd�
$model/fmn2_pca_5000_1000_out/SigmoidSigmoid-model/fmn2_pca_5000_1000_out/BiasAdd:output:0*
T0*'
_output_shapes
:���������2&
$model/fmn2_pca_5000_1000_out/Sigmoid�
3model/fmn2_tsvd_5000_1000_out/MatMul/ReadVariableOpReadVariableOp<model_fmn2_tsvd_5000_1000_out_matmul_readvariableop_resource*
_output_shapes
:	�*
dtype025
3model/fmn2_tsvd_5000_1000_out/MatMul/ReadVariableOp�
$model/fmn2_tsvd_5000_1000_out/MatMulMatMul.model/fmn2_tsvd_5000_1000_3/Relu:activations:0;model/fmn2_tsvd_5000_1000_out/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2&
$model/fmn2_tsvd_5000_1000_out/MatMul�
4model/fmn2_tsvd_5000_1000_out/BiasAdd/ReadVariableOpReadVariableOp=model_fmn2_tsvd_5000_1000_out_biasadd_readvariableop_resource*
_output_shapes
:*
dtype026
4model/fmn2_tsvd_5000_1000_out/BiasAdd/ReadVariableOp�
%model/fmn2_tsvd_5000_1000_out/BiasAddBiasAdd.model/fmn2_tsvd_5000_1000_out/MatMul:product:0<model/fmn2_tsvd_5000_1000_out/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2'
%model/fmn2_tsvd_5000_1000_out/BiasAdd�
%model/fmn2_tsvd_5000_1000_out/SigmoidSigmoid.model/fmn2_tsvd_5000_1000_out/BiasAdd:output:0*
T0*'
_output_shapes
:���������2'
%model/fmn2_tsvd_5000_1000_out/Sigmoid�
 model/concatenate_46/concat/axisConst*
_output_shapes
: *
dtype0*
value	B :2"
 model/concatenate_46/concat/axis�
model/concatenate_46/concatConcatV2(model/fmn2_pca_5000_1000_out/Sigmoid:y:0)model/fmn2_tsvd_5000_1000_out/Sigmoid:y:0)model/concatenate_46/concat/axis:output:0*
N*
T0*'
_output_shapes
:���������2
model/concatenate_46/concat�
"model/joined/MatMul/ReadVariableOpReadVariableOp+model_joined_matmul_readvariableop_resource*
_output_shapes

:*
dtype02$
"model/joined/MatMul/ReadVariableOp�
model/joined/MatMulMatMul$model/concatenate_46/concat:output:0*model/joined/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
model/joined/MatMul�
#model/joined/BiasAdd/ReadVariableOpReadVariableOp,model_joined_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02%
#model/joined/BiasAdd/ReadVariableOp�
model/joined/BiasAddBiasAddmodel/joined/MatMul:product:0+model/joined/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
model/joined/BiasAdd�
model/joined/SigmoidSigmoidmodel/joined/BiasAdd:output:0*
T0*'
_output_shapes
:���������2
model/joined/Sigmoids
IdentityIdentitymodel/joined/Sigmoid:y:0^NoOp*
T0*'
_output_shapes
:���������2

Identity�
NoOpNoOp2^model/fmn2_pca_5000_1000_1/BiasAdd/ReadVariableOp1^model/fmn2_pca_5000_1000_1/MatMul/ReadVariableOp2^model/fmn2_pca_5000_1000_2/BiasAdd/ReadVariableOp1^model/fmn2_pca_5000_1000_2/MatMul/ReadVariableOp2^model/fmn2_pca_5000_1000_3/BiasAdd/ReadVariableOp1^model/fmn2_pca_5000_1000_3/MatMul/ReadVariableOp4^model/fmn2_pca_5000_1000_out/BiasAdd/ReadVariableOp3^model/fmn2_pca_5000_1000_out/MatMul/ReadVariableOp3^model/fmn2_tsvd_5000_1000_1/BiasAdd/ReadVariableOp2^model/fmn2_tsvd_5000_1000_1/MatMul/ReadVariableOp3^model/fmn2_tsvd_5000_1000_2/BiasAdd/ReadVariableOp2^model/fmn2_tsvd_5000_1000_2/MatMul/ReadVariableOp3^model/fmn2_tsvd_5000_1000_3/BiasAdd/ReadVariableOp2^model/fmn2_tsvd_5000_1000_3/MatMul/ReadVariableOp5^model/fmn2_tsvd_5000_1000_out/BiasAdd/ReadVariableOp4^model/fmn2_tsvd_5000_1000_out/MatMul/ReadVariableOp$^model/joined/BiasAdd/ReadVariableOp#^model/joined/MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*_
_input_shapesN
L:����������:����������: : : : : : : : : : : : : : : : : : 2f
1model/fmn2_pca_5000_1000_1/BiasAdd/ReadVariableOp1model/fmn2_pca_5000_1000_1/BiasAdd/ReadVariableOp2d
0model/fmn2_pca_5000_1000_1/MatMul/ReadVariableOp0model/fmn2_pca_5000_1000_1/MatMul/ReadVariableOp2f
1model/fmn2_pca_5000_1000_2/BiasAdd/ReadVariableOp1model/fmn2_pca_5000_1000_2/BiasAdd/ReadVariableOp2d
0model/fmn2_pca_5000_1000_2/MatMul/ReadVariableOp0model/fmn2_pca_5000_1000_2/MatMul/ReadVariableOp2f
1model/fmn2_pca_5000_1000_3/BiasAdd/ReadVariableOp1model/fmn2_pca_5000_1000_3/BiasAdd/ReadVariableOp2d
0model/fmn2_pca_5000_1000_3/MatMul/ReadVariableOp0model/fmn2_pca_5000_1000_3/MatMul/ReadVariableOp2j
3model/fmn2_pca_5000_1000_out/BiasAdd/ReadVariableOp3model/fmn2_pca_5000_1000_out/BiasAdd/ReadVariableOp2h
2model/fmn2_pca_5000_1000_out/MatMul/ReadVariableOp2model/fmn2_pca_5000_1000_out/MatMul/ReadVariableOp2h
2model/fmn2_tsvd_5000_1000_1/BiasAdd/ReadVariableOp2model/fmn2_tsvd_5000_1000_1/BiasAdd/ReadVariableOp2f
1model/fmn2_tsvd_5000_1000_1/MatMul/ReadVariableOp1model/fmn2_tsvd_5000_1000_1/MatMul/ReadVariableOp2h
2model/fmn2_tsvd_5000_1000_2/BiasAdd/ReadVariableOp2model/fmn2_tsvd_5000_1000_2/BiasAdd/ReadVariableOp2f
1model/fmn2_tsvd_5000_1000_2/MatMul/ReadVariableOp1model/fmn2_tsvd_5000_1000_2/MatMul/ReadVariableOp2h
2model/fmn2_tsvd_5000_1000_3/BiasAdd/ReadVariableOp2model/fmn2_tsvd_5000_1000_3/BiasAdd/ReadVariableOp2f
1model/fmn2_tsvd_5000_1000_3/MatMul/ReadVariableOp1model/fmn2_tsvd_5000_1000_3/MatMul/ReadVariableOp2l
4model/fmn2_tsvd_5000_1000_out/BiasAdd/ReadVariableOp4model/fmn2_tsvd_5000_1000_out/BiasAdd/ReadVariableOp2j
3model/fmn2_tsvd_5000_1000_out/MatMul/ReadVariableOp3model/fmn2_tsvd_5000_1000_out/MatMul/ReadVariableOp2J
#model/joined/BiasAdd/ReadVariableOp#model/joined/BiasAdd/ReadVariableOp2H
"model/joined/MatMul/ReadVariableOp"model/joined/MatMul/ReadVariableOp:b ^
(
_output_shapes
:����������
2
_user_specified_namefmn2_pca_5000_1000_input:c_
(
_output_shapes
:����������
3
_user_specified_namefmn2_tsvd_5000_1000_input
�
�
6__inference_fmn2_pca_5000_1000_1_layer_call_fn_1539534

inputs
unknown:
��
	unknown_0:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_fmn2_pca_5000_1000_1_layer_call_and_return_conditional_losses_15387012
StatefulPartitionedCall|
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:����������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
8__inference_fmn2_pca_5000_1000_out_layer_call_fn_1539654

inputs
unknown:	�
	unknown_0:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *\
fWRU
S__inference_fmn2_pca_5000_1000_out_layer_call_and_return_conditional_losses_15387862
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
R__inference_fmn2_tsvd_5000_1000_2_layer_call_and_return_conditional_losses_1539605

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
#__inference__traced_restore_1539938
file_prefix@
,assignvariableop_fmn2_pca_5000_1000_1_kernel:
��;
,assignvariableop_1_fmn2_pca_5000_1000_1_bias:	�C
/assignvariableop_2_fmn2_tsvd_5000_1000_1_kernel:
��<
-assignvariableop_3_fmn2_tsvd_5000_1000_1_bias:	�B
.assignvariableop_4_fmn2_pca_5000_1000_2_kernel:
��;
,assignvariableop_5_fmn2_pca_5000_1000_2_bias:	�C
/assignvariableop_6_fmn2_tsvd_5000_1000_2_kernel:
��<
-assignvariableop_7_fmn2_tsvd_5000_1000_2_bias:	�B
.assignvariableop_8_fmn2_pca_5000_1000_3_kernel:
��;
,assignvariableop_9_fmn2_pca_5000_1000_3_bias:	�D
0assignvariableop_10_fmn2_tsvd_5000_1000_3_kernel:
��=
.assignvariableop_11_fmn2_tsvd_5000_1000_3_bias:	�D
1assignvariableop_12_fmn2_pca_5000_1000_out_kernel:	�=
/assignvariableop_13_fmn2_pca_5000_1000_out_bias:E
2assignvariableop_14_fmn2_tsvd_5000_1000_out_kernel:	�>
0assignvariableop_15_fmn2_tsvd_5000_1000_out_bias:3
!assignvariableop_16_joined_kernel:-
assignvariableop_17_joined_bias:'
assignvariableop_18_adam_iter:	 )
assignvariableop_19_adam_beta_1: )
assignvariableop_20_adam_beta_2: (
assignvariableop_21_adam_decay: 0
&assignvariableop_22_adam_learning_rate: #
assignvariableop_23_total: #
assignvariableop_24_count: %
assignvariableop_25_total_1: %
assignvariableop_26_count_1: :
(assignvariableop_27_adam_joined_kernel_m:4
&assignvariableop_28_adam_joined_bias_m::
(assignvariableop_29_adam_joined_kernel_v:4
&assignvariableop_30_adam_joined_bias_v:
identity_32��AssignVariableOp�AssignVariableOp_1�AssignVariableOp_10�AssignVariableOp_11�AssignVariableOp_12�AssignVariableOp_13�AssignVariableOp_14�AssignVariableOp_15�AssignVariableOp_16�AssignVariableOp_17�AssignVariableOp_18�AssignVariableOp_19�AssignVariableOp_2�AssignVariableOp_20�AssignVariableOp_21�AssignVariableOp_22�AssignVariableOp_23�AssignVariableOp_24�AssignVariableOp_25�AssignVariableOp_26�AssignVariableOp_27�AssignVariableOp_28�AssignVariableOp_29�AssignVariableOp_3�AssignVariableOp_30�AssignVariableOp_4�AssignVariableOp_5�AssignVariableOp_6�AssignVariableOp_7�AssignVariableOp_8�AssignVariableOp_9�
RestoreV2/tensor_namesConst"/device:CPU:0*
_output_shapes
: *
dtype0*�
value�B� B6layer_with_weights-0/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-0/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-1/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-1/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-3/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-3/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-4/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-4/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-5/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-5/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-6/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-6/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-7/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-7/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-8/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-8/bias/.ATTRIBUTES/VARIABLE_VALUEB)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUEB*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/count/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-8/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-8/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-8/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-8/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEB_CHECKPOINTABLE_OBJECT_GRAPH2
RestoreV2/tensor_names�
RestoreV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
: *
dtype0*S
valueJBH B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B 2
RestoreV2/shape_and_slices�
	RestoreV2	RestoreV2file_prefixRestoreV2/tensor_names:output:0#RestoreV2/shape_and_slices:output:0"/device:CPU:0*�
_output_shapes�
�::::::::::::::::::::::::::::::::*.
dtypes$
"2 	2
	RestoreV2g
IdentityIdentityRestoreV2:tensors:0"/device:CPU:0*
T0*
_output_shapes
:2

Identity�
AssignVariableOpAssignVariableOp,assignvariableop_fmn2_pca_5000_1000_1_kernelIdentity:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOpk

Identity_1IdentityRestoreV2:tensors:1"/device:CPU:0*
T0*
_output_shapes
:2

Identity_1�
AssignVariableOp_1AssignVariableOp,assignvariableop_1_fmn2_pca_5000_1000_1_biasIdentity_1:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_1k

Identity_2IdentityRestoreV2:tensors:2"/device:CPU:0*
T0*
_output_shapes
:2

Identity_2�
AssignVariableOp_2AssignVariableOp/assignvariableop_2_fmn2_tsvd_5000_1000_1_kernelIdentity_2:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_2k

Identity_3IdentityRestoreV2:tensors:3"/device:CPU:0*
T0*
_output_shapes
:2

Identity_3�
AssignVariableOp_3AssignVariableOp-assignvariableop_3_fmn2_tsvd_5000_1000_1_biasIdentity_3:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_3k

Identity_4IdentityRestoreV2:tensors:4"/device:CPU:0*
T0*
_output_shapes
:2

Identity_4�
AssignVariableOp_4AssignVariableOp.assignvariableop_4_fmn2_pca_5000_1000_2_kernelIdentity_4:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_4k

Identity_5IdentityRestoreV2:tensors:5"/device:CPU:0*
T0*
_output_shapes
:2

Identity_5�
AssignVariableOp_5AssignVariableOp,assignvariableop_5_fmn2_pca_5000_1000_2_biasIdentity_5:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_5k

Identity_6IdentityRestoreV2:tensors:6"/device:CPU:0*
T0*
_output_shapes
:2

Identity_6�
AssignVariableOp_6AssignVariableOp/assignvariableop_6_fmn2_tsvd_5000_1000_2_kernelIdentity_6:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_6k

Identity_7IdentityRestoreV2:tensors:7"/device:CPU:0*
T0*
_output_shapes
:2

Identity_7�
AssignVariableOp_7AssignVariableOp-assignvariableop_7_fmn2_tsvd_5000_1000_2_biasIdentity_7:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_7k

Identity_8IdentityRestoreV2:tensors:8"/device:CPU:0*
T0*
_output_shapes
:2

Identity_8�
AssignVariableOp_8AssignVariableOp.assignvariableop_8_fmn2_pca_5000_1000_3_kernelIdentity_8:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_8k

Identity_9IdentityRestoreV2:tensors:9"/device:CPU:0*
T0*
_output_shapes
:2

Identity_9�
AssignVariableOp_9AssignVariableOp,assignvariableop_9_fmn2_pca_5000_1000_3_biasIdentity_9:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_9n
Identity_10IdentityRestoreV2:tensors:10"/device:CPU:0*
T0*
_output_shapes
:2
Identity_10�
AssignVariableOp_10AssignVariableOp0assignvariableop_10_fmn2_tsvd_5000_1000_3_kernelIdentity_10:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_10n
Identity_11IdentityRestoreV2:tensors:11"/device:CPU:0*
T0*
_output_shapes
:2
Identity_11�
AssignVariableOp_11AssignVariableOp.assignvariableop_11_fmn2_tsvd_5000_1000_3_biasIdentity_11:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_11n
Identity_12IdentityRestoreV2:tensors:12"/device:CPU:0*
T0*
_output_shapes
:2
Identity_12�
AssignVariableOp_12AssignVariableOp1assignvariableop_12_fmn2_pca_5000_1000_out_kernelIdentity_12:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_12n
Identity_13IdentityRestoreV2:tensors:13"/device:CPU:0*
T0*
_output_shapes
:2
Identity_13�
AssignVariableOp_13AssignVariableOp/assignvariableop_13_fmn2_pca_5000_1000_out_biasIdentity_13:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_13n
Identity_14IdentityRestoreV2:tensors:14"/device:CPU:0*
T0*
_output_shapes
:2
Identity_14�
AssignVariableOp_14AssignVariableOp2assignvariableop_14_fmn2_tsvd_5000_1000_out_kernelIdentity_14:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_14n
Identity_15IdentityRestoreV2:tensors:15"/device:CPU:0*
T0*
_output_shapes
:2
Identity_15�
AssignVariableOp_15AssignVariableOp0assignvariableop_15_fmn2_tsvd_5000_1000_out_biasIdentity_15:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_15n
Identity_16IdentityRestoreV2:tensors:16"/device:CPU:0*
T0*
_output_shapes
:2
Identity_16�
AssignVariableOp_16AssignVariableOp!assignvariableop_16_joined_kernelIdentity_16:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_16n
Identity_17IdentityRestoreV2:tensors:17"/device:CPU:0*
T0*
_output_shapes
:2
Identity_17�
AssignVariableOp_17AssignVariableOpassignvariableop_17_joined_biasIdentity_17:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_17n
Identity_18IdentityRestoreV2:tensors:18"/device:CPU:0*
T0	*
_output_shapes
:2
Identity_18�
AssignVariableOp_18AssignVariableOpassignvariableop_18_adam_iterIdentity_18:output:0"/device:CPU:0*
_output_shapes
 *
dtype0	2
AssignVariableOp_18n
Identity_19IdentityRestoreV2:tensors:19"/device:CPU:0*
T0*
_output_shapes
:2
Identity_19�
AssignVariableOp_19AssignVariableOpassignvariableop_19_adam_beta_1Identity_19:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_19n
Identity_20IdentityRestoreV2:tensors:20"/device:CPU:0*
T0*
_output_shapes
:2
Identity_20�
AssignVariableOp_20AssignVariableOpassignvariableop_20_adam_beta_2Identity_20:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_20n
Identity_21IdentityRestoreV2:tensors:21"/device:CPU:0*
T0*
_output_shapes
:2
Identity_21�
AssignVariableOp_21AssignVariableOpassignvariableop_21_adam_decayIdentity_21:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_21n
Identity_22IdentityRestoreV2:tensors:22"/device:CPU:0*
T0*
_output_shapes
:2
Identity_22�
AssignVariableOp_22AssignVariableOp&assignvariableop_22_adam_learning_rateIdentity_22:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_22n
Identity_23IdentityRestoreV2:tensors:23"/device:CPU:0*
T0*
_output_shapes
:2
Identity_23�
AssignVariableOp_23AssignVariableOpassignvariableop_23_totalIdentity_23:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_23n
Identity_24IdentityRestoreV2:tensors:24"/device:CPU:0*
T0*
_output_shapes
:2
Identity_24�
AssignVariableOp_24AssignVariableOpassignvariableop_24_countIdentity_24:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_24n
Identity_25IdentityRestoreV2:tensors:25"/device:CPU:0*
T0*
_output_shapes
:2
Identity_25�
AssignVariableOp_25AssignVariableOpassignvariableop_25_total_1Identity_25:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_25n
Identity_26IdentityRestoreV2:tensors:26"/device:CPU:0*
T0*
_output_shapes
:2
Identity_26�
AssignVariableOp_26AssignVariableOpassignvariableop_26_count_1Identity_26:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_26n
Identity_27IdentityRestoreV2:tensors:27"/device:CPU:0*
T0*
_output_shapes
:2
Identity_27�
AssignVariableOp_27AssignVariableOp(assignvariableop_27_adam_joined_kernel_mIdentity_27:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_27n
Identity_28IdentityRestoreV2:tensors:28"/device:CPU:0*
T0*
_output_shapes
:2
Identity_28�
AssignVariableOp_28AssignVariableOp&assignvariableop_28_adam_joined_bias_mIdentity_28:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_28n
Identity_29IdentityRestoreV2:tensors:29"/device:CPU:0*
T0*
_output_shapes
:2
Identity_29�
AssignVariableOp_29AssignVariableOp(assignvariableop_29_adam_joined_kernel_vIdentity_29:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_29n
Identity_30IdentityRestoreV2:tensors:30"/device:CPU:0*
T0*
_output_shapes
:2
Identity_30�
AssignVariableOp_30AssignVariableOp&assignvariableop_30_adam_joined_bias_vIdentity_30:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_309
NoOpNoOp"/device:CPU:0*
_output_shapes
 2
NoOp�
Identity_31Identityfile_prefix^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_12^AssignVariableOp_13^AssignVariableOp_14^AssignVariableOp_15^AssignVariableOp_16^AssignVariableOp_17^AssignVariableOp_18^AssignVariableOp_19^AssignVariableOp_2^AssignVariableOp_20^AssignVariableOp_21^AssignVariableOp_22^AssignVariableOp_23^AssignVariableOp_24^AssignVariableOp_25^AssignVariableOp_26^AssignVariableOp_27^AssignVariableOp_28^AssignVariableOp_29^AssignVariableOp_3^AssignVariableOp_30^AssignVariableOp_4^AssignVariableOp_5^AssignVariableOp_6^AssignVariableOp_7^AssignVariableOp_8^AssignVariableOp_9^NoOp"/device:CPU:0*
T0*
_output_shapes
: 2
Identity_31f
Identity_32IdentityIdentity_31:output:0^NoOp_1*
T0*
_output_shapes
: 2
Identity_32�
NoOp_1NoOp^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_12^AssignVariableOp_13^AssignVariableOp_14^AssignVariableOp_15^AssignVariableOp_16^AssignVariableOp_17^AssignVariableOp_18^AssignVariableOp_19^AssignVariableOp_2^AssignVariableOp_20^AssignVariableOp_21^AssignVariableOp_22^AssignVariableOp_23^AssignVariableOp_24^AssignVariableOp_25^AssignVariableOp_26^AssignVariableOp_27^AssignVariableOp_28^AssignVariableOp_29^AssignVariableOp_3^AssignVariableOp_30^AssignVariableOp_4^AssignVariableOp_5^AssignVariableOp_6^AssignVariableOp_7^AssignVariableOp_8^AssignVariableOp_9*"
_acd_function_control_output(*
_output_shapes
 2
NoOp_1"#
identity_32Identity_32:output:0*S
_input_shapesB
@: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 2$
AssignVariableOpAssignVariableOp2(
AssignVariableOp_1AssignVariableOp_12*
AssignVariableOp_10AssignVariableOp_102*
AssignVariableOp_11AssignVariableOp_112*
AssignVariableOp_12AssignVariableOp_122*
AssignVariableOp_13AssignVariableOp_132*
AssignVariableOp_14AssignVariableOp_142*
AssignVariableOp_15AssignVariableOp_152*
AssignVariableOp_16AssignVariableOp_162*
AssignVariableOp_17AssignVariableOp_172*
AssignVariableOp_18AssignVariableOp_182*
AssignVariableOp_19AssignVariableOp_192(
AssignVariableOp_2AssignVariableOp_22*
AssignVariableOp_20AssignVariableOp_202*
AssignVariableOp_21AssignVariableOp_212*
AssignVariableOp_22AssignVariableOp_222*
AssignVariableOp_23AssignVariableOp_232*
AssignVariableOp_24AssignVariableOp_242*
AssignVariableOp_25AssignVariableOp_252*
AssignVariableOp_26AssignVariableOp_262*
AssignVariableOp_27AssignVariableOp_272*
AssignVariableOp_28AssignVariableOp_282*
AssignVariableOp_29AssignVariableOp_292(
AssignVariableOp_3AssignVariableOp_32*
AssignVariableOp_30AssignVariableOp_302(
AssignVariableOp_4AssignVariableOp_42(
AssignVariableOp_5AssignVariableOp_52(
AssignVariableOp_6AssignVariableOp_62(
AssignVariableOp_7AssignVariableOp_72(
AssignVariableOp_8AssignVariableOp_82(
AssignVariableOp_9AssignVariableOp_9:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix
�
�
R__inference_fmn2_tsvd_5000_1000_1_layer_call_and_return_conditional_losses_1538684

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�@
�

B__inference_model_layer_call_and_return_conditional_losses_1538836

inputs
inputs_11
fmn2_tsvd_5000_1000_1_1538685:
��,
fmn2_tsvd_5000_1000_1_1538687:	�0
fmn2_pca_5000_1000_1_1538702:
��+
fmn2_pca_5000_1000_1_1538704:	�1
fmn2_tsvd_5000_1000_2_1538719:
��,
fmn2_tsvd_5000_1000_2_1538721:	�0
fmn2_pca_5000_1000_2_1538736:
��+
fmn2_pca_5000_1000_2_1538738:	�1
fmn2_tsvd_5000_1000_3_1538753:
��,
fmn2_tsvd_5000_1000_3_1538755:	�0
fmn2_pca_5000_1000_3_1538770:
��+
fmn2_pca_5000_1000_3_1538772:	�1
fmn2_pca_5000_1000_out_1538787:	�,
fmn2_pca_5000_1000_out_1538789:2
fmn2_tsvd_5000_1000_out_1538804:	�-
fmn2_tsvd_5000_1000_out_1538806: 
joined_1538830:
joined_1538832:
identity��,fmn2_pca_5000_1000_1/StatefulPartitionedCall�,fmn2_pca_5000_1000_2/StatefulPartitionedCall�,fmn2_pca_5000_1000_3/StatefulPartitionedCall�.fmn2_pca_5000_1000_out/StatefulPartitionedCall�-fmn2_tsvd_5000_1000_1/StatefulPartitionedCall�-fmn2_tsvd_5000_1000_2/StatefulPartitionedCall�-fmn2_tsvd_5000_1000_3/StatefulPartitionedCall�/fmn2_tsvd_5000_1000_out/StatefulPartitionedCall�joined/StatefulPartitionedCall�
-fmn2_tsvd_5000_1000_1/StatefulPartitionedCallStatefulPartitionedCallinputs_1fmn2_tsvd_5000_1000_1_1538685fmn2_tsvd_5000_1000_1_1538687*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_fmn2_tsvd_5000_1000_1_layer_call_and_return_conditional_losses_15386842/
-fmn2_tsvd_5000_1000_1/StatefulPartitionedCall�
,fmn2_pca_5000_1000_1/StatefulPartitionedCallStatefulPartitionedCallinputsfmn2_pca_5000_1000_1_1538702fmn2_pca_5000_1000_1_1538704*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_fmn2_pca_5000_1000_1_layer_call_and_return_conditional_losses_15387012.
,fmn2_pca_5000_1000_1/StatefulPartitionedCall�
-fmn2_tsvd_5000_1000_2/StatefulPartitionedCallStatefulPartitionedCall6fmn2_tsvd_5000_1000_1/StatefulPartitionedCall:output:0fmn2_tsvd_5000_1000_2_1538719fmn2_tsvd_5000_1000_2_1538721*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_fmn2_tsvd_5000_1000_2_layer_call_and_return_conditional_losses_15387182/
-fmn2_tsvd_5000_1000_2/StatefulPartitionedCall�
,fmn2_pca_5000_1000_2/StatefulPartitionedCallStatefulPartitionedCall5fmn2_pca_5000_1000_1/StatefulPartitionedCall:output:0fmn2_pca_5000_1000_2_1538736fmn2_pca_5000_1000_2_1538738*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_fmn2_pca_5000_1000_2_layer_call_and_return_conditional_losses_15387352.
,fmn2_pca_5000_1000_2/StatefulPartitionedCall�
-fmn2_tsvd_5000_1000_3/StatefulPartitionedCallStatefulPartitionedCall6fmn2_tsvd_5000_1000_2/StatefulPartitionedCall:output:0fmn2_tsvd_5000_1000_3_1538753fmn2_tsvd_5000_1000_3_1538755*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_fmn2_tsvd_5000_1000_3_layer_call_and_return_conditional_losses_15387522/
-fmn2_tsvd_5000_1000_3/StatefulPartitionedCall�
,fmn2_pca_5000_1000_3/StatefulPartitionedCallStatefulPartitionedCall5fmn2_pca_5000_1000_2/StatefulPartitionedCall:output:0fmn2_pca_5000_1000_3_1538770fmn2_pca_5000_1000_3_1538772*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_fmn2_pca_5000_1000_3_layer_call_and_return_conditional_losses_15387692.
,fmn2_pca_5000_1000_3/StatefulPartitionedCall�
.fmn2_pca_5000_1000_out/StatefulPartitionedCallStatefulPartitionedCall5fmn2_pca_5000_1000_3/StatefulPartitionedCall:output:0fmn2_pca_5000_1000_out_1538787fmn2_pca_5000_1000_out_1538789*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *\
fWRU
S__inference_fmn2_pca_5000_1000_out_layer_call_and_return_conditional_losses_153878620
.fmn2_pca_5000_1000_out/StatefulPartitionedCall�
/fmn2_tsvd_5000_1000_out/StatefulPartitionedCallStatefulPartitionedCall6fmn2_tsvd_5000_1000_3/StatefulPartitionedCall:output:0fmn2_tsvd_5000_1000_out_1538804fmn2_tsvd_5000_1000_out_1538806*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *]
fXRV
T__inference_fmn2_tsvd_5000_1000_out_layer_call_and_return_conditional_losses_153880321
/fmn2_tsvd_5000_1000_out/StatefulPartitionedCall�
concatenate_46/PartitionedCallPartitionedCall7fmn2_pca_5000_1000_out/StatefulPartitionedCall:output:08fmn2_tsvd_5000_1000_out/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *T
fORM
K__inference_concatenate_46_layer_call_and_return_conditional_losses_15388162 
concatenate_46/PartitionedCall�
joined/StatefulPartitionedCallStatefulPartitionedCall'concatenate_46/PartitionedCall:output:0joined_1538830joined_1538832*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *L
fGRE
C__inference_joined_layer_call_and_return_conditional_losses_15388292 
joined/StatefulPartitionedCall�
IdentityIdentity'joined/StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identity�
NoOpNoOp-^fmn2_pca_5000_1000_1/StatefulPartitionedCall-^fmn2_pca_5000_1000_2/StatefulPartitionedCall-^fmn2_pca_5000_1000_3/StatefulPartitionedCall/^fmn2_pca_5000_1000_out/StatefulPartitionedCall.^fmn2_tsvd_5000_1000_1/StatefulPartitionedCall.^fmn2_tsvd_5000_1000_2/StatefulPartitionedCall.^fmn2_tsvd_5000_1000_3/StatefulPartitionedCall0^fmn2_tsvd_5000_1000_out/StatefulPartitionedCall^joined/StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*_
_input_shapesN
L:����������:����������: : : : : : : : : : : : : : : : : : 2\
,fmn2_pca_5000_1000_1/StatefulPartitionedCall,fmn2_pca_5000_1000_1/StatefulPartitionedCall2\
,fmn2_pca_5000_1000_2/StatefulPartitionedCall,fmn2_pca_5000_1000_2/StatefulPartitionedCall2\
,fmn2_pca_5000_1000_3/StatefulPartitionedCall,fmn2_pca_5000_1000_3/StatefulPartitionedCall2`
.fmn2_pca_5000_1000_out/StatefulPartitionedCall.fmn2_pca_5000_1000_out/StatefulPartitionedCall2^
-fmn2_tsvd_5000_1000_1/StatefulPartitionedCall-fmn2_tsvd_5000_1000_1/StatefulPartitionedCall2^
-fmn2_tsvd_5000_1000_2/StatefulPartitionedCall-fmn2_tsvd_5000_1000_2/StatefulPartitionedCall2^
-fmn2_tsvd_5000_1000_3/StatefulPartitionedCall-fmn2_tsvd_5000_1000_3/StatefulPartitionedCall2b
/fmn2_tsvd_5000_1000_out/StatefulPartitionedCall/fmn2_tsvd_5000_1000_out/StatefulPartitionedCall2@
joined/StatefulPartitionedCalljoined/StatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs:PL
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
Q__inference_fmn2_pca_5000_1000_2_layer_call_and_return_conditional_losses_1538735

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
%__inference_signature_wrapper_1539301
fmn2_pca_5000_1000_input
fmn2_tsvd_5000_1000_input
unknown:
��
	unknown_0:	�
	unknown_1:
��
	unknown_2:	�
	unknown_3:
��
	unknown_4:	�
	unknown_5:
��
	unknown_6:	�
	unknown_7:
��
	unknown_8:	�
	unknown_9:
��

unknown_10:	�

unknown_11:	�

unknown_12:

unknown_13:	�

unknown_14:

unknown_15:

unknown_16:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallfmn2_pca_5000_1000_inputfmn2_tsvd_5000_1000_inputunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10
unknown_11
unknown_12
unknown_13
unknown_14
unknown_15
unknown_16*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*4
_read_only_resource_inputs
	
*0
config_proto 

CPU

GPU2*0J 8� *+
f&R$
"__inference__wrapped_model_15386642
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*_
_input_shapesN
L:����������:����������: : : : : : : : : : : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:b ^
(
_output_shapes
:����������
2
_user_specified_namefmn2_pca_5000_1000_input:c_
(
_output_shapes
:����������
3
_user_specified_namefmn2_tsvd_5000_1000_input
�
�
6__inference_fmn2_pca_5000_1000_2_layer_call_fn_1539574

inputs
unknown:
��
	unknown_0:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_fmn2_pca_5000_1000_2_layer_call_and_return_conditional_losses_15387352
StatefulPartitionedCall|
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:����������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
R__inference_fmn2_tsvd_5000_1000_2_layer_call_and_return_conditional_losses_1538718

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�p
�
B__inference_model_layer_call_and_return_conditional_losses_1539455
inputs_0
inputs_1H
4fmn2_tsvd_5000_1000_1_matmul_readvariableop_resource:
��D
5fmn2_tsvd_5000_1000_1_biasadd_readvariableop_resource:	�G
3fmn2_pca_5000_1000_1_matmul_readvariableop_resource:
��C
4fmn2_pca_5000_1000_1_biasadd_readvariableop_resource:	�H
4fmn2_tsvd_5000_1000_2_matmul_readvariableop_resource:
��D
5fmn2_tsvd_5000_1000_2_biasadd_readvariableop_resource:	�G
3fmn2_pca_5000_1000_2_matmul_readvariableop_resource:
��C
4fmn2_pca_5000_1000_2_biasadd_readvariableop_resource:	�H
4fmn2_tsvd_5000_1000_3_matmul_readvariableop_resource:
��D
5fmn2_tsvd_5000_1000_3_biasadd_readvariableop_resource:	�G
3fmn2_pca_5000_1000_3_matmul_readvariableop_resource:
��C
4fmn2_pca_5000_1000_3_biasadd_readvariableop_resource:	�H
5fmn2_pca_5000_1000_out_matmul_readvariableop_resource:	�D
6fmn2_pca_5000_1000_out_biasadd_readvariableop_resource:I
6fmn2_tsvd_5000_1000_out_matmul_readvariableop_resource:	�E
7fmn2_tsvd_5000_1000_out_biasadd_readvariableop_resource:7
%joined_matmul_readvariableop_resource:4
&joined_biasadd_readvariableop_resource:
identity��+fmn2_pca_5000_1000_1/BiasAdd/ReadVariableOp�*fmn2_pca_5000_1000_1/MatMul/ReadVariableOp�+fmn2_pca_5000_1000_2/BiasAdd/ReadVariableOp�*fmn2_pca_5000_1000_2/MatMul/ReadVariableOp�+fmn2_pca_5000_1000_3/BiasAdd/ReadVariableOp�*fmn2_pca_5000_1000_3/MatMul/ReadVariableOp�-fmn2_pca_5000_1000_out/BiasAdd/ReadVariableOp�,fmn2_pca_5000_1000_out/MatMul/ReadVariableOp�,fmn2_tsvd_5000_1000_1/BiasAdd/ReadVariableOp�+fmn2_tsvd_5000_1000_1/MatMul/ReadVariableOp�,fmn2_tsvd_5000_1000_2/BiasAdd/ReadVariableOp�+fmn2_tsvd_5000_1000_2/MatMul/ReadVariableOp�,fmn2_tsvd_5000_1000_3/BiasAdd/ReadVariableOp�+fmn2_tsvd_5000_1000_3/MatMul/ReadVariableOp�.fmn2_tsvd_5000_1000_out/BiasAdd/ReadVariableOp�-fmn2_tsvd_5000_1000_out/MatMul/ReadVariableOp�joined/BiasAdd/ReadVariableOp�joined/MatMul/ReadVariableOp�
+fmn2_tsvd_5000_1000_1/MatMul/ReadVariableOpReadVariableOp4fmn2_tsvd_5000_1000_1_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02-
+fmn2_tsvd_5000_1000_1/MatMul/ReadVariableOp�
fmn2_tsvd_5000_1000_1/MatMulMatMulinputs_13fmn2_tsvd_5000_1000_1/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn2_tsvd_5000_1000_1/MatMul�
,fmn2_tsvd_5000_1000_1/BiasAdd/ReadVariableOpReadVariableOp5fmn2_tsvd_5000_1000_1_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02.
,fmn2_tsvd_5000_1000_1/BiasAdd/ReadVariableOp�
fmn2_tsvd_5000_1000_1/BiasAddBiasAdd&fmn2_tsvd_5000_1000_1/MatMul:product:04fmn2_tsvd_5000_1000_1/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn2_tsvd_5000_1000_1/BiasAdd�
fmn2_tsvd_5000_1000_1/ReluRelu&fmn2_tsvd_5000_1000_1/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
fmn2_tsvd_5000_1000_1/Relu�
*fmn2_pca_5000_1000_1/MatMul/ReadVariableOpReadVariableOp3fmn2_pca_5000_1000_1_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02,
*fmn2_pca_5000_1000_1/MatMul/ReadVariableOp�
fmn2_pca_5000_1000_1/MatMulMatMulinputs_02fmn2_pca_5000_1000_1/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn2_pca_5000_1000_1/MatMul�
+fmn2_pca_5000_1000_1/BiasAdd/ReadVariableOpReadVariableOp4fmn2_pca_5000_1000_1_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02-
+fmn2_pca_5000_1000_1/BiasAdd/ReadVariableOp�
fmn2_pca_5000_1000_1/BiasAddBiasAdd%fmn2_pca_5000_1000_1/MatMul:product:03fmn2_pca_5000_1000_1/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn2_pca_5000_1000_1/BiasAdd�
fmn2_pca_5000_1000_1/ReluRelu%fmn2_pca_5000_1000_1/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
fmn2_pca_5000_1000_1/Relu�
+fmn2_tsvd_5000_1000_2/MatMul/ReadVariableOpReadVariableOp4fmn2_tsvd_5000_1000_2_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02-
+fmn2_tsvd_5000_1000_2/MatMul/ReadVariableOp�
fmn2_tsvd_5000_1000_2/MatMulMatMul(fmn2_tsvd_5000_1000_1/Relu:activations:03fmn2_tsvd_5000_1000_2/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn2_tsvd_5000_1000_2/MatMul�
,fmn2_tsvd_5000_1000_2/BiasAdd/ReadVariableOpReadVariableOp5fmn2_tsvd_5000_1000_2_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02.
,fmn2_tsvd_5000_1000_2/BiasAdd/ReadVariableOp�
fmn2_tsvd_5000_1000_2/BiasAddBiasAdd&fmn2_tsvd_5000_1000_2/MatMul:product:04fmn2_tsvd_5000_1000_2/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn2_tsvd_5000_1000_2/BiasAdd�
fmn2_tsvd_5000_1000_2/ReluRelu&fmn2_tsvd_5000_1000_2/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
fmn2_tsvd_5000_1000_2/Relu�
*fmn2_pca_5000_1000_2/MatMul/ReadVariableOpReadVariableOp3fmn2_pca_5000_1000_2_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02,
*fmn2_pca_5000_1000_2/MatMul/ReadVariableOp�
fmn2_pca_5000_1000_2/MatMulMatMul'fmn2_pca_5000_1000_1/Relu:activations:02fmn2_pca_5000_1000_2/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn2_pca_5000_1000_2/MatMul�
+fmn2_pca_5000_1000_2/BiasAdd/ReadVariableOpReadVariableOp4fmn2_pca_5000_1000_2_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02-
+fmn2_pca_5000_1000_2/BiasAdd/ReadVariableOp�
fmn2_pca_5000_1000_2/BiasAddBiasAdd%fmn2_pca_5000_1000_2/MatMul:product:03fmn2_pca_5000_1000_2/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn2_pca_5000_1000_2/BiasAdd�
fmn2_pca_5000_1000_2/ReluRelu%fmn2_pca_5000_1000_2/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
fmn2_pca_5000_1000_2/Relu�
+fmn2_tsvd_5000_1000_3/MatMul/ReadVariableOpReadVariableOp4fmn2_tsvd_5000_1000_3_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02-
+fmn2_tsvd_5000_1000_3/MatMul/ReadVariableOp�
fmn2_tsvd_5000_1000_3/MatMulMatMul(fmn2_tsvd_5000_1000_2/Relu:activations:03fmn2_tsvd_5000_1000_3/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn2_tsvd_5000_1000_3/MatMul�
,fmn2_tsvd_5000_1000_3/BiasAdd/ReadVariableOpReadVariableOp5fmn2_tsvd_5000_1000_3_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02.
,fmn2_tsvd_5000_1000_3/BiasAdd/ReadVariableOp�
fmn2_tsvd_5000_1000_3/BiasAddBiasAdd&fmn2_tsvd_5000_1000_3/MatMul:product:04fmn2_tsvd_5000_1000_3/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn2_tsvd_5000_1000_3/BiasAdd�
fmn2_tsvd_5000_1000_3/ReluRelu&fmn2_tsvd_5000_1000_3/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
fmn2_tsvd_5000_1000_3/Relu�
*fmn2_pca_5000_1000_3/MatMul/ReadVariableOpReadVariableOp3fmn2_pca_5000_1000_3_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02,
*fmn2_pca_5000_1000_3/MatMul/ReadVariableOp�
fmn2_pca_5000_1000_3/MatMulMatMul'fmn2_pca_5000_1000_2/Relu:activations:02fmn2_pca_5000_1000_3/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn2_pca_5000_1000_3/MatMul�
+fmn2_pca_5000_1000_3/BiasAdd/ReadVariableOpReadVariableOp4fmn2_pca_5000_1000_3_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02-
+fmn2_pca_5000_1000_3/BiasAdd/ReadVariableOp�
fmn2_pca_5000_1000_3/BiasAddBiasAdd%fmn2_pca_5000_1000_3/MatMul:product:03fmn2_pca_5000_1000_3/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn2_pca_5000_1000_3/BiasAdd�
fmn2_pca_5000_1000_3/ReluRelu%fmn2_pca_5000_1000_3/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
fmn2_pca_5000_1000_3/Relu�
,fmn2_pca_5000_1000_out/MatMul/ReadVariableOpReadVariableOp5fmn2_pca_5000_1000_out_matmul_readvariableop_resource*
_output_shapes
:	�*
dtype02.
,fmn2_pca_5000_1000_out/MatMul/ReadVariableOp�
fmn2_pca_5000_1000_out/MatMulMatMul'fmn2_pca_5000_1000_3/Relu:activations:04fmn2_pca_5000_1000_out/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
fmn2_pca_5000_1000_out/MatMul�
-fmn2_pca_5000_1000_out/BiasAdd/ReadVariableOpReadVariableOp6fmn2_pca_5000_1000_out_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02/
-fmn2_pca_5000_1000_out/BiasAdd/ReadVariableOp�
fmn2_pca_5000_1000_out/BiasAddBiasAdd'fmn2_pca_5000_1000_out/MatMul:product:05fmn2_pca_5000_1000_out/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2 
fmn2_pca_5000_1000_out/BiasAdd�
fmn2_pca_5000_1000_out/SigmoidSigmoid'fmn2_pca_5000_1000_out/BiasAdd:output:0*
T0*'
_output_shapes
:���������2 
fmn2_pca_5000_1000_out/Sigmoid�
-fmn2_tsvd_5000_1000_out/MatMul/ReadVariableOpReadVariableOp6fmn2_tsvd_5000_1000_out_matmul_readvariableop_resource*
_output_shapes
:	�*
dtype02/
-fmn2_tsvd_5000_1000_out/MatMul/ReadVariableOp�
fmn2_tsvd_5000_1000_out/MatMulMatMul(fmn2_tsvd_5000_1000_3/Relu:activations:05fmn2_tsvd_5000_1000_out/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2 
fmn2_tsvd_5000_1000_out/MatMul�
.fmn2_tsvd_5000_1000_out/BiasAdd/ReadVariableOpReadVariableOp7fmn2_tsvd_5000_1000_out_biasadd_readvariableop_resource*
_output_shapes
:*
dtype020
.fmn2_tsvd_5000_1000_out/BiasAdd/ReadVariableOp�
fmn2_tsvd_5000_1000_out/BiasAddBiasAdd(fmn2_tsvd_5000_1000_out/MatMul:product:06fmn2_tsvd_5000_1000_out/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2!
fmn2_tsvd_5000_1000_out/BiasAdd�
fmn2_tsvd_5000_1000_out/SigmoidSigmoid(fmn2_tsvd_5000_1000_out/BiasAdd:output:0*
T0*'
_output_shapes
:���������2!
fmn2_tsvd_5000_1000_out/Sigmoidz
concatenate_46/concat/axisConst*
_output_shapes
: *
dtype0*
value	B :2
concatenate_46/concat/axis�
concatenate_46/concatConcatV2"fmn2_pca_5000_1000_out/Sigmoid:y:0#fmn2_tsvd_5000_1000_out/Sigmoid:y:0#concatenate_46/concat/axis:output:0*
N*
T0*'
_output_shapes
:���������2
concatenate_46/concat�
joined/MatMul/ReadVariableOpReadVariableOp%joined_matmul_readvariableop_resource*
_output_shapes

:*
dtype02
joined/MatMul/ReadVariableOp�
joined/MatMulMatMulconcatenate_46/concat:output:0$joined/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
joined/MatMul�
joined/BiasAdd/ReadVariableOpReadVariableOp&joined_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02
joined/BiasAdd/ReadVariableOp�
joined/BiasAddBiasAddjoined/MatMul:product:0%joined/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
joined/BiasAddv
joined/SigmoidSigmoidjoined/BiasAdd:output:0*
T0*'
_output_shapes
:���������2
joined/Sigmoidm
IdentityIdentityjoined/Sigmoid:y:0^NoOp*
T0*'
_output_shapes
:���������2

Identity�
NoOpNoOp,^fmn2_pca_5000_1000_1/BiasAdd/ReadVariableOp+^fmn2_pca_5000_1000_1/MatMul/ReadVariableOp,^fmn2_pca_5000_1000_2/BiasAdd/ReadVariableOp+^fmn2_pca_5000_1000_2/MatMul/ReadVariableOp,^fmn2_pca_5000_1000_3/BiasAdd/ReadVariableOp+^fmn2_pca_5000_1000_3/MatMul/ReadVariableOp.^fmn2_pca_5000_1000_out/BiasAdd/ReadVariableOp-^fmn2_pca_5000_1000_out/MatMul/ReadVariableOp-^fmn2_tsvd_5000_1000_1/BiasAdd/ReadVariableOp,^fmn2_tsvd_5000_1000_1/MatMul/ReadVariableOp-^fmn2_tsvd_5000_1000_2/BiasAdd/ReadVariableOp,^fmn2_tsvd_5000_1000_2/MatMul/ReadVariableOp-^fmn2_tsvd_5000_1000_3/BiasAdd/ReadVariableOp,^fmn2_tsvd_5000_1000_3/MatMul/ReadVariableOp/^fmn2_tsvd_5000_1000_out/BiasAdd/ReadVariableOp.^fmn2_tsvd_5000_1000_out/MatMul/ReadVariableOp^joined/BiasAdd/ReadVariableOp^joined/MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*_
_input_shapesN
L:����������:����������: : : : : : : : : : : : : : : : : : 2Z
+fmn2_pca_5000_1000_1/BiasAdd/ReadVariableOp+fmn2_pca_5000_1000_1/BiasAdd/ReadVariableOp2X
*fmn2_pca_5000_1000_1/MatMul/ReadVariableOp*fmn2_pca_5000_1000_1/MatMul/ReadVariableOp2Z
+fmn2_pca_5000_1000_2/BiasAdd/ReadVariableOp+fmn2_pca_5000_1000_2/BiasAdd/ReadVariableOp2X
*fmn2_pca_5000_1000_2/MatMul/ReadVariableOp*fmn2_pca_5000_1000_2/MatMul/ReadVariableOp2Z
+fmn2_pca_5000_1000_3/BiasAdd/ReadVariableOp+fmn2_pca_5000_1000_3/BiasAdd/ReadVariableOp2X
*fmn2_pca_5000_1000_3/MatMul/ReadVariableOp*fmn2_pca_5000_1000_3/MatMul/ReadVariableOp2^
-fmn2_pca_5000_1000_out/BiasAdd/ReadVariableOp-fmn2_pca_5000_1000_out/BiasAdd/ReadVariableOp2\
,fmn2_pca_5000_1000_out/MatMul/ReadVariableOp,fmn2_pca_5000_1000_out/MatMul/ReadVariableOp2\
,fmn2_tsvd_5000_1000_1/BiasAdd/ReadVariableOp,fmn2_tsvd_5000_1000_1/BiasAdd/ReadVariableOp2Z
+fmn2_tsvd_5000_1000_1/MatMul/ReadVariableOp+fmn2_tsvd_5000_1000_1/MatMul/ReadVariableOp2\
,fmn2_tsvd_5000_1000_2/BiasAdd/ReadVariableOp,fmn2_tsvd_5000_1000_2/BiasAdd/ReadVariableOp2Z
+fmn2_tsvd_5000_1000_2/MatMul/ReadVariableOp+fmn2_tsvd_5000_1000_2/MatMul/ReadVariableOp2\
,fmn2_tsvd_5000_1000_3/BiasAdd/ReadVariableOp,fmn2_tsvd_5000_1000_3/BiasAdd/ReadVariableOp2Z
+fmn2_tsvd_5000_1000_3/MatMul/ReadVariableOp+fmn2_tsvd_5000_1000_3/MatMul/ReadVariableOp2`
.fmn2_tsvd_5000_1000_out/BiasAdd/ReadVariableOp.fmn2_tsvd_5000_1000_out/BiasAdd/ReadVariableOp2^
-fmn2_tsvd_5000_1000_out/MatMul/ReadVariableOp-fmn2_tsvd_5000_1000_out/MatMul/ReadVariableOp2>
joined/BiasAdd/ReadVariableOpjoined/BiasAdd/ReadVariableOp2<
joined/MatMul/ReadVariableOpjoined/MatMul/ReadVariableOp:R N
(
_output_shapes
:����������
"
_user_specified_name
inputs/0:RN
(
_output_shapes
:����������
"
_user_specified_name
inputs/1
�
�
'__inference_model_layer_call_fn_1539149
fmn2_pca_5000_1000_input
fmn2_tsvd_5000_1000_input
unknown:
��
	unknown_0:	�
	unknown_1:
��
	unknown_2:	�
	unknown_3:
��
	unknown_4:	�
	unknown_5:
��
	unknown_6:	�
	unknown_7:
��
	unknown_8:	�
	unknown_9:
��

unknown_10:	�

unknown_11:	�

unknown_12:

unknown_13:	�

unknown_14:

unknown_15:

unknown_16:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallfmn2_pca_5000_1000_inputfmn2_tsvd_5000_1000_inputunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10
unknown_11
unknown_12
unknown_13
unknown_14
unknown_15
unknown_16*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*4
_read_only_resource_inputs
	
*0
config_proto 

CPU

GPU2*0J 8� *K
fFRD
B__inference_model_layer_call_and_return_conditional_losses_15390682
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*_
_input_shapesN
L:����������:����������: : : : : : : : : : : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:b ^
(
_output_shapes
:����������
2
_user_specified_namefmn2_pca_5000_1000_input:c_
(
_output_shapes
:����������
3
_user_specified_namefmn2_tsvd_5000_1000_input
�@
�

B__inference_model_layer_call_and_return_conditional_losses_1539068

inputs
inputs_11
fmn2_tsvd_5000_1000_1_1539021:
��,
fmn2_tsvd_5000_1000_1_1539023:	�0
fmn2_pca_5000_1000_1_1539026:
��+
fmn2_pca_5000_1000_1_1539028:	�1
fmn2_tsvd_5000_1000_2_1539031:
��,
fmn2_tsvd_5000_1000_2_1539033:	�0
fmn2_pca_5000_1000_2_1539036:
��+
fmn2_pca_5000_1000_2_1539038:	�1
fmn2_tsvd_5000_1000_3_1539041:
��,
fmn2_tsvd_5000_1000_3_1539043:	�0
fmn2_pca_5000_1000_3_1539046:
��+
fmn2_pca_5000_1000_3_1539048:	�1
fmn2_pca_5000_1000_out_1539051:	�,
fmn2_pca_5000_1000_out_1539053:2
fmn2_tsvd_5000_1000_out_1539056:	�-
fmn2_tsvd_5000_1000_out_1539058: 
joined_1539062:
joined_1539064:
identity��,fmn2_pca_5000_1000_1/StatefulPartitionedCall�,fmn2_pca_5000_1000_2/StatefulPartitionedCall�,fmn2_pca_5000_1000_3/StatefulPartitionedCall�.fmn2_pca_5000_1000_out/StatefulPartitionedCall�-fmn2_tsvd_5000_1000_1/StatefulPartitionedCall�-fmn2_tsvd_5000_1000_2/StatefulPartitionedCall�-fmn2_tsvd_5000_1000_3/StatefulPartitionedCall�/fmn2_tsvd_5000_1000_out/StatefulPartitionedCall�joined/StatefulPartitionedCall�
-fmn2_tsvd_5000_1000_1/StatefulPartitionedCallStatefulPartitionedCallinputs_1fmn2_tsvd_5000_1000_1_1539021fmn2_tsvd_5000_1000_1_1539023*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_fmn2_tsvd_5000_1000_1_layer_call_and_return_conditional_losses_15386842/
-fmn2_tsvd_5000_1000_1/StatefulPartitionedCall�
,fmn2_pca_5000_1000_1/StatefulPartitionedCallStatefulPartitionedCallinputsfmn2_pca_5000_1000_1_1539026fmn2_pca_5000_1000_1_1539028*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_fmn2_pca_5000_1000_1_layer_call_and_return_conditional_losses_15387012.
,fmn2_pca_5000_1000_1/StatefulPartitionedCall�
-fmn2_tsvd_5000_1000_2/StatefulPartitionedCallStatefulPartitionedCall6fmn2_tsvd_5000_1000_1/StatefulPartitionedCall:output:0fmn2_tsvd_5000_1000_2_1539031fmn2_tsvd_5000_1000_2_1539033*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_fmn2_tsvd_5000_1000_2_layer_call_and_return_conditional_losses_15387182/
-fmn2_tsvd_5000_1000_2/StatefulPartitionedCall�
,fmn2_pca_5000_1000_2/StatefulPartitionedCallStatefulPartitionedCall5fmn2_pca_5000_1000_1/StatefulPartitionedCall:output:0fmn2_pca_5000_1000_2_1539036fmn2_pca_5000_1000_2_1539038*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_fmn2_pca_5000_1000_2_layer_call_and_return_conditional_losses_15387352.
,fmn2_pca_5000_1000_2/StatefulPartitionedCall�
-fmn2_tsvd_5000_1000_3/StatefulPartitionedCallStatefulPartitionedCall6fmn2_tsvd_5000_1000_2/StatefulPartitionedCall:output:0fmn2_tsvd_5000_1000_3_1539041fmn2_tsvd_5000_1000_3_1539043*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_fmn2_tsvd_5000_1000_3_layer_call_and_return_conditional_losses_15387522/
-fmn2_tsvd_5000_1000_3/StatefulPartitionedCall�
,fmn2_pca_5000_1000_3/StatefulPartitionedCallStatefulPartitionedCall5fmn2_pca_5000_1000_2/StatefulPartitionedCall:output:0fmn2_pca_5000_1000_3_1539046fmn2_pca_5000_1000_3_1539048*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_fmn2_pca_5000_1000_3_layer_call_and_return_conditional_losses_15387692.
,fmn2_pca_5000_1000_3/StatefulPartitionedCall�
.fmn2_pca_5000_1000_out/StatefulPartitionedCallStatefulPartitionedCall5fmn2_pca_5000_1000_3/StatefulPartitionedCall:output:0fmn2_pca_5000_1000_out_1539051fmn2_pca_5000_1000_out_1539053*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *\
fWRU
S__inference_fmn2_pca_5000_1000_out_layer_call_and_return_conditional_losses_153878620
.fmn2_pca_5000_1000_out/StatefulPartitionedCall�
/fmn2_tsvd_5000_1000_out/StatefulPartitionedCallStatefulPartitionedCall6fmn2_tsvd_5000_1000_3/StatefulPartitionedCall:output:0fmn2_tsvd_5000_1000_out_1539056fmn2_tsvd_5000_1000_out_1539058*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *]
fXRV
T__inference_fmn2_tsvd_5000_1000_out_layer_call_and_return_conditional_losses_153880321
/fmn2_tsvd_5000_1000_out/StatefulPartitionedCall�
concatenate_46/PartitionedCallPartitionedCall7fmn2_pca_5000_1000_out/StatefulPartitionedCall:output:08fmn2_tsvd_5000_1000_out/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *T
fORM
K__inference_concatenate_46_layer_call_and_return_conditional_losses_15388162 
concatenate_46/PartitionedCall�
joined/StatefulPartitionedCallStatefulPartitionedCall'concatenate_46/PartitionedCall:output:0joined_1539062joined_1539064*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *L
fGRE
C__inference_joined_layer_call_and_return_conditional_losses_15388292 
joined/StatefulPartitionedCall�
IdentityIdentity'joined/StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identity�
NoOpNoOp-^fmn2_pca_5000_1000_1/StatefulPartitionedCall-^fmn2_pca_5000_1000_2/StatefulPartitionedCall-^fmn2_pca_5000_1000_3/StatefulPartitionedCall/^fmn2_pca_5000_1000_out/StatefulPartitionedCall.^fmn2_tsvd_5000_1000_1/StatefulPartitionedCall.^fmn2_tsvd_5000_1000_2/StatefulPartitionedCall.^fmn2_tsvd_5000_1000_3/StatefulPartitionedCall0^fmn2_tsvd_5000_1000_out/StatefulPartitionedCall^joined/StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*_
_input_shapesN
L:����������:����������: : : : : : : : : : : : : : : : : : 2\
,fmn2_pca_5000_1000_1/StatefulPartitionedCall,fmn2_pca_5000_1000_1/StatefulPartitionedCall2\
,fmn2_pca_5000_1000_2/StatefulPartitionedCall,fmn2_pca_5000_1000_2/StatefulPartitionedCall2\
,fmn2_pca_5000_1000_3/StatefulPartitionedCall,fmn2_pca_5000_1000_3/StatefulPartitionedCall2`
.fmn2_pca_5000_1000_out/StatefulPartitionedCall.fmn2_pca_5000_1000_out/StatefulPartitionedCall2^
-fmn2_tsvd_5000_1000_1/StatefulPartitionedCall-fmn2_tsvd_5000_1000_1/StatefulPartitionedCall2^
-fmn2_tsvd_5000_1000_2/StatefulPartitionedCall-fmn2_tsvd_5000_1000_2/StatefulPartitionedCall2^
-fmn2_tsvd_5000_1000_3/StatefulPartitionedCall-fmn2_tsvd_5000_1000_3/StatefulPartitionedCall2b
/fmn2_tsvd_5000_1000_out/StatefulPartitionedCall/fmn2_tsvd_5000_1000_out/StatefulPartitionedCall2@
joined/StatefulPartitionedCalljoined/StatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs:PL
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
C__inference_joined_layer_call_and_return_conditional_losses_1538829

inputs0
matmul_readvariableop_resource:-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2	
BiasAdda
SigmoidSigmoidBiasAdd:output:0*
T0*'
_output_shapes
:���������2	
Sigmoidf
IdentityIdentitySigmoid:y:0^NoOp*
T0*'
_output_shapes
:���������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs
�
�
Q__inference_fmn2_pca_5000_1000_3_layer_call_and_return_conditional_losses_1538769

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
Q__inference_fmn2_pca_5000_1000_2_layer_call_and_return_conditional_losses_1539585

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
S__inference_fmn2_pca_5000_1000_out_layer_call_and_return_conditional_losses_1538786

inputs1
matmul_readvariableop_resource:	�-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes
:	�*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2	
BiasAdda
SigmoidSigmoidBiasAdd:output:0*
T0*'
_output_shapes
:���������2	
Sigmoidf
IdentityIdentitySigmoid:y:0^NoOp*
T0*'
_output_shapes
:���������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�p
�
B__inference_model_layer_call_and_return_conditional_losses_1539525
inputs_0
inputs_1H
4fmn2_tsvd_5000_1000_1_matmul_readvariableop_resource:
��D
5fmn2_tsvd_5000_1000_1_biasadd_readvariableop_resource:	�G
3fmn2_pca_5000_1000_1_matmul_readvariableop_resource:
��C
4fmn2_pca_5000_1000_1_biasadd_readvariableop_resource:	�H
4fmn2_tsvd_5000_1000_2_matmul_readvariableop_resource:
��D
5fmn2_tsvd_5000_1000_2_biasadd_readvariableop_resource:	�G
3fmn2_pca_5000_1000_2_matmul_readvariableop_resource:
��C
4fmn2_pca_5000_1000_2_biasadd_readvariableop_resource:	�H
4fmn2_tsvd_5000_1000_3_matmul_readvariableop_resource:
��D
5fmn2_tsvd_5000_1000_3_biasadd_readvariableop_resource:	�G
3fmn2_pca_5000_1000_3_matmul_readvariableop_resource:
��C
4fmn2_pca_5000_1000_3_biasadd_readvariableop_resource:	�H
5fmn2_pca_5000_1000_out_matmul_readvariableop_resource:	�D
6fmn2_pca_5000_1000_out_biasadd_readvariableop_resource:I
6fmn2_tsvd_5000_1000_out_matmul_readvariableop_resource:	�E
7fmn2_tsvd_5000_1000_out_biasadd_readvariableop_resource:7
%joined_matmul_readvariableop_resource:4
&joined_biasadd_readvariableop_resource:
identity��+fmn2_pca_5000_1000_1/BiasAdd/ReadVariableOp�*fmn2_pca_5000_1000_1/MatMul/ReadVariableOp�+fmn2_pca_5000_1000_2/BiasAdd/ReadVariableOp�*fmn2_pca_5000_1000_2/MatMul/ReadVariableOp�+fmn2_pca_5000_1000_3/BiasAdd/ReadVariableOp�*fmn2_pca_5000_1000_3/MatMul/ReadVariableOp�-fmn2_pca_5000_1000_out/BiasAdd/ReadVariableOp�,fmn2_pca_5000_1000_out/MatMul/ReadVariableOp�,fmn2_tsvd_5000_1000_1/BiasAdd/ReadVariableOp�+fmn2_tsvd_5000_1000_1/MatMul/ReadVariableOp�,fmn2_tsvd_5000_1000_2/BiasAdd/ReadVariableOp�+fmn2_tsvd_5000_1000_2/MatMul/ReadVariableOp�,fmn2_tsvd_5000_1000_3/BiasAdd/ReadVariableOp�+fmn2_tsvd_5000_1000_3/MatMul/ReadVariableOp�.fmn2_tsvd_5000_1000_out/BiasAdd/ReadVariableOp�-fmn2_tsvd_5000_1000_out/MatMul/ReadVariableOp�joined/BiasAdd/ReadVariableOp�joined/MatMul/ReadVariableOp�
+fmn2_tsvd_5000_1000_1/MatMul/ReadVariableOpReadVariableOp4fmn2_tsvd_5000_1000_1_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02-
+fmn2_tsvd_5000_1000_1/MatMul/ReadVariableOp�
fmn2_tsvd_5000_1000_1/MatMulMatMulinputs_13fmn2_tsvd_5000_1000_1/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn2_tsvd_5000_1000_1/MatMul�
,fmn2_tsvd_5000_1000_1/BiasAdd/ReadVariableOpReadVariableOp5fmn2_tsvd_5000_1000_1_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02.
,fmn2_tsvd_5000_1000_1/BiasAdd/ReadVariableOp�
fmn2_tsvd_5000_1000_1/BiasAddBiasAdd&fmn2_tsvd_5000_1000_1/MatMul:product:04fmn2_tsvd_5000_1000_1/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn2_tsvd_5000_1000_1/BiasAdd�
fmn2_tsvd_5000_1000_1/ReluRelu&fmn2_tsvd_5000_1000_1/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
fmn2_tsvd_5000_1000_1/Relu�
*fmn2_pca_5000_1000_1/MatMul/ReadVariableOpReadVariableOp3fmn2_pca_5000_1000_1_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02,
*fmn2_pca_5000_1000_1/MatMul/ReadVariableOp�
fmn2_pca_5000_1000_1/MatMulMatMulinputs_02fmn2_pca_5000_1000_1/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn2_pca_5000_1000_1/MatMul�
+fmn2_pca_5000_1000_1/BiasAdd/ReadVariableOpReadVariableOp4fmn2_pca_5000_1000_1_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02-
+fmn2_pca_5000_1000_1/BiasAdd/ReadVariableOp�
fmn2_pca_5000_1000_1/BiasAddBiasAdd%fmn2_pca_5000_1000_1/MatMul:product:03fmn2_pca_5000_1000_1/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn2_pca_5000_1000_1/BiasAdd�
fmn2_pca_5000_1000_1/ReluRelu%fmn2_pca_5000_1000_1/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
fmn2_pca_5000_1000_1/Relu�
+fmn2_tsvd_5000_1000_2/MatMul/ReadVariableOpReadVariableOp4fmn2_tsvd_5000_1000_2_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02-
+fmn2_tsvd_5000_1000_2/MatMul/ReadVariableOp�
fmn2_tsvd_5000_1000_2/MatMulMatMul(fmn2_tsvd_5000_1000_1/Relu:activations:03fmn2_tsvd_5000_1000_2/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn2_tsvd_5000_1000_2/MatMul�
,fmn2_tsvd_5000_1000_2/BiasAdd/ReadVariableOpReadVariableOp5fmn2_tsvd_5000_1000_2_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02.
,fmn2_tsvd_5000_1000_2/BiasAdd/ReadVariableOp�
fmn2_tsvd_5000_1000_2/BiasAddBiasAdd&fmn2_tsvd_5000_1000_2/MatMul:product:04fmn2_tsvd_5000_1000_2/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn2_tsvd_5000_1000_2/BiasAdd�
fmn2_tsvd_5000_1000_2/ReluRelu&fmn2_tsvd_5000_1000_2/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
fmn2_tsvd_5000_1000_2/Relu�
*fmn2_pca_5000_1000_2/MatMul/ReadVariableOpReadVariableOp3fmn2_pca_5000_1000_2_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02,
*fmn2_pca_5000_1000_2/MatMul/ReadVariableOp�
fmn2_pca_5000_1000_2/MatMulMatMul'fmn2_pca_5000_1000_1/Relu:activations:02fmn2_pca_5000_1000_2/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn2_pca_5000_1000_2/MatMul�
+fmn2_pca_5000_1000_2/BiasAdd/ReadVariableOpReadVariableOp4fmn2_pca_5000_1000_2_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02-
+fmn2_pca_5000_1000_2/BiasAdd/ReadVariableOp�
fmn2_pca_5000_1000_2/BiasAddBiasAdd%fmn2_pca_5000_1000_2/MatMul:product:03fmn2_pca_5000_1000_2/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn2_pca_5000_1000_2/BiasAdd�
fmn2_pca_5000_1000_2/ReluRelu%fmn2_pca_5000_1000_2/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
fmn2_pca_5000_1000_2/Relu�
+fmn2_tsvd_5000_1000_3/MatMul/ReadVariableOpReadVariableOp4fmn2_tsvd_5000_1000_3_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02-
+fmn2_tsvd_5000_1000_3/MatMul/ReadVariableOp�
fmn2_tsvd_5000_1000_3/MatMulMatMul(fmn2_tsvd_5000_1000_2/Relu:activations:03fmn2_tsvd_5000_1000_3/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn2_tsvd_5000_1000_3/MatMul�
,fmn2_tsvd_5000_1000_3/BiasAdd/ReadVariableOpReadVariableOp5fmn2_tsvd_5000_1000_3_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02.
,fmn2_tsvd_5000_1000_3/BiasAdd/ReadVariableOp�
fmn2_tsvd_5000_1000_3/BiasAddBiasAdd&fmn2_tsvd_5000_1000_3/MatMul:product:04fmn2_tsvd_5000_1000_3/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn2_tsvd_5000_1000_3/BiasAdd�
fmn2_tsvd_5000_1000_3/ReluRelu&fmn2_tsvd_5000_1000_3/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
fmn2_tsvd_5000_1000_3/Relu�
*fmn2_pca_5000_1000_3/MatMul/ReadVariableOpReadVariableOp3fmn2_pca_5000_1000_3_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02,
*fmn2_pca_5000_1000_3/MatMul/ReadVariableOp�
fmn2_pca_5000_1000_3/MatMulMatMul'fmn2_pca_5000_1000_2/Relu:activations:02fmn2_pca_5000_1000_3/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn2_pca_5000_1000_3/MatMul�
+fmn2_pca_5000_1000_3/BiasAdd/ReadVariableOpReadVariableOp4fmn2_pca_5000_1000_3_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02-
+fmn2_pca_5000_1000_3/BiasAdd/ReadVariableOp�
fmn2_pca_5000_1000_3/BiasAddBiasAdd%fmn2_pca_5000_1000_3/MatMul:product:03fmn2_pca_5000_1000_3/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn2_pca_5000_1000_3/BiasAdd�
fmn2_pca_5000_1000_3/ReluRelu%fmn2_pca_5000_1000_3/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
fmn2_pca_5000_1000_3/Relu�
,fmn2_pca_5000_1000_out/MatMul/ReadVariableOpReadVariableOp5fmn2_pca_5000_1000_out_matmul_readvariableop_resource*
_output_shapes
:	�*
dtype02.
,fmn2_pca_5000_1000_out/MatMul/ReadVariableOp�
fmn2_pca_5000_1000_out/MatMulMatMul'fmn2_pca_5000_1000_3/Relu:activations:04fmn2_pca_5000_1000_out/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
fmn2_pca_5000_1000_out/MatMul�
-fmn2_pca_5000_1000_out/BiasAdd/ReadVariableOpReadVariableOp6fmn2_pca_5000_1000_out_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02/
-fmn2_pca_5000_1000_out/BiasAdd/ReadVariableOp�
fmn2_pca_5000_1000_out/BiasAddBiasAdd'fmn2_pca_5000_1000_out/MatMul:product:05fmn2_pca_5000_1000_out/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2 
fmn2_pca_5000_1000_out/BiasAdd�
fmn2_pca_5000_1000_out/SigmoidSigmoid'fmn2_pca_5000_1000_out/BiasAdd:output:0*
T0*'
_output_shapes
:���������2 
fmn2_pca_5000_1000_out/Sigmoid�
-fmn2_tsvd_5000_1000_out/MatMul/ReadVariableOpReadVariableOp6fmn2_tsvd_5000_1000_out_matmul_readvariableop_resource*
_output_shapes
:	�*
dtype02/
-fmn2_tsvd_5000_1000_out/MatMul/ReadVariableOp�
fmn2_tsvd_5000_1000_out/MatMulMatMul(fmn2_tsvd_5000_1000_3/Relu:activations:05fmn2_tsvd_5000_1000_out/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2 
fmn2_tsvd_5000_1000_out/MatMul�
.fmn2_tsvd_5000_1000_out/BiasAdd/ReadVariableOpReadVariableOp7fmn2_tsvd_5000_1000_out_biasadd_readvariableop_resource*
_output_shapes
:*
dtype020
.fmn2_tsvd_5000_1000_out/BiasAdd/ReadVariableOp�
fmn2_tsvd_5000_1000_out/BiasAddBiasAdd(fmn2_tsvd_5000_1000_out/MatMul:product:06fmn2_tsvd_5000_1000_out/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2!
fmn2_tsvd_5000_1000_out/BiasAdd�
fmn2_tsvd_5000_1000_out/SigmoidSigmoid(fmn2_tsvd_5000_1000_out/BiasAdd:output:0*
T0*'
_output_shapes
:���������2!
fmn2_tsvd_5000_1000_out/Sigmoidz
concatenate_46/concat/axisConst*
_output_shapes
: *
dtype0*
value	B :2
concatenate_46/concat/axis�
concatenate_46/concatConcatV2"fmn2_pca_5000_1000_out/Sigmoid:y:0#fmn2_tsvd_5000_1000_out/Sigmoid:y:0#concatenate_46/concat/axis:output:0*
N*
T0*'
_output_shapes
:���������2
concatenate_46/concat�
joined/MatMul/ReadVariableOpReadVariableOp%joined_matmul_readvariableop_resource*
_output_shapes

:*
dtype02
joined/MatMul/ReadVariableOp�
joined/MatMulMatMulconcatenate_46/concat:output:0$joined/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
joined/MatMul�
joined/BiasAdd/ReadVariableOpReadVariableOp&joined_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02
joined/BiasAdd/ReadVariableOp�
joined/BiasAddBiasAddjoined/MatMul:product:0%joined/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
joined/BiasAddv
joined/SigmoidSigmoidjoined/BiasAdd:output:0*
T0*'
_output_shapes
:���������2
joined/Sigmoidm
IdentityIdentityjoined/Sigmoid:y:0^NoOp*
T0*'
_output_shapes
:���������2

Identity�
NoOpNoOp,^fmn2_pca_5000_1000_1/BiasAdd/ReadVariableOp+^fmn2_pca_5000_1000_1/MatMul/ReadVariableOp,^fmn2_pca_5000_1000_2/BiasAdd/ReadVariableOp+^fmn2_pca_5000_1000_2/MatMul/ReadVariableOp,^fmn2_pca_5000_1000_3/BiasAdd/ReadVariableOp+^fmn2_pca_5000_1000_3/MatMul/ReadVariableOp.^fmn2_pca_5000_1000_out/BiasAdd/ReadVariableOp-^fmn2_pca_5000_1000_out/MatMul/ReadVariableOp-^fmn2_tsvd_5000_1000_1/BiasAdd/ReadVariableOp,^fmn2_tsvd_5000_1000_1/MatMul/ReadVariableOp-^fmn2_tsvd_5000_1000_2/BiasAdd/ReadVariableOp,^fmn2_tsvd_5000_1000_2/MatMul/ReadVariableOp-^fmn2_tsvd_5000_1000_3/BiasAdd/ReadVariableOp,^fmn2_tsvd_5000_1000_3/MatMul/ReadVariableOp/^fmn2_tsvd_5000_1000_out/BiasAdd/ReadVariableOp.^fmn2_tsvd_5000_1000_out/MatMul/ReadVariableOp^joined/BiasAdd/ReadVariableOp^joined/MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*_
_input_shapesN
L:����������:����������: : : : : : : : : : : : : : : : : : 2Z
+fmn2_pca_5000_1000_1/BiasAdd/ReadVariableOp+fmn2_pca_5000_1000_1/BiasAdd/ReadVariableOp2X
*fmn2_pca_5000_1000_1/MatMul/ReadVariableOp*fmn2_pca_5000_1000_1/MatMul/ReadVariableOp2Z
+fmn2_pca_5000_1000_2/BiasAdd/ReadVariableOp+fmn2_pca_5000_1000_2/BiasAdd/ReadVariableOp2X
*fmn2_pca_5000_1000_2/MatMul/ReadVariableOp*fmn2_pca_5000_1000_2/MatMul/ReadVariableOp2Z
+fmn2_pca_5000_1000_3/BiasAdd/ReadVariableOp+fmn2_pca_5000_1000_3/BiasAdd/ReadVariableOp2X
*fmn2_pca_5000_1000_3/MatMul/ReadVariableOp*fmn2_pca_5000_1000_3/MatMul/ReadVariableOp2^
-fmn2_pca_5000_1000_out/BiasAdd/ReadVariableOp-fmn2_pca_5000_1000_out/BiasAdd/ReadVariableOp2\
,fmn2_pca_5000_1000_out/MatMul/ReadVariableOp,fmn2_pca_5000_1000_out/MatMul/ReadVariableOp2\
,fmn2_tsvd_5000_1000_1/BiasAdd/ReadVariableOp,fmn2_tsvd_5000_1000_1/BiasAdd/ReadVariableOp2Z
+fmn2_tsvd_5000_1000_1/MatMul/ReadVariableOp+fmn2_tsvd_5000_1000_1/MatMul/ReadVariableOp2\
,fmn2_tsvd_5000_1000_2/BiasAdd/ReadVariableOp,fmn2_tsvd_5000_1000_2/BiasAdd/ReadVariableOp2Z
+fmn2_tsvd_5000_1000_2/MatMul/ReadVariableOp+fmn2_tsvd_5000_1000_2/MatMul/ReadVariableOp2\
,fmn2_tsvd_5000_1000_3/BiasAdd/ReadVariableOp,fmn2_tsvd_5000_1000_3/BiasAdd/ReadVariableOp2Z
+fmn2_tsvd_5000_1000_3/MatMul/ReadVariableOp+fmn2_tsvd_5000_1000_3/MatMul/ReadVariableOp2`
.fmn2_tsvd_5000_1000_out/BiasAdd/ReadVariableOp.fmn2_tsvd_5000_1000_out/BiasAdd/ReadVariableOp2^
-fmn2_tsvd_5000_1000_out/MatMul/ReadVariableOp-fmn2_tsvd_5000_1000_out/MatMul/ReadVariableOp2>
joined/BiasAdd/ReadVariableOpjoined/BiasAdd/ReadVariableOp2<
joined/MatMul/ReadVariableOpjoined/MatMul/ReadVariableOp:R N
(
_output_shapes
:����������
"
_user_specified_name
inputs/0:RN
(
_output_shapes
:����������
"
_user_specified_name
inputs/1
�
u
K__inference_concatenate_46_layer_call_and_return_conditional_losses_1538816

inputs
inputs_1
identity\
concat/axisConst*
_output_shapes
: *
dtype0*
value	B :2
concat/axis
concatConcatV2inputsinputs_1concat/axis:output:0*
N*
T0*'
_output_shapes
:���������2
concatc
IdentityIdentityconcat:output:0*
T0*'
_output_shapes
:���������2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*9
_input_shapes(
&:���������:���������:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs:OK
'
_output_shapes
:���������
 
_user_specified_nameinputs
�
�
'__inference_model_layer_call_fn_1539385
inputs_0
inputs_1
unknown:
��
	unknown_0:	�
	unknown_1:
��
	unknown_2:	�
	unknown_3:
��
	unknown_4:	�
	unknown_5:
��
	unknown_6:	�
	unknown_7:
��
	unknown_8:	�
	unknown_9:
��

unknown_10:	�

unknown_11:	�

unknown_12:

unknown_13:	�

unknown_14:

unknown_15:

unknown_16:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputs_0inputs_1unknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10
unknown_11
unknown_12
unknown_13
unknown_14
unknown_15
unknown_16*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*4
_read_only_resource_inputs
	
*0
config_proto 

CPU

GPU2*0J 8� *K
fFRD
B__inference_model_layer_call_and_return_conditional_losses_15390682
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*_
_input_shapesN
L:����������:����������: : : : : : : : : : : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:R N
(
_output_shapes
:����������
"
_user_specified_name
inputs/0:RN
(
_output_shapes
:����������
"
_user_specified_name
inputs/1
�
�
6__inference_fmn2_pca_5000_1000_3_layer_call_fn_1539614

inputs
unknown:
��
	unknown_0:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_fmn2_pca_5000_1000_3_layer_call_and_return_conditional_losses_15387692
StatefulPartitionedCall|
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:����������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
S__inference_fmn2_pca_5000_1000_out_layer_call_and_return_conditional_losses_1539665

inputs1
matmul_readvariableop_resource:	�-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes
:	�*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2	
BiasAdda
SigmoidSigmoidBiasAdd:output:0*
T0*'
_output_shapes
:���������2	
Sigmoidf
IdentityIdentitySigmoid:y:0^NoOp*
T0*'
_output_shapes
:���������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
7__inference_fmn2_tsvd_5000_1000_2_layer_call_fn_1539594

inputs
unknown:
��
	unknown_0:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_fmn2_tsvd_5000_1000_2_layer_call_and_return_conditional_losses_15387182
StatefulPartitionedCall|
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:����������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
9__inference_fmn2_tsvd_5000_1000_out_layer_call_fn_1539674

inputs
unknown:	�
	unknown_0:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *]
fXRV
T__inference_fmn2_tsvd_5000_1000_out_layer_call_and_return_conditional_losses_15388032
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�D
�
 __inference__traced_save_1539835
file_prefix:
6savev2_fmn2_pca_5000_1000_1_kernel_read_readvariableop8
4savev2_fmn2_pca_5000_1000_1_bias_read_readvariableop;
7savev2_fmn2_tsvd_5000_1000_1_kernel_read_readvariableop9
5savev2_fmn2_tsvd_5000_1000_1_bias_read_readvariableop:
6savev2_fmn2_pca_5000_1000_2_kernel_read_readvariableop8
4savev2_fmn2_pca_5000_1000_2_bias_read_readvariableop;
7savev2_fmn2_tsvd_5000_1000_2_kernel_read_readvariableop9
5savev2_fmn2_tsvd_5000_1000_2_bias_read_readvariableop:
6savev2_fmn2_pca_5000_1000_3_kernel_read_readvariableop8
4savev2_fmn2_pca_5000_1000_3_bias_read_readvariableop;
7savev2_fmn2_tsvd_5000_1000_3_kernel_read_readvariableop9
5savev2_fmn2_tsvd_5000_1000_3_bias_read_readvariableop<
8savev2_fmn2_pca_5000_1000_out_kernel_read_readvariableop:
6savev2_fmn2_pca_5000_1000_out_bias_read_readvariableop=
9savev2_fmn2_tsvd_5000_1000_out_kernel_read_readvariableop;
7savev2_fmn2_tsvd_5000_1000_out_bias_read_readvariableop,
(savev2_joined_kernel_read_readvariableop*
&savev2_joined_bias_read_readvariableop(
$savev2_adam_iter_read_readvariableop	*
&savev2_adam_beta_1_read_readvariableop*
&savev2_adam_beta_2_read_readvariableop)
%savev2_adam_decay_read_readvariableop1
-savev2_adam_learning_rate_read_readvariableop$
 savev2_total_read_readvariableop$
 savev2_count_read_readvariableop&
"savev2_total_1_read_readvariableop&
"savev2_count_1_read_readvariableop3
/savev2_adam_joined_kernel_m_read_readvariableop1
-savev2_adam_joined_bias_m_read_readvariableop3
/savev2_adam_joined_kernel_v_read_readvariableop1
-savev2_adam_joined_bias_v_read_readvariableop
savev2_const

identity_1��MergeV2Checkpoints�
StaticRegexFullMatchStaticRegexFullMatchfile_prefix"/device:CPU:**
_output_shapes
: *
pattern
^s3://.*2
StaticRegexFullMatchc
ConstConst"/device:CPU:**
_output_shapes
: *
dtype0*
valueB B.part2
Constl
Const_1Const"/device:CPU:**
_output_shapes
: *
dtype0*
valueB B
_temp/part2	
Const_1�
SelectSelectStaticRegexFullMatch:output:0Const:output:0Const_1:output:0"/device:CPU:**
T0*
_output_shapes
: 2
Selectt

StringJoin
StringJoinfile_prefixSelect:output:0"/device:CPU:**
N*
_output_shapes
: 2

StringJoinZ

num_shardsConst*
_output_shapes
: *
dtype0*
value	B :2

num_shards
ShardedFilename/shardConst"/device:CPU:0*
_output_shapes
: *
dtype0*
value	B : 2
ShardedFilename/shard�
ShardedFilenameShardedFilenameStringJoin:output:0ShardedFilename/shard:output:0num_shards:output:0"/device:CPU:0*
_output_shapes
: 2
ShardedFilename�
SaveV2/tensor_namesConst"/device:CPU:0*
_output_shapes
: *
dtype0*�
value�B� B6layer_with_weights-0/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-0/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-1/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-1/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-3/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-3/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-4/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-4/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-5/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-5/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-6/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-6/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-7/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-7/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-8/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-8/bias/.ATTRIBUTES/VARIABLE_VALUEB)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUEB*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/count/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-8/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-8/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-8/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-8/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEB_CHECKPOINTABLE_OBJECT_GRAPH2
SaveV2/tensor_names�
SaveV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
: *
dtype0*S
valueJBH B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B 2
SaveV2/shape_and_slices�
SaveV2SaveV2ShardedFilename:filename:0SaveV2/tensor_names:output:0 SaveV2/shape_and_slices:output:06savev2_fmn2_pca_5000_1000_1_kernel_read_readvariableop4savev2_fmn2_pca_5000_1000_1_bias_read_readvariableop7savev2_fmn2_tsvd_5000_1000_1_kernel_read_readvariableop5savev2_fmn2_tsvd_5000_1000_1_bias_read_readvariableop6savev2_fmn2_pca_5000_1000_2_kernel_read_readvariableop4savev2_fmn2_pca_5000_1000_2_bias_read_readvariableop7savev2_fmn2_tsvd_5000_1000_2_kernel_read_readvariableop5savev2_fmn2_tsvd_5000_1000_2_bias_read_readvariableop6savev2_fmn2_pca_5000_1000_3_kernel_read_readvariableop4savev2_fmn2_pca_5000_1000_3_bias_read_readvariableop7savev2_fmn2_tsvd_5000_1000_3_kernel_read_readvariableop5savev2_fmn2_tsvd_5000_1000_3_bias_read_readvariableop8savev2_fmn2_pca_5000_1000_out_kernel_read_readvariableop6savev2_fmn2_pca_5000_1000_out_bias_read_readvariableop9savev2_fmn2_tsvd_5000_1000_out_kernel_read_readvariableop7savev2_fmn2_tsvd_5000_1000_out_bias_read_readvariableop(savev2_joined_kernel_read_readvariableop&savev2_joined_bias_read_readvariableop$savev2_adam_iter_read_readvariableop&savev2_adam_beta_1_read_readvariableop&savev2_adam_beta_2_read_readvariableop%savev2_adam_decay_read_readvariableop-savev2_adam_learning_rate_read_readvariableop savev2_total_read_readvariableop savev2_count_read_readvariableop"savev2_total_1_read_readvariableop"savev2_count_1_read_readvariableop/savev2_adam_joined_kernel_m_read_readvariableop-savev2_adam_joined_bias_m_read_readvariableop/savev2_adam_joined_kernel_v_read_readvariableop-savev2_adam_joined_bias_v_read_readvariableopsavev2_const"/device:CPU:0*
_output_shapes
 *.
dtypes$
"2 	2
SaveV2�
&MergeV2Checkpoints/checkpoint_prefixesPackShardedFilename:filename:0^SaveV2"/device:CPU:0*
N*
T0*
_output_shapes
:2(
&MergeV2Checkpoints/checkpoint_prefixes�
MergeV2CheckpointsMergeV2Checkpoints/MergeV2Checkpoints/checkpoint_prefixes:output:0file_prefix"/device:CPU:0*
_output_shapes
 2
MergeV2Checkpointsr
IdentityIdentityfile_prefix^MergeV2Checkpoints"/device:CPU:0*
T0*
_output_shapes
: 2

Identity_

Identity_1IdentityIdentity:output:0^NoOp*
T0*
_output_shapes
: 2

Identity_1c
NoOpNoOp^MergeV2Checkpoints*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"!

identity_1Identity_1:output:0*�
_input_shapes�
�: :
��:�:
��:�:
��:�:
��:�:
��:�:
��:�:	�::	�:::: : : : : : : : : ::::: 2(
MergeV2CheckpointsMergeV2Checkpoints:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix:&"
 
_output_shapes
:
��:!

_output_shapes	
:�:&"
 
_output_shapes
:
��:!

_output_shapes	
:�:&"
 
_output_shapes
:
��:!

_output_shapes	
:�:&"
 
_output_shapes
:
��:!

_output_shapes	
:�:&	"
 
_output_shapes
:
��:!


_output_shapes	
:�:&"
 
_output_shapes
:
��:!

_output_shapes	
:�:%!

_output_shapes
:	�: 

_output_shapes
::%!

_output_shapes
:	�: 

_output_shapes
::$ 

_output_shapes

:: 

_output_shapes
::

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :$ 

_output_shapes

:: 

_output_shapes
::$ 

_output_shapes

:: 

_output_shapes
:: 

_output_shapes
: 
�
�
T__inference_fmn2_tsvd_5000_1000_out_layer_call_and_return_conditional_losses_1538803

inputs1
matmul_readvariableop_resource:	�-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes
:	�*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2	
BiasAdda
SigmoidSigmoidBiasAdd:output:0*
T0*'
_output_shapes
:���������2	
Sigmoidf
IdentityIdentitySigmoid:y:0^NoOp*
T0*'
_output_shapes
:���������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
R__inference_fmn2_tsvd_5000_1000_3_layer_call_and_return_conditional_losses_1539645

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
T__inference_fmn2_tsvd_5000_1000_out_layer_call_and_return_conditional_losses_1539685

inputs1
matmul_readvariableop_resource:	�-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes
:	�*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2	
BiasAdda
SigmoidSigmoidBiasAdd:output:0*
T0*'
_output_shapes
:���������2	
Sigmoidf
IdentityIdentitySigmoid:y:0^NoOp*
T0*'
_output_shapes
:���������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
Q__inference_fmn2_pca_5000_1000_1_layer_call_and_return_conditional_losses_1539545

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
7__inference_fmn2_tsvd_5000_1000_1_layer_call_fn_1539554

inputs
unknown:
��
	unknown_0:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_fmn2_tsvd_5000_1000_1_layer_call_and_return_conditional_losses_15386842
StatefulPartitionedCall|
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:����������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
Q__inference_fmn2_pca_5000_1000_3_layer_call_and_return_conditional_losses_1539625

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
\
0__inference_concatenate_46_layer_call_fn_1539691
inputs_0
inputs_1
identity�
PartitionedCallPartitionedCallinputs_0inputs_1*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *T
fORM
K__inference_concatenate_46_layer_call_and_return_conditional_losses_15388162
PartitionedCalll
IdentityIdentityPartitionedCall:output:0*
T0*'
_output_shapes
:���������2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*9
_input_shapes(
&:���������:���������:Q M
'
_output_shapes
:���������
"
_user_specified_name
inputs/0:QM
'
_output_shapes
:���������
"
_user_specified_name
inputs/1
�
�
'__inference_model_layer_call_fn_1539343
inputs_0
inputs_1
unknown:
��
	unknown_0:	�
	unknown_1:
��
	unknown_2:	�
	unknown_3:
��
	unknown_4:	�
	unknown_5:
��
	unknown_6:	�
	unknown_7:
��
	unknown_8:	�
	unknown_9:
��

unknown_10:	�

unknown_11:	�

unknown_12:

unknown_13:	�

unknown_14:

unknown_15:

unknown_16:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputs_0inputs_1unknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10
unknown_11
unknown_12
unknown_13
unknown_14
unknown_15
unknown_16*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*4
_read_only_resource_inputs
	
*0
config_proto 

CPU

GPU2*0J 8� *K
fFRD
B__inference_model_layer_call_and_return_conditional_losses_15388362
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*_
_input_shapesN
L:����������:����������: : : : : : : : : : : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:R N
(
_output_shapes
:����������
"
_user_specified_name
inputs/0:RN
(
_output_shapes
:����������
"
_user_specified_name
inputs/1
�@
�

B__inference_model_layer_call_and_return_conditional_losses_1539200
fmn2_pca_5000_1000_input
fmn2_tsvd_5000_1000_input1
fmn2_tsvd_5000_1000_1_1539153:
��,
fmn2_tsvd_5000_1000_1_1539155:	�0
fmn2_pca_5000_1000_1_1539158:
��+
fmn2_pca_5000_1000_1_1539160:	�1
fmn2_tsvd_5000_1000_2_1539163:
��,
fmn2_tsvd_5000_1000_2_1539165:	�0
fmn2_pca_5000_1000_2_1539168:
��+
fmn2_pca_5000_1000_2_1539170:	�1
fmn2_tsvd_5000_1000_3_1539173:
��,
fmn2_tsvd_5000_1000_3_1539175:	�0
fmn2_pca_5000_1000_3_1539178:
��+
fmn2_pca_5000_1000_3_1539180:	�1
fmn2_pca_5000_1000_out_1539183:	�,
fmn2_pca_5000_1000_out_1539185:2
fmn2_tsvd_5000_1000_out_1539188:	�-
fmn2_tsvd_5000_1000_out_1539190: 
joined_1539194:
joined_1539196:
identity��,fmn2_pca_5000_1000_1/StatefulPartitionedCall�,fmn2_pca_5000_1000_2/StatefulPartitionedCall�,fmn2_pca_5000_1000_3/StatefulPartitionedCall�.fmn2_pca_5000_1000_out/StatefulPartitionedCall�-fmn2_tsvd_5000_1000_1/StatefulPartitionedCall�-fmn2_tsvd_5000_1000_2/StatefulPartitionedCall�-fmn2_tsvd_5000_1000_3/StatefulPartitionedCall�/fmn2_tsvd_5000_1000_out/StatefulPartitionedCall�joined/StatefulPartitionedCall�
-fmn2_tsvd_5000_1000_1/StatefulPartitionedCallStatefulPartitionedCallfmn2_tsvd_5000_1000_inputfmn2_tsvd_5000_1000_1_1539153fmn2_tsvd_5000_1000_1_1539155*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_fmn2_tsvd_5000_1000_1_layer_call_and_return_conditional_losses_15386842/
-fmn2_tsvd_5000_1000_1/StatefulPartitionedCall�
,fmn2_pca_5000_1000_1/StatefulPartitionedCallStatefulPartitionedCallfmn2_pca_5000_1000_inputfmn2_pca_5000_1000_1_1539158fmn2_pca_5000_1000_1_1539160*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_fmn2_pca_5000_1000_1_layer_call_and_return_conditional_losses_15387012.
,fmn2_pca_5000_1000_1/StatefulPartitionedCall�
-fmn2_tsvd_5000_1000_2/StatefulPartitionedCallStatefulPartitionedCall6fmn2_tsvd_5000_1000_1/StatefulPartitionedCall:output:0fmn2_tsvd_5000_1000_2_1539163fmn2_tsvd_5000_1000_2_1539165*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_fmn2_tsvd_5000_1000_2_layer_call_and_return_conditional_losses_15387182/
-fmn2_tsvd_5000_1000_2/StatefulPartitionedCall�
,fmn2_pca_5000_1000_2/StatefulPartitionedCallStatefulPartitionedCall5fmn2_pca_5000_1000_1/StatefulPartitionedCall:output:0fmn2_pca_5000_1000_2_1539168fmn2_pca_5000_1000_2_1539170*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_fmn2_pca_5000_1000_2_layer_call_and_return_conditional_losses_15387352.
,fmn2_pca_5000_1000_2/StatefulPartitionedCall�
-fmn2_tsvd_5000_1000_3/StatefulPartitionedCallStatefulPartitionedCall6fmn2_tsvd_5000_1000_2/StatefulPartitionedCall:output:0fmn2_tsvd_5000_1000_3_1539173fmn2_tsvd_5000_1000_3_1539175*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_fmn2_tsvd_5000_1000_3_layer_call_and_return_conditional_losses_15387522/
-fmn2_tsvd_5000_1000_3/StatefulPartitionedCall�
,fmn2_pca_5000_1000_3/StatefulPartitionedCallStatefulPartitionedCall5fmn2_pca_5000_1000_2/StatefulPartitionedCall:output:0fmn2_pca_5000_1000_3_1539178fmn2_pca_5000_1000_3_1539180*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_fmn2_pca_5000_1000_3_layer_call_and_return_conditional_losses_15387692.
,fmn2_pca_5000_1000_3/StatefulPartitionedCall�
.fmn2_pca_5000_1000_out/StatefulPartitionedCallStatefulPartitionedCall5fmn2_pca_5000_1000_3/StatefulPartitionedCall:output:0fmn2_pca_5000_1000_out_1539183fmn2_pca_5000_1000_out_1539185*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *\
fWRU
S__inference_fmn2_pca_5000_1000_out_layer_call_and_return_conditional_losses_153878620
.fmn2_pca_5000_1000_out/StatefulPartitionedCall�
/fmn2_tsvd_5000_1000_out/StatefulPartitionedCallStatefulPartitionedCall6fmn2_tsvd_5000_1000_3/StatefulPartitionedCall:output:0fmn2_tsvd_5000_1000_out_1539188fmn2_tsvd_5000_1000_out_1539190*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *]
fXRV
T__inference_fmn2_tsvd_5000_1000_out_layer_call_and_return_conditional_losses_153880321
/fmn2_tsvd_5000_1000_out/StatefulPartitionedCall�
concatenate_46/PartitionedCallPartitionedCall7fmn2_pca_5000_1000_out/StatefulPartitionedCall:output:08fmn2_tsvd_5000_1000_out/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *T
fORM
K__inference_concatenate_46_layer_call_and_return_conditional_losses_15388162 
concatenate_46/PartitionedCall�
joined/StatefulPartitionedCallStatefulPartitionedCall'concatenate_46/PartitionedCall:output:0joined_1539194joined_1539196*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *L
fGRE
C__inference_joined_layer_call_and_return_conditional_losses_15388292 
joined/StatefulPartitionedCall�
IdentityIdentity'joined/StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identity�
NoOpNoOp-^fmn2_pca_5000_1000_1/StatefulPartitionedCall-^fmn2_pca_5000_1000_2/StatefulPartitionedCall-^fmn2_pca_5000_1000_3/StatefulPartitionedCall/^fmn2_pca_5000_1000_out/StatefulPartitionedCall.^fmn2_tsvd_5000_1000_1/StatefulPartitionedCall.^fmn2_tsvd_5000_1000_2/StatefulPartitionedCall.^fmn2_tsvd_5000_1000_3/StatefulPartitionedCall0^fmn2_tsvd_5000_1000_out/StatefulPartitionedCall^joined/StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*_
_input_shapesN
L:����������:����������: : : : : : : : : : : : : : : : : : 2\
,fmn2_pca_5000_1000_1/StatefulPartitionedCall,fmn2_pca_5000_1000_1/StatefulPartitionedCall2\
,fmn2_pca_5000_1000_2/StatefulPartitionedCall,fmn2_pca_5000_1000_2/StatefulPartitionedCall2\
,fmn2_pca_5000_1000_3/StatefulPartitionedCall,fmn2_pca_5000_1000_3/StatefulPartitionedCall2`
.fmn2_pca_5000_1000_out/StatefulPartitionedCall.fmn2_pca_5000_1000_out/StatefulPartitionedCall2^
-fmn2_tsvd_5000_1000_1/StatefulPartitionedCall-fmn2_tsvd_5000_1000_1/StatefulPartitionedCall2^
-fmn2_tsvd_5000_1000_2/StatefulPartitionedCall-fmn2_tsvd_5000_1000_2/StatefulPartitionedCall2^
-fmn2_tsvd_5000_1000_3/StatefulPartitionedCall-fmn2_tsvd_5000_1000_3/StatefulPartitionedCall2b
/fmn2_tsvd_5000_1000_out/StatefulPartitionedCall/fmn2_tsvd_5000_1000_out/StatefulPartitionedCall2@
joined/StatefulPartitionedCalljoined/StatefulPartitionedCall:b ^
(
_output_shapes
:����������
2
_user_specified_namefmn2_pca_5000_1000_input:c_
(
_output_shapes
:����������
3
_user_specified_namefmn2_tsvd_5000_1000_input
�
�
'__inference_model_layer_call_fn_1538875
fmn2_pca_5000_1000_input
fmn2_tsvd_5000_1000_input
unknown:
��
	unknown_0:	�
	unknown_1:
��
	unknown_2:	�
	unknown_3:
��
	unknown_4:	�
	unknown_5:
��
	unknown_6:	�
	unknown_7:
��
	unknown_8:	�
	unknown_9:
��

unknown_10:	�

unknown_11:	�

unknown_12:

unknown_13:	�

unknown_14:

unknown_15:

unknown_16:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallfmn2_pca_5000_1000_inputfmn2_tsvd_5000_1000_inputunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10
unknown_11
unknown_12
unknown_13
unknown_14
unknown_15
unknown_16*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*4
_read_only_resource_inputs
	
*0
config_proto 

CPU

GPU2*0J 8� *K
fFRD
B__inference_model_layer_call_and_return_conditional_losses_15388362
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*_
_input_shapesN
L:����������:����������: : : : : : : : : : : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:b ^
(
_output_shapes
:����������
2
_user_specified_namefmn2_pca_5000_1000_input:c_
(
_output_shapes
:����������
3
_user_specified_namefmn2_tsvd_5000_1000_input
�
�
Q__inference_fmn2_pca_5000_1000_1_layer_call_and_return_conditional_losses_1538701

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
(__inference_joined_layer_call_fn_1539707

inputs
unknown:
	unknown_0:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *L
fGRE
C__inference_joined_layer_call_and_return_conditional_losses_15388292
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������: : 22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs"�L
saver_filename:0StatefulPartitionedCall_1:0StatefulPartitionedCall_28"
saved_model_main_op

NoOp*>
__saved_model_init_op%#
__saved_model_init_op

NoOp*�
serving_default�
^
fmn2_pca_5000_1000_inputB
*serving_default_fmn2_pca_5000_1000_input:0����������
`
fmn2_tsvd_5000_1000_inputC
+serving_default_fmn2_tsvd_5000_1000_input:0����������:
joined0
StatefulPartitionedCall:0���������tensorflow/serving/predict:Ϸ
�
layer-0
layer-1
layer_with_weights-0
layer-2
layer_with_weights-1
layer-3
layer_with_weights-2
layer-4
layer_with_weights-3
layer-5
layer_with_weights-4
layer-6
layer_with_weights-5
layer-7
	layer_with_weights-6
	layer-8

layer_with_weights-7

layer-9
layer-10
layer_with_weights-8
layer-11
	optimizer
	variables
regularization_losses
trainable_variables
	keras_api

signatures
�__call__
�_default_save_signature
+�&call_and_return_all_conditional_losses"
_tf_keras_network
D
#_self_saveable_object_factories"
_tf_keras_input_layer
"
_tf_keras_input_layer
�

kernel
bias
#_self_saveable_object_factories
	variables
regularization_losses
trainable_variables
	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�

kernel
bias
	variables
regularization_losses
trainable_variables
 	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�

!kernel
"bias
##_self_saveable_object_factories
$	variables
%regularization_losses
&trainable_variables
'	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�

(kernel
)bias
*	variables
+regularization_losses
,trainable_variables
-	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�

.kernel
/bias
#0_self_saveable_object_factories
1	variables
2regularization_losses
3trainable_variables
4	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�

5kernel
6bias
7	variables
8regularization_losses
9trainable_variables
:	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�

;kernel
<bias
#=_self_saveable_object_factories
>	variables
?regularization_losses
@trainable_variables
A	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�

Bkernel
Cbias
D	variables
Eregularization_losses
Ftrainable_variables
G	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�
H	variables
Iregularization_losses
Jtrainable_variables
K	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�

Lkernel
Mbias
N	variables
Oregularization_losses
Ptrainable_variables
Q	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
{
Riter

Sbeta_1

Tbeta_2
	Udecay
Vlearning_rateLm�Mm�Lv�Mv�"
	optimizer
�
0
1
2
3
!4
"5
(6
)7
.8
/9
510
611
;12
<13
B14
C15
L16
M17"
trackable_list_wrapper
 "
trackable_list_wrapper
.
L0
M1"
trackable_list_wrapper
�
Wmetrics

Xlayers
	variables
Ylayer_metrics
Zlayer_regularization_losses
regularization_losses
trainable_variables
[non_trainable_variables
�__call__
�_default_save_signature
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
-
�serving_default"
signature_map
 "
trackable_dict_wrapper
/:-
��2fmn2_pca_5000_1000_1/kernel
(:&�2fmn2_pca_5000_1000_1/bias
 "
trackable_dict_wrapper
.
0
1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
\metrics

]layers
	variables
regularization_losses
^layer_metrics
_layer_regularization_losses
trainable_variables
`non_trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
0:.
��2fmn2_tsvd_5000_1000_1/kernel
):'�2fmn2_tsvd_5000_1000_1/bias
.
0
1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
ametrics

blayers
	variables
regularization_losses
clayer_metrics
dlayer_regularization_losses
trainable_variables
enon_trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
/:-
��2fmn2_pca_5000_1000_2/kernel
(:&�2fmn2_pca_5000_1000_2/bias
 "
trackable_dict_wrapper
.
!0
"1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
fmetrics

glayers
$	variables
%regularization_losses
hlayer_metrics
ilayer_regularization_losses
&trainable_variables
jnon_trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
0:.
��2fmn2_tsvd_5000_1000_2/kernel
):'�2fmn2_tsvd_5000_1000_2/bias
.
(0
)1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
kmetrics

llayers
*	variables
+regularization_losses
mlayer_metrics
nlayer_regularization_losses
,trainable_variables
onon_trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
/:-
��2fmn2_pca_5000_1000_3/kernel
(:&�2fmn2_pca_5000_1000_3/bias
 "
trackable_dict_wrapper
.
.0
/1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
pmetrics

qlayers
1	variables
2regularization_losses
rlayer_metrics
slayer_regularization_losses
3trainable_variables
tnon_trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
0:.
��2fmn2_tsvd_5000_1000_3/kernel
):'�2fmn2_tsvd_5000_1000_3/bias
.
50
61"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
umetrics

vlayers
7	variables
8regularization_losses
wlayer_metrics
xlayer_regularization_losses
9trainable_variables
ynon_trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
0:.	�2fmn2_pca_5000_1000_out/kernel
):'2fmn2_pca_5000_1000_out/bias
 "
trackable_dict_wrapper
.
;0
<1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
zmetrics

{layers
>	variables
?regularization_losses
|layer_metrics
}layer_regularization_losses
@trainable_variables
~non_trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
1:/	�2fmn2_tsvd_5000_1000_out/kernel
*:(2fmn2_tsvd_5000_1000_out/bias
.
B0
C1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
metrics
�layers
D	variables
Eregularization_losses
�layer_metrics
 �layer_regularization_losses
Ftrainable_variables
�non_trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�metrics
�layers
H	variables
Iregularization_losses
�layer_metrics
 �layer_regularization_losses
Jtrainable_variables
�non_trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
:2joined/kernel
:2joined/bias
.
L0
M1"
trackable_list_wrapper
 "
trackable_list_wrapper
.
L0
M1"
trackable_list_wrapper
�
�metrics
�layers
N	variables
Oregularization_losses
�layer_metrics
 �layer_regularization_losses
Ptrainable_variables
�non_trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
:	 (2	Adam/iter
: (2Adam/beta_1
: (2Adam/beta_2
: (2
Adam/decay
: (2Adam/learning_rate
0
�0
�1"
trackable_list_wrapper
v
0
1
2
3
4
5
6
7
	8

9
10
11"
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
�
0
1
2
3
!4
"5
(6
)7
.8
/9
510
611
;12
<13
B14
C15"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
.
!0
"1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
.
(0
)1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
.
.0
/1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
.
50
61"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
.
;0
<1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
.
B0
C1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
R

�total

�count
�	variables
�	keras_api"
_tf_keras_metric
c

�total

�count
�
_fn_kwargs
�	variables
�	keras_api"
_tf_keras_metric
:  (2total
:  (2count
0
�0
�1"
trackable_list_wrapper
.
�	variables"
_generic_user_object
:  (2total
:  (2count
 "
trackable_dict_wrapper
0
�0
�1"
trackable_list_wrapper
.
�	variables"
_generic_user_object
$:"2Adam/joined/kernel/m
:2Adam/joined/bias/m
$:"2Adam/joined/kernel/v
:2Adam/joined/bias/v
�2�
'__inference_model_layer_call_fn_1538875
'__inference_model_layer_call_fn_1539343
'__inference_model_layer_call_fn_1539385
'__inference_model_layer_call_fn_1539149�
���
FullArgSpec1
args)�&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�B�
"__inference__wrapped_model_1538664fmn2_pca_5000_1000_inputfmn2_tsvd_5000_1000_input"�
���
FullArgSpec
args� 
varargsjargs
varkwjkwargs
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
B__inference_model_layer_call_and_return_conditional_losses_1539455
B__inference_model_layer_call_and_return_conditional_losses_1539525
B__inference_model_layer_call_and_return_conditional_losses_1539200
B__inference_model_layer_call_and_return_conditional_losses_1539251�
���
FullArgSpec1
args)�&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2�
6__inference_fmn2_pca_5000_1000_1_layer_call_fn_1539534�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
Q__inference_fmn2_pca_5000_1000_1_layer_call_and_return_conditional_losses_1539545�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
7__inference_fmn2_tsvd_5000_1000_1_layer_call_fn_1539554�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
R__inference_fmn2_tsvd_5000_1000_1_layer_call_and_return_conditional_losses_1539565�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
6__inference_fmn2_pca_5000_1000_2_layer_call_fn_1539574�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
Q__inference_fmn2_pca_5000_1000_2_layer_call_and_return_conditional_losses_1539585�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
7__inference_fmn2_tsvd_5000_1000_2_layer_call_fn_1539594�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
R__inference_fmn2_tsvd_5000_1000_2_layer_call_and_return_conditional_losses_1539605�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
6__inference_fmn2_pca_5000_1000_3_layer_call_fn_1539614�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
Q__inference_fmn2_pca_5000_1000_3_layer_call_and_return_conditional_losses_1539625�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
7__inference_fmn2_tsvd_5000_1000_3_layer_call_fn_1539634�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
R__inference_fmn2_tsvd_5000_1000_3_layer_call_and_return_conditional_losses_1539645�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
8__inference_fmn2_pca_5000_1000_out_layer_call_fn_1539654�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
S__inference_fmn2_pca_5000_1000_out_layer_call_and_return_conditional_losses_1539665�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
9__inference_fmn2_tsvd_5000_1000_out_layer_call_fn_1539674�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
T__inference_fmn2_tsvd_5000_1000_out_layer_call_and_return_conditional_losses_1539685�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
0__inference_concatenate_46_layer_call_fn_1539691�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
K__inference_concatenate_46_layer_call_and_return_conditional_losses_1539698�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
(__inference_joined_layer_call_fn_1539707�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
C__inference_joined_layer_call_and_return_conditional_losses_1539718�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
%__inference_signature_wrapper_1539301fmn2_pca_5000_1000_inputfmn2_tsvd_5000_1000_input"�
���
FullArgSpec
args� 
varargs
 
varkwjkwargs
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 �
"__inference__wrapped_model_1538664�()!"56./;<BCLM}�z
s�p
n�k
3�0
fmn2_pca_5000_1000_input����������
4�1
fmn2_tsvd_5000_1000_input����������
� "/�,
*
joined �
joined����������
K__inference_concatenate_46_layer_call_and_return_conditional_losses_1539698�Z�W
P�M
K�H
"�
inputs/0���������
"�
inputs/1���������
� "%�"
�
0���������
� �
0__inference_concatenate_46_layer_call_fn_1539691vZ�W
P�M
K�H
"�
inputs/0���������
"�
inputs/1���������
� "�����������
Q__inference_fmn2_pca_5000_1000_1_layer_call_and_return_conditional_losses_1539545^0�-
&�#
!�
inputs����������
� "&�#
�
0����������
� �
6__inference_fmn2_pca_5000_1000_1_layer_call_fn_1539534Q0�-
&�#
!�
inputs����������
� "������������
Q__inference_fmn2_pca_5000_1000_2_layer_call_and_return_conditional_losses_1539585^!"0�-
&�#
!�
inputs����������
� "&�#
�
0����������
� �
6__inference_fmn2_pca_5000_1000_2_layer_call_fn_1539574Q!"0�-
&�#
!�
inputs����������
� "������������
Q__inference_fmn2_pca_5000_1000_3_layer_call_and_return_conditional_losses_1539625^./0�-
&�#
!�
inputs����������
� "&�#
�
0����������
� �
6__inference_fmn2_pca_5000_1000_3_layer_call_fn_1539614Q./0�-
&�#
!�
inputs����������
� "������������
S__inference_fmn2_pca_5000_1000_out_layer_call_and_return_conditional_losses_1539665];<0�-
&�#
!�
inputs����������
� "%�"
�
0���������
� �
8__inference_fmn2_pca_5000_1000_out_layer_call_fn_1539654P;<0�-
&�#
!�
inputs����������
� "�����������
R__inference_fmn2_tsvd_5000_1000_1_layer_call_and_return_conditional_losses_1539565^0�-
&�#
!�
inputs����������
� "&�#
�
0����������
� �
7__inference_fmn2_tsvd_5000_1000_1_layer_call_fn_1539554Q0�-
&�#
!�
inputs����������
� "������������
R__inference_fmn2_tsvd_5000_1000_2_layer_call_and_return_conditional_losses_1539605^()0�-
&�#
!�
inputs����������
� "&�#
�
0����������
� �
7__inference_fmn2_tsvd_5000_1000_2_layer_call_fn_1539594Q()0�-
&�#
!�
inputs����������
� "������������
R__inference_fmn2_tsvd_5000_1000_3_layer_call_and_return_conditional_losses_1539645^560�-
&�#
!�
inputs����������
� "&�#
�
0����������
� �
7__inference_fmn2_tsvd_5000_1000_3_layer_call_fn_1539634Q560�-
&�#
!�
inputs����������
� "������������
T__inference_fmn2_tsvd_5000_1000_out_layer_call_and_return_conditional_losses_1539685]BC0�-
&�#
!�
inputs����������
� "%�"
�
0���������
� �
9__inference_fmn2_tsvd_5000_1000_out_layer_call_fn_1539674PBC0�-
&�#
!�
inputs����������
� "�����������
C__inference_joined_layer_call_and_return_conditional_losses_1539718\LM/�,
%�"
 �
inputs���������
� "%�"
�
0���������
� {
(__inference_joined_layer_call_fn_1539707OLM/�,
%�"
 �
inputs���������
� "�����������
B__inference_model_layer_call_and_return_conditional_losses_1539200�()!"56./;<BCLM���
{�x
n�k
3�0
fmn2_pca_5000_1000_input����������
4�1
fmn2_tsvd_5000_1000_input����������
p 

 
� "%�"
�
0���������
� �
B__inference_model_layer_call_and_return_conditional_losses_1539251�()!"56./;<BCLM���
{�x
n�k
3�0
fmn2_pca_5000_1000_input����������
4�1
fmn2_tsvd_5000_1000_input����������
p

 
� "%�"
�
0���������
� �
B__inference_model_layer_call_and_return_conditional_losses_1539455�()!"56./;<BCLMd�a
Z�W
M�J
#� 
inputs/0����������
#� 
inputs/1����������
p 

 
� "%�"
�
0���������
� �
B__inference_model_layer_call_and_return_conditional_losses_1539525�()!"56./;<BCLMd�a
Z�W
M�J
#� 
inputs/0����������
#� 
inputs/1����������
p

 
� "%�"
�
0���������
� �
'__inference_model_layer_call_fn_1538875�()!"56./;<BCLM���
{�x
n�k
3�0
fmn2_pca_5000_1000_input����������
4�1
fmn2_tsvd_5000_1000_input����������
p 

 
� "�����������
'__inference_model_layer_call_fn_1539149�()!"56./;<BCLM���
{�x
n�k
3�0
fmn2_pca_5000_1000_input����������
4�1
fmn2_tsvd_5000_1000_input����������
p

 
� "�����������
'__inference_model_layer_call_fn_1539343�()!"56./;<BCLMd�a
Z�W
M�J
#� 
inputs/0����������
#� 
inputs/1����������
p 

 
� "�����������
'__inference_model_layer_call_fn_1539385�()!"56./;<BCLMd�a
Z�W
M�J
#� 
inputs/0����������
#� 
inputs/1����������
p

 
� "�����������
%__inference_signature_wrapper_1539301�()!"56./;<BCLM���
� 
���
O
fmn2_pca_5000_1000_input3�0
fmn2_pca_5000_1000_input����������
Q
fmn2_tsvd_5000_1000_input4�1
fmn2_tsvd_5000_1000_input����������"/�,
*
joined �
joined���������