��
��
B
AssignVariableOp
resource
value"dtype"
dtypetype�
~
BiasAdd

value"T	
bias"T
output"T" 
Ttype:
2	"-
data_formatstringNHWC:
NHWCNCHW
h
ConcatV2
values"T*N
axis"Tidx
output"T"
Nint(0"	
Ttype"
Tidxtype0:
2	
8
Const
output"dtype"
valuetensor"
dtypetype
.
Identity

input"T
output"T"	
Ttype
q
MatMul
a"T
b"T
product"T"
transpose_abool( "
transpose_bbool( "
Ttype:

2	
e
MergeV2Checkpoints
checkpoint_prefixes
destination_prefix"
delete_old_dirsbool(�

NoOp
M
Pack
values"T*N
output"T"
Nint(0"	
Ttype"
axisint 
C
Placeholder
output"dtype"
dtypetype"
shapeshape:
@
ReadVariableOp
resource
value"dtype"
dtypetype�
E
Relu
features"T
activations"T"
Ttype:
2	
o
	RestoreV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0�
l
SaveV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0�
?
Select
	condition

t"T
e"T
output"T"	
Ttype
H
ShardedFilename
basename	
shard

num_shards
filename
0
Sigmoid
x"T
y"T"
Ttype:

2
�
StatefulPartitionedCall
args2Tin
output2Tout"
Tin
list(type)("
Tout
list(type)("	
ffunc"
configstring "
config_protostring "
executor_typestring �
@
StaticRegexFullMatch	
input

output
"
patternstring
N

StringJoin
inputs*N

output"
Nint(0"
	separatorstring 
�
VarHandleOp
resource"
	containerstring "
shared_namestring "
dtypetype"
shapeshape"#
allowed_deviceslist(string)
 �"serve*2.6.02unknown8�
�
fmn2_pca_5000_1000_1/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*,
shared_namefmn2_pca_5000_1000_1/kernel
�
/fmn2_pca_5000_1000_1/kernel/Read/ReadVariableOpReadVariableOpfmn2_pca_5000_1000_1/kernel* 
_output_shapes
:
��*
dtype0
�
fmn2_pca_5000_1000_1/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�**
shared_namefmn2_pca_5000_1000_1/bias
�
-fmn2_pca_5000_1000_1/bias/Read/ReadVariableOpReadVariableOpfmn2_pca_5000_1000_1/bias*
_output_shapes	
:�*
dtype0
�
fmn2_tsvd_5000_1000_1/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*-
shared_namefmn2_tsvd_5000_1000_1/kernel
�
0fmn2_tsvd_5000_1000_1/kernel/Read/ReadVariableOpReadVariableOpfmn2_tsvd_5000_1000_1/kernel* 
_output_shapes
:
��*
dtype0
�
fmn2_tsvd_5000_1000_1/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*+
shared_namefmn2_tsvd_5000_1000_1/bias
�
.fmn2_tsvd_5000_1000_1/bias/Read/ReadVariableOpReadVariableOpfmn2_tsvd_5000_1000_1/bias*
_output_shapes	
:�*
dtype0
�
fmn2_isomap_5000_1000_1/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*/
shared_name fmn2_isomap_5000_1000_1/kernel
�
2fmn2_isomap_5000_1000_1/kernel/Read/ReadVariableOpReadVariableOpfmn2_isomap_5000_1000_1/kernel* 
_output_shapes
:
��*
dtype0
�
fmn2_isomap_5000_1000_1/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*-
shared_namefmn2_isomap_5000_1000_1/bias
�
0fmn2_isomap_5000_1000_1/bias/Read/ReadVariableOpReadVariableOpfmn2_isomap_5000_1000_1/bias*
_output_shapes	
:�*
dtype0
�
fmn2_lle_5000_1000_1/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*,
shared_namefmn2_lle_5000_1000_1/kernel
�
/fmn2_lle_5000_1000_1/kernel/Read/ReadVariableOpReadVariableOpfmn2_lle_5000_1000_1/kernel* 
_output_shapes
:
��*
dtype0
�
fmn2_lle_5000_1000_1/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�**
shared_namefmn2_lle_5000_1000_1/bias
�
-fmn2_lle_5000_1000_1/bias/Read/ReadVariableOpReadVariableOpfmn2_lle_5000_1000_1/bias*
_output_shapes	
:�*
dtype0
�
fmn2_pca_5000_1000_2/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*,
shared_namefmn2_pca_5000_1000_2/kernel
�
/fmn2_pca_5000_1000_2/kernel/Read/ReadVariableOpReadVariableOpfmn2_pca_5000_1000_2/kernel* 
_output_shapes
:
��*
dtype0
�
fmn2_pca_5000_1000_2/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�**
shared_namefmn2_pca_5000_1000_2/bias
�
-fmn2_pca_5000_1000_2/bias/Read/ReadVariableOpReadVariableOpfmn2_pca_5000_1000_2/bias*
_output_shapes	
:�*
dtype0
�
fmn2_tsvd_5000_1000_2/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*-
shared_namefmn2_tsvd_5000_1000_2/kernel
�
0fmn2_tsvd_5000_1000_2/kernel/Read/ReadVariableOpReadVariableOpfmn2_tsvd_5000_1000_2/kernel* 
_output_shapes
:
��*
dtype0
�
fmn2_tsvd_5000_1000_2/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*+
shared_namefmn2_tsvd_5000_1000_2/bias
�
.fmn2_tsvd_5000_1000_2/bias/Read/ReadVariableOpReadVariableOpfmn2_tsvd_5000_1000_2/bias*
_output_shapes	
:�*
dtype0
�
fmn2_isomap_5000_1000_2/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*/
shared_name fmn2_isomap_5000_1000_2/kernel
�
2fmn2_isomap_5000_1000_2/kernel/Read/ReadVariableOpReadVariableOpfmn2_isomap_5000_1000_2/kernel* 
_output_shapes
:
��*
dtype0
�
fmn2_isomap_5000_1000_2/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*-
shared_namefmn2_isomap_5000_1000_2/bias
�
0fmn2_isomap_5000_1000_2/bias/Read/ReadVariableOpReadVariableOpfmn2_isomap_5000_1000_2/bias*
_output_shapes	
:�*
dtype0
�
fmn2_lle_5000_1000_2/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*,
shared_namefmn2_lle_5000_1000_2/kernel
�
/fmn2_lle_5000_1000_2/kernel/Read/ReadVariableOpReadVariableOpfmn2_lle_5000_1000_2/kernel* 
_output_shapes
:
��*
dtype0
�
fmn2_lle_5000_1000_2/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�**
shared_namefmn2_lle_5000_1000_2/bias
�
-fmn2_lle_5000_1000_2/bias/Read/ReadVariableOpReadVariableOpfmn2_lle_5000_1000_2/bias*
_output_shapes	
:�*
dtype0
�
fmn2_pca_5000_1000_3/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*,
shared_namefmn2_pca_5000_1000_3/kernel
�
/fmn2_pca_5000_1000_3/kernel/Read/ReadVariableOpReadVariableOpfmn2_pca_5000_1000_3/kernel* 
_output_shapes
:
��*
dtype0
�
fmn2_pca_5000_1000_3/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�**
shared_namefmn2_pca_5000_1000_3/bias
�
-fmn2_pca_5000_1000_3/bias/Read/ReadVariableOpReadVariableOpfmn2_pca_5000_1000_3/bias*
_output_shapes	
:�*
dtype0
�
fmn2_tsvd_5000_1000_3/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*-
shared_namefmn2_tsvd_5000_1000_3/kernel
�
0fmn2_tsvd_5000_1000_3/kernel/Read/ReadVariableOpReadVariableOpfmn2_tsvd_5000_1000_3/kernel* 
_output_shapes
:
��*
dtype0
�
fmn2_tsvd_5000_1000_3/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*+
shared_namefmn2_tsvd_5000_1000_3/bias
�
.fmn2_tsvd_5000_1000_3/bias/Read/ReadVariableOpReadVariableOpfmn2_tsvd_5000_1000_3/bias*
_output_shapes	
:�*
dtype0
�
fmn2_isomap_5000_1000_3/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*/
shared_name fmn2_isomap_5000_1000_3/kernel
�
2fmn2_isomap_5000_1000_3/kernel/Read/ReadVariableOpReadVariableOpfmn2_isomap_5000_1000_3/kernel* 
_output_shapes
:
��*
dtype0
�
fmn2_isomap_5000_1000_3/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*-
shared_namefmn2_isomap_5000_1000_3/bias
�
0fmn2_isomap_5000_1000_3/bias/Read/ReadVariableOpReadVariableOpfmn2_isomap_5000_1000_3/bias*
_output_shapes	
:�*
dtype0
�
fmn2_lle_5000_1000_3/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*,
shared_namefmn2_lle_5000_1000_3/kernel
�
/fmn2_lle_5000_1000_3/kernel/Read/ReadVariableOpReadVariableOpfmn2_lle_5000_1000_3/kernel* 
_output_shapes
:
��*
dtype0
�
fmn2_lle_5000_1000_3/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�**
shared_namefmn2_lle_5000_1000_3/bias
�
-fmn2_lle_5000_1000_3/bias/Read/ReadVariableOpReadVariableOpfmn2_lle_5000_1000_3/bias*
_output_shapes	
:�*
dtype0
�
fmn2_pca_5000_1000_out/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:	�*.
shared_namefmn2_pca_5000_1000_out/kernel
�
1fmn2_pca_5000_1000_out/kernel/Read/ReadVariableOpReadVariableOpfmn2_pca_5000_1000_out/kernel*
_output_shapes
:	�*
dtype0
�
fmn2_pca_5000_1000_out/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:*,
shared_namefmn2_pca_5000_1000_out/bias
�
/fmn2_pca_5000_1000_out/bias/Read/ReadVariableOpReadVariableOpfmn2_pca_5000_1000_out/bias*
_output_shapes
:*
dtype0
�
fmn2_tsvd_5000_1000_out/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:	�*/
shared_name fmn2_tsvd_5000_1000_out/kernel
�
2fmn2_tsvd_5000_1000_out/kernel/Read/ReadVariableOpReadVariableOpfmn2_tsvd_5000_1000_out/kernel*
_output_shapes
:	�*
dtype0
�
fmn2_tsvd_5000_1000_out/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:*-
shared_namefmn2_tsvd_5000_1000_out/bias
�
0fmn2_tsvd_5000_1000_out/bias/Read/ReadVariableOpReadVariableOpfmn2_tsvd_5000_1000_out/bias*
_output_shapes
:*
dtype0
�
 fmn2_isomap_5000_1000_out/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:	�*1
shared_name" fmn2_isomap_5000_1000_out/kernel
�
4fmn2_isomap_5000_1000_out/kernel/Read/ReadVariableOpReadVariableOp fmn2_isomap_5000_1000_out/kernel*
_output_shapes
:	�*
dtype0
�
fmn2_isomap_5000_1000_out/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:*/
shared_name fmn2_isomap_5000_1000_out/bias
�
2fmn2_isomap_5000_1000_out/bias/Read/ReadVariableOpReadVariableOpfmn2_isomap_5000_1000_out/bias*
_output_shapes
:*
dtype0
�
fmn2_lle_5000_1000_out/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:	�*.
shared_namefmn2_lle_5000_1000_out/kernel
�
1fmn2_lle_5000_1000_out/kernel/Read/ReadVariableOpReadVariableOpfmn2_lle_5000_1000_out/kernel*
_output_shapes
:	�*
dtype0
�
fmn2_lle_5000_1000_out/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:*,
shared_namefmn2_lle_5000_1000_out/bias
�
/fmn2_lle_5000_1000_out/bias/Read/ReadVariableOpReadVariableOpfmn2_lle_5000_1000_out/bias*
_output_shapes
:*
dtype0
v
joined/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:*
shared_namejoined/kernel
o
!joined/kernel/Read/ReadVariableOpReadVariableOpjoined/kernel*
_output_shapes

:*
dtype0
n
joined/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:*
shared_namejoined/bias
g
joined/bias/Read/ReadVariableOpReadVariableOpjoined/bias*
_output_shapes
:*
dtype0
f
	Adam/iterVarHandleOp*
_output_shapes
: *
dtype0	*
shape: *
shared_name	Adam/iter
_
Adam/iter/Read/ReadVariableOpReadVariableOp	Adam/iter*
_output_shapes
: *
dtype0	
j
Adam/beta_1VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nameAdam/beta_1
c
Adam/beta_1/Read/ReadVariableOpReadVariableOpAdam/beta_1*
_output_shapes
: *
dtype0
j
Adam/beta_2VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nameAdam/beta_2
c
Adam/beta_2/Read/ReadVariableOpReadVariableOpAdam/beta_2*
_output_shapes
: *
dtype0
h

Adam/decayVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name
Adam/decay
a
Adam/decay/Read/ReadVariableOpReadVariableOp
Adam/decay*
_output_shapes
: *
dtype0
x
Adam/learning_rateVarHandleOp*
_output_shapes
: *
dtype0*
shape: *#
shared_nameAdam/learning_rate
q
&Adam/learning_rate/Read/ReadVariableOpReadVariableOpAdam/learning_rate*
_output_shapes
: *
dtype0
^
totalVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nametotal
W
total/Read/ReadVariableOpReadVariableOptotal*
_output_shapes
: *
dtype0
^
countVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_namecount
W
count/Read/ReadVariableOpReadVariableOpcount*
_output_shapes
: *
dtype0
b
total_1VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name	total_1
[
total_1/Read/ReadVariableOpReadVariableOptotal_1*
_output_shapes
: *
dtype0
b
count_1VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name	count_1
[
count_1/Read/ReadVariableOpReadVariableOpcount_1*
_output_shapes
: *
dtype0
�
Adam/joined/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape
:*%
shared_nameAdam/joined/kernel/m
}
(Adam/joined/kernel/m/Read/ReadVariableOpReadVariableOpAdam/joined/kernel/m*
_output_shapes

:*
dtype0
|
Adam/joined/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:*#
shared_nameAdam/joined/bias/m
u
&Adam/joined/bias/m/Read/ReadVariableOpReadVariableOpAdam/joined/bias/m*
_output_shapes
:*
dtype0
�
Adam/joined/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape
:*%
shared_nameAdam/joined/kernel/v
}
(Adam/joined/kernel/v/Read/ReadVariableOpReadVariableOpAdam/joined/kernel/v*
_output_shapes

:*
dtype0
|
Adam/joined/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:*#
shared_nameAdam/joined/bias/v
u
&Adam/joined/bias/v/Read/ReadVariableOpReadVariableOpAdam/joined/bias/v*
_output_shapes
:*
dtype0

NoOpNoOp
�k
ConstConst"/device:CPU:0*
_output_shapes
: *
dtype0*�j
value�jB�j B�j
�
layer-0
layer-1
layer-2
layer-3
layer_with_weights-0
layer-4
layer_with_weights-1
layer-5
layer_with_weights-2
layer-6
layer_with_weights-3
layer-7
	layer_with_weights-4
	layer-8

layer_with_weights-5

layer-9
layer_with_weights-6
layer-10
layer_with_weights-7
layer-11
layer_with_weights-8
layer-12
layer_with_weights-9
layer-13
layer_with_weights-10
layer-14
layer_with_weights-11
layer-15
layer_with_weights-12
layer-16
layer_with_weights-13
layer-17
layer_with_weights-14
layer-18
layer_with_weights-15
layer-19
layer-20
layer_with_weights-16
layer-21
	optimizer
	variables
regularization_losses
trainable_variables
	keras_api

signatures
%
#_self_saveable_object_factories
%
#_self_saveable_object_factories
%
#_self_saveable_object_factories
%
# _self_saveable_object_factories
�

!kernel
"bias
##_self_saveable_object_factories
$	variables
%regularization_losses
&trainable_variables
'	keras_api
�

(kernel
)bias
#*_self_saveable_object_factories
+	variables
,regularization_losses
-trainable_variables
.	keras_api
�

/kernel
0bias
#1_self_saveable_object_factories
2	variables
3regularization_losses
4trainable_variables
5	keras_api
�

6kernel
7bias
#8_self_saveable_object_factories
9	variables
:regularization_losses
;trainable_variables
<	keras_api
�

=kernel
>bias
#?_self_saveable_object_factories
@	variables
Aregularization_losses
Btrainable_variables
C	keras_api
�

Dkernel
Ebias
#F_self_saveable_object_factories
G	variables
Hregularization_losses
Itrainable_variables
J	keras_api
�

Kkernel
Lbias
#M_self_saveable_object_factories
N	variables
Oregularization_losses
Ptrainable_variables
Q	keras_api
�

Rkernel
Sbias
#T_self_saveable_object_factories
U	variables
Vregularization_losses
Wtrainable_variables
X	keras_api
�

Ykernel
Zbias
#[_self_saveable_object_factories
\	variables
]regularization_losses
^trainable_variables
_	keras_api
�

`kernel
abias
#b_self_saveable_object_factories
c	variables
dregularization_losses
etrainable_variables
f	keras_api
�

gkernel
hbias
#i_self_saveable_object_factories
j	variables
kregularization_losses
ltrainable_variables
m	keras_api
�

nkernel
obias
#p_self_saveable_object_factories
q	variables
rregularization_losses
strainable_variables
t	keras_api
�

ukernel
vbias
#w_self_saveable_object_factories
x	variables
yregularization_losses
ztrainable_variables
{	keras_api
�

|kernel
}bias
#~_self_saveable_object_factories
	variables
�regularization_losses
�trainable_variables
�	keras_api
�
�kernel
	�bias
$�_self_saveable_object_factories
�	variables
�regularization_losses
�trainable_variables
�	keras_api
�
�kernel
	�bias
$�_self_saveable_object_factories
�	variables
�regularization_losses
�trainable_variables
�	keras_api
V
�	variables
�regularization_losses
�trainable_variables
�	keras_api
n
�kernel
	�bias
�	variables
�regularization_losses
�trainable_variables
�	keras_api
q
	�iter
�beta_1
�beta_2

�decay
�learning_rate	�m�	�m�	�v�	�v�
�
!0
"1
(2
)3
/4
05
66
77
=8
>9
D10
E11
K12
L13
R14
S15
Y16
Z17
`18
a19
g20
h21
n22
o23
u24
v25
|26
}27
�28
�29
�30
�31
�32
�33
 

�0
�1
�
�metrics
�layers
	variables
�layer_metrics
 �layer_regularization_losses
regularization_losses
trainable_variables
�non_trainable_variables
 
 
 
 
 
ge
VARIABLE_VALUEfmn2_pca_5000_1000_1/kernel6layer_with_weights-0/kernel/.ATTRIBUTES/VARIABLE_VALUE
ca
VARIABLE_VALUEfmn2_pca_5000_1000_1/bias4layer_with_weights-0/bias/.ATTRIBUTES/VARIABLE_VALUE
 

!0
"1
 
 
�
�metrics
�layers
$	variables
%regularization_losses
�layer_metrics
 �layer_regularization_losses
&trainable_variables
�non_trainable_variables
hf
VARIABLE_VALUEfmn2_tsvd_5000_1000_1/kernel6layer_with_weights-1/kernel/.ATTRIBUTES/VARIABLE_VALUE
db
VARIABLE_VALUEfmn2_tsvd_5000_1000_1/bias4layer_with_weights-1/bias/.ATTRIBUTES/VARIABLE_VALUE
 

(0
)1
 
 
�
�metrics
�layers
+	variables
,regularization_losses
�layer_metrics
 �layer_regularization_losses
-trainable_variables
�non_trainable_variables
jh
VARIABLE_VALUEfmn2_isomap_5000_1000_1/kernel6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUE
fd
VARIABLE_VALUEfmn2_isomap_5000_1000_1/bias4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUE
 

/0
01
 
 
�
�metrics
�layers
2	variables
3regularization_losses
�layer_metrics
 �layer_regularization_losses
4trainable_variables
�non_trainable_variables
ge
VARIABLE_VALUEfmn2_lle_5000_1000_1/kernel6layer_with_weights-3/kernel/.ATTRIBUTES/VARIABLE_VALUE
ca
VARIABLE_VALUEfmn2_lle_5000_1000_1/bias4layer_with_weights-3/bias/.ATTRIBUTES/VARIABLE_VALUE
 

60
71
 
 
�
�metrics
�layers
9	variables
:regularization_losses
�layer_metrics
 �layer_regularization_losses
;trainable_variables
�non_trainable_variables
ge
VARIABLE_VALUEfmn2_pca_5000_1000_2/kernel6layer_with_weights-4/kernel/.ATTRIBUTES/VARIABLE_VALUE
ca
VARIABLE_VALUEfmn2_pca_5000_1000_2/bias4layer_with_weights-4/bias/.ATTRIBUTES/VARIABLE_VALUE
 

=0
>1
 
 
�
�metrics
�layers
@	variables
Aregularization_losses
�layer_metrics
 �layer_regularization_losses
Btrainable_variables
�non_trainable_variables
hf
VARIABLE_VALUEfmn2_tsvd_5000_1000_2/kernel6layer_with_weights-5/kernel/.ATTRIBUTES/VARIABLE_VALUE
db
VARIABLE_VALUEfmn2_tsvd_5000_1000_2/bias4layer_with_weights-5/bias/.ATTRIBUTES/VARIABLE_VALUE
 

D0
E1
 
 
�
�metrics
�layers
G	variables
Hregularization_losses
�layer_metrics
 �layer_regularization_losses
Itrainable_variables
�non_trainable_variables
jh
VARIABLE_VALUEfmn2_isomap_5000_1000_2/kernel6layer_with_weights-6/kernel/.ATTRIBUTES/VARIABLE_VALUE
fd
VARIABLE_VALUEfmn2_isomap_5000_1000_2/bias4layer_with_weights-6/bias/.ATTRIBUTES/VARIABLE_VALUE
 

K0
L1
 
 
�
�metrics
�layers
N	variables
Oregularization_losses
�layer_metrics
 �layer_regularization_losses
Ptrainable_variables
�non_trainable_variables
ge
VARIABLE_VALUEfmn2_lle_5000_1000_2/kernel6layer_with_weights-7/kernel/.ATTRIBUTES/VARIABLE_VALUE
ca
VARIABLE_VALUEfmn2_lle_5000_1000_2/bias4layer_with_weights-7/bias/.ATTRIBUTES/VARIABLE_VALUE
 

R0
S1
 
 
�
�metrics
�layers
U	variables
Vregularization_losses
�layer_metrics
 �layer_regularization_losses
Wtrainable_variables
�non_trainable_variables
ge
VARIABLE_VALUEfmn2_pca_5000_1000_3/kernel6layer_with_weights-8/kernel/.ATTRIBUTES/VARIABLE_VALUE
ca
VARIABLE_VALUEfmn2_pca_5000_1000_3/bias4layer_with_weights-8/bias/.ATTRIBUTES/VARIABLE_VALUE
 

Y0
Z1
 
 
�
�metrics
�layers
\	variables
]regularization_losses
�layer_metrics
 �layer_regularization_losses
^trainable_variables
�non_trainable_variables
hf
VARIABLE_VALUEfmn2_tsvd_5000_1000_3/kernel6layer_with_weights-9/kernel/.ATTRIBUTES/VARIABLE_VALUE
db
VARIABLE_VALUEfmn2_tsvd_5000_1000_3/bias4layer_with_weights-9/bias/.ATTRIBUTES/VARIABLE_VALUE
 

`0
a1
 
 
�
�metrics
�layers
c	variables
dregularization_losses
�layer_metrics
 �layer_regularization_losses
etrainable_variables
�non_trainable_variables
ki
VARIABLE_VALUEfmn2_isomap_5000_1000_3/kernel7layer_with_weights-10/kernel/.ATTRIBUTES/VARIABLE_VALUE
ge
VARIABLE_VALUEfmn2_isomap_5000_1000_3/bias5layer_with_weights-10/bias/.ATTRIBUTES/VARIABLE_VALUE
 

g0
h1
 
 
�
�metrics
�layers
j	variables
kregularization_losses
�layer_metrics
 �layer_regularization_losses
ltrainable_variables
�non_trainable_variables
hf
VARIABLE_VALUEfmn2_lle_5000_1000_3/kernel7layer_with_weights-11/kernel/.ATTRIBUTES/VARIABLE_VALUE
db
VARIABLE_VALUEfmn2_lle_5000_1000_3/bias5layer_with_weights-11/bias/.ATTRIBUTES/VARIABLE_VALUE
 

n0
o1
 
 
�
�metrics
�layers
q	variables
rregularization_losses
�layer_metrics
 �layer_regularization_losses
strainable_variables
�non_trainable_variables
jh
VARIABLE_VALUEfmn2_pca_5000_1000_out/kernel7layer_with_weights-12/kernel/.ATTRIBUTES/VARIABLE_VALUE
fd
VARIABLE_VALUEfmn2_pca_5000_1000_out/bias5layer_with_weights-12/bias/.ATTRIBUTES/VARIABLE_VALUE
 

u0
v1
 
 
�
�metrics
�layers
x	variables
yregularization_losses
�layer_metrics
 �layer_regularization_losses
ztrainable_variables
�non_trainable_variables
ki
VARIABLE_VALUEfmn2_tsvd_5000_1000_out/kernel7layer_with_weights-13/kernel/.ATTRIBUTES/VARIABLE_VALUE
ge
VARIABLE_VALUEfmn2_tsvd_5000_1000_out/bias5layer_with_weights-13/bias/.ATTRIBUTES/VARIABLE_VALUE
 

|0
}1
 
 
�
�metrics
�layers
	variables
�regularization_losses
�layer_metrics
 �layer_regularization_losses
�trainable_variables
�non_trainable_variables
mk
VARIABLE_VALUE fmn2_isomap_5000_1000_out/kernel7layer_with_weights-14/kernel/.ATTRIBUTES/VARIABLE_VALUE
ig
VARIABLE_VALUEfmn2_isomap_5000_1000_out/bias5layer_with_weights-14/bias/.ATTRIBUTES/VARIABLE_VALUE
 

�0
�1
 
 
�
�metrics
�layers
�	variables
�regularization_losses
�layer_metrics
 �layer_regularization_losses
�trainable_variables
�non_trainable_variables
jh
VARIABLE_VALUEfmn2_lle_5000_1000_out/kernel7layer_with_weights-15/kernel/.ATTRIBUTES/VARIABLE_VALUE
fd
VARIABLE_VALUEfmn2_lle_5000_1000_out/bias5layer_with_weights-15/bias/.ATTRIBUTES/VARIABLE_VALUE
 

�0
�1
 
 
�
�metrics
�layers
�	variables
�regularization_losses
�layer_metrics
 �layer_regularization_losses
�trainable_variables
�non_trainable_variables
 
 
 
�
�metrics
�layers
�	variables
�regularization_losses
�layer_metrics
 �layer_regularization_losses
�trainable_variables
�non_trainable_variables
ZX
VARIABLE_VALUEjoined/kernel7layer_with_weights-16/kernel/.ATTRIBUTES/VARIABLE_VALUE
VT
VARIABLE_VALUEjoined/bias5layer_with_weights-16/bias/.ATTRIBUTES/VARIABLE_VALUE

�0
�1
 

�0
�1
�
�metrics
�layers
�	variables
�regularization_losses
�layer_metrics
 �layer_regularization_losses
�trainable_variables
�non_trainable_variables
HF
VARIABLE_VALUE	Adam/iter)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUE
LJ
VARIABLE_VALUEAdam/beta_1+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUE
LJ
VARIABLE_VALUEAdam/beta_2+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUE
JH
VARIABLE_VALUE
Adam/decay*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUE
ZX
VARIABLE_VALUEAdam/learning_rate2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUE

�0
�1
�
0
1
2
3
4
5
6
7
	8

9
10
11
12
13
14
15
16
17
18
19
20
21
 
 
�
!0
"1
(2
)3
/4
05
66
77
=8
>9
D10
E11
K12
L13
R14
S15
Y16
Z17
`18
a19
g20
h21
n22
o23
u24
v25
|26
}27
�28
�29
�30
�31
 
 
 
 

!0
"1
 
 
 
 

(0
)1
 
 
 
 

/0
01
 
 
 
 

60
71
 
 
 
 

=0
>1
 
 
 
 

D0
E1
 
 
 
 

K0
L1
 
 
 
 

R0
S1
 
 
 
 

Y0
Z1
 
 
 
 

`0
a1
 
 
 
 

g0
h1
 
 
 
 

n0
o1
 
 
 
 

u0
v1
 
 
 
 

|0
}1
 
 
 
 

�0
�1
 
 
 
 

�0
�1
 
 
 
 
 
 
 
 
 
 
8

�total

�count
�	variables
�	keras_api
I

�total

�count
�
_fn_kwargs
�	variables
�	keras_api
OM
VARIABLE_VALUEtotal4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUE
OM
VARIABLE_VALUEcount4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUE

�0
�1

�	variables
QO
VARIABLE_VALUEtotal_14keras_api/metrics/1/total/.ATTRIBUTES/VARIABLE_VALUE
QO
VARIABLE_VALUEcount_14keras_api/metrics/1/count/.ATTRIBUTES/VARIABLE_VALUE
 

�0
�1

�	variables
}{
VARIABLE_VALUEAdam/joined/kernel/mSlayer_with_weights-16/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
yw
VARIABLE_VALUEAdam/joined/bias/mQlayer_with_weights-16/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
}{
VARIABLE_VALUEAdam/joined/kernel/vSlayer_with_weights-16/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
yw
VARIABLE_VALUEAdam/joined/bias/vQlayer_with_weights-16/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
�
+serving_default_fmn2_isomap_5000_1000_inputPlaceholder*(
_output_shapes
:����������*
dtype0*
shape:����������
�
(serving_default_fmn2_lle_5000_1000_inputPlaceholder*(
_output_shapes
:����������*
dtype0*
shape:����������
�
(serving_default_fmn2_pca_5000_1000_inputPlaceholder*(
_output_shapes
:����������*
dtype0*
shape:����������
�
)serving_default_fmn2_tsvd_5000_1000_inputPlaceholder*(
_output_shapes
:����������*
dtype0*
shape:����������
�
StatefulPartitionedCallStatefulPartitionedCall+serving_default_fmn2_isomap_5000_1000_input(serving_default_fmn2_lle_5000_1000_input(serving_default_fmn2_pca_5000_1000_input)serving_default_fmn2_tsvd_5000_1000_inputfmn2_lle_5000_1000_1/kernelfmn2_lle_5000_1000_1/biasfmn2_isomap_5000_1000_1/kernelfmn2_isomap_5000_1000_1/biasfmn2_tsvd_5000_1000_1/kernelfmn2_tsvd_5000_1000_1/biasfmn2_pca_5000_1000_1/kernelfmn2_pca_5000_1000_1/biasfmn2_lle_5000_1000_2/kernelfmn2_lle_5000_1000_2/biasfmn2_isomap_5000_1000_2/kernelfmn2_isomap_5000_1000_2/biasfmn2_tsvd_5000_1000_2/kernelfmn2_tsvd_5000_1000_2/biasfmn2_pca_5000_1000_2/kernelfmn2_pca_5000_1000_2/biasfmn2_lle_5000_1000_3/kernelfmn2_lle_5000_1000_3/biasfmn2_isomap_5000_1000_3/kernelfmn2_isomap_5000_1000_3/biasfmn2_tsvd_5000_1000_3/kernelfmn2_tsvd_5000_1000_3/biasfmn2_pca_5000_1000_3/kernelfmn2_pca_5000_1000_3/biasfmn2_pca_5000_1000_out/kernelfmn2_pca_5000_1000_out/biasfmn2_tsvd_5000_1000_out/kernelfmn2_tsvd_5000_1000_out/bias fmn2_isomap_5000_1000_out/kernelfmn2_isomap_5000_1000_out/biasfmn2_lle_5000_1000_out/kernelfmn2_lle_5000_1000_out/biasjoined/kerneljoined/bias*1
Tin*
(2&*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*D
_read_only_resource_inputs&
$"	
 !"#$%*0
config_proto 

CPU

GPU2*0J 8� *.
f)R'
%__inference_signature_wrapper_1731223
O
saver_filenamePlaceholder*
_output_shapes
: *
dtype0*
shape: 
�
StatefulPartitionedCall_1StatefulPartitionedCallsaver_filename/fmn2_pca_5000_1000_1/kernel/Read/ReadVariableOp-fmn2_pca_5000_1000_1/bias/Read/ReadVariableOp0fmn2_tsvd_5000_1000_1/kernel/Read/ReadVariableOp.fmn2_tsvd_5000_1000_1/bias/Read/ReadVariableOp2fmn2_isomap_5000_1000_1/kernel/Read/ReadVariableOp0fmn2_isomap_5000_1000_1/bias/Read/ReadVariableOp/fmn2_lle_5000_1000_1/kernel/Read/ReadVariableOp-fmn2_lle_5000_1000_1/bias/Read/ReadVariableOp/fmn2_pca_5000_1000_2/kernel/Read/ReadVariableOp-fmn2_pca_5000_1000_2/bias/Read/ReadVariableOp0fmn2_tsvd_5000_1000_2/kernel/Read/ReadVariableOp.fmn2_tsvd_5000_1000_2/bias/Read/ReadVariableOp2fmn2_isomap_5000_1000_2/kernel/Read/ReadVariableOp0fmn2_isomap_5000_1000_2/bias/Read/ReadVariableOp/fmn2_lle_5000_1000_2/kernel/Read/ReadVariableOp-fmn2_lle_5000_1000_2/bias/Read/ReadVariableOp/fmn2_pca_5000_1000_3/kernel/Read/ReadVariableOp-fmn2_pca_5000_1000_3/bias/Read/ReadVariableOp0fmn2_tsvd_5000_1000_3/kernel/Read/ReadVariableOp.fmn2_tsvd_5000_1000_3/bias/Read/ReadVariableOp2fmn2_isomap_5000_1000_3/kernel/Read/ReadVariableOp0fmn2_isomap_5000_1000_3/bias/Read/ReadVariableOp/fmn2_lle_5000_1000_3/kernel/Read/ReadVariableOp-fmn2_lle_5000_1000_3/bias/Read/ReadVariableOp1fmn2_pca_5000_1000_out/kernel/Read/ReadVariableOp/fmn2_pca_5000_1000_out/bias/Read/ReadVariableOp2fmn2_tsvd_5000_1000_out/kernel/Read/ReadVariableOp0fmn2_tsvd_5000_1000_out/bias/Read/ReadVariableOp4fmn2_isomap_5000_1000_out/kernel/Read/ReadVariableOp2fmn2_isomap_5000_1000_out/bias/Read/ReadVariableOp1fmn2_lle_5000_1000_out/kernel/Read/ReadVariableOp/fmn2_lle_5000_1000_out/bias/Read/ReadVariableOp!joined/kernel/Read/ReadVariableOpjoined/bias/Read/ReadVariableOpAdam/iter/Read/ReadVariableOpAdam/beta_1/Read/ReadVariableOpAdam/beta_2/Read/ReadVariableOpAdam/decay/Read/ReadVariableOp&Adam/learning_rate/Read/ReadVariableOptotal/Read/ReadVariableOpcount/Read/ReadVariableOptotal_1/Read/ReadVariableOpcount_1/Read/ReadVariableOp(Adam/joined/kernel/m/Read/ReadVariableOp&Adam/joined/bias/m/Read/ReadVariableOp(Adam/joined/kernel/v/Read/ReadVariableOp&Adam/joined/bias/v/Read/ReadVariableOpConst*<
Tin5
321	*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *)
f$R"
 __inference__traced_save_1732155
�
StatefulPartitionedCall_2StatefulPartitionedCallsaver_filenamefmn2_pca_5000_1000_1/kernelfmn2_pca_5000_1000_1/biasfmn2_tsvd_5000_1000_1/kernelfmn2_tsvd_5000_1000_1/biasfmn2_isomap_5000_1000_1/kernelfmn2_isomap_5000_1000_1/biasfmn2_lle_5000_1000_1/kernelfmn2_lle_5000_1000_1/biasfmn2_pca_5000_1000_2/kernelfmn2_pca_5000_1000_2/biasfmn2_tsvd_5000_1000_2/kernelfmn2_tsvd_5000_1000_2/biasfmn2_isomap_5000_1000_2/kernelfmn2_isomap_5000_1000_2/biasfmn2_lle_5000_1000_2/kernelfmn2_lle_5000_1000_2/biasfmn2_pca_5000_1000_3/kernelfmn2_pca_5000_1000_3/biasfmn2_tsvd_5000_1000_3/kernelfmn2_tsvd_5000_1000_3/biasfmn2_isomap_5000_1000_3/kernelfmn2_isomap_5000_1000_3/biasfmn2_lle_5000_1000_3/kernelfmn2_lle_5000_1000_3/biasfmn2_pca_5000_1000_out/kernelfmn2_pca_5000_1000_out/biasfmn2_tsvd_5000_1000_out/kernelfmn2_tsvd_5000_1000_out/bias fmn2_isomap_5000_1000_out/kernelfmn2_isomap_5000_1000_out/biasfmn2_lle_5000_1000_out/kernelfmn2_lle_5000_1000_out/biasjoined/kerneljoined/bias	Adam/iterAdam/beta_1Adam/beta_2
Adam/decayAdam/learning_ratetotalcounttotal_1count_1Adam/joined/kernel/mAdam/joined/bias/mAdam/joined/kernel/vAdam/joined/bias/v*;
Tin4
220*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *,
f'R%
#__inference__traced_restore_1732306��
�
�
T__inference_fmn2_isomap_5000_1000_1_layer_call_and_return_conditional_losses_1731691

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
9__inference_fmn2_tsvd_5000_1000_out_layer_call_fn_1731900

inputs
unknown:	�
	unknown_0:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *]
fXRV
T__inference_fmn2_tsvd_5000_1000_out_layer_call_and_return_conditional_losses_17303132
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
T__inference_fmn2_isomap_5000_1000_1_layer_call_and_return_conditional_losses_1730109

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
6__inference_fmn2_lle_5000_1000_3_layer_call_fn_1731860

inputs
unknown:
��
	unknown_0:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_fmn2_lle_5000_1000_3_layer_call_and_return_conditional_losses_17302282
StatefulPartitionedCall|
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:����������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
K__inference_concatenate_51_layer_call_and_return_conditional_losses_1731968
inputs_0
inputs_1
inputs_2
inputs_3
identity\
concat/axisConst*
_output_shapes
: *
dtype0*
value	B :2
concat/axis�
concatConcatV2inputs_0inputs_1inputs_2inputs_3concat/axis:output:0*
N*
T0*'
_output_shapes
:���������2
concatc
IdentityIdentityconcat:output:0*
T0*'
_output_shapes
:���������2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*_
_input_shapesN
L:���������:���������:���������:���������:Q M
'
_output_shapes
:���������
"
_user_specified_name
inputs/0:QM
'
_output_shapes
:���������
"
_user_specified_name
inputs/1:QM
'
_output_shapes
:���������
"
_user_specified_name
inputs/2:QM
'
_output_shapes
:���������
"
_user_specified_name
inputs/3
�
�
S__inference_fmn2_lle_5000_1000_out_layer_call_and_return_conditional_losses_1730347

inputs1
matmul_readvariableop_resource:	�-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes
:	�*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2	
BiasAdda
SigmoidSigmoidBiasAdd:output:0*
T0*'
_output_shapes
:���������2	
Sigmoidf
IdentityIdentitySigmoid:y:0^NoOp*
T0*'
_output_shapes
:���������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
6__inference_fmn2_pca_5000_1000_3_layer_call_fn_1731800

inputs
unknown:
��
	unknown_0:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_fmn2_pca_5000_1000_3_layer_call_and_return_conditional_losses_17302792
StatefulPartitionedCall|
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:����������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
Q__inference_fmn2_pca_5000_1000_2_layer_call_and_return_conditional_losses_1730211

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
��
� 
B__inference_model_layer_call_and_return_conditional_losses_1731631
inputs_0
inputs_1
inputs_2
inputs_3G
3fmn2_lle_5000_1000_1_matmul_readvariableop_resource:
��C
4fmn2_lle_5000_1000_1_biasadd_readvariableop_resource:	�J
6fmn2_isomap_5000_1000_1_matmul_readvariableop_resource:
��F
7fmn2_isomap_5000_1000_1_biasadd_readvariableop_resource:	�H
4fmn2_tsvd_5000_1000_1_matmul_readvariableop_resource:
��D
5fmn2_tsvd_5000_1000_1_biasadd_readvariableop_resource:	�G
3fmn2_pca_5000_1000_1_matmul_readvariableop_resource:
��C
4fmn2_pca_5000_1000_1_biasadd_readvariableop_resource:	�G
3fmn2_lle_5000_1000_2_matmul_readvariableop_resource:
��C
4fmn2_lle_5000_1000_2_biasadd_readvariableop_resource:	�J
6fmn2_isomap_5000_1000_2_matmul_readvariableop_resource:
��F
7fmn2_isomap_5000_1000_2_biasadd_readvariableop_resource:	�H
4fmn2_tsvd_5000_1000_2_matmul_readvariableop_resource:
��D
5fmn2_tsvd_5000_1000_2_biasadd_readvariableop_resource:	�G
3fmn2_pca_5000_1000_2_matmul_readvariableop_resource:
��C
4fmn2_pca_5000_1000_2_biasadd_readvariableop_resource:	�G
3fmn2_lle_5000_1000_3_matmul_readvariableop_resource:
��C
4fmn2_lle_5000_1000_3_biasadd_readvariableop_resource:	�J
6fmn2_isomap_5000_1000_3_matmul_readvariableop_resource:
��F
7fmn2_isomap_5000_1000_3_biasadd_readvariableop_resource:	�H
4fmn2_tsvd_5000_1000_3_matmul_readvariableop_resource:
��D
5fmn2_tsvd_5000_1000_3_biasadd_readvariableop_resource:	�G
3fmn2_pca_5000_1000_3_matmul_readvariableop_resource:
��C
4fmn2_pca_5000_1000_3_biasadd_readvariableop_resource:	�H
5fmn2_pca_5000_1000_out_matmul_readvariableop_resource:	�D
6fmn2_pca_5000_1000_out_biasadd_readvariableop_resource:I
6fmn2_tsvd_5000_1000_out_matmul_readvariableop_resource:	�E
7fmn2_tsvd_5000_1000_out_biasadd_readvariableop_resource:K
8fmn2_isomap_5000_1000_out_matmul_readvariableop_resource:	�G
9fmn2_isomap_5000_1000_out_biasadd_readvariableop_resource:H
5fmn2_lle_5000_1000_out_matmul_readvariableop_resource:	�D
6fmn2_lle_5000_1000_out_biasadd_readvariableop_resource:7
%joined_matmul_readvariableop_resource:4
&joined_biasadd_readvariableop_resource:
identity��.fmn2_isomap_5000_1000_1/BiasAdd/ReadVariableOp�-fmn2_isomap_5000_1000_1/MatMul/ReadVariableOp�.fmn2_isomap_5000_1000_2/BiasAdd/ReadVariableOp�-fmn2_isomap_5000_1000_2/MatMul/ReadVariableOp�.fmn2_isomap_5000_1000_3/BiasAdd/ReadVariableOp�-fmn2_isomap_5000_1000_3/MatMul/ReadVariableOp�0fmn2_isomap_5000_1000_out/BiasAdd/ReadVariableOp�/fmn2_isomap_5000_1000_out/MatMul/ReadVariableOp�+fmn2_lle_5000_1000_1/BiasAdd/ReadVariableOp�*fmn2_lle_5000_1000_1/MatMul/ReadVariableOp�+fmn2_lle_5000_1000_2/BiasAdd/ReadVariableOp�*fmn2_lle_5000_1000_2/MatMul/ReadVariableOp�+fmn2_lle_5000_1000_3/BiasAdd/ReadVariableOp�*fmn2_lle_5000_1000_3/MatMul/ReadVariableOp�-fmn2_lle_5000_1000_out/BiasAdd/ReadVariableOp�,fmn2_lle_5000_1000_out/MatMul/ReadVariableOp�+fmn2_pca_5000_1000_1/BiasAdd/ReadVariableOp�*fmn2_pca_5000_1000_1/MatMul/ReadVariableOp�+fmn2_pca_5000_1000_2/BiasAdd/ReadVariableOp�*fmn2_pca_5000_1000_2/MatMul/ReadVariableOp�+fmn2_pca_5000_1000_3/BiasAdd/ReadVariableOp�*fmn2_pca_5000_1000_3/MatMul/ReadVariableOp�-fmn2_pca_5000_1000_out/BiasAdd/ReadVariableOp�,fmn2_pca_5000_1000_out/MatMul/ReadVariableOp�,fmn2_tsvd_5000_1000_1/BiasAdd/ReadVariableOp�+fmn2_tsvd_5000_1000_1/MatMul/ReadVariableOp�,fmn2_tsvd_5000_1000_2/BiasAdd/ReadVariableOp�+fmn2_tsvd_5000_1000_2/MatMul/ReadVariableOp�,fmn2_tsvd_5000_1000_3/BiasAdd/ReadVariableOp�+fmn2_tsvd_5000_1000_3/MatMul/ReadVariableOp�.fmn2_tsvd_5000_1000_out/BiasAdd/ReadVariableOp�-fmn2_tsvd_5000_1000_out/MatMul/ReadVariableOp�joined/BiasAdd/ReadVariableOp�joined/MatMul/ReadVariableOp�
*fmn2_lle_5000_1000_1/MatMul/ReadVariableOpReadVariableOp3fmn2_lle_5000_1000_1_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02,
*fmn2_lle_5000_1000_1/MatMul/ReadVariableOp�
fmn2_lle_5000_1000_1/MatMulMatMulinputs_32fmn2_lle_5000_1000_1/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn2_lle_5000_1000_1/MatMul�
+fmn2_lle_5000_1000_1/BiasAdd/ReadVariableOpReadVariableOp4fmn2_lle_5000_1000_1_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02-
+fmn2_lle_5000_1000_1/BiasAdd/ReadVariableOp�
fmn2_lle_5000_1000_1/BiasAddBiasAdd%fmn2_lle_5000_1000_1/MatMul:product:03fmn2_lle_5000_1000_1/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn2_lle_5000_1000_1/BiasAdd�
fmn2_lle_5000_1000_1/ReluRelu%fmn2_lle_5000_1000_1/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
fmn2_lle_5000_1000_1/Relu�
-fmn2_isomap_5000_1000_1/MatMul/ReadVariableOpReadVariableOp6fmn2_isomap_5000_1000_1_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02/
-fmn2_isomap_5000_1000_1/MatMul/ReadVariableOp�
fmn2_isomap_5000_1000_1/MatMulMatMulinputs_25fmn2_isomap_5000_1000_1/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2 
fmn2_isomap_5000_1000_1/MatMul�
.fmn2_isomap_5000_1000_1/BiasAdd/ReadVariableOpReadVariableOp7fmn2_isomap_5000_1000_1_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype020
.fmn2_isomap_5000_1000_1/BiasAdd/ReadVariableOp�
fmn2_isomap_5000_1000_1/BiasAddBiasAdd(fmn2_isomap_5000_1000_1/MatMul:product:06fmn2_isomap_5000_1000_1/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2!
fmn2_isomap_5000_1000_1/BiasAdd�
fmn2_isomap_5000_1000_1/ReluRelu(fmn2_isomap_5000_1000_1/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
fmn2_isomap_5000_1000_1/Relu�
+fmn2_tsvd_5000_1000_1/MatMul/ReadVariableOpReadVariableOp4fmn2_tsvd_5000_1000_1_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02-
+fmn2_tsvd_5000_1000_1/MatMul/ReadVariableOp�
fmn2_tsvd_5000_1000_1/MatMulMatMulinputs_13fmn2_tsvd_5000_1000_1/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn2_tsvd_5000_1000_1/MatMul�
,fmn2_tsvd_5000_1000_1/BiasAdd/ReadVariableOpReadVariableOp5fmn2_tsvd_5000_1000_1_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02.
,fmn2_tsvd_5000_1000_1/BiasAdd/ReadVariableOp�
fmn2_tsvd_5000_1000_1/BiasAddBiasAdd&fmn2_tsvd_5000_1000_1/MatMul:product:04fmn2_tsvd_5000_1000_1/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn2_tsvd_5000_1000_1/BiasAdd�
fmn2_tsvd_5000_1000_1/ReluRelu&fmn2_tsvd_5000_1000_1/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
fmn2_tsvd_5000_1000_1/Relu�
*fmn2_pca_5000_1000_1/MatMul/ReadVariableOpReadVariableOp3fmn2_pca_5000_1000_1_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02,
*fmn2_pca_5000_1000_1/MatMul/ReadVariableOp�
fmn2_pca_5000_1000_1/MatMulMatMulinputs_02fmn2_pca_5000_1000_1/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn2_pca_5000_1000_1/MatMul�
+fmn2_pca_5000_1000_1/BiasAdd/ReadVariableOpReadVariableOp4fmn2_pca_5000_1000_1_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02-
+fmn2_pca_5000_1000_1/BiasAdd/ReadVariableOp�
fmn2_pca_5000_1000_1/BiasAddBiasAdd%fmn2_pca_5000_1000_1/MatMul:product:03fmn2_pca_5000_1000_1/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn2_pca_5000_1000_1/BiasAdd�
fmn2_pca_5000_1000_1/ReluRelu%fmn2_pca_5000_1000_1/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
fmn2_pca_5000_1000_1/Relu�
*fmn2_lle_5000_1000_2/MatMul/ReadVariableOpReadVariableOp3fmn2_lle_5000_1000_2_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02,
*fmn2_lle_5000_1000_2/MatMul/ReadVariableOp�
fmn2_lle_5000_1000_2/MatMulMatMul'fmn2_lle_5000_1000_1/Relu:activations:02fmn2_lle_5000_1000_2/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn2_lle_5000_1000_2/MatMul�
+fmn2_lle_5000_1000_2/BiasAdd/ReadVariableOpReadVariableOp4fmn2_lle_5000_1000_2_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02-
+fmn2_lle_5000_1000_2/BiasAdd/ReadVariableOp�
fmn2_lle_5000_1000_2/BiasAddBiasAdd%fmn2_lle_5000_1000_2/MatMul:product:03fmn2_lle_5000_1000_2/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn2_lle_5000_1000_2/BiasAdd�
fmn2_lle_5000_1000_2/ReluRelu%fmn2_lle_5000_1000_2/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
fmn2_lle_5000_1000_2/Relu�
-fmn2_isomap_5000_1000_2/MatMul/ReadVariableOpReadVariableOp6fmn2_isomap_5000_1000_2_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02/
-fmn2_isomap_5000_1000_2/MatMul/ReadVariableOp�
fmn2_isomap_5000_1000_2/MatMulMatMul*fmn2_isomap_5000_1000_1/Relu:activations:05fmn2_isomap_5000_1000_2/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2 
fmn2_isomap_5000_1000_2/MatMul�
.fmn2_isomap_5000_1000_2/BiasAdd/ReadVariableOpReadVariableOp7fmn2_isomap_5000_1000_2_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype020
.fmn2_isomap_5000_1000_2/BiasAdd/ReadVariableOp�
fmn2_isomap_5000_1000_2/BiasAddBiasAdd(fmn2_isomap_5000_1000_2/MatMul:product:06fmn2_isomap_5000_1000_2/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2!
fmn2_isomap_5000_1000_2/BiasAdd�
fmn2_isomap_5000_1000_2/ReluRelu(fmn2_isomap_5000_1000_2/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
fmn2_isomap_5000_1000_2/Relu�
+fmn2_tsvd_5000_1000_2/MatMul/ReadVariableOpReadVariableOp4fmn2_tsvd_5000_1000_2_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02-
+fmn2_tsvd_5000_1000_2/MatMul/ReadVariableOp�
fmn2_tsvd_5000_1000_2/MatMulMatMul(fmn2_tsvd_5000_1000_1/Relu:activations:03fmn2_tsvd_5000_1000_2/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn2_tsvd_5000_1000_2/MatMul�
,fmn2_tsvd_5000_1000_2/BiasAdd/ReadVariableOpReadVariableOp5fmn2_tsvd_5000_1000_2_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02.
,fmn2_tsvd_5000_1000_2/BiasAdd/ReadVariableOp�
fmn2_tsvd_5000_1000_2/BiasAddBiasAdd&fmn2_tsvd_5000_1000_2/MatMul:product:04fmn2_tsvd_5000_1000_2/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn2_tsvd_5000_1000_2/BiasAdd�
fmn2_tsvd_5000_1000_2/ReluRelu&fmn2_tsvd_5000_1000_2/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
fmn2_tsvd_5000_1000_2/Relu�
*fmn2_pca_5000_1000_2/MatMul/ReadVariableOpReadVariableOp3fmn2_pca_5000_1000_2_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02,
*fmn2_pca_5000_1000_2/MatMul/ReadVariableOp�
fmn2_pca_5000_1000_2/MatMulMatMul'fmn2_pca_5000_1000_1/Relu:activations:02fmn2_pca_5000_1000_2/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn2_pca_5000_1000_2/MatMul�
+fmn2_pca_5000_1000_2/BiasAdd/ReadVariableOpReadVariableOp4fmn2_pca_5000_1000_2_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02-
+fmn2_pca_5000_1000_2/BiasAdd/ReadVariableOp�
fmn2_pca_5000_1000_2/BiasAddBiasAdd%fmn2_pca_5000_1000_2/MatMul:product:03fmn2_pca_5000_1000_2/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn2_pca_5000_1000_2/BiasAdd�
fmn2_pca_5000_1000_2/ReluRelu%fmn2_pca_5000_1000_2/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
fmn2_pca_5000_1000_2/Relu�
*fmn2_lle_5000_1000_3/MatMul/ReadVariableOpReadVariableOp3fmn2_lle_5000_1000_3_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02,
*fmn2_lle_5000_1000_3/MatMul/ReadVariableOp�
fmn2_lle_5000_1000_3/MatMulMatMul'fmn2_lle_5000_1000_2/Relu:activations:02fmn2_lle_5000_1000_3/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn2_lle_5000_1000_3/MatMul�
+fmn2_lle_5000_1000_3/BiasAdd/ReadVariableOpReadVariableOp4fmn2_lle_5000_1000_3_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02-
+fmn2_lle_5000_1000_3/BiasAdd/ReadVariableOp�
fmn2_lle_5000_1000_3/BiasAddBiasAdd%fmn2_lle_5000_1000_3/MatMul:product:03fmn2_lle_5000_1000_3/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn2_lle_5000_1000_3/BiasAdd�
fmn2_lle_5000_1000_3/ReluRelu%fmn2_lle_5000_1000_3/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
fmn2_lle_5000_1000_3/Relu�
-fmn2_isomap_5000_1000_3/MatMul/ReadVariableOpReadVariableOp6fmn2_isomap_5000_1000_3_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02/
-fmn2_isomap_5000_1000_3/MatMul/ReadVariableOp�
fmn2_isomap_5000_1000_3/MatMulMatMul*fmn2_isomap_5000_1000_2/Relu:activations:05fmn2_isomap_5000_1000_3/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2 
fmn2_isomap_5000_1000_3/MatMul�
.fmn2_isomap_5000_1000_3/BiasAdd/ReadVariableOpReadVariableOp7fmn2_isomap_5000_1000_3_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype020
.fmn2_isomap_5000_1000_3/BiasAdd/ReadVariableOp�
fmn2_isomap_5000_1000_3/BiasAddBiasAdd(fmn2_isomap_5000_1000_3/MatMul:product:06fmn2_isomap_5000_1000_3/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2!
fmn2_isomap_5000_1000_3/BiasAdd�
fmn2_isomap_5000_1000_3/ReluRelu(fmn2_isomap_5000_1000_3/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
fmn2_isomap_5000_1000_3/Relu�
+fmn2_tsvd_5000_1000_3/MatMul/ReadVariableOpReadVariableOp4fmn2_tsvd_5000_1000_3_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02-
+fmn2_tsvd_5000_1000_3/MatMul/ReadVariableOp�
fmn2_tsvd_5000_1000_3/MatMulMatMul(fmn2_tsvd_5000_1000_2/Relu:activations:03fmn2_tsvd_5000_1000_3/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn2_tsvd_5000_1000_3/MatMul�
,fmn2_tsvd_5000_1000_3/BiasAdd/ReadVariableOpReadVariableOp5fmn2_tsvd_5000_1000_3_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02.
,fmn2_tsvd_5000_1000_3/BiasAdd/ReadVariableOp�
fmn2_tsvd_5000_1000_3/BiasAddBiasAdd&fmn2_tsvd_5000_1000_3/MatMul:product:04fmn2_tsvd_5000_1000_3/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn2_tsvd_5000_1000_3/BiasAdd�
fmn2_tsvd_5000_1000_3/ReluRelu&fmn2_tsvd_5000_1000_3/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
fmn2_tsvd_5000_1000_3/Relu�
*fmn2_pca_5000_1000_3/MatMul/ReadVariableOpReadVariableOp3fmn2_pca_5000_1000_3_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02,
*fmn2_pca_5000_1000_3/MatMul/ReadVariableOp�
fmn2_pca_5000_1000_3/MatMulMatMul'fmn2_pca_5000_1000_2/Relu:activations:02fmn2_pca_5000_1000_3/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn2_pca_5000_1000_3/MatMul�
+fmn2_pca_5000_1000_3/BiasAdd/ReadVariableOpReadVariableOp4fmn2_pca_5000_1000_3_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02-
+fmn2_pca_5000_1000_3/BiasAdd/ReadVariableOp�
fmn2_pca_5000_1000_3/BiasAddBiasAdd%fmn2_pca_5000_1000_3/MatMul:product:03fmn2_pca_5000_1000_3/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn2_pca_5000_1000_3/BiasAdd�
fmn2_pca_5000_1000_3/ReluRelu%fmn2_pca_5000_1000_3/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
fmn2_pca_5000_1000_3/Relu�
,fmn2_pca_5000_1000_out/MatMul/ReadVariableOpReadVariableOp5fmn2_pca_5000_1000_out_matmul_readvariableop_resource*
_output_shapes
:	�*
dtype02.
,fmn2_pca_5000_1000_out/MatMul/ReadVariableOp�
fmn2_pca_5000_1000_out/MatMulMatMul'fmn2_pca_5000_1000_3/Relu:activations:04fmn2_pca_5000_1000_out/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
fmn2_pca_5000_1000_out/MatMul�
-fmn2_pca_5000_1000_out/BiasAdd/ReadVariableOpReadVariableOp6fmn2_pca_5000_1000_out_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02/
-fmn2_pca_5000_1000_out/BiasAdd/ReadVariableOp�
fmn2_pca_5000_1000_out/BiasAddBiasAdd'fmn2_pca_5000_1000_out/MatMul:product:05fmn2_pca_5000_1000_out/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2 
fmn2_pca_5000_1000_out/BiasAdd�
fmn2_pca_5000_1000_out/SigmoidSigmoid'fmn2_pca_5000_1000_out/BiasAdd:output:0*
T0*'
_output_shapes
:���������2 
fmn2_pca_5000_1000_out/Sigmoid�
-fmn2_tsvd_5000_1000_out/MatMul/ReadVariableOpReadVariableOp6fmn2_tsvd_5000_1000_out_matmul_readvariableop_resource*
_output_shapes
:	�*
dtype02/
-fmn2_tsvd_5000_1000_out/MatMul/ReadVariableOp�
fmn2_tsvd_5000_1000_out/MatMulMatMul(fmn2_tsvd_5000_1000_3/Relu:activations:05fmn2_tsvd_5000_1000_out/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2 
fmn2_tsvd_5000_1000_out/MatMul�
.fmn2_tsvd_5000_1000_out/BiasAdd/ReadVariableOpReadVariableOp7fmn2_tsvd_5000_1000_out_biasadd_readvariableop_resource*
_output_shapes
:*
dtype020
.fmn2_tsvd_5000_1000_out/BiasAdd/ReadVariableOp�
fmn2_tsvd_5000_1000_out/BiasAddBiasAdd(fmn2_tsvd_5000_1000_out/MatMul:product:06fmn2_tsvd_5000_1000_out/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2!
fmn2_tsvd_5000_1000_out/BiasAdd�
fmn2_tsvd_5000_1000_out/SigmoidSigmoid(fmn2_tsvd_5000_1000_out/BiasAdd:output:0*
T0*'
_output_shapes
:���������2!
fmn2_tsvd_5000_1000_out/Sigmoid�
/fmn2_isomap_5000_1000_out/MatMul/ReadVariableOpReadVariableOp8fmn2_isomap_5000_1000_out_matmul_readvariableop_resource*
_output_shapes
:	�*
dtype021
/fmn2_isomap_5000_1000_out/MatMul/ReadVariableOp�
 fmn2_isomap_5000_1000_out/MatMulMatMul*fmn2_isomap_5000_1000_3/Relu:activations:07fmn2_isomap_5000_1000_out/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2"
 fmn2_isomap_5000_1000_out/MatMul�
0fmn2_isomap_5000_1000_out/BiasAdd/ReadVariableOpReadVariableOp9fmn2_isomap_5000_1000_out_biasadd_readvariableop_resource*
_output_shapes
:*
dtype022
0fmn2_isomap_5000_1000_out/BiasAdd/ReadVariableOp�
!fmn2_isomap_5000_1000_out/BiasAddBiasAdd*fmn2_isomap_5000_1000_out/MatMul:product:08fmn2_isomap_5000_1000_out/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2#
!fmn2_isomap_5000_1000_out/BiasAdd�
!fmn2_isomap_5000_1000_out/SigmoidSigmoid*fmn2_isomap_5000_1000_out/BiasAdd:output:0*
T0*'
_output_shapes
:���������2#
!fmn2_isomap_5000_1000_out/Sigmoid�
,fmn2_lle_5000_1000_out/MatMul/ReadVariableOpReadVariableOp5fmn2_lle_5000_1000_out_matmul_readvariableop_resource*
_output_shapes
:	�*
dtype02.
,fmn2_lle_5000_1000_out/MatMul/ReadVariableOp�
fmn2_lle_5000_1000_out/MatMulMatMul'fmn2_lle_5000_1000_3/Relu:activations:04fmn2_lle_5000_1000_out/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
fmn2_lle_5000_1000_out/MatMul�
-fmn2_lle_5000_1000_out/BiasAdd/ReadVariableOpReadVariableOp6fmn2_lle_5000_1000_out_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02/
-fmn2_lle_5000_1000_out/BiasAdd/ReadVariableOp�
fmn2_lle_5000_1000_out/BiasAddBiasAdd'fmn2_lle_5000_1000_out/MatMul:product:05fmn2_lle_5000_1000_out/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2 
fmn2_lle_5000_1000_out/BiasAdd�
fmn2_lle_5000_1000_out/SigmoidSigmoid'fmn2_lle_5000_1000_out/BiasAdd:output:0*
T0*'
_output_shapes
:���������2 
fmn2_lle_5000_1000_out/Sigmoidz
concatenate_51/concat/axisConst*
_output_shapes
: *
dtype0*
value	B :2
concatenate_51/concat/axis�
concatenate_51/concatConcatV2"fmn2_pca_5000_1000_out/Sigmoid:y:0#fmn2_tsvd_5000_1000_out/Sigmoid:y:0%fmn2_isomap_5000_1000_out/Sigmoid:y:0"fmn2_lle_5000_1000_out/Sigmoid:y:0#concatenate_51/concat/axis:output:0*
N*
T0*'
_output_shapes
:���������2
concatenate_51/concat�
joined/MatMul/ReadVariableOpReadVariableOp%joined_matmul_readvariableop_resource*
_output_shapes

:*
dtype02
joined/MatMul/ReadVariableOp�
joined/MatMulMatMulconcatenate_51/concat:output:0$joined/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
joined/MatMul�
joined/BiasAdd/ReadVariableOpReadVariableOp&joined_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02
joined/BiasAdd/ReadVariableOp�
joined/BiasAddBiasAddjoined/MatMul:product:0%joined/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
joined/BiasAddv
joined/SigmoidSigmoidjoined/BiasAdd:output:0*
T0*'
_output_shapes
:���������2
joined/Sigmoidm
IdentityIdentityjoined/Sigmoid:y:0^NoOp*
T0*'
_output_shapes
:���������2

Identity�
NoOpNoOp/^fmn2_isomap_5000_1000_1/BiasAdd/ReadVariableOp.^fmn2_isomap_5000_1000_1/MatMul/ReadVariableOp/^fmn2_isomap_5000_1000_2/BiasAdd/ReadVariableOp.^fmn2_isomap_5000_1000_2/MatMul/ReadVariableOp/^fmn2_isomap_5000_1000_3/BiasAdd/ReadVariableOp.^fmn2_isomap_5000_1000_3/MatMul/ReadVariableOp1^fmn2_isomap_5000_1000_out/BiasAdd/ReadVariableOp0^fmn2_isomap_5000_1000_out/MatMul/ReadVariableOp,^fmn2_lle_5000_1000_1/BiasAdd/ReadVariableOp+^fmn2_lle_5000_1000_1/MatMul/ReadVariableOp,^fmn2_lle_5000_1000_2/BiasAdd/ReadVariableOp+^fmn2_lle_5000_1000_2/MatMul/ReadVariableOp,^fmn2_lle_5000_1000_3/BiasAdd/ReadVariableOp+^fmn2_lle_5000_1000_3/MatMul/ReadVariableOp.^fmn2_lle_5000_1000_out/BiasAdd/ReadVariableOp-^fmn2_lle_5000_1000_out/MatMul/ReadVariableOp,^fmn2_pca_5000_1000_1/BiasAdd/ReadVariableOp+^fmn2_pca_5000_1000_1/MatMul/ReadVariableOp,^fmn2_pca_5000_1000_2/BiasAdd/ReadVariableOp+^fmn2_pca_5000_1000_2/MatMul/ReadVariableOp,^fmn2_pca_5000_1000_3/BiasAdd/ReadVariableOp+^fmn2_pca_5000_1000_3/MatMul/ReadVariableOp.^fmn2_pca_5000_1000_out/BiasAdd/ReadVariableOp-^fmn2_pca_5000_1000_out/MatMul/ReadVariableOp-^fmn2_tsvd_5000_1000_1/BiasAdd/ReadVariableOp,^fmn2_tsvd_5000_1000_1/MatMul/ReadVariableOp-^fmn2_tsvd_5000_1000_2/BiasAdd/ReadVariableOp,^fmn2_tsvd_5000_1000_2/MatMul/ReadVariableOp-^fmn2_tsvd_5000_1000_3/BiasAdd/ReadVariableOp,^fmn2_tsvd_5000_1000_3/MatMul/ReadVariableOp/^fmn2_tsvd_5000_1000_out/BiasAdd/ReadVariableOp.^fmn2_tsvd_5000_1000_out/MatMul/ReadVariableOp^joined/BiasAdd/ReadVariableOp^joined/MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*�
_input_shapes�
�:����������:����������:����������:����������: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 2`
.fmn2_isomap_5000_1000_1/BiasAdd/ReadVariableOp.fmn2_isomap_5000_1000_1/BiasAdd/ReadVariableOp2^
-fmn2_isomap_5000_1000_1/MatMul/ReadVariableOp-fmn2_isomap_5000_1000_1/MatMul/ReadVariableOp2`
.fmn2_isomap_5000_1000_2/BiasAdd/ReadVariableOp.fmn2_isomap_5000_1000_2/BiasAdd/ReadVariableOp2^
-fmn2_isomap_5000_1000_2/MatMul/ReadVariableOp-fmn2_isomap_5000_1000_2/MatMul/ReadVariableOp2`
.fmn2_isomap_5000_1000_3/BiasAdd/ReadVariableOp.fmn2_isomap_5000_1000_3/BiasAdd/ReadVariableOp2^
-fmn2_isomap_5000_1000_3/MatMul/ReadVariableOp-fmn2_isomap_5000_1000_3/MatMul/ReadVariableOp2d
0fmn2_isomap_5000_1000_out/BiasAdd/ReadVariableOp0fmn2_isomap_5000_1000_out/BiasAdd/ReadVariableOp2b
/fmn2_isomap_5000_1000_out/MatMul/ReadVariableOp/fmn2_isomap_5000_1000_out/MatMul/ReadVariableOp2Z
+fmn2_lle_5000_1000_1/BiasAdd/ReadVariableOp+fmn2_lle_5000_1000_1/BiasAdd/ReadVariableOp2X
*fmn2_lle_5000_1000_1/MatMul/ReadVariableOp*fmn2_lle_5000_1000_1/MatMul/ReadVariableOp2Z
+fmn2_lle_5000_1000_2/BiasAdd/ReadVariableOp+fmn2_lle_5000_1000_2/BiasAdd/ReadVariableOp2X
*fmn2_lle_5000_1000_2/MatMul/ReadVariableOp*fmn2_lle_5000_1000_2/MatMul/ReadVariableOp2Z
+fmn2_lle_5000_1000_3/BiasAdd/ReadVariableOp+fmn2_lle_5000_1000_3/BiasAdd/ReadVariableOp2X
*fmn2_lle_5000_1000_3/MatMul/ReadVariableOp*fmn2_lle_5000_1000_3/MatMul/ReadVariableOp2^
-fmn2_lle_5000_1000_out/BiasAdd/ReadVariableOp-fmn2_lle_5000_1000_out/BiasAdd/ReadVariableOp2\
,fmn2_lle_5000_1000_out/MatMul/ReadVariableOp,fmn2_lle_5000_1000_out/MatMul/ReadVariableOp2Z
+fmn2_pca_5000_1000_1/BiasAdd/ReadVariableOp+fmn2_pca_5000_1000_1/BiasAdd/ReadVariableOp2X
*fmn2_pca_5000_1000_1/MatMul/ReadVariableOp*fmn2_pca_5000_1000_1/MatMul/ReadVariableOp2Z
+fmn2_pca_5000_1000_2/BiasAdd/ReadVariableOp+fmn2_pca_5000_1000_2/BiasAdd/ReadVariableOp2X
*fmn2_pca_5000_1000_2/MatMul/ReadVariableOp*fmn2_pca_5000_1000_2/MatMul/ReadVariableOp2Z
+fmn2_pca_5000_1000_3/BiasAdd/ReadVariableOp+fmn2_pca_5000_1000_3/BiasAdd/ReadVariableOp2X
*fmn2_pca_5000_1000_3/MatMul/ReadVariableOp*fmn2_pca_5000_1000_3/MatMul/ReadVariableOp2^
-fmn2_pca_5000_1000_out/BiasAdd/ReadVariableOp-fmn2_pca_5000_1000_out/BiasAdd/ReadVariableOp2\
,fmn2_pca_5000_1000_out/MatMul/ReadVariableOp,fmn2_pca_5000_1000_out/MatMul/ReadVariableOp2\
,fmn2_tsvd_5000_1000_1/BiasAdd/ReadVariableOp,fmn2_tsvd_5000_1000_1/BiasAdd/ReadVariableOp2Z
+fmn2_tsvd_5000_1000_1/MatMul/ReadVariableOp+fmn2_tsvd_5000_1000_1/MatMul/ReadVariableOp2\
,fmn2_tsvd_5000_1000_2/BiasAdd/ReadVariableOp,fmn2_tsvd_5000_1000_2/BiasAdd/ReadVariableOp2Z
+fmn2_tsvd_5000_1000_2/MatMul/ReadVariableOp+fmn2_tsvd_5000_1000_2/MatMul/ReadVariableOp2\
,fmn2_tsvd_5000_1000_3/BiasAdd/ReadVariableOp,fmn2_tsvd_5000_1000_3/BiasAdd/ReadVariableOp2Z
+fmn2_tsvd_5000_1000_3/MatMul/ReadVariableOp+fmn2_tsvd_5000_1000_3/MatMul/ReadVariableOp2`
.fmn2_tsvd_5000_1000_out/BiasAdd/ReadVariableOp.fmn2_tsvd_5000_1000_out/BiasAdd/ReadVariableOp2^
-fmn2_tsvd_5000_1000_out/MatMul/ReadVariableOp-fmn2_tsvd_5000_1000_out/MatMul/ReadVariableOp2>
joined/BiasAdd/ReadVariableOpjoined/BiasAdd/ReadVariableOp2<
joined/MatMul/ReadVariableOpjoined/MatMul/ReadVariableOp:R N
(
_output_shapes
:����������
"
_user_specified_name
inputs/0:RN
(
_output_shapes
:����������
"
_user_specified_name
inputs/1:RN
(
_output_shapes
:����������
"
_user_specified_name
inputs/2:RN
(
_output_shapes
:����������
"
_user_specified_name
inputs/3
�
�
S__inference_fmn2_lle_5000_1000_out_layer_call_and_return_conditional_losses_1731951

inputs1
matmul_readvariableop_resource:	�-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes
:	�*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2	
BiasAdda
SigmoidSigmoidBiasAdd:output:0*
T0*'
_output_shapes
:���������2	
Sigmoidf
IdentityIdentitySigmoid:y:0^NoOp*
T0*'
_output_shapes
:���������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
(__inference_joined_layer_call_fn_1731977

inputs
unknown:
	unknown_0:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *L
fGRE
C__inference_joined_layer_call_and_return_conditional_losses_17303752
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������: : 22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs
�
�
9__inference_fmn2_isomap_5000_1000_3_layer_call_fn_1731840

inputs
unknown:
��
	unknown_0:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *]
fXRV
T__inference_fmn2_isomap_5000_1000_3_layer_call_and_return_conditional_losses_17302452
StatefulPartitionedCall|
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:����������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
��
�#
"__inference__wrapped_model_1730068
fmn2_pca_5000_1000_input
fmn2_tsvd_5000_1000_input
fmn2_isomap_5000_1000_input
fmn2_lle_5000_1000_inputM
9model_fmn2_lle_5000_1000_1_matmul_readvariableop_resource:
��I
:model_fmn2_lle_5000_1000_1_biasadd_readvariableop_resource:	�P
<model_fmn2_isomap_5000_1000_1_matmul_readvariableop_resource:
��L
=model_fmn2_isomap_5000_1000_1_biasadd_readvariableop_resource:	�N
:model_fmn2_tsvd_5000_1000_1_matmul_readvariableop_resource:
��J
;model_fmn2_tsvd_5000_1000_1_biasadd_readvariableop_resource:	�M
9model_fmn2_pca_5000_1000_1_matmul_readvariableop_resource:
��I
:model_fmn2_pca_5000_1000_1_biasadd_readvariableop_resource:	�M
9model_fmn2_lle_5000_1000_2_matmul_readvariableop_resource:
��I
:model_fmn2_lle_5000_1000_2_biasadd_readvariableop_resource:	�P
<model_fmn2_isomap_5000_1000_2_matmul_readvariableop_resource:
��L
=model_fmn2_isomap_5000_1000_2_biasadd_readvariableop_resource:	�N
:model_fmn2_tsvd_5000_1000_2_matmul_readvariableop_resource:
��J
;model_fmn2_tsvd_5000_1000_2_biasadd_readvariableop_resource:	�M
9model_fmn2_pca_5000_1000_2_matmul_readvariableop_resource:
��I
:model_fmn2_pca_5000_1000_2_biasadd_readvariableop_resource:	�M
9model_fmn2_lle_5000_1000_3_matmul_readvariableop_resource:
��I
:model_fmn2_lle_5000_1000_3_biasadd_readvariableop_resource:	�P
<model_fmn2_isomap_5000_1000_3_matmul_readvariableop_resource:
��L
=model_fmn2_isomap_5000_1000_3_biasadd_readvariableop_resource:	�N
:model_fmn2_tsvd_5000_1000_3_matmul_readvariableop_resource:
��J
;model_fmn2_tsvd_5000_1000_3_biasadd_readvariableop_resource:	�M
9model_fmn2_pca_5000_1000_3_matmul_readvariableop_resource:
��I
:model_fmn2_pca_5000_1000_3_biasadd_readvariableop_resource:	�N
;model_fmn2_pca_5000_1000_out_matmul_readvariableop_resource:	�J
<model_fmn2_pca_5000_1000_out_biasadd_readvariableop_resource:O
<model_fmn2_tsvd_5000_1000_out_matmul_readvariableop_resource:	�K
=model_fmn2_tsvd_5000_1000_out_biasadd_readvariableop_resource:Q
>model_fmn2_isomap_5000_1000_out_matmul_readvariableop_resource:	�M
?model_fmn2_isomap_5000_1000_out_biasadd_readvariableop_resource:N
;model_fmn2_lle_5000_1000_out_matmul_readvariableop_resource:	�J
<model_fmn2_lle_5000_1000_out_biasadd_readvariableop_resource:=
+model_joined_matmul_readvariableop_resource::
,model_joined_biasadd_readvariableop_resource:
identity��4model/fmn2_isomap_5000_1000_1/BiasAdd/ReadVariableOp�3model/fmn2_isomap_5000_1000_1/MatMul/ReadVariableOp�4model/fmn2_isomap_5000_1000_2/BiasAdd/ReadVariableOp�3model/fmn2_isomap_5000_1000_2/MatMul/ReadVariableOp�4model/fmn2_isomap_5000_1000_3/BiasAdd/ReadVariableOp�3model/fmn2_isomap_5000_1000_3/MatMul/ReadVariableOp�6model/fmn2_isomap_5000_1000_out/BiasAdd/ReadVariableOp�5model/fmn2_isomap_5000_1000_out/MatMul/ReadVariableOp�1model/fmn2_lle_5000_1000_1/BiasAdd/ReadVariableOp�0model/fmn2_lle_5000_1000_1/MatMul/ReadVariableOp�1model/fmn2_lle_5000_1000_2/BiasAdd/ReadVariableOp�0model/fmn2_lle_5000_1000_2/MatMul/ReadVariableOp�1model/fmn2_lle_5000_1000_3/BiasAdd/ReadVariableOp�0model/fmn2_lle_5000_1000_3/MatMul/ReadVariableOp�3model/fmn2_lle_5000_1000_out/BiasAdd/ReadVariableOp�2model/fmn2_lle_5000_1000_out/MatMul/ReadVariableOp�1model/fmn2_pca_5000_1000_1/BiasAdd/ReadVariableOp�0model/fmn2_pca_5000_1000_1/MatMul/ReadVariableOp�1model/fmn2_pca_5000_1000_2/BiasAdd/ReadVariableOp�0model/fmn2_pca_5000_1000_2/MatMul/ReadVariableOp�1model/fmn2_pca_5000_1000_3/BiasAdd/ReadVariableOp�0model/fmn2_pca_5000_1000_3/MatMul/ReadVariableOp�3model/fmn2_pca_5000_1000_out/BiasAdd/ReadVariableOp�2model/fmn2_pca_5000_1000_out/MatMul/ReadVariableOp�2model/fmn2_tsvd_5000_1000_1/BiasAdd/ReadVariableOp�1model/fmn2_tsvd_5000_1000_1/MatMul/ReadVariableOp�2model/fmn2_tsvd_5000_1000_2/BiasAdd/ReadVariableOp�1model/fmn2_tsvd_5000_1000_2/MatMul/ReadVariableOp�2model/fmn2_tsvd_5000_1000_3/BiasAdd/ReadVariableOp�1model/fmn2_tsvd_5000_1000_3/MatMul/ReadVariableOp�4model/fmn2_tsvd_5000_1000_out/BiasAdd/ReadVariableOp�3model/fmn2_tsvd_5000_1000_out/MatMul/ReadVariableOp�#model/joined/BiasAdd/ReadVariableOp�"model/joined/MatMul/ReadVariableOp�
0model/fmn2_lle_5000_1000_1/MatMul/ReadVariableOpReadVariableOp9model_fmn2_lle_5000_1000_1_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype022
0model/fmn2_lle_5000_1000_1/MatMul/ReadVariableOp�
!model/fmn2_lle_5000_1000_1/MatMulMatMulfmn2_lle_5000_1000_input8model/fmn2_lle_5000_1000_1/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2#
!model/fmn2_lle_5000_1000_1/MatMul�
1model/fmn2_lle_5000_1000_1/BiasAdd/ReadVariableOpReadVariableOp:model_fmn2_lle_5000_1000_1_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype023
1model/fmn2_lle_5000_1000_1/BiasAdd/ReadVariableOp�
"model/fmn2_lle_5000_1000_1/BiasAddBiasAdd+model/fmn2_lle_5000_1000_1/MatMul:product:09model/fmn2_lle_5000_1000_1/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2$
"model/fmn2_lle_5000_1000_1/BiasAdd�
model/fmn2_lle_5000_1000_1/ReluRelu+model/fmn2_lle_5000_1000_1/BiasAdd:output:0*
T0*(
_output_shapes
:����������2!
model/fmn2_lle_5000_1000_1/Relu�
3model/fmn2_isomap_5000_1000_1/MatMul/ReadVariableOpReadVariableOp<model_fmn2_isomap_5000_1000_1_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype025
3model/fmn2_isomap_5000_1000_1/MatMul/ReadVariableOp�
$model/fmn2_isomap_5000_1000_1/MatMulMatMulfmn2_isomap_5000_1000_input;model/fmn2_isomap_5000_1000_1/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2&
$model/fmn2_isomap_5000_1000_1/MatMul�
4model/fmn2_isomap_5000_1000_1/BiasAdd/ReadVariableOpReadVariableOp=model_fmn2_isomap_5000_1000_1_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype026
4model/fmn2_isomap_5000_1000_1/BiasAdd/ReadVariableOp�
%model/fmn2_isomap_5000_1000_1/BiasAddBiasAdd.model/fmn2_isomap_5000_1000_1/MatMul:product:0<model/fmn2_isomap_5000_1000_1/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2'
%model/fmn2_isomap_5000_1000_1/BiasAdd�
"model/fmn2_isomap_5000_1000_1/ReluRelu.model/fmn2_isomap_5000_1000_1/BiasAdd:output:0*
T0*(
_output_shapes
:����������2$
"model/fmn2_isomap_5000_1000_1/Relu�
1model/fmn2_tsvd_5000_1000_1/MatMul/ReadVariableOpReadVariableOp:model_fmn2_tsvd_5000_1000_1_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype023
1model/fmn2_tsvd_5000_1000_1/MatMul/ReadVariableOp�
"model/fmn2_tsvd_5000_1000_1/MatMulMatMulfmn2_tsvd_5000_1000_input9model/fmn2_tsvd_5000_1000_1/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2$
"model/fmn2_tsvd_5000_1000_1/MatMul�
2model/fmn2_tsvd_5000_1000_1/BiasAdd/ReadVariableOpReadVariableOp;model_fmn2_tsvd_5000_1000_1_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype024
2model/fmn2_tsvd_5000_1000_1/BiasAdd/ReadVariableOp�
#model/fmn2_tsvd_5000_1000_1/BiasAddBiasAdd,model/fmn2_tsvd_5000_1000_1/MatMul:product:0:model/fmn2_tsvd_5000_1000_1/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2%
#model/fmn2_tsvd_5000_1000_1/BiasAdd�
 model/fmn2_tsvd_5000_1000_1/ReluRelu,model/fmn2_tsvd_5000_1000_1/BiasAdd:output:0*
T0*(
_output_shapes
:����������2"
 model/fmn2_tsvd_5000_1000_1/Relu�
0model/fmn2_pca_5000_1000_1/MatMul/ReadVariableOpReadVariableOp9model_fmn2_pca_5000_1000_1_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype022
0model/fmn2_pca_5000_1000_1/MatMul/ReadVariableOp�
!model/fmn2_pca_5000_1000_1/MatMulMatMulfmn2_pca_5000_1000_input8model/fmn2_pca_5000_1000_1/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2#
!model/fmn2_pca_5000_1000_1/MatMul�
1model/fmn2_pca_5000_1000_1/BiasAdd/ReadVariableOpReadVariableOp:model_fmn2_pca_5000_1000_1_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype023
1model/fmn2_pca_5000_1000_1/BiasAdd/ReadVariableOp�
"model/fmn2_pca_5000_1000_1/BiasAddBiasAdd+model/fmn2_pca_5000_1000_1/MatMul:product:09model/fmn2_pca_5000_1000_1/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2$
"model/fmn2_pca_5000_1000_1/BiasAdd�
model/fmn2_pca_5000_1000_1/ReluRelu+model/fmn2_pca_5000_1000_1/BiasAdd:output:0*
T0*(
_output_shapes
:����������2!
model/fmn2_pca_5000_1000_1/Relu�
0model/fmn2_lle_5000_1000_2/MatMul/ReadVariableOpReadVariableOp9model_fmn2_lle_5000_1000_2_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype022
0model/fmn2_lle_5000_1000_2/MatMul/ReadVariableOp�
!model/fmn2_lle_5000_1000_2/MatMulMatMul-model/fmn2_lle_5000_1000_1/Relu:activations:08model/fmn2_lle_5000_1000_2/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2#
!model/fmn2_lle_5000_1000_2/MatMul�
1model/fmn2_lle_5000_1000_2/BiasAdd/ReadVariableOpReadVariableOp:model_fmn2_lle_5000_1000_2_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype023
1model/fmn2_lle_5000_1000_2/BiasAdd/ReadVariableOp�
"model/fmn2_lle_5000_1000_2/BiasAddBiasAdd+model/fmn2_lle_5000_1000_2/MatMul:product:09model/fmn2_lle_5000_1000_2/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2$
"model/fmn2_lle_5000_1000_2/BiasAdd�
model/fmn2_lle_5000_1000_2/ReluRelu+model/fmn2_lle_5000_1000_2/BiasAdd:output:0*
T0*(
_output_shapes
:����������2!
model/fmn2_lle_5000_1000_2/Relu�
3model/fmn2_isomap_5000_1000_2/MatMul/ReadVariableOpReadVariableOp<model_fmn2_isomap_5000_1000_2_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype025
3model/fmn2_isomap_5000_1000_2/MatMul/ReadVariableOp�
$model/fmn2_isomap_5000_1000_2/MatMulMatMul0model/fmn2_isomap_5000_1000_1/Relu:activations:0;model/fmn2_isomap_5000_1000_2/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2&
$model/fmn2_isomap_5000_1000_2/MatMul�
4model/fmn2_isomap_5000_1000_2/BiasAdd/ReadVariableOpReadVariableOp=model_fmn2_isomap_5000_1000_2_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype026
4model/fmn2_isomap_5000_1000_2/BiasAdd/ReadVariableOp�
%model/fmn2_isomap_5000_1000_2/BiasAddBiasAdd.model/fmn2_isomap_5000_1000_2/MatMul:product:0<model/fmn2_isomap_5000_1000_2/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2'
%model/fmn2_isomap_5000_1000_2/BiasAdd�
"model/fmn2_isomap_5000_1000_2/ReluRelu.model/fmn2_isomap_5000_1000_2/BiasAdd:output:0*
T0*(
_output_shapes
:����������2$
"model/fmn2_isomap_5000_1000_2/Relu�
1model/fmn2_tsvd_5000_1000_2/MatMul/ReadVariableOpReadVariableOp:model_fmn2_tsvd_5000_1000_2_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype023
1model/fmn2_tsvd_5000_1000_2/MatMul/ReadVariableOp�
"model/fmn2_tsvd_5000_1000_2/MatMulMatMul.model/fmn2_tsvd_5000_1000_1/Relu:activations:09model/fmn2_tsvd_5000_1000_2/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2$
"model/fmn2_tsvd_5000_1000_2/MatMul�
2model/fmn2_tsvd_5000_1000_2/BiasAdd/ReadVariableOpReadVariableOp;model_fmn2_tsvd_5000_1000_2_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype024
2model/fmn2_tsvd_5000_1000_2/BiasAdd/ReadVariableOp�
#model/fmn2_tsvd_5000_1000_2/BiasAddBiasAdd,model/fmn2_tsvd_5000_1000_2/MatMul:product:0:model/fmn2_tsvd_5000_1000_2/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2%
#model/fmn2_tsvd_5000_1000_2/BiasAdd�
 model/fmn2_tsvd_5000_1000_2/ReluRelu,model/fmn2_tsvd_5000_1000_2/BiasAdd:output:0*
T0*(
_output_shapes
:����������2"
 model/fmn2_tsvd_5000_1000_2/Relu�
0model/fmn2_pca_5000_1000_2/MatMul/ReadVariableOpReadVariableOp9model_fmn2_pca_5000_1000_2_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype022
0model/fmn2_pca_5000_1000_2/MatMul/ReadVariableOp�
!model/fmn2_pca_5000_1000_2/MatMulMatMul-model/fmn2_pca_5000_1000_1/Relu:activations:08model/fmn2_pca_5000_1000_2/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2#
!model/fmn2_pca_5000_1000_2/MatMul�
1model/fmn2_pca_5000_1000_2/BiasAdd/ReadVariableOpReadVariableOp:model_fmn2_pca_5000_1000_2_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype023
1model/fmn2_pca_5000_1000_2/BiasAdd/ReadVariableOp�
"model/fmn2_pca_5000_1000_2/BiasAddBiasAdd+model/fmn2_pca_5000_1000_2/MatMul:product:09model/fmn2_pca_5000_1000_2/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2$
"model/fmn2_pca_5000_1000_2/BiasAdd�
model/fmn2_pca_5000_1000_2/ReluRelu+model/fmn2_pca_5000_1000_2/BiasAdd:output:0*
T0*(
_output_shapes
:����������2!
model/fmn2_pca_5000_1000_2/Relu�
0model/fmn2_lle_5000_1000_3/MatMul/ReadVariableOpReadVariableOp9model_fmn2_lle_5000_1000_3_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype022
0model/fmn2_lle_5000_1000_3/MatMul/ReadVariableOp�
!model/fmn2_lle_5000_1000_3/MatMulMatMul-model/fmn2_lle_5000_1000_2/Relu:activations:08model/fmn2_lle_5000_1000_3/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2#
!model/fmn2_lle_5000_1000_3/MatMul�
1model/fmn2_lle_5000_1000_3/BiasAdd/ReadVariableOpReadVariableOp:model_fmn2_lle_5000_1000_3_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype023
1model/fmn2_lle_5000_1000_3/BiasAdd/ReadVariableOp�
"model/fmn2_lle_5000_1000_3/BiasAddBiasAdd+model/fmn2_lle_5000_1000_3/MatMul:product:09model/fmn2_lle_5000_1000_3/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2$
"model/fmn2_lle_5000_1000_3/BiasAdd�
model/fmn2_lle_5000_1000_3/ReluRelu+model/fmn2_lle_5000_1000_3/BiasAdd:output:0*
T0*(
_output_shapes
:����������2!
model/fmn2_lle_5000_1000_3/Relu�
3model/fmn2_isomap_5000_1000_3/MatMul/ReadVariableOpReadVariableOp<model_fmn2_isomap_5000_1000_3_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype025
3model/fmn2_isomap_5000_1000_3/MatMul/ReadVariableOp�
$model/fmn2_isomap_5000_1000_3/MatMulMatMul0model/fmn2_isomap_5000_1000_2/Relu:activations:0;model/fmn2_isomap_5000_1000_3/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2&
$model/fmn2_isomap_5000_1000_3/MatMul�
4model/fmn2_isomap_5000_1000_3/BiasAdd/ReadVariableOpReadVariableOp=model_fmn2_isomap_5000_1000_3_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype026
4model/fmn2_isomap_5000_1000_3/BiasAdd/ReadVariableOp�
%model/fmn2_isomap_5000_1000_3/BiasAddBiasAdd.model/fmn2_isomap_5000_1000_3/MatMul:product:0<model/fmn2_isomap_5000_1000_3/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2'
%model/fmn2_isomap_5000_1000_3/BiasAdd�
"model/fmn2_isomap_5000_1000_3/ReluRelu.model/fmn2_isomap_5000_1000_3/BiasAdd:output:0*
T0*(
_output_shapes
:����������2$
"model/fmn2_isomap_5000_1000_3/Relu�
1model/fmn2_tsvd_5000_1000_3/MatMul/ReadVariableOpReadVariableOp:model_fmn2_tsvd_5000_1000_3_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype023
1model/fmn2_tsvd_5000_1000_3/MatMul/ReadVariableOp�
"model/fmn2_tsvd_5000_1000_3/MatMulMatMul.model/fmn2_tsvd_5000_1000_2/Relu:activations:09model/fmn2_tsvd_5000_1000_3/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2$
"model/fmn2_tsvd_5000_1000_3/MatMul�
2model/fmn2_tsvd_5000_1000_3/BiasAdd/ReadVariableOpReadVariableOp;model_fmn2_tsvd_5000_1000_3_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype024
2model/fmn2_tsvd_5000_1000_3/BiasAdd/ReadVariableOp�
#model/fmn2_tsvd_5000_1000_3/BiasAddBiasAdd,model/fmn2_tsvd_5000_1000_3/MatMul:product:0:model/fmn2_tsvd_5000_1000_3/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2%
#model/fmn2_tsvd_5000_1000_3/BiasAdd�
 model/fmn2_tsvd_5000_1000_3/ReluRelu,model/fmn2_tsvd_5000_1000_3/BiasAdd:output:0*
T0*(
_output_shapes
:����������2"
 model/fmn2_tsvd_5000_1000_3/Relu�
0model/fmn2_pca_5000_1000_3/MatMul/ReadVariableOpReadVariableOp9model_fmn2_pca_5000_1000_3_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype022
0model/fmn2_pca_5000_1000_3/MatMul/ReadVariableOp�
!model/fmn2_pca_5000_1000_3/MatMulMatMul-model/fmn2_pca_5000_1000_2/Relu:activations:08model/fmn2_pca_5000_1000_3/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2#
!model/fmn2_pca_5000_1000_3/MatMul�
1model/fmn2_pca_5000_1000_3/BiasAdd/ReadVariableOpReadVariableOp:model_fmn2_pca_5000_1000_3_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype023
1model/fmn2_pca_5000_1000_3/BiasAdd/ReadVariableOp�
"model/fmn2_pca_5000_1000_3/BiasAddBiasAdd+model/fmn2_pca_5000_1000_3/MatMul:product:09model/fmn2_pca_5000_1000_3/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2$
"model/fmn2_pca_5000_1000_3/BiasAdd�
model/fmn2_pca_5000_1000_3/ReluRelu+model/fmn2_pca_5000_1000_3/BiasAdd:output:0*
T0*(
_output_shapes
:����������2!
model/fmn2_pca_5000_1000_3/Relu�
2model/fmn2_pca_5000_1000_out/MatMul/ReadVariableOpReadVariableOp;model_fmn2_pca_5000_1000_out_matmul_readvariableop_resource*
_output_shapes
:	�*
dtype024
2model/fmn2_pca_5000_1000_out/MatMul/ReadVariableOp�
#model/fmn2_pca_5000_1000_out/MatMulMatMul-model/fmn2_pca_5000_1000_3/Relu:activations:0:model/fmn2_pca_5000_1000_out/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2%
#model/fmn2_pca_5000_1000_out/MatMul�
3model/fmn2_pca_5000_1000_out/BiasAdd/ReadVariableOpReadVariableOp<model_fmn2_pca_5000_1000_out_biasadd_readvariableop_resource*
_output_shapes
:*
dtype025
3model/fmn2_pca_5000_1000_out/BiasAdd/ReadVariableOp�
$model/fmn2_pca_5000_1000_out/BiasAddBiasAdd-model/fmn2_pca_5000_1000_out/MatMul:product:0;model/fmn2_pca_5000_1000_out/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2&
$model/fmn2_pca_5000_1000_out/BiasAdd�
$model/fmn2_pca_5000_1000_out/SigmoidSigmoid-model/fmn2_pca_5000_1000_out/BiasAdd:output:0*
T0*'
_output_shapes
:���������2&
$model/fmn2_pca_5000_1000_out/Sigmoid�
3model/fmn2_tsvd_5000_1000_out/MatMul/ReadVariableOpReadVariableOp<model_fmn2_tsvd_5000_1000_out_matmul_readvariableop_resource*
_output_shapes
:	�*
dtype025
3model/fmn2_tsvd_5000_1000_out/MatMul/ReadVariableOp�
$model/fmn2_tsvd_5000_1000_out/MatMulMatMul.model/fmn2_tsvd_5000_1000_3/Relu:activations:0;model/fmn2_tsvd_5000_1000_out/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2&
$model/fmn2_tsvd_5000_1000_out/MatMul�
4model/fmn2_tsvd_5000_1000_out/BiasAdd/ReadVariableOpReadVariableOp=model_fmn2_tsvd_5000_1000_out_biasadd_readvariableop_resource*
_output_shapes
:*
dtype026
4model/fmn2_tsvd_5000_1000_out/BiasAdd/ReadVariableOp�
%model/fmn2_tsvd_5000_1000_out/BiasAddBiasAdd.model/fmn2_tsvd_5000_1000_out/MatMul:product:0<model/fmn2_tsvd_5000_1000_out/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2'
%model/fmn2_tsvd_5000_1000_out/BiasAdd�
%model/fmn2_tsvd_5000_1000_out/SigmoidSigmoid.model/fmn2_tsvd_5000_1000_out/BiasAdd:output:0*
T0*'
_output_shapes
:���������2'
%model/fmn2_tsvd_5000_1000_out/Sigmoid�
5model/fmn2_isomap_5000_1000_out/MatMul/ReadVariableOpReadVariableOp>model_fmn2_isomap_5000_1000_out_matmul_readvariableop_resource*
_output_shapes
:	�*
dtype027
5model/fmn2_isomap_5000_1000_out/MatMul/ReadVariableOp�
&model/fmn2_isomap_5000_1000_out/MatMulMatMul0model/fmn2_isomap_5000_1000_3/Relu:activations:0=model/fmn2_isomap_5000_1000_out/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2(
&model/fmn2_isomap_5000_1000_out/MatMul�
6model/fmn2_isomap_5000_1000_out/BiasAdd/ReadVariableOpReadVariableOp?model_fmn2_isomap_5000_1000_out_biasadd_readvariableop_resource*
_output_shapes
:*
dtype028
6model/fmn2_isomap_5000_1000_out/BiasAdd/ReadVariableOp�
'model/fmn2_isomap_5000_1000_out/BiasAddBiasAdd0model/fmn2_isomap_5000_1000_out/MatMul:product:0>model/fmn2_isomap_5000_1000_out/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2)
'model/fmn2_isomap_5000_1000_out/BiasAdd�
'model/fmn2_isomap_5000_1000_out/SigmoidSigmoid0model/fmn2_isomap_5000_1000_out/BiasAdd:output:0*
T0*'
_output_shapes
:���������2)
'model/fmn2_isomap_5000_1000_out/Sigmoid�
2model/fmn2_lle_5000_1000_out/MatMul/ReadVariableOpReadVariableOp;model_fmn2_lle_5000_1000_out_matmul_readvariableop_resource*
_output_shapes
:	�*
dtype024
2model/fmn2_lle_5000_1000_out/MatMul/ReadVariableOp�
#model/fmn2_lle_5000_1000_out/MatMulMatMul-model/fmn2_lle_5000_1000_3/Relu:activations:0:model/fmn2_lle_5000_1000_out/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2%
#model/fmn2_lle_5000_1000_out/MatMul�
3model/fmn2_lle_5000_1000_out/BiasAdd/ReadVariableOpReadVariableOp<model_fmn2_lle_5000_1000_out_biasadd_readvariableop_resource*
_output_shapes
:*
dtype025
3model/fmn2_lle_5000_1000_out/BiasAdd/ReadVariableOp�
$model/fmn2_lle_5000_1000_out/BiasAddBiasAdd-model/fmn2_lle_5000_1000_out/MatMul:product:0;model/fmn2_lle_5000_1000_out/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2&
$model/fmn2_lle_5000_1000_out/BiasAdd�
$model/fmn2_lle_5000_1000_out/SigmoidSigmoid-model/fmn2_lle_5000_1000_out/BiasAdd:output:0*
T0*'
_output_shapes
:���������2&
$model/fmn2_lle_5000_1000_out/Sigmoid�
 model/concatenate_51/concat/axisConst*
_output_shapes
: *
dtype0*
value	B :2"
 model/concatenate_51/concat/axis�
model/concatenate_51/concatConcatV2(model/fmn2_pca_5000_1000_out/Sigmoid:y:0)model/fmn2_tsvd_5000_1000_out/Sigmoid:y:0+model/fmn2_isomap_5000_1000_out/Sigmoid:y:0(model/fmn2_lle_5000_1000_out/Sigmoid:y:0)model/concatenate_51/concat/axis:output:0*
N*
T0*'
_output_shapes
:���������2
model/concatenate_51/concat�
"model/joined/MatMul/ReadVariableOpReadVariableOp+model_joined_matmul_readvariableop_resource*
_output_shapes

:*
dtype02$
"model/joined/MatMul/ReadVariableOp�
model/joined/MatMulMatMul$model/concatenate_51/concat:output:0*model/joined/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
model/joined/MatMul�
#model/joined/BiasAdd/ReadVariableOpReadVariableOp,model_joined_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02%
#model/joined/BiasAdd/ReadVariableOp�
model/joined/BiasAddBiasAddmodel/joined/MatMul:product:0+model/joined/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
model/joined/BiasAdd�
model/joined/SigmoidSigmoidmodel/joined/BiasAdd:output:0*
T0*'
_output_shapes
:���������2
model/joined/Sigmoids
IdentityIdentitymodel/joined/Sigmoid:y:0^NoOp*
T0*'
_output_shapes
:���������2

Identity�
NoOpNoOp5^model/fmn2_isomap_5000_1000_1/BiasAdd/ReadVariableOp4^model/fmn2_isomap_5000_1000_1/MatMul/ReadVariableOp5^model/fmn2_isomap_5000_1000_2/BiasAdd/ReadVariableOp4^model/fmn2_isomap_5000_1000_2/MatMul/ReadVariableOp5^model/fmn2_isomap_5000_1000_3/BiasAdd/ReadVariableOp4^model/fmn2_isomap_5000_1000_3/MatMul/ReadVariableOp7^model/fmn2_isomap_5000_1000_out/BiasAdd/ReadVariableOp6^model/fmn2_isomap_5000_1000_out/MatMul/ReadVariableOp2^model/fmn2_lle_5000_1000_1/BiasAdd/ReadVariableOp1^model/fmn2_lle_5000_1000_1/MatMul/ReadVariableOp2^model/fmn2_lle_5000_1000_2/BiasAdd/ReadVariableOp1^model/fmn2_lle_5000_1000_2/MatMul/ReadVariableOp2^model/fmn2_lle_5000_1000_3/BiasAdd/ReadVariableOp1^model/fmn2_lle_5000_1000_3/MatMul/ReadVariableOp4^model/fmn2_lle_5000_1000_out/BiasAdd/ReadVariableOp3^model/fmn2_lle_5000_1000_out/MatMul/ReadVariableOp2^model/fmn2_pca_5000_1000_1/BiasAdd/ReadVariableOp1^model/fmn2_pca_5000_1000_1/MatMul/ReadVariableOp2^model/fmn2_pca_5000_1000_2/BiasAdd/ReadVariableOp1^model/fmn2_pca_5000_1000_2/MatMul/ReadVariableOp2^model/fmn2_pca_5000_1000_3/BiasAdd/ReadVariableOp1^model/fmn2_pca_5000_1000_3/MatMul/ReadVariableOp4^model/fmn2_pca_5000_1000_out/BiasAdd/ReadVariableOp3^model/fmn2_pca_5000_1000_out/MatMul/ReadVariableOp3^model/fmn2_tsvd_5000_1000_1/BiasAdd/ReadVariableOp2^model/fmn2_tsvd_5000_1000_1/MatMul/ReadVariableOp3^model/fmn2_tsvd_5000_1000_2/BiasAdd/ReadVariableOp2^model/fmn2_tsvd_5000_1000_2/MatMul/ReadVariableOp3^model/fmn2_tsvd_5000_1000_3/BiasAdd/ReadVariableOp2^model/fmn2_tsvd_5000_1000_3/MatMul/ReadVariableOp5^model/fmn2_tsvd_5000_1000_out/BiasAdd/ReadVariableOp4^model/fmn2_tsvd_5000_1000_out/MatMul/ReadVariableOp$^model/joined/BiasAdd/ReadVariableOp#^model/joined/MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*�
_input_shapes�
�:����������:����������:����������:����������: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 2l
4model/fmn2_isomap_5000_1000_1/BiasAdd/ReadVariableOp4model/fmn2_isomap_5000_1000_1/BiasAdd/ReadVariableOp2j
3model/fmn2_isomap_5000_1000_1/MatMul/ReadVariableOp3model/fmn2_isomap_5000_1000_1/MatMul/ReadVariableOp2l
4model/fmn2_isomap_5000_1000_2/BiasAdd/ReadVariableOp4model/fmn2_isomap_5000_1000_2/BiasAdd/ReadVariableOp2j
3model/fmn2_isomap_5000_1000_2/MatMul/ReadVariableOp3model/fmn2_isomap_5000_1000_2/MatMul/ReadVariableOp2l
4model/fmn2_isomap_5000_1000_3/BiasAdd/ReadVariableOp4model/fmn2_isomap_5000_1000_3/BiasAdd/ReadVariableOp2j
3model/fmn2_isomap_5000_1000_3/MatMul/ReadVariableOp3model/fmn2_isomap_5000_1000_3/MatMul/ReadVariableOp2p
6model/fmn2_isomap_5000_1000_out/BiasAdd/ReadVariableOp6model/fmn2_isomap_5000_1000_out/BiasAdd/ReadVariableOp2n
5model/fmn2_isomap_5000_1000_out/MatMul/ReadVariableOp5model/fmn2_isomap_5000_1000_out/MatMul/ReadVariableOp2f
1model/fmn2_lle_5000_1000_1/BiasAdd/ReadVariableOp1model/fmn2_lle_5000_1000_1/BiasAdd/ReadVariableOp2d
0model/fmn2_lle_5000_1000_1/MatMul/ReadVariableOp0model/fmn2_lle_5000_1000_1/MatMul/ReadVariableOp2f
1model/fmn2_lle_5000_1000_2/BiasAdd/ReadVariableOp1model/fmn2_lle_5000_1000_2/BiasAdd/ReadVariableOp2d
0model/fmn2_lle_5000_1000_2/MatMul/ReadVariableOp0model/fmn2_lle_5000_1000_2/MatMul/ReadVariableOp2f
1model/fmn2_lle_5000_1000_3/BiasAdd/ReadVariableOp1model/fmn2_lle_5000_1000_3/BiasAdd/ReadVariableOp2d
0model/fmn2_lle_5000_1000_3/MatMul/ReadVariableOp0model/fmn2_lle_5000_1000_3/MatMul/ReadVariableOp2j
3model/fmn2_lle_5000_1000_out/BiasAdd/ReadVariableOp3model/fmn2_lle_5000_1000_out/BiasAdd/ReadVariableOp2h
2model/fmn2_lle_5000_1000_out/MatMul/ReadVariableOp2model/fmn2_lle_5000_1000_out/MatMul/ReadVariableOp2f
1model/fmn2_pca_5000_1000_1/BiasAdd/ReadVariableOp1model/fmn2_pca_5000_1000_1/BiasAdd/ReadVariableOp2d
0model/fmn2_pca_5000_1000_1/MatMul/ReadVariableOp0model/fmn2_pca_5000_1000_1/MatMul/ReadVariableOp2f
1model/fmn2_pca_5000_1000_2/BiasAdd/ReadVariableOp1model/fmn2_pca_5000_1000_2/BiasAdd/ReadVariableOp2d
0model/fmn2_pca_5000_1000_2/MatMul/ReadVariableOp0model/fmn2_pca_5000_1000_2/MatMul/ReadVariableOp2f
1model/fmn2_pca_5000_1000_3/BiasAdd/ReadVariableOp1model/fmn2_pca_5000_1000_3/BiasAdd/ReadVariableOp2d
0model/fmn2_pca_5000_1000_3/MatMul/ReadVariableOp0model/fmn2_pca_5000_1000_3/MatMul/ReadVariableOp2j
3model/fmn2_pca_5000_1000_out/BiasAdd/ReadVariableOp3model/fmn2_pca_5000_1000_out/BiasAdd/ReadVariableOp2h
2model/fmn2_pca_5000_1000_out/MatMul/ReadVariableOp2model/fmn2_pca_5000_1000_out/MatMul/ReadVariableOp2h
2model/fmn2_tsvd_5000_1000_1/BiasAdd/ReadVariableOp2model/fmn2_tsvd_5000_1000_1/BiasAdd/ReadVariableOp2f
1model/fmn2_tsvd_5000_1000_1/MatMul/ReadVariableOp1model/fmn2_tsvd_5000_1000_1/MatMul/ReadVariableOp2h
2model/fmn2_tsvd_5000_1000_2/BiasAdd/ReadVariableOp2model/fmn2_tsvd_5000_1000_2/BiasAdd/ReadVariableOp2f
1model/fmn2_tsvd_5000_1000_2/MatMul/ReadVariableOp1model/fmn2_tsvd_5000_1000_2/MatMul/ReadVariableOp2h
2model/fmn2_tsvd_5000_1000_3/BiasAdd/ReadVariableOp2model/fmn2_tsvd_5000_1000_3/BiasAdd/ReadVariableOp2f
1model/fmn2_tsvd_5000_1000_3/MatMul/ReadVariableOp1model/fmn2_tsvd_5000_1000_3/MatMul/ReadVariableOp2l
4model/fmn2_tsvd_5000_1000_out/BiasAdd/ReadVariableOp4model/fmn2_tsvd_5000_1000_out/BiasAdd/ReadVariableOp2j
3model/fmn2_tsvd_5000_1000_out/MatMul/ReadVariableOp3model/fmn2_tsvd_5000_1000_out/MatMul/ReadVariableOp2J
#model/joined/BiasAdd/ReadVariableOp#model/joined/BiasAdd/ReadVariableOp2H
"model/joined/MatMul/ReadVariableOp"model/joined/MatMul/ReadVariableOp:b ^
(
_output_shapes
:����������
2
_user_specified_namefmn2_pca_5000_1000_input:c_
(
_output_shapes
:����������
3
_user_specified_namefmn2_tsvd_5000_1000_input:ea
(
_output_shapes
:����������
5
_user_specified_namefmn2_isomap_5000_1000_input:b^
(
_output_shapes
:����������
2
_user_specified_namefmn2_lle_5000_1000_input
�
�
7__inference_fmn2_tsvd_5000_1000_2_layer_call_fn_1731740

inputs
unknown:
��
	unknown_0:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_fmn2_tsvd_5000_1000_2_layer_call_and_return_conditional_losses_17301942
StatefulPartitionedCall|
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:����������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
C__inference_joined_layer_call_and_return_conditional_losses_1731988

inputs0
matmul_readvariableop_resource:-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2	
BiasAdda
SigmoidSigmoidBiasAdd:output:0*
T0*'
_output_shapes
:���������2	
Sigmoidf
IdentityIdentitySigmoid:y:0^NoOp*
T0*'
_output_shapes
:���������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs
�
�
7__inference_fmn2_tsvd_5000_1000_3_layer_call_fn_1731820

inputs
unknown:
��
	unknown_0:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_fmn2_tsvd_5000_1000_3_layer_call_and_return_conditional_losses_17302622
StatefulPartitionedCall|
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:����������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
R__inference_fmn2_tsvd_5000_1000_3_layer_call_and_return_conditional_losses_1731831

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
R__inference_fmn2_tsvd_5000_1000_3_layer_call_and_return_conditional_losses_1730262

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
T__inference_fmn2_isomap_5000_1000_2_layer_call_and_return_conditional_losses_1730177

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�u
�
B__inference_model_layer_call_and_return_conditional_losses_1730382

inputs
inputs_1
inputs_2
inputs_30
fmn2_lle_5000_1000_1_1730093:
��+
fmn2_lle_5000_1000_1_1730095:	�3
fmn2_isomap_5000_1000_1_1730110:
��.
fmn2_isomap_5000_1000_1_1730112:	�1
fmn2_tsvd_5000_1000_1_1730127:
��,
fmn2_tsvd_5000_1000_1_1730129:	�0
fmn2_pca_5000_1000_1_1730144:
��+
fmn2_pca_5000_1000_1_1730146:	�0
fmn2_lle_5000_1000_2_1730161:
��+
fmn2_lle_5000_1000_2_1730163:	�3
fmn2_isomap_5000_1000_2_1730178:
��.
fmn2_isomap_5000_1000_2_1730180:	�1
fmn2_tsvd_5000_1000_2_1730195:
��,
fmn2_tsvd_5000_1000_2_1730197:	�0
fmn2_pca_5000_1000_2_1730212:
��+
fmn2_pca_5000_1000_2_1730214:	�0
fmn2_lle_5000_1000_3_1730229:
��+
fmn2_lle_5000_1000_3_1730231:	�3
fmn2_isomap_5000_1000_3_1730246:
��.
fmn2_isomap_5000_1000_3_1730248:	�1
fmn2_tsvd_5000_1000_3_1730263:
��,
fmn2_tsvd_5000_1000_3_1730265:	�0
fmn2_pca_5000_1000_3_1730280:
��+
fmn2_pca_5000_1000_3_1730282:	�1
fmn2_pca_5000_1000_out_1730297:	�,
fmn2_pca_5000_1000_out_1730299:2
fmn2_tsvd_5000_1000_out_1730314:	�-
fmn2_tsvd_5000_1000_out_1730316:4
!fmn2_isomap_5000_1000_out_1730331:	�/
!fmn2_isomap_5000_1000_out_1730333:1
fmn2_lle_5000_1000_out_1730348:	�,
fmn2_lle_5000_1000_out_1730350: 
joined_1730376:
joined_1730378:
identity��/fmn2_isomap_5000_1000_1/StatefulPartitionedCall�/fmn2_isomap_5000_1000_2/StatefulPartitionedCall�/fmn2_isomap_5000_1000_3/StatefulPartitionedCall�1fmn2_isomap_5000_1000_out/StatefulPartitionedCall�,fmn2_lle_5000_1000_1/StatefulPartitionedCall�,fmn2_lle_5000_1000_2/StatefulPartitionedCall�,fmn2_lle_5000_1000_3/StatefulPartitionedCall�.fmn2_lle_5000_1000_out/StatefulPartitionedCall�,fmn2_pca_5000_1000_1/StatefulPartitionedCall�,fmn2_pca_5000_1000_2/StatefulPartitionedCall�,fmn2_pca_5000_1000_3/StatefulPartitionedCall�.fmn2_pca_5000_1000_out/StatefulPartitionedCall�-fmn2_tsvd_5000_1000_1/StatefulPartitionedCall�-fmn2_tsvd_5000_1000_2/StatefulPartitionedCall�-fmn2_tsvd_5000_1000_3/StatefulPartitionedCall�/fmn2_tsvd_5000_1000_out/StatefulPartitionedCall�joined/StatefulPartitionedCall�
,fmn2_lle_5000_1000_1/StatefulPartitionedCallStatefulPartitionedCallinputs_3fmn2_lle_5000_1000_1_1730093fmn2_lle_5000_1000_1_1730095*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_fmn2_lle_5000_1000_1_layer_call_and_return_conditional_losses_17300922.
,fmn2_lle_5000_1000_1/StatefulPartitionedCall�
/fmn2_isomap_5000_1000_1/StatefulPartitionedCallStatefulPartitionedCallinputs_2fmn2_isomap_5000_1000_1_1730110fmn2_isomap_5000_1000_1_1730112*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *]
fXRV
T__inference_fmn2_isomap_5000_1000_1_layer_call_and_return_conditional_losses_173010921
/fmn2_isomap_5000_1000_1/StatefulPartitionedCall�
-fmn2_tsvd_5000_1000_1/StatefulPartitionedCallStatefulPartitionedCallinputs_1fmn2_tsvd_5000_1000_1_1730127fmn2_tsvd_5000_1000_1_1730129*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_fmn2_tsvd_5000_1000_1_layer_call_and_return_conditional_losses_17301262/
-fmn2_tsvd_5000_1000_1/StatefulPartitionedCall�
,fmn2_pca_5000_1000_1/StatefulPartitionedCallStatefulPartitionedCallinputsfmn2_pca_5000_1000_1_1730144fmn2_pca_5000_1000_1_1730146*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_fmn2_pca_5000_1000_1_layer_call_and_return_conditional_losses_17301432.
,fmn2_pca_5000_1000_1/StatefulPartitionedCall�
,fmn2_lle_5000_1000_2/StatefulPartitionedCallStatefulPartitionedCall5fmn2_lle_5000_1000_1/StatefulPartitionedCall:output:0fmn2_lle_5000_1000_2_1730161fmn2_lle_5000_1000_2_1730163*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_fmn2_lle_5000_1000_2_layer_call_and_return_conditional_losses_17301602.
,fmn2_lle_5000_1000_2/StatefulPartitionedCall�
/fmn2_isomap_5000_1000_2/StatefulPartitionedCallStatefulPartitionedCall8fmn2_isomap_5000_1000_1/StatefulPartitionedCall:output:0fmn2_isomap_5000_1000_2_1730178fmn2_isomap_5000_1000_2_1730180*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *]
fXRV
T__inference_fmn2_isomap_5000_1000_2_layer_call_and_return_conditional_losses_173017721
/fmn2_isomap_5000_1000_2/StatefulPartitionedCall�
-fmn2_tsvd_5000_1000_2/StatefulPartitionedCallStatefulPartitionedCall6fmn2_tsvd_5000_1000_1/StatefulPartitionedCall:output:0fmn2_tsvd_5000_1000_2_1730195fmn2_tsvd_5000_1000_2_1730197*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_fmn2_tsvd_5000_1000_2_layer_call_and_return_conditional_losses_17301942/
-fmn2_tsvd_5000_1000_2/StatefulPartitionedCall�
,fmn2_pca_5000_1000_2/StatefulPartitionedCallStatefulPartitionedCall5fmn2_pca_5000_1000_1/StatefulPartitionedCall:output:0fmn2_pca_5000_1000_2_1730212fmn2_pca_5000_1000_2_1730214*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_fmn2_pca_5000_1000_2_layer_call_and_return_conditional_losses_17302112.
,fmn2_pca_5000_1000_2/StatefulPartitionedCall�
,fmn2_lle_5000_1000_3/StatefulPartitionedCallStatefulPartitionedCall5fmn2_lle_5000_1000_2/StatefulPartitionedCall:output:0fmn2_lle_5000_1000_3_1730229fmn2_lle_5000_1000_3_1730231*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_fmn2_lle_5000_1000_3_layer_call_and_return_conditional_losses_17302282.
,fmn2_lle_5000_1000_3/StatefulPartitionedCall�
/fmn2_isomap_5000_1000_3/StatefulPartitionedCallStatefulPartitionedCall8fmn2_isomap_5000_1000_2/StatefulPartitionedCall:output:0fmn2_isomap_5000_1000_3_1730246fmn2_isomap_5000_1000_3_1730248*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *]
fXRV
T__inference_fmn2_isomap_5000_1000_3_layer_call_and_return_conditional_losses_173024521
/fmn2_isomap_5000_1000_3/StatefulPartitionedCall�
-fmn2_tsvd_5000_1000_3/StatefulPartitionedCallStatefulPartitionedCall6fmn2_tsvd_5000_1000_2/StatefulPartitionedCall:output:0fmn2_tsvd_5000_1000_3_1730263fmn2_tsvd_5000_1000_3_1730265*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_fmn2_tsvd_5000_1000_3_layer_call_and_return_conditional_losses_17302622/
-fmn2_tsvd_5000_1000_3/StatefulPartitionedCall�
,fmn2_pca_5000_1000_3/StatefulPartitionedCallStatefulPartitionedCall5fmn2_pca_5000_1000_2/StatefulPartitionedCall:output:0fmn2_pca_5000_1000_3_1730280fmn2_pca_5000_1000_3_1730282*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_fmn2_pca_5000_1000_3_layer_call_and_return_conditional_losses_17302792.
,fmn2_pca_5000_1000_3/StatefulPartitionedCall�
.fmn2_pca_5000_1000_out/StatefulPartitionedCallStatefulPartitionedCall5fmn2_pca_5000_1000_3/StatefulPartitionedCall:output:0fmn2_pca_5000_1000_out_1730297fmn2_pca_5000_1000_out_1730299*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *\
fWRU
S__inference_fmn2_pca_5000_1000_out_layer_call_and_return_conditional_losses_173029620
.fmn2_pca_5000_1000_out/StatefulPartitionedCall�
/fmn2_tsvd_5000_1000_out/StatefulPartitionedCallStatefulPartitionedCall6fmn2_tsvd_5000_1000_3/StatefulPartitionedCall:output:0fmn2_tsvd_5000_1000_out_1730314fmn2_tsvd_5000_1000_out_1730316*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *]
fXRV
T__inference_fmn2_tsvd_5000_1000_out_layer_call_and_return_conditional_losses_173031321
/fmn2_tsvd_5000_1000_out/StatefulPartitionedCall�
1fmn2_isomap_5000_1000_out/StatefulPartitionedCallStatefulPartitionedCall8fmn2_isomap_5000_1000_3/StatefulPartitionedCall:output:0!fmn2_isomap_5000_1000_out_1730331!fmn2_isomap_5000_1000_out_1730333*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *_
fZRX
V__inference_fmn2_isomap_5000_1000_out_layer_call_and_return_conditional_losses_173033023
1fmn2_isomap_5000_1000_out/StatefulPartitionedCall�
.fmn2_lle_5000_1000_out/StatefulPartitionedCallStatefulPartitionedCall5fmn2_lle_5000_1000_3/StatefulPartitionedCall:output:0fmn2_lle_5000_1000_out_1730348fmn2_lle_5000_1000_out_1730350*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *\
fWRU
S__inference_fmn2_lle_5000_1000_out_layer_call_and_return_conditional_losses_173034720
.fmn2_lle_5000_1000_out/StatefulPartitionedCall�
concatenate_51/PartitionedCallPartitionedCall7fmn2_pca_5000_1000_out/StatefulPartitionedCall:output:08fmn2_tsvd_5000_1000_out/StatefulPartitionedCall:output:0:fmn2_isomap_5000_1000_out/StatefulPartitionedCall:output:07fmn2_lle_5000_1000_out/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *T
fORM
K__inference_concatenate_51_layer_call_and_return_conditional_losses_17303622 
concatenate_51/PartitionedCall�
joined/StatefulPartitionedCallStatefulPartitionedCall'concatenate_51/PartitionedCall:output:0joined_1730376joined_1730378*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *L
fGRE
C__inference_joined_layer_call_and_return_conditional_losses_17303752 
joined/StatefulPartitionedCall�
IdentityIdentity'joined/StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identity�
NoOpNoOp0^fmn2_isomap_5000_1000_1/StatefulPartitionedCall0^fmn2_isomap_5000_1000_2/StatefulPartitionedCall0^fmn2_isomap_5000_1000_3/StatefulPartitionedCall2^fmn2_isomap_5000_1000_out/StatefulPartitionedCall-^fmn2_lle_5000_1000_1/StatefulPartitionedCall-^fmn2_lle_5000_1000_2/StatefulPartitionedCall-^fmn2_lle_5000_1000_3/StatefulPartitionedCall/^fmn2_lle_5000_1000_out/StatefulPartitionedCall-^fmn2_pca_5000_1000_1/StatefulPartitionedCall-^fmn2_pca_5000_1000_2/StatefulPartitionedCall-^fmn2_pca_5000_1000_3/StatefulPartitionedCall/^fmn2_pca_5000_1000_out/StatefulPartitionedCall.^fmn2_tsvd_5000_1000_1/StatefulPartitionedCall.^fmn2_tsvd_5000_1000_2/StatefulPartitionedCall.^fmn2_tsvd_5000_1000_3/StatefulPartitionedCall0^fmn2_tsvd_5000_1000_out/StatefulPartitionedCall^joined/StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*�
_input_shapes�
�:����������:����������:����������:����������: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 2b
/fmn2_isomap_5000_1000_1/StatefulPartitionedCall/fmn2_isomap_5000_1000_1/StatefulPartitionedCall2b
/fmn2_isomap_5000_1000_2/StatefulPartitionedCall/fmn2_isomap_5000_1000_2/StatefulPartitionedCall2b
/fmn2_isomap_5000_1000_3/StatefulPartitionedCall/fmn2_isomap_5000_1000_3/StatefulPartitionedCall2f
1fmn2_isomap_5000_1000_out/StatefulPartitionedCall1fmn2_isomap_5000_1000_out/StatefulPartitionedCall2\
,fmn2_lle_5000_1000_1/StatefulPartitionedCall,fmn2_lle_5000_1000_1/StatefulPartitionedCall2\
,fmn2_lle_5000_1000_2/StatefulPartitionedCall,fmn2_lle_5000_1000_2/StatefulPartitionedCall2\
,fmn2_lle_5000_1000_3/StatefulPartitionedCall,fmn2_lle_5000_1000_3/StatefulPartitionedCall2`
.fmn2_lle_5000_1000_out/StatefulPartitionedCall.fmn2_lle_5000_1000_out/StatefulPartitionedCall2\
,fmn2_pca_5000_1000_1/StatefulPartitionedCall,fmn2_pca_5000_1000_1/StatefulPartitionedCall2\
,fmn2_pca_5000_1000_2/StatefulPartitionedCall,fmn2_pca_5000_1000_2/StatefulPartitionedCall2\
,fmn2_pca_5000_1000_3/StatefulPartitionedCall,fmn2_pca_5000_1000_3/StatefulPartitionedCall2`
.fmn2_pca_5000_1000_out/StatefulPartitionedCall.fmn2_pca_5000_1000_out/StatefulPartitionedCall2^
-fmn2_tsvd_5000_1000_1/StatefulPartitionedCall-fmn2_tsvd_5000_1000_1/StatefulPartitionedCall2^
-fmn2_tsvd_5000_1000_2/StatefulPartitionedCall-fmn2_tsvd_5000_1000_2/StatefulPartitionedCall2^
-fmn2_tsvd_5000_1000_3/StatefulPartitionedCall-fmn2_tsvd_5000_1000_3/StatefulPartitionedCall2b
/fmn2_tsvd_5000_1000_out/StatefulPartitionedCall/fmn2_tsvd_5000_1000_out/StatefulPartitionedCall2@
joined/StatefulPartitionedCalljoined/StatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs:PL
(
_output_shapes
:����������
 
_user_specified_nameinputs:PL
(
_output_shapes
:����������
 
_user_specified_nameinputs:PL
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
R__inference_fmn2_tsvd_5000_1000_1_layer_call_and_return_conditional_losses_1731671

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
8__inference_fmn2_pca_5000_1000_out_layer_call_fn_1731880

inputs
unknown:	�
	unknown_0:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *\
fWRU
S__inference_fmn2_pca_5000_1000_out_layer_call_and_return_conditional_losses_17302962
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�	
%__inference_signature_wrapper_1731223
fmn2_isomap_5000_1000_input
fmn2_lle_5000_1000_input
fmn2_pca_5000_1000_input
fmn2_tsvd_5000_1000_input
unknown:
��
	unknown_0:	�
	unknown_1:
��
	unknown_2:	�
	unknown_3:
��
	unknown_4:	�
	unknown_5:
��
	unknown_6:	�
	unknown_7:
��
	unknown_8:	�
	unknown_9:
��

unknown_10:	�

unknown_11:
��

unknown_12:	�

unknown_13:
��

unknown_14:	�

unknown_15:
��

unknown_16:	�

unknown_17:
��

unknown_18:	�

unknown_19:
��

unknown_20:	�

unknown_21:
��

unknown_22:	�

unknown_23:	�

unknown_24:

unknown_25:	�

unknown_26:

unknown_27:	�

unknown_28:

unknown_29:	�

unknown_30:

unknown_31:

unknown_32:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallfmn2_pca_5000_1000_inputfmn2_tsvd_5000_1000_inputfmn2_isomap_5000_1000_inputfmn2_lle_5000_1000_inputunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10
unknown_11
unknown_12
unknown_13
unknown_14
unknown_15
unknown_16
unknown_17
unknown_18
unknown_19
unknown_20
unknown_21
unknown_22
unknown_23
unknown_24
unknown_25
unknown_26
unknown_27
unknown_28
unknown_29
unknown_30
unknown_31
unknown_32*1
Tin*
(2&*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*D
_read_only_resource_inputs&
$"	
 !"#$%*0
config_proto 

CPU

GPU2*0J 8� *+
f&R$
"__inference__wrapped_model_17300682
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*�
_input_shapes�
�:����������:����������:����������:����������: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:e a
(
_output_shapes
:����������
5
_user_specified_namefmn2_isomap_5000_1000_input:b^
(
_output_shapes
:����������
2
_user_specified_namefmn2_lle_5000_1000_input:b^
(
_output_shapes
:����������
2
_user_specified_namefmn2_pca_5000_1000_input:c_
(
_output_shapes
:����������
3
_user_specified_namefmn2_tsvd_5000_1000_input
��
� 
B__inference_model_layer_call_and_return_conditional_losses_1731503
inputs_0
inputs_1
inputs_2
inputs_3G
3fmn2_lle_5000_1000_1_matmul_readvariableop_resource:
��C
4fmn2_lle_5000_1000_1_biasadd_readvariableop_resource:	�J
6fmn2_isomap_5000_1000_1_matmul_readvariableop_resource:
��F
7fmn2_isomap_5000_1000_1_biasadd_readvariableop_resource:	�H
4fmn2_tsvd_5000_1000_1_matmul_readvariableop_resource:
��D
5fmn2_tsvd_5000_1000_1_biasadd_readvariableop_resource:	�G
3fmn2_pca_5000_1000_1_matmul_readvariableop_resource:
��C
4fmn2_pca_5000_1000_1_biasadd_readvariableop_resource:	�G
3fmn2_lle_5000_1000_2_matmul_readvariableop_resource:
��C
4fmn2_lle_5000_1000_2_biasadd_readvariableop_resource:	�J
6fmn2_isomap_5000_1000_2_matmul_readvariableop_resource:
��F
7fmn2_isomap_5000_1000_2_biasadd_readvariableop_resource:	�H
4fmn2_tsvd_5000_1000_2_matmul_readvariableop_resource:
��D
5fmn2_tsvd_5000_1000_2_biasadd_readvariableop_resource:	�G
3fmn2_pca_5000_1000_2_matmul_readvariableop_resource:
��C
4fmn2_pca_5000_1000_2_biasadd_readvariableop_resource:	�G
3fmn2_lle_5000_1000_3_matmul_readvariableop_resource:
��C
4fmn2_lle_5000_1000_3_biasadd_readvariableop_resource:	�J
6fmn2_isomap_5000_1000_3_matmul_readvariableop_resource:
��F
7fmn2_isomap_5000_1000_3_biasadd_readvariableop_resource:	�H
4fmn2_tsvd_5000_1000_3_matmul_readvariableop_resource:
��D
5fmn2_tsvd_5000_1000_3_biasadd_readvariableop_resource:	�G
3fmn2_pca_5000_1000_3_matmul_readvariableop_resource:
��C
4fmn2_pca_5000_1000_3_biasadd_readvariableop_resource:	�H
5fmn2_pca_5000_1000_out_matmul_readvariableop_resource:	�D
6fmn2_pca_5000_1000_out_biasadd_readvariableop_resource:I
6fmn2_tsvd_5000_1000_out_matmul_readvariableop_resource:	�E
7fmn2_tsvd_5000_1000_out_biasadd_readvariableop_resource:K
8fmn2_isomap_5000_1000_out_matmul_readvariableop_resource:	�G
9fmn2_isomap_5000_1000_out_biasadd_readvariableop_resource:H
5fmn2_lle_5000_1000_out_matmul_readvariableop_resource:	�D
6fmn2_lle_5000_1000_out_biasadd_readvariableop_resource:7
%joined_matmul_readvariableop_resource:4
&joined_biasadd_readvariableop_resource:
identity��.fmn2_isomap_5000_1000_1/BiasAdd/ReadVariableOp�-fmn2_isomap_5000_1000_1/MatMul/ReadVariableOp�.fmn2_isomap_5000_1000_2/BiasAdd/ReadVariableOp�-fmn2_isomap_5000_1000_2/MatMul/ReadVariableOp�.fmn2_isomap_5000_1000_3/BiasAdd/ReadVariableOp�-fmn2_isomap_5000_1000_3/MatMul/ReadVariableOp�0fmn2_isomap_5000_1000_out/BiasAdd/ReadVariableOp�/fmn2_isomap_5000_1000_out/MatMul/ReadVariableOp�+fmn2_lle_5000_1000_1/BiasAdd/ReadVariableOp�*fmn2_lle_5000_1000_1/MatMul/ReadVariableOp�+fmn2_lle_5000_1000_2/BiasAdd/ReadVariableOp�*fmn2_lle_5000_1000_2/MatMul/ReadVariableOp�+fmn2_lle_5000_1000_3/BiasAdd/ReadVariableOp�*fmn2_lle_5000_1000_3/MatMul/ReadVariableOp�-fmn2_lle_5000_1000_out/BiasAdd/ReadVariableOp�,fmn2_lle_5000_1000_out/MatMul/ReadVariableOp�+fmn2_pca_5000_1000_1/BiasAdd/ReadVariableOp�*fmn2_pca_5000_1000_1/MatMul/ReadVariableOp�+fmn2_pca_5000_1000_2/BiasAdd/ReadVariableOp�*fmn2_pca_5000_1000_2/MatMul/ReadVariableOp�+fmn2_pca_5000_1000_3/BiasAdd/ReadVariableOp�*fmn2_pca_5000_1000_3/MatMul/ReadVariableOp�-fmn2_pca_5000_1000_out/BiasAdd/ReadVariableOp�,fmn2_pca_5000_1000_out/MatMul/ReadVariableOp�,fmn2_tsvd_5000_1000_1/BiasAdd/ReadVariableOp�+fmn2_tsvd_5000_1000_1/MatMul/ReadVariableOp�,fmn2_tsvd_5000_1000_2/BiasAdd/ReadVariableOp�+fmn2_tsvd_5000_1000_2/MatMul/ReadVariableOp�,fmn2_tsvd_5000_1000_3/BiasAdd/ReadVariableOp�+fmn2_tsvd_5000_1000_3/MatMul/ReadVariableOp�.fmn2_tsvd_5000_1000_out/BiasAdd/ReadVariableOp�-fmn2_tsvd_5000_1000_out/MatMul/ReadVariableOp�joined/BiasAdd/ReadVariableOp�joined/MatMul/ReadVariableOp�
*fmn2_lle_5000_1000_1/MatMul/ReadVariableOpReadVariableOp3fmn2_lle_5000_1000_1_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02,
*fmn2_lle_5000_1000_1/MatMul/ReadVariableOp�
fmn2_lle_5000_1000_1/MatMulMatMulinputs_32fmn2_lle_5000_1000_1/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn2_lle_5000_1000_1/MatMul�
+fmn2_lle_5000_1000_1/BiasAdd/ReadVariableOpReadVariableOp4fmn2_lle_5000_1000_1_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02-
+fmn2_lle_5000_1000_1/BiasAdd/ReadVariableOp�
fmn2_lle_5000_1000_1/BiasAddBiasAdd%fmn2_lle_5000_1000_1/MatMul:product:03fmn2_lle_5000_1000_1/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn2_lle_5000_1000_1/BiasAdd�
fmn2_lle_5000_1000_1/ReluRelu%fmn2_lle_5000_1000_1/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
fmn2_lle_5000_1000_1/Relu�
-fmn2_isomap_5000_1000_1/MatMul/ReadVariableOpReadVariableOp6fmn2_isomap_5000_1000_1_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02/
-fmn2_isomap_5000_1000_1/MatMul/ReadVariableOp�
fmn2_isomap_5000_1000_1/MatMulMatMulinputs_25fmn2_isomap_5000_1000_1/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2 
fmn2_isomap_5000_1000_1/MatMul�
.fmn2_isomap_5000_1000_1/BiasAdd/ReadVariableOpReadVariableOp7fmn2_isomap_5000_1000_1_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype020
.fmn2_isomap_5000_1000_1/BiasAdd/ReadVariableOp�
fmn2_isomap_5000_1000_1/BiasAddBiasAdd(fmn2_isomap_5000_1000_1/MatMul:product:06fmn2_isomap_5000_1000_1/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2!
fmn2_isomap_5000_1000_1/BiasAdd�
fmn2_isomap_5000_1000_1/ReluRelu(fmn2_isomap_5000_1000_1/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
fmn2_isomap_5000_1000_1/Relu�
+fmn2_tsvd_5000_1000_1/MatMul/ReadVariableOpReadVariableOp4fmn2_tsvd_5000_1000_1_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02-
+fmn2_tsvd_5000_1000_1/MatMul/ReadVariableOp�
fmn2_tsvd_5000_1000_1/MatMulMatMulinputs_13fmn2_tsvd_5000_1000_1/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn2_tsvd_5000_1000_1/MatMul�
,fmn2_tsvd_5000_1000_1/BiasAdd/ReadVariableOpReadVariableOp5fmn2_tsvd_5000_1000_1_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02.
,fmn2_tsvd_5000_1000_1/BiasAdd/ReadVariableOp�
fmn2_tsvd_5000_1000_1/BiasAddBiasAdd&fmn2_tsvd_5000_1000_1/MatMul:product:04fmn2_tsvd_5000_1000_1/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn2_tsvd_5000_1000_1/BiasAdd�
fmn2_tsvd_5000_1000_1/ReluRelu&fmn2_tsvd_5000_1000_1/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
fmn2_tsvd_5000_1000_1/Relu�
*fmn2_pca_5000_1000_1/MatMul/ReadVariableOpReadVariableOp3fmn2_pca_5000_1000_1_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02,
*fmn2_pca_5000_1000_1/MatMul/ReadVariableOp�
fmn2_pca_5000_1000_1/MatMulMatMulinputs_02fmn2_pca_5000_1000_1/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn2_pca_5000_1000_1/MatMul�
+fmn2_pca_5000_1000_1/BiasAdd/ReadVariableOpReadVariableOp4fmn2_pca_5000_1000_1_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02-
+fmn2_pca_5000_1000_1/BiasAdd/ReadVariableOp�
fmn2_pca_5000_1000_1/BiasAddBiasAdd%fmn2_pca_5000_1000_1/MatMul:product:03fmn2_pca_5000_1000_1/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn2_pca_5000_1000_1/BiasAdd�
fmn2_pca_5000_1000_1/ReluRelu%fmn2_pca_5000_1000_1/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
fmn2_pca_5000_1000_1/Relu�
*fmn2_lle_5000_1000_2/MatMul/ReadVariableOpReadVariableOp3fmn2_lle_5000_1000_2_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02,
*fmn2_lle_5000_1000_2/MatMul/ReadVariableOp�
fmn2_lle_5000_1000_2/MatMulMatMul'fmn2_lle_5000_1000_1/Relu:activations:02fmn2_lle_5000_1000_2/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn2_lle_5000_1000_2/MatMul�
+fmn2_lle_5000_1000_2/BiasAdd/ReadVariableOpReadVariableOp4fmn2_lle_5000_1000_2_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02-
+fmn2_lle_5000_1000_2/BiasAdd/ReadVariableOp�
fmn2_lle_5000_1000_2/BiasAddBiasAdd%fmn2_lle_5000_1000_2/MatMul:product:03fmn2_lle_5000_1000_2/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn2_lle_5000_1000_2/BiasAdd�
fmn2_lle_5000_1000_2/ReluRelu%fmn2_lle_5000_1000_2/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
fmn2_lle_5000_1000_2/Relu�
-fmn2_isomap_5000_1000_2/MatMul/ReadVariableOpReadVariableOp6fmn2_isomap_5000_1000_2_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02/
-fmn2_isomap_5000_1000_2/MatMul/ReadVariableOp�
fmn2_isomap_5000_1000_2/MatMulMatMul*fmn2_isomap_5000_1000_1/Relu:activations:05fmn2_isomap_5000_1000_2/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2 
fmn2_isomap_5000_1000_2/MatMul�
.fmn2_isomap_5000_1000_2/BiasAdd/ReadVariableOpReadVariableOp7fmn2_isomap_5000_1000_2_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype020
.fmn2_isomap_5000_1000_2/BiasAdd/ReadVariableOp�
fmn2_isomap_5000_1000_2/BiasAddBiasAdd(fmn2_isomap_5000_1000_2/MatMul:product:06fmn2_isomap_5000_1000_2/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2!
fmn2_isomap_5000_1000_2/BiasAdd�
fmn2_isomap_5000_1000_2/ReluRelu(fmn2_isomap_5000_1000_2/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
fmn2_isomap_5000_1000_2/Relu�
+fmn2_tsvd_5000_1000_2/MatMul/ReadVariableOpReadVariableOp4fmn2_tsvd_5000_1000_2_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02-
+fmn2_tsvd_5000_1000_2/MatMul/ReadVariableOp�
fmn2_tsvd_5000_1000_2/MatMulMatMul(fmn2_tsvd_5000_1000_1/Relu:activations:03fmn2_tsvd_5000_1000_2/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn2_tsvd_5000_1000_2/MatMul�
,fmn2_tsvd_5000_1000_2/BiasAdd/ReadVariableOpReadVariableOp5fmn2_tsvd_5000_1000_2_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02.
,fmn2_tsvd_5000_1000_2/BiasAdd/ReadVariableOp�
fmn2_tsvd_5000_1000_2/BiasAddBiasAdd&fmn2_tsvd_5000_1000_2/MatMul:product:04fmn2_tsvd_5000_1000_2/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn2_tsvd_5000_1000_2/BiasAdd�
fmn2_tsvd_5000_1000_2/ReluRelu&fmn2_tsvd_5000_1000_2/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
fmn2_tsvd_5000_1000_2/Relu�
*fmn2_pca_5000_1000_2/MatMul/ReadVariableOpReadVariableOp3fmn2_pca_5000_1000_2_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02,
*fmn2_pca_5000_1000_2/MatMul/ReadVariableOp�
fmn2_pca_5000_1000_2/MatMulMatMul'fmn2_pca_5000_1000_1/Relu:activations:02fmn2_pca_5000_1000_2/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn2_pca_5000_1000_2/MatMul�
+fmn2_pca_5000_1000_2/BiasAdd/ReadVariableOpReadVariableOp4fmn2_pca_5000_1000_2_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02-
+fmn2_pca_5000_1000_2/BiasAdd/ReadVariableOp�
fmn2_pca_5000_1000_2/BiasAddBiasAdd%fmn2_pca_5000_1000_2/MatMul:product:03fmn2_pca_5000_1000_2/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn2_pca_5000_1000_2/BiasAdd�
fmn2_pca_5000_1000_2/ReluRelu%fmn2_pca_5000_1000_2/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
fmn2_pca_5000_1000_2/Relu�
*fmn2_lle_5000_1000_3/MatMul/ReadVariableOpReadVariableOp3fmn2_lle_5000_1000_3_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02,
*fmn2_lle_5000_1000_3/MatMul/ReadVariableOp�
fmn2_lle_5000_1000_3/MatMulMatMul'fmn2_lle_5000_1000_2/Relu:activations:02fmn2_lle_5000_1000_3/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn2_lle_5000_1000_3/MatMul�
+fmn2_lle_5000_1000_3/BiasAdd/ReadVariableOpReadVariableOp4fmn2_lle_5000_1000_3_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02-
+fmn2_lle_5000_1000_3/BiasAdd/ReadVariableOp�
fmn2_lle_5000_1000_3/BiasAddBiasAdd%fmn2_lle_5000_1000_3/MatMul:product:03fmn2_lle_5000_1000_3/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn2_lle_5000_1000_3/BiasAdd�
fmn2_lle_5000_1000_3/ReluRelu%fmn2_lle_5000_1000_3/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
fmn2_lle_5000_1000_3/Relu�
-fmn2_isomap_5000_1000_3/MatMul/ReadVariableOpReadVariableOp6fmn2_isomap_5000_1000_3_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02/
-fmn2_isomap_5000_1000_3/MatMul/ReadVariableOp�
fmn2_isomap_5000_1000_3/MatMulMatMul*fmn2_isomap_5000_1000_2/Relu:activations:05fmn2_isomap_5000_1000_3/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2 
fmn2_isomap_5000_1000_3/MatMul�
.fmn2_isomap_5000_1000_3/BiasAdd/ReadVariableOpReadVariableOp7fmn2_isomap_5000_1000_3_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype020
.fmn2_isomap_5000_1000_3/BiasAdd/ReadVariableOp�
fmn2_isomap_5000_1000_3/BiasAddBiasAdd(fmn2_isomap_5000_1000_3/MatMul:product:06fmn2_isomap_5000_1000_3/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2!
fmn2_isomap_5000_1000_3/BiasAdd�
fmn2_isomap_5000_1000_3/ReluRelu(fmn2_isomap_5000_1000_3/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
fmn2_isomap_5000_1000_3/Relu�
+fmn2_tsvd_5000_1000_3/MatMul/ReadVariableOpReadVariableOp4fmn2_tsvd_5000_1000_3_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02-
+fmn2_tsvd_5000_1000_3/MatMul/ReadVariableOp�
fmn2_tsvd_5000_1000_3/MatMulMatMul(fmn2_tsvd_5000_1000_2/Relu:activations:03fmn2_tsvd_5000_1000_3/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn2_tsvd_5000_1000_3/MatMul�
,fmn2_tsvd_5000_1000_3/BiasAdd/ReadVariableOpReadVariableOp5fmn2_tsvd_5000_1000_3_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02.
,fmn2_tsvd_5000_1000_3/BiasAdd/ReadVariableOp�
fmn2_tsvd_5000_1000_3/BiasAddBiasAdd&fmn2_tsvd_5000_1000_3/MatMul:product:04fmn2_tsvd_5000_1000_3/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn2_tsvd_5000_1000_3/BiasAdd�
fmn2_tsvd_5000_1000_3/ReluRelu&fmn2_tsvd_5000_1000_3/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
fmn2_tsvd_5000_1000_3/Relu�
*fmn2_pca_5000_1000_3/MatMul/ReadVariableOpReadVariableOp3fmn2_pca_5000_1000_3_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02,
*fmn2_pca_5000_1000_3/MatMul/ReadVariableOp�
fmn2_pca_5000_1000_3/MatMulMatMul'fmn2_pca_5000_1000_2/Relu:activations:02fmn2_pca_5000_1000_3/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn2_pca_5000_1000_3/MatMul�
+fmn2_pca_5000_1000_3/BiasAdd/ReadVariableOpReadVariableOp4fmn2_pca_5000_1000_3_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02-
+fmn2_pca_5000_1000_3/BiasAdd/ReadVariableOp�
fmn2_pca_5000_1000_3/BiasAddBiasAdd%fmn2_pca_5000_1000_3/MatMul:product:03fmn2_pca_5000_1000_3/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
fmn2_pca_5000_1000_3/BiasAdd�
fmn2_pca_5000_1000_3/ReluRelu%fmn2_pca_5000_1000_3/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
fmn2_pca_5000_1000_3/Relu�
,fmn2_pca_5000_1000_out/MatMul/ReadVariableOpReadVariableOp5fmn2_pca_5000_1000_out_matmul_readvariableop_resource*
_output_shapes
:	�*
dtype02.
,fmn2_pca_5000_1000_out/MatMul/ReadVariableOp�
fmn2_pca_5000_1000_out/MatMulMatMul'fmn2_pca_5000_1000_3/Relu:activations:04fmn2_pca_5000_1000_out/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
fmn2_pca_5000_1000_out/MatMul�
-fmn2_pca_5000_1000_out/BiasAdd/ReadVariableOpReadVariableOp6fmn2_pca_5000_1000_out_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02/
-fmn2_pca_5000_1000_out/BiasAdd/ReadVariableOp�
fmn2_pca_5000_1000_out/BiasAddBiasAdd'fmn2_pca_5000_1000_out/MatMul:product:05fmn2_pca_5000_1000_out/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2 
fmn2_pca_5000_1000_out/BiasAdd�
fmn2_pca_5000_1000_out/SigmoidSigmoid'fmn2_pca_5000_1000_out/BiasAdd:output:0*
T0*'
_output_shapes
:���������2 
fmn2_pca_5000_1000_out/Sigmoid�
-fmn2_tsvd_5000_1000_out/MatMul/ReadVariableOpReadVariableOp6fmn2_tsvd_5000_1000_out_matmul_readvariableop_resource*
_output_shapes
:	�*
dtype02/
-fmn2_tsvd_5000_1000_out/MatMul/ReadVariableOp�
fmn2_tsvd_5000_1000_out/MatMulMatMul(fmn2_tsvd_5000_1000_3/Relu:activations:05fmn2_tsvd_5000_1000_out/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2 
fmn2_tsvd_5000_1000_out/MatMul�
.fmn2_tsvd_5000_1000_out/BiasAdd/ReadVariableOpReadVariableOp7fmn2_tsvd_5000_1000_out_biasadd_readvariableop_resource*
_output_shapes
:*
dtype020
.fmn2_tsvd_5000_1000_out/BiasAdd/ReadVariableOp�
fmn2_tsvd_5000_1000_out/BiasAddBiasAdd(fmn2_tsvd_5000_1000_out/MatMul:product:06fmn2_tsvd_5000_1000_out/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2!
fmn2_tsvd_5000_1000_out/BiasAdd�
fmn2_tsvd_5000_1000_out/SigmoidSigmoid(fmn2_tsvd_5000_1000_out/BiasAdd:output:0*
T0*'
_output_shapes
:���������2!
fmn2_tsvd_5000_1000_out/Sigmoid�
/fmn2_isomap_5000_1000_out/MatMul/ReadVariableOpReadVariableOp8fmn2_isomap_5000_1000_out_matmul_readvariableop_resource*
_output_shapes
:	�*
dtype021
/fmn2_isomap_5000_1000_out/MatMul/ReadVariableOp�
 fmn2_isomap_5000_1000_out/MatMulMatMul*fmn2_isomap_5000_1000_3/Relu:activations:07fmn2_isomap_5000_1000_out/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2"
 fmn2_isomap_5000_1000_out/MatMul�
0fmn2_isomap_5000_1000_out/BiasAdd/ReadVariableOpReadVariableOp9fmn2_isomap_5000_1000_out_biasadd_readvariableop_resource*
_output_shapes
:*
dtype022
0fmn2_isomap_5000_1000_out/BiasAdd/ReadVariableOp�
!fmn2_isomap_5000_1000_out/BiasAddBiasAdd*fmn2_isomap_5000_1000_out/MatMul:product:08fmn2_isomap_5000_1000_out/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2#
!fmn2_isomap_5000_1000_out/BiasAdd�
!fmn2_isomap_5000_1000_out/SigmoidSigmoid*fmn2_isomap_5000_1000_out/BiasAdd:output:0*
T0*'
_output_shapes
:���������2#
!fmn2_isomap_5000_1000_out/Sigmoid�
,fmn2_lle_5000_1000_out/MatMul/ReadVariableOpReadVariableOp5fmn2_lle_5000_1000_out_matmul_readvariableop_resource*
_output_shapes
:	�*
dtype02.
,fmn2_lle_5000_1000_out/MatMul/ReadVariableOp�
fmn2_lle_5000_1000_out/MatMulMatMul'fmn2_lle_5000_1000_3/Relu:activations:04fmn2_lle_5000_1000_out/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
fmn2_lle_5000_1000_out/MatMul�
-fmn2_lle_5000_1000_out/BiasAdd/ReadVariableOpReadVariableOp6fmn2_lle_5000_1000_out_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02/
-fmn2_lle_5000_1000_out/BiasAdd/ReadVariableOp�
fmn2_lle_5000_1000_out/BiasAddBiasAdd'fmn2_lle_5000_1000_out/MatMul:product:05fmn2_lle_5000_1000_out/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2 
fmn2_lle_5000_1000_out/BiasAdd�
fmn2_lle_5000_1000_out/SigmoidSigmoid'fmn2_lle_5000_1000_out/BiasAdd:output:0*
T0*'
_output_shapes
:���������2 
fmn2_lle_5000_1000_out/Sigmoidz
concatenate_51/concat/axisConst*
_output_shapes
: *
dtype0*
value	B :2
concatenate_51/concat/axis�
concatenate_51/concatConcatV2"fmn2_pca_5000_1000_out/Sigmoid:y:0#fmn2_tsvd_5000_1000_out/Sigmoid:y:0%fmn2_isomap_5000_1000_out/Sigmoid:y:0"fmn2_lle_5000_1000_out/Sigmoid:y:0#concatenate_51/concat/axis:output:0*
N*
T0*'
_output_shapes
:���������2
concatenate_51/concat�
joined/MatMul/ReadVariableOpReadVariableOp%joined_matmul_readvariableop_resource*
_output_shapes

:*
dtype02
joined/MatMul/ReadVariableOp�
joined/MatMulMatMulconcatenate_51/concat:output:0$joined/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
joined/MatMul�
joined/BiasAdd/ReadVariableOpReadVariableOp&joined_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02
joined/BiasAdd/ReadVariableOp�
joined/BiasAddBiasAddjoined/MatMul:product:0%joined/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
joined/BiasAddv
joined/SigmoidSigmoidjoined/BiasAdd:output:0*
T0*'
_output_shapes
:���������2
joined/Sigmoidm
IdentityIdentityjoined/Sigmoid:y:0^NoOp*
T0*'
_output_shapes
:���������2

Identity�
NoOpNoOp/^fmn2_isomap_5000_1000_1/BiasAdd/ReadVariableOp.^fmn2_isomap_5000_1000_1/MatMul/ReadVariableOp/^fmn2_isomap_5000_1000_2/BiasAdd/ReadVariableOp.^fmn2_isomap_5000_1000_2/MatMul/ReadVariableOp/^fmn2_isomap_5000_1000_3/BiasAdd/ReadVariableOp.^fmn2_isomap_5000_1000_3/MatMul/ReadVariableOp1^fmn2_isomap_5000_1000_out/BiasAdd/ReadVariableOp0^fmn2_isomap_5000_1000_out/MatMul/ReadVariableOp,^fmn2_lle_5000_1000_1/BiasAdd/ReadVariableOp+^fmn2_lle_5000_1000_1/MatMul/ReadVariableOp,^fmn2_lle_5000_1000_2/BiasAdd/ReadVariableOp+^fmn2_lle_5000_1000_2/MatMul/ReadVariableOp,^fmn2_lle_5000_1000_3/BiasAdd/ReadVariableOp+^fmn2_lle_5000_1000_3/MatMul/ReadVariableOp.^fmn2_lle_5000_1000_out/BiasAdd/ReadVariableOp-^fmn2_lle_5000_1000_out/MatMul/ReadVariableOp,^fmn2_pca_5000_1000_1/BiasAdd/ReadVariableOp+^fmn2_pca_5000_1000_1/MatMul/ReadVariableOp,^fmn2_pca_5000_1000_2/BiasAdd/ReadVariableOp+^fmn2_pca_5000_1000_2/MatMul/ReadVariableOp,^fmn2_pca_5000_1000_3/BiasAdd/ReadVariableOp+^fmn2_pca_5000_1000_3/MatMul/ReadVariableOp.^fmn2_pca_5000_1000_out/BiasAdd/ReadVariableOp-^fmn2_pca_5000_1000_out/MatMul/ReadVariableOp-^fmn2_tsvd_5000_1000_1/BiasAdd/ReadVariableOp,^fmn2_tsvd_5000_1000_1/MatMul/ReadVariableOp-^fmn2_tsvd_5000_1000_2/BiasAdd/ReadVariableOp,^fmn2_tsvd_5000_1000_2/MatMul/ReadVariableOp-^fmn2_tsvd_5000_1000_3/BiasAdd/ReadVariableOp,^fmn2_tsvd_5000_1000_3/MatMul/ReadVariableOp/^fmn2_tsvd_5000_1000_out/BiasAdd/ReadVariableOp.^fmn2_tsvd_5000_1000_out/MatMul/ReadVariableOp^joined/BiasAdd/ReadVariableOp^joined/MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*�
_input_shapes�
�:����������:����������:����������:����������: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 2`
.fmn2_isomap_5000_1000_1/BiasAdd/ReadVariableOp.fmn2_isomap_5000_1000_1/BiasAdd/ReadVariableOp2^
-fmn2_isomap_5000_1000_1/MatMul/ReadVariableOp-fmn2_isomap_5000_1000_1/MatMul/ReadVariableOp2`
.fmn2_isomap_5000_1000_2/BiasAdd/ReadVariableOp.fmn2_isomap_5000_1000_2/BiasAdd/ReadVariableOp2^
-fmn2_isomap_5000_1000_2/MatMul/ReadVariableOp-fmn2_isomap_5000_1000_2/MatMul/ReadVariableOp2`
.fmn2_isomap_5000_1000_3/BiasAdd/ReadVariableOp.fmn2_isomap_5000_1000_3/BiasAdd/ReadVariableOp2^
-fmn2_isomap_5000_1000_3/MatMul/ReadVariableOp-fmn2_isomap_5000_1000_3/MatMul/ReadVariableOp2d
0fmn2_isomap_5000_1000_out/BiasAdd/ReadVariableOp0fmn2_isomap_5000_1000_out/BiasAdd/ReadVariableOp2b
/fmn2_isomap_5000_1000_out/MatMul/ReadVariableOp/fmn2_isomap_5000_1000_out/MatMul/ReadVariableOp2Z
+fmn2_lle_5000_1000_1/BiasAdd/ReadVariableOp+fmn2_lle_5000_1000_1/BiasAdd/ReadVariableOp2X
*fmn2_lle_5000_1000_1/MatMul/ReadVariableOp*fmn2_lle_5000_1000_1/MatMul/ReadVariableOp2Z
+fmn2_lle_5000_1000_2/BiasAdd/ReadVariableOp+fmn2_lle_5000_1000_2/BiasAdd/ReadVariableOp2X
*fmn2_lle_5000_1000_2/MatMul/ReadVariableOp*fmn2_lle_5000_1000_2/MatMul/ReadVariableOp2Z
+fmn2_lle_5000_1000_3/BiasAdd/ReadVariableOp+fmn2_lle_5000_1000_3/BiasAdd/ReadVariableOp2X
*fmn2_lle_5000_1000_3/MatMul/ReadVariableOp*fmn2_lle_5000_1000_3/MatMul/ReadVariableOp2^
-fmn2_lle_5000_1000_out/BiasAdd/ReadVariableOp-fmn2_lle_5000_1000_out/BiasAdd/ReadVariableOp2\
,fmn2_lle_5000_1000_out/MatMul/ReadVariableOp,fmn2_lle_5000_1000_out/MatMul/ReadVariableOp2Z
+fmn2_pca_5000_1000_1/BiasAdd/ReadVariableOp+fmn2_pca_5000_1000_1/BiasAdd/ReadVariableOp2X
*fmn2_pca_5000_1000_1/MatMul/ReadVariableOp*fmn2_pca_5000_1000_1/MatMul/ReadVariableOp2Z
+fmn2_pca_5000_1000_2/BiasAdd/ReadVariableOp+fmn2_pca_5000_1000_2/BiasAdd/ReadVariableOp2X
*fmn2_pca_5000_1000_2/MatMul/ReadVariableOp*fmn2_pca_5000_1000_2/MatMul/ReadVariableOp2Z
+fmn2_pca_5000_1000_3/BiasAdd/ReadVariableOp+fmn2_pca_5000_1000_3/BiasAdd/ReadVariableOp2X
*fmn2_pca_5000_1000_3/MatMul/ReadVariableOp*fmn2_pca_5000_1000_3/MatMul/ReadVariableOp2^
-fmn2_pca_5000_1000_out/BiasAdd/ReadVariableOp-fmn2_pca_5000_1000_out/BiasAdd/ReadVariableOp2\
,fmn2_pca_5000_1000_out/MatMul/ReadVariableOp,fmn2_pca_5000_1000_out/MatMul/ReadVariableOp2\
,fmn2_tsvd_5000_1000_1/BiasAdd/ReadVariableOp,fmn2_tsvd_5000_1000_1/BiasAdd/ReadVariableOp2Z
+fmn2_tsvd_5000_1000_1/MatMul/ReadVariableOp+fmn2_tsvd_5000_1000_1/MatMul/ReadVariableOp2\
,fmn2_tsvd_5000_1000_2/BiasAdd/ReadVariableOp,fmn2_tsvd_5000_1000_2/BiasAdd/ReadVariableOp2Z
+fmn2_tsvd_5000_1000_2/MatMul/ReadVariableOp+fmn2_tsvd_5000_1000_2/MatMul/ReadVariableOp2\
,fmn2_tsvd_5000_1000_3/BiasAdd/ReadVariableOp,fmn2_tsvd_5000_1000_3/BiasAdd/ReadVariableOp2Z
+fmn2_tsvd_5000_1000_3/MatMul/ReadVariableOp+fmn2_tsvd_5000_1000_3/MatMul/ReadVariableOp2`
.fmn2_tsvd_5000_1000_out/BiasAdd/ReadVariableOp.fmn2_tsvd_5000_1000_out/BiasAdd/ReadVariableOp2^
-fmn2_tsvd_5000_1000_out/MatMul/ReadVariableOp-fmn2_tsvd_5000_1000_out/MatMul/ReadVariableOp2>
joined/BiasAdd/ReadVariableOpjoined/BiasAdd/ReadVariableOp2<
joined/MatMul/ReadVariableOpjoined/MatMul/ReadVariableOp:R N
(
_output_shapes
:����������
"
_user_specified_name
inputs/0:RN
(
_output_shapes
:����������
"
_user_specified_name
inputs/1:RN
(
_output_shapes
:����������
"
_user_specified_name
inputs/2:RN
(
_output_shapes
:����������
"
_user_specified_name
inputs/3
�
�
Q__inference_fmn2_lle_5000_1000_1_layer_call_and_return_conditional_losses_1731711

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
R__inference_fmn2_tsvd_5000_1000_2_layer_call_and_return_conditional_losses_1731751

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�u
�
B__inference_model_layer_call_and_return_conditional_losses_1730806

inputs
inputs_1
inputs_2
inputs_30
fmn2_lle_5000_1000_1_1730719:
��+
fmn2_lle_5000_1000_1_1730721:	�3
fmn2_isomap_5000_1000_1_1730724:
��.
fmn2_isomap_5000_1000_1_1730726:	�1
fmn2_tsvd_5000_1000_1_1730729:
��,
fmn2_tsvd_5000_1000_1_1730731:	�0
fmn2_pca_5000_1000_1_1730734:
��+
fmn2_pca_5000_1000_1_1730736:	�0
fmn2_lle_5000_1000_2_1730739:
��+
fmn2_lle_5000_1000_2_1730741:	�3
fmn2_isomap_5000_1000_2_1730744:
��.
fmn2_isomap_5000_1000_2_1730746:	�1
fmn2_tsvd_5000_1000_2_1730749:
��,
fmn2_tsvd_5000_1000_2_1730751:	�0
fmn2_pca_5000_1000_2_1730754:
��+
fmn2_pca_5000_1000_2_1730756:	�0
fmn2_lle_5000_1000_3_1730759:
��+
fmn2_lle_5000_1000_3_1730761:	�3
fmn2_isomap_5000_1000_3_1730764:
��.
fmn2_isomap_5000_1000_3_1730766:	�1
fmn2_tsvd_5000_1000_3_1730769:
��,
fmn2_tsvd_5000_1000_3_1730771:	�0
fmn2_pca_5000_1000_3_1730774:
��+
fmn2_pca_5000_1000_3_1730776:	�1
fmn2_pca_5000_1000_out_1730779:	�,
fmn2_pca_5000_1000_out_1730781:2
fmn2_tsvd_5000_1000_out_1730784:	�-
fmn2_tsvd_5000_1000_out_1730786:4
!fmn2_isomap_5000_1000_out_1730789:	�/
!fmn2_isomap_5000_1000_out_1730791:1
fmn2_lle_5000_1000_out_1730794:	�,
fmn2_lle_5000_1000_out_1730796: 
joined_1730800:
joined_1730802:
identity��/fmn2_isomap_5000_1000_1/StatefulPartitionedCall�/fmn2_isomap_5000_1000_2/StatefulPartitionedCall�/fmn2_isomap_5000_1000_3/StatefulPartitionedCall�1fmn2_isomap_5000_1000_out/StatefulPartitionedCall�,fmn2_lle_5000_1000_1/StatefulPartitionedCall�,fmn2_lle_5000_1000_2/StatefulPartitionedCall�,fmn2_lle_5000_1000_3/StatefulPartitionedCall�.fmn2_lle_5000_1000_out/StatefulPartitionedCall�,fmn2_pca_5000_1000_1/StatefulPartitionedCall�,fmn2_pca_5000_1000_2/StatefulPartitionedCall�,fmn2_pca_5000_1000_3/StatefulPartitionedCall�.fmn2_pca_5000_1000_out/StatefulPartitionedCall�-fmn2_tsvd_5000_1000_1/StatefulPartitionedCall�-fmn2_tsvd_5000_1000_2/StatefulPartitionedCall�-fmn2_tsvd_5000_1000_3/StatefulPartitionedCall�/fmn2_tsvd_5000_1000_out/StatefulPartitionedCall�joined/StatefulPartitionedCall�
,fmn2_lle_5000_1000_1/StatefulPartitionedCallStatefulPartitionedCallinputs_3fmn2_lle_5000_1000_1_1730719fmn2_lle_5000_1000_1_1730721*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_fmn2_lle_5000_1000_1_layer_call_and_return_conditional_losses_17300922.
,fmn2_lle_5000_1000_1/StatefulPartitionedCall�
/fmn2_isomap_5000_1000_1/StatefulPartitionedCallStatefulPartitionedCallinputs_2fmn2_isomap_5000_1000_1_1730724fmn2_isomap_5000_1000_1_1730726*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *]
fXRV
T__inference_fmn2_isomap_5000_1000_1_layer_call_and_return_conditional_losses_173010921
/fmn2_isomap_5000_1000_1/StatefulPartitionedCall�
-fmn2_tsvd_5000_1000_1/StatefulPartitionedCallStatefulPartitionedCallinputs_1fmn2_tsvd_5000_1000_1_1730729fmn2_tsvd_5000_1000_1_1730731*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_fmn2_tsvd_5000_1000_1_layer_call_and_return_conditional_losses_17301262/
-fmn2_tsvd_5000_1000_1/StatefulPartitionedCall�
,fmn2_pca_5000_1000_1/StatefulPartitionedCallStatefulPartitionedCallinputsfmn2_pca_5000_1000_1_1730734fmn2_pca_5000_1000_1_1730736*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_fmn2_pca_5000_1000_1_layer_call_and_return_conditional_losses_17301432.
,fmn2_pca_5000_1000_1/StatefulPartitionedCall�
,fmn2_lle_5000_1000_2/StatefulPartitionedCallStatefulPartitionedCall5fmn2_lle_5000_1000_1/StatefulPartitionedCall:output:0fmn2_lle_5000_1000_2_1730739fmn2_lle_5000_1000_2_1730741*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_fmn2_lle_5000_1000_2_layer_call_and_return_conditional_losses_17301602.
,fmn2_lle_5000_1000_2/StatefulPartitionedCall�
/fmn2_isomap_5000_1000_2/StatefulPartitionedCallStatefulPartitionedCall8fmn2_isomap_5000_1000_1/StatefulPartitionedCall:output:0fmn2_isomap_5000_1000_2_1730744fmn2_isomap_5000_1000_2_1730746*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *]
fXRV
T__inference_fmn2_isomap_5000_1000_2_layer_call_and_return_conditional_losses_173017721
/fmn2_isomap_5000_1000_2/StatefulPartitionedCall�
-fmn2_tsvd_5000_1000_2/StatefulPartitionedCallStatefulPartitionedCall6fmn2_tsvd_5000_1000_1/StatefulPartitionedCall:output:0fmn2_tsvd_5000_1000_2_1730749fmn2_tsvd_5000_1000_2_1730751*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_fmn2_tsvd_5000_1000_2_layer_call_and_return_conditional_losses_17301942/
-fmn2_tsvd_5000_1000_2/StatefulPartitionedCall�
,fmn2_pca_5000_1000_2/StatefulPartitionedCallStatefulPartitionedCall5fmn2_pca_5000_1000_1/StatefulPartitionedCall:output:0fmn2_pca_5000_1000_2_1730754fmn2_pca_5000_1000_2_1730756*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_fmn2_pca_5000_1000_2_layer_call_and_return_conditional_losses_17302112.
,fmn2_pca_5000_1000_2/StatefulPartitionedCall�
,fmn2_lle_5000_1000_3/StatefulPartitionedCallStatefulPartitionedCall5fmn2_lle_5000_1000_2/StatefulPartitionedCall:output:0fmn2_lle_5000_1000_3_1730759fmn2_lle_5000_1000_3_1730761*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_fmn2_lle_5000_1000_3_layer_call_and_return_conditional_losses_17302282.
,fmn2_lle_5000_1000_3/StatefulPartitionedCall�
/fmn2_isomap_5000_1000_3/StatefulPartitionedCallStatefulPartitionedCall8fmn2_isomap_5000_1000_2/StatefulPartitionedCall:output:0fmn2_isomap_5000_1000_3_1730764fmn2_isomap_5000_1000_3_1730766*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *]
fXRV
T__inference_fmn2_isomap_5000_1000_3_layer_call_and_return_conditional_losses_173024521
/fmn2_isomap_5000_1000_3/StatefulPartitionedCall�
-fmn2_tsvd_5000_1000_3/StatefulPartitionedCallStatefulPartitionedCall6fmn2_tsvd_5000_1000_2/StatefulPartitionedCall:output:0fmn2_tsvd_5000_1000_3_1730769fmn2_tsvd_5000_1000_3_1730771*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_fmn2_tsvd_5000_1000_3_layer_call_and_return_conditional_losses_17302622/
-fmn2_tsvd_5000_1000_3/StatefulPartitionedCall�
,fmn2_pca_5000_1000_3/StatefulPartitionedCallStatefulPartitionedCall5fmn2_pca_5000_1000_2/StatefulPartitionedCall:output:0fmn2_pca_5000_1000_3_1730774fmn2_pca_5000_1000_3_1730776*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_fmn2_pca_5000_1000_3_layer_call_and_return_conditional_losses_17302792.
,fmn2_pca_5000_1000_3/StatefulPartitionedCall�
.fmn2_pca_5000_1000_out/StatefulPartitionedCallStatefulPartitionedCall5fmn2_pca_5000_1000_3/StatefulPartitionedCall:output:0fmn2_pca_5000_1000_out_1730779fmn2_pca_5000_1000_out_1730781*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *\
fWRU
S__inference_fmn2_pca_5000_1000_out_layer_call_and_return_conditional_losses_173029620
.fmn2_pca_5000_1000_out/StatefulPartitionedCall�
/fmn2_tsvd_5000_1000_out/StatefulPartitionedCallStatefulPartitionedCall6fmn2_tsvd_5000_1000_3/StatefulPartitionedCall:output:0fmn2_tsvd_5000_1000_out_1730784fmn2_tsvd_5000_1000_out_1730786*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *]
fXRV
T__inference_fmn2_tsvd_5000_1000_out_layer_call_and_return_conditional_losses_173031321
/fmn2_tsvd_5000_1000_out/StatefulPartitionedCall�
1fmn2_isomap_5000_1000_out/StatefulPartitionedCallStatefulPartitionedCall8fmn2_isomap_5000_1000_3/StatefulPartitionedCall:output:0!fmn2_isomap_5000_1000_out_1730789!fmn2_isomap_5000_1000_out_1730791*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *_
fZRX
V__inference_fmn2_isomap_5000_1000_out_layer_call_and_return_conditional_losses_173033023
1fmn2_isomap_5000_1000_out/StatefulPartitionedCall�
.fmn2_lle_5000_1000_out/StatefulPartitionedCallStatefulPartitionedCall5fmn2_lle_5000_1000_3/StatefulPartitionedCall:output:0fmn2_lle_5000_1000_out_1730794fmn2_lle_5000_1000_out_1730796*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *\
fWRU
S__inference_fmn2_lle_5000_1000_out_layer_call_and_return_conditional_losses_173034720
.fmn2_lle_5000_1000_out/StatefulPartitionedCall�
concatenate_51/PartitionedCallPartitionedCall7fmn2_pca_5000_1000_out/StatefulPartitionedCall:output:08fmn2_tsvd_5000_1000_out/StatefulPartitionedCall:output:0:fmn2_isomap_5000_1000_out/StatefulPartitionedCall:output:07fmn2_lle_5000_1000_out/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *T
fORM
K__inference_concatenate_51_layer_call_and_return_conditional_losses_17303622 
concatenate_51/PartitionedCall�
joined/StatefulPartitionedCallStatefulPartitionedCall'concatenate_51/PartitionedCall:output:0joined_1730800joined_1730802*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *L
fGRE
C__inference_joined_layer_call_and_return_conditional_losses_17303752 
joined/StatefulPartitionedCall�
IdentityIdentity'joined/StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identity�
NoOpNoOp0^fmn2_isomap_5000_1000_1/StatefulPartitionedCall0^fmn2_isomap_5000_1000_2/StatefulPartitionedCall0^fmn2_isomap_5000_1000_3/StatefulPartitionedCall2^fmn2_isomap_5000_1000_out/StatefulPartitionedCall-^fmn2_lle_5000_1000_1/StatefulPartitionedCall-^fmn2_lle_5000_1000_2/StatefulPartitionedCall-^fmn2_lle_5000_1000_3/StatefulPartitionedCall/^fmn2_lle_5000_1000_out/StatefulPartitionedCall-^fmn2_pca_5000_1000_1/StatefulPartitionedCall-^fmn2_pca_5000_1000_2/StatefulPartitionedCall-^fmn2_pca_5000_1000_3/StatefulPartitionedCall/^fmn2_pca_5000_1000_out/StatefulPartitionedCall.^fmn2_tsvd_5000_1000_1/StatefulPartitionedCall.^fmn2_tsvd_5000_1000_2/StatefulPartitionedCall.^fmn2_tsvd_5000_1000_3/StatefulPartitionedCall0^fmn2_tsvd_5000_1000_out/StatefulPartitionedCall^joined/StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*�
_input_shapes�
�:����������:����������:����������:����������: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 2b
/fmn2_isomap_5000_1000_1/StatefulPartitionedCall/fmn2_isomap_5000_1000_1/StatefulPartitionedCall2b
/fmn2_isomap_5000_1000_2/StatefulPartitionedCall/fmn2_isomap_5000_1000_2/StatefulPartitionedCall2b
/fmn2_isomap_5000_1000_3/StatefulPartitionedCall/fmn2_isomap_5000_1000_3/StatefulPartitionedCall2f
1fmn2_isomap_5000_1000_out/StatefulPartitionedCall1fmn2_isomap_5000_1000_out/StatefulPartitionedCall2\
,fmn2_lle_5000_1000_1/StatefulPartitionedCall,fmn2_lle_5000_1000_1/StatefulPartitionedCall2\
,fmn2_lle_5000_1000_2/StatefulPartitionedCall,fmn2_lle_5000_1000_2/StatefulPartitionedCall2\
,fmn2_lle_5000_1000_3/StatefulPartitionedCall,fmn2_lle_5000_1000_3/StatefulPartitionedCall2`
.fmn2_lle_5000_1000_out/StatefulPartitionedCall.fmn2_lle_5000_1000_out/StatefulPartitionedCall2\
,fmn2_pca_5000_1000_1/StatefulPartitionedCall,fmn2_pca_5000_1000_1/StatefulPartitionedCall2\
,fmn2_pca_5000_1000_2/StatefulPartitionedCall,fmn2_pca_5000_1000_2/StatefulPartitionedCall2\
,fmn2_pca_5000_1000_3/StatefulPartitionedCall,fmn2_pca_5000_1000_3/StatefulPartitionedCall2`
.fmn2_pca_5000_1000_out/StatefulPartitionedCall.fmn2_pca_5000_1000_out/StatefulPartitionedCall2^
-fmn2_tsvd_5000_1000_1/StatefulPartitionedCall-fmn2_tsvd_5000_1000_1/StatefulPartitionedCall2^
-fmn2_tsvd_5000_1000_2/StatefulPartitionedCall-fmn2_tsvd_5000_1000_2/StatefulPartitionedCall2^
-fmn2_tsvd_5000_1000_3/StatefulPartitionedCall-fmn2_tsvd_5000_1000_3/StatefulPartitionedCall2b
/fmn2_tsvd_5000_1000_out/StatefulPartitionedCall/fmn2_tsvd_5000_1000_out/StatefulPartitionedCall2@
joined/StatefulPartitionedCalljoined/StatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs:PL
(
_output_shapes
:����������
 
_user_specified_nameinputs:PL
(
_output_shapes
:����������
 
_user_specified_nameinputs:PL
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
Q__inference_fmn2_pca_5000_1000_1_layer_call_and_return_conditional_losses_1731651

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
'__inference_model_layer_call_fn_1731375
inputs_0
inputs_1
inputs_2
inputs_3
unknown:
��
	unknown_0:	�
	unknown_1:
��
	unknown_2:	�
	unknown_3:
��
	unknown_4:	�
	unknown_5:
��
	unknown_6:	�
	unknown_7:
��
	unknown_8:	�
	unknown_9:
��

unknown_10:	�

unknown_11:
��

unknown_12:	�

unknown_13:
��

unknown_14:	�

unknown_15:
��

unknown_16:	�

unknown_17:
��

unknown_18:	�

unknown_19:
��

unknown_20:	�

unknown_21:
��

unknown_22:	�

unknown_23:	�

unknown_24:

unknown_25:	�

unknown_26:

unknown_27:	�

unknown_28:

unknown_29:	�

unknown_30:

unknown_31:

unknown_32:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputs_0inputs_1inputs_2inputs_3unknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10
unknown_11
unknown_12
unknown_13
unknown_14
unknown_15
unknown_16
unknown_17
unknown_18
unknown_19
unknown_20
unknown_21
unknown_22
unknown_23
unknown_24
unknown_25
unknown_26
unknown_27
unknown_28
unknown_29
unknown_30
unknown_31
unknown_32*1
Tin*
(2&*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*D
_read_only_resource_inputs&
$"	
 !"#$%*0
config_proto 

CPU

GPU2*0J 8� *K
fFRD
B__inference_model_layer_call_and_return_conditional_losses_17308062
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*�
_input_shapes�
�:����������:����������:����������:����������: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:R N
(
_output_shapes
:����������
"
_user_specified_name
inputs/0:RN
(
_output_shapes
:����������
"
_user_specified_name
inputs/1:RN
(
_output_shapes
:����������
"
_user_specified_name
inputs/2:RN
(
_output_shapes
:����������
"
_user_specified_name
inputs/3
�
�
S__inference_fmn2_pca_5000_1000_out_layer_call_and_return_conditional_losses_1730296

inputs1
matmul_readvariableop_resource:	�-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes
:	�*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2	
BiasAdda
SigmoidSigmoidBiasAdd:output:0*
T0*'
_output_shapes
:���������2	
Sigmoidf
IdentityIdentitySigmoid:y:0^NoOp*
T0*'
_output_shapes
:���������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�`
�
 __inference__traced_save_1732155
file_prefix:
6savev2_fmn2_pca_5000_1000_1_kernel_read_readvariableop8
4savev2_fmn2_pca_5000_1000_1_bias_read_readvariableop;
7savev2_fmn2_tsvd_5000_1000_1_kernel_read_readvariableop9
5savev2_fmn2_tsvd_5000_1000_1_bias_read_readvariableop=
9savev2_fmn2_isomap_5000_1000_1_kernel_read_readvariableop;
7savev2_fmn2_isomap_5000_1000_1_bias_read_readvariableop:
6savev2_fmn2_lle_5000_1000_1_kernel_read_readvariableop8
4savev2_fmn2_lle_5000_1000_1_bias_read_readvariableop:
6savev2_fmn2_pca_5000_1000_2_kernel_read_readvariableop8
4savev2_fmn2_pca_5000_1000_2_bias_read_readvariableop;
7savev2_fmn2_tsvd_5000_1000_2_kernel_read_readvariableop9
5savev2_fmn2_tsvd_5000_1000_2_bias_read_readvariableop=
9savev2_fmn2_isomap_5000_1000_2_kernel_read_readvariableop;
7savev2_fmn2_isomap_5000_1000_2_bias_read_readvariableop:
6savev2_fmn2_lle_5000_1000_2_kernel_read_readvariableop8
4savev2_fmn2_lle_5000_1000_2_bias_read_readvariableop:
6savev2_fmn2_pca_5000_1000_3_kernel_read_readvariableop8
4savev2_fmn2_pca_5000_1000_3_bias_read_readvariableop;
7savev2_fmn2_tsvd_5000_1000_3_kernel_read_readvariableop9
5savev2_fmn2_tsvd_5000_1000_3_bias_read_readvariableop=
9savev2_fmn2_isomap_5000_1000_3_kernel_read_readvariableop;
7savev2_fmn2_isomap_5000_1000_3_bias_read_readvariableop:
6savev2_fmn2_lle_5000_1000_3_kernel_read_readvariableop8
4savev2_fmn2_lle_5000_1000_3_bias_read_readvariableop<
8savev2_fmn2_pca_5000_1000_out_kernel_read_readvariableop:
6savev2_fmn2_pca_5000_1000_out_bias_read_readvariableop=
9savev2_fmn2_tsvd_5000_1000_out_kernel_read_readvariableop;
7savev2_fmn2_tsvd_5000_1000_out_bias_read_readvariableop?
;savev2_fmn2_isomap_5000_1000_out_kernel_read_readvariableop=
9savev2_fmn2_isomap_5000_1000_out_bias_read_readvariableop<
8savev2_fmn2_lle_5000_1000_out_kernel_read_readvariableop:
6savev2_fmn2_lle_5000_1000_out_bias_read_readvariableop,
(savev2_joined_kernel_read_readvariableop*
&savev2_joined_bias_read_readvariableop(
$savev2_adam_iter_read_readvariableop	*
&savev2_adam_beta_1_read_readvariableop*
&savev2_adam_beta_2_read_readvariableop)
%savev2_adam_decay_read_readvariableop1
-savev2_adam_learning_rate_read_readvariableop$
 savev2_total_read_readvariableop$
 savev2_count_read_readvariableop&
"savev2_total_1_read_readvariableop&
"savev2_count_1_read_readvariableop3
/savev2_adam_joined_kernel_m_read_readvariableop1
-savev2_adam_joined_bias_m_read_readvariableop3
/savev2_adam_joined_kernel_v_read_readvariableop1
-savev2_adam_joined_bias_v_read_readvariableop
savev2_const

identity_1��MergeV2Checkpoints�
StaticRegexFullMatchStaticRegexFullMatchfile_prefix"/device:CPU:**
_output_shapes
: *
pattern
^s3://.*2
StaticRegexFullMatchc
ConstConst"/device:CPU:**
_output_shapes
: *
dtype0*
valueB B.part2
Constl
Const_1Const"/device:CPU:**
_output_shapes
: *
dtype0*
valueB B
_temp/part2	
Const_1�
SelectSelectStaticRegexFullMatch:output:0Const:output:0Const_1:output:0"/device:CPU:**
T0*
_output_shapes
: 2
Selectt

StringJoin
StringJoinfile_prefixSelect:output:0"/device:CPU:**
N*
_output_shapes
: 2

StringJoinZ

num_shardsConst*
_output_shapes
: *
dtype0*
value	B :2

num_shards
ShardedFilename/shardConst"/device:CPU:0*
_output_shapes
: *
dtype0*
value	B : 2
ShardedFilename/shard�
ShardedFilenameShardedFilenameStringJoin:output:0ShardedFilename/shard:output:0num_shards:output:0"/device:CPU:0*
_output_shapes
: 2
ShardedFilename�
SaveV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:0*
dtype0*�
value�B�0B6layer_with_weights-0/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-0/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-1/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-1/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-3/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-3/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-4/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-4/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-5/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-5/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-6/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-6/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-7/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-7/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-8/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-8/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-9/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-9/bias/.ATTRIBUTES/VARIABLE_VALUEB7layer_with_weights-10/kernel/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-10/bias/.ATTRIBUTES/VARIABLE_VALUEB7layer_with_weights-11/kernel/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-11/bias/.ATTRIBUTES/VARIABLE_VALUEB7layer_with_weights-12/kernel/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-12/bias/.ATTRIBUTES/VARIABLE_VALUEB7layer_with_weights-13/kernel/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-13/bias/.ATTRIBUTES/VARIABLE_VALUEB7layer_with_weights-14/kernel/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-14/bias/.ATTRIBUTES/VARIABLE_VALUEB7layer_with_weights-15/kernel/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-15/bias/.ATTRIBUTES/VARIABLE_VALUEB7layer_with_weights-16/kernel/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-16/bias/.ATTRIBUTES/VARIABLE_VALUEB)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUEB*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/count/.ATTRIBUTES/VARIABLE_VALUEBSlayer_with_weights-16/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-16/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBSlayer_with_weights-16/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-16/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEB_CHECKPOINTABLE_OBJECT_GRAPH2
SaveV2/tensor_names�
SaveV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:0*
dtype0*s
valuejBh0B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B 2
SaveV2/shape_and_slices�
SaveV2SaveV2ShardedFilename:filename:0SaveV2/tensor_names:output:0 SaveV2/shape_and_slices:output:06savev2_fmn2_pca_5000_1000_1_kernel_read_readvariableop4savev2_fmn2_pca_5000_1000_1_bias_read_readvariableop7savev2_fmn2_tsvd_5000_1000_1_kernel_read_readvariableop5savev2_fmn2_tsvd_5000_1000_1_bias_read_readvariableop9savev2_fmn2_isomap_5000_1000_1_kernel_read_readvariableop7savev2_fmn2_isomap_5000_1000_1_bias_read_readvariableop6savev2_fmn2_lle_5000_1000_1_kernel_read_readvariableop4savev2_fmn2_lle_5000_1000_1_bias_read_readvariableop6savev2_fmn2_pca_5000_1000_2_kernel_read_readvariableop4savev2_fmn2_pca_5000_1000_2_bias_read_readvariableop7savev2_fmn2_tsvd_5000_1000_2_kernel_read_readvariableop5savev2_fmn2_tsvd_5000_1000_2_bias_read_readvariableop9savev2_fmn2_isomap_5000_1000_2_kernel_read_readvariableop7savev2_fmn2_isomap_5000_1000_2_bias_read_readvariableop6savev2_fmn2_lle_5000_1000_2_kernel_read_readvariableop4savev2_fmn2_lle_5000_1000_2_bias_read_readvariableop6savev2_fmn2_pca_5000_1000_3_kernel_read_readvariableop4savev2_fmn2_pca_5000_1000_3_bias_read_readvariableop7savev2_fmn2_tsvd_5000_1000_3_kernel_read_readvariableop5savev2_fmn2_tsvd_5000_1000_3_bias_read_readvariableop9savev2_fmn2_isomap_5000_1000_3_kernel_read_readvariableop7savev2_fmn2_isomap_5000_1000_3_bias_read_readvariableop6savev2_fmn2_lle_5000_1000_3_kernel_read_readvariableop4savev2_fmn2_lle_5000_1000_3_bias_read_readvariableop8savev2_fmn2_pca_5000_1000_out_kernel_read_readvariableop6savev2_fmn2_pca_5000_1000_out_bias_read_readvariableop9savev2_fmn2_tsvd_5000_1000_out_kernel_read_readvariableop7savev2_fmn2_tsvd_5000_1000_out_bias_read_readvariableop;savev2_fmn2_isomap_5000_1000_out_kernel_read_readvariableop9savev2_fmn2_isomap_5000_1000_out_bias_read_readvariableop8savev2_fmn2_lle_5000_1000_out_kernel_read_readvariableop6savev2_fmn2_lle_5000_1000_out_bias_read_readvariableop(savev2_joined_kernel_read_readvariableop&savev2_joined_bias_read_readvariableop$savev2_adam_iter_read_readvariableop&savev2_adam_beta_1_read_readvariableop&savev2_adam_beta_2_read_readvariableop%savev2_adam_decay_read_readvariableop-savev2_adam_learning_rate_read_readvariableop savev2_total_read_readvariableop savev2_count_read_readvariableop"savev2_total_1_read_readvariableop"savev2_count_1_read_readvariableop/savev2_adam_joined_kernel_m_read_readvariableop-savev2_adam_joined_bias_m_read_readvariableop/savev2_adam_joined_kernel_v_read_readvariableop-savev2_adam_joined_bias_v_read_readvariableopsavev2_const"/device:CPU:0*
_output_shapes
 *>
dtypes4
220	2
SaveV2�
&MergeV2Checkpoints/checkpoint_prefixesPackShardedFilename:filename:0^SaveV2"/device:CPU:0*
N*
T0*
_output_shapes
:2(
&MergeV2Checkpoints/checkpoint_prefixes�
MergeV2CheckpointsMergeV2Checkpoints/MergeV2Checkpoints/checkpoint_prefixes:output:0file_prefix"/device:CPU:0*
_output_shapes
 2
MergeV2Checkpointsr
IdentityIdentityfile_prefix^MergeV2Checkpoints"/device:CPU:0*
T0*
_output_shapes
: 2

Identity_

Identity_1IdentityIdentity:output:0^NoOp*
T0*
_output_shapes
: 2

Identity_1c
NoOpNoOp^MergeV2Checkpoints*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"!

identity_1Identity_1:output:0*�
_input_shapes�
�: :
��:�:
��:�:
��:�:
��:�:
��:�:
��:�:
��:�:
��:�:
��:�:
��:�:
��:�:
��:�:	�::	�::	�::	�:::: : : : : : : : : ::::: 2(
MergeV2CheckpointsMergeV2Checkpoints:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix:&"
 
_output_shapes
:
��:!

_output_shapes	
:�:&"
 
_output_shapes
:
��:!

_output_shapes	
:�:&"
 
_output_shapes
:
��:!

_output_shapes	
:�:&"
 
_output_shapes
:
��:!

_output_shapes	
:�:&	"
 
_output_shapes
:
��:!


_output_shapes	
:�:&"
 
_output_shapes
:
��:!

_output_shapes	
:�:&"
 
_output_shapes
:
��:!

_output_shapes	
:�:&"
 
_output_shapes
:
��:!

_output_shapes	
:�:&"
 
_output_shapes
:
��:!

_output_shapes	
:�:&"
 
_output_shapes
:
��:!

_output_shapes	
:�:&"
 
_output_shapes
:
��:!

_output_shapes	
:�:&"
 
_output_shapes
:
��:!

_output_shapes	
:�:%!

_output_shapes
:	�: 

_output_shapes
::%!

_output_shapes
:	�: 

_output_shapes
::%!

_output_shapes
:	�: 

_output_shapes
::%!

_output_shapes
:	�:  

_output_shapes
::$! 

_output_shapes

:: "

_output_shapes
::#

_output_shapes
: :$

_output_shapes
: :%

_output_shapes
: :&

_output_shapes
: :'

_output_shapes
: :(

_output_shapes
: :)

_output_shapes
: :*

_output_shapes
: :+

_output_shapes
: :$, 

_output_shapes

:: -

_output_shapes
::$. 

_output_shapes

:: /

_output_shapes
::0

_output_shapes
: 
�
�
Q__inference_fmn2_lle_5000_1000_2_layer_call_and_return_conditional_losses_1731791

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
Q__inference_fmn2_pca_5000_1000_3_layer_call_and_return_conditional_losses_1731811

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
7__inference_fmn2_tsvd_5000_1000_1_layer_call_fn_1731660

inputs
unknown:
��
	unknown_0:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_fmn2_tsvd_5000_1000_1_layer_call_and_return_conditional_losses_17301262
StatefulPartitionedCall|
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:����������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
S__inference_fmn2_pca_5000_1000_out_layer_call_and_return_conditional_losses_1731891

inputs1
matmul_readvariableop_resource:	�-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes
:	�*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2	
BiasAdda
SigmoidSigmoidBiasAdd:output:0*
T0*'
_output_shapes
:���������2	
Sigmoidf
IdentityIdentitySigmoid:y:0^NoOp*
T0*'
_output_shapes
:���������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
Q__inference_fmn2_pca_5000_1000_1_layer_call_and_return_conditional_losses_1730143

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
R__inference_fmn2_tsvd_5000_1000_1_layer_call_and_return_conditional_losses_1730126

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
Q__inference_fmn2_pca_5000_1000_3_layer_call_and_return_conditional_losses_1730279

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
T__inference_fmn2_tsvd_5000_1000_out_layer_call_and_return_conditional_losses_1731911

inputs1
matmul_readvariableop_resource:	�-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes
:	�*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2	
BiasAdda
SigmoidSigmoidBiasAdd:output:0*
T0*'
_output_shapes
:���������2	
Sigmoidf
IdentityIdentitySigmoid:y:0^NoOp*
T0*'
_output_shapes
:���������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�	
'__inference_model_layer_call_fn_1730953
fmn2_pca_5000_1000_input
fmn2_tsvd_5000_1000_input
fmn2_isomap_5000_1000_input
fmn2_lle_5000_1000_input
unknown:
��
	unknown_0:	�
	unknown_1:
��
	unknown_2:	�
	unknown_3:
��
	unknown_4:	�
	unknown_5:
��
	unknown_6:	�
	unknown_7:
��
	unknown_8:	�
	unknown_9:
��

unknown_10:	�

unknown_11:
��

unknown_12:	�

unknown_13:
��

unknown_14:	�

unknown_15:
��

unknown_16:	�

unknown_17:
��

unknown_18:	�

unknown_19:
��

unknown_20:	�

unknown_21:
��

unknown_22:	�

unknown_23:	�

unknown_24:

unknown_25:	�

unknown_26:

unknown_27:	�

unknown_28:

unknown_29:	�

unknown_30:

unknown_31:

unknown_32:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallfmn2_pca_5000_1000_inputfmn2_tsvd_5000_1000_inputfmn2_isomap_5000_1000_inputfmn2_lle_5000_1000_inputunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10
unknown_11
unknown_12
unknown_13
unknown_14
unknown_15
unknown_16
unknown_17
unknown_18
unknown_19
unknown_20
unknown_21
unknown_22
unknown_23
unknown_24
unknown_25
unknown_26
unknown_27
unknown_28
unknown_29
unknown_30
unknown_31
unknown_32*1
Tin*
(2&*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*D
_read_only_resource_inputs&
$"	
 !"#$%*0
config_proto 

CPU

GPU2*0J 8� *K
fFRD
B__inference_model_layer_call_and_return_conditional_losses_17308062
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*�
_input_shapes�
�:����������:����������:����������:����������: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:b ^
(
_output_shapes
:����������
2
_user_specified_namefmn2_pca_5000_1000_input:c_
(
_output_shapes
:����������
3
_user_specified_namefmn2_tsvd_5000_1000_input:ea
(
_output_shapes
:����������
5
_user_specified_namefmn2_isomap_5000_1000_input:b^
(
_output_shapes
:����������
2
_user_specified_namefmn2_lle_5000_1000_input
�
�
K__inference_concatenate_51_layer_call_and_return_conditional_losses_1730362

inputs
inputs_1
inputs_2
inputs_3
identity\
concat/axisConst*
_output_shapes
: *
dtype0*
value	B :2
concat/axis�
concatConcatV2inputsinputs_1inputs_2inputs_3concat/axis:output:0*
N*
T0*'
_output_shapes
:���������2
concatc
IdentityIdentityconcat:output:0*
T0*'
_output_shapes
:���������2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*_
_input_shapesN
L:���������:���������:���������:���������:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs:OK
'
_output_shapes
:���������
 
_user_specified_nameinputs:OK
'
_output_shapes
:���������
 
_user_specified_nameinputs:OK
'
_output_shapes
:���������
 
_user_specified_nameinputs
�
�
T__inference_fmn2_isomap_5000_1000_2_layer_call_and_return_conditional_losses_1731771

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
R__inference_fmn2_tsvd_5000_1000_2_layer_call_and_return_conditional_losses_1730194

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
Q__inference_fmn2_pca_5000_1000_2_layer_call_and_return_conditional_losses_1731731

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
Q__inference_fmn2_lle_5000_1000_2_layer_call_and_return_conditional_losses_1730160

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
Q__inference_fmn2_lle_5000_1000_3_layer_call_and_return_conditional_losses_1730228

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�w
�
B__inference_model_layer_call_and_return_conditional_losses_1731139
fmn2_pca_5000_1000_input
fmn2_tsvd_5000_1000_input
fmn2_isomap_5000_1000_input
fmn2_lle_5000_1000_input0
fmn2_lle_5000_1000_1_1731052:
��+
fmn2_lle_5000_1000_1_1731054:	�3
fmn2_isomap_5000_1000_1_1731057:
��.
fmn2_isomap_5000_1000_1_1731059:	�1
fmn2_tsvd_5000_1000_1_1731062:
��,
fmn2_tsvd_5000_1000_1_1731064:	�0
fmn2_pca_5000_1000_1_1731067:
��+
fmn2_pca_5000_1000_1_1731069:	�0
fmn2_lle_5000_1000_2_1731072:
��+
fmn2_lle_5000_1000_2_1731074:	�3
fmn2_isomap_5000_1000_2_1731077:
��.
fmn2_isomap_5000_1000_2_1731079:	�1
fmn2_tsvd_5000_1000_2_1731082:
��,
fmn2_tsvd_5000_1000_2_1731084:	�0
fmn2_pca_5000_1000_2_1731087:
��+
fmn2_pca_5000_1000_2_1731089:	�0
fmn2_lle_5000_1000_3_1731092:
��+
fmn2_lle_5000_1000_3_1731094:	�3
fmn2_isomap_5000_1000_3_1731097:
��.
fmn2_isomap_5000_1000_3_1731099:	�1
fmn2_tsvd_5000_1000_3_1731102:
��,
fmn2_tsvd_5000_1000_3_1731104:	�0
fmn2_pca_5000_1000_3_1731107:
��+
fmn2_pca_5000_1000_3_1731109:	�1
fmn2_pca_5000_1000_out_1731112:	�,
fmn2_pca_5000_1000_out_1731114:2
fmn2_tsvd_5000_1000_out_1731117:	�-
fmn2_tsvd_5000_1000_out_1731119:4
!fmn2_isomap_5000_1000_out_1731122:	�/
!fmn2_isomap_5000_1000_out_1731124:1
fmn2_lle_5000_1000_out_1731127:	�,
fmn2_lle_5000_1000_out_1731129: 
joined_1731133:
joined_1731135:
identity��/fmn2_isomap_5000_1000_1/StatefulPartitionedCall�/fmn2_isomap_5000_1000_2/StatefulPartitionedCall�/fmn2_isomap_5000_1000_3/StatefulPartitionedCall�1fmn2_isomap_5000_1000_out/StatefulPartitionedCall�,fmn2_lle_5000_1000_1/StatefulPartitionedCall�,fmn2_lle_5000_1000_2/StatefulPartitionedCall�,fmn2_lle_5000_1000_3/StatefulPartitionedCall�.fmn2_lle_5000_1000_out/StatefulPartitionedCall�,fmn2_pca_5000_1000_1/StatefulPartitionedCall�,fmn2_pca_5000_1000_2/StatefulPartitionedCall�,fmn2_pca_5000_1000_3/StatefulPartitionedCall�.fmn2_pca_5000_1000_out/StatefulPartitionedCall�-fmn2_tsvd_5000_1000_1/StatefulPartitionedCall�-fmn2_tsvd_5000_1000_2/StatefulPartitionedCall�-fmn2_tsvd_5000_1000_3/StatefulPartitionedCall�/fmn2_tsvd_5000_1000_out/StatefulPartitionedCall�joined/StatefulPartitionedCall�
,fmn2_lle_5000_1000_1/StatefulPartitionedCallStatefulPartitionedCallfmn2_lle_5000_1000_inputfmn2_lle_5000_1000_1_1731052fmn2_lle_5000_1000_1_1731054*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_fmn2_lle_5000_1000_1_layer_call_and_return_conditional_losses_17300922.
,fmn2_lle_5000_1000_1/StatefulPartitionedCall�
/fmn2_isomap_5000_1000_1/StatefulPartitionedCallStatefulPartitionedCallfmn2_isomap_5000_1000_inputfmn2_isomap_5000_1000_1_1731057fmn2_isomap_5000_1000_1_1731059*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *]
fXRV
T__inference_fmn2_isomap_5000_1000_1_layer_call_and_return_conditional_losses_173010921
/fmn2_isomap_5000_1000_1/StatefulPartitionedCall�
-fmn2_tsvd_5000_1000_1/StatefulPartitionedCallStatefulPartitionedCallfmn2_tsvd_5000_1000_inputfmn2_tsvd_5000_1000_1_1731062fmn2_tsvd_5000_1000_1_1731064*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_fmn2_tsvd_5000_1000_1_layer_call_and_return_conditional_losses_17301262/
-fmn2_tsvd_5000_1000_1/StatefulPartitionedCall�
,fmn2_pca_5000_1000_1/StatefulPartitionedCallStatefulPartitionedCallfmn2_pca_5000_1000_inputfmn2_pca_5000_1000_1_1731067fmn2_pca_5000_1000_1_1731069*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_fmn2_pca_5000_1000_1_layer_call_and_return_conditional_losses_17301432.
,fmn2_pca_5000_1000_1/StatefulPartitionedCall�
,fmn2_lle_5000_1000_2/StatefulPartitionedCallStatefulPartitionedCall5fmn2_lle_5000_1000_1/StatefulPartitionedCall:output:0fmn2_lle_5000_1000_2_1731072fmn2_lle_5000_1000_2_1731074*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_fmn2_lle_5000_1000_2_layer_call_and_return_conditional_losses_17301602.
,fmn2_lle_5000_1000_2/StatefulPartitionedCall�
/fmn2_isomap_5000_1000_2/StatefulPartitionedCallStatefulPartitionedCall8fmn2_isomap_5000_1000_1/StatefulPartitionedCall:output:0fmn2_isomap_5000_1000_2_1731077fmn2_isomap_5000_1000_2_1731079*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *]
fXRV
T__inference_fmn2_isomap_5000_1000_2_layer_call_and_return_conditional_losses_173017721
/fmn2_isomap_5000_1000_2/StatefulPartitionedCall�
-fmn2_tsvd_5000_1000_2/StatefulPartitionedCallStatefulPartitionedCall6fmn2_tsvd_5000_1000_1/StatefulPartitionedCall:output:0fmn2_tsvd_5000_1000_2_1731082fmn2_tsvd_5000_1000_2_1731084*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_fmn2_tsvd_5000_1000_2_layer_call_and_return_conditional_losses_17301942/
-fmn2_tsvd_5000_1000_2/StatefulPartitionedCall�
,fmn2_pca_5000_1000_2/StatefulPartitionedCallStatefulPartitionedCall5fmn2_pca_5000_1000_1/StatefulPartitionedCall:output:0fmn2_pca_5000_1000_2_1731087fmn2_pca_5000_1000_2_1731089*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_fmn2_pca_5000_1000_2_layer_call_and_return_conditional_losses_17302112.
,fmn2_pca_5000_1000_2/StatefulPartitionedCall�
,fmn2_lle_5000_1000_3/StatefulPartitionedCallStatefulPartitionedCall5fmn2_lle_5000_1000_2/StatefulPartitionedCall:output:0fmn2_lle_5000_1000_3_1731092fmn2_lle_5000_1000_3_1731094*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_fmn2_lle_5000_1000_3_layer_call_and_return_conditional_losses_17302282.
,fmn2_lle_5000_1000_3/StatefulPartitionedCall�
/fmn2_isomap_5000_1000_3/StatefulPartitionedCallStatefulPartitionedCall8fmn2_isomap_5000_1000_2/StatefulPartitionedCall:output:0fmn2_isomap_5000_1000_3_1731097fmn2_isomap_5000_1000_3_1731099*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *]
fXRV
T__inference_fmn2_isomap_5000_1000_3_layer_call_and_return_conditional_losses_173024521
/fmn2_isomap_5000_1000_3/StatefulPartitionedCall�
-fmn2_tsvd_5000_1000_3/StatefulPartitionedCallStatefulPartitionedCall6fmn2_tsvd_5000_1000_2/StatefulPartitionedCall:output:0fmn2_tsvd_5000_1000_3_1731102fmn2_tsvd_5000_1000_3_1731104*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_fmn2_tsvd_5000_1000_3_layer_call_and_return_conditional_losses_17302622/
-fmn2_tsvd_5000_1000_3/StatefulPartitionedCall�
,fmn2_pca_5000_1000_3/StatefulPartitionedCallStatefulPartitionedCall5fmn2_pca_5000_1000_2/StatefulPartitionedCall:output:0fmn2_pca_5000_1000_3_1731107fmn2_pca_5000_1000_3_1731109*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_fmn2_pca_5000_1000_3_layer_call_and_return_conditional_losses_17302792.
,fmn2_pca_5000_1000_3/StatefulPartitionedCall�
.fmn2_pca_5000_1000_out/StatefulPartitionedCallStatefulPartitionedCall5fmn2_pca_5000_1000_3/StatefulPartitionedCall:output:0fmn2_pca_5000_1000_out_1731112fmn2_pca_5000_1000_out_1731114*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *\
fWRU
S__inference_fmn2_pca_5000_1000_out_layer_call_and_return_conditional_losses_173029620
.fmn2_pca_5000_1000_out/StatefulPartitionedCall�
/fmn2_tsvd_5000_1000_out/StatefulPartitionedCallStatefulPartitionedCall6fmn2_tsvd_5000_1000_3/StatefulPartitionedCall:output:0fmn2_tsvd_5000_1000_out_1731117fmn2_tsvd_5000_1000_out_1731119*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *]
fXRV
T__inference_fmn2_tsvd_5000_1000_out_layer_call_and_return_conditional_losses_173031321
/fmn2_tsvd_5000_1000_out/StatefulPartitionedCall�
1fmn2_isomap_5000_1000_out/StatefulPartitionedCallStatefulPartitionedCall8fmn2_isomap_5000_1000_3/StatefulPartitionedCall:output:0!fmn2_isomap_5000_1000_out_1731122!fmn2_isomap_5000_1000_out_1731124*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *_
fZRX
V__inference_fmn2_isomap_5000_1000_out_layer_call_and_return_conditional_losses_173033023
1fmn2_isomap_5000_1000_out/StatefulPartitionedCall�
.fmn2_lle_5000_1000_out/StatefulPartitionedCallStatefulPartitionedCall5fmn2_lle_5000_1000_3/StatefulPartitionedCall:output:0fmn2_lle_5000_1000_out_1731127fmn2_lle_5000_1000_out_1731129*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *\
fWRU
S__inference_fmn2_lle_5000_1000_out_layer_call_and_return_conditional_losses_173034720
.fmn2_lle_5000_1000_out/StatefulPartitionedCall�
concatenate_51/PartitionedCallPartitionedCall7fmn2_pca_5000_1000_out/StatefulPartitionedCall:output:08fmn2_tsvd_5000_1000_out/StatefulPartitionedCall:output:0:fmn2_isomap_5000_1000_out/StatefulPartitionedCall:output:07fmn2_lle_5000_1000_out/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *T
fORM
K__inference_concatenate_51_layer_call_and_return_conditional_losses_17303622 
concatenate_51/PartitionedCall�
joined/StatefulPartitionedCallStatefulPartitionedCall'concatenate_51/PartitionedCall:output:0joined_1731133joined_1731135*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *L
fGRE
C__inference_joined_layer_call_and_return_conditional_losses_17303752 
joined/StatefulPartitionedCall�
IdentityIdentity'joined/StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identity�
NoOpNoOp0^fmn2_isomap_5000_1000_1/StatefulPartitionedCall0^fmn2_isomap_5000_1000_2/StatefulPartitionedCall0^fmn2_isomap_5000_1000_3/StatefulPartitionedCall2^fmn2_isomap_5000_1000_out/StatefulPartitionedCall-^fmn2_lle_5000_1000_1/StatefulPartitionedCall-^fmn2_lle_5000_1000_2/StatefulPartitionedCall-^fmn2_lle_5000_1000_3/StatefulPartitionedCall/^fmn2_lle_5000_1000_out/StatefulPartitionedCall-^fmn2_pca_5000_1000_1/StatefulPartitionedCall-^fmn2_pca_5000_1000_2/StatefulPartitionedCall-^fmn2_pca_5000_1000_3/StatefulPartitionedCall/^fmn2_pca_5000_1000_out/StatefulPartitionedCall.^fmn2_tsvd_5000_1000_1/StatefulPartitionedCall.^fmn2_tsvd_5000_1000_2/StatefulPartitionedCall.^fmn2_tsvd_5000_1000_3/StatefulPartitionedCall0^fmn2_tsvd_5000_1000_out/StatefulPartitionedCall^joined/StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*�
_input_shapes�
�:����������:����������:����������:����������: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 2b
/fmn2_isomap_5000_1000_1/StatefulPartitionedCall/fmn2_isomap_5000_1000_1/StatefulPartitionedCall2b
/fmn2_isomap_5000_1000_2/StatefulPartitionedCall/fmn2_isomap_5000_1000_2/StatefulPartitionedCall2b
/fmn2_isomap_5000_1000_3/StatefulPartitionedCall/fmn2_isomap_5000_1000_3/StatefulPartitionedCall2f
1fmn2_isomap_5000_1000_out/StatefulPartitionedCall1fmn2_isomap_5000_1000_out/StatefulPartitionedCall2\
,fmn2_lle_5000_1000_1/StatefulPartitionedCall,fmn2_lle_5000_1000_1/StatefulPartitionedCall2\
,fmn2_lle_5000_1000_2/StatefulPartitionedCall,fmn2_lle_5000_1000_2/StatefulPartitionedCall2\
,fmn2_lle_5000_1000_3/StatefulPartitionedCall,fmn2_lle_5000_1000_3/StatefulPartitionedCall2`
.fmn2_lle_5000_1000_out/StatefulPartitionedCall.fmn2_lle_5000_1000_out/StatefulPartitionedCall2\
,fmn2_pca_5000_1000_1/StatefulPartitionedCall,fmn2_pca_5000_1000_1/StatefulPartitionedCall2\
,fmn2_pca_5000_1000_2/StatefulPartitionedCall,fmn2_pca_5000_1000_2/StatefulPartitionedCall2\
,fmn2_pca_5000_1000_3/StatefulPartitionedCall,fmn2_pca_5000_1000_3/StatefulPartitionedCall2`
.fmn2_pca_5000_1000_out/StatefulPartitionedCall.fmn2_pca_5000_1000_out/StatefulPartitionedCall2^
-fmn2_tsvd_5000_1000_1/StatefulPartitionedCall-fmn2_tsvd_5000_1000_1/StatefulPartitionedCall2^
-fmn2_tsvd_5000_1000_2/StatefulPartitionedCall-fmn2_tsvd_5000_1000_2/StatefulPartitionedCall2^
-fmn2_tsvd_5000_1000_3/StatefulPartitionedCall-fmn2_tsvd_5000_1000_3/StatefulPartitionedCall2b
/fmn2_tsvd_5000_1000_out/StatefulPartitionedCall/fmn2_tsvd_5000_1000_out/StatefulPartitionedCall2@
joined/StatefulPartitionedCalljoined/StatefulPartitionedCall:b ^
(
_output_shapes
:����������
2
_user_specified_namefmn2_pca_5000_1000_input:c_
(
_output_shapes
:����������
3
_user_specified_namefmn2_tsvd_5000_1000_input:ea
(
_output_shapes
:����������
5
_user_specified_namefmn2_isomap_5000_1000_input:b^
(
_output_shapes
:����������
2
_user_specified_namefmn2_lle_5000_1000_input
�
�
Q__inference_fmn2_lle_5000_1000_1_layer_call_and_return_conditional_losses_1730092

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
6__inference_fmn2_pca_5000_1000_1_layer_call_fn_1731640

inputs
unknown:
��
	unknown_0:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_fmn2_pca_5000_1000_1_layer_call_and_return_conditional_losses_17301432
StatefulPartitionedCall|
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:����������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
6__inference_fmn2_lle_5000_1000_1_layer_call_fn_1731700

inputs
unknown:
��
	unknown_0:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_fmn2_lle_5000_1000_1_layer_call_and_return_conditional_losses_17300922
StatefulPartitionedCall|
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:����������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
T__inference_fmn2_tsvd_5000_1000_out_layer_call_and_return_conditional_losses_1730313

inputs1
matmul_readvariableop_resource:	�-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes
:	�*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2	
BiasAdda
SigmoidSigmoidBiasAdd:output:0*
T0*'
_output_shapes
:���������2	
Sigmoidf
IdentityIdentitySigmoid:y:0^NoOp*
T0*'
_output_shapes
:���������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
C__inference_joined_layer_call_and_return_conditional_losses_1730375

inputs0
matmul_readvariableop_resource:-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2	
BiasAdda
SigmoidSigmoidBiasAdd:output:0*
T0*'
_output_shapes
:���������2	
Sigmoidf
IdentityIdentitySigmoid:y:0^NoOp*
T0*'
_output_shapes
:���������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs
�
�
8__inference_fmn2_lle_5000_1000_out_layer_call_fn_1731940

inputs
unknown:	�
	unknown_0:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *\
fWRU
S__inference_fmn2_lle_5000_1000_out_layer_call_and_return_conditional_losses_17303472
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
6__inference_fmn2_lle_5000_1000_2_layer_call_fn_1731780

inputs
unknown:
��
	unknown_0:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_fmn2_lle_5000_1000_2_layer_call_and_return_conditional_losses_17301602
StatefulPartitionedCall|
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:����������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
T__inference_fmn2_isomap_5000_1000_3_layer_call_and_return_conditional_losses_1731851

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
9__inference_fmn2_isomap_5000_1000_1_layer_call_fn_1731680

inputs
unknown:
��
	unknown_0:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *]
fXRV
T__inference_fmn2_isomap_5000_1000_1_layer_call_and_return_conditional_losses_17301092
StatefulPartitionedCall|
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:����������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
x
0__inference_concatenate_51_layer_call_fn_1731959
inputs_0
inputs_1
inputs_2
inputs_3
identity�
PartitionedCallPartitionedCallinputs_0inputs_1inputs_2inputs_3*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *T
fORM
K__inference_concatenate_51_layer_call_and_return_conditional_losses_17303622
PartitionedCalll
IdentityIdentityPartitionedCall:output:0*
T0*'
_output_shapes
:���������2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*_
_input_shapesN
L:���������:���������:���������:���������:Q M
'
_output_shapes
:���������
"
_user_specified_name
inputs/0:QM
'
_output_shapes
:���������
"
_user_specified_name
inputs/1:QM
'
_output_shapes
:���������
"
_user_specified_name
inputs/2:QM
'
_output_shapes
:���������
"
_user_specified_name
inputs/3
�
�
9__inference_fmn2_isomap_5000_1000_2_layer_call_fn_1731760

inputs
unknown:
��
	unknown_0:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *]
fXRV
T__inference_fmn2_isomap_5000_1000_2_layer_call_and_return_conditional_losses_17301772
StatefulPartitionedCall|
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:����������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
V__inference_fmn2_isomap_5000_1000_out_layer_call_and_return_conditional_losses_1731931

inputs1
matmul_readvariableop_resource:	�-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes
:	�*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2	
BiasAdda
SigmoidSigmoidBiasAdd:output:0*
T0*'
_output_shapes
:���������2	
Sigmoidf
IdentityIdentitySigmoid:y:0^NoOp*
T0*'
_output_shapes
:���������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
;__inference_fmn2_isomap_5000_1000_out_layer_call_fn_1731920

inputs
unknown:	�
	unknown_0:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *_
fZRX
V__inference_fmn2_isomap_5000_1000_out_layer_call_and_return_conditional_losses_17303302
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
'__inference_model_layer_call_fn_1731299
inputs_0
inputs_1
inputs_2
inputs_3
unknown:
��
	unknown_0:	�
	unknown_1:
��
	unknown_2:	�
	unknown_3:
��
	unknown_4:	�
	unknown_5:
��
	unknown_6:	�
	unknown_7:
��
	unknown_8:	�
	unknown_9:
��

unknown_10:	�

unknown_11:
��

unknown_12:	�

unknown_13:
��

unknown_14:	�

unknown_15:
��

unknown_16:	�

unknown_17:
��

unknown_18:	�

unknown_19:
��

unknown_20:	�

unknown_21:
��

unknown_22:	�

unknown_23:	�

unknown_24:

unknown_25:	�

unknown_26:

unknown_27:	�

unknown_28:

unknown_29:	�

unknown_30:

unknown_31:

unknown_32:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputs_0inputs_1inputs_2inputs_3unknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10
unknown_11
unknown_12
unknown_13
unknown_14
unknown_15
unknown_16
unknown_17
unknown_18
unknown_19
unknown_20
unknown_21
unknown_22
unknown_23
unknown_24
unknown_25
unknown_26
unknown_27
unknown_28
unknown_29
unknown_30
unknown_31
unknown_32*1
Tin*
(2&*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*D
_read_only_resource_inputs&
$"	
 !"#$%*0
config_proto 

CPU

GPU2*0J 8� *K
fFRD
B__inference_model_layer_call_and_return_conditional_losses_17303822
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*�
_input_shapes�
�:����������:����������:����������:����������: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:R N
(
_output_shapes
:����������
"
_user_specified_name
inputs/0:RN
(
_output_shapes
:����������
"
_user_specified_name
inputs/1:RN
(
_output_shapes
:����������
"
_user_specified_name
inputs/2:RN
(
_output_shapes
:����������
"
_user_specified_name
inputs/3
��
�
#__inference__traced_restore_1732306
file_prefix@
,assignvariableop_fmn2_pca_5000_1000_1_kernel:
��;
,assignvariableop_1_fmn2_pca_5000_1000_1_bias:	�C
/assignvariableop_2_fmn2_tsvd_5000_1000_1_kernel:
��<
-assignvariableop_3_fmn2_tsvd_5000_1000_1_bias:	�E
1assignvariableop_4_fmn2_isomap_5000_1000_1_kernel:
��>
/assignvariableop_5_fmn2_isomap_5000_1000_1_bias:	�B
.assignvariableop_6_fmn2_lle_5000_1000_1_kernel:
��;
,assignvariableop_7_fmn2_lle_5000_1000_1_bias:	�B
.assignvariableop_8_fmn2_pca_5000_1000_2_kernel:
��;
,assignvariableop_9_fmn2_pca_5000_1000_2_bias:	�D
0assignvariableop_10_fmn2_tsvd_5000_1000_2_kernel:
��=
.assignvariableop_11_fmn2_tsvd_5000_1000_2_bias:	�F
2assignvariableop_12_fmn2_isomap_5000_1000_2_kernel:
��?
0assignvariableop_13_fmn2_isomap_5000_1000_2_bias:	�C
/assignvariableop_14_fmn2_lle_5000_1000_2_kernel:
��<
-assignvariableop_15_fmn2_lle_5000_1000_2_bias:	�C
/assignvariableop_16_fmn2_pca_5000_1000_3_kernel:
��<
-assignvariableop_17_fmn2_pca_5000_1000_3_bias:	�D
0assignvariableop_18_fmn2_tsvd_5000_1000_3_kernel:
��=
.assignvariableop_19_fmn2_tsvd_5000_1000_3_bias:	�F
2assignvariableop_20_fmn2_isomap_5000_1000_3_kernel:
��?
0assignvariableop_21_fmn2_isomap_5000_1000_3_bias:	�C
/assignvariableop_22_fmn2_lle_5000_1000_3_kernel:
��<
-assignvariableop_23_fmn2_lle_5000_1000_3_bias:	�D
1assignvariableop_24_fmn2_pca_5000_1000_out_kernel:	�=
/assignvariableop_25_fmn2_pca_5000_1000_out_bias:E
2assignvariableop_26_fmn2_tsvd_5000_1000_out_kernel:	�>
0assignvariableop_27_fmn2_tsvd_5000_1000_out_bias:G
4assignvariableop_28_fmn2_isomap_5000_1000_out_kernel:	�@
2assignvariableop_29_fmn2_isomap_5000_1000_out_bias:D
1assignvariableop_30_fmn2_lle_5000_1000_out_kernel:	�=
/assignvariableop_31_fmn2_lle_5000_1000_out_bias:3
!assignvariableop_32_joined_kernel:-
assignvariableop_33_joined_bias:'
assignvariableop_34_adam_iter:	 )
assignvariableop_35_adam_beta_1: )
assignvariableop_36_adam_beta_2: (
assignvariableop_37_adam_decay: 0
&assignvariableop_38_adam_learning_rate: #
assignvariableop_39_total: #
assignvariableop_40_count: %
assignvariableop_41_total_1: %
assignvariableop_42_count_1: :
(assignvariableop_43_adam_joined_kernel_m:4
&assignvariableop_44_adam_joined_bias_m::
(assignvariableop_45_adam_joined_kernel_v:4
&assignvariableop_46_adam_joined_bias_v:
identity_48��AssignVariableOp�AssignVariableOp_1�AssignVariableOp_10�AssignVariableOp_11�AssignVariableOp_12�AssignVariableOp_13�AssignVariableOp_14�AssignVariableOp_15�AssignVariableOp_16�AssignVariableOp_17�AssignVariableOp_18�AssignVariableOp_19�AssignVariableOp_2�AssignVariableOp_20�AssignVariableOp_21�AssignVariableOp_22�AssignVariableOp_23�AssignVariableOp_24�AssignVariableOp_25�AssignVariableOp_26�AssignVariableOp_27�AssignVariableOp_28�AssignVariableOp_29�AssignVariableOp_3�AssignVariableOp_30�AssignVariableOp_31�AssignVariableOp_32�AssignVariableOp_33�AssignVariableOp_34�AssignVariableOp_35�AssignVariableOp_36�AssignVariableOp_37�AssignVariableOp_38�AssignVariableOp_39�AssignVariableOp_4�AssignVariableOp_40�AssignVariableOp_41�AssignVariableOp_42�AssignVariableOp_43�AssignVariableOp_44�AssignVariableOp_45�AssignVariableOp_46�AssignVariableOp_5�AssignVariableOp_6�AssignVariableOp_7�AssignVariableOp_8�AssignVariableOp_9�
RestoreV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:0*
dtype0*�
value�B�0B6layer_with_weights-0/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-0/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-1/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-1/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-3/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-3/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-4/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-4/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-5/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-5/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-6/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-6/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-7/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-7/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-8/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-8/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-9/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-9/bias/.ATTRIBUTES/VARIABLE_VALUEB7layer_with_weights-10/kernel/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-10/bias/.ATTRIBUTES/VARIABLE_VALUEB7layer_with_weights-11/kernel/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-11/bias/.ATTRIBUTES/VARIABLE_VALUEB7layer_with_weights-12/kernel/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-12/bias/.ATTRIBUTES/VARIABLE_VALUEB7layer_with_weights-13/kernel/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-13/bias/.ATTRIBUTES/VARIABLE_VALUEB7layer_with_weights-14/kernel/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-14/bias/.ATTRIBUTES/VARIABLE_VALUEB7layer_with_weights-15/kernel/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-15/bias/.ATTRIBUTES/VARIABLE_VALUEB7layer_with_weights-16/kernel/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-16/bias/.ATTRIBUTES/VARIABLE_VALUEB)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUEB*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/count/.ATTRIBUTES/VARIABLE_VALUEBSlayer_with_weights-16/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-16/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBSlayer_with_weights-16/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-16/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEB_CHECKPOINTABLE_OBJECT_GRAPH2
RestoreV2/tensor_names�
RestoreV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:0*
dtype0*s
valuejBh0B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B 2
RestoreV2/shape_and_slices�
	RestoreV2	RestoreV2file_prefixRestoreV2/tensor_names:output:0#RestoreV2/shape_and_slices:output:0"/device:CPU:0*�
_output_shapes�
�::::::::::::::::::::::::::::::::::::::::::::::::*>
dtypes4
220	2
	RestoreV2g
IdentityIdentityRestoreV2:tensors:0"/device:CPU:0*
T0*
_output_shapes
:2

Identity�
AssignVariableOpAssignVariableOp,assignvariableop_fmn2_pca_5000_1000_1_kernelIdentity:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOpk

Identity_1IdentityRestoreV2:tensors:1"/device:CPU:0*
T0*
_output_shapes
:2

Identity_1�
AssignVariableOp_1AssignVariableOp,assignvariableop_1_fmn2_pca_5000_1000_1_biasIdentity_1:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_1k

Identity_2IdentityRestoreV2:tensors:2"/device:CPU:0*
T0*
_output_shapes
:2

Identity_2�
AssignVariableOp_2AssignVariableOp/assignvariableop_2_fmn2_tsvd_5000_1000_1_kernelIdentity_2:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_2k

Identity_3IdentityRestoreV2:tensors:3"/device:CPU:0*
T0*
_output_shapes
:2

Identity_3�
AssignVariableOp_3AssignVariableOp-assignvariableop_3_fmn2_tsvd_5000_1000_1_biasIdentity_3:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_3k

Identity_4IdentityRestoreV2:tensors:4"/device:CPU:0*
T0*
_output_shapes
:2

Identity_4�
AssignVariableOp_4AssignVariableOp1assignvariableop_4_fmn2_isomap_5000_1000_1_kernelIdentity_4:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_4k

Identity_5IdentityRestoreV2:tensors:5"/device:CPU:0*
T0*
_output_shapes
:2

Identity_5�
AssignVariableOp_5AssignVariableOp/assignvariableop_5_fmn2_isomap_5000_1000_1_biasIdentity_5:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_5k

Identity_6IdentityRestoreV2:tensors:6"/device:CPU:0*
T0*
_output_shapes
:2

Identity_6�
AssignVariableOp_6AssignVariableOp.assignvariableop_6_fmn2_lle_5000_1000_1_kernelIdentity_6:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_6k

Identity_7IdentityRestoreV2:tensors:7"/device:CPU:0*
T0*
_output_shapes
:2

Identity_7�
AssignVariableOp_7AssignVariableOp,assignvariableop_7_fmn2_lle_5000_1000_1_biasIdentity_7:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_7k

Identity_8IdentityRestoreV2:tensors:8"/device:CPU:0*
T0*
_output_shapes
:2

Identity_8�
AssignVariableOp_8AssignVariableOp.assignvariableop_8_fmn2_pca_5000_1000_2_kernelIdentity_8:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_8k

Identity_9IdentityRestoreV2:tensors:9"/device:CPU:0*
T0*
_output_shapes
:2

Identity_9�
AssignVariableOp_9AssignVariableOp,assignvariableop_9_fmn2_pca_5000_1000_2_biasIdentity_9:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_9n
Identity_10IdentityRestoreV2:tensors:10"/device:CPU:0*
T0*
_output_shapes
:2
Identity_10�
AssignVariableOp_10AssignVariableOp0assignvariableop_10_fmn2_tsvd_5000_1000_2_kernelIdentity_10:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_10n
Identity_11IdentityRestoreV2:tensors:11"/device:CPU:0*
T0*
_output_shapes
:2
Identity_11�
AssignVariableOp_11AssignVariableOp.assignvariableop_11_fmn2_tsvd_5000_1000_2_biasIdentity_11:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_11n
Identity_12IdentityRestoreV2:tensors:12"/device:CPU:0*
T0*
_output_shapes
:2
Identity_12�
AssignVariableOp_12AssignVariableOp2assignvariableop_12_fmn2_isomap_5000_1000_2_kernelIdentity_12:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_12n
Identity_13IdentityRestoreV2:tensors:13"/device:CPU:0*
T0*
_output_shapes
:2
Identity_13�
AssignVariableOp_13AssignVariableOp0assignvariableop_13_fmn2_isomap_5000_1000_2_biasIdentity_13:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_13n
Identity_14IdentityRestoreV2:tensors:14"/device:CPU:0*
T0*
_output_shapes
:2
Identity_14�
AssignVariableOp_14AssignVariableOp/assignvariableop_14_fmn2_lle_5000_1000_2_kernelIdentity_14:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_14n
Identity_15IdentityRestoreV2:tensors:15"/device:CPU:0*
T0*
_output_shapes
:2
Identity_15�
AssignVariableOp_15AssignVariableOp-assignvariableop_15_fmn2_lle_5000_1000_2_biasIdentity_15:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_15n
Identity_16IdentityRestoreV2:tensors:16"/device:CPU:0*
T0*
_output_shapes
:2
Identity_16�
AssignVariableOp_16AssignVariableOp/assignvariableop_16_fmn2_pca_5000_1000_3_kernelIdentity_16:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_16n
Identity_17IdentityRestoreV2:tensors:17"/device:CPU:0*
T0*
_output_shapes
:2
Identity_17�
AssignVariableOp_17AssignVariableOp-assignvariableop_17_fmn2_pca_5000_1000_3_biasIdentity_17:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_17n
Identity_18IdentityRestoreV2:tensors:18"/device:CPU:0*
T0*
_output_shapes
:2
Identity_18�
AssignVariableOp_18AssignVariableOp0assignvariableop_18_fmn2_tsvd_5000_1000_3_kernelIdentity_18:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_18n
Identity_19IdentityRestoreV2:tensors:19"/device:CPU:0*
T0*
_output_shapes
:2
Identity_19�
AssignVariableOp_19AssignVariableOp.assignvariableop_19_fmn2_tsvd_5000_1000_3_biasIdentity_19:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_19n
Identity_20IdentityRestoreV2:tensors:20"/device:CPU:0*
T0*
_output_shapes
:2
Identity_20�
AssignVariableOp_20AssignVariableOp2assignvariableop_20_fmn2_isomap_5000_1000_3_kernelIdentity_20:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_20n
Identity_21IdentityRestoreV2:tensors:21"/device:CPU:0*
T0*
_output_shapes
:2
Identity_21�
AssignVariableOp_21AssignVariableOp0assignvariableop_21_fmn2_isomap_5000_1000_3_biasIdentity_21:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_21n
Identity_22IdentityRestoreV2:tensors:22"/device:CPU:0*
T0*
_output_shapes
:2
Identity_22�
AssignVariableOp_22AssignVariableOp/assignvariableop_22_fmn2_lle_5000_1000_3_kernelIdentity_22:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_22n
Identity_23IdentityRestoreV2:tensors:23"/device:CPU:0*
T0*
_output_shapes
:2
Identity_23�
AssignVariableOp_23AssignVariableOp-assignvariableop_23_fmn2_lle_5000_1000_3_biasIdentity_23:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_23n
Identity_24IdentityRestoreV2:tensors:24"/device:CPU:0*
T0*
_output_shapes
:2
Identity_24�
AssignVariableOp_24AssignVariableOp1assignvariableop_24_fmn2_pca_5000_1000_out_kernelIdentity_24:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_24n
Identity_25IdentityRestoreV2:tensors:25"/device:CPU:0*
T0*
_output_shapes
:2
Identity_25�
AssignVariableOp_25AssignVariableOp/assignvariableop_25_fmn2_pca_5000_1000_out_biasIdentity_25:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_25n
Identity_26IdentityRestoreV2:tensors:26"/device:CPU:0*
T0*
_output_shapes
:2
Identity_26�
AssignVariableOp_26AssignVariableOp2assignvariableop_26_fmn2_tsvd_5000_1000_out_kernelIdentity_26:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_26n
Identity_27IdentityRestoreV2:tensors:27"/device:CPU:0*
T0*
_output_shapes
:2
Identity_27�
AssignVariableOp_27AssignVariableOp0assignvariableop_27_fmn2_tsvd_5000_1000_out_biasIdentity_27:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_27n
Identity_28IdentityRestoreV2:tensors:28"/device:CPU:0*
T0*
_output_shapes
:2
Identity_28�
AssignVariableOp_28AssignVariableOp4assignvariableop_28_fmn2_isomap_5000_1000_out_kernelIdentity_28:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_28n
Identity_29IdentityRestoreV2:tensors:29"/device:CPU:0*
T0*
_output_shapes
:2
Identity_29�
AssignVariableOp_29AssignVariableOp2assignvariableop_29_fmn2_isomap_5000_1000_out_biasIdentity_29:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_29n
Identity_30IdentityRestoreV2:tensors:30"/device:CPU:0*
T0*
_output_shapes
:2
Identity_30�
AssignVariableOp_30AssignVariableOp1assignvariableop_30_fmn2_lle_5000_1000_out_kernelIdentity_30:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_30n
Identity_31IdentityRestoreV2:tensors:31"/device:CPU:0*
T0*
_output_shapes
:2
Identity_31�
AssignVariableOp_31AssignVariableOp/assignvariableop_31_fmn2_lle_5000_1000_out_biasIdentity_31:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_31n
Identity_32IdentityRestoreV2:tensors:32"/device:CPU:0*
T0*
_output_shapes
:2
Identity_32�
AssignVariableOp_32AssignVariableOp!assignvariableop_32_joined_kernelIdentity_32:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_32n
Identity_33IdentityRestoreV2:tensors:33"/device:CPU:0*
T0*
_output_shapes
:2
Identity_33�
AssignVariableOp_33AssignVariableOpassignvariableop_33_joined_biasIdentity_33:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_33n
Identity_34IdentityRestoreV2:tensors:34"/device:CPU:0*
T0	*
_output_shapes
:2
Identity_34�
AssignVariableOp_34AssignVariableOpassignvariableop_34_adam_iterIdentity_34:output:0"/device:CPU:0*
_output_shapes
 *
dtype0	2
AssignVariableOp_34n
Identity_35IdentityRestoreV2:tensors:35"/device:CPU:0*
T0*
_output_shapes
:2
Identity_35�
AssignVariableOp_35AssignVariableOpassignvariableop_35_adam_beta_1Identity_35:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_35n
Identity_36IdentityRestoreV2:tensors:36"/device:CPU:0*
T0*
_output_shapes
:2
Identity_36�
AssignVariableOp_36AssignVariableOpassignvariableop_36_adam_beta_2Identity_36:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_36n
Identity_37IdentityRestoreV2:tensors:37"/device:CPU:0*
T0*
_output_shapes
:2
Identity_37�
AssignVariableOp_37AssignVariableOpassignvariableop_37_adam_decayIdentity_37:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_37n
Identity_38IdentityRestoreV2:tensors:38"/device:CPU:0*
T0*
_output_shapes
:2
Identity_38�
AssignVariableOp_38AssignVariableOp&assignvariableop_38_adam_learning_rateIdentity_38:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_38n
Identity_39IdentityRestoreV2:tensors:39"/device:CPU:0*
T0*
_output_shapes
:2
Identity_39�
AssignVariableOp_39AssignVariableOpassignvariableop_39_totalIdentity_39:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_39n
Identity_40IdentityRestoreV2:tensors:40"/device:CPU:0*
T0*
_output_shapes
:2
Identity_40�
AssignVariableOp_40AssignVariableOpassignvariableop_40_countIdentity_40:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_40n
Identity_41IdentityRestoreV2:tensors:41"/device:CPU:0*
T0*
_output_shapes
:2
Identity_41�
AssignVariableOp_41AssignVariableOpassignvariableop_41_total_1Identity_41:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_41n
Identity_42IdentityRestoreV2:tensors:42"/device:CPU:0*
T0*
_output_shapes
:2
Identity_42�
AssignVariableOp_42AssignVariableOpassignvariableop_42_count_1Identity_42:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_42n
Identity_43IdentityRestoreV2:tensors:43"/device:CPU:0*
T0*
_output_shapes
:2
Identity_43�
AssignVariableOp_43AssignVariableOp(assignvariableop_43_adam_joined_kernel_mIdentity_43:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_43n
Identity_44IdentityRestoreV2:tensors:44"/device:CPU:0*
T0*
_output_shapes
:2
Identity_44�
AssignVariableOp_44AssignVariableOp&assignvariableop_44_adam_joined_bias_mIdentity_44:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_44n
Identity_45IdentityRestoreV2:tensors:45"/device:CPU:0*
T0*
_output_shapes
:2
Identity_45�
AssignVariableOp_45AssignVariableOp(assignvariableop_45_adam_joined_kernel_vIdentity_45:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_45n
Identity_46IdentityRestoreV2:tensors:46"/device:CPU:0*
T0*
_output_shapes
:2
Identity_46�
AssignVariableOp_46AssignVariableOp&assignvariableop_46_adam_joined_bias_vIdentity_46:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_469
NoOpNoOp"/device:CPU:0*
_output_shapes
 2
NoOp�
Identity_47Identityfile_prefix^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_12^AssignVariableOp_13^AssignVariableOp_14^AssignVariableOp_15^AssignVariableOp_16^AssignVariableOp_17^AssignVariableOp_18^AssignVariableOp_19^AssignVariableOp_2^AssignVariableOp_20^AssignVariableOp_21^AssignVariableOp_22^AssignVariableOp_23^AssignVariableOp_24^AssignVariableOp_25^AssignVariableOp_26^AssignVariableOp_27^AssignVariableOp_28^AssignVariableOp_29^AssignVariableOp_3^AssignVariableOp_30^AssignVariableOp_31^AssignVariableOp_32^AssignVariableOp_33^AssignVariableOp_34^AssignVariableOp_35^AssignVariableOp_36^AssignVariableOp_37^AssignVariableOp_38^AssignVariableOp_39^AssignVariableOp_4^AssignVariableOp_40^AssignVariableOp_41^AssignVariableOp_42^AssignVariableOp_43^AssignVariableOp_44^AssignVariableOp_45^AssignVariableOp_46^AssignVariableOp_5^AssignVariableOp_6^AssignVariableOp_7^AssignVariableOp_8^AssignVariableOp_9^NoOp"/device:CPU:0*
T0*
_output_shapes
: 2
Identity_47f
Identity_48IdentityIdentity_47:output:0^NoOp_1*
T0*
_output_shapes
: 2
Identity_48�
NoOp_1NoOp^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_12^AssignVariableOp_13^AssignVariableOp_14^AssignVariableOp_15^AssignVariableOp_16^AssignVariableOp_17^AssignVariableOp_18^AssignVariableOp_19^AssignVariableOp_2^AssignVariableOp_20^AssignVariableOp_21^AssignVariableOp_22^AssignVariableOp_23^AssignVariableOp_24^AssignVariableOp_25^AssignVariableOp_26^AssignVariableOp_27^AssignVariableOp_28^AssignVariableOp_29^AssignVariableOp_3^AssignVariableOp_30^AssignVariableOp_31^AssignVariableOp_32^AssignVariableOp_33^AssignVariableOp_34^AssignVariableOp_35^AssignVariableOp_36^AssignVariableOp_37^AssignVariableOp_38^AssignVariableOp_39^AssignVariableOp_4^AssignVariableOp_40^AssignVariableOp_41^AssignVariableOp_42^AssignVariableOp_43^AssignVariableOp_44^AssignVariableOp_45^AssignVariableOp_46^AssignVariableOp_5^AssignVariableOp_6^AssignVariableOp_7^AssignVariableOp_8^AssignVariableOp_9*"
_acd_function_control_output(*
_output_shapes
 2
NoOp_1"#
identity_48Identity_48:output:0*s
_input_shapesb
`: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 2$
AssignVariableOpAssignVariableOp2(
AssignVariableOp_1AssignVariableOp_12*
AssignVariableOp_10AssignVariableOp_102*
AssignVariableOp_11AssignVariableOp_112*
AssignVariableOp_12AssignVariableOp_122*
AssignVariableOp_13AssignVariableOp_132*
AssignVariableOp_14AssignVariableOp_142*
AssignVariableOp_15AssignVariableOp_152*
AssignVariableOp_16AssignVariableOp_162*
AssignVariableOp_17AssignVariableOp_172*
AssignVariableOp_18AssignVariableOp_182*
AssignVariableOp_19AssignVariableOp_192(
AssignVariableOp_2AssignVariableOp_22*
AssignVariableOp_20AssignVariableOp_202*
AssignVariableOp_21AssignVariableOp_212*
AssignVariableOp_22AssignVariableOp_222*
AssignVariableOp_23AssignVariableOp_232*
AssignVariableOp_24AssignVariableOp_242*
AssignVariableOp_25AssignVariableOp_252*
AssignVariableOp_26AssignVariableOp_262*
AssignVariableOp_27AssignVariableOp_272*
AssignVariableOp_28AssignVariableOp_282*
AssignVariableOp_29AssignVariableOp_292(
AssignVariableOp_3AssignVariableOp_32*
AssignVariableOp_30AssignVariableOp_302*
AssignVariableOp_31AssignVariableOp_312*
AssignVariableOp_32AssignVariableOp_322*
AssignVariableOp_33AssignVariableOp_332*
AssignVariableOp_34AssignVariableOp_342*
AssignVariableOp_35AssignVariableOp_352*
AssignVariableOp_36AssignVariableOp_362*
AssignVariableOp_37AssignVariableOp_372*
AssignVariableOp_38AssignVariableOp_382*
AssignVariableOp_39AssignVariableOp_392(
AssignVariableOp_4AssignVariableOp_42*
AssignVariableOp_40AssignVariableOp_402*
AssignVariableOp_41AssignVariableOp_412*
AssignVariableOp_42AssignVariableOp_422*
AssignVariableOp_43AssignVariableOp_432*
AssignVariableOp_44AssignVariableOp_442*
AssignVariableOp_45AssignVariableOp_452*
AssignVariableOp_46AssignVariableOp_462(
AssignVariableOp_5AssignVariableOp_52(
AssignVariableOp_6AssignVariableOp_62(
AssignVariableOp_7AssignVariableOp_72(
AssignVariableOp_8AssignVariableOp_82(
AssignVariableOp_9AssignVariableOp_9:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix
�
�
6__inference_fmn2_pca_5000_1000_2_layer_call_fn_1731720

inputs
unknown:
��
	unknown_0:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_fmn2_pca_5000_1000_2_layer_call_and_return_conditional_losses_17302112
StatefulPartitionedCall|
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:����������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�	
'__inference_model_layer_call_fn_1730453
fmn2_pca_5000_1000_input
fmn2_tsvd_5000_1000_input
fmn2_isomap_5000_1000_input
fmn2_lle_5000_1000_input
unknown:
��
	unknown_0:	�
	unknown_1:
��
	unknown_2:	�
	unknown_3:
��
	unknown_4:	�
	unknown_5:
��
	unknown_6:	�
	unknown_7:
��
	unknown_8:	�
	unknown_9:
��

unknown_10:	�

unknown_11:
��

unknown_12:	�

unknown_13:
��

unknown_14:	�

unknown_15:
��

unknown_16:	�

unknown_17:
��

unknown_18:	�

unknown_19:
��

unknown_20:	�

unknown_21:
��

unknown_22:	�

unknown_23:	�

unknown_24:

unknown_25:	�

unknown_26:

unknown_27:	�

unknown_28:

unknown_29:	�

unknown_30:

unknown_31:

unknown_32:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallfmn2_pca_5000_1000_inputfmn2_tsvd_5000_1000_inputfmn2_isomap_5000_1000_inputfmn2_lle_5000_1000_inputunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10
unknown_11
unknown_12
unknown_13
unknown_14
unknown_15
unknown_16
unknown_17
unknown_18
unknown_19
unknown_20
unknown_21
unknown_22
unknown_23
unknown_24
unknown_25
unknown_26
unknown_27
unknown_28
unknown_29
unknown_30
unknown_31
unknown_32*1
Tin*
(2&*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*D
_read_only_resource_inputs&
$"	
 !"#$%*0
config_proto 

CPU

GPU2*0J 8� *K
fFRD
B__inference_model_layer_call_and_return_conditional_losses_17303822
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*�
_input_shapes�
�:����������:����������:����������:����������: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:b ^
(
_output_shapes
:����������
2
_user_specified_namefmn2_pca_5000_1000_input:c_
(
_output_shapes
:����������
3
_user_specified_namefmn2_tsvd_5000_1000_input:ea
(
_output_shapes
:����������
5
_user_specified_namefmn2_isomap_5000_1000_input:b^
(
_output_shapes
:����������
2
_user_specified_namefmn2_lle_5000_1000_input
�
�
Q__inference_fmn2_lle_5000_1000_3_layer_call_and_return_conditional_losses_1731871

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
T__inference_fmn2_isomap_5000_1000_3_layer_call_and_return_conditional_losses_1730245

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
V__inference_fmn2_isomap_5000_1000_out_layer_call_and_return_conditional_losses_1730330

inputs1
matmul_readvariableop_resource:	�-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes
:	�*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2	
BiasAdda
SigmoidSigmoidBiasAdd:output:0*
T0*'
_output_shapes
:���������2	
Sigmoidf
IdentityIdentitySigmoid:y:0^NoOp*
T0*'
_output_shapes
:���������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�w
�
B__inference_model_layer_call_and_return_conditional_losses_1731046
fmn2_pca_5000_1000_input
fmn2_tsvd_5000_1000_input
fmn2_isomap_5000_1000_input
fmn2_lle_5000_1000_input0
fmn2_lle_5000_1000_1_1730959:
��+
fmn2_lle_5000_1000_1_1730961:	�3
fmn2_isomap_5000_1000_1_1730964:
��.
fmn2_isomap_5000_1000_1_1730966:	�1
fmn2_tsvd_5000_1000_1_1730969:
��,
fmn2_tsvd_5000_1000_1_1730971:	�0
fmn2_pca_5000_1000_1_1730974:
��+
fmn2_pca_5000_1000_1_1730976:	�0
fmn2_lle_5000_1000_2_1730979:
��+
fmn2_lle_5000_1000_2_1730981:	�3
fmn2_isomap_5000_1000_2_1730984:
��.
fmn2_isomap_5000_1000_2_1730986:	�1
fmn2_tsvd_5000_1000_2_1730989:
��,
fmn2_tsvd_5000_1000_2_1730991:	�0
fmn2_pca_5000_1000_2_1730994:
��+
fmn2_pca_5000_1000_2_1730996:	�0
fmn2_lle_5000_1000_3_1730999:
��+
fmn2_lle_5000_1000_3_1731001:	�3
fmn2_isomap_5000_1000_3_1731004:
��.
fmn2_isomap_5000_1000_3_1731006:	�1
fmn2_tsvd_5000_1000_3_1731009:
��,
fmn2_tsvd_5000_1000_3_1731011:	�0
fmn2_pca_5000_1000_3_1731014:
��+
fmn2_pca_5000_1000_3_1731016:	�1
fmn2_pca_5000_1000_out_1731019:	�,
fmn2_pca_5000_1000_out_1731021:2
fmn2_tsvd_5000_1000_out_1731024:	�-
fmn2_tsvd_5000_1000_out_1731026:4
!fmn2_isomap_5000_1000_out_1731029:	�/
!fmn2_isomap_5000_1000_out_1731031:1
fmn2_lle_5000_1000_out_1731034:	�,
fmn2_lle_5000_1000_out_1731036: 
joined_1731040:
joined_1731042:
identity��/fmn2_isomap_5000_1000_1/StatefulPartitionedCall�/fmn2_isomap_5000_1000_2/StatefulPartitionedCall�/fmn2_isomap_5000_1000_3/StatefulPartitionedCall�1fmn2_isomap_5000_1000_out/StatefulPartitionedCall�,fmn2_lle_5000_1000_1/StatefulPartitionedCall�,fmn2_lle_5000_1000_2/StatefulPartitionedCall�,fmn2_lle_5000_1000_3/StatefulPartitionedCall�.fmn2_lle_5000_1000_out/StatefulPartitionedCall�,fmn2_pca_5000_1000_1/StatefulPartitionedCall�,fmn2_pca_5000_1000_2/StatefulPartitionedCall�,fmn2_pca_5000_1000_3/StatefulPartitionedCall�.fmn2_pca_5000_1000_out/StatefulPartitionedCall�-fmn2_tsvd_5000_1000_1/StatefulPartitionedCall�-fmn2_tsvd_5000_1000_2/StatefulPartitionedCall�-fmn2_tsvd_5000_1000_3/StatefulPartitionedCall�/fmn2_tsvd_5000_1000_out/StatefulPartitionedCall�joined/StatefulPartitionedCall�
,fmn2_lle_5000_1000_1/StatefulPartitionedCallStatefulPartitionedCallfmn2_lle_5000_1000_inputfmn2_lle_5000_1000_1_1730959fmn2_lle_5000_1000_1_1730961*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_fmn2_lle_5000_1000_1_layer_call_and_return_conditional_losses_17300922.
,fmn2_lle_5000_1000_1/StatefulPartitionedCall�
/fmn2_isomap_5000_1000_1/StatefulPartitionedCallStatefulPartitionedCallfmn2_isomap_5000_1000_inputfmn2_isomap_5000_1000_1_1730964fmn2_isomap_5000_1000_1_1730966*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *]
fXRV
T__inference_fmn2_isomap_5000_1000_1_layer_call_and_return_conditional_losses_173010921
/fmn2_isomap_5000_1000_1/StatefulPartitionedCall�
-fmn2_tsvd_5000_1000_1/StatefulPartitionedCallStatefulPartitionedCallfmn2_tsvd_5000_1000_inputfmn2_tsvd_5000_1000_1_1730969fmn2_tsvd_5000_1000_1_1730971*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_fmn2_tsvd_5000_1000_1_layer_call_and_return_conditional_losses_17301262/
-fmn2_tsvd_5000_1000_1/StatefulPartitionedCall�
,fmn2_pca_5000_1000_1/StatefulPartitionedCallStatefulPartitionedCallfmn2_pca_5000_1000_inputfmn2_pca_5000_1000_1_1730974fmn2_pca_5000_1000_1_1730976*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_fmn2_pca_5000_1000_1_layer_call_and_return_conditional_losses_17301432.
,fmn2_pca_5000_1000_1/StatefulPartitionedCall�
,fmn2_lle_5000_1000_2/StatefulPartitionedCallStatefulPartitionedCall5fmn2_lle_5000_1000_1/StatefulPartitionedCall:output:0fmn2_lle_5000_1000_2_1730979fmn2_lle_5000_1000_2_1730981*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_fmn2_lle_5000_1000_2_layer_call_and_return_conditional_losses_17301602.
,fmn2_lle_5000_1000_2/StatefulPartitionedCall�
/fmn2_isomap_5000_1000_2/StatefulPartitionedCallStatefulPartitionedCall8fmn2_isomap_5000_1000_1/StatefulPartitionedCall:output:0fmn2_isomap_5000_1000_2_1730984fmn2_isomap_5000_1000_2_1730986*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *]
fXRV
T__inference_fmn2_isomap_5000_1000_2_layer_call_and_return_conditional_losses_173017721
/fmn2_isomap_5000_1000_2/StatefulPartitionedCall�
-fmn2_tsvd_5000_1000_2/StatefulPartitionedCallStatefulPartitionedCall6fmn2_tsvd_5000_1000_1/StatefulPartitionedCall:output:0fmn2_tsvd_5000_1000_2_1730989fmn2_tsvd_5000_1000_2_1730991*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_fmn2_tsvd_5000_1000_2_layer_call_and_return_conditional_losses_17301942/
-fmn2_tsvd_5000_1000_2/StatefulPartitionedCall�
,fmn2_pca_5000_1000_2/StatefulPartitionedCallStatefulPartitionedCall5fmn2_pca_5000_1000_1/StatefulPartitionedCall:output:0fmn2_pca_5000_1000_2_1730994fmn2_pca_5000_1000_2_1730996*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_fmn2_pca_5000_1000_2_layer_call_and_return_conditional_losses_17302112.
,fmn2_pca_5000_1000_2/StatefulPartitionedCall�
,fmn2_lle_5000_1000_3/StatefulPartitionedCallStatefulPartitionedCall5fmn2_lle_5000_1000_2/StatefulPartitionedCall:output:0fmn2_lle_5000_1000_3_1730999fmn2_lle_5000_1000_3_1731001*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_fmn2_lle_5000_1000_3_layer_call_and_return_conditional_losses_17302282.
,fmn2_lle_5000_1000_3/StatefulPartitionedCall�
/fmn2_isomap_5000_1000_3/StatefulPartitionedCallStatefulPartitionedCall8fmn2_isomap_5000_1000_2/StatefulPartitionedCall:output:0fmn2_isomap_5000_1000_3_1731004fmn2_isomap_5000_1000_3_1731006*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *]
fXRV
T__inference_fmn2_isomap_5000_1000_3_layer_call_and_return_conditional_losses_173024521
/fmn2_isomap_5000_1000_3/StatefulPartitionedCall�
-fmn2_tsvd_5000_1000_3/StatefulPartitionedCallStatefulPartitionedCall6fmn2_tsvd_5000_1000_2/StatefulPartitionedCall:output:0fmn2_tsvd_5000_1000_3_1731009fmn2_tsvd_5000_1000_3_1731011*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *[
fVRT
R__inference_fmn2_tsvd_5000_1000_3_layer_call_and_return_conditional_losses_17302622/
-fmn2_tsvd_5000_1000_3/StatefulPartitionedCall�
,fmn2_pca_5000_1000_3/StatefulPartitionedCallStatefulPartitionedCall5fmn2_pca_5000_1000_2/StatefulPartitionedCall:output:0fmn2_pca_5000_1000_3_1731014fmn2_pca_5000_1000_3_1731016*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Z
fURS
Q__inference_fmn2_pca_5000_1000_3_layer_call_and_return_conditional_losses_17302792.
,fmn2_pca_5000_1000_3/StatefulPartitionedCall�
.fmn2_pca_5000_1000_out/StatefulPartitionedCallStatefulPartitionedCall5fmn2_pca_5000_1000_3/StatefulPartitionedCall:output:0fmn2_pca_5000_1000_out_1731019fmn2_pca_5000_1000_out_1731021*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *\
fWRU
S__inference_fmn2_pca_5000_1000_out_layer_call_and_return_conditional_losses_173029620
.fmn2_pca_5000_1000_out/StatefulPartitionedCall�
/fmn2_tsvd_5000_1000_out/StatefulPartitionedCallStatefulPartitionedCall6fmn2_tsvd_5000_1000_3/StatefulPartitionedCall:output:0fmn2_tsvd_5000_1000_out_1731024fmn2_tsvd_5000_1000_out_1731026*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *]
fXRV
T__inference_fmn2_tsvd_5000_1000_out_layer_call_and_return_conditional_losses_173031321
/fmn2_tsvd_5000_1000_out/StatefulPartitionedCall�
1fmn2_isomap_5000_1000_out/StatefulPartitionedCallStatefulPartitionedCall8fmn2_isomap_5000_1000_3/StatefulPartitionedCall:output:0!fmn2_isomap_5000_1000_out_1731029!fmn2_isomap_5000_1000_out_1731031*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *_
fZRX
V__inference_fmn2_isomap_5000_1000_out_layer_call_and_return_conditional_losses_173033023
1fmn2_isomap_5000_1000_out/StatefulPartitionedCall�
.fmn2_lle_5000_1000_out/StatefulPartitionedCallStatefulPartitionedCall5fmn2_lle_5000_1000_3/StatefulPartitionedCall:output:0fmn2_lle_5000_1000_out_1731034fmn2_lle_5000_1000_out_1731036*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *\
fWRU
S__inference_fmn2_lle_5000_1000_out_layer_call_and_return_conditional_losses_173034720
.fmn2_lle_5000_1000_out/StatefulPartitionedCall�
concatenate_51/PartitionedCallPartitionedCall7fmn2_pca_5000_1000_out/StatefulPartitionedCall:output:08fmn2_tsvd_5000_1000_out/StatefulPartitionedCall:output:0:fmn2_isomap_5000_1000_out/StatefulPartitionedCall:output:07fmn2_lle_5000_1000_out/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *T
fORM
K__inference_concatenate_51_layer_call_and_return_conditional_losses_17303622 
concatenate_51/PartitionedCall�
joined/StatefulPartitionedCallStatefulPartitionedCall'concatenate_51/PartitionedCall:output:0joined_1731040joined_1731042*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *L
fGRE
C__inference_joined_layer_call_and_return_conditional_losses_17303752 
joined/StatefulPartitionedCall�
IdentityIdentity'joined/StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identity�
NoOpNoOp0^fmn2_isomap_5000_1000_1/StatefulPartitionedCall0^fmn2_isomap_5000_1000_2/StatefulPartitionedCall0^fmn2_isomap_5000_1000_3/StatefulPartitionedCall2^fmn2_isomap_5000_1000_out/StatefulPartitionedCall-^fmn2_lle_5000_1000_1/StatefulPartitionedCall-^fmn2_lle_5000_1000_2/StatefulPartitionedCall-^fmn2_lle_5000_1000_3/StatefulPartitionedCall/^fmn2_lle_5000_1000_out/StatefulPartitionedCall-^fmn2_pca_5000_1000_1/StatefulPartitionedCall-^fmn2_pca_5000_1000_2/StatefulPartitionedCall-^fmn2_pca_5000_1000_3/StatefulPartitionedCall/^fmn2_pca_5000_1000_out/StatefulPartitionedCall.^fmn2_tsvd_5000_1000_1/StatefulPartitionedCall.^fmn2_tsvd_5000_1000_2/StatefulPartitionedCall.^fmn2_tsvd_5000_1000_3/StatefulPartitionedCall0^fmn2_tsvd_5000_1000_out/StatefulPartitionedCall^joined/StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*�
_input_shapes�
�:����������:����������:����������:����������: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 2b
/fmn2_isomap_5000_1000_1/StatefulPartitionedCall/fmn2_isomap_5000_1000_1/StatefulPartitionedCall2b
/fmn2_isomap_5000_1000_2/StatefulPartitionedCall/fmn2_isomap_5000_1000_2/StatefulPartitionedCall2b
/fmn2_isomap_5000_1000_3/StatefulPartitionedCall/fmn2_isomap_5000_1000_3/StatefulPartitionedCall2f
1fmn2_isomap_5000_1000_out/StatefulPartitionedCall1fmn2_isomap_5000_1000_out/StatefulPartitionedCall2\
,fmn2_lle_5000_1000_1/StatefulPartitionedCall,fmn2_lle_5000_1000_1/StatefulPartitionedCall2\
,fmn2_lle_5000_1000_2/StatefulPartitionedCall,fmn2_lle_5000_1000_2/StatefulPartitionedCall2\
,fmn2_lle_5000_1000_3/StatefulPartitionedCall,fmn2_lle_5000_1000_3/StatefulPartitionedCall2`
.fmn2_lle_5000_1000_out/StatefulPartitionedCall.fmn2_lle_5000_1000_out/StatefulPartitionedCall2\
,fmn2_pca_5000_1000_1/StatefulPartitionedCall,fmn2_pca_5000_1000_1/StatefulPartitionedCall2\
,fmn2_pca_5000_1000_2/StatefulPartitionedCall,fmn2_pca_5000_1000_2/StatefulPartitionedCall2\
,fmn2_pca_5000_1000_3/StatefulPartitionedCall,fmn2_pca_5000_1000_3/StatefulPartitionedCall2`
.fmn2_pca_5000_1000_out/StatefulPartitionedCall.fmn2_pca_5000_1000_out/StatefulPartitionedCall2^
-fmn2_tsvd_5000_1000_1/StatefulPartitionedCall-fmn2_tsvd_5000_1000_1/StatefulPartitionedCall2^
-fmn2_tsvd_5000_1000_2/StatefulPartitionedCall-fmn2_tsvd_5000_1000_2/StatefulPartitionedCall2^
-fmn2_tsvd_5000_1000_3/StatefulPartitionedCall-fmn2_tsvd_5000_1000_3/StatefulPartitionedCall2b
/fmn2_tsvd_5000_1000_out/StatefulPartitionedCall/fmn2_tsvd_5000_1000_out/StatefulPartitionedCall2@
joined/StatefulPartitionedCalljoined/StatefulPartitionedCall:b ^
(
_output_shapes
:����������
2
_user_specified_namefmn2_pca_5000_1000_input:c_
(
_output_shapes
:����������
3
_user_specified_namefmn2_tsvd_5000_1000_input:ea
(
_output_shapes
:����������
5
_user_specified_namefmn2_isomap_5000_1000_input:b^
(
_output_shapes
:����������
2
_user_specified_namefmn2_lle_5000_1000_input"�L
saver_filename:0StatefulPartitionedCall_1:0StatefulPartitionedCall_28"
saved_model_main_op

NoOp*>
__saved_model_init_op%#
__saved_model_init_op

NoOp*�
serving_default�
d
fmn2_isomap_5000_1000_inputE
-serving_default_fmn2_isomap_5000_1000_input:0����������
^
fmn2_lle_5000_1000_inputB
*serving_default_fmn2_lle_5000_1000_input:0����������
^
fmn2_pca_5000_1000_inputB
*serving_default_fmn2_pca_5000_1000_input:0����������
`
fmn2_tsvd_5000_1000_inputC
+serving_default_fmn2_tsvd_5000_1000_input:0����������:
joined0
StatefulPartitionedCall:0���������tensorflow/serving/predict:�
�
layer-0
layer-1
layer-2
layer-3
layer_with_weights-0
layer-4
layer_with_weights-1
layer-5
layer_with_weights-2
layer-6
layer_with_weights-3
layer-7
	layer_with_weights-4
	layer-8

layer_with_weights-5

layer-9
layer_with_weights-6
layer-10
layer_with_weights-7
layer-11
layer_with_weights-8
layer-12
layer_with_weights-9
layer-13
layer_with_weights-10
layer-14
layer_with_weights-11
layer-15
layer_with_weights-12
layer-16
layer_with_weights-13
layer-17
layer_with_weights-14
layer-18
layer_with_weights-15
layer-19
layer-20
layer_with_weights-16
layer-21
	optimizer
	variables
regularization_losses
trainable_variables
	keras_api

signatures
�__call__
�_default_save_signature
+�&call_and_return_all_conditional_losses"
_tf_keras_network
D
#_self_saveable_object_factories"
_tf_keras_input_layer
D
#_self_saveable_object_factories"
_tf_keras_input_layer
D
#_self_saveable_object_factories"
_tf_keras_input_layer
D
# _self_saveable_object_factories"
_tf_keras_input_layer
�

!kernel
"bias
##_self_saveable_object_factories
$	variables
%regularization_losses
&trainable_variables
'	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�

(kernel
)bias
#*_self_saveable_object_factories
+	variables
,regularization_losses
-trainable_variables
.	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�

/kernel
0bias
#1_self_saveable_object_factories
2	variables
3regularization_losses
4trainable_variables
5	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�

6kernel
7bias
#8_self_saveable_object_factories
9	variables
:regularization_losses
;trainable_variables
<	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�

=kernel
>bias
#?_self_saveable_object_factories
@	variables
Aregularization_losses
Btrainable_variables
C	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�

Dkernel
Ebias
#F_self_saveable_object_factories
G	variables
Hregularization_losses
Itrainable_variables
J	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�

Kkernel
Lbias
#M_self_saveable_object_factories
N	variables
Oregularization_losses
Ptrainable_variables
Q	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�

Rkernel
Sbias
#T_self_saveable_object_factories
U	variables
Vregularization_losses
Wtrainable_variables
X	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�

Ykernel
Zbias
#[_self_saveable_object_factories
\	variables
]regularization_losses
^trainable_variables
_	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�

`kernel
abias
#b_self_saveable_object_factories
c	variables
dregularization_losses
etrainable_variables
f	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�

gkernel
hbias
#i_self_saveable_object_factories
j	variables
kregularization_losses
ltrainable_variables
m	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�

nkernel
obias
#p_self_saveable_object_factories
q	variables
rregularization_losses
strainable_variables
t	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�

ukernel
vbias
#w_self_saveable_object_factories
x	variables
yregularization_losses
ztrainable_variables
{	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�

|kernel
}bias
#~_self_saveable_object_factories
	variables
�regularization_losses
�trainable_variables
�	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�
�kernel
	�bias
$�_self_saveable_object_factories
�	variables
�regularization_losses
�trainable_variables
�	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�
�kernel
	�bias
$�_self_saveable_object_factories
�	variables
�regularization_losses
�trainable_variables
�	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�
�	variables
�regularization_losses
�trainable_variables
�	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�
�kernel
	�bias
�	variables
�regularization_losses
�trainable_variables
�	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�
	�iter
�beta_1
�beta_2

�decay
�learning_rate	�m�	�m�	�v�	�v�"
	optimizer
�
!0
"1
(2
)3
/4
05
66
77
=8
>9
D10
E11
K12
L13
R14
S15
Y16
Z17
`18
a19
g20
h21
n22
o23
u24
v25
|26
}27
�28
�29
�30
�31
�32
�33"
trackable_list_wrapper
 "
trackable_list_wrapper
0
�0
�1"
trackable_list_wrapper
�
�metrics
�layers
	variables
�layer_metrics
 �layer_regularization_losses
regularization_losses
trainable_variables
�non_trainable_variables
�__call__
�_default_save_signature
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
-
�serving_default"
signature_map
 "
trackable_dict_wrapper
 "
trackable_dict_wrapper
 "
trackable_dict_wrapper
 "
trackable_dict_wrapper
/:-
��2fmn2_pca_5000_1000_1/kernel
(:&�2fmn2_pca_5000_1000_1/bias
 "
trackable_dict_wrapper
.
!0
"1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�metrics
�layers
$	variables
%regularization_losses
�layer_metrics
 �layer_regularization_losses
&trainable_variables
�non_trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
0:.
��2fmn2_tsvd_5000_1000_1/kernel
):'�2fmn2_tsvd_5000_1000_1/bias
 "
trackable_dict_wrapper
.
(0
)1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�metrics
�layers
+	variables
,regularization_losses
�layer_metrics
 �layer_regularization_losses
-trainable_variables
�non_trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
2:0
��2fmn2_isomap_5000_1000_1/kernel
+:)�2fmn2_isomap_5000_1000_1/bias
 "
trackable_dict_wrapper
.
/0
01"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�metrics
�layers
2	variables
3regularization_losses
�layer_metrics
 �layer_regularization_losses
4trainable_variables
�non_trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
/:-
��2fmn2_lle_5000_1000_1/kernel
(:&�2fmn2_lle_5000_1000_1/bias
 "
trackable_dict_wrapper
.
60
71"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�metrics
�layers
9	variables
:regularization_losses
�layer_metrics
 �layer_regularization_losses
;trainable_variables
�non_trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
/:-
��2fmn2_pca_5000_1000_2/kernel
(:&�2fmn2_pca_5000_1000_2/bias
 "
trackable_dict_wrapper
.
=0
>1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�metrics
�layers
@	variables
Aregularization_losses
�layer_metrics
 �layer_regularization_losses
Btrainable_variables
�non_trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
0:.
��2fmn2_tsvd_5000_1000_2/kernel
):'�2fmn2_tsvd_5000_1000_2/bias
 "
trackable_dict_wrapper
.
D0
E1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�metrics
�layers
G	variables
Hregularization_losses
�layer_metrics
 �layer_regularization_losses
Itrainable_variables
�non_trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
2:0
��2fmn2_isomap_5000_1000_2/kernel
+:)�2fmn2_isomap_5000_1000_2/bias
 "
trackable_dict_wrapper
.
K0
L1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�metrics
�layers
N	variables
Oregularization_losses
�layer_metrics
 �layer_regularization_losses
Ptrainable_variables
�non_trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
/:-
��2fmn2_lle_5000_1000_2/kernel
(:&�2fmn2_lle_5000_1000_2/bias
 "
trackable_dict_wrapper
.
R0
S1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�metrics
�layers
U	variables
Vregularization_losses
�layer_metrics
 �layer_regularization_losses
Wtrainable_variables
�non_trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
/:-
��2fmn2_pca_5000_1000_3/kernel
(:&�2fmn2_pca_5000_1000_3/bias
 "
trackable_dict_wrapper
.
Y0
Z1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�metrics
�layers
\	variables
]regularization_losses
�layer_metrics
 �layer_regularization_losses
^trainable_variables
�non_trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
0:.
��2fmn2_tsvd_5000_1000_3/kernel
):'�2fmn2_tsvd_5000_1000_3/bias
 "
trackable_dict_wrapper
.
`0
a1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�metrics
�layers
c	variables
dregularization_losses
�layer_metrics
 �layer_regularization_losses
etrainable_variables
�non_trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
2:0
��2fmn2_isomap_5000_1000_3/kernel
+:)�2fmn2_isomap_5000_1000_3/bias
 "
trackable_dict_wrapper
.
g0
h1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�metrics
�layers
j	variables
kregularization_losses
�layer_metrics
 �layer_regularization_losses
ltrainable_variables
�non_trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
/:-
��2fmn2_lle_5000_1000_3/kernel
(:&�2fmn2_lle_5000_1000_3/bias
 "
trackable_dict_wrapper
.
n0
o1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�metrics
�layers
q	variables
rregularization_losses
�layer_metrics
 �layer_regularization_losses
strainable_variables
�non_trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
0:.	�2fmn2_pca_5000_1000_out/kernel
):'2fmn2_pca_5000_1000_out/bias
 "
trackable_dict_wrapper
.
u0
v1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�metrics
�layers
x	variables
yregularization_losses
�layer_metrics
 �layer_regularization_losses
ztrainable_variables
�non_trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
1:/	�2fmn2_tsvd_5000_1000_out/kernel
*:(2fmn2_tsvd_5000_1000_out/bias
 "
trackable_dict_wrapper
.
|0
}1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�metrics
�layers
	variables
�regularization_losses
�layer_metrics
 �layer_regularization_losses
�trainable_variables
�non_trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
3:1	�2 fmn2_isomap_5000_1000_out/kernel
,:*2fmn2_isomap_5000_1000_out/bias
 "
trackable_dict_wrapper
0
�0
�1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�metrics
�layers
�	variables
�regularization_losses
�layer_metrics
 �layer_regularization_losses
�trainable_variables
�non_trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
0:.	�2fmn2_lle_5000_1000_out/kernel
):'2fmn2_lle_5000_1000_out/bias
 "
trackable_dict_wrapper
0
�0
�1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�metrics
�layers
�	variables
�regularization_losses
�layer_metrics
 �layer_regularization_losses
�trainable_variables
�non_trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�metrics
�layers
�	variables
�regularization_losses
�layer_metrics
 �layer_regularization_losses
�trainable_variables
�non_trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
:2joined/kernel
:2joined/bias
0
�0
�1"
trackable_list_wrapper
 "
trackable_list_wrapper
0
�0
�1"
trackable_list_wrapper
�
�metrics
�layers
�	variables
�regularization_losses
�layer_metrics
 �layer_regularization_losses
�trainable_variables
�non_trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
:	 (2	Adam/iter
: (2Adam/beta_1
: (2Adam/beta_2
: (2
Adam/decay
: (2Adam/learning_rate
0
�0
�1"
trackable_list_wrapper
�
0
1
2
3
4
5
6
7
	8

9
10
11
12
13
14
15
16
17
18
19
20
21"
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
�
!0
"1
(2
)3
/4
05
66
77
=8
>9
D10
E11
K12
L13
R14
S15
Y16
Z17
`18
a19
g20
h21
n22
o23
u24
v25
|26
}27
�28
�29
�30
�31"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
.
!0
"1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
.
(0
)1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
.
/0
01"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
.
60
71"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
.
=0
>1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
.
D0
E1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
.
K0
L1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
.
R0
S1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
.
Y0
Z1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
.
`0
a1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
.
g0
h1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
.
n0
o1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
.
u0
v1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
.
|0
}1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
0
�0
�1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
0
�0
�1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
R

�total

�count
�	variables
�	keras_api"
_tf_keras_metric
c

�total

�count
�
_fn_kwargs
�	variables
�	keras_api"
_tf_keras_metric
:  (2total
:  (2count
0
�0
�1"
trackable_list_wrapper
.
�	variables"
_generic_user_object
:  (2total
:  (2count
 "
trackable_dict_wrapper
0
�0
�1"
trackable_list_wrapper
.
�	variables"
_generic_user_object
$:"2Adam/joined/kernel/m
:2Adam/joined/bias/m
$:"2Adam/joined/kernel/v
:2Adam/joined/bias/v
�2�
'__inference_model_layer_call_fn_1730453
'__inference_model_layer_call_fn_1731299
'__inference_model_layer_call_fn_1731375
'__inference_model_layer_call_fn_1730953�
���
FullArgSpec1
args)�&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�B�
"__inference__wrapped_model_1730068fmn2_pca_5000_1000_inputfmn2_tsvd_5000_1000_inputfmn2_isomap_5000_1000_inputfmn2_lle_5000_1000_input"�
���
FullArgSpec
args� 
varargsjargs
varkwjkwargs
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
B__inference_model_layer_call_and_return_conditional_losses_1731503
B__inference_model_layer_call_and_return_conditional_losses_1731631
B__inference_model_layer_call_and_return_conditional_losses_1731046
B__inference_model_layer_call_and_return_conditional_losses_1731139�
���
FullArgSpec1
args)�&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2�
6__inference_fmn2_pca_5000_1000_1_layer_call_fn_1731640�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
Q__inference_fmn2_pca_5000_1000_1_layer_call_and_return_conditional_losses_1731651�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
7__inference_fmn2_tsvd_5000_1000_1_layer_call_fn_1731660�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
R__inference_fmn2_tsvd_5000_1000_1_layer_call_and_return_conditional_losses_1731671�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
9__inference_fmn2_isomap_5000_1000_1_layer_call_fn_1731680�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
T__inference_fmn2_isomap_5000_1000_1_layer_call_and_return_conditional_losses_1731691�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
6__inference_fmn2_lle_5000_1000_1_layer_call_fn_1731700�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
Q__inference_fmn2_lle_5000_1000_1_layer_call_and_return_conditional_losses_1731711�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
6__inference_fmn2_pca_5000_1000_2_layer_call_fn_1731720�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
Q__inference_fmn2_pca_5000_1000_2_layer_call_and_return_conditional_losses_1731731�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
7__inference_fmn2_tsvd_5000_1000_2_layer_call_fn_1731740�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
R__inference_fmn2_tsvd_5000_1000_2_layer_call_and_return_conditional_losses_1731751�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
9__inference_fmn2_isomap_5000_1000_2_layer_call_fn_1731760�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
T__inference_fmn2_isomap_5000_1000_2_layer_call_and_return_conditional_losses_1731771�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
6__inference_fmn2_lle_5000_1000_2_layer_call_fn_1731780�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
Q__inference_fmn2_lle_5000_1000_2_layer_call_and_return_conditional_losses_1731791�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
6__inference_fmn2_pca_5000_1000_3_layer_call_fn_1731800�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
Q__inference_fmn2_pca_5000_1000_3_layer_call_and_return_conditional_losses_1731811�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
7__inference_fmn2_tsvd_5000_1000_3_layer_call_fn_1731820�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
R__inference_fmn2_tsvd_5000_1000_3_layer_call_and_return_conditional_losses_1731831�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
9__inference_fmn2_isomap_5000_1000_3_layer_call_fn_1731840�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
T__inference_fmn2_isomap_5000_1000_3_layer_call_and_return_conditional_losses_1731851�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
6__inference_fmn2_lle_5000_1000_3_layer_call_fn_1731860�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
Q__inference_fmn2_lle_5000_1000_3_layer_call_and_return_conditional_losses_1731871�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
8__inference_fmn2_pca_5000_1000_out_layer_call_fn_1731880�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
S__inference_fmn2_pca_5000_1000_out_layer_call_and_return_conditional_losses_1731891�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
9__inference_fmn2_tsvd_5000_1000_out_layer_call_fn_1731900�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
T__inference_fmn2_tsvd_5000_1000_out_layer_call_and_return_conditional_losses_1731911�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
;__inference_fmn2_isomap_5000_1000_out_layer_call_fn_1731920�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
V__inference_fmn2_isomap_5000_1000_out_layer_call_and_return_conditional_losses_1731931�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
8__inference_fmn2_lle_5000_1000_out_layer_call_fn_1731940�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
S__inference_fmn2_lle_5000_1000_out_layer_call_and_return_conditional_losses_1731951�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
0__inference_concatenate_51_layer_call_fn_1731959�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
K__inference_concatenate_51_layer_call_and_return_conditional_losses_1731968�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
(__inference_joined_layer_call_fn_1731977�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
C__inference_joined_layer_call_and_return_conditional_losses_1731988�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
%__inference_signature_wrapper_1731223fmn2_isomap_5000_1000_inputfmn2_lle_5000_1000_inputfmn2_pca_5000_1000_inputfmn2_tsvd_5000_1000_input"�
���
FullArgSpec
args� 
varargs
 
varkwjkwargs
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 �
"__inference__wrapped_model_1730068�(67/0()!"RSKLDE=>nogh`aYZuv|}���������
���
���
3�0
fmn2_pca_5000_1000_input����������
4�1
fmn2_tsvd_5000_1000_input����������
6�3
fmn2_isomap_5000_1000_input����������
3�0
fmn2_lle_5000_1000_input����������
� "/�,
*
joined �
joined����������
K__inference_concatenate_51_layer_call_and_return_conditional_losses_1731968����
���
���
"�
inputs/0���������
"�
inputs/1���������
"�
inputs/2���������
"�
inputs/3���������
� "%�"
�
0���������
� �
0__inference_concatenate_51_layer_call_fn_1731959����
���
���
"�
inputs/0���������
"�
inputs/1���������
"�
inputs/2���������
"�
inputs/3���������
� "�����������
T__inference_fmn2_isomap_5000_1000_1_layer_call_and_return_conditional_losses_1731691^/00�-
&�#
!�
inputs����������
� "&�#
�
0����������
� �
9__inference_fmn2_isomap_5000_1000_1_layer_call_fn_1731680Q/00�-
&�#
!�
inputs����������
� "������������
T__inference_fmn2_isomap_5000_1000_2_layer_call_and_return_conditional_losses_1731771^KL0�-
&�#
!�
inputs����������
� "&�#
�
0����������
� �
9__inference_fmn2_isomap_5000_1000_2_layer_call_fn_1731760QKL0�-
&�#
!�
inputs����������
� "������������
T__inference_fmn2_isomap_5000_1000_3_layer_call_and_return_conditional_losses_1731851^gh0�-
&�#
!�
inputs����������
� "&�#
�
0����������
� �
9__inference_fmn2_isomap_5000_1000_3_layer_call_fn_1731840Qgh0�-
&�#
!�
inputs����������
� "������������
V__inference_fmn2_isomap_5000_1000_out_layer_call_and_return_conditional_losses_1731931_��0�-
&�#
!�
inputs����������
� "%�"
�
0���������
� �
;__inference_fmn2_isomap_5000_1000_out_layer_call_fn_1731920R��0�-
&�#
!�
inputs����������
� "�����������
Q__inference_fmn2_lle_5000_1000_1_layer_call_and_return_conditional_losses_1731711^670�-
&�#
!�
inputs����������
� "&�#
�
0����������
� �
6__inference_fmn2_lle_5000_1000_1_layer_call_fn_1731700Q670�-
&�#
!�
inputs����������
� "������������
Q__inference_fmn2_lle_5000_1000_2_layer_call_and_return_conditional_losses_1731791^RS0�-
&�#
!�
inputs����������
� "&�#
�
0����������
� �
6__inference_fmn2_lle_5000_1000_2_layer_call_fn_1731780QRS0�-
&�#
!�
inputs����������
� "������������
Q__inference_fmn2_lle_5000_1000_3_layer_call_and_return_conditional_losses_1731871^no0�-
&�#
!�
inputs����������
� "&�#
�
0����������
� �
6__inference_fmn2_lle_5000_1000_3_layer_call_fn_1731860Qno0�-
&�#
!�
inputs����������
� "������������
S__inference_fmn2_lle_5000_1000_out_layer_call_and_return_conditional_losses_1731951_��0�-
&�#
!�
inputs����������
� "%�"
�
0���������
� �
8__inference_fmn2_lle_5000_1000_out_layer_call_fn_1731940R��0�-
&�#
!�
inputs����������
� "�����������
Q__inference_fmn2_pca_5000_1000_1_layer_call_and_return_conditional_losses_1731651^!"0�-
&�#
!�
inputs����������
� "&�#
�
0����������
� �
6__inference_fmn2_pca_5000_1000_1_layer_call_fn_1731640Q!"0�-
&�#
!�
inputs����������
� "������������
Q__inference_fmn2_pca_5000_1000_2_layer_call_and_return_conditional_losses_1731731^=>0�-
&�#
!�
inputs����������
� "&�#
�
0����������
� �
6__inference_fmn2_pca_5000_1000_2_layer_call_fn_1731720Q=>0�-
&�#
!�
inputs����������
� "������������
Q__inference_fmn2_pca_5000_1000_3_layer_call_and_return_conditional_losses_1731811^YZ0�-
&�#
!�
inputs����������
� "&�#
�
0����������
� �
6__inference_fmn2_pca_5000_1000_3_layer_call_fn_1731800QYZ0�-
&�#
!�
inputs����������
� "������������
S__inference_fmn2_pca_5000_1000_out_layer_call_and_return_conditional_losses_1731891]uv0�-
&�#
!�
inputs����������
� "%�"
�
0���������
� �
8__inference_fmn2_pca_5000_1000_out_layer_call_fn_1731880Puv0�-
&�#
!�
inputs����������
� "�����������
R__inference_fmn2_tsvd_5000_1000_1_layer_call_and_return_conditional_losses_1731671^()0�-
&�#
!�
inputs����������
� "&�#
�
0����������
� �
7__inference_fmn2_tsvd_5000_1000_1_layer_call_fn_1731660Q()0�-
&�#
!�
inputs����������
� "������������
R__inference_fmn2_tsvd_5000_1000_2_layer_call_and_return_conditional_losses_1731751^DE0�-
&�#
!�
inputs����������
� "&�#
�
0����������
� �
7__inference_fmn2_tsvd_5000_1000_2_layer_call_fn_1731740QDE0�-
&�#
!�
inputs����������
� "������������
R__inference_fmn2_tsvd_5000_1000_3_layer_call_and_return_conditional_losses_1731831^`a0�-
&�#
!�
inputs����������
� "&�#
�
0����������
� �
7__inference_fmn2_tsvd_5000_1000_3_layer_call_fn_1731820Q`a0�-
&�#
!�
inputs����������
� "������������
T__inference_fmn2_tsvd_5000_1000_out_layer_call_and_return_conditional_losses_1731911]|}0�-
&�#
!�
inputs����������
� "%�"
�
0���������
� �
9__inference_fmn2_tsvd_5000_1000_out_layer_call_fn_1731900P|}0�-
&�#
!�
inputs����������
� "�����������
C__inference_joined_layer_call_and_return_conditional_losses_1731988^��/�,
%�"
 �
inputs���������
� "%�"
�
0���������
� }
(__inference_joined_layer_call_fn_1731977Q��/�,
%�"
 �
inputs���������
� "�����������
B__inference_model_layer_call_and_return_conditional_losses_1731046�(67/0()!"RSKLDE=>nogh`aYZuv|}���������
���
���
3�0
fmn2_pca_5000_1000_input����������
4�1
fmn2_tsvd_5000_1000_input����������
6�3
fmn2_isomap_5000_1000_input����������
3�0
fmn2_lle_5000_1000_input����������
p 

 
� "%�"
�
0���������
� �
B__inference_model_layer_call_and_return_conditional_losses_1731139�(67/0()!"RSKLDE=>nogh`aYZuv|}���������
���
���
3�0
fmn2_pca_5000_1000_input����������
4�1
fmn2_tsvd_5000_1000_input����������
6�3
fmn2_isomap_5000_1000_input����������
3�0
fmn2_lle_5000_1000_input����������
p

 
� "%�"
�
0���������
� �
B__inference_model_layer_call_and_return_conditional_losses_1731503�(67/0()!"RSKLDE=>nogh`aYZuv|}���������
���
���
#� 
inputs/0����������
#� 
inputs/1����������
#� 
inputs/2����������
#� 
inputs/3����������
p 

 
� "%�"
�
0���������
� �
B__inference_model_layer_call_and_return_conditional_losses_1731631�(67/0()!"RSKLDE=>nogh`aYZuv|}���������
���
���
#� 
inputs/0����������
#� 
inputs/1����������
#� 
inputs/2����������
#� 
inputs/3����������
p

 
� "%�"
�
0���������
� �
'__inference_model_layer_call_fn_1730453�(67/0()!"RSKLDE=>nogh`aYZuv|}���������
���
���
3�0
fmn2_pca_5000_1000_input����������
4�1
fmn2_tsvd_5000_1000_input����������
6�3
fmn2_isomap_5000_1000_input����������
3�0
fmn2_lle_5000_1000_input����������
p 

 
� "�����������
'__inference_model_layer_call_fn_1730953�(67/0()!"RSKLDE=>nogh`aYZuv|}���������
���
���
3�0
fmn2_pca_5000_1000_input����������
4�1
fmn2_tsvd_5000_1000_input����������
6�3
fmn2_isomap_5000_1000_input����������
3�0
fmn2_lle_5000_1000_input����������
p

 
� "�����������
'__inference_model_layer_call_fn_1731299�(67/0()!"RSKLDE=>nogh`aYZuv|}���������
���
���
#� 
inputs/0����������
#� 
inputs/1����������
#� 
inputs/2����������
#� 
inputs/3����������
p 

 
� "�����������
'__inference_model_layer_call_fn_1731375�(67/0()!"RSKLDE=>nogh`aYZuv|}���������
���
���
#� 
inputs/0����������
#� 
inputs/1����������
#� 
inputs/2����������
#� 
inputs/3����������
p

 
� "�����������
%__inference_signature_wrapper_1731223�(67/0()!"RSKLDE=>nogh`aYZuv|}���������
� 
���
U
fmn2_isomap_5000_1000_input6�3
fmn2_isomap_5000_1000_input����������
O
fmn2_lle_5000_1000_input3�0
fmn2_lle_5000_1000_input����������
O
fmn2_pca_5000_1000_input3�0
fmn2_pca_5000_1000_input����������
Q
fmn2_tsvd_5000_1000_input4�1
fmn2_tsvd_5000_1000_input����������"/�,
*
joined �
joined���������