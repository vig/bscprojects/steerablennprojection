# Metrics are based on the work of Espadoto et al. 2020

import numpy as np
from sklearn.neighbors import KNeighborsClassifier
from scipy import spatial

#our evaluation metrics
def compute_distance_matrix(X):
    D = spatial.distance.pdist(X, 'euclidean')
    return spatial.distance.squareform(D)

def trustworthiness(validation_images, steerable_proj, k=7):
    D_high = compute_distance_matrix(validation_images)
    D_low = compute_distance_matrix(steerable_proj)

    n = validation_images.shape[0]

    nn_orig = D_high.argsort()
    nn_proj = D_low.argsort()

    knn_orig = nn_orig[:, :k + 1][:, 1:]
    knn_proj = nn_proj[:, :k + 1][:, 1:]

    sum_i = 0

    for i in range(n):
        U = np.setdiff1d(knn_proj[i], knn_orig[i])

        sum_j = 0
        for j in range(U.shape[0]):
            sum_j += np.where(nn_orig[i] == U[j])[0] - k

        sum_i += sum_j

    return float((1 - (2 / (n * k * (2 * n - 3 * k - 1)) * sum_i)).squeeze())


def continuity(validation_images, steerable_proj, k=7):
    D_high = compute_distance_matrix(validation_images)
    D_low = compute_distance_matrix(steerable_proj)

    n = validation_images.shape[0]

    nn_orig = D_high.argsort()
    nn_proj = D_low.argsort()

    knn_orig = nn_orig[:, :k + 1][:, 1:]
    knn_proj = nn_proj[:, :k + 1][:, 1:]

    sum_i = 0

    for i in range(n):
        V = np.setdiff1d(knn_orig[i], knn_proj[i])

        sum_j = 0
        for j in range(V.shape[0]):
            sum_j += np.where(nn_proj[i] == V[j])[0] - k

        sum_i += sum_j

    return float((1 - (2 / (n * k * (2 * n - 3 * k - 1)) * sum_i)).squeeze())


def neighborhood_hit(steerable_proj, class_labels, k=7):
    knn = KNeighborsClassifier(n_neighbors=k)
    knn.fit(steerable_proj, class_labels)

    neighbors = knn.kneighbors(steerable_proj, return_distance=False)
    return np.mean(np.mean((class_labels[neighbors] == np.tile(class_labels.reshape((-1, 1)), k)).astype('uint8'), axis=1))