<h1>Steerable MNN Projection by Joep Robben</h1>
<h2>Intro</h2>
This is the Git repository of the Steerable MNN Projection Bachelor's Thesis project by Joep Robben. This repository contains every piece of code used in this project, including the method pipeline, visualization tool prototype, experiment, and analysis. For a short overview, you can check out the presentation. In the thesis, a more eleborate explanation and evaluation of the method is provided. 

<br>

<h2>Installation</h2>
In order to run an experiment and the prototype, a few packages must be installed first. 

These are:
- [scikitlearn](https://scikit-learn.org/stable/install.html)
- [tensorflow](https://www.tensorflow.org/install)
- [pandas](https://pandas.pydata.org/docs/getting_started/install.html)
- [numpy](https://numpy.org/install/)
- [plotly](https://plotly.com/python/getting-started/)
- [dash](https://dash.plotly.com/installation)
- [matplotlib](https://matplotlib.org/stable/users/installing/index.html)
- [scipy](https://scipy.org/install/)

To install these packages, you can use one of the following commands in your terminal. For installation with PIP, use:
```
pip install scipy matplotlib dash plotly numpy pandas tensorflow scikit-learn
```
For installation with conda, use:
```
conda install scipy matplotlib dash plotly numpy pandas tensorflow scikit-learn
```
<h2>Basic usage</h2>
<h3>Running the tool</h3>
The visualization tool can be used to debug the method and view its results. In this tool, configurations can be set to run single experiments. 

To run the visualization tool, enter
```
index.py
```
in your terminal or just press the run button inside the [index.py](index.py) file.

Now the tool is running, you can view it inside your webbrowser. Open the localhost page inside your browser. The default port is 8050. You should see the following:

![The visualization tool](results/plots/100.png)

<h3>Parameters and functionality</h3>
<b>Projection techniques</b> - With the current version of the tool, you can use 3 projection techniques. Choose them using the dropdown menues and adjust the proportions using the sliders. For the best results, make sure the proportions amount to 100. 

<br>
<b>Data set</b> - There are two data sets to choose from: MNIST and Fashion MNIST. For MNIST enter 'nmnist' (number MNIST) inside the input field. For Fashion MNIST, use 'fmnist' (Fashion MNIST) inside the input field.

<br>
<b>Classes</b> - You can choose between 2 or 10 classes the method takes into account. Simply enter 2 or 10 inside the input field.

<br>
<b>Number of training samples</b> - You can freely choose a number of training samples.

<br>
<b>Scores</b> - After running, the tool shows the quality scores. You can find a more elaborate explanation for each score in the thesis.

<br>
<b>GO button</b> - After you have set the configuration you want to use, you can run it by pressing the 'GO!' button. A loading wheel will appear during runtime and the generated projection and scores will show after the method is finished running.

<br>
<b>Interactive plot</b> - The interactive plot provides many functionalities, such as various selection tools, zooming, panning, and saving the plot as .PNG. In addition, when clicking on a label in the legend, you can select the classes you want to appear on the plot. Hovering over a data point show the exact values of this point.

<br>
<h3>Large scale experiments</h3>
The experiment.py file provides the possibility to run large scale experiments using the method. Inside this file, you can set parameters for such an experiment. 

After setting the configuration, enter
```
experiment.py
```
in your terminal or just press the run button inside the [experiment.py](experiment.py) file.

Results of an experiment can be found inside of the [results](/results/) folder.