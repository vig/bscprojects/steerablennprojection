import pandas as pd
import plotly.express as px
import plotly
import matplotlib.pyplot as plt
import os
import plotly.graph_objects as go
from plotly.subplots import make_subplots

#display mnist image
def display_image(array, idx):
    plt.figure()
    plt.imshow(array[idx])
    plt.colorbar()
    plt.grid(False)
    plt.show()

#save a generated projection
def save_plot(dataframe, dataset, number_of_classes, dr_name_lower, number_of_points):
    fig = px.scatter(dataframe, x="PC1", y="PC2", color="label", title=('{b}{e}_{w}_{g}').format(b=dataset[0:3],e=number_of_classes, w=dr_name_lower, g=number_of_points))
    fig = fig.update_yaxes(range=[-0.1, 1.1])
    fig = fig.update_xaxes(range=[-0.1, 1.1])
    fig = fig.update_traces(marker=dict(size=1.2))
    try:
        os.mkdir(('plots/projections/{b}{e}/').format(b=dataset[0:3],e=number_of_classes))
    except:
        pass
    plotly.io.write_image(fig, ('plots/projections/{b}{e}/{b}{e}_{w}_{g}.png').format(b=dataset[0:3],e=number_of_classes, w=dr_name_lower, g=number_of_points), format='png', scale=5, width=None, height=None, validate=True, engine='auto')

#show a plot
def show_plot(dataframe, dataset, number_of_classes, dr_name_lower, number_of_points):
    fig = px.scatter(dataframe, x="PC1", y="PC2", color="label", title=('{b}{e}_{w}_{g}').format(b=dataset[0:3],e=number_of_classes, w=dr_name_lower, g=number_of_points))
    fig = fig.update_yaxes(range=[-0.1, 1.1])
    fig = fig.update_xaxes(range=[-0.1, 1.1])
    fig = fig.update_traces(marker=dict(size=1.2))
    fig.show()

#save a history plot of the model
def save_historyplot(history, dataset, number_of_classes, dr_name_lower, number_of_points):
    dfHistory = pd.DataFrame(history.history)
    dfHistory['epoch'] = list(range(1,len(history.history['loss']) + 1))

    fig = make_subplots(rows=1, cols=2, start_cell="bottom-left", x_title='epochs')

    fig.add_trace(go.Scatter(x=dfHistory['epoch'], y=dfHistory["loss"], name="loss"),
                row=1, col=1)
    fig.add_trace(go.Scatter(x=dfHistory['epoch'], y=dfHistory["val_loss"], name="val_loss"),
                row=1, col=1)
    fig.add_trace(go.Scatter(x=dfHistory['epoch'], y=dfHistory["accuracy"], name="accuracy"),
                row=1, col=2)
    fig.add_trace(go.Scatter(x=dfHistory['epoch'], y=dfHistory["val_accuracy"],  name="val_accuracy"),
                row=1, col=2)

    fig.update_xaxes(title_text="epochs", row=1, col=1)
    fig.update_xaxes(title_text="epochs", row=1, col=2)
    fig.update_yaxes(title_text="loss", row=1, col=1)
    fig.update_yaxes(title_text="accuracy", row=1, col=2)

    fig = fig.update_layout(height=600, width=1000, title='his_{b}{e}_{w}_{g}'.format(b=dataset[0:3],e=number_of_classes, w=dr_name_lower, g=number_of_points))

    try:
        os.mkdir(('plots/history/his_{b}{e}/').format(b=dataset[0:3],e=number_of_classes))
    except:
        pass
    plotly.io.write_image(fig, ('plots/history/his_{b}{e}/his_{b}{e}_{w}_{g}.png').format(b=dataset[0:3],e=number_of_classes, w=dr_name_lower, g=number_of_points), format='png', scale=5, width=None, height=None, validate=True, engine='auto')
