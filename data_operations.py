import pandas as pd
import tensorflow as tf
from tensorflow import keras
import numpy as np

#loading the data
def load_data(dataset, number_of_classes):
    if dataset == 'nmnist':
        number_mnist = keras.datasets.mnist
        (train_images, train_labels), (test_images, test_labels) = number_mnist.load_data()
        classes = determine_classes(dataset, number_of_classes)
        train_images, test_images = normalize_data(dataset, train_images, test_images)
    
    if dataset == 'fmnist':
        fashion_mnist = keras.datasets.fashion_mnist
        (train_images, train_labels), (test_images, test_labels) = fashion_mnist.load_data()
        classes = determine_classes(dataset, number_of_classes)
        train_images, test_images = normalize_data(dataset, train_images, test_images)
    
    print("Train images", train_images.shape)
    print("Train labels", train_labels.shape)
    print("Test images", test_images.shape)
    print("Test labels", test_labels.shape)
    print("CLASSES",classes)

    return (train_images, train_labels), (test_images, test_labels), classes

#return list of chosen classes
def determine_classes(dataset, number_of_classes):
    classes = []
    if dataset == 'nmnist' or 'fmnist':
        if number_of_classes == 2:
            classes = ["0", "1"]
        if number_of_classes == 10:
            classes = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
    return classes

#normalize images data
def normalize_data(dataset, train_images, test_images):
    if dataset == 'nmnist' or 'fmnist':
        train_images = train_images / 255.0
        test_images = test_images / 255.0
    
    return train_images, test_images

#select only the images of the chosen classes
def select_data_classes(classes, train_images, train_labels, test_images, test_labels):
    train_images_mask = [train_images[key] for (key, label) in enumerate(train_labels) if str(label) in classes]
    train_images_mask = np.array(train_images_mask)

    train_labels_mask = [train_labels[key] for (key, label) in enumerate(train_labels) if str(label) in classes]
    train_labels_mask = np.array(train_labels_mask)

    test_images_mask = [test_images[key] for (key, label) in enumerate(test_labels) if str(label) in classes]
    test_images_mask = np.array(test_images_mask)

    test_labels_mask = [test_labels[key] for (key, label) in enumerate(test_labels) if str(label) in classes]
    test_labels_mask = np.array(test_labels_mask)

    print("Train images mask", train_images_mask.shape)
    print("Train labels mask", train_labels_mask.shape)
    print("Test images mask", test_images_mask.shape)
    print("Test labels mask", test_labels_mask.shape)

    return train_images_mask, train_labels_mask, test_images_mask, test_labels_mask

#reshape images to 2D
def reshapeXD2D(train_images_mask):
    nsamples, nx, ny = train_images_mask.shape
    train_images2D = train_images_mask.reshape((nsamples, nx * ny))

    print("Images reshaped", train_images2D.shape)

    return train_images2D

#create training and testing samples
def sample_data(train_images_mask, train_labels_mask, list_of_projections, steerable_projection, number_of_Tsamples, number_of_Vsamples, indeces):
    subset_Tidx = np.random.choice(indeces, number_of_Tsamples, replace=False)
    not_Tidx = []
    k = 0
    for j in range(len(train_images_mask)):
        if k not in subset_Tidx and k in indeces:
            not_Tidx.append(k)
        k = k + 1
    subset_Vidx = np.random.choice(not_Tidx, number_of_Vsamples, replace=False)

    train_images_proj = []
    train_labels_proj = []
    h = 0
    for image, label in zip(train_images_mask, train_labels_mask):
        if h in indeces:
            train_images_proj.append(image)
            train_labels_proj.append(label)
        h = h + 1

    train_images_samp = []
    train_labels_samp = []
    list_of_tr_proj_samp = []
    train_sproj = []
    val_images_samp = []
    val_labels_samp = []
    list_of_val_proj_samp = []
    val_sproj = []

    for index, image, label, sproj in zip(indeces, train_images_proj, train_labels_proj, steerable_projection):
        if index in subset_Tidx:
            train_images_samp.append(image)
            train_labels_samp.append(label)
            train_sproj.append(sproj)
        if index in subset_Vidx:
            val_images_samp.append(image)
            val_labels_samp.append(label)
            val_sproj.append(sproj)

    for projection in list_of_projections:
        temp_tr_list = []
        temp_val_list = []
        for index, proj in zip(indeces, projection):
            if index in subset_Tidx:
                temp_tr_list.append(proj)
            if index in subset_Vidx:
                temp_val_list.append(proj)

        list_of_tr_proj_samp.append(temp_tr_list)
        list_of_val_proj_samp.append(temp_val_list)

    train_images_sample = np.array(train_images_samp)
    train_labels_sample = np.array(train_labels_samp)
    list_of_tr_proj_samp = np.array(list_of_tr_proj_samp)
    train_sproj = np.array(train_sproj)
    val_images_sample = np.array(val_images_samp)
    val_labels_sample = np.array(val_labels_samp)
    list_of_val_proj_samp = np.array(list_of_val_proj_samp)
    val_sproj = np.array(val_sproj)

    print("Train images sample", train_images_sample.shape)
    print("Train labels sample", train_labels_sample.shape)
    print("List train projection sample", list_of_tr_proj_samp.shape)
    print("Train steerable projection sample", train_sproj.shape)

    print("Validation images sample", val_images_sample.shape)
    print("Validation lables sample", val_labels_sample.shape)
    print("List validation projection sample", list_of_val_proj_samp.shape)
    print("Validation steerable projection sample", val_sproj.shape)

    return train_images_sample, train_labels_sample, list_of_tr_proj_samp, train_sproj, val_images_sample, val_labels_sample, list_of_val_proj_samp, val_sproj

#creating training samples for generating precomputed projection
def sample_proj_data(train_images_mask, train_labels_mask, number_of_points):
    #subset_idx = np.random.choice(len(train_images_mask), number_of_points, replace=False)

    subset_idx = []
    for num in range(number_of_points):
        subset_idx.append(num)

    train_images_sample = []
    train_labels_sample = []

    i = 0
    for image, label in zip(train_images_mask, train_labels_mask):
        if i in subset_idx:
            train_images_sample.append(image)
            train_labels_sample.append(label)
        i = i + 1

    train_images_sample = np.array(train_images_sample)
    train_labels_sample = np.array(train_labels_sample)

    print("Train images sample", train_images_sample.shape)
    print("Train labels sample", train_labels_sample.shape)

    return train_images_sample, train_labels_sample, subset_idx

#create a precomputed steerable projection
def create_steerable(proportions, list_of_projections):
    print(proportions)
    list_of_proportions = []
    i = 0
    for proj in list_of_projections:
        proportion = float(proportions[i]) * proj
        list_of_proportions.append(proportion)
        i = i + 1

    list_of_dfps = []
    for prop in list_of_proportions:
        dfp = pd.DataFrame(data = prop, columns=["PC1", "PC2"])
        list_of_dfps.append(dfp)

    print("projection dfs")
    for dfp in list_of_dfps:
        print(dfp[0:5])
        print("")

    dfsteerable_projection = sum(list_of_dfps)
    print("steerable projection",dfsteerable_projection[0:5])
    steerable_projection = dfsteerable_projection.to_numpy()

    return steerable_projection
