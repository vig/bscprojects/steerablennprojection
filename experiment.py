import json
import os
import method as me

#variables used in our experiment
datasets = ["nmnist", "fmnist"]
number_of_classes = ["2", "10"]
number_of_training_samples = ["2000", "5000"]
techniques = [["PCA"], ["PCA", "TSVD"], ["PCA", "TSVD", "ISOMAP"], ["PCA", "TSVD", "ISOMAP", "LLE"], ["PCA", "TSVD", "ISOMAP", "LLE", "UMAP"]]
proportions = [[[1]], [[1, 0], [0.5, 0.5]], [[1, 0, 0], [0.5, 0.5, 0]], [[1, 0, 0, 0], [0.5, 0.5, 0, 0]], [[1, 0, 0, 0, 0], [0.5, 0.5, 0, 0, 0]]]


#performing the experiment
for data in datasets:
    for classes in number_of_classes:
        for sample in number_of_training_samples:
            for technique in techniques:
                le = len(technique)
                prop = proportions[le-1]        
                for pr in prop:
                    filename = "config.json"
                    with open(filename) as jsonFile:
                            jsonObject = json.load(jsonFile)
                            jsonObject['DATASET'] = data
                            jsonObject['DRS'] = technique
                            jsonObject['PROPORTIONS'] = pr
                            jsonObject['NUMBER_OF_TSAMPLES'] = sample
                            jsonObject['NUMBER_OF_CLASSES'] = classes

                    os.remove(filename)
                    with open(filename, 'w') as f:
                        json.dump(jsonObject, f, indent=2)

                    me.method()


