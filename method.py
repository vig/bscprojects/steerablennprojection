def method():
    #imports 
    import pandas as pd
    import tensorflow as tf
    from tensorflow import keras
    import numpy as np
    import matplotlib.pyplot as plt
    from sklearn.preprocessing import MinMaxScaler
    import json
    import data_operations as do
    import figure_operations as fo
    from tqdm import tqdm
    import os
    import plotly.express as px
    from tensorflow.keras import initializers, layers
    import evaluations as ev

    #open json file
    with open("config.json") as jsonFile:
        jsonObject = json.load(jsonFile)
        jsonFile.close()

    #constants
    NUMBER_OF_POINTS = int(jsonObject['NUMBER_OF_POINTS'])
    NUMBER_OF_TSAMPLES = int(jsonObject['NUMBER_OF_TSAMPLES'])
    NUMBER_OF_VSAMPLES = int(jsonObject['NUMBER_OF_VSAMPLES'])
    DRS = list(jsonObject['DRS'])
    REPLACE = int(jsonObject['REPLACE'])
    PROPORTIONS = list(jsonObject['PROPORTIONS'])
    DRS_NAME = ''.join(DRS).lower()
    PROPORTIONS_NAME = ''.join(str(PROPORTIONS)).lower()
    DATASET = str(jsonObject['DATASET'])
    NUMBER_OF_CLASSES = int(jsonObject['NUMBER_OF_CLASSES'])

    #check whether it's a new configuration to save time
    new_config = 0
    for dr in DRS:
        projection_name = ('projections/{b}{e}_{w}_{g}.csv').format(b=DATASET[0:3],e=NUMBER_OF_CLASSES, w=str(dr).lower(), g=NUMBER_OF_POINTS)
        if os.path.exists(projection_name) == 0:
            new_config = 1
    print("NEW_CONFIG =",new_config)

    if new_config == 1:
        projection_generation()

    print("DRS =",DRS)
    print("PROP =",PROPORTIONS)

    model_path = ('models/{b}{e}_{w}{v}_{f}_{g}').format(b=DATASET[0:3],e=NUMBER_OF_CLASSES, w=DRS_NAME, v=PROPORTIONS, f=NUMBER_OF_TSAMPLES, g=NUMBER_OF_VSAMPLES)

    if os.path.exists(model_path) == 1 and REPLACE == 0:
        predictions = ('results/projections/{b}{e}_{w}{v}_{f}_{g}.csv').format(b=DATASET[0:3],e=NUMBER_OF_CLASSES, w=DRS_NAME, v=PROPORTIONS, f=NUMBER_OF_TSAMPLES, g=NUMBER_OF_VSAMPLES)
        
        with open(('results/scores/{b}{e}_{w}{v}_{f}_{g}.json').format(b=DATASET[0:3],e=NUMBER_OF_CLASSES, w=DRS_NAME, v=PROPORTIONS, f=NUMBER_OF_TSAMPLES, g=NUMBER_OF_VSAMPLES)) as jsonFile:
            results = json.load(jsonFile)
            jsonFile.close()

        #constants
        loss = float(results['loss'])
        trustworthiness = float(results['trustworthiness'])
        continuity = float(results['continuity'])
        neighborhood_hit = float(results['neighborhood_hit'])

        print("Model already exists")




    else:

        #load dataset
        (train_images, train_labels), (test_images, test_labels), CLASSES = do.load_data(DATASET, NUMBER_OF_CLASSES)

        #selecting data based on chosen classes
        train_images_mask, train_labels_mask, test_images_mask, test_labels_mask = do.select_data_classes(CLASSES, train_images, train_labels, test_images, test_labels)

        list_of_projections = []
        indeces = []

        for dr in DRS:
            dfProjection = pd.read_csv(("projections/{b}{e}_{w}_{g}.csv").format(b=DATASET[0:3],e=NUMBER_OF_CLASSES, w=dr, g=NUMBER_OF_POINTS))
            indeces = dfProjection['index'].tolist()
            dfProjection = dfProjection.drop(columns=['index'], axis=0)
            projection = dfProjection.to_numpy()
            list_of_projections.append(projection)

        n = 0
        for pro in list_of_projections:
            print("projection of",DRS[n])
            print(pro[0:5])
            n = n + 1

        #create steerable projection
        steerable_projection = do.create_steerable(PROPORTIONS, list_of_projections)

        #sampling training data (note that the test data is not taken into account)
        training_images_sample,training_labels_sample,list_of_tr_proj_sample,training_sproj,validation_images,validation_labels,list_of_val_proj_sample,validation_sproj = do.sample_data(train_images_mask, train_labels_mask, list_of_projections, steerable_projection, NUMBER_OF_TSAMPLES, NUMBER_OF_VSAMPLES, indeces)

        training_images_flatten = do.reshapeXD2D(training_images_sample)
        validation_images_flatten = do.reshapeXD2D(validation_images)

        #model architecture
        weight_init = tf.keras.initializers.HeUniform()
        bias_init = initializers.Constant(0.0001)
        module_epochs = 40
        model_epochs = 80

        list_of_modules = []
        for dr in DRS:
            name = ('{b}{e}_{w}_{g}_{h}').format(b=DATASET[0:3],e=NUMBER_OF_CLASSES, w=str(dr).lower(), g=NUMBER_OF_TSAMPLES, h=NUMBER_OF_VSAMPLES)
            module_path = ('models/{o}').format(o=name)  
            if os.path.exists(module_path) == 1 and REPLACE == 0:
                y = keras.models.load_model(module_path)
                list_of_modules.append(y)

            else:
                input = keras.Input(shape = (784,), name=("{p}_input").format(p=name))
                x = layers.Dense(256, activation='relu', kernel_initializer= weight_init, bias_initializer=bias_init, name=("{q}_1").format(q=name))(input)
                x = layers.Dense(512, activation='relu', kernel_initializer= weight_init, bias_initializer=bias_init, name=("{q}_2").format(q=name))(x)
                x = layers.Dense(256, activation='relu', kernel_initializer= weight_init, bias_initializer=bias_init, name=("{q}_3").format(q=name))(x)
                x = layers.Dense(2, activation='sigmoid', kernel_initializer= weight_init, bias_initializer=bias_init, name=("{q}_out").format(q=name))(x)
                x = keras.Model(input, x, name=name)
                list_of_modules.append(x)

        list_of_outputs = []
        list_of_inputs = []
        for module in list_of_modules:
            output = module.output
            input = module.input
            list_of_outputs.append(output)
            list_of_inputs.append(input)

        # combine the output of the two branches
        combined = layers.Concatenate()(list_of_outputs)
        z = layers.Dense(2, activation="sigmoid", name="joined", kernel_initializer= weight_init, bias_initializer=bias_init)(combined)
        model = keras.Model(inputs=list_of_inputs, outputs=z, name="model")

        #compiling
        k = 0
        for module in list_of_modules:
            name = ('{b}{e}_{w}_{g}_{h}').format(b=DATASET[0:3],e=NUMBER_OF_CLASSES, w=str(DRS[k]).lower(), g=NUMBER_OF_TSAMPLES, h=NUMBER_OF_VSAMPLES)
            module_path = ('models/{o}').format(o=name)  
            if os.path.exists(module_path) == 1 and REPLACE == 0:
                pass
            else:
                print("=================================================================")
                print(("{a}").format(a=name))
                module.compile(optimizer='adam',
                            loss=tf.keras.losses.MeanSquaredError(),
                            metrics=['accuracy'])

                #training x
                callback = tf.keras.callbacks.EarlyStopping(monitor='loss', patience=4)

                module.fit(x=training_images_flatten, y=list_of_tr_proj_sample[k], validation_data=(validation_images_flatten, list_of_val_proj_sample[k]), epochs=module_epochs, shuffle=True, callbacks=[callback])
                module.save(module_path)
            k = k + 1

        print("MODEL")
        #set previously trained layers non-trainable
        for module in list_of_modules:
            for layer in module.layers[:]:
                layer.trainable = False

        #compiling model
        model.compile(optimizer='adam',
                    loss=tf.keras.losses.MeanSquaredError(),
                    metrics=['accuracy'])

        #creating the right shape of model input
        test_images_flatten = do.reshapeXD2D(test_images_mask)

        model_x_train = []
        model_x_val = []
        model_x_test = []
        for i in range(len(DRS)):
            model_x_train.append(training_images_flatten)
            model_x_val.append(validation_images_flatten)
            model_x_test.append(test_images_flatten)

        #training the model
        callback = tf.keras.callbacks.EarlyStopping(monitor='loss', patience=4)
        modelhistory = model.fit(x=(model_x_train), y=training_sproj, validation_data=((model_x_val), validation_sproj), epochs=model_epochs, shuffle=True, callbacks=[callback])
        model.save(model_path)
        
        drs_name = DRS_NAME + PROPORTIONS_NAME

        #predicting projections
        predictions = model.predict(model_x_test)
        dfPRED = pd.DataFrame(data = predictions, columns = ["PC1", "PC2"])
        dfPRED["label"] = test_labels_mask
        dfPRED = dfPRED.sort_values(by=['label'])
        dfPRED.to_csv(('results/projections/{b}{e}_{w}_{f}_{g}.csv').format(b=DATASET[0:3],e=NUMBER_OF_CLASSES, w=drs_name, f=NUMBER_OF_TSAMPLES, g=NUMBER_OF_VSAMPLES), index=False)
        print("predictions")
        print(dfPRED.head())
        print("")

        

        #evaluating the model
        loss, acc = model.evaluate(model_x_val, validation_sproj, verbose=0)
        trustworthiness = ev.trustworthiness(validation_images=test_images_flatten, steerable_proj=predictions)
        continuity = ev.continuity(validation_images=test_images_flatten, steerable_proj=predictions)
        neighborhood_hit = ev.neighborhood_hit(steerable_proj=predictions, class_labels=test_labels_mask)

        #saving evaluations
        eval_data ={
            "dimensionality_reduction_methods" : str(DRS),
            "proportions" : str(PROPORTIONS),
            "dataset" : str(DATASET),
            "number_of_classes" : str(NUMBER_OF_CLASSES),
            "number_of_points" : str(NUMBER_OF_POINTS),
            "number_of_Tsamples" : str(NUMBER_OF_TSAMPLES),
            "number_ofVsamples" : str(NUMBER_OF_VSAMPLES),
            "loss" : str(loss),

            "trustworthiness" : str(trustworthiness),
            "continuity" : str(continuity),
            "neighborhood_hit" : str(neighborhood_hit)
        }

        with open(('results/scores/{b}{e}_{w}_{f}_{g}.json').format(b=DATASET[0:3],e=NUMBER_OF_CLASSES, w=drs_name, f=NUMBER_OF_TSAMPLES, g=NUMBER_OF_VSAMPLES), "w") as f:
            json.dump(eval_data, f, ensure_ascii=False, indent=4)

    return loss, trustworthiness, continuity, neighborhood_hit 


def projection_generation():
    #imports 
    import pandas as pd
    import tensorflow as tf
    from tensorflow import keras
    import numpy as np
    import matplotlib.pyplot as plt
    from sklearn.preprocessing import MinMaxScaler
    import json
    import data_operations as do
    import figure_operations as fo
    from tqdm import tqdm
    import os
    import plotly.express as px
    from tensorflow.keras import initializers, layers
    import evaluations as ev

    #open json file
    with open("config.json") as jsonFile:
        jsonObject = json.load(jsonFile)
        jsonFile.close()

    #constants
    NUMBER_OF_POINTS = int(jsonObject['NUMBER_OF_POINTS'])
    DRS = list(jsonObject['DRS'])
    DRS_NAME = ''.join(DRS).lower()
    DATASET = str(jsonObject['DATASET'])
    NUMBER_OF_CLASSES = int(jsonObject['NUMBER_OF_CLASSES'])

    #load dataset
    (train_images, train_labels), (test_images, test_labels), CLASSES = do.load_data(DATASET, NUMBER_OF_CLASSES)

    #selecting data based on chosen classes
    train_images_mask, train_labels_mask, test_images_mask, test_labels_mask = do.select_data_classes(CLASSES, train_images, train_labels, test_images, test_labels)

    #sampling training data (note that the test data is not taken into account)
    training_images_sample,training_labels_sample, subset_idx = do.sample_proj_data(train_images_mask, train_labels_mask, NUMBER_OF_POINTS)

    #reshaping 3D data into 2D data
    train_images2D = do.reshapeXD2D(training_images_sample)

    #generate projections for every chosen DR technique
    from sklearn.decomposition import PCA
    from sklearn.manifold import MDS
    from sklearn.manifold import TSNE
    from sklearn.manifold import LocallyLinearEmbedding as LLE
    from sklearn.manifold import Isomap
    from sklearn.decomposition import TruncatedSVD
    from umap import UMAP

    new_config = []
    for dr in DRS:
        projection_name = ('projections/{b}{e}_{w}_{g}.csv').format(b=DATASET[0:3],e=NUMBER_OF_CLASSES, w=str(dr).lower(), g=NUMBER_OF_POINTS)
        if os.path.exists(projection_name) == 0:
            new_config.append(dr)

    for dr in tqdm(new_config):
        if dr in DRS:
            print("DR method currently running is", dr)
            dr_name = dr
            dr_name_lower = dr_name.lower()
            if dr_name == 'PCA':    
                dr_obj = PCA(n_components=2)
            if dr_name == 'MDS':
                dr_obj= MDS(n_components=2)
            if dr_name == 'TSNE':
                dr_obj = TSNE(n_components=2)
            if dr_name == 'LLE':
                dr_obj = LLE(n_components=2)
            if dr_name == 'UMAP':
                dr_obj = UMAP(n_components=2)
            if dr_name == 'ISOMAP':
                dr_obj = Isomap(n_components=2)
            if dr_name == 'TSVD':
                dr_obj = TruncatedSVD(n_components=2)
            
            drArray = dr_obj.fit_transform(train_images2D)

            scaler = MinMaxScaler()
            drNorm = scaler.fit_transform(drArray)

            dfDR = pd.DataFrame(data = drNorm, columns = ['PC1', "PC2"])
            dfDR["index"] = subset_idx
            dfDR_label = dfDR.copy()
            dfDR_label["label"] = training_labels_sample
            dfDR_label = dfDR_label.sort_values(by=['label'])

            dfDR.to_csv(('projections/{b}{e}_{w}_{g}.csv').format(b=DATASET[0:3],e=NUMBER_OF_CLASSES, w=dr_name_lower, g=len(training_images_sample)), index=False)
            fo.save_plot(dfDR_label, DATASET, NUMBER_OF_CLASSES, dr_name_lower, len(dfDR_label))
